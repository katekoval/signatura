<?php
// @codingStandardsIgnoreFile
require_once '../abstract.php';

/**
 * @category Webcooking
 * @package  Webcooking_GoogleUniversalAnalytics
 */
class Webcooking_Shell_GoogleUniversalAnalytics extends Mage_Shell_Abstract
{
    public function run()
    {
        error_reporting(E_ALL);
        ini_set('error_reporting', E_ALL);
        ini_set('max_execution_time', 360000);
        set_time_limit(360000);

        if ($this->getArg('sendtransaction')) {
            $items = false;
            /*if($this->getArg('items')) {
                $rawItems = explode(',', $this->getArg('items'));
                foreach($rawItems as $rawItem) {
                    $rawItemParts = explode(':', $rawItem);
                    if(count($rawItemParts) != '2') {
                        echo "Error in items paramater\n";
                        exit();
                    }
                    $items[$rawItemParts[0]] = $rawItemParts[1];
                }
            }*/
            $this->_sendTransaction($this->getArg('sendtransaction'), $this->getArg('force'), $items);
        } elseif ($this->getArg('canceltransaction')) {
            $items = false;
            /*if($this->getArg('items')) {
                $rawItems = explode(',', $this->getArg('items'));
                foreach($rawItems as $rawItem) {
                    $rawItemParts = explode(':', $rawItem);
                    if(count($rawItemParts) != '2') {
                        echo "Error in items paramater\n";
                        exit();
                    }
                    $items[$rawItemParts[0]] = $rawItemParts[1];
                }
            }*/
            $this->_sendTransaction($this->getArg('canceltransaction'), $this->getArg('force'), $items, true);
        }  elseif ($this->getArg('sendqueuedhits')) {
            $cron = Mage::getModel('googleuniversalanalytics/cron');
            $cron->sendQueuedHits();
        } elseif ($this->getArg('importproductdata')) {
            $cron = Mage::getModel('googleuniversalanalytics/cron');
            $cron->importProductData();
        } else {
            echo $this->usageHelp();
        }
    }
    
    protected function _sendTransaction($orderIncrementId, $force = false, $items=false, $cancel = false) {
        $order = Mage::getModel('sales/order')->loadByIncrementId($orderIncrementId);
        if(!$order || !$order->getId()) {
           echo "Order not found\n";
           exit();
        }
        if(!$force && $order->getData('gua_sent_flag')) {
           echo "Order already sent. Use --force to send the hit anyway\n";
           exit();
        }
        $mp = Mage::getModel('googleuniversalanalytics/measurement_protocol');
        $mp->sendTransactionRequestFromOrder($order, $cancel);
    }

    /**
     * Retrieve Usage Help Message
     *
     */
    public function usageHelp()
    {
        return <<<USAGE
Usage:  php -f googleuniversalanalytics.php -- [options]
                    
  --sendtransaction [--force] <orderIncrementId> Send transaction from order
  --canceltransaction <orderIncrementId>  Cancel transaction from order
  --importproductdata Import Product Data from GA
  --sendqueuedhits Send queued hits. Warning, should not be run when crons are running.
  --help              Help

USAGE;
    }
}

$shell = new Webcooking_Shell_GoogleUniversalAnalytics();
$shell->run();
