#/usr/bin/env sh
a="/$0"; a=${a%/*}; a=${a#/}; a=${a:-.}; BASEDIR=$(cd "$a"; pwd)

if [ -f $BASEDIR/app/etc/local.xml ]; then
  echo "local.xml already exists";
  exit 1;
fi

PACKAGE_MANAGER=$(which yarn || which npm 2>/dev/null)

if [ -z "$PACKAGE_MANAGER" ]; then
  echo "Neither yarn nor npm is installed."
  exit 1;
fi

if [ ! -d $BASEDIR/node_modules ]; then
  $PACKAGE_MANAGER install
fi
  
cp $BASEDIR/app/etc/local.xml.docker $BASEDIR/app/etc/local.xml
docker-compose run --rm app mysql_install_db
echo "docker-compose up"
docker-compose up -d
echo "Waiting for MySQL to be up..."
sleep 10
echo "Creating DB"
docker-compose exec app mysql -uroot -e "CREATE DATABASE magento;"
echo "Installing db..."
curl -sS 'http://127.0.0.1:3980/index.php/install/wizard/installDb/'>/dev/null

echo "Creating admin user..."
docker-compose exec app php /var/www/magento/shell/n98-magerun.phar --root-dir=/var/www/magento admin:user:create

echo "Setting base URL"
docker-compose exec app mysql -uroot magento -e 'INSERT INTO core_config_data(path,value) VALUES("web/unsecure/base_url", "http://127.0.0.1:3980/")'

echo "Done!"
open 'http://127.0.0.1:3980/admin'
open 'http://127.0.0.1:3980/'
