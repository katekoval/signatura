# IMPORTANT
Please run `yarn install` to ensure that your commits follow the conventionalcommits guideline!
#### conventional commits? wtf is that? 
Commits should follow the format:
```
type(optional scope): short description aka subject

optional body

optional footer
```
> Read more at [conventionalcommits.org](https://conventionalcommits.org)

tl;dr: These are valid commit messages:
```
- feat: added tax module
- style(css): changed shade of green for the add to cart button
- fix(checkout): cart didn't update when clicking delete
```
These are *NOT* valid commit messages:
```
- Fixed a bug (this message has no type, e.g. "feat: ")
- css: hide box on small ("css: " is not a valid type)
- feat: Descriptions cannot start with uppercase 
- style: the header line (the first line of the commit message) cannot be longer than 72 characters. 

  need to add more context? use the commit body.
```

## Installing
### Automatic
`./install.sh` from the Magento root.
### Manual
1. Duplicate `app/etc/local.xml.docker` and rename it to `app/etc/local.xml`
2. Run `docker-compose run --rm app mysql_install_db` to initialize MySQL
3. Start docker: `docker-compose up -d`
4. Create a database for Magento: `docker-compose exec app mysql -uroot -e "CREATE DATABASE magento;"` 
5. Visit `http://127.0.0.1:3980/index.php/install/wizard/installDb/`
6. Create an admin user: `docker-compose exec app php /var/www/magento/shell/n98-magerun.phar --root-dir=/var/www/magento admin:user:create`
7. Run `yarn install` or `npm install`
