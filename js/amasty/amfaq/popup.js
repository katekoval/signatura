/**
 * @author Amasty Team
 * @copyright Copyright (c) 2010-2013 Amasty (http://www.amasty.com)
 */

var amfaqAsk = new Class.create(),
    recaptchaObject;

amfaqAsk.prototype = {
    initialize: function()
    {
        recaptchaObject = this;
        $('amfaqHideDiv').observe('click', this.close);

        $$('.ask.button').each(function(element) {
            element.observe('click', this.show.bind(this));
        }.bind(this));

        $$('#amfaq-ask-form .send').each(function(element) {
            element.observe('submit', this.submit.bind(this));
        }.bind(this));

        $$('#amfaqConfirmButtons .continue, #amfaqConfirmButtons .cancel').each(function(element) {
            element.observe('click', this.close.bind(this));
        }.bind(this));

        this.validationForm = new VarienForm('amfaq-ask-form', true);
    },
    show: function()
    {
        $$('#amfaq-question, #captcha_amfaq_ask_form').each(function(element) {
            element.value = '';
        });
        this.validationForm.validator.reset();

        this.clearCountdown();
        $$('#amfaqConfirmOverlay form')[0].show();
        $$('#amfaqConfirmOverlay .message')[0].hide();

        $('amfaqConfirmOverlay').appear();
    },
    close: function()
    {
        $('amfaqConfirmOverlay').fade();
    },
    submit: function()
    {
        var form = $('amfaq-ask-form');
        if (!this.validationForm) {
            this.validationForm = new VarienForm('amfaq-ask-form', true);
        }
        if ($$('#amfaq-ask-form > input[name="amasty_invisible_token"]')) {
            if (!$$('#amfaq-ask-form > input[name="amasty_invisible_token"]')[0].getValue()) {
                return;
            }
        }

        if (this.validationForm.validator.validate())
        {
            new Ajax.Request(form.action, {
                method:'post',
                onSuccess: function(transport) {
                    var error = transport.responseText;
                    if (error != "")
                    {
                        Validation.ajaxError($('captcha_amfaq_ask_form'), error);
                        return;
                    }
                    this.startCountdown();
                    form.fade();
                    $$('#amfaqConfirmOverlay .message')[0].appear();
                    $('amfaq_ask_form').captcha.refresh();
                }.bind(this),
                onFailure: function() { alert('Something went wrong...'); },
                parameters: form.serialize(true)
            });
        }
    },

    startCountdown: function()
    {
        this.secs = 5;
        this.timer = setInterval(this.tick.bind(this), 1000);
        this.updateSecs(this.secs);

        $$('#amfaqConfirmButtons .send')[0].hide();
        $$('#amfaqConfirmButtons .cancel')[0].hide();
        $$('#amfaqConfirmButtons .continue')[0].show();
    },
    clearCountdown: function()
    {
        clearInterval(this.timer);
        $$('#amfaqConfirmButtons .send')[0].show();
        $$('#amfaqConfirmButtons .cancel')[0].show();
        $$('#amfaqConfirmButtons .continue')[0].hide();
    },
    tick: function()
    {
        this.secs--;

        this.updateSecs(this.secs);
        if (this.secs <= 0)
        {
            clearInterval(this.timer);
            this.close();
            return;
        }
    },
    updateSecs: function(val)
    {
        $$('#amfaqConfirmButtons .continue .secs')[0].update('(' + val + ')');
    }
};
