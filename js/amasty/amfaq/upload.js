document.observe("dom:loaded", function() {
    function removeFile() {

        var answer = confirm("Are you sure ?")
        if (answer) {
//            var position = this.attributes["position"].value;
//            $('file' + position).down('.delete-input').value=1;
//            $('file' + position).setStyle({display: 'none'});
            var container = this.up('.box');

            container.down('.delete-input').value=1;
            container.setStyle({display: 'none'});
        }
        else {
            return false;
        }
    }

    function addNewFile() {
        var position = $$('.file-form').length;

        var block = amfaq_new_upload_template.clone(true);
        block.id = 'file'+position;
        block = block.outerHTML.replace(/\[\d+\]/g, '['+position+']');
        $("files").insert(
            {
                bottom: new String(block)
            }
        );
        $$('.delete').each(function(element) {
            element.observe('click', removeFile);
        });
    }

    $$('.amfaq-uploads .add').each(function(element) {
        element.observe('click', addNewFile.bind(this));
    });
    $$('.amfaq-uploads .delete').each(function(element) {
        element.observe('click', removeFile);
    });

    var amfaq_new_upload_template = $$('.amfaq-uploads .new-upload')[0].clone(true);
});

