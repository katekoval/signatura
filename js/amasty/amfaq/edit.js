document.observe("dom:loaded", function() {
    if (!$('url_key').value)
    {
        $('title').observe('change', function(){
            $('url_key').value = this.value.toLowerCase().replace(/[^\w]+/g, '-');
        });
        $('url_key').observe('change', function(){
            $('title').stopObserving('change');
        });
    }
});
