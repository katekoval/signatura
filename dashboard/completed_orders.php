	<?php require_once('boot.php');

	$orders = Mage::getModel('sales/order')->getCollection();

	$orders->addFieldToFilter('status','complete')->addFieldToFilter('updated_at',array('gteq'=>date('Y-m-d 00:00:00')));
	$response = array();
	$totalSubTotal = 0;
	$orderCount = 0;
	foreach ($orders as $order) {
		$totalSubTotal += $order->getBaseSubtotal();
		$orderCount++;

	}
	$orders7days = Mage::getModel('sales/order')->getCollection();
	$orders7days->addFieldToFilter('state','complete')->addFieldToFilter('updated_at',array('gteq'=>date('Y-m-d 00:00:00', strtotime('-7 days'))));
	$orderCoun7days = 0;
	foreach ($orders7days as $order) {
		$orderCount7days++;

	}
	/** 
	*Get total order sum of today
	 **/

	$orderTotals = Mage::getModel('sales/order')->getCollection()
		->addFieldToFilter('status', array('in' => array('pending','complete')))
		->addFieldToFilter('store_id', array('neq' => array('7')))
		->addFieldToFilter('created_at', array('gt' => Mage::getModel('core/date')->date('Y-m-d 00:00:01', strtotime('today'))))
	    ->addAttributeToSelect('grand_total')
	    ->getColumnValues('grand_total');

	$totalSum = array_sum($orderTotals);
	$totalCount = count($orderTotals);



	/** 
	*Get stockless orders
	 **/
	$resource = Mage::getSingleton('core/resource');
	$readConnection = $resource->getConnection('core_read');

	/** 
	*Get rma created
	 **/

	$table1 = $resource->getTableName('ProductReturn/RmaProducts');
	$table2 = $resource->getTableName('ProductReturn/Rma');

	$sentWrongItemCollection = Mage::getModel('ProductReturn/RmaProducts')->getCollection();
	$sentWrongItemCollection->getSelect()->join(array('rma' => $table2), 'rma.rma_id = main_table.rp_rma_id', array());
	$sentWrongItemCollection->addFieldToFilter('rma_created_at', array("gteq"=>date('Y-m-d 00:00:00', strtotime('-7 days'))))
								->addFieldToFilter('rp_reason', "Sendt forkert vare")
								->getSelect()->group('rma.rma_id');
	$sentWrongItem = $sentWrongItemCollection->count();

	$missingInShipmentCollection = Mage::getModel('ProductReturn/RmaProducts')->getCollection();
	$missingInShipmentCollection->getSelect()->join(array('rma' => $table2), 'rma.rma_id = main_table.rp_rma_id', array());
	$missingInShipmentCollection->addFieldToFilter('rma_created_at', array("gteq"=>date('Y-m-d 00:00:00', strtotime('-7 days'))))
								->addFieldToFilter('rp_reason', "Mangler i forsendelsen")
								->getSelect()->group('rma.rma_id');
	$missingInShipment = $missingInShipmentCollection->count();

	$wrongQtyCollection = Mage::getModel('ProductReturn/RmaProducts')->getCollection();
	$wrongQtyCollection->getSelect()->join(array('rma' => $table2), 'rma.rma_id = main_table.rp_rma_id', array());
	$wrongQtyCollection->addFieldToFilter('rma_created_at', array("gteq"=>date('Y-m-d 00:00:00', strtotime('-7 days'))))
								->addFieldToFilter('rp_reason', "Sendt forkert antal")
								->getSelect()->group('rma.rma_id');
	$wrongQty = $wrongQtyCollection->count();


$response['wrong_qty'] = $wrongQty;
$response['missing_in_shipment'] = $missingInShipment;
$response['wrong_goods'] = $sentWrongItem;
$response['totals_subtotal'] = $totalSubTotal;
$response['completed_orders'] = $orderCount;
$response['completed_orders_7_days'] = $orderCount7days;
$response['total_sum'] = $totalSum;
$response['total_count'] = $totalCount;


header('Content-Type: application/json');
echo json_encode($response);
