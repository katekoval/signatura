	<?php require_once('boot.php');

	$orders = Mage::getModel('sales/order')->getCollection();

	$orders->addFieldToFilter('state','complete')->addFieldToFilter('updated_at',array('gteq'=>date('Y-m-d 00:00:00', strtotime('-90 days'))));
	$response = array();
	$totalSubTotal = 0;
	$orderCount = 0;
	foreach ($orders as $order) {
		$orderCount++;

	}


	/** 
	*Get stockless orders
	 **/
	$resource = Mage::getSingleton('core/resource');
	$readConnection = $resource->getConnection('core_read');

	$countPreparePending = Mage::getModel('Orderpreparation/ordertopreparepending')
	->getCollection()
	->addFieldToFilter('opp_type', 'stockless')
	->count();

	/** 
	*Get fullstock orders orders
	 **/

	$countFullstock = Mage::getModel('Orderpreparation/ordertopreparepending')
	->getCollection()
	->addFieldToFilter('opp_type', 'fullstock')
	->count();

	/** 
	*Get selected orders orders
	 **/


	$countSelectedCollection = Mage::getModel('Orderpreparation/ordertoprepare')
		->getCollection(); // SELECT main_table.*, sales_flat_order.status  FROM order_to_prepare AS main_table 

	$countSelectedCollection->getSelect()->join('sales_flat_order', 'sales_flat_order.entity_id = main_table.order_id', ['status']); // "LEFT JOIN sales_flat_order ON main table som er den main i collection...". Sidste array = felter vi vil have med (kig i linje 48)

	$countSelectedCollection->addFieldToFilter('status', ['neq'=>'complete']); // != | <> where status!="complete"

	$countSelected = $countSelectedCollection->count(); //count()




	/** 
	*Get rma created
	 **/

	$table1 = $resource->getTableName('ProductReturn/RmaProducts');
	$table2 = $resource->getTableName('ProductReturn/Rma');

	$sentWrongItemCollection = Mage::getModel('ProductReturn/RmaProducts')->getCollection();
	$sentWrongItemCollection->getSelect()->join(array('rma' => $table2), 'rma.rma_id = main_table.rp_rma_id', array());
	$sentWrongItemCollection->addFieldToFilter('rma_created_at', array("gteq"=>date('Y-m-d 00:00:00', strtotime('-90 days'))))
								->addFieldToFilter('rp_reason', "Sendt forkert vare")
								->getSelect()->group('rma.rma_id');
	$sentWrongItem = $sentWrongItemCollection->count();

	$missingInShipmentCollection = Mage::getModel('ProductReturn/RmaProducts')->getCollection();
	$missingInShipmentCollection->getSelect()->join(array('rma' => $table2), 'rma.rma_id = main_table.rp_rma_id', array());
	$missingInShipmentCollection->addFieldToFilter('rma_created_at', array("gteq"=>date('Y-m-d 00:00:00', strtotime('-90 days'))))
								->addFieldToFilter('rp_reason', "Mangler i forsendelsen")
								->getSelect()->group('rma.rma_id');
	$missingInShipment = $missingInShipmentCollection->count();

	$wrongQtyCollection = Mage::getModel('ProductReturn/RmaProducts')->getCollection();
	$wrongQtyCollection->getSelect()->join(array('rma' => $table2), 'rma.rma_id = main_table.rp_rma_id', array());
	$wrongQtyCollection->addFieldToFilter('rma_created_at', array("gteq"=>date('Y-m-d 00:00:00', strtotime('-90 days'))))
	->addFieldToFilter('rp_reason', "Sendt forkert antal")
	->getSelect()->group('rma.rma_id');
	$wrongQty = $wrongQtyCollection->count();


	$sentWrongColorCollection = Mage::getModel('ProductReturn/RmaProducts')->getCollection();
	$sentWrongColorCollection->getSelect()->join(array('rma' => $table2), 'rma.rma_id = main_table.rp_rma_id', array());
	$sentWrongColorCollection->addFieldToFilter('rma_created_at', array("gteq"=>date('Y-m-d 00:00:00', strtotime('-90 days'))))
								->addFieldToFilter('rp_reason', "Sendt forkert farve")
								->getSelect()->group('rma.rma_id');
	$sentWrongColor = $sentWrongColorCollection->count();


	$brokenShippingCollection = Mage::getModel('ProductReturn/RmaProducts')->getCollection();
	$brokenShippingCollection->getSelect()->join(array('rma' => $table2), 'rma.rma_id = main_table.rp_rma_id', array());
	$brokenShippingCollection->addFieldToFilter('rma_created_at', array("gteq"=>date('Y-m-d 00:00:00', strtotime('-90 days'))))
	->addFieldToFilter('rp_reason', "Gået itu under fragt")
	->getSelect()->group('rma.rma_id');
	$brokenShipping = $brokenShippingCollection->count();

	$purchaseOrder = $resource->getTableName('Purchase/Order');
	$incomingProductsCollection = Mage::getModel('Purchase/OrderProduct')->getCollection();
	$incomingProductsCollection->getSelect()->join(array('po' => $purchaseOrder), 'po.po_num = main_table.pop_order_num', array());
	$incomingProductsCollection->addFieldToFilter('po_supply_date', array("eq"=>date('Y-m-d')));
	$incomingProductsCollection->addFieldToFilter('po_status', ['eq'=>'waiting_for_delivery']);
	$incomingProductsCollection->addFieldToFilter('pop_supplied_qty', ['eq'=>'0']);


	$incomingProducts = $incomingProductsCollection->count();




$response['broken_shipping'] = $brokenShipping;
$response['wrong_qty'] = $wrongQty;
$response['missing_in_shipment'] = $missingInShipment;
$response['sent_wrong_color'] = $sentWrongColor;
$response['wrong_goods'] = $sentWrongItem;
$response['completed_orders'] = $orderCount;

header('Content-Type: application/json');
echo json_encode($response);
