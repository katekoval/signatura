<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magento.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magento.com for more information.
 *
 * @category    Mage
 * @package     Mage_Admin
 * @copyright  Copyright (c) 2006-2017 X.commerce, Inc. and affiliates (http://www.magento.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Class Mage_Admin_Model_Variable
 */
class Mage_Admin_Model_Variable extends Mage_Core_Model_Abstract
{
    protected $secureValues
        = [
            'backend_model' => ['adminhtml/system_config_backend_encrypted'],
            'frontend_type' => ['obscure', 'hidden', 'password'],
        ];

    /**
     * Initialize variable model
     */
    protected function _construct()
    {
        $this->_init('admin/variable');
    }

    /**
     * @return array|bool
     * @throws Exception
     * @throws Zend_Validate_Exception
     */
    public function validate()
    {
        $errors = array();

        if (!Zend_Validate::is($this->getVariableName(), 'NotEmpty')) {
            $errors[] = Mage::helper('adminhtml')->__('Variable Name is required field.');
        }
        if (!Zend_Validate::is($this->getVariableName(), 'Regex', array('/^[-_a-zA-Z0-9\/]*$/'))) {
            $errors[] = Mage::helper('adminhtml')->__('Variable Name is incorrect.');
        }

        if (!in_array($this->getIsAllowed(), array('0', '1'))) {
            $errors[] = Mage::helper('adminhtml')->__('Is Allowed is required field.');
        }

        if ($this->isSecureValue($this->getVariableName())) {
            $errors[] = Mage::helper('adminhtml')->__('The variable can not be used, because it is a secure value.');
        }

        if (empty($errors)) {
            return true;
        }
        return $errors;
    }

    private function isSecureValue($value)
    {
        $node = Mage::getConfig()->getNode('default/' . $value);

        foreach ($this->secureValues as $attribute => $secureValueTypes) {
            $nodeValue = $node->getAttribute('backend_model');
            if (in_array($nodeValue, $secureValueTypes, true)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Check is config directive with given path can be parsed via configDirective method
     *
     * @param string $path
     * @return bool
     */
    public function isPathAllowed($path)
    {
        return Mage::helper('admin/variable')->isPathAllowed($path);
    }
}
