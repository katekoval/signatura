<?php

$installer = $this;

$installer->startSetup();

$installer->run("ALTER TABLE {$this->getTable('catalog_product_entity_tier_price')}  ADD tier_type INT NOT NULL AFTER qty ; ");

$installer->getConnection()->resetDdlCache('catalog_product_entity_tier_price');

$installer->endSetup();


