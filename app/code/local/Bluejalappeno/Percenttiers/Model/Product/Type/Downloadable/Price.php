<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Downloadable
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
  /**
   *
   * @category   Bluejalappeno
   * @package    Bluejalappeno_Percenttiers
   * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
   * @license    http://www.bluejalappeno.com/license.txt - Commercial license
*/

class Bluejalappeno_Percenttiers_Model_Product_Type_Downloadable_Price extends Mage_Downloadable_Model_Product_Price {


	protected $_debug;
	protected $_safetyValve = 0;

public function getFinalPrice($qty=null, $product)
    {

    	if (!Mage::helper('bjalcommon')->isModuleEnabled('Bluejalappeno_Tieredproducts', 'catalog/tieredproducts/active')) {
    		return parent::getFinalPrice($qty, $product);
    	}
    	$this->_debug = Mage::getStoreConfig('catalog/tieredproducts/debug');
    	if ($qty == '' && Mage::getStoreConfig('catalog/tieredproducts/update_price'))
    	{
    		$qty = $this->_getItemQuantity($product->getEntityId());
    	}

        if (is_null($qty) && !is_null($product->getCalculatedFinalPrice())) {
        	return $product->getCalculatedFinalPrice();
        }

        if (Mage::helper('bjalcommon')->getNewVersion() >= 12) {
	        $finalPrice = $this->getBasePrice($product, $qty);
	        $product->setFinalPrice($finalPrice);
	        Mage::dispatchEvent('catalog_product_get_final_price', array('product' => $product, 'qty' => $qty));

	        $finalPrice = $product->getData('final_price');
	        $finalPrice = $this->_applyOptionsPrice($product, $qty, $finalPrice);
	        $finalPrice = max(0, $finalPrice);
        }
        else {
	        $finalPrice = $product->getPrice();
	        $finalPrice = $this->_applyTierPrice($product, $qty, $finalPrice);

	     // Call our custom function to apply category Tier Pricing, if applicable.
	        if (!is_null($qty) || Mage::getStoreConfig('catalog/tieredproducts/update_price')) {
	        	$finalPrice = $this->_applyWebshopAppsTierPrice($qty, $finalPrice,$product->getEntityId());
	        }
	        $finalPrice = $this->_applySpecialPrice($product, $finalPrice);
	       if ($this->_debug) Mage::helper('wsalogger/log')->postDebug('Tieredproducts', 'Downloadable Product Final Price' , $product->getSku() .' qty: ' .$qty .' Final price is '.$finalPrice);
	        $product->setFinalPrice($finalPrice);

	        Mage::dispatchEvent('catalog_product_get_final_price', array('product'=>$product));

	        $finalPrice = $product->getData('final_price');
	        $finalPrice = $this->_applyOptionsPrice($product, $qty, $finalPrice);
        }


     	/**
         * links prices are added to base product price only if they can be purchased separately
         */
        if ($product->getLinksPurchasedSeparately()) {
            if ($linksIds = $product->getCustomOption('downloadable_link_ids')) {
                $linkPrice = 0;
                $links = $product->getTypeInstance(true)
                    ->getLinks($product);
                foreach (explode(',', $linksIds->getValue()) as $linkId) {
                    if (isset($links[$linkId])) {
                        $linkPrice += $links[$linkId]->getPrice();
                    }
                }
                $finalPrice += $linkPrice;
            }
        }
        $product->setFinalPrice($finalPrice);
		return max(0, $finalPrice);
    }

  /**
     * Get base price with apply Group, Tier, Special prises
     *
     * @param Mage_Catalog_Model_Product $product
     * @param float|null $qty
     *
     * @return float
     */
 	public function getBasePrice($product, $qty = null)
    {
    	if (!Mage::helper('bjalcommon')->isModuleEnabled('Bluejalappeno_Tieredproducts', 'catalog/tieredproducts/active') || (is_null($qty) && !Mage::getStoreConfig('catalog/tieredproducts/update_price'))) {
    		return parent::getBasePrice($product,$qty);
    	}
        $price = (float)$product->getPrice();
        return min($this->_applyGroupPrice($product, $price),$this->_applyTierPrice($product, $qty, $price), $this->_applyWebshopAppsTierPrice($qty, $price,$product->getEntityId()),
            $this->_applySpecialPrice($product, $price)
        );
    }
    
    
    protected function _getItemQuantity($baseProductId)
    {
    	$items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
    	foreach($items as $item)
    	{
    		if ($item->getProductId()==$baseProductId)
    		{
    			return $item->getQty();
    		}
		}
    	return 0;
    }

    protected function _applyWebshopAppsTierPrice($qty, $finalPrice,$baseProductId)
    {
    	if (is_null($qty)) {
            return $finalPrice;
        }
        $this->_safetyValve++;
        if ( $this->_safetyValve > 1) return $finalPrice;
        // Get array of items in cart
        $items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
        $tieredItems=array();
		$masterTierId="";
		foreach($items as $item) {
        	$crossTierId="";
            $productId = $item->getProductId();
			/*if ($item->getProductType() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE ||
				$item->getProductType() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE  ) {
				continue;
			} */
			if ($item->getProductType() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE) {
				continue;

			}

			$productList = Mage::getModel('catalog/product')->getResourceCollection()
                ->addAttributeToSelect('cross_tier_id')
                ->addIdFilter($productId);

		  // Because we filtered by productId in the Collection above, and productId is unique, there will only be one item in the collection
            foreach($productList as $product)
            {
                continue;
            }


			if ($item->getParentItem()!=null) {
				if ($item->getParentItem()->getProductType() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE ||
					$item->getParentItem()->getProductType() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE ) {
					continue;
				}
			}
			$qty=$item->getQty();

	        $crossTierId = $product->getAttributeText('cross_tier_id');
	        if ($this->_debug) Mage::helper('wsalogger/log')->postDebug('Tieredproducts' , 'Cross Tier Id value in cart','Product: ' .$product->getSku() .', Cross tier id: '.$crossTierId);
            if(!empty($crossTierId))
            {
            	$found = false;
            	$marked = false;
            	if ($item->getProductId()==$baseProductId) {
            		$marked=true;
	            	if ($item->getProductType() != Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE &&
	        			$item->getProductType() != Mage_Catalog_Model_Product_Type::TYPE_BUNDLE) {

	            		$masterTierId=$crossTierId;

	        		}
            	}
            	// lets collate like products together
            	foreach ($tieredItems as $key=>$tieredItem) {

            		if ($tieredItem['tier_id']==$crossTierId) {
            			$found = true;
						$tieredItems[$key]['qty']+=$qty;
						$tieredItems[$key]['multiple']=true;
						if ($marked) { $tieredItems[$key]['marked']=true; }
            			break;
            		}
            	}
            	if (!$found) {
            			$tieredItem=array('tier_id'			=> $crossTierId,
            								'qty' 			=> $qty,
            								'multiple' 		=> false,
            								'marked'		=> $marked);

            			$tieredItems[]=$tieredItem;
            	}
            }
		}
 		$currentProduct = Mage::getModel('catalog/product')->load($baseProductId);
		$currentTierId = $currentProduct->getAttributeText('cross_tier_id');
		$currentProduct->setFinalPrice($finalPrice);
		foreach ($tieredItems as $tieredItem) {
        	if ($tieredItem['marked'] && $tieredItem['multiple'] && $masterTierId==$tieredItem['tier_id']) {
        	//		$currentProduct = Mage::getModel('catalog/product')->load($baseProductId);
        			$tierPrice = $currentProduct->getTierPrice($tieredItem['qty']);
        			$finalPrice = min($finalPrice, $tierPrice);

	        	break;
        	}
			else if ($currentTierId ==$tieredItem['tier_id']) {
        			$tierPrice = $currentProduct->getTierPrice($tieredItem['qty']);
        			$finalPrice = min($finalPrice, $tierPrice);
	        	break;
        	}
        }
       $this->_safetyValve =0;
        return $finalPrice;
    }

	public function getTierPrice($qty = null, $product)
    {
    	if (!Mage::helper('bjalcommon')->isModuleEnabled('Bluejalappeno_Percenttiers', 'catalog/percenttiers/active')) {
    	    return parent::getTierPrice($qty,$product);
    	}
        $allGroups = Mage_Customer_Model_Group::CUST_GROUP_ALL;
        $prices = $product->getData('tier_price');
        if (is_null($prices)) {
            $attribute = $product->getResource()->getAttribute('tier_price');
            if ($attribute) {
                $attribute->getBackend()->afterLoad($product);
                $prices = $product->getData('tier_price');
            }
        }

        if (is_null($prices) || !is_array($prices)) {
            if (!is_null($qty)) {
                return $product->getPrice();
            }
            return array(array(
                'price'         => $product->getPrice(),
                'website_price' => $product->getPrice(),
                'price_qty'     => 1,
                'cust_group'    => $allGroups,
            ));
        }

        $custGroup = $this->_getCustomerGroupId($product);
        if ($qty) {
            $prevQty = 1;
            $prevPrice = $product->getPrice();
            $prevGroup = $allGroups;

            foreach ($prices as $price) {
                if ($price['cust_group']!=$custGroup && $price['cust_group']!=$allGroups) {
                    // tier not for current customer group nor is for all groups
                    continue;
                }
                if ($qty < $price['price_qty']) {
                    // tier is higher than product qty
                    continue;
                }
                if ($price['price_qty'] < $prevQty) {
                    // higher tier qty already found
                    continue;
                }
                if ($price['price_qty'] == $prevQty && $prevGroup != $allGroups && $price['cust_group'] == $allGroups) {
                    // found tier qty is same as current tier qty but current tier group is ALL_GROUPS
                    continue;
                }
                if ($product->getData('final_price')) $priceToUse = $product->getData('final_price');
                else	$priceToUse = $product->getPrice();
                if ($price['tier_type'] == 1 && $price['website_price'] <= 100) {
	                $discount = ($price['website_price'] * $priceToUse)/100;
	                $discountedPrice = round(($priceToUse  - $discount), 2);
                	if ($discountedPrice < $prevPrice) {
	                    $prevPrice  = $discountedPrice;
	                    $prevQty    = $price['price_qty'];
	                    $prevGroup  = $price['cust_group'];
	                }
                }
                else {
                	if ($price['website_price'] < $prevPrice) {
	                    $prevPrice  = $price['website_price'];
	                    $prevQty    = $price['price_qty'];
	                    $prevGroup  = $price['cust_group'];
	                }
                }
            }
            return $prevPrice;
        } else {
            $qtyCache = array();
            foreach ($prices as $i => $price) {
            	if ($price['tier_type'] == 1 && $price['website_price'] <= 100) {
                	if ($product->getData('final_price') && Mage::getStoreConfig('catalog/percenttiers/catalog_rules')) $priceToUse = $product->getData('final_price');
                	else	$priceToUse = $product->getPrice();
	                $discount = ($price['website_price'] * $priceToUse)/100;
	                $price['website_price'] = $price['price'] = round(($priceToUse  - $discount),2);
	                $prices[$i] = $price;
                }
                if ($price['cust_group'] != $custGroup && $price['cust_group'] != $allGroups) {
                    unset($prices[$i]);
                } else if (isset($qtyCache[$price['price_qty']])) {
                    $j = $qtyCache[$price['price_qty']];
                    if ($prices[$j]['website_price'] > $price['website_price']) {
                        unset($prices[$j]);
                        $qtyCache[$price['price_qty']] = $i;
                    } else {
                        unset($prices[$i]);
                    }
                } else {
                    $qtyCache[$price['price_qty']] = $i;
                }
            }
        }

        return ($prices) ? $prices : array();
    }


 	public function getLowestTierPrice($product)
    {
    	$allGroups = Mage_Customer_Model_Group::CUST_GROUP_ALL;
    	$attribute = $product->getResource()->getAttribute('tier_price');
    	if ($attribute) {
                $attribute->getBackend()->afterLoad($product);
                $prices = $product->getData('tier_price');
        }
    	$custGroup = $this->_getCustomerGroupId($product);
    	$maxPercentageOff = 0;
    	$minTierPrice =0;
     	foreach ($prices as $price) {
                if ($price['cust_group']!=$custGroup && $price['cust_group']!=$allGroups) {
                    // tier not for current customer group nor is for all groups
                    continue;
                }
                if ($price['tier_type'] ==1) {
                	$maxPercentageOff = max($maxPercentageOff, $price['website_price']);
                }
                else if ($minTierPrice == 0) {
                	$minTierPrice =$price['website_price'];
                }
                else {
                	$minTierPrice = min($minTierPrice, $price['website_price']);
     			}
     	}

     	if ($product->getData('final_price') && Mage::getStoreConfig('catalog/percenttiers/catalog_rules')) $priceToUse = $product->getData('final_price');
        else	$priceToUse = $product->getPrice();
        $percentDiscountTier = round(($priceToUse - (($maxPercentageOff/100)*$priceToUse)), 2);
        if ($minTierPrice > 0) {
        	return min($minTierPrice, $percentDiscountTier);
        }
        return max($percentDiscountTier, 0);
    }
}

