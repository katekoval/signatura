<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 /**
  *
  * @category   Bluejalappeno
  * @package    Bluejalappeno_Percenttiers
  * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
  * @license    http://www.bluejalappeno.com/license.txt - Commercial license
*/

class Bluejalappeno_Percenttiers_Model_Product_Type_Configurable_Price extends Mage_Catalog_Model_Product_Type_Configurable_Price
{
  protected $_debug;
	protected $_safetyValve = 0;
    /**
     * Get product final price
     *
     * @param   double $qty
     * @param   Mage_Catalog_Model_Product $product
     * @return  double
     */
public function getFinalPrice($qty=null, $product)
    {
    	if (!Mage::helper('bjalcommon')->isModuleEnabled('Bluejalappeno_Tieredproducts', 'catalog/tieredproducts/active')) {
    		return parent::getFinalPrice($qty, $product);
    	}
    	$this->_debug = Mage::getStoreConfig('catalog/tieredproducts/debug');
    	if ($qty == '' && Mage::getStoreConfig('catalog/tieredproducts/update_price'))
    	{
    		$qty = $this->_getItemQuantity($product->getEntityId());
    	}

        if (is_null($qty) && !is_null($product->getCalculatedFinalPrice())) {
            return $product->getCalculatedFinalPrice();
        }

        if (Mage::helper('bjalcommon')->getNewVersion() >= 12) {
            $price = (float)$product->getPrice();
        	$configOptionFees =  $this->getTotalConfigurableItemsPrice($product, $price);
	        $basePrice = $this->getBasePrice($product, $qty);
	        $finalPrice = $basePrice;
	        $product->setFinalPrice($finalPrice);
	        Mage::dispatchEvent('catalog_product_get_final_price', array('product' => $product, 'qty' => $qty));

	        $finalPrice = $product->getData('final_price');
	        $finalPrice += $configOptionFees;
	        $finalPrice += $this->_applyOptionsPrice($product, $qty, $basePrice) - $basePrice;
	        $finalPrice = max(0, $finalPrice);

        }
        else {
	    	$product->getTypeInstance(true)
	            ->setStoreFilter($product->getStore(), $product);
	        $attributes = $product->getTypeInstance(true)
	            ->getConfigurableAttributes($product);

	        $selectedAttributes = array();
	        if ($product->getCustomOption('attributes')) {
	            $selectedAttributes = unserialize($product->getCustomOption('attributes')->getValue());
	        }
	        $finalPrice = Mage_Catalog_Model_Product_Type_Price::getFinalPrice($qty, $product);
	        if (!is_null($qty) || Mage::getStoreConfig('catalog/tieredproducts/update_price')) {
	        	$tierPrice = $this->_applyWebshopAppsTierPrice($finalPrice, $product, $product->getEntityId());
	        } else {
	        	$tierPrice = $finalPrice;
	        }
	        $tierPrice = $this->_applyOptionsPrice($product, $qty, $tierPrice);

	        if ($tierPrice>-1) {
	        	$finalPrice = min(array($tierPrice, $finalPrice));
	        }
	        $basePrice = $finalPrice;
	        foreach ($attributes as $attribute) {
	            $attributeId = $attribute->getProductAttribute()->getId();
	            $value = $this->_getValueByIndex(
	                $attribute->getPrices() ? $attribute->getPrices() : array(),
	                isset($selectedAttributes[$attributeId]) ? $selectedAttributes[$attributeId] : null
	            );
	            if($value) {
	                if($value['pricing_value'] != 0) {
	                    $finalPrice += $this->_calcSelectionPrice($value, $basePrice);
	                }
	            }
	        }
        }
         if ($this->_debug) Mage::helper('wsalogger/log')->postDebug('Tieredproducts', 'Product Final Price' , $product->getSku() .' qty: ' .$qty .' Final price is '.$finalPrice);
        $product->setFinalPrice($finalPrice);
        return max(0, $product->getData('final_price'));
    }

     /**
     * Get base price with apply Group, Tier, Special prises
     *
     * @param Mage_Catalog_Model_Product $product
     * @param float|null $qty
     *
     * @return float
     */
	public function getBasePrice($product, $qty = null)
    {
    	if (!Mage::helper('bjalcommon')->isModuleEnabled('Bluejalappeno_Tieredproducts', 'catalog/tieredproducts/active') || (is_null($qty) && !Mage::getStoreConfig('catalog/tieredproducts/update_price'))) {
    		return parent::getBasePrice($product,$qty);
    	}
        $price = (float)$product->getPrice();
        return min($this->_applyGroupPrice($product, $price),$this->_applyTierPrice($product, $qty, $price), $this->_applyWebshopAppsTierPrice($price, $product, $product->getEntityId()),
            $this->_applySpecialPrice($product, $price)
        );
    }

	protected function _getItemQuantity($baseProductId)
    {
    	$items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
    	foreach($items as $item)
    	{
    		if ($item->getProductId()==$baseProductId)
    		{
    			return $item->getQty();
    		}
		}
    	return 0;
    }

    public function _applyWebshopAppsTierPrice($finalPrice,$baseProduct, $baseProductId)
    {
        $tierPrice = -1;
         $this->_safetyValve++;
        if ( $this->_safetyValve > 1) return $finalPrice;
        $items = Mage::getSingleton('checkout/session')->getQuote()->getAllItems();
    	if (empty($items)) {
        	$items = Mage::getSingleton('adminhtml/session_quote')->getQuote()->getAllItems();
        }
        $tieredItems=array();
        $configSharedQtys = array();
        foreach($items as $item) {
        	if ( $item->getProductType() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE) {
				continue;
			}
            $productId = $item->getProductId();

			$productList = Mage::getModel('catalog/product')->getResourceCollection()
                ->addAttributeToSelect('cross_tier_id')
                ->addIdFilter($productId);

		  // Because we filtered by productId in the Collection above, and productId is unique, there will only be one item in the collection
            foreach($productList as $product)
            {
                continue;
            }
        	if ($item->getParentItem()!=null) {
				if ($item->getParentItem()->getProductType() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE ||
				$item->getParentItem()->getProductType() == Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE) {
					continue;
				}

			}
	        $crossTierId = $product->getAttributeText('cross_tier_id');
	        if ($this->_debug) Mage::helper('wsalogger/log')->postDebug('Tieredproducts' , 'Cross Tier Id value in cart','Product: ' .$product->getSku() .', Cross tier id: '.$crossTierId);
            $entityId = $item->getProductId();
	        $qty = $item->getQty();
            $configSharedQtys[$entityId][] = $qty;

        	if(!empty($crossTierId))
            {
            	$found = false;
            	$marked = false;
            	if ($item->getProductId()==$baseProductId) {
            		$marked=true;
            	}
            	// lets collate like products together
            	foreach ($tieredItems as $key=>$tieredItem) {

            		if ($tieredItem['tier_id']==$crossTierId) {
            			$found = true;
						$tieredItems[$key]['qty']+=$qty;
						$tieredItems[$key]['multiple']=true;
						if ($marked) { $tieredItems[$key]['marked']=true; }
            			break;
            		}
            	}
            	if (!$found) {
            			$tieredItem=array('tier_id'			=> $crossTierId,
            								'qty' 			=> $qty,
            								'multiple' 		=> false,
            								'marked'		=> $marked);

            			$tieredItems[]=$tieredItem;
            	}
            }
        }
           $currentProduct = Mage::getModel('catalog/product')->load($baseProductId);
		$currentTierId = $currentProduct->getAttributeText('cross_tier_id');
        foreach ($tieredItems as $tieredItem) {
        	if ($tieredItem['marked'] && $tieredItem['multiple']) {
        		$currentProduct = Mage::getModel('catalog/product')->load($baseProductId);
        		$tierPrice = $currentProduct->getTierPrice($tieredItem['qty']);
        		break;
        	}
        else if ($currentTierId ==$tieredItem['tier_id']) {
        			$tierPrice = $currentProduct->getTierPrice($tieredItem['qty']);
        			$finalPrice = min($finalPrice, $tierPrice);
	        	break;
        	}
        }

    	if(array_key_exists($baseProductId, $configSharedQtys))
        {
        	$totalQty = array_sum($configSharedQtys[$baseProductId]);
        	if ($tierPrice>-1) {
        		$tierPrice = min(array($this->getTierPrice($totalQty, $baseProduct),$tierPrice));
        	} else {
        		$tierPrice =$this->getTierPrice($totalQty, $baseProduct);
        	}
        }
        $this->_safetyValve =0;
        if ($tierPrice > -1) {
        	return $tierPrice;
        } else {
        	return $finalPrice;
        }
    }

    public function getTierPrice($qty = null, $product)
    {
    	if (!Mage::helper('bjalcommon')->isModuleEnabled('Bluejalappeno_Percenttiers', 'catalog/percenttiers/active')) {
    	    return parent::getTierPrice($qty,$product);
    	}
        $allGroups = Mage_Customer_Model_Group::CUST_GROUP_ALL;
        $prices = $product->getData('tier_price');
        if (is_null($prices)) {
            $attribute = $product->getResource()->getAttribute('tier_price');
            if ($attribute) {
                $attribute->getBackend()->afterLoad($product);
                $prices = $product->getData('tier_price');
            }
        }

        if (is_null($prices) || !is_array($prices)) {
            if (!is_null($qty)) {
                return $product->getPrice();
            }
            return array(array(
                'price'         => $product->getPrice(),
                'website_price' => $product->getPrice(),
                'price_qty'     => 1,
                'cust_group'    => $allGroups,
            ));
        }

        $custGroup = $this->_getCustomerGroupId($product);
        if ($qty) {
            $prevQty = 1;
            $prevPrice = $product->getPrice();
            $prevGroup = $allGroups;

            foreach ($prices as $price) {
                if ($price['cust_group']!=$custGroup && $price['cust_group']!=$allGroups) {
                    // tier not for current customer group nor is for all groups
                    continue;
                }
                if ($qty < $price['price_qty']) {
                    // tier is higher than product qty
                    continue;
                }
                if ($price['price_qty'] < $prevQty) {
                    // higher tier qty already found
                    continue;
                }
                if ($price['price_qty'] == $prevQty && $prevGroup != $allGroups && $price['cust_group'] == $allGroups) {
                    // found tier qty is same as current tier qty but current tier group is ALL_GROUPS
                    continue;
                }
                if ($product->getData('final_price')) $priceToUse = $product->getData('final_price');
                else	$priceToUse = $product->getPrice();
                if ($price['tier_type'] == 1 && $price['website_price'] <= 100) {
	                $discount = ($price['website_price'] * $priceToUse)/100;
	                $discountedPrice = round(($priceToUse  - $discount),2);
                	if ($discountedPrice < $prevPrice) {
	                    $prevPrice  = $discountedPrice;
	                    $prevQty    = $price['price_qty'];
	                    $prevGroup  = $price['cust_group'];
	                }
                }
                else {
                	if ($price['website_price'] < $prevPrice) {
	                    $prevPrice  = $price['website_price'];
	                    $prevQty    = $price['price_qty'];
	                    $prevGroup  = $price['cust_group'];
	                }
                }
            }
            return $prevPrice;
        } else {
            $qtyCache = array();
            foreach ($prices as $i => $price) {
            	if ($price['tier_type'] == 1 && $price['website_price'] <= 100) {
                	if ($product->getData('final_price') && Mage::getStoreConfig('catalog/percenttiers/catalog_rules')) $priceToUse = $product->getData('final_price');
                	else	$priceToUse = $product->getPrice();
	                $discount = ($price['website_price'] * $priceToUse)/100;
	                $price['website_price'] = $price['price'] = round(($priceToUse  - $discount),2);
	                $prices[$i] = $price;
                }
                if ($price['cust_group'] != $custGroup && $price['cust_group'] != $allGroups) {
                    unset($prices[$i]);
                } else if (isset($qtyCache[$price['price_qty']])) {
                    $j = $qtyCache[$price['price_qty']];
                    if ($prices[$j]['website_price'] > $price['website_price']) {
                        unset($prices[$j]);
                        $qtyCache[$price['price_qty']] = $i;
                    } else {
                        unset($prices[$i]);
                    }
                } else {
                    $qtyCache[$price['price_qty']] = $i;
                }
            }
        }
        return ($prices) ? $prices : array();
    }

 	public function getLowestTierPrice($product)
    {
    	$allGroups = Mage_Customer_Model_Group::CUST_GROUP_ALL;
    	$attribute = $product->getResource()->getAttribute('tier_price');
    	if ($attribute) {
                $attribute->getBackend()->afterLoad($product);
                $prices = $product->getData('tier_price');
        }
    	$custGroup = $this->_getCustomerGroupId($product);
    	$maxPercentageOff = 0;
    	$minTierPrice =0;
     	foreach ($prices as $price) {
                if ($price['cust_group']!=$custGroup && $price['cust_group']!=$allGroups) {
                    // tier not for current customer group nor is for all groups
                    continue;
                }
                if ($price['tier_type'] ==1) {
                	$maxPercentageOff = max($maxPercentageOff, $price['website_price']);
                }
                else if ($minTierPrice == 0) {
                	$minTierPrice =$price['website_price'];
                }
                else {
                	$minTierPrice = min($minTierPrice, $price['website_price']);
     			}
     	}

     	if ($product->getData('final_price') && Mage::getStoreConfig('catalog/percenttiers/catalog_rules')) $priceToUse = $product->getData('final_price');
        else	$priceToUse = $product->getPrice();
         $percentDiscountTier = round(($priceToUse - (($maxPercentageOff/100)*$priceToUse)),2);
        if ($minTierPrice > 0) {
        	return min($minTierPrice, $percentDiscountTier);
        }
        return max($percentDiscountTier, 0);
    }
}