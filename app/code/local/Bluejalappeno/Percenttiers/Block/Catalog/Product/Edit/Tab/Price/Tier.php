<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 /**
  *
  * @category   Bluejalappeno
  * @package    Bluejalappeno_Percenttiers
  * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
  * @license    http://www.bluejalappeno.com/license.txt - Commercial license
 */
class Bluejalappeno_Percenttiers_Block_Catalog_Product_Edit_Tab_Price_Tier
    extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tab_Price_Tier
    implements Varien_Data_Form_Element_Renderer_Interface
{
	public function __construct()
    {
    	if (!Mage::getStoreConfig('catalog/percenttiers/active')) {
    		return parent::__construct();
    	}
        $this->setTemplate('bluejalappeno/percenttiers/catalog/product/edit/price/tier.phtml');
    }

    /**
     * Check is allow change website value for combination
     *
     * @return bool
     */
    public function isShowTierType()
    {
    	if (!Mage::getStoreConfig('catalog/percenttiers/active')) {
    		return false;
    	}
        if ( $this->getProduct()->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_GROUPED || $this->getProduct()->getTypeId() == Mage_Catalog_Model_Product_Type::TYPE_BUNDLE) {
            return false;
        }
        return true;
    }


}
