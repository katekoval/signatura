<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 /**
  *
  * @category   Bluejalappeno
  * @package    Bluejalappeno_Priceupdater
  * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
  * @license    http://www.bluejalappeno.com/license.txt - Commercial license
 */

class Bluejalappeno_Priceupdater_Helper_Data extends Mage_Core_Helper_Abstract
{

	protected $_debug;

	/**
     * Are tier prices set by cost plus markup?
     *
     * @return bool
     */
    public function isCostMarkup()
    {

        if ($this->_getPriceConfigOption() == Bluejalappeno_Priceupdater_Model_Config_Source_Priceoptions::COST_MARKUP){
        	return true;
        }
        return false;
    }

	/**
     * Are tier prices set by percentage discount?
     *
     * @return bool
     */
    public function isPercentDiscount()
    {
        if ($this->_getPriceConfigOption() == Bluejalappeno_Priceupdater_Model_Config_Source_Priceoptions::PERCENT_DISCOUNT){
        	return true;
        }
        return false;
    }

    /**
     * Are tier prices set by percentage discount?
     *
     * @return bool
     */
    public function isPercentRuntime()
    {
        if ($this->_getPriceConfigOption() == Bluejalappeno_Priceupdater_Model_Config_Source_Priceoptions::PERCENT_RUNTIME && Mage::helper('bjalcommon')->isModuleEnabled('Bluejalappeno_Percenttiers', 'catalog/percenttiers/active')){
        	return true;
        }
        return false;
    }

    
    public function isTierChanges($id, $data)
    {
    		$oldTierData = Mage::getModel('priceupdater/priceupdater')->load($id)->getData();
    		$hasChanged = false;
    		foreach($data['tier_price'] as $newtier) {
    			if ($newtier['delete'] != '') {
    				Mage::log('tier deleted');
    				return true;
    			}
    		}
    		$oldTiersFormated = $this->_formatDiscountTiers($oldTierData['tiers']);
    		$newTiersFormated = $this->_formatDiscountTiers($data['tier_price']);
    		foreach($oldTiersFormated as $tierQty => $oldtier) {
    			if(!array_key_exists($tierQty, $newTiersFormated)) {
    				$hasChanged = true;
    				break;	
    			}	
    			$diff = array_intersect_assoc($newTiersFormated[$tierQty], $oldtier);
    			if (count($diff) != count($newTiersFormated[$tierQty])){
    				$hasChanged = true;
	    			break;
    			}
    		}
    		return $hasChanged;
    }
    
    protected function _formatDiscountTiers($data)
    {
    	$formatedArray = array();
    	
    	foreach ($data as $tier) 
    	{
    		$formatedArray[(int)$tier['qty']] = array (
    			'website_id'  => $tier['website_id'],
    		 	'customer_group_id' => $tier['customer_group_id'],
    			'qty'			=> (int)$tier['qty'],
    			'discount'		=>(float)$tier['discount']
    			
    		);
    		if (array_key_exists('tier_type', $tier)) {
    			$formatedArray[(int)$tier['qty']]['tier_type'] = $tier['tier_type'];
    		}
    	}
    	ksort($formatedArray, SORT_NUMERIC);
    	return $formatedArray;
    }

	/**
     * get value of Price configuration option from config
     *
     * @return bool
     */
    protected function _getPriceConfigOption()
    {
        return Mage::getStoreConfig('catalog/priceupdater/priceoption');
    }

	public function saveSimpleProductData($data, $templateId) {
		$this->_debug = Mage::getStoreConfig('catalog/priceupdater/debug');
 		$productIds = array();
 		if ($data['template_products']) {
			 parse_str($data['template_products'], $productIds);
 			if ($this->_debug) {
		 				Mage::helper('wsalogger/log')->postDebug('Bluejalappeno Price Template:','Products in this template are ',$productIds);
		 	}
		    $tiers = array();
		    if (array_key_exists('tier_price', $data)) {
			    $markup = Mage::helper('priceupdater')->isCostMarkup();
			    $deletedTiers = 0;
				foreach ($data['tier_price'] as $tierData) {
				    if (!array_key_exists('delete', $data)) {
		    			if ($markup) {
		    				if (empty($tierData['cost']) || empty($tierData['markup']) ) {
		    					continue;
		    				}
		    				$price = (float)$tierData['cost']*(1+((float)$tierData['markup']/100));
		    			}
		    			else {
		    				if (empty($tierData['price'])) {
		    					continue;
		    				}

		    				$price = $tierData['price'];
		    			}
		    		
		    			$tierPrice = array(
		    				'website_id' => $tierData['website_id'],	
		    				'cust_group' => $tierData['customer_group_id'],
		    				'price_qty'	=> $tierData['qty'],
		    				'tier_type'	=> $tierData['tier_type'],
		    				'price'	=> $price,
		    				'delete'	=> ''
		    			);
		    			$tiers[] = $tierPrice;
		    		}
					else if (array_key_exists('delete', $data)) {
						$deletedTiers++;
					}
		    	}
		    	if ($deletedTiers == count($data['tier_price']) && count($tiers) == 0) {
					foreach($productIds as $prodId) {
						$this->removeTiers($prodId);
					}
				}

		    }
			$this->_saveProductData ($data, $productIds, $templateId, $tiers);
 		}
 		$this->_reconcileTemplate($templateId, $productIds);

    }

	public function saveDiscountProductData($data, $templateId) {
		$this->_debug = Mage::getStoreConfig('catalog/priceupdater/debug');
 		$productIds = array();
 		if ($data['template_products']) {
			 parse_str($data['template_products'], $productIds);
 			if ($this->_debug) {
		 				Mage::helper('wsalogger/log')->postDebug('Bluejalappeno Price Template:','Products in this template are ',$productIds);
		 	}

			//with each product id  get the product and look at the price if the price in template is 0
			//use this price to set the price of the tier
		 	//otherwise use the price from the template
			foreach ($productIds as $oneProductId => $nothing) {
				$tiers = array();

				if (array_key_exists('tier_price', $data)) {
					$listPrice = 0;
					if (!empty($data['price']) && $data['price'] > 0) {
						$listPrice = $data['price'] ;
					}
					else {
						$product = Mage::getModel('catalog/product')->load($oneProductId);
						$listPrice = $product->getPrice();
					}
						//create each tier using this price
					$deletedTiers = 0;
					foreach ($data['tier_price'] as $tierData) {
						if (!array_key_exists('delete', $data) && !empty($tierData['discount'])) {
							$price = $listPrice * ((100-$tierData['discount'])/100);
							$tierPrice = array(
			    				'website_id' => $tierData['website_id'],	
			    				'cust_group' => $tierData['customer_group_id'],
			    				'price_qty'	=> $tierData['qty'],
			    				'tier_type'	=> $tierData['tier_type'],
			    				'price'	=> $price,
			    				'delete'	=> ''
			    			);
			    			$tiers[] = $tierPrice;
						}
						else if (array_key_exists('delete', $data)) {
							$deletedTiers++;
						}
					}
					if ($deletedTiers == count($data['tier_price']) && count($tiers) == 0) {
						$this->removeTiers($oneProductId);
					}
				}
				$oneProductIdArray = array($oneProductId=>1);
				$this->_saveProductData ($data, $oneProductIdArray, $templateId, $tiers);
			}

 		}

 		$this->_reconcileTemplate($templateId, $productIds);

    }
    
	public function saveCostPlusProductData($data, $templateId) {
		$this->_debug = Mage::getStoreConfig('catalog/priceupdater/debug');
 		$productIds = array();
 		if ($data['template_products']) {
			 parse_str($data['template_products'], $productIds);
 			if ($this->_debug) {
		 				Mage::helper('wsalogger/log')->postDebug('Bluejalappeno Price Template:','Cost plus markup algorithm. Products in template are',$productIds);
		 	}
			//use cost from product otherwise use default cost
			foreach ($productIds as $oneProductId => $nothing) {

                $tiers = array();
				if (array_key_exists('tier_price', $data)) {
					$listPrice = 0;
					$product = Mage::getModel('catalog/product')->load($oneProductId);
					
					
						//create each tier using this price
					$deletedTiers = 0;
					foreach ($data['tier_price'] as $tierData) {
						if (!array_key_exists('delete', $data) && !empty($tierData['markup'])) {
							
							$prodCost = $product->getCost();
							if ($prodCost == 0 || $prodCost == '') {
								$prodCost = $tierData['cost'] ;
							}
							$price = $prodCost * (1+((float)$tierData['markup']/100));
							$tierPrice = array(
			    				'website_id' => $tierData['website_id'],	
			    				'cust_group' => $tierData['customer_group_id'],
			    				'price_qty'	=> $tierData['qty'],
			    				'tier_type'	=> $tierData['tier_type'],
			    				'price'	=> $price,
			    				'delete'	=> ''
			    			);
			    			$tiers[] = $tierPrice;
						}
						else  if (array_key_exists('delete', $data)) {
							$deletedTiers++;
						}
					}
					if ($deletedTiers == count($data['tier_price']) && count($tiers) == 0) {
						$this->removeTiers($oneProductId);
					}
				}
				$oneProductIdArray = array($oneProductId=>1);
				$this->_saveProductData($data, $oneProductIdArray, $templateId, $tiers);
			}

 		}

 		$this->_reconcileTemplate($templateId, $productIds);

    }

  	public function removeTemplateFromProducts($productIds) {
    	if ($this->_debug) {
		 		Mage::helper('wsalogger/log')->postDebug('Bluejalappeno Price Template:','Removing template id from these products ',$productIds);
		 }
 		$apiModel = Mage::getSingleton('catalog/product_api_v2');
 		$obj = new stdClass();
		$obj->price_template_id = '';
		$isUpdated = false;
 		foreach ($productIds as $product_id=>$nothing) {
 			$isUpdated = $apiModel->update($product_id, $obj, 0, 'id');
 		}
 		return $isUpdated;
    }
    
    public function isTieredPricing() {
    	return Mage::helper('bjalcommon')->isModuleEnabled('Bluejalappeno_Tieredproducts', 'catalog/tieredproducts/active');
    }

    protected function _reconcileTemplate($templateId, $productIds) {

   		 //data reconcilliation
 		$checkProductIds = Mage::getModel('priceupdater/priceupdater')->getProductsInTemplate($templateId);
 		$removeThese = array_diff_key($checkProductIds, $productIds);
 		if (!empty($removeThese) && count($removeThese) > 0) {
 			$this->removeTemplateFromProducts($removeThese);
 		}
    }

    protected function _saveProductData ($data, $productIds, $templateId, $tiers = array()) {

    	$apiModel = Mage::getSingleton('catalog/product_api_v2');
		$obj = new stdClass();
		$enabled = false;
		
		if ($data['status'] == Bluejalappeno_Priceupdater_Model_Status::STATUS_ENABLED) {
			$enabled = true;
		}
		if ($this->_debug) {
		 		Mage::helper('wsalogger/log')->postDebug('Bluejalappeno Price Template:','Saving data to products, data is ',$data);
		}
    	foreach ($productIds as $product_id=>$nothing) {
    		try {
				  $product = Mage::helper('catalog/product')->getProduct($product_id,0);
				  $product->setData('price_template_id', $templateId);
				  if($enabled) {
				  	if ($data['special_price'] != '') $product->setData('special_price', $data['special_price']);
					if ($data['price_from_date']!= '') $product->setData('special_from_date', $data['price_from_date']);
					if ($data['price_to_date'] != '') $product->setData('special_to_date', $data['price_to_date']);
					if ($data['price'] != '' && (float)$data['price'] > 0) $product->setData('price', $data['price']);
					if (array_key_exists('tax_class_id', $data) && $data['tax_class_id'] != '') $product->setData('tax_class_id', $data['tax_class_id']);
					if (array_key_exists('cross_tier_id', $data) && $data['cross_tier_id'] != '') $product->setData('cross_tier_id', $data['cross_tier_id']);
					if(count($tiers) > 0) {
						$product->setData(Mage_Catalog_Model_Product_Attribute_Tierprice_Api_V2::ATTRIBUTE_CODE, $tiers);
					}
				  }
				  $product->save();
    		}
    		catch (Exception $e) {
    			Mage::log($e->getMessage());
    			return false;
    		}
		}
		return true;
    }

/**
     * Retrieve resource instance
     *
     * @return Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Attribute_Backend_Tierprice
     */
    protected function _getTierResource()
    {
        return Mage::getResourceSingleton('catalog/product_attribute_backend_tierprice');
    }

    protected function removeTiers($productId)
    {
          $this->_getTierResource()->deletePriceData($productId);

    }
}