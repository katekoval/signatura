 <?php


 $installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */
$installer->startSetup();

$conn = $installer->getConnection();

$conn->addColumn($this->getTable('priceupdater_tiers'), 'tier_type', 'INT NOT NULL');


$installer->endSetup();
