 <?php


 $installer = $this;
/* @var $installer Mage_Catalog_Model_Resource_Eav_Mysql4_Setup */
$installer->startSetup();

$conn = $installer->getConnection();

$conn->addColumn($this->getTable('priceupdater_tiers'), 'discount', 'decimal(12,4) NULL');


$installer->endSetup();
