<?php

$installer = $this;

$installer->startSetup();
$installer->run("

DROP TABLE IF EXISTS {$this->getTable('priceupdater')};
CREATE TABLE {$this->getTable('priceupdater')} (
  `template_id` int(11) unsigned NOT NULL auto_increment,
  `title` varchar(255) NOT NULL default '',
  `description` varchar(255) NOT NULL default '',
  `status` smallint(6) NOT NULL default '0',
  `effective_date` datetime NULL,
  `end_date` datetime NULL,
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$this->getTable('priceupdater_detail')};
CREATE TABLE {$this->getTable('priceupdater_detail')} (
    `template_id` int(11)  unsigned NOT NULL default '0',
    `price` decimal(12,4)  NULL,
    `special_price` decimal(12,4)  NULL,
    `price_from_date` datetime NULL,
    `price_to_date` datetime NULL,
    `tax_class_id` int(11)  NULL,
    PRIMARY KEY  (`template_id`, `price`),
    CONSTRAINT `FK_BLUEJALAPPENO_PRICEUPDATER_DETAIL` FOREIGN KEY (`template_id`) REFERENCES {$this->getTable('priceupdater')} (`template_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS {$this->getTable('priceupdater_tiers')};
CREATE TABLE {$this->getTable('priceupdater_tiers')} (
	`tier_id` int(11)  unsigned NOT NULL auto_increment,
    `template_id` int(11)  unsigned NOT NULL default '0',
    `website_id` int(11) unsigned NOT NULL default '0',
    `all_groups` tinyint(1) unsigned NOT NULL default '1',
    `customer_group_id` int(11)  NULL,
    `qty` decimal(12,4)  NULL,
    `discount` decimal (12,4) NULL,
    `cost` decimal(12,4)  NULL,
    `markup` decimal(12,4)  NULL,
    `price` decimal(12,4) NULL,
    PRIMARY KEY  (`tier_id`,`template_id`),
    CONSTRAINT `FK_PRICEUPDATER_TIERS_TEMPLATE` FOREIGN KEY (`template_id`) REFERENCES {$this->getTable('priceupdater')} (`template_id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

select @entity_type_id:=entity_type_id from {$this->getTable('eav_entity_type')} where entity_type_code='catalog_product';

insert ignore into {$this->getTable('eav_attribute')}
    set entity_type_id 	= @entity_type_id,
    	attribute_code 	= 'price_template_id',
    	backend_type	= 'int',
    	frontend_input	= 'text',
    	is_required	= 0,
    	source_model = 'priceupdater/priceupdater',
    	frontend_label	= 'Price Template';

select @attribute_id:=attribute_id from {$this->getTable('eav_attribute')} where attribute_code='price_template_id';

insert ignore into {$this->getTable('catalog_eav_attribute')}
    set attribute_id 	= @attribute_id,
    	is_visible 	= 1,
    	used_in_product_listing	= 1,
    	is_filterable_in_search	= 1;


");

$installer->endSetup();


