<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 /**
 *
 * @category   Bluejalappeno
 * @package    Bluejalappeno_Priceupdater
 * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
 * @license    http://www.bluejalappeno.com/license.txt - Commercial license
 */
class Bluejalappeno_Priceupdater_Adminhtml_PriceupdaterController extends Mage_Adminhtml_Controller_Action
{

	protected $_debug;

	protected function _initAction() {

		$this->loadLayout()
			->_setActiveMenu('priceupdater/items')
			->_addBreadcrumb(Mage::helper('adminhtml')->__('Price Updater Manager'), Mage::helper('adminhtml')->__('Price Updater Manager'));

		return $this;
	}

	protected function _initTemplate($getRootInstead = false) {

        $templateId = (int) $this->getRequest()->getParam('id');
        $storeId    = (int) $this->getRequest()->getParam('store');
        $template = Mage::getModel('priceupdater/priceupdater');
     //   $template->setStoreId($storeId);

        if ($templateId) {
            $template->load($templateId);
      /*      if ($storeId) {
                $rootId = Mage::app()->getStore($storeId)->getRootCategoryId();
                if (!in_array($rootId, $category->getPathIds())) {
                    // load root category instead wrong one
                    if ($getRootInstead) {
                        $category->load($rootId);
                    }
                    else {
                        $this->_redirect('*///*/', array('_current'=>true, 'id'=>null));
                        /*return false;
                    }
                }
            }*/
        }

        if ($activeTabId = (string) $this->getRequest()->getParam('active_tab_id')) {
            Mage::getSingleton('admin/session')->setActiveTabId($activeTabId);
        }
        Mage::register('priceupdater_data', $template);
        Mage::register('current_template', $template);
     //   Mage::getSingleton('cms/wysiwyg_config')->setStoreId($this->getRequest()->getParam('store'));
        return $template;
	}

	public function indexAction() {

			$this->_initAction();
		    $this->renderLayout();
	}

	public function editAction() {

		
		$id     = $this->getRequest()->getParam('id');
		$model  = Mage::getModel('priceupdater/priceupdater')->load($id);
		
		if (!Mage::helper('bjalcommon')->checkItems('Y2F0YWxvZy9wcmljZXVwZGF0ZXIvc2hpcF9vbmNl',
        	'ZGhhYmlzb3Vr','Y2F0YWxvZy9wcmljZXVwZGF0ZXIvc2VyaWFs')) { $this->_redirect('*/*/');}
		if ($model->getId() || $id == 0) {
			$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
			if (!empty($data)) {
				$model->setData($data);
			}
			Mage::register('priceupdater_data', $model);

			$this->loadLayout();
			$this->_setActiveMenu('priceupdater/items');

			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Price Tier Manager'), Mage::helper('adminhtml')->__('Priceupdater Manager'));
			$this->_addBreadcrumb(Mage::helper('adminhtml')->__('Price Tier Group News'), Mage::helper('adminhtml')->__('Priceupdater News'));

			$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);

			$this->_addContent($this->getLayout()->createBlock('priceupdater/adminhtml_priceupdater_edit'))
			->_addLeft($this->getLayout()->createBlock('priceupdater/adminhtml_priceupdater_edit_tabs'));

			$this->renderLayout();
		} else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('priceupdater')->__('Price template does not exist'));
			$this->_redirect('*/*/');
		}
	}

	public function newAction() {
		if (Mage::getStoreConfig('catalog/priceupdater/active')) {
			$this->_forward('edit');
		}
		else {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('priceupdater')->__('Price Updater extension not enabled in configuration'));
			$this->_redirect('*/*/');
		}
	}

	public function saveAction() {

		if ($data = $this->getRequest()->getPost()) {
			if (!Mage::helper('bjalcommon')->checkItems('Y2F0YWxvZy9wcmljZXVwZGF0ZXIvc2hpcF9vbmNl',
        	'ZGhhYmlzb3Vr','Y2F0YWxvZy9wcmljZXVwZGF0ZXIvc2VyaWFs')) { return ;}
			$data = $this->_filterPostData($data);
			$model = Mage::getModel('priceupdater/priceupdater');
			$model->setData($data)
				->setId($this->getRequest()->getParam('id'));
			$timeLimit = ini_get('max_execution_time');
			set_time_limit(0);
			$this->_debug = Mage::getStoreConfig('catalog/priceupdater/debug');
			try {
				if (array_key_exists('tier_price', $data)) {
					$model->validate($data['tier_price']);
				}
				$model->save();
				$templateId = $model->getId();
				if ($percentDiscount = Mage::helper('priceupdater')->isPercentDiscount()) {
					Mage::helper('priceupdater')->saveDiscountProductData($data, $templateId);
				}
				
				elseif ($costmarkup = Mage::helper('priceupdater')->isCostMarkup()) {
					Mage::helper('priceupdater')->saveCostPlusProductData($data, $templateId);
				}
				else {
					Mage::helper('priceupdater')->saveSimpleProductData($data, $templateId);
				}

				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('priceupdater')->__('Template was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);

				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $model->getId()));
					set_time_limit($timeLimit);
					return;
				}
				$this->_redirect('*/*/');
				set_time_limit($timeLimit);
				return;
			} catch (Exception $e) {
					if ($this->_debug) {
 						Mage::helper('wsalogger/log')->postCritical('Bluejalappeno Price Template:','Problem saving template: ',$e->getMessage());
 					}
	                Mage::getSingleton('adminhtml/session')->addError('Problem saving the template: ' .$e->getMessage());
	                Mage::getSingleton('adminhtml/session')->setPriceupdaterData($data);
	                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
	                set_time_limit($timeLimit);
	                return;
			}

        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('priceupdater')->__('Unable to find template to save'));
        set_time_limit($timeLimit);
        $this->_redirect('*/*/');
	}
	}

	public function deleteAction() {
		$this->_debug = Mage::getStoreConfig('catalog/priceupdater/debug');
		if( $this->getRequest()->getParam('id') > 0 ) {
			try {
				$model = Mage::getModel('priceupdater/priceupdater');
				$model->setId($this->getRequest()->getParam('id'));
				 Mage::helper('priceupdater')->removeTemplateFromProducts($model->getProductsInTemplate($this->getRequest()->getParam('id')));
				$model->delete();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Template was successfully deleted'));
				$this->_redirect('*/*/');
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				if ($this->_debug) {
 					Mage::helper('wsalogger/log')->postCritical('Bluejalappeno Price Template:','Problem deleting template: ',$e->getMessage());
 				}
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
		}
		$this->_redirect('*/*/');
	}

    public function massDeleteAction() {
    	$this->_debug = Mage::getStoreConfig('catalog/priceupdater/debug');
        $priceupdaterIds = $this->getRequest()->getParam('priceupdater');
        if(!is_array($priceupdaterIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select Template(s)'));
        } else {
            try {
                foreach ($priceupdaterIds as $priceupdaterId) {
                    $priceupdater = Mage::getModel('priceupdater/priceupdater')->load($priceupdaterId);
                     Mage::helper('priceupdater')->removeTemplateFromProducts($priceupdater->getProductsInTemplate($priceupdaterId));
                    $priceupdater->delete();
                }
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__(
                        'Total of %d record(s) were successfully deleted', count($priceupdaterIds)
                    )
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            	if ($this->_debug) {
 					Mage::helper('wsalogger/log')->postCritical('Bluejalappeno Price Template:','Problem mass deleting templates: ',$e->getMessage());
 				}
            }
        }
        $this->_redirect('*/*/index');
    }
    
    public function crontestAction()
    {
    	$observer = Mage::getModel('priceupdater/observer');
    	$observer->saveTemplates();
    	 $this->_redirect('*/*/index');
    }

    public function massStatusAction()
    {
        $priceupdaterIds = $this->getRequest()->getParam('priceupdater');
        if(!is_array($priceupdaterIds)) {
            Mage::getSingleton('adminhtml/session')->addError($this->__('Please select Template(s)'));
        } else {
            try {
                foreach ($priceupdaterIds as $priceupdaterId) {
                    $priceupdater = Mage::getSingleton('priceupdater/priceupdater')
                        ->load($priceupdaterId)
                        ->setStatus($this->getRequest()->getParam('status'))
                        ->setIsMassupdate(true)
                        ->save();
                }
                $this->_getSession()->addSuccess(
                    $this->__('Total of %d record(s) were successfully updated', count($priceupdaterIds))
                );
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            	if ($this->_debug) {
 					Mage::helper('wsalogger/log')->postCritical('Bluejalappeno Price Template:','Problem updating templates: ',$e->getMessage());
 				}
            }
        }
        $this->_redirect('*/*/index');
    }
/*
    public function exportCsvAction()
    {
        $fileName   = 'priceupdater.csv';
        $content    = $this->getLayout()->createBlock('priceupdater/adminhtml_priceupdater_grid')
            ->getCsv();

        $this->_sendUploadResponse($fileName, $content);
    }

    public function exportXmlAction()
    {
        $fileName   = 'priceupdater.xml';
        $content    = $this->getLayout()->createBlock('priceupdater/adminhtml_priceupdater_grid')
            ->getXml();

        $this->_sendUploadResponse($fileName, $content);
    }

    protected function _sendUploadResponse($fileName, $content, $contentType='application/octet-stream')
    {
        $response = $this->getResponse();
        $response->setHeader('HTTP/1.1 200 OK','');
        $response->setHeader('Pragma', 'public', true);
        $response->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true);
        $response->setHeader('Content-Disposition', 'attachment; filename='.$fileName);
        $response->setHeader('Last-Modified', date('r'));
        $response->setHeader('Accept-Ranges', 'bytes');
        $response->setHeader('Content-Length', strlen($content));
        $response->setHeader('Content-type', $contentType);
        $response->setBody($content);
        $response->sendResponse();
        die;
    }*/

 /**
     * Filtering posted data. Converting localized data if needed
     *
     * @param array
     * @return array
     */
    protected function _filterPostData($data)
    {
        $data = $this->_filterDates($data, array('price_from_date', 'price_to_date'));
        return $data;
    }


    public function gridAction() {
    	if (!$template = $this->_initTemplate(true)) {
            return;
        }
      $this->getResponse()->setBody(
            $this->getLayout()->createBlock('priceupdater/adminhtml_priceupdater_edit_tab_productsform')->toHtml()
        );
    }


}