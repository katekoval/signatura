<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 /**
  *
  * @category   Bluejalappeno
  * @package    Bluejalappeno_Priceupdater
  * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
  * @license    http://www.bluejalappeno.com/license.txt - Commercial license
*/

class Bluejalappeno_Priceupdater_Block_Adminhtml_Priceupdater_Edit_Tab_Productsform extends Mage_Adminhtml_Block_Widget_Grid
{

	public function __construct()
    {
        parent::__construct();
        $this->setId('template_product_grid');
        $this->setDefaultSort('entity_id');
        $this->setUseAjax(true);
        if (Mage::registry('priceupdater_data')->getId())	{
        	$this->setDefaultFilter(array('in_products'=>1));
        }
    }


    public function getTabClass()
    {
        return 'ajax';
    }

 protected function _addColumnFilterToCollection($column)
    {
        // Set custom filter for in product flag
        if ($column->getId() == 'in_products') {

            $productIds = $this->_getSelectedProducts();
           if (empty($productIds)) {
                $productIds = 0;
            }
            if ($column->getFilter()->getValue()) {
                $this->getCollection()->addFieldToFilter('entity_id', array('in'=>$productIds));
            }
            else {
                $this->getCollection()->addFieldToFilter('entity_id', array('nin'=>$productIds));
            }
        }
        else {
            parent::_addColumnFilterToCollection($column);
        }

        return $this;
    }

    protected function _prepareCollection()
    {

        $templateId = Mage::registry('priceupdater_data')->getId();
		$collection = Mage::getModel('catalog/product')->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('type_id', array_keys(
                Mage::getConfig()->getNode('adminhtml/priceupdater/available_product_types')->asArray()
            ));
            //          ->addAttributeToFilter('price_template_id', $somecollectionoftemplateids);

        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
       $this->addColumn('in_products', array(
            'header_css_class' => 'a-center',
            'type'      => 'checkbox',
            'name'      => 'in_products',
       		'field_name' => 'in_products',
            'values'    => $this->_getSelectedProducts(),
            'align'     => 'center',
            'index'     => 'entity_id',
            'renderer'  => 'adminhtml/catalog_product_edit_tab_super_config_grid_renderer_checkbox'
        ));

        $this->addColumn('entity_id', array(
            'header'    => Mage::helper('catalog')->__('ID'),
            'sortable'  => true,
            'width'     => '60px',
            'index'     => 'entity_id'
        ));
        $this->addColumn('name', array(
            'header'    => Mage::helper('catalog')->__('Name'),
            'index'     => 'name'
        ));
        $this->addColumn('sku', array(
            'header'    => Mage::helper('catalog')->__('SKU'),
            'width'     => '80px',
            'index'     => 'sku'
        ));


        $this->addColumn('type', array(
            'header'    => Mage::helper('catalog')->__('Type'),
            'width'     => 100,
            'index'     => 'type_id',
            'type'      => 'options',
            'options'   => Mage::getSingleton('catalog/product_type')->getOptionArray(),
        ));

       $this->addColumn('prod_price', array(
            'header'    => Mage::helper('catalog')->__('Price'),
            'width'     => 90,
            'index'     => 'price',

        ));

        $this->addColumn('template', array(
            'header'    => Mage::helper('catalog')->__('Template'),
            'width'     => '80px',
            'index'     => 'price_template_id'
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
       return $this->getUrl('*/*/grid', array('_current'=>true));

    }

    /**
     * Retrieve selected grouped products
     *
     * @return array
     */
    protected function _getSelectedProducts()
    {
        $products = $this->getProductsTemplated();
        if (!is_array($products)) {
            $products = $this->getSelectedTemplateProducts();
        }
        return $products;
    }

    /**
     * Retrieve grouped products
     *
     * @return array
     */
   public function getSelectedTemplateProducts()
    {
    	$templateId = Mage::registry('priceupdater_data')->getId();
    	$products = $this->getRequest()->getPost('selected_products');
        if (is_null($products) && isset($templateId)) {
        	$productList = Mage::getModel('catalog/product')->getResourceCollection()
               ->addAttributeToFilter('price_template_id', $templateId);
			$products = array();
	            foreach($productList as $product)
	            {
	               if ((int)$product->getData('price_template_id') == $templateId) {
	               	$products[] = $product->getId();
	               }
	            }
        }
        return $products;
    }

}