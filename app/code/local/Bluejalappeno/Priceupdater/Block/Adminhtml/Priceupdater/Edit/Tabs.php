<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 /**
  *
  * @category   Bluejalappeno
  * @package    Bluejalappeno_Priceupdater
  * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
  * @license    http://www.bluejalappeno.com/license.txt - Commercial license
*/

class Bluejalappeno_Priceupdater_Block_Adminhtml_Priceupdater_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('priceupdater_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('priceupdater')->__('Price Template Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('priceupdater')->__('Template Information'),
          'title'     => Mage::helper('priceupdater')->__('Template Information'),
          'content'   => $this->getLayout()->createBlock('priceupdater/adminhtml_priceupdater_edit_tab_form')->toHtml(),
      ));

         $this->addTab('form_options', array(
          'label'     => Mage::helper('priceupdater')->__('Price Information'),
          'title'     => Mage::helper('priceupdater')->__('Price Information'),
          'content'   => $this->getLayout()->createBlock('priceupdater/adminhtml_priceupdater_edit_tab_optionsform')->toHtml(),
      ));
       $this->addTab('form_products', array(
          'label'     => Mage::helper('priceupdater')->__('Assigned Products'),
          'title'     => Mage::helper('priceupdater')->__('Assigned Products'),
         'content'   => $this->getLayout()->createBlock('priceupdater/adminhtml_priceupdater_edit_tab_productsform', 'priceupdater.products.grid')->toHtml(),

      ));

     return parent::_beforeToHtml();
  }
}