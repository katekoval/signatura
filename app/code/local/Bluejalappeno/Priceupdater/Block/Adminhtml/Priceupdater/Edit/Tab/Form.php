<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 /**
  *
  * @category   Bluejalappeno
  * @package    Bluejalappeno_Priceupdater
  * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
  * @license    http://www.bluejalappeno.com/license.txt - Commercial license
*/

class Bluejalappeno_Priceupdater_Block_Adminhtml_Priceupdater_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('priceupdater_form', array('legend'=>Mage::helper('priceupdater')->__('Price Template Information')));

      $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('priceupdater')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title'
      ));

      $fieldset->addField('description', 'textarea', array(
          'label'     => Mage::helper('priceupdater')->__('Description'),
          'required'  => false,
          'name'      => 'description',
      ));

	 $statuses = Mage::getSingleton('priceupdater/status')->getOptionArray();

        $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('priceupdater')->__('Status'),
          'required'  => true,
          'name'      => 'status',
      	  'values'	  => $statuses,
      ));



      if ( Mage::getSingleton('adminhtml/session')->getPriceupdaterData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getPriceupdaterData());
          Mage::getSingleton('adminhtml/session')->setPriceupdaterData(null);
      } elseif ( Mage::registry('priceupdater_data') ) {
          $form->setValues(Mage::registry('priceupdater_data')->getData());

      }
      return parent::_prepareForm();
  }


}