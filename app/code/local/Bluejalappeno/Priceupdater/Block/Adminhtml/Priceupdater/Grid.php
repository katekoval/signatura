<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 /**
  *
  * @category   Bluejalappeno
  * @package    Bluejalappeno_Priceupdater
  * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
  * @license    http://www.bluejalappeno.com/license.txt - Commercial license
*/

class Bluejalappeno_Priceupdater_Block_Adminhtml_Priceupdater_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
  	if (Mage::getStoreConfig('catalog/priceupdater/active')) {
      parent::__construct();
      $this->setId('priceupdaterGrid');
      $this->setDefaultSort('priceupdater_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  	}
  }

  protected function _prepareCollection()
  {
       $colArray = array('priceupdater.template_id', 'priceupdater.price', 'priceupdater.special_price', 'priceupdater.price_from_date', 'priceupdater.price_to_date', 'priceupdater.tax_class_id');
      $collection = Mage::getResourceModel('priceupdater/priceupdater_collection');
    //  	->addLeftJoinId();
      //	->addExpressionFieldToSelect('country_list','GROUP_CONCAT(zonezip.dest_country_id SEPARATOR \',\')','zonezip.dest_country_id');


      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('template_id', array(
          'header'    => Mage::helper('priceupdater')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'template_id',
      ));

      $this->addColumn('title', array(
          'header'    => Mage::helper('priceupdater')->__('Price Template Title'),
      	  'align'     =>'left',
          'index'     => 'title',
      ));

      $this->addColumn('description', array(
          'header'    => Mage::helper('priceupdater')->__('Description'),
          'align'     =>'left',
          'index'     => 'description',
      ));

       $this->addColumn('status', array (
          'header' 	=> Mage::helper('priceupdater')->__('Active'),
       		'align'	=>'left',
            'width'     => '80px',
       		'index'	=>'status',
       		 'type'      => 'options',
          'options'   => array(
              1 => 'Yes',
              0 => 'No',
          ),
       ));

      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('template_id');
        $this->getMassactionBlock()->setFormFieldName('priceupdater');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('priceupdater')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('priceupdater')->__('Are you sure?')
        ));
        
    //   $this->getMassactionBlock()->addItem('cronttest', array(
    //         'label'    => Mage::helper('priceupdater')->__('Cron Test'),
    //         'url'      => $this->getUrl('*/*/crontest'),
    //    ));

        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}