<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 /**
  *
  * @category   Bluejalappeno
  * @package    Bluejalappeno_Priceupdater
  * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
  * @license    http://www.bluejalappeno.com/license.txt - Commercial license
*/

class Bluejalappeno_Priceupdater_Block_Adminhtml_Priceupdater_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'priceupdater';
        $this->_controller = 'adminhtml_priceupdater';

        $this->_updateButton('save', 'label', Mage::helper('priceupdater')->__('Save Template'));
        $this->_updateButton('delete', 'label', Mage::helper('priceupdater')->__('Delete Template'));

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('priceupdater_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'priceupdater_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'priceupdater_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('priceupdater_data') && Mage::registry('priceupdater_data')->getId() ) {
            return Mage::helper('priceupdater')->__("Edit Template '%s'", $this->htmlEscape(Mage::registry('priceupdater_data')->getTitle()));
        } else {
            return Mage::helper('priceupdater')->__('Add Template');
        }
    }
}