<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
/**
 *
 * @category   Bluejalappeno
 * @package    Bluejalappeno_Priceupdater
 * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
 * @license    http://www.bluejalappeno.com/license.txt - Commercial license
 */

class Bluejalappeno_Priceupdater_Block_Adminhtml_Priceupdater_Edit_Tab_Optionsform extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm()
	{
		$form = new Varien_Data_Form();
		$this->setForm($form);
		$fieldset = $form->addFieldset('tierpriceoptions_form', array('legend'=>Mage::helper('priceupdater')->__('Prices')));

		$fieldset->addField('price', 'text', array(
          'label'     => Mage::helper('priceupdater')->__('Price'),
		// 'class'     => 'required-entry',
          'required'  => false,
          'name'      => 'price',
		));

		$fieldset->addField('special_price', 'text', array(
          'label'     => Mage::helper('priceupdater')->__('Special Price'),
          'required'  => false,
          'name'      => 'special_price',
		));

		$dateFormatIso = Mage::app()->getLocale()->getDateFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
		$fieldset->addField('price_from_date', 'date', array(
            'name'   => 'price_from_date',
            'label'  => Mage::helper('catalogrule')->__('Special Price From Date'),
            'title'  => Mage::helper('catalogrule')->__('Special Price From Date'),
            'image'  => $this->getSkinUrl('images/grid-cal.gif'),
            'format'       => $dateFormatIso
		));
		$fieldset->addField('price_to_date', 'date', array(
            'name'   => 'price_to_date',
            'label'  => Mage::helper('catalogrule')->__('Special Price To Date'),
            'title'  => Mage::helper('catalogrule')->__('Special Price To Date'),
            'image'  => $this->getSkinUrl('images/grid-cal.gif'),
		//   'input_format' => Varien_Date::DATE_INTERNAL_FORMAT,
            'format'       => $dateFormatIso
		));

		$fieldset->addField('tax_class_id', 'select', array(
          'label'     => Mage::helper('priceupdater')->__('Tax Class'),
          'required'  => false,
          'name'      => 'tax_class_id',
        'values' => Mage::getSingleton('tax/class_source_product')->getAllOptions(true)


		));
		$tiers = $this->getTiers();
		$fieldset->addField('tier_price', 'text', array(
                'name'=>'tier_price',
      	'label' => Mage::helper('priceupdater')->__('Tier Price'),
                'class'=>'requried-entry',
                'values'=>$tiers
		));

		 
		if(Mage::helper('bjalcommon')->isModuleEnabled('Bluejalappeno_Tieredproducts', 'catalog/tieredproducts/active')) {
			$config    = Mage::getModel('eav/config');
			$attribute = $config->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'cross_tier_id');
			$crossTierIds    = $attribute->getSource()->getAllOptions();
			$fieldset->addField('cross_tier_id', 'select', array(
                'name'=>'cross_tier_id',
      			'label' => Mage::helper('priceupdater')->__('Cross Tier Id'),
               'required'  => false,
                'values'=>$crossTierIds
			));
		}

		$form->getElement('tier_price')->setRenderer(
		$this->getLayout()->createBlock('priceupdater/tier')
		);
		if ( Mage::getSingleton('adminhtml/session')->getPriceupdaterData() )
		{
			$form->setValues(Mage::getSingleton('adminhtml/session')->getPriceupdaterData());
			Mage::getSingleton('adminhtml/session')->setPriceupdaterData(null);
		} elseif ( Mage::registry('priceupdater_data') ) {
			$form->setValues(Mage::registry('priceupdater_data')->getData('detail'));
		}
		return parent::_prepareForm();
	}


	// get the tiers and put in a suitable array
	private function getTiers()
	{
		if ( Mage::registry('priceupdater_data') ) {
			return Mage::registry('priceupdater_data')->getData('tiers');
		}
		return array();
	}


}