<?php

/**
 *
 * @category   Bluejalappeno
 * @package    Bluejalappeno_Priceupdater
 * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
 * @license    http://www.bluejalappeno.com/license.txt - Commercial license
*/

class Bluejalappeno_Priceupdater_Model_Status extends Varien_Object
{
    const STATUS_ENABLED	= 1;
    const STATUS_DISABLED	= 0;

    static public function getOptionArray()
    {
        return array(
            self::STATUS_DISABLED   => Mage::helper('priceupdater')->__('Inactive'),
            self::STATUS_ENABLED    => Mage::helper('priceupdater')->__('Active'),
        );
    }
}