<?php

/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 /**
  *
  * @category   Bluejalappeno
  * @package    Bluejalappeno_Priceupdater
  * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
  * @license    http://www.bluejalappeno.com/license.txt - Commercial license
 */

class Bluejalappeno_Priceupdater_Model_Observer
{
	protected $_debug;
	
	public function saveTemplates()
	{
		$this->_debug = Mage::getStoreConfig('catalog/priceupdater/debug');
	  	$allActiveTemplates = Mage::getModel('priceupdater/priceupdater')->getCollection();
	  	foreach($allActiveTemplates as $aTemplate) {
	  		$aTemplateId = $aTemplate->getId();
	  		$template = Mage::getModel('priceupdater/priceupdater')->load($aTemplateId);
	  		if($template->getStatus() == 1) {
				$rawData = $template->getData();
				$data = $rawData;
				foreach ($rawData['detail'] as $key => $value) {
					$data[$key] = $value;
				}
				$data['tier_price'] = $rawData['tiers'];
				$products = $template->getProductsInTemplate($aTemplateId);
				$productString = null;
				foreach($products as $productId => $templateId) {
					if(!is_null($productString)){
						$productString.= '&';
					}
					$productString .= $productId .'=' .$templateId;
				}
				$data['template_products'] = $productString;
				if ($percentDiscount = Mage::helper('priceupdater')->isPercentDiscount()) {
					Mage::helper('priceupdater')->saveDiscountProductData($data, $aTemplateId);
				}
				elseif ($costmarkup = Mage::helper('priceupdater')->isCostMarkup()) {
					Mage::helper('priceupdater')->saveCostPlusProductData($data, $aTemplateId);
				}
				else {
					Mage::helper('priceupdater')->saveSimpleProductData($data, $aTemplateId);
				}
	  		 	if ($this->_debug) {
 					Mage::helper('wsalogger/log')->postDebug('priceupdater','Cron save and data is',$data);
 				}
	  	}
			
			
       }
	}
	
}