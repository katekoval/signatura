<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 /**
  *
  * @category   Bluejalappeno
  * @package    Bluejalappeno_Priceupdater
  * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
  * @license    http://www.bluejalappeno.com/license.txt - Commercial license
 */

class Bluejalappeno_Priceupdater_Model_Priceupdater extends Mage_Core_Model_Abstract
{

	static protected $_templates;
	protected $_rates;

    public function _construct()
    {
        parent::_construct();
        $this->_init('priceupdater/priceupdater');
        $this->setIdFieldName('template_id');
    }


     /**
     * Retrieve option array
     *
     * @return array
     */
    static public function getOptionArray()
    {
        $options = array();
        foreach(self::getTemplates() as $templateId=>$template) {
            $options[$templateId] = $template['title'];
        }
        return $options;
    }

    static public function getTemplates()
    {
        if (is_null(self::$_templates)) {
            self::$_templates = Mage::getModel('priceupdater/priceupdater')->getCollection();
        }

        return self::$_templates;
    }




/**
     * Retireve all options
     *
     * @return array
     */
    static public function getAllOptions()
    {
        $res = array();
        foreach (self::getOptionArray() as $index => $value) {
            $res[] = array(
               'value' => $index,
               'label' => $value
            );
        }
        return $res;
    }

 /**
     * Retrieve option text
     *
     * @param int $optionId
     * @return string
     */
    static public function getOptionText($optionId)
    {
        $options = self::getOptionArray();
        return isset($options[$optionId]) ? $options[$optionId] : null;
    }


     /**
     * Get Column(s) names for flat data building
     *
     * @return array
     */
    public function getFlatColums()
    {
        $columns = array();
        $columns[$this->getAttribute()->getAttributeCode()] = array(
            'type'      => 'int',
            'unsigned'  => false,
            'is_null'   => true,
            'default'   => null,
            'extra'     => null
        );
        return $columns;
   }

    /**
     * Retrieve Select for update Attribute value in flat table
     *
     * @param   int $store
     * @return  Varien_Db_Select|null
     */
    public function getFlatUpdateSelect($store)
    {
        return Mage::getResourceModel('eav/entity_attribute_option')
            ->getFlatUpdateSelect($this->getAttribute(), $store, false);
    }

    public function getProductsInTemplate($templateid = null)
    {
    	if ($templateid == null) $templateid = $this->getId();
    	$productColl = Mage::getModel('catalog/product')->getResourceCollection()
					    ->addAttributeToSelect('price_template_id')
					    ->addAttributeToFilter('price_template_id',$templateid);
		$productsInTemplate = array();
    	foreach($productColl as $product) {
       			$productsInTemplate[$product->getId()] = $product->getData('price_template_id');
       		}
	    return $productsInTemplate;


    }

/**
     * Validate tier price data
     *
     * @param Mage_Catalog_Model_Product $object
     * @throws Mage_Core_Exception
     * @return bool
     */
    public function validate($tiers)
    {
        if (empty($tiers)) {
            return true;
        }

        // validate per website
        $duplicates = array();
        foreach ($tiers as $tier) {
            if (!empty($tier['delete'])) {
                continue;
            }
            $compare = join('-', array($tier['website_id'], $tier['customer_group_id'], $tier['qty'] * 1));
            if (isset($duplicates[$compare])) {
                Mage::throwException(
                    Mage::helper('catalog')->__('Duplicate website tier price customer group and quantity.')
                );
            }
            $duplicates[$compare] = true;
        }

     /*   // if attribute scope is website and edit in store view scope
        // add global tier prices for duplicates find
        if (!$attribute->isScopeGlobal() && $object->getStoreId()) {
            $origTierPrices = $object->getOrigData($attribute->getName());
            foreach ($origTierPrices as $tier) {
                if ($tier['website_id'] == 0) {
                    $compare = join('-', array($tier['website_id'], $tier['customer_group_id'], $tier['qty'] * 1));
                    $duplicates[$compare] = true;
                }
            }
        }*/

        // validate currency
        $baseCurrency = Mage::app()->getBaseCurrencyCode();
        $rates = $this->_getWebsiteRates();
        foreach ($tiers as $tier) {
            if (!empty($tier['delete'])) {
                continue;
            }
            if ($tier['website_id'] == 0) {
                continue;
            }

            $compare = join('-', array($tier['website_id'], $tier['customer_group_id'], $tier['qty']));
            $globalCompare = join('-', array(0, $tier['customer_group_id'], $tier['qty'] * 1));
            $websiteCurrency = $rates[$tier['website_id']]['code'];

            if ($baseCurrency == $websiteCurrency && isset($duplicates[$globalCompare])) {
                Mage::throwException(
                    Mage::helper('catalog')->__('Duplicate website tier price customer group and quantity.')
                );
            }
        }

        return true;
    }

/**
     * Retrieve websites rates and base currency codes
     *
     * @return array
     */
    public function _getWebsiteRates()
    {
        if (is_null($this->_rates)) {
            $this->_rates = array();
            $baseCurrency = Mage::app()->getBaseCurrencyCode();
            foreach (Mage::app()->getWebsites() as $website) {
                /* @var $website Mage_Core_Model_Website */
                if ($website->getBaseCurrencyCode() != $baseCurrency) {
                    $rate = Mage::getModel('directory/currency')
                        ->load($baseCurrency)
                        ->getRate($website->getBaseCurrencyCode());
                    if (!$rate) {
                        $rate = 1;
                    }
                    $this->_rates[$website->getId()] = array(
                        'code' => $website->getBaseCurrencyCode(),
                        'rate' => $rate
                    );
                } else {
                    $this->_rates[$website->getId()] = array(
                        'code' => $baseCurrency,
                        'rate' => 1
                    );
                }
            }
        }
    }

}