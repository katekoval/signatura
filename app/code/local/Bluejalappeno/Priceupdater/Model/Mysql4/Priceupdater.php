<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 /**
  *
  * @category   Bluejalappeno
  * @package    Bluejalappeno_Priceupdater
  * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
  * @license    http://www.bluejalappeno.com/license.txt - Commercial license
 */

class Bluejalappeno_Priceupdater_Model_Mysql4_Priceupdater extends Mage_Core_Model_Mysql4_Abstract
{
	protected $_orig = null;
	protected $_debug;

    public function _construct()
    {
        // Note that the priceupdater_id refers to the key field in your database table.
        $this->_init('priceupdater/priceupdater', 'template_id');
    }

    protected function _beforeSave(Mage_Core_Model_Abstract $object)
    {
    	parent::_beforeSave($object);

    //	$object->setDestCountryList($object->getDestCountryId());
        return $this;
    }

   	protected function _afterSave(Mage_Core_Model_Abstract $object) {
        parent::_afterSave($object);
		$this->_debug = Mage::getStoreConfig('catalog/priceupdater/debug');
		try {
               $condition = $this->_getWriteAdapter()->quoteInto('template_id = ?', $object->getId());
	    		$this->_getWriteAdapter()->delete($this->getTable('templatedetail'), $condition);

                if ($data = $object->getData()) {
                	 if ($this->_debug) {
 						Mage::helper('wsalogger/log')->postDebug('priceupdater','Saving template, data is ',$data);
 					}
                    $detailInsert = new Varien_Object();
                    $detailInsert->setTemplateId($object->getId());
                    $detailInsert->setPrice($data['price']);
                    if ($data['special_price']) $detailInsert->setSpecialPrice($data['special_price']);
                    if ($data['price_from_date']) $detailInsert->setPriceFromDate($data['price_from_date']);
                    if ($data['price_to_date']) $detailInsert->setPriceToDate($data['price_to_date']);
                    if ($data['tax_class_id'])$detailInsert->setTaxClassId($data['tax_class_id']);
                    if(Mage::helper('bjalcommon')->isModuleEnabled('Bluejalappeno_Tieredproducts', 'catalog/tieredproducts/active')) {
                    	 if ($data['cross_tier_id'])$detailInsert->setCrossTierId($data['cross_tier_id']);
                    }

                    $this->_getWriteAdapter()->insert($this->getTable('templatedetail'), $detailInsert->getData());
                    if (array_key_exists('tier_price', $data)) {

	                     $old = array();
	        			$new = array();
	        			$markup = Mage::helper('priceupdater')->isCostMarkup();
	        			$percentDiscount = Mage::helper('priceupdater')->isPercentDiscount();
	        			// prepare original data for compare
	        			$origTierPrices = $this->retrieveTiers($object->getId());
	        			if (!is_array($origTierPrices)) {
	            			$origTierPrices = array();
	        			}
	        			foreach ($origTierPrices as $oldData) {
	            			if ($oldData['website_id'] > 0 || $oldData['website_id'] == '0') {
	                			$key = join('-', array($oldData['website_id'], $oldData['customer_group_id'], $oldData['qty'] * 1));
	                			$old[$key] = $oldData;
	            			}
	        			}
	                    // prepare data for save
	        			foreach ($data['tier_price'] as $newData) {
	        				if (($markup && (empty($newData['cost']) || empty($newData['markup']))) || ($percentDiscount && empty($newData['discount'])) || (!$percentDiscount && !$markup && empty($newData['price'])) || !isset($newData['customer_group_id']) || !empty($newData['delete'])) {
				           		continue;
	        				}
				           /* if ((empty($newData['cost'])&& empty($newData['price'])) || (empty($newData['markup']) && empty($newData['price']))|| !isset($newData['customer_group_id']) || !empty($newData['delete'])) {
				                continue;
				            }*/

				            $key = join('-', array($newData['website_id'], $newData['customer_group_id'],$newData['qty'] * 1));

				            $useForAllGroups = $newData['customer_group_id'] == Mage_Customer_Model_Group::CUST_GROUP_ALL;
				          	$customerGroupId = $newData['customer_group_id'];
				         //   if (empty($newData['price'])) {
				         	if ($markup) {
								$price = (float)$newData['cost'] * (1+ ((float)$newData['markup']/100));
								$newData['discount'] = 0;
				            }
				            elseif ($percentDiscount) {
				            	$price = '';
				            	$newData['cost'] = $newData['markup'] = 0;
				            }
				            else {
				            	$price = $newData['price'];
				            	$newData['cost'] = $newData['markup'] = $newData['discount']= 0;
				            }
				            $new[$key] = array(
				                'website_id'        => $newData['website_id'],
				                'all_groups'        => $useForAllGroups ? 1 : 0,
				                'customer_group_id' => $customerGroupId,
				                'qty'               => $newData['qty'],
				            	'discount'			=> $newData['discount'],
				                'cost'             => $newData['cost'],
				            	'markup'             => $newData['markup'],
				            	'price'             => $price,
				            //	'tier_type'			=> $newData['tier_type'],
				            );
				            if(array_key_exists('tier_type', $newData)) {
				            	$new[$key]['tier_type'] = $newData['tier_type'];
				            }
				        }
				        $delete = array_diff_key($old, $new);
        				$insert = array_diff_key($new, $old);
        				$update = array_intersect_key($new, $old);
                        $isChanged  = false;
				        $templateId  = $object->getId();
				        if (!empty($delete)) {
				            foreach ($delete as $deleteData) {
				                $this->deleteTiers($templateId, $deleteData['website_id'], $deleteData['customer_group_id'], $deleteData['qty']);
				                $isChanged = true;
				            }
				        }

				        if (!empty($insert)) {
				            foreach ($insert as $insertData) {
				                $this->saveTier($templateId, $insertData);
				                $isChanged = true;
				            }
				        }

				        if (!empty($update)) {
				            foreach ($update as $k => $v) {
				                if ($old[$k]['cost'] != $v['cost'] || $old[$k]['markup'] != $v['markup'] || $old[$k]['price'] != $v['price'] || $old[$k]['discount'] != $v['discount'] ||$old[$k]['tier_type'] != $v['tier_type']) {
				                   $this->saveTier($templateId, $v);
				                    $isChanged = true;
				                }
				            }
				        }

				      /*  if ($isChanged) {
				            $valueChangedKey = $this->getAttribute()->getName() . '_changed';
				            $object->setData($valueChangedKey, 1);
				        }*/

                    }
                    else {
                    	$this->deleteTiers($object->getId());
                    }
                }
                $this->_getWriteAdapter()->commit();
               // TO DO then need to save the template id onto actual product
            }

            catch (Exception  $e) {
                $this->_getWriteAdapter()->rollBack();
            }


   		return $this;
    }

 	protected function _afterLoad(Mage_Core_Model_Abstract $object) {
        parent::_afterLoad($object);

        $detailSelect = $this->_getReadAdapter()->select()
            ->from($this->getTable('templatedetail'))
            ->where('template_id=?', $object->getId());

        $templateDetail = $this->_getReadAdapter()->fetchAll($detailSelect);

        foreach ($templateDetail as $detail) {
        	$object->setDetail($detail);
        }
        $object->setTiers($this->retrieveTiers($object->getId()));

        return $this;
    }

    protected function retrieveTiers($templateId, $website_id = NULL, $customer_group_id = NULL, $qty = NULL) {


		$where = $this->getWhereStatement($templateId, $website_id , $customer_group_id, $qty);
    	$tiersSelect = $this->_getReadAdapter()->select()
            ->from($this->getTable('templatetiers'))
            ->where($where);
        return $this->_getReadAdapter()->fetchAll($tiersSelect);
    }

    protected function deleteTiers($templateId, $website_id = NULL, $customer_group_id = NULL, $qty = NULL) {
    	$adapter = $this->_getWriteAdapter();
    	$conds   = array(
            $adapter->quoteInto('template_id = ?', $templateId)
        );
         if (!is_null($qty)) {
            $conds[] = $adapter->quoteInto('qty=?', $qty);
        }
        if (!is_null($website_id)) {
            $conds[] = $adapter->quoteInto('website_id=?', $website_id);
        }
        if (!is_null($customer_group_id)) {
            $conds[] = $adapter->quoteInto('customer_group_id=?', $customer_group_id);
        }
        $where = join(' AND ', $conds);

		return $adapter->delete($this->getTable('templatetiers'), $where);
    }

    protected function saveTier($templateId, $data)
    {
    	$adapter = $this->_getWriteAdapter();
  		$tierInsert = new Varien_Object();
		$tierInsert->setTemplateId($templateId);
		$tierInsert->setWebsiteId($data['website_id']);
		$tierInsert->setCustomerGroupId($data['customer_group_id']);
		$tierInsert->setQty($data['qty']);
		$tierInsert->setDiscount($data['discount']);
		$tierInsert->setCost($data['cost']);
		$tierInsert->setMarkup($data['markup']);
		$tierInsert->setPrice($data['price']);
		$tierInsert->setTierType($data['tier_type']);
		$updatable = $this->retrieveTiers($templateId, $data['website_id'], $data['customer_group_id'], $data['qty']);
		$data    = $this->_prepareDataForTable($tierInsert, $this->getTable('templatetiers'));
    	if (count($updatable) > 0) {
    		$where = $this->getWhereStatement($templateId, $data['website_id'], $data['customer_group_id'], $data['qty']);
           return $adapter->update($this->getTable('templatetiers'), $data, $where);
        } else {
           return $adapter->insert($this->getTable('templatetiers'), $data);
        }
		return $this;
    }

    protected function getWhereStatement ($templateId, $website_id = NULL, $customer_group_id = NULL, $qty = NULL) {
    	$conds   = array(
            $this->_getReadAdapter()->quoteInto('template_id = ?', $templateId)
        );

        if (!is_null($qty)) {
            $conds[] = $this->_getReadAdapter()->quoteInto('qty=?', $qty);
        }
        if (!is_null($website_id)) {
            $conds[] = $this->_getReadAdapter()->quoteInto('website_id=?', $website_id);
        }
        if (!is_null($customer_group_id)) {
            $conds[] = $this->_getReadAdapter()->quoteInto('customer_group_id=?', $customer_group_id);
        }
        return join(' AND ', $conds);
    }





}