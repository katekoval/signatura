<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Core
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
 /**
  *
  * @category   Bluejalappeno
  * @package    Bluejalappeno_Priceupdater
  * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
  * @license    http://www.bluejalappeno.com/license.txt - Commercial license
 */

class Bluejalappeno_Priceupdater_Model_Config_Source_Cron extends Mage_Core_Model_Config_Data
{

    const CRON_STRING_PATH = 'crontab/jobs/priceupdater/schedule/cron_expr';

    protected function _afterSave()
    {
        $enabled = $this->getData('groups/priceupdater/fields/cron_enabled/value');
        $cronExprString = '';
        $time = $this->getData('groups/priceupdater/fields/time/value');
 
        $cronDayOfWeek = date('N');
        if ($enabled) {
        
		        $cronExprArray = array(
		       // 	intval($time[2]),                                   # Seconds
		            intval($time[1]),                                   # Minute
		            intval($time[0]),                                   # Hour
		             '*',       										# Day 
		            '*',                                                # Month of the Year
		             '*'         										# Day of the Week
		        );
		        $cronExprString = join(' ', $cronExprArray);
        	
        }
        try {
            Mage::getModel('core/config_data')
                ->load(self::CRON_STRING_PATH, 'path')
                ->setValue($cronExprString)
                ->setPath(self::CRON_STRING_PATH)
                ->save();
        } catch (Exception $e) {
            throw new Exception(Mage::helper('cron')->__('Unable to save the cron expression.'));
        }

    }

 
}
