<?php
 /**
  *
  * @category   Bluejalappeno
  * @package    Bluejalappeno_Priceupdater
  * @copyright  Copyright (c) 2012 Wimbolt Ltd (http://www.bluejalappeno.com)
  * @license    http://www.bluejalappeno.com/license.txt - Commercial license
 */

class Bluejalappeno_Priceupdater_Model_Config_Source_Priceoptions
{

	const SET_PRICE = 0;
    const COST_MARKUP    = 1;
    const PERCENT_DISCOUNT    = 2;
    const PERCENT_RUNTIME = 3;
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {

    	if (Mage::helper('bjalcommon')->isModuleEnabled('Bluejalappeno_Percenttiers')) {
    		return array (
    			array('value' => self::PERCENT_DISCOUNT, 'label' =>Mage::helper('adminhtml')->__('Percentage discount off product price')),
    			array('value' => self::PERCENT_RUNTIME, 'label'=>Mage::helper('adminhtml')->__('Percentage discount using Blue Jalappeno Percentage Off Tier Pricing')),
    			array('value' => self::COST_MARKUP, 'label'=>Mage::helper('adminhtml')->__('Cost plus percentage markup')),
    			array('value' => self::SET_PRICE, 'label'=>Mage::helper('adminhtml')->__('Set price')),
    		);
    	}
        return array(
        	array('value' => self::PERCENT_DISCOUNT, 'label'=>Mage::helper('adminhtml')->__('Percentage discount off product price')),
            array('value' => self::COST_MARKUP, 'label'=>Mage::helper('adminhtml')->__('Cost plus percentage markup')),
            array('value' => self::SET_PRICE, 'label'=>Mage::helper('adminhtml')->__('Set price')),
        );
    }

}
