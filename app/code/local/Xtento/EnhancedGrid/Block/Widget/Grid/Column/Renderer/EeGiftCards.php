<?php

/**
 * Product:       Xtento_EnhancedGrid
 * ID:            hMYZHIHXT+h7RHcfoi442EtzXc7PCSkUbKJ0hZ3VxtA=
 * Last Modified: 2018-12-13T21:16:37+01:00
 * File:          app/code/local/Xtento/EnhancedGrid/Block/Widget/Grid/Column/Renderer/EeGiftCards.php
 * Copyright:     Copyright (c) XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_EnhancedGrid_Block_Widget_Grid_Column_Renderer_EeGiftCards extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $orderId = $row->getEntityId();
        $order = Mage::getModel('sales/order')->load($orderId);
        if (!$order->getId()) {
            return "";
        }
        $giftCardString = "";
        if ($order->getData('gift_cards')) {
            #$giftCardSerialized = 'a:1:{i:0;a:5:{s:1:"i";s:1:"1";s:1:"c";s:12:"01S003ZRDKQD";s:1:"a";d:10.99;s:2:"ba";d:10.99;s:10:"authorized";d:10.99;}}';
            $giftCardSerialized = $order->getData('gift_cards');
            $giftCards = @unserialize($giftCardSerialized);
            if (!empty($giftCardSerialized) && !$giftCards) {
                $giftCards = @json_decode($giftCards, true);
            }
            if (!empty($giftCards) && is_array($giftCards)) {
                foreach ($giftCards as $giftCard) {
                    $giftCardString .= $giftCard['c'] . ", ";
                }
            }
        }
        return rtrim(trim($giftCardString), ",");
    }

    /*
     * Return dummy filter.
     */
    public function getFilter()
    {
        return false;
    }
}