<?php

/**
 * Product:       Xtento_EnhancedGrid
 * ID:            hMYZHIHXT+h7RHcfoi442EtzXc7PCSkUbKJ0hZ3VxtA=
 * Last Modified: 2017-01-25T14:54:16+01:00
 * File:          app/code/local/Xtento/EnhancedGrid/Block/Widget/Grid/Column/Filter/PaymentMethod.php
 * Copyright:     Copyright (c) XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_EnhancedGrid_Block_Widget_Grid_Column_Filter_PaymentMethod extends Mage_Adminhtml_Block_Widget_Grid_Column_Filter_Select
{
    protected function _getOptions()
    {
        $options = array(array('value' => null, 'label' => ''));

        $allActivePaymentMethods = Mage::getModel('payment/config')->getActiveMethods();
        $paymentMethods = Mage::helper('xtcore/payment')->getPaymentMethodList(true, false, false, null, false);
        foreach ($paymentMethods as $code => $title) {
            if ($this->getColumn()->getHideDisabledMethods() && !Mage::getStoreConfigFlag('payment/' . $code . '/active')) {
                continue;
            }
            if ($this->getColumn()->getHideDisabledMethods() && isset($allActivePaymentMethods[$code]) && is_object($allActivePaymentMethods[$code]) && !$allActivePaymentMethods[$code]->canUseCheckout() && !$allActivePaymentMethods[$code]->canUseInternal() && !$allActivePaymentMethods[$code]->canOrder()) {
                continue;
            }
            if (empty($title)) {
                $title = $code;
            }
            $options[] = array('value' => $code, 'label' => $title);
        }
        return $options;
    }
}
