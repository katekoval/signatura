<?php

/**
 * Product:       Xtento_EnhancedGrid
 * ID:            hMYZHIHXT+h7RHcfoi442EtzXc7PCSkUbKJ0hZ3VxtA=
 * Last Modified: 2016-10-17T16:03:14+02:00
 * File:          app/code/local/Xtento/EnhancedGrid/Block/Widget/Grid/Column/Renderer/Backordered.php
 * Copyright:     Copyright (c) XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

class Xtento_EnhancedGrid_Block_Widget_Grid_Column_Renderer_Backordered extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $orderItems = $row->getAllItems();
        $backOrderItemQty = 0;
        foreach ($orderItems as $orderItem) {
            if ($orderItem->getQtyBackordered() > 0) {
                $backOrderItemQty += $orderItem->getQtyBackordered();
            }
        }
        $columnHtml = 'No';
        if ($backOrderItemQty > 0) {
            $columnHtml = '<span style="font-size:14px; color: red; font-weight: bold;">'.Mage::helper('xtento_enhancedgrid')->__('Yes').' ('.$backOrderItemQty.')</span>';
        }
        return $columnHtml;
    }
}