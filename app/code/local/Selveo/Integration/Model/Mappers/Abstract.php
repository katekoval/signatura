<?php

abstract class Selveo_Integration_Model_Mappers_Abstract
{
    protected $keyMap = [];
    protected $appends = [];

    protected $addsToKeyMap = null;

    public function map(Varien_Object $model, array $modelData = null, $appendModelData = true) {

        $mapped = [];

        if(is_null($modelData)) {
            $modelData = $model->getData();
        }

        $this->prepareKeyMap($model);

        foreach ($modelData as $key => $value) {

            if(!$this->keymapHas($key)) {
                continue;
            }

            $mapped[$this->mapKey($key)] = $this->mapAttribute($key, $value, $modelData, $model);

        }

        if(!$appendModelData) {
            return $mapped;
        }

        foreach ($this->appends as $appendKey => $method) {

            if(method_exists($this, $method)) {
                $mapped[$appendKey] = $this->{$method}($model, $modelData);
                continue;
            }

        }

        $mapObject = new Varien_Object(['attributes' => $mapped]);
        Mage::dispatchEvent('map_'.strtolower(get_class($this)), array('mapped' => $mapObject, 'model' => $model));

        return $mapObject->getAttributes();

    }

    protected function mapAttribute($key, $value, $modelData, $model) {

        $mapMethod = 'map_'.$key;

        if(method_exists($this, $mapMethod)) {
            return $this->{$mapMethod}($value, $modelData, $model);
        }

        return $value;

    }

    /**
     * @return array
     */
    public function getKeyMap() {

        $this->prepareKeyMap();

        return $this->keyMap;
    }

    protected function mapAttrValues($mapperModel, $value, Mage_Core_Model_Abstract $model) {

        $mapper = $this->getMapper($mapperModel);

        if(!$this->canMapAttrValues($mapper)) {
            return $value;
        }

        $config = $this->getConfigMapped($mapper, $model);

        $from = $mapper->from;
        $to = $mapper->to;

        foreach ($config as $configRow) {

            if($configRow[$from] != $value) {
                continue;
            }

            return $configRow[$to];

        }

        return $value;

    }

    protected function canMapAttrValues($mapper) {
        return $mapper && $mapper->from && $mapper->to && $mapper->configPath;
    }

    protected function mapKey($key) {

        if(is_numeric($this->keyMap[$key])) {

        }

        return $this->keyMap[$key];
    }

    /**
     * @return Selveo_Integration_Helper_Config
     */
    protected function getConfigHelper() {
        return Mage::helper('selveointegration/config');
    }

    public function prepareCollection(Mage_Eav_Model_Entity_Collection_Abstract $collection)
    {
        if(method_exists($collection, 'addAttributeToSelect')){
            $collection->addAttributeToSelect($this->getKeyMap());
        }

        if(method_exists($collection, 'addFieldToSelect')){
            $collection->addFieldToSelect($this->getKeyMap());
        }

        return $collection;
    }

    /**
     * @param null|Varien_Object $model
     */
    private function prepareKeyMap($model = null)
    {
        foreach ($this->keyMap as $from => $to) {

            if(is_numeric($from)) {
                $this->keyMap[$to] = $to;
                unset($this->keyMap[$from]);
            }

        }

        if (!is_null($this->addsToKeyMap)) {
            $this->addToKeyMap($model);
        }

        $keyMapObject = new Varien_Object($this->keyMap);
        Mage::dispatchEvent('keymap_'.strtolower(get_class($this)), array('key_map' => $keyMapObject));

        $this->keyMap = $keyMapObject->getData();

    }

    /**
     * @param $key
     * @return bool
     */
    protected function keyMapHas($key) {
        return isset($this->keyMap[$key]);
    }

    /**
     * @param null|Varien_Object $model
     */
    private function addToKeyMap($model = null)
    {
        $mapper = $this->getMapper($this->addsToKeyMap);

        if (!$this->canMapAttrValues($mapper) && $mapper->direction && $mapper->currentDirection) {
            return;
        }

        $attributes = $this->getConfigMapped($mapper, $model);

        foreach ($attributes as $attribute) {

            $configDirection = $attribute[$mapper->direction];

            if (!$this->isDirectionToSync($configDirection, $mapper->currentDirection)) {
                continue;
            }

            $from = $attribute[$mapper->from];
            $to   = $attribute[$mapper->to];

            $this->keyMap[$from] = $to;

        }

    }

    /**
     * @param $mapperModel
     * @return Mage_Core_Model_Abstract
     */
    protected function getMapper($mapperModel)
    {
        return Mage::getSingleton($mapperModel);
    }

    /**
     * @param $mapper
     * @param Varien_Object|Varien_Object $model
     * @return array
     */
    protected function getConfigMapped($mapper, $model = null)
    {
        $store = method_exists($model, 'getStore') ? $model->getStore() : null;

        return $this->getConfigHelper()->getUnserialized($mapper->configPath, $store);
    }

    private function isDirectionToSync($configDirection, $mappingDirection)
    {
        return in_array($configDirection, [$mappingDirection, Selveo_Integration_Model_Config_Sync_Directions::BOTH_WAYS]);
    }
}