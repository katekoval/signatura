<?php

class Selveo_Integration_Model_Mappers_Order_Address extends Selveo_Integration_Model_Mappers_Abstract {

    protected $keyMap = array(
        'firstname',
        'middlename',
        'lastname',
        'company' => 'company_name',
        'street',
        'city',
        'postcode' => 'zip',
        'email',
        'telephone' => 'phone',
        'country_id'
    );

    public function map_email($value, $modelData, Mage_Sales_Model_Order_Address $model)
    {
        if(($email = $model->getOrder()->getCustomerEmail()) !== null) {
            return $email;
        }

        return $value;
    }

    public function map_firstname($value, $modelData, $model) {

        if(!$value) {
            return $model->getCompany();
        }

        return $value;

    }

}