<?php

class Selveo_Integration_Model_Mappers_Order_Shipment extends Selveo_Integration_Model_Mappers_Abstract
{

    protected $keyMap = array(
        'entity_id' => 'bu_shipment_id',
    );

    protected $appends = array(
        '@fromOrderItems' => 'getItems',
        'order_number'    => 'getOrderIncrementId'
    );

    public function getOrderIncrementId(Mage_Sales_Model_Order_Shipment $shipment) {
        return $shipment->getOrder()->getIncrementId();
    }

    public function getItems(Mage_Sales_Model_Order_Shipment $shipment) {

        $items = array();

        foreach ($this->getItemsHelper()->getItems($shipment) as $item) {

            $items[] = array(
                'bu_shipment_item_id' => $item->getId(),
                'bu_order_item_id'    => $item->getOrderItemId(),
                'quantity'            => (float)$item->getQty()
            );

        }

        return $items;

    }

    /**
     * @return Selveo_Integration_Helper_Items
     */
    private function getItemsHelper() {
        return Mage::helper('selveointegration/items');
    }

}