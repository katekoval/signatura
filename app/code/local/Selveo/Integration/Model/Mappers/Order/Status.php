<?php

class Selveo_Integration_Model_Mappers_Order_Status
{
    public $configPath = "selveointegration_options/order_status/map";
    public $from = "magento_order_status";
    public $to = "selveo_order_status";

    protected $mapped;

    public function isMapped($status) {
        return $this->checkMapped($status, $this->from);
    }

    /**
     * @return Selveo_Integration_Helper_Config
     */
    private function getConfigHelper() {
        return Mage::helper('selveointegration/config');
    }

    /**
     * @param $status
     * @param $column
     * @return bool
     */
    private function checkMapped($status, $column) {

        foreach ($this->getMapped() as $mappedStatus) {

            if ($mappedStatus[$column] === $status) {
                return true;
            }

        }

        return false;
    }

    /**
     * @return array
     */
    private function getMapped() {

        if(!$this->mapped) {
            $this->mapped = $this->getConfigHelper()->getUnserialized($this->configPath);
        }

        return $this->mapped;
    }

}