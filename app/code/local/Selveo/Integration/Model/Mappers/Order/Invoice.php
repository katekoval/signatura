<?php

class Selveo_Integration_Model_Mappers_Order_Invoice extends Selveo_Integration_Model_Mappers_Abstract
{
    protected $keyMap = array(
        'base_currency_code' => 'currency',
        'entity_id'          => 'bu_invoice_id',
    );

    protected $appends = array(
        'order_number'    => 'getOrderIncrementId',
        '@fromOrderItems' => 'getItems'
    );

    public function getOrderIncrementId(Mage_Sales_Model_Order_Invoice $invoice) {
        return $invoice->getOrder()->getIncrementId();
    }

    public function getItems(Mage_Sales_Model_Order_Invoice $invoice) {

        $items = array();

        foreach ($this->getItemsHelper()->getItems($invoice) as $item) {

            $items[] = array(
                'bu_invoice_item_id' => $item->getId(),
                'bu_order_item_id'   => $item->getOrderItemId(),
                'quantity'           => (float)$item->getQty(),
                'unit_price'         => (float)$item->getBasePriceInclTax(),
                'discount'           => (float)($item->getBaseDiscountAmount() ? $item->getBaseDiscountAmount() : 0),
                'tax_amount'         => (float)$item->getBaseTaxAmount(),
                'created_at'         => $invoice->getCreatedAt()
            );

        }

        return $items;

    }

    /**
     * @return Selveo_Integration_Helper_Items
     */
    private function getItemsHelper() {
        return Mage::helper('selveointegration/items');
    }

}