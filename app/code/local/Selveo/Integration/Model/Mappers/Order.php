<?php

class Selveo_Integration_Model_Mappers_Order extends Selveo_Integration_Model_Mappers_Abstract
{
    use Selveo_Integration_Helper_Array;

    protected $keyMap = [
        'created_at',
        'increment_id'        => 'order_number',
        'status'              => 'status_code',
        'state'               => 'state_code',
        'base_to_global_rate' => 'exchange_rate',
        'shipping_method',
        'shipping_description'
    ];
    protected $appends = [
        'customer_firstname'   => 'getFirstname',
        'customer_middlename'  => 'getMiddlename',
        'customer_lastname'    => 'getLastname',
        'currency'             => 'getCurrency',
        'billing_address'      => 'getBillingAddress',
        'shipping_address'     => 'getShippingAddress',
        'sender_address'       => 'getSenderAddress',
        'items'                => 'getItems',
        'totals'               => 'getTotals',
        'additional_addresses' => 'getAdditionalAddresses',
        'annotations'          => 'getAnnotations'
    ];

    protected $addressMapper;

    public function getFirstname(Mage_Sales_Model_Order $order)
    {
        return $order->getCustomerFirstname() ? $order->getCustomerFirstname() : $order->getBillingAddress()->getFirstname();
    }

    public function getMiddlename(Mage_Sales_Model_Order $order)
    {
        return $order->getCustomerMiddlename() ? $order->getCustomerMiddlename() : $order->getBillingAddress()->getMiddlename();
    }

    public function getLastname(Mage_Sales_Model_Order $order)
    {
        return $order->getCustomerLastname() ? $order->getCustomerLastname() : $order->getBillingAddress()->getLastname();
    }

    public function getCurrency(Mage_Sales_Model_Order $order)
    {
        return $order->getStoreCurrencyCode() ? $order->getStoreCurrencyCode() : Mage::app()->getStore(0)->getBaseCurrencyCode();
    }

    public function getBillingAddress(Mage_Sales_Model_Order $order)
    {
        $billingAddress = $order->getBillingAddress();

        return $this->getAddressMapper()->map($billingAddress, $billingAddress->getData());
    }

    public function getSenderAddress(Mage_Sales_Model_Order $order)
    {
        $store  = $order->getStore();
        $config = $this->getConfigHelper();

        if (!$config->isOrderSenderSyncEnabled($store)) {
            return null;
        }

        return [
            'name'    => $config->getOrderSenderName($store),
            'street'       => $config->getOrderSenderStreet($store),
            'zip'          => $config->getOrderSenderZip($store),
            'city'         => $config->getOrderSenderCity($store),
            'country_id'   => $config->getOrderSenderCountryId($store),
            'email'        => $config->getOrderSenderEmail($store),
            'phone'        => $config->getOrderSenderPhone($store),
            'identifier'   => $config->getOrderSenderIdentifier($store)
        ];

    }

    public function getAdditionalAddresses(Mage_Sales_Model_Order $order)
    {

        $additional = [];

        $collection = Mage::getResourceModel('sales/order_address_collection')->setOrderFilter($order);

        foreach ($collection as $address) {

            if (in_array($address->getAddressType(), array('billing', 'shipping'))) {
                continue;
            }

            $additional[$address->getAddressType()] = $this->getAddressMapper()->map($address, $address->getData());

        }

        return $additional;

    }

    public function getTotals(Mage_Sales_Model_Order $order) {

        $totals = array(
            array(
                'description' => 'Shipping & handling',
                'amount'      => (float)$this->getShippingAmount($order),
                'tax_amount'  => (float)$this->getShippingTaxAmount($order)
            )
        );

        $extraTotalAmount = $this->getGrandTotal($order) - $this->getBaseSubTotalWithTax($order) - $this->getDiscount($order) - $this->getShippingAmount($order);

        if ($extraTotalAmount == 0) {
            return $totals;
        }

        $totals[] = array(
            'description' => 'Extra total',
            'amount'      => (float)$extraTotalAmount
        );

        return $totals;

    }

    private function getGrandTotal($order) {
        return $order->getGrandTotal();
    }

    private function getDiscount($order) {
        return $order->getDiscountAmount();
    }

    private function getBaseSubTotalWithTax(Mage_Sales_Model_Order $order) {
        return $order->getBaseSubtotalInclTax();
    }

    private function getShippingAmount(Mage_Sales_Model_Order $order) {
        return $order->getShippingInclTax();
    }

    public function getShippingAddress(Mage_Sales_Model_Order $order) {

        if (!$shippingAddress = $order->getShippingAddress()) {
            return null;
        }

        return $this->getAddressMapper()
            ->map($shippingAddress, $shippingAddress->getData());

    }

    public function map_status($value, $modelData, Mage_Sales_Model_Order $model) {
        return $this->mapAttrValues('selveointegration/mappers_order_status', $value, $model);
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @param $dirtyData
     *
     * @return array
     */
    public function getItems(Mage_Sales_Model_Order $order, $dirtyData) {

        $items = [];

        /** @var Mage_Sales_Model_Order_Item $item */
        foreach ($order->getAllItems() as $item) {

            $children = $item->getChildrenItems();

            if (!empty($children)) {
                /** @var Mage_Sales_Model_Order_Item $childItem */

                $totalChildrenPrice    = 0;
                $totalChildrenQuantity = 0;

                foreach ($children as $childItem) {

                    $product = $this->getProductByItem($order, $childItem);

                    $totalChildrenPrice    += $item->getPriceInclTax() * $childItem->getQtyOrdered();
                    $totalChildrenQuantity += $childItem->getQtyOrdered();

                    $items[$childItem->getId()] = [
                        'bu_item_id' => $childItem->getId(),
                        'name'       => $childItem->getName(),
                        'sku'        => $product ? $product->getSku() : $childItem->getSku(),
                        'bundle'     => [
                            'id'         => $item->getSku(),
                            'name'       => $item->getName(),
                            'unit_price' => (float)$item->getPriceInclTax(),
                            'quantity'   => (float)$item->getQtyOrdered()
                        ],
                        'unit_price' => (float)$item->getPriceInclTax(),
                        'quantity'   => (float)$childItem->getQtyOrdered(),
                        'created_at' => $childItem->getCreatedAt()
                    ];
                }

                $bundlePrice   = $item->getPriceInclTax() * $item->getQtyOrdered();
                $totalDiscount = ($totalChildrenPrice - $bundlePrice) + $item->getDiscountAmount();

                foreach ($children as $childItem) {

                    $childId  = $childItem->getId();
                    $quantity = $items[$childId]['quantity'];

                    $discountFactor = 0;

                    if ($totalChildrenPrice > 0) { //prevent division by 0
                        $discountFactor = ($item->getPriceInclTax() * $quantity) / $totalChildrenPrice;
                    }

                    $items[$childId]['discount']   = $totalDiscount * $discountFactor;
                    $items[$childId]['tax_amount'] = $item->getTaxAmount() * ($quantity / $totalChildrenQuantity);

                }

                continue;
            }

            if (isset($items[$item->getId()])) {
                continue;
            }

            $product = $this->getProductByItem($order, $item);

            $items[$item->getId()] = [
                'bu_item_id' => $item->getId(),
                'name'       => $item->getName(),
                'sku'        => $product ? $product->getSku() : $item->getSku(),
                'tax_amount' => (float)$item->getTaxAmount(),
                'unit_price' => (float)$item->getPriceInclTax(),
                'quantity'   => (float)$item->getQtyOrdered(),
                'discount'   => (float)$item->getDiscountAmount(),
                'created_at' => $item->getCreatedAt()
            ];

        }

        return array_values($items);

    }

    /**
     * @return Selveo_Integration_Model_Mappers_Order_Address
     */
    private function getAddressMapper() {
        return Mage::getModel('selveointegration/mappers_order_address');
    }

    /**
     * @param $order
     * @param $item
     * @return Mage_Catalog_Model_Product
     */
    private function getProductByItem(Mage_Sales_Model_Order $order, $item) {

        $product = Mage::getModel('catalog/product')
            ->setStore($order->getStore())
            ->load($item->getProductId());

        if (!$product->getId()) {
            return null;
        }

        return $product;

    }

    protected function getAnnotations(Mage_Sales_Model_Order $order) {
        if(!$order->getId()){
            return [];
        }

        if (!$organizerTaskModel = Mage::getResourceModel('Organizer/Task_Collection')) {
            return [];
        }

        $annotations = [];

        foreach ($organizerTaskModel->getTasksForEntity('order', $order->getId(), '') as $organizerTask) {
            $annotations[] = [
                'bu_id'    => $organizerTask->getId(),
                'internal' => false,
                'message'  => $organizerTask->getOtCaption() . " - " . $organizerTask->getOtDescription()
            ];
        }

        return $annotations;
    }

    private function getShippingTaxAmount(Mage_Sales_Model_Order $order)
    {
        return $order->getShippingTaxAmount();
    }

}
