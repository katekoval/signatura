<?php

class Selveo_Integration_Model_Mappers_Product extends Selveo_Integration_Model_Mappers_Abstract
{
    protected $keyMap = array(
        'name',
        'weight',
        'sku'
    );

    protected $appends = array(
        'price'           => 'getPrice',
        '@ensureInArray'  => 'ensureArrayAttributes'
    );

    protected $addsToKeyMap = 'selveointegration/mappers_product_attribute';

    public function map_sku($sku)
    {
        return trim($sku);
    }

    /**
     * @param Mage_Eav_Model_Entity_Collection_Abstract $collection
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     * @throws Mage_Core_Exception
     */
    public function prepareCollection(Mage_Eav_Model_Entity_Collection_Abstract $collection)
    {
        return parent::prepareCollection($collection)
            ->addAttributeToSelect("barcode");
    }

    public function ensureArrayAttributes(Mage_Catalog_Model_Product $product)
    {
        $barcode = trim($product->getData('barcode'));
        
        return [
            'barcodes' => $barcode ? array($barcode) : array()
        ];
    }

    public function getPrice(Mage_Catalog_Model_Product $product) {
        return $product->getPrice() !== null ? (float) $product->getPrice() : 0;
    }

}