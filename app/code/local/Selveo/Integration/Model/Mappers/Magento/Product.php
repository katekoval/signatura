<?php

class Selveo_Integration_Model_Mappers_Magento_Product extends Selveo_Integration_Model_Mappers_Abstract
{
    protected $keyMap = [
        //todo: decimal_precision => for stock item,
    ];

    protected $addsToKeyMap = 'selveointegration/mappers_magento_product_attribute';

    protected function mapAttribute($key, $value, $modelData, $model)
    {
        $value = parent::mapAttribute($key, $value, $modelData, $model);

        /** Be graceful about NOT_NULL product attributes. Set a empty string */
        if($value === null) {
            return '';
        }

        return $value;
    }

    public function map_sku($sku)
    {
        return trim($sku);
    }

}