<?php

class Selveo_Integration_Model_Mappers_Magento_Product_Attribute
{
    public $configPath = 'selveointegration_options/product/attribute_mapping';

    public $from = 'selveo_attribute';
    public $to = 'magento_attribute';

    public $direction = 'sync_direction';
    public $currentDirection = Selveo_Integration_Model_Config_Sync_Directions::TO_MAGENTO;
}