<?php

use Selveo_Integration_Model_Config_Order_Handle_Status as ConfigOrderHandle;

class Selveo_Integration_Model_Mappers_Magento_Order extends Selveo_Integration_Model_Mappers_Abstract
{
    protected $keyMap = [
        'order_number' => 'increment_id',
        'status_code'  => 'status',
        'state_code'   => 'state',
        'billing_address',
        'shipping_address',
        'customer_email',
        'customer_firstname',
        'customer_middlename',
        'customer_lastname',
        'shipping_description',
        'shipping_method'
    ];

    public function map_status_code($value, $modelData, Mage_Sales_Model_Order $model) {

        $mappedValue = $this->mapAttrValues('selveointegration/mappers_magento_order_status', $value, $model);

        if ($mappedValue != $value) {
            return $mappedValue;
        }

        if ($this->getConfigHelper()->getHandleOrderStatus() == ConfigOrderHandle::USE_DEFAULT_STATUS
            && !$this->getOrderStatusModel()->hasStatus($value)) {

            return $this->getOrderStatusModel()->getDefaultByState($modelData->state_code);

        }

        return $value;

    }

    /**
     * @param $value
     * @param $modelData
     * @return bool|Mage_Sales_Model_Order_Address
     */
    public function map_billing_address($value, $modelData, $order) {

        if (is_null($value) || !$order->getId()) {
            return false;
        }

        return $this->mapAddress($value, $order->getBillingAddress()->getId());

    }

    /**
     * @param $value
     * @param $modelData
     * @return bool|Mage_Sales_Model_Order_Address
     */
    public function map_shipping_address($value, $modelData, $order) {

        if (is_null($value) || !$order->getId()) {
            return false;
        }

        return $this->mapAddress($value, $order->getShippingAddress()->getId());

    }

    /**
     * @param $addressValues
     * @param $addressId
     * @return Mage_Sales_Model_Order_Address
     */
    protected function mapAddress($addressValues, $addressId) {

        if(array_key_exists('zip', $addressValues)) {
            $addressValues['postcode'] = $addressValues['zip'];
            unset($addressValues['zip']);
        }

        return $this->getOrderAddressModel()
            ->load($addressId)
            ->addData($addressValues)
            ->implodeStreetAddress();

    }

    /**
     * @return Selveo_Integration_Model_Order_Status
     */
    private function getOrderStatusModel() {
        return Mage::getModel('selveointegration/order_status');
    }

    /**
     * @return Mage_Sales_Model_Order_Address
     */
    private function getOrderAddressModel() {
        return Mage::getModel('sales/order_address');
    }

}