<?php

class Selveo_Integration_Model_Mappers_Magento_Order_Status
{
    public $configPath = "selveointegration_options/order_status/map";
    public $from = 'selveo_order_status';
    public $to = 'magento_order_status';
}