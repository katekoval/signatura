<?php

class Selveo_Integration_Model_Mappers_Product_Image extends Selveo_Integration_Model_Mappers_Abstract
{

    protected $keyMap = [
        'value_id'  => '@myid',
        'file'      => 'url',
    ];

    public function map_file($file)
    {
        return $this->getProductMediaUrl() . $file;
    }

    /**
     * @return string
     */
    private function getProductMediaUrl()
    {
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . '/catalog/product';
    }

}