<?php

class Selveo_Integration_Model_Mappers_Product_Attribute
{
    public $configPath = 'selveointegration_options/product/attribute_mapping';

    public $from = 'magento_attribute';
    public $to = 'selveo_attribute';

    public $direction = 'sync_direction';
    public $currentDirection = Selveo_Integration_Model_Config_Sync_Directions::TO_SELVEO;
}