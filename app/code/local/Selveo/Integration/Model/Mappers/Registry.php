<?php

class Selveo_Integration_Model_Mappers_Registry
{
    protected $registeredMappers = [
        'orders'       => 'Selveo_Integration_Model_Mappers_Magento_Order',
        'products'     => 'Selveo_Integration_Model_Mappers_Magento_Product',
        'stock_status' => 'Selveo_Integration_Model_Mappers_Magento_Catalog_Inventory_Stock_Item'
    ];

    /**
     * @return array
     */
    public function getSelveoAttributesWhitelist()
    {
        $whitelist = [];

        foreach ($this->registeredMappers as $entityName => $mapper) {
            $whitelist[$entityName] = $this->extractSelveoAttributes((new $mapper)->getKeyMap());
        }

        return $whitelist;

    }

    /**
     * @param array $keyMap
     * @return array
     */
    private function extractSelveoAttributes(array $keyMap)
    {
        $selveoAttributes = [];

        foreach ($keyMap as $srcAttribute => $targetAttribute) {
            if (!is_numeric($srcAttribute)) {
                $selveoAttributes[] = $srcAttribute;
                continue;
            }

            $selveoAttributes[] = $targetAttribute;
        }

        return $selveoAttributes;
    }

}