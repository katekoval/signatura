<?php

class Selveo_Integration_Model_SerializedCollection implements Serializable
{
    protected $ids = array();
    protected $modelResourceName;

    public function __construct(Varien_Data_Collection_Db $collection) {
        $this->ids               = $collection->getAllIds();
        $this->modelResourceName = $collection->getNewEmptyItem()->getResourceName();
    }

    public function getCollection() {

        $model = Mage::getModel($this->modelResourceName);

        return $model->getCollection()->addFieldToFilter($model->getIdFieldName(), ['in' => $this->ids]);

    }

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize() {
        return json_encode(array('ids' => $this->ids, 'resource' => $this->modelResourceName));
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized) {

        $unserialized = json_decode($serialized);
        $this->ids = $unserialized->ids;
        $this->modelResourceName = $unserialized->resource;

    }
}