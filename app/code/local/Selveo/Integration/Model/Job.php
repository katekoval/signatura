<?php

class Selveo_Integration_Model_Job extends Mage_Core_Model_Abstract
{

    /** @var Selveo_Integration_Model_Interfaces_Job */
    protected $job;

    const MAX_RETRIES = 5;

    const STATE_PENDING     = 0;
    const STATE_SUCCESSFUL  = 1;
    const STATE_FATAL       = 2;

    protected function _construct() {
        $this->_init('selveointegration/job');
    }

    /**
     * @return $this
     */
    public function incrementTries() {

        $tries = ($this->getTries() ?: 0) + 1;

        if($tries > self::MAX_RETRIES){
            $this->getLogHelper()->log('Job with id: '.$this->getId().' exceeded max tries. Job is getting a fatal state');

            $this->setState(self::STATE_FATAL);
        }

        return $this->setTries($tries);

    }

    public function success() {
        $this->setState(self::STATE_SUCCESSFUL)
            ->setRetryAt(null)
            ->save();
    }

    public static function wrap(Selveo_Integration_Model_Interfaces_Job $job) {

        $instance = new static();
        $instance->setJob($job);

        return $instance;
    }

    protected function _beforeSave() {

        if ($this->job) {
            $this->serializeJob();
        }

        return parent::_beforeSave();

    }

    /**
     * @param Selveo_Integration_Model_Interfaces_Job $job
     */
    public function setJob(Selveo_Integration_Model_Interfaces_Job $job) {
        $this->job = $job;
    }

    /**
     * Returns the job.
     *
     * @return Selveo_Integration_Model_Abstract_Job
     */
    public function getJob() {

        if(!$this->job){
            $this->job = unserialize($this->getSerializedJob()) ?: null;
        }

        return $this->job;
    }

    /**
     * @return string
     */
    public function getSerializedJob() {
        return $this->getData('serialized');
    }

    public function reserve() {

        $this->setReservedAt(Mage::helper('selveointegration/date')->now())
            ->save();

    }

    /**
     * @return Selveo_Integration_Helper_Log
     */
    private function getLogHelper() {
        return Mage::helper('selveointegration/log');
    }

    /**
     * @return string
     */
    public function serializeJob()
    {
        $this->setData('serialized', $serialized = serialize($this->job));

        return $serialized;
    }

}