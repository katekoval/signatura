<?php

class Selveo_Integration_Model_Observers_Adminhtml_Block
{
    public function addForceSyncButtons($observer) {

        if(!$this->getConfigHelper()->canForceSync()) {
            return;
        }

        $block   = $observer->getBlock();
        $request = Mage::app()->getRequest();

        if ($block instanceof Mage_Adminhtml_Block_Catalog_Product_Edit) {
            $this->addProductForceSyncButton($block, $request);
        }

    }

    private function addProductForceSyncButton(Mage_Adminhtml_Block_Catalog_Product_Edit $block, Mage_Core_Controller_Request_Http $request) {

        $productId = $request->getParam('id');

        $button = $block->getLayout()->createBlock('adminhtml/widget_button')->setData(array(
            'label'   => $this->getDataHelper()->__('Sync to Selveo'),
            'onclick' => "setLocation('".$this->getForceSyncProductUrl($productId)."');",
            'style' => 'float: right; margin-left: 10px;'
        ));

        $block->getLayout()->getBlock('content')->append($button,'selveo_product_sync');

    }

    private function getForceSyncProductUrl($productId) {

        return $this->getAdminhtmlHelper()->getUrl(
            'adminhtml/selveointegration_productsync/force',
            array('id' => $productId)
        );

    }

    /**
     * @return Mage_Adminhtml_Helper_Data
     */
    private function getAdminhtmlHelper() {
        return Mage::helper('adminhtml');
    }

    /**
     * @return Selveo_Integration_Helper_Data
     */
    private function getDataHelper() {
        return Mage::helper('selveointegration');
    }

    /**
     * @return Selveo_Integration_Helper_Config
     */
    private function getConfigHelper()
    {
        return Mage::helper('selveointegration/config');
    }

}