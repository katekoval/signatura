<?php

use Selveo_Integration_Model_Jobs_SyncInvoiceToSelveo as SyncInvoiceToSelveo;
use Selveo_Integration_Model_Jobs_SyncOrdersToSelveo as SyncOrdersToSelveo;
use Selveo_Integration_Model_Jobs_SyncShipmentToSelveo as SyncShipmentToSelveo;

class Selveo_Integration_Model_Observers_Order
{
    use Selveo_Integration_Model_FlagsModels;

    protected $loggedOrders = array();

    public function model_save_commit_after($observer)
    {

        $task = $observer->getEvent()->getObject();

        if(!$task instanceof MDN_Organizer_Model_Task){
            return;
        }

        if($task->getot_entity_type() !== "order"){
            return;
        }

        $orderId = $task->getot_entity_id();

        /** @var Mage_Sales_Model_Order $order */
        if(!$order = Mage::getModel('sales/order')->load($orderId)){
            $this->getLogModel()->log(__METHOD__."can't load order, order id: ".$orderId);
            return;
        }

        if (!$this->shouldNotSyncOrder($order)) {
            $this->getLogModel()->log(__METHOD__." shouldn't sync order, order #".$order->getIncrementId());
            return;
        }

        if(!$this->shouldSyncWithStatus($order)) {
            $this->logNotSyncedWithStatus($order);
            return;
        }

        $this->getLogModel()->log(__METHOD__.' queue, order #'.$order->getIncrementId());

        $this->getJobHandler()->queue(
            new SyncOrdersToSelveo(array($order))
        );
    }

    public function orderPlaced(Varien_Event_Observer $observer)
    {
        $this->getLogModel()->log(__METHOD__ . ' before');

        $orderId = current($observer->getEvent()->getOrderIds());

        /** @var Mage_Sales_Model_Order $order */
        $order = Mage::getModel('sales/order')->load($orderId);

        /** Don't sync the order on success, when we have a ePay order, because we already did on afterCommitSave */
        if ($this->shouldNotSyncOrder($order) || $order->getPayment()->getMethodInstance()->getCode() === 'epay_standard') {
            $this->getLogModel()->log(__METHOD__ . " shouldn't sync order #".$order->getIncrementId());
            return;
        }

        if(!$this->shouldSyncWithStatus($order)) {
            $this->logNotSyncedWithStatus($order);
            return;
        }

        $this->getLogModel()->log(__METHOD__ ." queue order #".$order->getIncrementId());

        $this->getJobHandler()->queue(
            new SyncOrdersToSelveo(array($order))
        );

    }

    public function afterCommitSave(Varien_Event_Observer $observer)
    {
        $this->getLogModel()->log(__METHOD__.' before');

        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getEvent()->getOrder();

        if ($this->shouldNotSyncOrder($order)) {
            $this->getLogModel()->log(__METHOD__." shouldn't sync order #".$order->getIncrementId());
            return;
        }

        if(!$this->shouldSyncWithStatus($order)) {
            $this->logNotSyncedWithStatus($order);
            return;
        }

        /** Sync the order now, when epay changes the order from before to the after payment status. They are exitting elseways, so we can't flush the queue as normal behaviour.. */
        if ($this->getModulesModel()->isInstalled('Mage_Epay')
            && $order->getStatus() === $order->getStore()->getConfig('payment/epay_standard/order_status_after_payment')) {

            $this->getJobHandler()->setMode(Selveo_Integration_Model_Job_Handler::MODE_SYNC);
            $this->getLogModel()->log(__METHOD__.' set sync mode, order #'.$order->getIncrementId());

        }

        $this->getLogModel()->log(__METHOD__.' queue, order #'.$order->getIncrementId());

        $this->getJobHandler()->queue(
            new SyncOrdersToSelveo(array($order))
        );

    }

    public function invoiceInSelveo($observer) {

        /** @var Mage_Sales_Model_Order_Invoice $invoice */
        $invoice = $observer->getEvent()->getInvoice();

        if (!$invoice->getId() || $this->modelWasSynced($invoice)) {
            $this->getLogModel()->log(__METHOD__." don't sync invoice #".$invoice->getIncrementId());
            return;
        }

        $this->getLogModel()->log(__METHOD__.' queue invoice #'.$invoice->getIncrementId());

        $this->getJobHandler()->queue(
            new SyncInvoiceToSelveo($invoice)
        );

    }

    public function shipInSelveo($observer) {


        /** @var Mage_Sales_Model_Order_Shipment $shipment */
        $shipment = $observer->getEvent()->getShipment();

        if (!$shipment->getId() || $this->modelWasSynced($shipment)) {
            $this->getLogModel()->log(__METHOD__." shouldn't sync shipment #".$shipment->getIncrementId());
            return;
        }

        $this->getLogModel()->log(__METHOD__.' queue shipment #'.$shipment->getIncrementId());

        $this->getJobHandler()->queue(
            new SyncShipmentToSelveo($shipment)
        );

    }

    public function addressAfterCommitSave($observer) {

        $this->getLogModel()->log(__METHOD__.' before');

        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getEvent()->getAddress()->getOrder();

        if ($this->shouldNotSyncOrder($order)) {
            $this->getLogModel()->log(__METHOD__." shouldn't sync order #".$order->getIncrementId());
            return;
        }

        if(!$this->shouldSyncWithStatus($order)) {
            $this->logNotSyncedWithStatus($order);
            return;
        }

        $this->getLogModel()->log(__METHOD__.' queue, order #'.$order->getIncrementId());

        $this->getJobHandler()->queue(
            new SyncOrdersToSelveo(array($order))
        );

    }

    /**
     * @param $order
     * @return bool
     */
    protected function shouldSyncWithStatus($order) {

        //Ignore the blacklist if the order has already landed in Selveo
        if($order->getSelveoSynced() !== null) {
            return true;
        }

        foreach ($this->getConfigHelper()->getSyncOrderStatusBlacklist($order->getStore()) as $status) {

            if($order->getStatus() === $status) {
                return false;
            }

        }

        return true;

    }

    /**
     * @return Selveo_Integration_Helper_Config
     */
    private function getConfigHelper() {
        return Mage::helper('selveointegration/config');
    }

    /**
     * @return Selveo_Integration_Model_Job_Handler
     */
    private function getJobHandler() {
        return Mage::getSingleton('selveointegration/job_handler');
    }

    private function logNotSyncedWithStatus($order) {

        if($this->orderIsLogged($order)) {
            return;
        }

        $this->getLogModel()->log('Order #'.$order->getIncrementId().' is not synced. Order has status: '.$order->getStatus());
        $this->addAsLoggedOrder($order);

    }

    /**
     * @return Selveo_Integration_Model_Log
     */
    private function getLogModel() {
        return Mage::getSingleton('selveointegration/log');
    }

    private function addAsLoggedOrder($order) {
        $this->loggedOrders[] = $order->getId();
    }

    private function orderIsLogged($order) {
        return in_array($order->getId(), $this->loggedOrders);
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return bool
     */
    protected function shouldNotSyncOrder(Mage_Sales_Model_Order $order)
    {
        return !$order->getId()
            || $this->modelWasSynced($order)
            || !$this->getConfigHelper()->getSyncOrdersAuto($order->getStore())
            || $order->getData('is_virtual') == 1 && !$this->getConfigHelper()->getSyncVirtualOrders($order->getStore());
    }

    /**
     * @return Selveo_Integration_Model_Modules
     */
    private function getModulesModel()
    {
        return Mage::getSingleton('selveointegration/modules');
    }

}