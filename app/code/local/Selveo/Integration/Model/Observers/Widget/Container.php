<?php

class Selveo_Integration_Model_Observers_Widget_Container
{
    public function addForceSyncButtons($observer) {

        if (!$this->getConfigHelper()->canForceSync()) {
            return;
        }

        $block   = $observer->getBlock();
        $request = Mage::app()->getRequest();

        if ($block instanceof Mage_Adminhtml_Block_Sales_Order_View) {
            $this->addOrderForceSyncButton($block, $request);
        }

        if ($block instanceof Mage_Adminhtml_Block_Sales_Order_Shipment_View) {
            $this->addShipmentForceSyncButton($block, $request);
        }

        if ($block instanceof Mage_Adminhtml_Block_Sales_Order_Invoice_View) {
            $this->addInvoiceForceSyncButton($block, $request);
        }

    }

    private function addOrderForceSyncButton($block, Mage_Core_Controller_Request_Http $request) {

        $orderId = $request->getParam('order_id');

        $block->addButton('selveo_force_sync_order', array(
            'label' => $this->getDataHelper()->__('Sync order to Selveo <br>(%s)', $this->getSelveoSyncedDateForOrder()),
            'onclick' => "setLocation('".$this->getOrderForceSyncUrl($orderId)."')"
        ));

    }

    private function addShipmentForceSyncButton($block, Mage_Core_Controller_Request_Http $request) {

        $shipmentId = $request->getParam('shipment_id');

        $block->addButton('selveo_force_sync_shipment', array(
            'label' => $this->getDataHelper()->__('Sync shipment to Selveo <br>(%s)', $this->getSelveoSyncedDateForShipment()),
            'onclick' => "setLocation('".$this->getShipmentForceSyncUrl($shipmentId)."')"
        ));

    }

    private function addInvoiceForceSyncButton($block, Mage_Core_Controller_Request_Http $request) {

        $invoiceId = $request->getParam('invoice_id');

        $block->addButton('selveo_force_sync_invoice', array(
            'label' => $this->getDataHelper()->__('Sync invoice to Selveo <br>(%s)', $this->getSelveoSyncedDateForInvoice()),
            'onclick' => "setLocation('".$this->getInvoiceForceSyncUrl($invoiceId)."')"
        ));

    }

    /**
     * @return string
     */
    private function getSelveoSyncedDateForOrder() {
        return Mage::registry('current_order')->getSelveoSynced() ? Mage::registry('current_order')->getSelveoSynced() : 'not yet synced';
    }

    private function getSelveoSyncedDateForShipment() {
        return Mage::registry('current_shipment')->getSelveoSynced() ? Mage::registry('current_shipment')->getSelveoSynced() : 'not yet synced';
    }

    private function getSelveoSyncedDateForInvoice() {
        return Mage::registry('current_invoice')->getSelveoSynced() ? Mage::registry('current_invoice')->getSelveoSynced() : 'not yet synced';
    }

    private function getOrderForceSyncUrl($orderId) {
        return Mage::helper('adminhtml')->getUrl('adminhtml/selveointegration_ordersync/force', array('id' => $orderId));
    }

    private function getShipmentForceSyncUrl($shipmentId) {
        return Mage::helper('adminhtml')->getUrl('adminhtml/selveointegration_shipmentsync/force', array('id' => $shipmentId));
    }

    private function getInvoiceForceSyncUrl($invoiceId) {
        return Mage::helper('adminhtml')->getUrl('adminhtml/selveointegration_invoicesync/force',
            array('id' => $invoiceId));
    }


    /**
     * @return Selveo_Integration_Helper_Data
     */
    private function getDataHelper()
    {
        return Mage::helper('selveointegration');
    }

    /**
     * @return Selveo_Integration_Helper_Config
     */
    private function getConfigHelper()
    {
        return Mage::helper('selveointegration/config');
    }

}