<?php

class Selveo_Integration_Model_Observers_MassAction
{
    public function addSyncToSelveoAction($observer) {

        if(!$this->getConfigHelper()->canForceSync()) {
            return;
        }

        $block = $observer->getEvent()->getBlock();

        if(get_class($block) === 'Mage_Adminhtml_Block_Widget_Grid_Massaction'
            && $block->getRequest()->getControllerName() === 'sales_order') {

            $block->addItem('selveointegration_sync_order_to_selveo_massaction', array(
                'label' => $this->getDataHelper()->__('Sync orders to Selveo'),
                'url' => $this->getSyncOrdersToSelveoUrl()
            ));

        }

        if(get_class($block) === 'Mage_Adminhtml_Block_Widget_Grid_Massaction'
            && $block->getRequest()->getControllerName() === 'catalog_product') {

            $block->addItem('selveointegration_sync_product_to_selveo_massaction', array(
                'label' => $this->getDataHelper()->__('Sync products to Selveo'),
                'url' => $this->getSyncProductsToSelveo()
            ));

        }

    }

    private function getSyncOrdersToSelveoUrl() {
        return $this->getAdminhtmlHelper()->getUrl('adminhtml/selveointegration_ordersync/massforce');
    }

    private function getSyncProductsToSelveo() {
        return $this->getAdminhtmlHelper()->getUrl('adminhtml/selveointegration_productsync/massforce');
    }

    /**
     * @return Selveo_Integration_Helper_Data
     */
    private function getDataHelper() {
        return Mage::helper('selveointegration');
    }

    /**
     * @return Mage_Adminhtml_Helper_Data
     */
    private function getAdminhtmlHelper() {
        return Mage::helper('adminhtml');
    }

    /**
     * @return Selveo_Integration_Helper_Config
     */
    private function getConfigHelper()
    {
        return Mage::helper('selveointegration/config');
    }

}