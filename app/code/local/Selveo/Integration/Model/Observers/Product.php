<?php

class Selveo_Integration_Model_Observers_Product
{

    public function syncChanges(Varien_Event_Observer $observer)
    {

        /** @var Mage_Catalog_Model_Product $product */
        $product = $observer->getEvent()->getProduct();

        $this->blockSkuChanges($product);

        $this->syncRemovedImages($product);

    }


    /**
     * catalog_product_save_before observer method
     */
    public function blockSkuChanges(Mage_Catalog_Model_Product $product) {

        if(!$product->getId() || !$this->getConfigHelper()->getModuleEnabled($product->getStore())) {
            return;
        }

        $origSku = $product->getOrigData('sku');

        if(!is_null($origSku) && $product->getSku() !== $origSku){

            $this->handleSkuChange($product, $origSku);

        }

    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @throws Selveo_Integration_Model_Exception_BlockSkuChangeException
     */
    private function handleSkuChange(Mage_Catalog_Model_Product $product, $origSku) {

        if ($this->getConfigHelper()->getBlockSkuChanges($product->getStore())) {
            throw new Selveo_Integration_Model_Exception_BlockSkuChangeException($product);
        }

        $this->getSessionModel()->adminhtmlWarning(
            "You recently changed the product with SKU '%s' to SKU '%s'. If you want SELVEO to still keep track of this product, remember to change the SKU in SELVEO manually. <br>
                If you want to start blocking the SKU changes go to the configuration",
            $origSku, $product->getSku()
        );

    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @throws Exception
     */
    private function syncRemovedImages(Mage_Catalog_Model_Product $product)
    {
        if(!array_key_exists('images', $gallery = $product->getData('media_gallery'))) {
            return;
        }

        if (count($this->getImagesHelper()->getDeletedImageIds(json_decode($gallery['images'], true))) === 0) {
            return;
        }

        $this->getJobHandler()
            ->setMode(Selveo_Integration_Model_Job_Handler::MODE_ASYNC)
            ->queue(new Selveo_Integration_Model_Jobs_DeleteImagesInSelveo($product));
    }

    /**
     * catalog_product_save_after observer method
     * @param Varien_Event_Observer $observer
     * @throws Varien_Exception
     * @throws Exception
     */
    public function syncCreated(Varien_Event_Observer $observer) {

        if(!$this->getConfigHelper()->syncNewlyCreatedProducts()) {
            return;
        }

        /** @var Mage_Catalog_Model_Product $product */
        $product = $observer->getEvent()->getProduct();

        if($product->getOrigData('sku') || $product->getSku() === null) {
            return;
        }

        /**
         * Sync product if product doesn't have a SKU before or the product has already a SKU.
         */
        $this->getJobHandler()->queue(new Selveo_Integration_Model_Jobs_SyncProductsToSelveo(array($product)));

        $this->getJobHandler()->queue(
            new Selveo_Integration_Model_Jobs_SyncImagesToSelveo($product)
        );

    }

    /**
     * @return Selveo_Integration_Helper_Config
     */
    private function getConfigHelper() {
        return Mage::helper('selveointegration/config');
    }

    /**
     * @return Selveo_Integration_Model_Job_Handler
     */
    private function getJobHandler() {
        return Mage::getSingleton('selveointegration/job_handler');
    }

    /**
     * @return Selveo_Integration_Model_Session
     */
    private function getSessionModel() {
        return Mage::getSingleton('selveointegration/session');
    }

    /**
     * @return Selveo_Integration_Helper_Images
     */
    private function getImagesHelper()
    {
        return Mage::helper('selveointegration/images');
    }

}