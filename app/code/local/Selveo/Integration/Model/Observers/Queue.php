<?php

class Selveo_Integration_Model_Observers_Queue
{
    public function flush() {

        $this->getLogModel()->log(__METHOD__.': '.json_encode(Mage::app()->getRequest()->getRequestUri()));

        $this->getJobHandler()->flushQueue();
    }

    public function flushAndSetSynchronous() {

        $this->getLogModel()->log(__METHOD__);

        $this->getJobHandler()
            ->setMode(Selveo_Integration_Model_Job_Handler::MODE_SYNC)
            ->flushQueue();

    }

    public function setSynchronous() {

        $this->getLogModel()->log(__METHOD__);

        $this->getJobHandler()->setMode(Selveo_Integration_Model_Job_Handler::MODE_SYNC);

    }

    public function setAsynchronous() {

        $this->getLogModel()->log(__METHOD__);

        $this->getJobHandler()->setMode(Selveo_Integration_Model_Job_Handler::MODE_ASYNC);

    }

    /**
     * @return Selveo_Integration_Model_Job_Handler
     */
    private function getJobHandler() {
        return Mage::getSingleton('selveointegration/job_handler');
    }

    /**
     * @return Selveo_Integration_Model_Log
     */
    private function getLogModel()
    {
        return Mage::getSingleton('selveointegration/log');
    }
}