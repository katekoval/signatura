<?php

class Selveo_Integration_Model_Observers_Block
{

    protected $productInfoBlockNames = array(
        'product.info.simple',
        'product.info.configurable'
    ); //'product.info.grouped'

    public function addAvailability($observer) {

        $layout = $observer->getEvent()->getLayout();

        if (!in_array('catalog_product_view', $layout->getUpdate()->getHandles())
            || !$this->moduleEnabled()
            || !$this->shouldShowAvailabilityMessageInFrontend()) {
            return;
        }

        if (!($product = Mage::registry('current_product'))
            || !$product->hasAvailabilityMessage()
            || !$product->getAvailabilityMessage()) {
            return;
        }

        $blocks = $layout->getAllBlocks();

        switch ($this->getConfigHelper()->getHandleAvailability()) {

            case Selveo_Integration_Model_Config_Availability::INJECT:
                $this->addChildBlockToExtrahint($layout, $blocks);
                break;

            case Selveo_Integration_Model_Config_Availability::OVERRIDE:
                $this->overrideProductInfoTemplate($layout, $blocks);
                break;

        }


    }

    /**
     * @return Selveo_Integration_Helper_Config
     */
    private function getConfigHelper() {
        return Mage::helper('selveointegration/config');
    }

    private function addChildBlockToExtrahint($layout, $blocks) {

        $availabilityBlock = $layout->createBlock(
            'Selveo_Integration_Block_Frontend_Product_Availability',
            'selveointegration_availability',
            array('template' => 'selveointegration/availability.phtml')
        );

        foreach ($blocks as $blockName => $block) {

            if ($blockName === 'product.info.extrahint') {
                $layout->getBlock($blockName)->append($availabilityBlock);
            }

        }

    }

    private function overrideProductInfoTemplate($layout, $blocks) {

        $template = 'selveointegration/type/default.phtml';

        foreach ($blocks as $blockName => $block) {

            if (in_array($blockName, $this->getProductInfoBlockNames())) {
                $layout->getBlock($blockName)->setTemplate($template);
            }

        }

    }

    private function moduleEnabled() {
        return $this->getConfigHelper()->getModuleEnabled();
    }

    private function shouldShowAvailabilityMessageInFrontend() {
        return $this->getConfigHelper()->getShowAvailability();
    }

    /**
     * @return array
     */
    public function getProductInfoBlockNames() {
        return $this->productInfoBlockNames;
    }

}