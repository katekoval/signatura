<?php

class Selveo_Integration_Model_Observers_Config
{
    public function configChanged()
    {
        $this->removePreflightCache();
    }

    private function removePreflightCache()
    {
        (new Selveo_Integration_Model_Job_Handler())
            ->handleNow(new Selveo_Integration_Model_Jobs_InvalidatePreflightInSelveo);
    }
}