<?php

class Selveo_Integration_Model_Config_Countries
{
    public function toOptionArray()
    {
        return array(
            array(
                'label' => 'Denmark',
                'value' => 'DK'
            ),
            array(
                'label' => 'Norway',
                'value' => 'NO'
            ),
            array(
                'label' => 'Sweeden',
                'value' => 'SE'
            ),
            array(
                'label' => 'Great britain',
                'value' => 'GB'
            )
        );
    }
}