<?php

class Selveo_Integration_Model_Config_Order_Status extends Mage_Sales_Model_Mysql4_Order_Status_Collection {

    public function getArrayCollection() {

        return array_map(function($status) {

            return [
                'label' => $status['label'],
                'value' => $status['status'],
                'params' => $this->getParams($status)
            ];

            }, $this->getData()
        );

    }

    /**
     * Todo: refactor to disable in js
     * @param array $status
     * @return array
     */
    private function getParams(array $status) {

        return [];

        if(!$this->getOrderStatusMapperModel()->isMapped($status['status'])) {
            return [];
        }

        return ['disabled' => 'disabled'];

    }

    /**
     * @return Selveo_Integration_Model_Mappers_Order_Status
     */
    private function getOrderStatusMapperModel() {
        return Mage::getSingleton('selveointegration/mappers_order_status');
    }

}