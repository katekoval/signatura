<?php

class Selveo_Integration_Model_Config_Order_Handle_Status
{
    const USE_DEFAULT_STATUS = 1;

    public function toOptionArray() {

        return [
            [
                'label' => 'Use default status for the state',
                'value' => self::USE_DEFAULT_STATUS
            ]
        ];

    }
}