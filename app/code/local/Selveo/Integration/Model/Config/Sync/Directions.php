<?php

class Selveo_Integration_Model_Config_Sync_Directions
{
    const BOTH_WAYS  = 0;
    const TO_SELVEO  = 1;
    const TO_MAGENTO = 2;

    public function toOptionArray()
    {
        return [
            [
                'label' => 'Sync both ways',
                'value' => self::BOTH_WAYS
            ],
            [
                'label' => 'Sync only to Selveo',
                'value' => self::TO_SELVEO
            ],
            [
                'label' => 'Sync only to Magento',
                'value' => self::TO_MAGENTO
            ]
        ];
    }
}