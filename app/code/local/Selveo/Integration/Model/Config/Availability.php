<?php

class Selveo_Integration_Model_Config_Availability
{

    const INJECT = 1;
    const OVERRIDE = 2;

    public function toOptionArray() {

        return array(
            array(
                'label' => 'Inject into product.info.extrahint',
                'value' => self::INJECT
            ),
            array(
                'label' => 'Override the product.info.* templates',
                'value' => self::OVERRIDE
            )
        );

    }

}