<?php

class Selveo_Integration_Model_Config_Product_Attributes_Varchar extends Selveo_Integration_Model_Config_Product_Attributes_Number
{
    protected $typesAllowed = array('varchar');

}