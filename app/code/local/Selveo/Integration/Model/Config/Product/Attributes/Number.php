<?php

class Selveo_Integration_Model_Config_Product_Attributes_Number
{
    protected $typesAllowed = array('int', 'decimal');

    public function toOptionArray() {

        $attributes = array();

        foreach (Mage::getResourceModel('catalog/product_attribute_collection') as $attribute) {

            if(!in_array($attribute->getBackendType(), $this->typesAllowed)) {
                continue;
            }

            $label = $attribute->getFrontendLabel() ? $attribute->getFrontendLabel() : $attribute->getAttributeCode();

            $attributes[] = array(
                'value' => $attribute->getAttributeCode(),
                'label' => $this->getCompatibilityHelper()->escapeConfig($label)
            );

        }

        return $attributes;

    }

    /**
     * @return Selveo_Integration_Helper_Compatibility
     */
    private function getCompatibilityHelper()
    {
        return Mage::helper('selveointegration/compatibility');
    }
}