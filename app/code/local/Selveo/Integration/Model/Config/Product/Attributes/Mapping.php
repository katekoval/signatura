<?php

class Selveo_Integration_Model_Config_Product_Attributes_Mapping extends Selveo_Integration_Model_Config_Product_Attributes_Number
{
    protected $typesAllowed = array('varchar', 'decimal', 'int', 'static');
}