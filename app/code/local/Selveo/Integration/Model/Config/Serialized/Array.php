<?php

class Selveo_Integration_Model_Config_Serialized_Array extends Mage_Adminhtml_Model_System_Config_Backend_Serialized_Array
{

    protected function _afterLoad()
    {
        if (!is_array($this->getValue())) {
            $serializedValue = $this->getValue();
            $unserializedValue = false;

            if (!empty($serializedValue)) {
                try {

                    if($serializedValue instanceof Mage_Core_Model_Config_Element) {
                        $serializedValue = $serializedValue->asArray();
                    }

                    $unserializedValue = Mage::helper('core/unserializeArray')
                        ->unserialize($serializedValue);
                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }
            $this->setValue($unserializedValue);
        }
    }


}