<?php

class Selveo_Integration_Model_Log
{
    /**
     * @var string
     */
    protected $txnId;

    public function __construct()
    {
        $this->txnId = uniqid();
    }

    public function log($message) {
        Mage::log("[{$this->txnId}] ".$message, null, 'firtal_selveointegration.log', true);
    }
}