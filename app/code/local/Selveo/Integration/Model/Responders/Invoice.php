<?php

class Selveo_Integration_Model_Responders_Invoice
{
    public function get(Mage_Sales_Model_Order_Invoice $invoice, array $requestBody, array $response)
    {
        if (!array_key_exists('items', $requestBody)) {
            return $response;
        }

        $response['bu_invoice_id'] = $invoice->getId();

        $items = $requestBody['items'];

        foreach ($invoice->getAllItems() as $invoiceItem) {

            if (($item = $this->itemRequested($items, $invoiceItem)) === null || !array_key_exists('invoice_item_id',
                    $item)) {
                continue;
            }

            $selveoInvoiceItemId = $item['invoice_item_id'];

            $response['invoice_item_ids'][$selveoInvoiceItemId] = $invoiceItem->getId();

        }

        return $response;
    }

    private function itemRequested(array $items, Mage_Sales_Model_Order_Invoice_Item $invoiceItem)
    {
        foreach ($items as $item) {

            if (!array_key_exists('bu_item_id', $item) || !array_key_exists('sku', $item)) {
                continue;
            }

            if ($item['bu_item_id'] !== $invoiceItem->getOrderItemId() || $item['sku'] !== $invoiceItem->getSku()) {
                continue;
            }

            return $item;

        }

        return null;
    }
}