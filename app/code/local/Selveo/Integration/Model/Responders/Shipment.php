<?php

class Selveo_Integration_Model_Responders_Shipment
{
    /**
     * Returns a mapping of the selveo shipment item id and the order item id in magento
     * @param Mage_Sales_Model_Order_Shipment $shipment
     * @param array $requestBody
     * @param array $response
     * @return array
     */
    public function get(Mage_Sales_Model_Order_Shipment $shipment, array $requestBody, array $response)
    {
        if (!array_key_exists('items', $requestBody)) {
            return $response;
        }

        $response['bu_shipment_id'] = $shipment->getId();

        $items = $requestBody['items'];

        foreach ($shipment->getAllItems() as $shipmentItem) {

            if (($item = $this->itemRequested($items, $shipmentItem)) === null
                || !array_key_exists('shipment_item_id', $item)) {
                continue;
            }

            $selveoShipmentItemId = $item['shipment_item_id'];

            $response['shipment_item_ids'][$selveoShipmentItemId] = $shipmentItem->getId();

        }

        return $response;
    }

    private function itemRequested(array $items, Mage_Sales_Model_Order_Shipment_Item $shipmentItem)
    {
        foreach ($items as $item) {

            if (!array_key_exists('bu_item_id', $item) || !array_key_exists('sku', $item)) {
                continue;
            }

            if ($item['bu_item_id'] !== $shipmentItem->getOrderItemId() || $item['sku'] !== $shipmentItem->getSku()) {
                continue;
            }

            return $item;

        }

        return null;
    }
}