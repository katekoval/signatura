<?php

class Selveo_Integration_Model_Resource_Job_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract //1.7: Mage_Core_Model_Resource_Db_Collection_Abstract
{

    protected function _construct() {
        $this->_init('selveointegration/job');
    }

}