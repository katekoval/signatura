<?php

class Selveo_Integration_Model_Modules
{
    protected $modules;

    public function isInstalled($moduleName)
    {
        return array_key_exists($moduleName, $this->getModules());
    }

    public function getModules()
    {
        if(!$this->modules) {
            $this->modules = (array) Mage::getConfig()->getNode('modules')->children();
        }

        return $this->modules;
    }
}