<?php

use Selveo_Integration_Model_Exception_ProductNotFound as ProductNotFound;
use Selveo_Integration_Model_Exception_ProductAttributeNotFound as ProductAttributeNotFound;

class Selveo_Integration_Model_Product_Availability extends Selveo_Integration_Model_Product
{

    public function update($sku, $message) {

        $this->getEmulationHelper()->onAdmin(function () use ($sku, $message) {

            if(!($product = $this->loadBySku($sku))) {
                throw new ProductNotFound;
            }

            if(!$product->getResource()->getAttribute('availability_message')) {
                throw new ProductAttributeNotFound("The field: availability_message wasn't found on the product");
            }

            $product->setAvailabilityMessage($message)->save();

        });

        return true;

    }

    public function getMessagesByIds(array $productIds) {

        $products = Mage::getModel('selveointegration/product')
                    ->getCollection()
                    ->addAttributeToSelect('availability_message')
                    ->addFieldtoFilter('entity_id', array('in', $productIds));

        $messages = array();

        foreach ($products as $product) {

            if(!$product->hasAvailabilityMessage() || !$product->getAvailabilityMessage()) {
                continue;
            }

            $messages[$product->getId()] = $product->getAvailabilityMessage();

        }

        return $messages;

    }

    /**
     * @return Selveo_Integration_Helper_Emulator
     */
    private function getEmulationHelper() {
        return Mage::helper('selveointegration/emulator');
    }

}