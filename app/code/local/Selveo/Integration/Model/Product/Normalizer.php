<?php

use Mage_Catalog_Model_Product as Product;

class Selveo_Integration_Model_Product_Normalizer
{
    protected $normalizers = array();

    /**
     * Selveo_Integration_Model_Product_Normalizer constructor.
     */
    public function __construct() {
        $this->addBaseNormalizers();
    }

    private function addBaseNormalizers() {

        $this->addNormalizer('Mage_Catalog_Model_Product_Type_Grouped', function(Product $product, $type) {
            /** @var Mage_Catalog_Model_Product_Type_Grouped $type */
            return $type->getAssociatedProducts($product);
        });

        $this->addNormalizer('Mage_Catalog_Model_Product_Type_Configurable', function(Product $product, $type) {

            /** @var Mage_Catalog_Model_Product_Type_Configurable $type */
            return $type->getUsedProducts(null, $product);
        });

        $this->addNormalizer('Mage_Bundle_Model_Product_Type', function(Product $product, $type) {

            /** @var Mage_Bundle_Model_Product_Type $type
             * If no bundled items are specified - all will be returned from selection collection
             * Therefore return a empty array
             */
            if(count($optionIds = $type->getOptionsIds($product)) === 0) {
                return array();
            }

            return $type->getSelectionsCollection(
                $optionIds, $product
            )->getItems();

        });

    }

    public function addNormalizer($type, $normalizer) {
        $this->normalizers[$type] = $normalizer;
        return $this;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return array
     * @throws Selveo_Integration_Model_Exception_NormalizerException
     */
    public function normalize(Mage_Catalog_Model_Product $product) {

        $type = $product->getTypeInstance(true);

        foreach ($this->normalizers as $baseClass => $normalizer) {

            if(get_class($type) === $baseClass || is_subclass_of($type, $baseClass, true)) {

                $products = call_user_func($normalizer, $product, $type);

                if(!is_array($products)) {
                    throw new Selveo_Integration_Model_Exception_NormalizerException("Normalizer with type: ".get_class($type)." doesn't return an array");
                }

                return $products;

            }

        }

        return array($product);

    }

}