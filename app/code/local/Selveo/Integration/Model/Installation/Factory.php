<?php

use Selveo_Integration_Helper_Config as ConfigHelper;

class Selveo_Integration_Model_Installation_Factory
{
    /**
     * @param Mage_Core_Model_Abstract $entity
     * @return Selveo_Integration_Model_Installation_Collection
     */
    public function getFromEntity(Mage_Core_Model_Abstract $entity) {

        $collection = new Selveo_Integration_Model_Installation_Collection();

        if (!$this->isModuleEnabled($entity->getStore())) {
            return $collection;
        }

        if ($entity instanceof Mage_Sales_Model_Order ||
            $entity instanceof Mage_Sales_Model_Order_Shipment ||
            $entity instanceof Mage_Sales_Model_Order_Invoice) {

            $collection->add($this->getForStore($entity->getStore()))
                ->addContext($entity->getId());

        }

        if ($entity instanceof Mage_Catalog_Model_Product) {
            $this->addFromProduct($entity, $collection);
        }

        return $collection;

    }

    /**
     * @param Mage_Catalog_Model_Product $entity
     * @param $collection
     */
    protected function addFromProduct(Mage_Catalog_Model_Product $entity, Selveo_Integration_Model_Installation_Collection $collection) {

        /** @var Mage_Core_Model_Mysql4_Store_Collection $stores */
        $stores = Mage::getModel('core/store')
            ->getCollection()
            ->setLoadDefault(true)
            ->addIdFilter($this->getProductStoreIdsToSync($entity));

        foreach ($stores as $store) {

            if (!$this->isModuleEnabled($store)) {
                continue;
            }

            $collection->add($this->getForStore($store))
                ->addContext($entity->getId());

        }

    }

    public function getFromEntities($entities) {

        $collection = new Selveo_Integration_Model_Installation_Collection();

        foreach ($entities as $entity) {
            $collection->merge($this->getFromEntity($entity));
        }

        return $collection;

    }

    public function forCurrentConfigView() {

        $collection = new Selveo_Integration_Model_Installation_Collection();

        /** @var ConfigHelper $helper */
        $helper = Mage::helper('selveointegration/config');

        if ($store = $helper->getCurrentConfigStore()) {

            $collection->add($this->getForStore($store));
            return $collection;

        }

        if ($website = $helper->getCurrentConfigWebsite()) {

            $collection->add($this->getForWebsite($website));
            return $collection;

        }

        /** @var Mage_Core_Model_Store $store */
        $store = Mage::getModel('core/store')->load(Mage_Core_Model_App::ADMIN_STORE_ID);
        $collection->add($this->getForStore($store));

        return $collection;

    }

    /**
     * @param Mage_Core_Model_Store|null $store
     * @return Selveo_Integration_Model_Installation
     */
    public function getForStore(Mage_Core_Model_Store $store) {

        return $this->getInstallation($store)
            ->setFriendlyName($store->getName() . "(" . $store->getCode() . ")");

    }

    private function getForWebsite(Mage_Core_Model_Website $website) {

        return $this->getInstallation($website)
            ->setFriendlyName($website->getName() . '(' . $website->getCode() . ')');

    }

    /**
     * @param $scope
     * @return bool
     */
    protected function isModuleEnabled($scope) {
        return !!$this->getSettingForScope($scope, ConfigHelper::SETTING_MODULE_ENABLED);
    }

    /**
     * @param $scope
     * @return Selveo_Integration_Model_Installation
     */
    protected function getInstallation($scope) {

        return new Selveo_Integration_Model_Installation(
            $this->getSettingForScope($scope, ConfigHelper::SETTING_KEY),
            $this->getSettingForScope($scope, ConfigHelper::SETTING_SECRET),
            $this->getSettingForScope($scope, ConfigHelper::SETTING_SLUG)
        );

    }

    /**
     * @param $scope
     * @string $setting
     * @return bool|string
     */
    private function getSettingForScope($scope, $setting) {

        if (!method_exists($scope, 'getConfig')) {
            return false;
        }

        return $scope->getConfig($setting);

    }

    /**
     * @param Mage_Catalog_Model_Product $entity
     * @return array
     */
    protected function getProductStoreIdsToSync(Mage_Catalog_Model_Product $entity)
    {
        $storeIds = $entity->getStoreIds();

        /**
         * Sync admin view if no websites are chosen for product
         */
        if (count($storeIds) === 0) {
            return array(Mage_Core_Model_App::ADMIN_STORE_ID);
        }

        return $storeIds;

    }

}