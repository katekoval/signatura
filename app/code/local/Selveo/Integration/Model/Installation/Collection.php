<?php

class Selveo_Integration_Model_Installation_Collection implements IteratorAggregate
{

    /** @var Selveo_Integration_Model_Installation[] */
    protected $installations = array();

    /**
     * @param Selveo_Integration_Model_Installation $installation
     * @return Selveo_Integration_Model_Installation
     */
    public function add(Selveo_Integration_Model_Installation $installation) {

        if(!array_key_exists($installation->getUniqueId(), $this->installations)) {
            $this->installations[$installation->getUniqueId()] = $installation;
        }

        return $this->installations[$installation->getUniqueId()];

    }

    public function getIterator() {
        return new ArrayIterator($this->installations);
    }

    public function merge(Selveo_Integration_Model_Installation_Collection $collection) {

        foreach ($collection as $installation) {

            $singletonInstallation = $this->add($installation);

            foreach($installation->getContext() as $context){
                $singletonInstallation->addContext($context);
            }

        }

    }
}