<?php

use Selveo_Integration_Model_Exception_ProductNotFound as ProductNotFoundException;
use Selveo_Integration_Model_Exception_StockItemNotFound as StockItemNotFoundException;
use Selveo_Integration_Model_Exception_StockItemNotSavedException as StockItemNotSavedException;

class Selveo_Integration_Model_Catalog_Inventory_Stock_Item extends Mage_CatalogInventory_Model_Stock_Item {

    protected $productModel;
    protected $apiHelper;
    protected $requestHelper;
    protected $mapper;

    /**
     * @param $sku
     * @param $requestBody
     * @return bool
     * @throws Exception
     */
    public function update($sku, $requestBody) {

        $fields = $this->getMapper()->map($this, $requestBody);

        $this->getEmulatorHelper()->onAdmin(function() use ($sku, $fields) {

            if(!($product = $this->getProductModel()->loadBySku($sku))) {
                throw new ProductNotFoundException("Product with sku: {$sku} wasn't found");
            }

            /** @var Mage_CatalogInventory_Model_Stock_Item $stockItem */
            $stockItem = $this->loadByProduct($product);

            if(!$stockItem->getItemId()) {
                throw new StockItemNotFoundException("Inventory stock item for product with sku: {$sku} wasn't found");
            }

            $fields['is_in_stock'] = $this->calculateStockStatus($fields, $stockItem);

            $savedStockItem = $stockItem
                ->setData(array_merge($stockItem->getData(), $fields))
                ->setForceReindexRequired(true)
                ->save();

            if(!$savedStockItem) {
                throw new StockItemNotSavedException("Stock for product with sku: {$sku} wasn't saved");
            }

        });
        
        return true;

    }

    private function calculateStockStatus($fields, Mage_CatalogInventory_Model_Stock_Item $stockItem) {
        if($fields['qty'] <= 0) {
            return $stockItem->getBackorders();
        }

        return 1;

    }

    /**
     * @return Selveo_Integration_Model_Mappers_Magento_Catalog_Inventory_Stock_Item
     */
    public function getMapper() {
        return Mage::getModel('selveointegration/mappers_magento_catalog_inventory_stock_item');
    }

    /**
     * @return Selveo_Integration_Model_Product
     */
    private function getProductModel() {
        return Mage::getModel('selveointegration/product');
    }

    /**
     * @return Selveo_Integration_Helper_Emulator
     */
    private function getEmulatorHelper() {
        return Mage::helper('selveointegration/emulator');
    }

}