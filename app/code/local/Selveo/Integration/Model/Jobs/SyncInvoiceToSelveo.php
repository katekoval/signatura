<?php

use Selveo_Integration_Model_Interfaces_DelaysJob as DelaysJob;
use Selveo_Integration_Model_Abstract_Job as JobAbstract;

class Selveo_Integration_Model_Jobs_SyncInvoiceToSelveo extends JobAbstract implements DelaysJob
{

    protected $invoice;

    public function __construct(Mage_Sales_Model_Order_Invoice $invoice) {
        $this->invoice = $invoice;
    }

    /**
     * The id for the queue to handle the job
     * @return string
     */
    public function getKey() {
        return 'in:'.$this->getInvoice()->getId();
    }

    /**
     * Handle the job
     */
    public function handle() {

        $installations = (new Selveo_Integration_Model_Installation_Factory)
                            ->getFromEntity($this->getInvoice());

        try {

            /** @var Selveo_Integration_Model_Installation $installation */
            foreach ($installations as $installation) {

                $installation
                    ->getClient()
                    ->invoice()
                    ->store($this->getInvoice());

                $this->successMessage($installation);
            }


        } catch (Exception $e) {
            $this->errorMessage($installation, $e);
        }

    }

    /**
     * The delay before the Job should be executed
     * @return int
     */
    public function getDelay() {
        return 0;
    }

    /**
     * @return Mage_Sales_Model_Order_Invoice
     */
    public function getInvoice() {
        return $this->invoice;
    }

}