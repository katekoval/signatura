<?php

use Selveo_Integration_Model_Interfaces_DelaysJob as DelaysJob;
use Selveo_Integration_Model_Abstract_Job as JobAbstract;

class Selveo_Integration_Model_Jobs_SyncShipmentToSelveo extends JobAbstract implements DelaysJob
{

    protected $shipment;

    public function __construct(Mage_Sales_Model_Order_Shipment $shipment) {
        $this->shipment = $shipment;
    }

    /**
     * The delay before the Job should be executed
     * @return int
     */
    public function getDelay() {
        return 0;
    }

    /**
     * Handle the job
     */
    public function handle() {

        $installations = (new Selveo_Integration_Model_Installation_Factory)
                            ->getFromEntity($this->getShipment());

        try {

            /** @var Selveo_Integration_Model_Installation $installation */
            foreach ($installations as $installation) {

                $installation
                    ->getClient()
                    ->shipment()
                    ->store($this->getShipment());

            }

            $this->successMessage($installation);

        } catch (Exception $e) {
            $this->errorMessage($installation, $e);
        }

    }

    /**
     * The id for the queue to handle the job
     * @return string
     */
    public function getKey() {
        return 's:'.$this->getShipment()->getId();
    }

    /**
     * @return Mage_Sales_Model_Order_Shipment
     */
    public function getShipment() {
        return $this->shipment;
    }
}