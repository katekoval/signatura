<?php

use Selveo_Integration_Model_Interfaces_DelaysJob as DelaysJob;
use Selveo_Integration_Model_Abstract_Job as JobAbstract;

class Selveo_Integration_Model_Jobs_SyncOrdersToSelveo extends JobAbstract implements DelaysJob
{

    protected $collection;
    protected $showSuccess = false;

    /**
     * @param iterable $collection
     */
    public function __construct($collection) {
        $this->collection = $collection;
    }

    public function showSuccessMessages() {

        $this->showSuccess = true;
        return $this;

    }

    public function handle() {

        $installations = (new Selveo_Integration_Model_Installation_Factory())
                            ->getFromEntities($this->getCollection());

        /** @var Selveo_Integration_Model_Installation $installation */
        foreach ($installations as $installation) {

            try {

                $entitiesToSync = $this->getEntitiesToSync($installation, $this->getCollection());

                if(count($entitiesToSync) === 0) {
                    continue;
                }

                $installation
                    ->getClient()
                    ->orders()
                    ->store($entitiesToSync);

                if($this->showSuccess) {
                    $this->successMessage($installation);
                }

            } catch (Exception $e) {
                $this->errorMessage($installation, $e);
            }

        }

    }

    /**
     * The delay before the Job should be executed
     * @return int
     */
    public function getDelay() {
        return 0;
    }

    /**
     * The id for the queue to handle the job
     * @return string
     */
    public function getKey() {

        $ids = array();

        /** @var Mage_Sales_Model_Order $order */
        foreach ($this->getCollection() as $order) {
            $ids[] = $order->getId();
        }

        return 'o:'.implode(':', $ids);

    }

    /**
     * @return iterable
     */
    public function getCollection() {
        return $this->collection;
    }

}