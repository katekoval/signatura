<?php

class Selveo_Integration_Model_Jobs_SyncImagesToSelveo extends Selveo_Integration_Model_Abstract_Job implements Selveo_Integration_Model_Interfaces_DelaysJob
{
    /** @var Varien_Data_Collection */
    protected $productImageCollection;

    /**
     * @var Mage_Catalog_Model_Product
     */
    private $product;

    /**
     * Selveo_Integration_Model_Jobs_SyncImagesToSelveo constructor.
     * @param Mage_Catalog_Model_Product $product
     */
    public function __construct(Mage_Catalog_Model_Product $product)
    {
        $this->productImageCollection = $product->load('media_gallery')->getMediaGalleryImages();
        $this->product                = $product;
    }

    /**
     * The delay before the Job should be executed
     * @return int
     */
    public function getDelay()
    {
        return 0;
    }

    /**
     * Handle the job
     * @throws Exception
     */
    public function handle()
    {

        if(count($this->productImageCollection) === 0) {
            return;
        }

        $installations = (new Selveo_Integration_Model_Installation_Factory)
            ->getFromEntity($this->getProduct());

        /** @var Selveo_Integration_Model_Installation $installation */
        foreach ($installations as $installation) {
            try {

                $installation
                    ->getClient()
                    ->images()
                    ->store($this->getProduct(), $this->getProductImageCollection());

            } catch (Exception $e) {
                $this->errorMessage($installation, $e);
            }
        }
    }

    /**
     * The id for the queue to handle the job
     * @return string
     */
    public function getKey()
    {
        return 'im:'.$this->getProduct()->getId();
    }

    /**
     * @return Varien_Data_Collection
     */
    public function getProductImageCollection()
    {
        return $this->productImageCollection;
    }

    /**
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct()
    {
        return $this->product;
    }

}