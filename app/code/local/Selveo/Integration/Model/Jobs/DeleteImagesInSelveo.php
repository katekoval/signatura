<?php

class Selveo_Integration_Model_Jobs_DeleteImagesInSelveo extends Selveo_Integration_Model_Abstract_Job implements Selveo_Integration_Model_Interfaces_DelaysJob
{
    protected $product;
    protected $imageIdsToDelete = array();

    public function __construct(Mage_Catalog_Model_Product $product)
    {
        $this->product = $product;
        $gallery        = $this->getProduct()->getData('media_gallery');

        if (!array_key_exists('images', $gallery)) {
            return;
        }

        $this->imageIdsToDelete = $this->getHelper()->getDeletedImageIds(
            json_decode($gallery['images'], true)
        );

    }

    /**
     * The delay before the Job should be executed
     * @return int
     */
    public function getDelay()
    {
        return 0;
    }

    /**
     * @return mixed
     * Handle the job
     * @throws Zend_Http_Client_Exception
     */
    public function handle()
    {

        if (count($this->imageIdsToDelete) === 0) {
            return;
        }

        $installations = (new Selveo_Integration_Model_Installation_Factory())->getFromEntity($this->getProduct());

        /** @var Selveo_Integration_Model_Installation $installation */
        foreach ($installations as $installation) {

            $installation
                ->getClient()
                ->images()
                ->destroy($this->getProduct(), $this->imageIdsToDelete);

        }

    }

    /**
     * The id for the queue to handle the job
     * @return string
     */
    public function getKey()
    {
        return 'di:'.$this->getProduct()->getId();
    }

    private function getProduct()
    {
        return $this->product;
    }

    /**
     * @return Selveo_Integration_Helper_Image
     */
    private function getHelper()
    {
        return Mage::helper('selveointegration/images');
    }

}