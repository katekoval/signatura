<?php

class Selveo_Integration_Model_Jobs_InvalidatePreflightInSelveo extends Selveo_Integration_Model_Abstract_Job implements Selveo_Integration_Model_Interfaces_DelaysJob
{

    /**
     * The delay before the Job should be executed
     * @return int
     */
    public function getDelay()
    {
        return 0;
    }

    /**
     * @return mixed|void
     * @throws Exception
     */
    public function handle()
    {
        $installations = (new Selveo_Integration_Model_Installation_Factory())->forCurrentConfigView();

        /** @var Selveo_Integration_Model_Installation $installation */
        foreach ($installations as $installation) {

            try {
                $installation
                    ->getClient()
                    ->invalidatePreflight()
                    ->destroy();
            } catch (Exception $e) {
                $this->errorMessage($installation, $e);
            }

        }

    }

    /**
     * The id for the queue to handle the job
     * @return string
     */
    public function getKey()
    {
        return 'ip';
    }
}