<?php

class Selveo_Integration_Model_Jobs_TestConnectionToSelveo extends Selveo_Integration_Model_Abstract_Job
{

    /**
     * @return mixed|void
     * @throws Zend_Http_Client_Exception
     * @throws Zend_Http_Exception
     * @throws Zend_Uri_Exception
     */
    public function handle()
    {
        $installations = (new Selveo_Integration_Model_Installation_Factory())->forCurrentConfigView();

        /** @var Selveo_Integration_Model_Installation $installation */
        foreach ($installations as $installation){
            $installation->getClient()->testConnection();
        }

    }

    /**
     * The id for the queue to handle the job
     * @return string
     */
    public function getKey()
    {
        return null;
    }
}