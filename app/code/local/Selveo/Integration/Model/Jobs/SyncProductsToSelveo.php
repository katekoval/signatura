<?php

use Selveo_Integration_Model_Abstract_Job as JobAbstract;
use Selveo_Integration_Model_Interfaces_DelaysJob as DelaysJob;

class Selveo_Integration_Model_Jobs_SyncProductsToSelveo extends JobAbstract implements DelaysJob
{

    private $collection;

    /**
     * @param iterable $collection
     */
    public function __construct($collection) {
        $this->collection = $collection;
    }

    /**
     * The delay before the Job should be executed
     * @return int
     */
    public function getDelay() {
        return 0;
    }

    /**
     * Handle the job
     */
    public function handle() {

        $installations = (new Selveo_Integration_Model_Installation_Factory)
            ->getFromEntities($this->getCollection());

        /** @var Selveo_Integration_Model_Installation $installation */
        foreach ($installations as $installation) {

            try {

                $entitiesToSync = $this->getEntitiesToSync($installation, $this->getCollection());

                if(count($entitiesToSync) === 0) {
                    continue;
                }

                $installation
                    ->getClient()
                    ->products()
                    ->store($entitiesToSync);

                $this->successMessage($installation);

            } catch (Exception $e) {
                $this->errorMessage($installation, $e);
            }

        }

    }

    /**
     * The id for the queue to handle the job
     * @return string
     */
    public function getKey() {

        $ids = array();

        /** @var Mage_Catalog_Model_Product $product */
        foreach ($this->getCollection() as $product) {
            $ids[] = $product->getId();
        }

        return 'p:'.implode(':', $ids);

    }

    /**
     * @return mixed
     */
    public function getCollection() {
        return $this->collection;
    }

}