<?php

class Selveo_Integration_Model_Order_Status extends Mage_Sales_Model_Order_Status
{

    public function hasStatus($value) {

        foreach ($this->getCollection() as $orderStatus) {

            if($orderStatus->getStatus() == $value) {
                return true;
            }

        }

        return false;


    }

    public function getDefaultByState($stateCode) {

        $defaultStatus = $this->loadDefaultByState($stateCode);

        return $defaultStatus->getStatus();

    }
}