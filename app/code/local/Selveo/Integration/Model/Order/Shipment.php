<?php

use Selveo_Integration_Model_Exception_OrderNotFound as OrderNotFoundException;
use Selveo_Integration_Model_Order_Prepare_Items as PrepareItems;

class Selveo_Integration_Model_Order_Shipment extends Mage_Sales_Model_Service_Order
{

    use Selveo_Integration_Model_FlagsModels;

    public function __construct() {
        $this->setConvertor($this->getConvertor());
    }

    /**
     * @param $orderIncrementId
     * @param $requestBody
     * @return bool|Mage_Sales_Model_Order_Shipment
     * @throws Selveo_Integration_Model_Exception_NoItemsArePreparedException
     * @throws Selveo_Integration_Model_Exception_OrderIsVirtualException
     * @throws Selveo_Integration_Model_Exception_OrderNotFound
     * @throws Selveo_Integration_Model_Exception_RequestedItemsNotFoundException
     */
    public function ship($orderIncrementId, $requestBody) {

        if (!$orderIncrementId || !($order = $this->loadOrder($orderIncrementId))) {
            throw new OrderNotFoundException("Shipment: Order #{$orderIncrementId} was not found");
        }

        if($order->getIsVirtual()) {
            throw new Selveo_Integration_Model_Exception_OrderIsVirtualException("Order: #{$orderIncrementId} is virtual. The order will not be shipped");
        }

        Mage::log(__METHOD__, null, 'selveointegration_events.log', true);

        $orderItems = $order->getItemsCollection();

        if(($shipment = $this->createShipment($orderItems, $requestBody, $order)) === null) {
            return null;
        }

        $shipment->sendEmail();

        return $shipment;

    }

    /**
     * @param $orderIncrementId
     * @return Mage_Sales_Model_Order
     */
    protected function loadOrder($orderIncrementId) {
        return $this->getSalesOrderModel()->loadByIncrementId($orderIncrementId);
    }


    /**
     * @return Selveo_Integration_Model_Sales_Order
     */
    private function getSalesOrderModel() {
        return Mage::getModel('selveointegration/sales_order');
    }

    /**
     * @return Mage_Sales_Model_Convert_Order
     */
    protected function getConvertor() {
        return Mage::getModel('sales/convert_order');
    }

    private function setOrder($order) {
        $this->_order = $order;
    }

    /**
     * @param $orderItems
     * @param $requestBody
     * @param $order
     * @return Mage_Sales_Model_Order_Shipment|null
     * @throws Selveo_Integration_Model_Exception_NoItemsArePreparedException
     * @throws Selveo_Integration_Model_Exception_RequestedItemsNotFoundException
     */
    private function createShipment($orderItems, $requestBody, $order) {

        $orderItemsToShip = $this->getOrderPrepareItemsModel()
                                ->setSituation(PrepareItems::SITUATION_SHIPPING)
                                ->prepareItems($orderItems, $requestBody);

        $this->getLoggerHelper()->log(json_encode(['$orderItemsToShip' => $orderItemsToShip]));

        $this->setOrder($order);

        /** @var Mage_Sales_Model_Order_Shipment $shipment */
        $shipment = $this->prepareShipment($orderItemsToShip);
        $shipment->register();

        if($shipment->getTotalQty() <= 0) {

            $this->getLoggerHelper()->log("Nothing to ship for order: #".$order->getIncrementId());
            return null;

        }

        $order->setIsInProgress(true);

        $this->flagModelAsSynced($shipment);
        $this->flagModelAsSynced($order);

        $this->getResourceTransactionModel()
            ->addObject($shipment)
            ->addObject($order)
            ->save();

        try {
            if (array_key_exists('tracking', $requestBody)) {
                $this->addTracking($shipment, $requestBody['tracking']);
            }
        } catch (Exception $e) {
            $this->getLoggerHelper()->log('failed to add tracking, request body: '. json_encode($requestBody));
            return null;
        }

        $shipment->save();

        /** Set the selveo_synced, so we don't sync the shipment to Selveo again */
        Mage::helper('selveointegration/synced')->setDirectly($shipment);

        return $shipment;

    }

    /**
     * @return Selveo_Integration_Helper_Log
     */
    private function getLoggerHelper() {
        return Mage::helper('selveointegration/log');
    }

    /**
     * @return Selveo_Integration_Model_Order_Prepare_Items
     */
    private function getOrderPrepareItemsModel() {
        return Mage::getModel('selveointegration/order_prepare_items');
    }

    /**
     * @return Selveo_Integration_Model_Resource_Transaction
     */
    private function getResourceTransactionModel() {
        return Mage::getModel('selveointegration/resource_transaction');
    }

    private function addTracking(Mage_Sales_Model_Order_Shipment $shipment, $tracking)
    {
        foreach ($tracking as $trackingItem) {
            $shipment->addTrack(
                Mage::getModel('sales/order_shipment_track')
                    ->setNumber($trackingItem['tracking_number'])
                    ->setCarrierCode('custom')
                    ->setTitle($trackingItem['carrier_code'])
            );
        }
    }

}