<?php

use Selveo_Integration_Model_Exception_OrderNotFound as OrderNotFoundException;
use Selveo_Integration_Model_Order_Prepare_Items as PrepareItems;

class Selveo_Integration_Model_Order_Invoice extends Mage_Sales_Model_Service_Order
{
    protected $orderModel;
    protected $orderPrepareItemsModel;
    protected $requestHelper;

    use Selveo_Integration_Model_FlagsModels;

    public function __construct() {
        $this->setConvertor($this->getConvertor());
    }

    /**
     * @param $orderIncrementId
     * @param $requestBody
     * @return Mage_Sales_Model_Order_Invoice|null
     * @throws Selveo_Integration_Model_Exception_NoItemsArePreparedException
     * @throws Selveo_Integration_Model_Exception_OrderNotFound
     * @throws Selveo_Integration_Model_Exception_RequestedItemsNotFoundException
     */
    public function invoice($orderIncrementId, $requestBody) {

        if (!$orderIncrementId || !($order = $this->loadOrder($orderIncrementId))) {
            throw new OrderNotFoundException("Order #{$orderIncrementId} was not found");
        }

        Mage::log(__METHOD__, null, 'selveointegration_events.log', true);

        $this->setOrder($order);
        if(($invoice = $this->create($order, $order->getItemsCollection(), $requestBody)) === null) {
            return null;
        }

        $invoice->sendEmail();

        return $invoice;

    }

    /**
     * @return Mage_Sales_Model_Convert_Order
     */
    protected function getConvertor() {
        return Mage::getModel('sales/convert_order');
    }

    /**
     * @param $orderIncrementId
     * @return Mage_Sales_Model_Order
     */
    protected function loadOrder($orderIncrementId) {
        return Mage::getModel('selveointegration/sales_order')->loadByIncrementId($orderIncrementId);
    }

    private function setOrder($order) {
        $this->_order = $order;
    }

    /**
     * @param $order
     * @param $orderItems
     * @param $requestBody
     * @return Mage_Sales_Model_Order_Invoice|null
     * @throws Selveo_Integration_Model_Exception_NoItemsArePreparedException
     * @throws Selveo_Integration_Model_Exception_RequestedItemsNotFoundException
     * @throws Exception
     */
    private function create(Mage_Sales_Model_Order $order, $orderItems, $requestBody) {

        $invoiceItems = $this->getOrderPrepareItemsModel()
                                ->setSituation(PrepareItems::SITUATION_INVOICING)
                                ->prepareItems($orderItems, $requestBody);

        $this->getLoggerHelper()->log(json_encode(['items to invoice before add child items' => $invoiceItems]));

        $invoiceItems = $this->addChildItems($orderItems, $invoiceItems, $requestBody);
        $invoiceItems = $this->prepareNotInvoicingItems($orderItems, $invoiceItems);

        if ($this->nothingToInvoice($invoiceItems)) {
            $this->getLoggerHelper()->log('Nothing to invoice for order: #' . $order->getIncrementId());
            return null;
        }

        $this->getLoggerHelper()->log(json_encode(['items to invoice after adding not invoicing items and child items' => $invoiceItems]));

        $invoice = $this->prepareInvoice($invoiceItems);

        $this->flagModelAsSynced($invoice);
        $this->flagModelAsSynced($order);

        $this->captureInvoice($order, $invoice);

        /** Set the selveo_synced, so we don't sync the invoice to Selveo again */
        Mage::helper('selveointegration/synced')->setDirectly($invoice);

        return $invoice;

    }

    private function nothingToInvoice($invoiceItems) {

        $qtyToInvoice = 0;

        foreach ($invoiceItems as $orderItemId => $qty) {
            $qtyToInvoice += $qty;
        }

        return $qtyToInvoice < 1;

    }

    /**
     * @return Selveo_Integration_Model_Order_Prepare_Items
     */
    private function getOrderPrepareItemsModel() {
        return Mage::getModel('selveointegration/order_prepare_items');
    }

    /**
     * @return Selveo_Integration_Model_Resource_Transaction
     */
    private function getResourceTransactionModel() {
        return Mage::getModel('selveointegration/resource_transaction');
    }

    private function addChildItems($orderItems, $invoiceItems, $requestBody) {

        /**
         * Get the child items for order items with child items, so they can get invoiced
         */
        foreach ($orderItems as $orderItem) {

            $parentItemId    = $orderItem->getParentItemId();
            $canInvoiceChild = $this->getOrderPrepareItemsModel()->canShipAndInvoiceChild($orderItem);

            if ($parentItemId !== null && in_array($parentItemId, array_keys($invoiceItems)) && $canInvoiceChild) {
                $invoiceItems[$orderItem->getId()] = $this->getInvoiceQuantityForChild($requestBody, $orderItem);
            }

        }

        return $invoiceItems;

    }

    private function getInvoiceQuantityForChild($requestBody, $orderItem) {

        /**
         * Get the amount of items for each bundle instead
         */
        foreach ($requestBody['items'] as $item) {

            if ($item['sku'] !== $orderItem->getSku() || !isset($item['quantity']) || $orderItem->getQtyInvoiced() >= $orderItem->getQtyOrdered()) {
                continue;
            }

            return $item['quantity'];

        }

        return 0;

    }

    private function prepareNotInvoicingItems($orderItems, $invoiceItems) {

        /**
         * Tell Magento that the rest of the order items is not going to be invoiced, so it can be partially invoiced
         */
        foreach ($orderItems as $orderItem) {

            if (!in_array($orderItem->getId(), array_keys($invoiceItems))) {
                $invoiceItems[$orderItem->getId()] = 0;
            }

        }

        return $invoiceItems;

    }

    /**
     * @return Selveo_Integration_Helper_Log
     */
    private function getLoggerHelper() {
        return Mage::helper('selveointegration/log');
    }

    /**
     * @param $order
     * @param $invoice
     * @throws Exception
     */
    private function captureInvoice($order, $invoice)
    {
        $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
        $invoice->register();

        $this->getResourceTransactionModel()
            ->addObject($invoice)
            ->addObject($order)
            ->save();
    }

}