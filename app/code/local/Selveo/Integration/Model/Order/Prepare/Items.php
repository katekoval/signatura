<?php

use Selveo_Integration_Model_Exception_NoItemsArePreparedException as NoItemsArePreparedException;
use Selveo_Integration_Model_Exception_RequestedItemsNotFoundException as RequestedItemsNotFoundException;

class Selveo_Integration_Model_Order_Prepare_Items
{
    protected $logHelper;
    protected $situation;
    protected $requestBody;

    const SITUATION_SHIPPING  = 1;
    const SITUATION_INVOICING = 2;

    public function setSituation($situation = self::SITUATION_SHIPPING) {

        $this->situation = $situation;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getSituation() {
        return $this->situation;
    }

    /**
     * @param $orderItems
     * @param $requestBody
     * @return array
     * @throws Selveo_Integration_Model_Exception_NoItemsArePreparedException
     * @throws Selveo_Integration_Model_Exception_RequestedItemsNotFoundException
     */
    public function prepareItems($orderItems, $requestBody) {

        $items             = array();
        $this->requestBody = $requestBody;

        if (!isset($requestBody['items']) || empty($requestBody['items'])) {
            throw new RequestedItemsNotFoundException("Order is not shipped or invoiced: No items to ship or invoice");
        }

        foreach ($requestBody['items'] as $item) {

            if (!isset($item['sku']) && !isset($item['bu_item_id'])) {
                continue;
            }

            $orderItem = $this->getItem($orderItems, $item);

            if (!is_null($orderItem)) {
                $items[$orderItem->getId()] = $this->getItemQty($orderItem, $item);
            }

        }

        if (empty($items)) {
            throw new NoItemsArePreparedException("Order is not shipped or invoiced: No items has been prepared");
        }

        return $items;

    }

    /**
     * @param Mage_Sales_Model_Order_Item $orderItem
     * @param $item
     * @return float
     */
    protected function getItemQty(Mage_Sales_Model_Order_Item $orderItem, $item)
    {
        if ($this->itemInvoicedOrShipped($orderItem)) {
            return 0;
        }

        $childrenItems = $orderItem->getChildrenItems();

        /**
         * Get the quantity for the bundle, if the order item is a bundle and if the children can not be shipped
         */
        if (!empty($childrenItems) && $this->isPartOfBundle($item)) {
            return $this->getQtyForBundle($childrenItems, $orderItem);
        }

        return round($item['quantity']);

    }

    private function getQtyForBundle($childrenItems, Mage_Sales_Model_Order_Item $orderItem)
    {

        $qtyShippedInvoiced      = $this->getShippedOrInvoiced($orderItem);
        $qtyOrdered              = $this->getOrdered($orderItem);
        $itemsPerBundle          = $this->getQtyOfItemsPerBundle($childrenItems, $orderItem);
        $totalRequestedForBundle = $this->getTotalQtyRequestedForBundle($orderItem);
        $qtyOfBundleToInvoice    = (float)$totalRequestedForBundle / $itemsPerBundle;

        $this->getLoggerHelper()->log(json_encode([
            'bundle $qtyOrdered'         => $qtyOrdered,
            'bundle $qtyShippedInvoiced' => $qtyShippedInvoiced,
            'itemsPerBundle'             => $itemsPerBundle,
            '$totalRequestedForBundle'   => $totalRequestedForBundle
        ]));

        $this->getLoggerHelper()->log(json_encode(['qty of bundle to invoice' => round($qtyOfBundleToInvoice)]));

        $this->getLoggerHelper()->log(json_encode(['left to invoice' => $qtyOrdered - $qtyShippedInvoiced]));

        /**
         * If shipping the bundle would have a less quantity than the quantity ordered for the bundle
         */
        if ((float)($qtyShippedInvoiced + round($qtyOfBundleToInvoice)) <= $qtyOrdered
            || (float)($qtyShippedInvoiced + floor($qtyOfBundleToInvoice)) <= $qtyOrdered) {
            return min(
                max(round($qtyOfBundleToInvoice), 1), //Take the max value of the quantity of the bundle to invoice
                $qtyOrdered - $qtyShippedInvoiced
            ); //Take the lowest value of the quantity left to invoice or the quantity of the bundle to invoice
        }

        return 1;

    }

    /**
     * Reference: Mage_Bundle_Block_Adminhtml_Sales_Order_Items_Renderer::isShipmentSeparately
     * @param null $item
     * @return bool
     */
    public function canShipChild($item = null)
    {
        if ($item) {
            if ($item->getOrderItem()) {
                $item = $item->getOrderItem();
            }
            if ($parentItem = $item->getParentItem()) {
                if ($options = $parentItem->getProductOptions()) {
                    if (isset($options['shipment_type']) && $options['shipment_type'] == Mage_Catalog_Model_Product_Type_Abstract::SHIPMENT_SEPARATELY) {
                        return true;
                    } else {
                        return false;
                    }
                }
            } else {
                if ($options = $item->getProductOptions()) {
                    if (isset($options['shipment_type']) && $options['shipment_type'] == Mage_Catalog_Model_Product_Type_Abstract::SHIPMENT_SEPARATELY) {
                        return false;
                    } else {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Reference: Mage_Bundle_Block_Adminhtml_Sales_Order_Items_Renderer::isChildCalculated
     * @param $orderItem
     * @return bool
     */
    public function canShipAndInvoiceChild($orderItem) {

        if ($parentItem = $orderItem->getParentItem()) {
            if ($options = $parentItem->getProductOptions()) {
                if (isset($options['product_calculations']) && $options['product_calculations'] == Mage_Catalog_Model_Product_Type_Abstract::CALCULATE_CHILD) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            if ($options = $orderItem->getProductOptions()) {
                if (isset($options['product_calculations']) && $options['product_calculations'] == Mage_Catalog_Model_Product_Type_Abstract::CALCULATE_CHILD) {
                    return false;
                } else {
                    return true;
                }
            }
        }

        return false;

    }

    /**
     * @param $orderItem
     * @return float
     */
    private function getTotalQtyRequestedForBundle($orderItem)
    {
        $totalQtyRequestedForBundle = 0;

        foreach ($this->getRequestBody()['items'] as $item) {

            if (!isset($item['bundle']) || !isset($item['bundle']['id'])) {
                continue;
            }

            if ($orderItem->getSku() === $item['bundle']['id']) {
                $totalQtyRequestedForBundle += $item['quantity'];
            }

        }

        return (float) $totalQtyRequestedForBundle;
    }

    /**
     * @param $orderItems
     * @param $item
     * @return mixed|null $orderItem|null
     */
    public function getItem($orderItems, $item)
    {
        foreach ($orderItems as $orderItem) {

            if (($item['bu_item_id'] && $orderItem->getId() != $item['bu_item_id'])
                || ($item['sku'] && $this->getSku($orderItem) != $item['sku'])) {
                continue;
            }

            /**
             * If order item has a bundle as a parent and we can not ship/invoice the children for the bundle
             */
            if ($orderItem->getParentItemId()) {

                if (!$this->isPartOfBundle($item)) {
                    continue;
                }

                if($this->getSituation() === self::SITUATION_SHIPPING && $this->canShipChild($orderItem)) { //Get the child item if we can ship thee child
                    return $orderItem;
                }

                return $orderItem->getParentItem();

            }

            return $orderItem;

        }

        return null;

    }

    /**
     * @param $item
     * @return bool
     */
    private function isPartOfBundle($item) {
        return isset($item['bundle']) && isset($item['bundle']['quantity']);
    }

    /**
     * @param $childrenItems
     * @return float
     */
    private function getQtyOfItemsPerBundle($childrenItems, Mage_Sales_Model_Order_Item $orderItem)
    {
        $totalItemsQty = 0;

        foreach ($childrenItems as $childrenItem) {
            $totalItemsQty += $childrenItem->getQtyOrdered();
        }

        /**
         * Divide the total ordered quantity for the children items with the quantity ordered for the bundle
         */
        return (float) round((float)$totalItemsQty / $this->getOrdered($orderItem));
    }

    /**
     * @param $orderItem
     * @return mixed
     */
    private function getShippedOrInvoiced($orderItem) {
        return $this->getSituation() === self::SITUATION_SHIPPING ? (float)$orderItem->getQtyShipped() : (float)$orderItem->getQtyInvoiced();
    }

    /**
     * @return Selveo_Integration_Helper_Log
     */
    private function getLoggerHelper() {
        return Mage::helper('selveointegration/log');
    }

    /**
     * @param Mage_Sales_Model_Order_Item $orderItem
     * @return bool
     */
    protected function itemInvoicedOrShipped(Mage_Sales_Model_Order_Item $orderItem)
    {
        return $this->getShippedOrInvoiced($orderItem) >= $this->getOrdered($orderItem);
    }

    /**
     * @param Mage_Sales_Model_Order_Item $orderItem
     * @return float
     */
    protected function getOrdered(Mage_Sales_Model_Order_Item $orderItem)
    {
        return (float)$orderItem->getQtyOrdered();
    }

    private function getSku(Mage_Sales_Model_Order_Item $orderItem)
    {
        return $this->getProductMapper()->map_sku($orderItem->getSku());
    }

    /**
     * @return Selveo_Integration_Model_Mappers_Product
     */
    private function getProductMapper()
    {
        return Mage::getSingleton('selveointegration/mappers_product');
    }

    /**
     * @return mixed
     */
    private function getRequestBody()
    {
        return $this->requestBody;
    }
}