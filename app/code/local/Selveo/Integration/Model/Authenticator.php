<?php

use Selveo_Integration_Model_Exception_UnauthorizedException as UnauthorizedException;

class Selveo_Integration_Model_Authenticator
{
    protected $configHelper;

    public function authorize(Mage_Core_Controller_Request_Http $request) {

        $timestamp = $request->getHeader('X-REQUEST-TIMESTAMP');
        $this->validateTimestamp($timestamp);

        $hmacHeader = $request->getHeader('X-HMAC');

        $hmac = $this->getHmac($timestamp);

        $this->validateHmac($hmacHeader, $hmac);

    }

    private function getEncryption() {
        return hash('sha256', $this->getConfigHelper()->getKey());
    }

    private function getHmac($timestamp) {
        return hash_hmac('sha256', $timestamp, $this->getEncryption());
    }

    private function validateTimestamp($timestamp) {

        $now = new Zend_Date(gmdate('c'));
        $nowClone = clone $now;

        $timestamp = new Zend_Date($timestamp);
        
        if($timestamp->isLater($now->subSecond(30)) && $timestamp->isEarlier($nowClone->addSecond(30))) {
            return;
        }

        throw new UnauthorizedException("Request is not inside time window");

    }

    private function validateHmac($hmac, $configHmac) {

        if($hmac === $configHmac) {
            return;
        }

        throw new UnauthorizedException("Invalid hash");

    }

    /**
     * @return Selveo_Integration_Helper_Config
     */
    private function getConfigHelper() {
        return Mage::helper('selveointegration/config');
    }

}