<?php

use Selveo_Integration_Model_Job as DatabaseJob;

class Selveo_Integration_Model_Cron_Jobs
{

    const MAX_JOB_CHUNK_SIZE          = 500;
    const RESERVATION_TIMEOUT_MINUTES = 8;

    public function handlePending() {

        if(!(bool) $this->getConfigHelper()->getModuleEnabled()) {
            return;
        }

        /** @var Mage_Core_Model_Date $date */
        $date = Mage::getModel('core/date');
        /** @var Mage_Core_Model_Mysql4_Collection_Abstract $jobs */
        $jobs = Mage::getModel('selveointegration/job')
            ->getCollection()
            ->addFieldToFilter('retry_at', array('lteq' => $this->getDateHelper()->now()))
            ->addFieldToFilter('state', array('nin' => [DatabaseJob::STATE_FATAL, DatabaseJob::STATE_SUCCESSFUL]))
            ->addFieldToFilter('reserved_at', array(
                array('null' => true),
                array('lteq' => $date->gmtDate(null, strtotime(sprintf('-%d minutes', 8))))
            ))
            ->addOrder('last_try_at', Mage_Core_Model_Mysql4_Collection_Abstract::SORT_ORDER_ASC);

        $jobs->getSelect()->limit(self::MAX_JOB_CHUNK_SIZE);
        $jobs->walk('reserve');

        Selveo_Integration_Model_Job_Handler::instance()->setTimeout(45)->handlePendingJobs($jobs);

    }

    /**
     * @return Selveo_Integration_Helper_Date
     */
    private function getDateHelper() {
        return Mage::helper('selveointegration/date');
    }

    /**
     * @return Selveo_Integration_Helper_Config
     */
    private function getConfigHelper()
    {
        return Mage::helper('selveointegration/config');
    }

}