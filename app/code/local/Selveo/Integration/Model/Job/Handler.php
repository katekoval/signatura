<?php

use Selveo_Integration_Model_Interfaces_Job as JobInterface;
use Selveo_Integration_Model_Job as DatabaseJob;

class Selveo_Integration_Model_Job_Handler
{
    const MODE_SYNC          = 'sync';
    const MODE_ASYNC         = 'async';

    protected $mode = self::MODE_SYNC;

    protected $timeout = null;

    /**
     * @return Selveo_Integration_Model_Job_Handler
     */
    public static function instance() {
        return Mage::getSingleton('selveointegration/job_handler');
    }

    /**
     * @return Selveo_Integration_Model_Job_Db_Handler
     */
    protected function getDbHandler()
    {
        return Mage::getSingleton('selveointegration/job_db_handler');
    }

    /**
     * @param $job
     * @throws Zend_Date_Exception
     * @throws Zend_Db_Adapter_Exception
     * @throws Exception
     */
    public function queue($job) {

        $this->addJob($job);

        if ($this->mode === self::MODE_SYNC) {
            $this->flushQueue();
        }

    }

    public function setMode($newMode) {

        $this->mode = $newMode;
        return $this;

    }

    /**
     * @throws Exception
     */
    public function flushQueue() {
        $this->getDbHandler()->flush();
    }

    /**
     * @param Selveo_Integration_Model_Interfaces_Job $job
     * @return bool
     * @throws Exception
     */
    public function handleNow(JobInterface $job) {
        return $this->getDbHandler()->handleNow($job);
    }

    /**
     * @param null $secs
     * @return $this
     */
    public function setTimeout($secs = null)
    {
        $this->timeout = $secs;
        return $this;
    }

    public function getTimeout()
    {
        if($this->timeout) {
            return $this->timeout;
        }

        if (Mage::app()->getStore()->isAdmin()) {
            return 60;
        }

        // Frontend timeout
        return 1.5;
    }

    /**
     * @param $jobs
     * @throws Exception
     */
    public function handlePendingJobs($jobs) {

        $this->log('Handle pending jobs');

        /** @var DatabaseJob $job */
        foreach ($jobs as $job) {
            $this->queue($job);
        }

    }

    /**
     * @param bool $throwExceptions
     * @return $this
     */
    public function setThrowExceptions($throwExceptions) {

        $this->getDbHandler()->setThrowExceptions($throwExceptions);
        return $this;

    }

    /**
     * @param string $message
     */
    private function log($message) {
        Mage::log($message, null, 'selveointegration_job_handling.log', true);
    }

    /**
     * @param JobInterface|DatabaseJob $job
     * @throws Zend_Date_Exception
     */
    private function addJob($job)
    {
        $this->getDbHandler()->add($job);
    }

}