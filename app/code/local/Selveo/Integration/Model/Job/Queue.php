<?php

use Selveo_Integration_Helper_Date as DateHelper;
use Selveo_Integration_Model_Interfaces_DelaysJob as DelaysJob;
use Selveo_Integration_Model_Interfaces_Job as JobInterface;
use Selveo_Integration_Model_Job as DatabaseJob;

class Selveo_Integration_Model_Job_Queue implements Selveo_Integration_Model_Interfaces_Job_Queue
{
    protected $lastModelIds = array();

    /**
     * Get the database job
     * @param Selveo_Integration_Model_Abstract_Job $job
     * @return DatabaseJob|null
     */
    public function get(Selveo_Integration_Model_Abstract_Job $job)
    {
        $jobKey = $this->getJobKey($job);

        /** @var DatabaseJob $dbJob */
        $dbJob = (new DatabaseJob())->load($this->getLastModelId($jobKey));

        if (!$dbJob->getId()) {
            $this->log('Failed to handle job with identifier: ' . $jobKey);
            return null;
        }

        return $dbJob->setLastTryAt($this->getDateHelper()->now())
            ->setReservedAt(null)
            ->incrementTries();
    }

    /**
     * Save a job to the database
     * @param Selveo_Integration_Model_Interfaces_Job $job
     * @return string Serialized
     */
    public function save($job)
    {
        $jobKey = $this->getJobKey($job);
        $dbJob = null;

        /** When added by cron */
        if ($job instanceof DatabaseJob) {
            $dbJob = $job;
        }

        /** When added in realtime */
        if ($job instanceof JobInterface) {
            $dbJob = $this->wrapJob($job);

            if (!$this->getLastModelId($jobKey)) {
                $this->log('save db job: ' . $jobKey);
                $this->saveDatabaseJob($dbJob, $this->getHighestDelay($job));
            }

        }

        if(is_null($dbJob)){
            $this->log("Hm. dbjob was null");
            return;
        }

        if (($lastModelId = $dbJob->getId()) != 0) {
            $this->setLastId($lastModelId, $jobKey);
        }

    }

    private function getJobKey($job)
    {
        if ($job instanceof DatabaseJob) {
            $job = $job->getJob();
        }

        return $job->getKey();
    }

    /**
     * @param Selveo_Integration_Model_Interfaces_Job $job
     * @return DatabaseJob
     */
    private function wrapJob(JobInterface $job)
    {
        if ($job instanceof DatabaseJob) {
            return $job;
        }

        return DatabaseJob::wrap($job);
    }

    /**
     * @return Selveo_Integration_Helper_Date
     */
    private function getDateHelper()
    {
        return Mage::helper('selveointegration/date');
    }

    /**
     * @param Selveo_Integration_Model_Job $dbJob
     * @param int $delay
     * @return Selveo_Integration_Model_Job
     * @throws Zend_Date_Exception
     */
    public function saveDatabaseJob(DatabaseJob $dbJob, $delay)
    {
        $retryTime = $this->getDateHelper()
            ->zend()
            ->addSecond($delay);

        $dbJob
            ->setSerialized($dbJob->serializeJob())
            ->setRetryAt(DateHelper::toStrfTime($retryTime))
            ->setState(DatabaseJob::STATE_PENDING)
            ->setReservedAt(null)
            ->save();

        $this->log('saved db job: '.$dbJob->getId());

        return $dbJob;

    }


    /**
     * @param JobInterface|null $job
     * @param Exception|null $e
     * @return int
     */
    private function getHighestDelay($job = null, $e = null)
    {
        $delays = [300]; //delay with a minimum of 5 min. for the request to handle the job, before the cron handles it

        if ($job instanceof DelaysJob) {
            $delays[] = $job->getDelay();
        }

        if ($e instanceof DelaysJob) {
            $delays[] = $e->getDelay();
        }

        return max($delays);
    }

    private function getLastModelId($id)
    {
        return $this->lastModelIds[$id];
    }

    private function setLastId($lastModelId, $jobKey)
    {
        $this->log('set last model id: ' . $lastModelId . ' for jobKey: ' . $jobKey);
        $this->lastModelIds[$jobKey] = $lastModelId;
    }

    /**
     * @param string $message
     */
    private function log($message)
    {
        Mage::log($message, null, 'selveointegration_job_handling.log', true);
    }

}