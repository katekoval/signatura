<?php

use Selveo_Integration_Model_Interfaces_Job as JobInterface;
use Selveo_Integration_Model_Job as DatabaseJob;

class Selveo_Integration_Model_Job_Db_Handler implements Selveo_Integration_Model_Interfaces_Job_Db_Handler
{

    protected $queue = array();
    protected $flushing = false;

    protected $timeout = null;
    protected $throwExceptions = false;

    /**
     * Add a job to the queue
     * @param DatabaseJob|JobInterface $job
     * @return void
     */
    public function add($job)
    {
        $jobKey = $this->getJobKey($job);

        $this->getQueue()->save($job);

        $this->queue[$jobKey] = $this->serialize($job);

        $this->log(json_encode($jobKey) . ' was queued');

    }

    /**
     * Handle the DB job
     * @param JobInterface $job
     * @return bool
     * @throws Exception
     */
    public function handleNow(JobInterface $job)
    {
        try {

            $this->log("handling " . get_class($job));
            $job->handle();

            return true;

        } catch (\Exception $e) {

            Mage::logException($e);

            if ($this->throwExceptions) {
                throw $e;
            }

            return false;

        }
    }

    /**
     * Flush the queue so the jobs can be synced to Selveo
     * @return void
     * @throws Exception
     */
    public function flush()
    {
        if ($this->flushing) {
            return;
        }

        $this->flushing = true;

        /**
         * Prioritise the jobs - jobs with a low value is queued first
         */
        uasort($this->queue, function ($jobA, $jobB) {
            return $jobA::PRIORITY - $jobB::PRIORITY;
        });

        /** @var Selveo_Integration_Model_Abstract_Job $job */
        foreach ($this->queue as $jobKey => $job) {

            $this->log(json_encode($jobKey) . ' is handling');

            $this->handleQueued(unserialize($job));

            $this->log(json_encode($jobKey) . ' was handled');

            unset($this->queue[$jobKey]);
        }

        $this->flushing = false;

    }

    /**
     * @param Selveo_Integration_Model_Abstract_Job $job
     * @return DatabaseJob|void
     * @throws Exception
     */
    private function handleQueued(Selveo_Integration_Model_Abstract_Job $job)
    {
        /** @var DatabaseJob $dbJob */
        if(($dbJob = $this->getQueue()->get($job)) === null) {
            return;
        }

        if ($this->handleNow($job)) {
            return $dbJob->success();
        }

        return $dbJob->save();
    }

    private function getJobKey($job)
    {
        if ($job instanceof DatabaseJob) {
            $job = $job->getJob();
        }

        return $job->getKey();
    }

    /**
     * @param string $message
     */
    private function log($message)
    {
        Mage::log($message, null, 'selveointegration_job_handling.log', true);
    }

    /**
     * Add a job to the queue
     * @param DatabaseJob|JobInterface $job
     * @return string
     */
    private function serialize($job)
    {
        if ($job instanceof DatabaseJob) {
            return $job->getSerializedJob();
        }

        if ($job instanceof JobInterface) {
            return serialize($job);
        }

        return null;
    }

    /**
     * @param bool $throwExceptions
     * @return Selveo_Integration_Model_Job_Db_Handler
     */
    public function setThrowExceptions($throwExceptions)
    {
        $this->throwExceptions = $throwExceptions;
        return $this;
    }

    /**
     * @return Selveo_Integration_Model_Job_Queue
     */
    private function getQueue()
    {
        return Mage::getSingleton('selveointegration/job_queue');
    }
}