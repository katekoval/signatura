<?php

trait Selveo_Integration_Model_Internal_RouterTrait
{

    /**
     * @param Mage_Core_Controller_Request_Http $request
     * @return Selveo_Integration_Model_Internal_Route
     */
    public function route()
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return new Selveo_Integration_Model_Internal_Route($this->getRequest());
    }

}