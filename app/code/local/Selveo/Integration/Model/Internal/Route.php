<?php

class Selveo_Integration_Model_Internal_Route
{
    /** @var Mage_Core_Controller_Request_Http */
    protected $request;
    protected $responders;

    /**
     * @param $request
     */
    public function __construct(Mage_Core_Controller_Request_Http $request)
    {
        $this->request = $request;
    }

    public function get($responder)
    {
        $this->responders['get'] = $responder;
    }

    public function put($responder)
    {
        $this->responders['put'] = $responder;
    }

    public function delete($responder)
    {
        $this->responders['delete'] = $responder;
    }

    /**
     * @return mixed
     * @throws Selveo_Integration_Model_Exception_UnsupportedRequestMethodException
     */
    public function run()
    {
        $method = strtolower($this->getRequest()->getMethod());

        if(!key_exists($method, $this->responders) || !is_callable($this->responders[$method])) {
            throw new Selveo_Integration_Model_Exception_UnsupportedRequestMethodException("Unsupported request method: ".$method);
        }

        return call_user_func($this->responders[$method], $this->getRequest());

    }

    /**
     * @return Mage_Core_Controller_Request_Http
     */
    public function getRequest()
    {
        return $this->request;
    }
}