<?php

trait Selveo_Integration_Model_FlagsModels
{
    protected static $synced = [];

    public function flagModelAsSynced(Mage_Core_Model_Abstract $model) {

        static::$synced[get_class($model)][] = $model->getId();
        $model->setData('_selveo_synced', true);

    }

    public function modelWasSynced(Mage_Core_Model_Abstract $model) {

        return (isset(static::$synced[get_class($model)])
                && in_array($model->getId(), static::$synced[get_class($model)]))
            || $model->getData('_selveo_synced') === true;

    }
}