<?php

class Selveo_Integration_Model_Signer
{
    private $secret;

    public function __construct($secret) {
        $this->secret = $secret;
    }

    public function sign($timestamp) {
        return hash_hmac('sha256', $timestamp, $this->secret);
    }
}