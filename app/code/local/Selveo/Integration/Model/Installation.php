<?php

class Selveo_Integration_Model_Installation implements Selveo_Integration_Model_Interfaces_Installation
{

    protected $apiKey;
    protected $secret;
    protected $endpoint;
    protected $friendlyName;
    protected $context = [];

    /**
     * Selveo_Integration_Model_Installation constructor.
     * @param string $apiKey
     * @param string $secret
     * @param string $endpoint
     */
    public function __construct($apiKey, $secret, $endpoint) {

        $this->apiKey   = $apiKey;
        $this->secret   = $secret;
        $this->endpoint = $endpoint;

    }

    /**
     * @return mixed
     */
    public function getFriendlyName() {
        return $this->friendlyName;
    }

    /**
     * @param mixed $friendlyName
     * @return Selveo_Integration_Model_Installation
     */
    public function setFriendlyName($friendlyName) {

        $this->friendlyName = $friendlyName;
        return $this;

    }


    public function getUniqueId() {
        return sha1($this->getApiKey() . $this->getSecret() . $this->getEndpoint());
    }

    /**
     * @return string
     */
    public function getApiKey() {
        return $this->apiKey;
    }

    /**
     * @return string
     */
    public function getSecret() {
        return $this->secret;
    }

    /**
     * @return string
     */
    public function getEndpoint() {
        return $this->endpoint;
    }

    /**
     * @return Selveo_Integration_Model_Clients_Zend_Http_Client
     * @throws Zend_Http_Client_Exception
     */
    public function getClient() {

        /** @var Selveo_Integration_Model_Clients_Zend_Http_Client $client */
        $client = Mage::getModel('selveointegration/clients_zend_http_client');

        return $client
            ->setKey($this->getApiKey())
            ->setSecret($this->getSecret())
            ->setSlug($this->getEndpoint())
            ->setTimeout(Selveo_Integration_Model_Job_Handler::instance()->getTimeout());

    }

    /**
     * @param array $context
     * @return Selveo_Integration_Model_Installation
     */
    public function addContext($context) {

        if(in_array($context, $this->context)){
            return $this;
        }

        $this->context[] = $context;

        return $this;

    }

    /**
     * @return array
     */
    public function getContext() {
        return $this->context;
    }

    public function isConfigured()
    {
        return $this->getApiKey() && $this->getSecret() && $this->getEndpoint();
    }

}