<?php

interface Selveo_Integration_Model_Interfaces_DelaysJob
{

    /**
     * The delay before the Job should be executed
     * @return int
     */
    public function getDelay();

}