<?php

interface Selveo_Integration_Model_Interfaces_Emulator {

    /**
     * @param $callback
     * @return callable
     * @throws Exception
     */
    public function onAdmin($callback);

    /**
     * @param $callback
     * @param $storeId
     * @return callable
     * @throws Exception
     */
    public function onStore($callback, $storeId);

}