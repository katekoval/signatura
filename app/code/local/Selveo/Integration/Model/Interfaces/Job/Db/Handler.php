<?php

use Selveo_Integration_Model_Interfaces_Job as JobInterface;
use Selveo_Integration_Model_Job as DatabaseJob;

interface Selveo_Integration_Model_Interfaces_Job_Db_Handler
{
    /**
     * Add a job to the queue
     * @param DatabaseJob|JobInterface $job
     * @return void
     */
    public function add($job);

    /**
     * Handle the DB job
     * @param Selveo_Integration_Model_Interfaces_Job $job
     * @return bool
     * @throws Exception
     */
    public function handleNow(Selveo_Integration_Model_Interfaces_Job $job);

    /**
     * Flush the queue so the jobs can be synced to Selveo
     * @return void
     */
    public function flush();

}