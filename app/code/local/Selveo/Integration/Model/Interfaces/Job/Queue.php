<?php

use Selveo_Integration_Model_Interfaces_Job as JobInterface;
use Selveo_Integration_Model_Job as DatabaseJob;

interface Selveo_Integration_Model_Interfaces_Job_Queue
{
    /**
     * Save a job to the database
     * @param Selveo_Integration_Model_Interfaces_Job $job
     * @return string Serialized
     */
    public function save($job);

    /**
     * Get the database job
     * @param Selveo_Integration_Model_Abstract_Job $job
     * @return DatabaseJob|null
     */
    public function get(Selveo_Integration_Model_Abstract_Job $job);
}