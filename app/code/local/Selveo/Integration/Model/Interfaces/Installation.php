<?php

interface Selveo_Integration_Model_Interfaces_Installation
{

    public function getUniqueId();

    public function getClient();

}