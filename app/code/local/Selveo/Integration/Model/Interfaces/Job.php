<?php

interface Selveo_Integration_Model_Interfaces_Job
{

    /**
     * @return mixed
     * Handle the job
     */
    public function handle();

    /**
     * The id for the queue to handle the job
     * @return string
     */
    public function getKey();

}