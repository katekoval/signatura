<?php

class Selveo_Integration_Model_Exception_Client_PartialSyncFailure extends Selveo_Integration_Model_Exception_Base
{

    private $failed;

    protected $ids = [];

    public function __construct($message = "", $code = 0, Throwable $previous = null, $failed = null) {

        parent::__construct($message, $code, $previous);
        $this->failed = $failed;
    }

    public function getFailed() {
        return $this->failed;
    }

    /**
     * @param array $ids
     * @return Selveo_Integration_Model_Exception_Client_PartialSyncFailure
     */
    public function setIds($ids) {
        $this->ids = $ids;
        return $this;
    }

    /**
     * @return array
     */
    public function getIds() {
        return $this->ids;
    }

}