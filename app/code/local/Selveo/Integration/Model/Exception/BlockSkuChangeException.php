<?php

class Selveo_Integration_Model_Exception_BlockSkuChangeException extends Selveo_Integration_Model_Exception_Base
{

    /**
     * Selveo_Integration_Model_Exception_BlockSkuChangeException constructor.
     * @param $product
     */
    public function __construct(Mage_Catalog_Model_Product $product) {

        Mage::getSingleton('selveointegration/session')->adminhtmlError(
            "SKU change for the product has been blocked. The product keeps the SKU: '%s'. <br>
            The setting for unblocking SKU change is found here: 'System => Configuration => Selveo integration => Advanced configuration => Block product sku changes'",
            $product->getOrigData('sku')
        );

    }

}