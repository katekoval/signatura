<?php

use Selveo_Integration_Model_Clients_Zend_Http_Client as Client;
use Zend_Http_Client_Adapter_Exception as ClientException;
use Selveo_Integration_Model_Exception_SchemaConstraintException as SchemaConstraintException;
use Selveo_Integration_Model_Exception_ResponseErrorException as ErrorResponseException;
use Selveo_Integration_Model_Exception_Client_ServerNotFoundException as ServerNotFoundException;
use Mage_Catalog_Model_Product as Product;

class Selveo_Integration_Model_Selveo_Api_Products
{

    protected $client;

    public function __construct(Client $client) {
        $this->client = $client;
    }

    public function store($productCollection) {

        try {

            $mappedProducts = $this->map($productCollection);

            if (count($mappedProducts) === 0) {
                $this->getSession()->addNotice("Nothing was synchronized to SELVEO; the product(s) does not have any sub-products");
                return;
            }

            $this->getClient()->post('catalog/products', $mappedProducts);

        } catch (Exception $e) {

            $this->informSyncError($e);
            throw $e;

        }

    }

    private function map($productCollection) {

        $products     = array();
        $productNames = array();

        /** @var Mage_Catalog_Model_Product $product */
        foreach ($productCollection as $product) {

            $bundledProducts = $this->getProductNormalizer()
                ->normalize($product);

            /** @var Mage_Catalog_Model_Product $bundledProduct */
            foreach ($bundledProducts as $bundledProduct) {

                if (isset($products[$bundledProduct->getId()])) {
                    continue;
                }

                $products[$bundledProduct->getId()]     = $this->getMapper()->map($bundledProduct, $bundledProduct->getData());
                $productNames[$bundledProduct->getId()] = $bundledProduct->getName();

            }

        }

        if (count($productNames) > 0 && Mage::registry('without_product_success') !== true) {
            $this->getSession()->addSuccess(implode(', ', $productNames) . ' has been synced to Selveo');
        }

        return array_values($products);

    }

    /**
     * @return Client
     */
    public function getClient() {
        return $this->client;
    }

    private function informSyncError($e = null) {
        $this->getLogHelper()->logWithException("Error syncing products to Selveo. The products will be synced by cron.",
            $e);
    }

    /**
     * @return Selveo_Integration_Helper_Log
     */
    private function getLogHelper() {
        return Mage::helper('selveointegration/log');
    }

    /**
     * @return Selveo_Integration_Model_Mappers_Product
     */
    private function getMapper() {
        return Mage::getSingleton('selveointegration/mappers_product');
    }

    /**
     * @return Selveo_Integration_Model_Product_Normalizer
     */
    private function getProductNormalizer() {
        return Mage::getSingleton('selveointegration/product_normalizer');
    }

    /**
     * @return Mage_Adminhtml_Model_Session
     */
    private function getSession() {
        return Mage::getSingleton('adminhtml/session');
    }

}