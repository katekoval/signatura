<?php

use Selveo_Integration_Model_Clients_Zend_Http_Client as Client;

class Selveo_Integration_Model_Selveo_Api_Order_Status {

    protected $client;

    public function __construct(Client $client) {
        $this->client = $client;
    }

    public function getArrayCollection() {

        try {

            $statuses = $this->getClient()
                            ->get('sales/orders/configuration/statuses')
                            ->json();

            return array_map(function($status) {

                return [
                    'label' => $status->name,
                    'value' => $status->code
                ];

            }, $statuses->data);

        } catch (Selveo_Integration_Model_Exception_Base $e) {

            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            return [];

        } catch (Zend_Http_Client_Exception $e) {

            Mage::getSingleton('adminhtml/session')->addError('Client error: '.$e->getMessage());
            return [];

        }

    }

    /**
     * @return Selveo_Integration_Model_Clients_Zend_Http_Client
     */
    public function getClient() {
        return $this->client;
    }

}