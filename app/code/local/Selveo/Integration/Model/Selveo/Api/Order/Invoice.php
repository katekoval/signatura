<?php

use Selveo_Integration_Model_Clients_Zend_Http_Client as Client;
use Selveo_Integration_Model_Exception_Client_ErrorResponse as ErrorResponseException;
use Selveo_Integration_Model_Exception_Client_ServerNotFoundException as ServerNotFoundException;
use Selveo_Integration_Model_Exception_SchemaConstraintException as SchemaConstraintException;
use Zend_Http_Client_Adapter_Exception as ClientException;

class Selveo_Integration_Model_Selveo_Api_Order_Invoice
{

    /**
     * @var Selveo_Integration_Model_Clients_Zend_Http_Client
     */
    private $client;

    public function __construct(Client $client) {
        $this->client = $client;
    }

    public function store(Mage_Sales_Model_Order_Invoice $invoice)
    {
        try {

            if (!$this->getSyncedHelper()->isSynced($invoice)) {

                $response = $this->getClient()->post('sales/invoices', $this->map($invoice));
                $this->getSyncedHelper()->set($invoice, $response);
                return;
            }

            $response = $this->getClient()->put('sales/invoices/' . $invoice->getId(), $this->map($invoice));
            $this->getSyncedHelper()->set($invoice, $response);

        } catch (ServerNotFoundException $e) {

            $this->informSyncError($e);
            throw $e;

        } catch (ErrorResponseException $e) {

            $this->informSyncError($e);
            throw $e;

        } catch (ClientException $e) {

            $this->informSyncError($e);
            throw $e;

        } catch (SchemaConstraintException $e) {

            $this->informSyncError($e);
            throw $e;

        } catch (Exception $e) {

            $this->informSyncError($e);
            throw $e;

        }

    }

    private function informSyncError($e = null) {
        $this->getLogHelper()->logWithException("Error syncing invoices to Selveo. The invoices will be synced by cron.", $e);
    }

    /**
     * @return Selveo_Integration_Helper_Log
     */
    private function getLogHelper() {
        return Mage::helper('selveointegration/log');
    }

    /**
     * @return Selveo_Integration_Model_Clients_Zend_Http_Client
     */
    public function getClient() {
        return $this->client;
    }

    private function map($invoice)
    {
        return $this->getMapper()->map($invoice, $invoice->getData());
    }

    /**
     * @return Selveo_Integration_Model_Mappers_Order_Invoice
     */
    private function getMapper() {
        return Mage::getModel('selveointegration/mappers_order_invoice');
    }

    /**
     * @return Selveo_Integration_Helper_Synced
     */
    private function getSyncedHelper()
    {
        return Mage::helper('selveointegration/synced');
    }

}