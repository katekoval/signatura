<?php

use Selveo_Integration_Model_Clients_Zend_Http_Client as Client;

class Selveo_Integration_Model_Selveo_Api_Orders
{

    /**
     * @var Selveo_Integration_Model_Clients_Zend_Http_Client
     */
    private $client;

    public function __construct(Client $client) {
        $this->client = $client;
    }

    /**
     * @param Mage_Sales_Model_Mysql4_Order_Collection $orderCollection
     * @throws Exception
     */
    public function store($orderCollection) {

        try {

            $this->getClient()->post('sales/orders', $this->map($orderCollection));

            $this->bulkUpdateSelveoSynced($orderCollection);

        } catch (Selveo_Integration_Model_Exception_Base $e) {

            $this->informSyncError($e, $orderCollection);
            throw $e;

        } catch (Exception $e) {

            $this->informSyncError($e, $orderCollection);
            throw $e;

        }

    }

    private function map($orderCollection) {

        $orders = array();

        foreach ($orderCollection as $order) {
            $orders[] = $this->getMapper()->map($order);
        }

        return $orders;

    }

    private function bulkUpdateSelveoSynced($orderCollection) {

        $now = $this->getDateHelper()->now();

        /** @var Mage_Sales_Model_Order $order */
        foreach ($orderCollection as $order) {

            $order->setSelveoSynced($now);
            $order->getResource()->saveAttribute($order, 'selveo_synced');

        }

    }

    /**
     * @return Selveo_Integration_Helper_Date
     */
    private function getDateHelper() {
        return Mage::helper('selveointegration/date');
    }

    private function informSyncError($e = null, $orderCollection = null) {
        $incrementIds = [];

        foreach($orderCollection ?: [] as $order){
            $incrementIds[] = $order->getIncrementId();
        }

        $this->getLogHelper()->logWithException(
            sprintf("Error syncing orders to Selveo. The orders (%s) will be synced by cron.", join(", ", $incrementIds))
            , $e);
    }

    /**
     * @return Selveo_Integration_Helper_Log
     */
    private function getLogHelper() {
        return Mage::helper('selveointegration/log');
    }

    /**
     * @return Selveo_Integration_Model_Mappers_Order
     */
    private function getMapper() {
        return Mage::getModel('selveointegration/mappers_order');
    }

    /**
     * @return Selveo_Integration_Model_Clients_Zend_Http_Client
     */
    public function getClient() {
        return $this->client;
    }

}