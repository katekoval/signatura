<?php

class Selveo_Integration_Model_Selveo_Api_Preflight_Invalidate
{
    /**
     * @var Selveo_Integration_Model_Clients_Zend_Http_Client
     */
    private $client;

    public function __construct(Selveo_Integration_Model_Clients_Zend_Http_Client $client)
    {
        $this->client = $client;
    }

    /**
     * @throws Selveo_Integration_Model_Exception_Client_ErrorResponse
     * @throws Selveo_Integration_Model_Exception_Client_PartialSyncFailure
     * @throws Selveo_Integration_Model_Exception_Client_ServerNotFoundException
     * @throws Selveo_Integration_Model_Exception_Client_UnauthorizedException
     * @throws Selveo_Integration_Model_Exception_SchemaConstraintException
     * @throws Zend_Http_Client_Exception
     * @throws Zend_Http_Exception
     * @throws Zend_Uri_Exception
     */
    public function destroy()
    {
        $this->getClient()->post('integrations/invalidate-preflight', []);
    }

    /**
     * @return Selveo_Integration_Model_Clients_Zend_Http_Client
     */
    public function getClient()
    {
        return $this->client;
    }
}