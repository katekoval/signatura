<?php

class Selveo_Integration_Model_Selveo_Api_Images
{

    /**
     * @var Selveo_Integration_Model_Clients_Zend_Http_Client
     */
    private $client;

    public function __construct(Selveo_Integration_Model_Clients_Zend_Http_Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @param Varien_Data_Collection $productImageCollection
     * @throws Selveo_Integration_Model_Exception_Client_ErrorResponse
     * @throws Selveo_Integration_Model_Exception_Client_PartialSyncFailure
     * @throws Selveo_Integration_Model_Exception_Client_ServerNotFoundException
     * @throws Selveo_Integration_Model_Exception_Client_UnauthorizedException
     * @throws Selveo_Integration_Model_Exception_SchemaConstraintException
     * @throws Zend_Http_Client_Exception
     * @throws Zend_Http_Exception
     * @throws Zend_Uri_Exception
     */
    public function store(Mage_Catalog_Model_Product $product, Varien_Data_Collection $productImageCollection)
    {

        $mappedImages  = [];

        /** @var Varien_Object $image */
        foreach ($productImageCollection as $image) {
            $mappedImages[] = $this->getMapper()->map($image);
        }

        $sku = urlencode($this->getMappedSku($product));

        $this->getClient()->post("catalog/products/{$sku}/images", $mappedImages);

    }

    public function destroy(Mage_Catalog_Model_Product $product, array $imageIdsToDelete)
    {
        $sku = urlencode($this->getMappedSku($product));

        foreach ($imageIdsToDelete as $imageId) {
            $this->getClient()->delete("catalog/products/{$sku}/images/{$imageId}");
        }
    }

    /**
     * @return Selveo_Integration_Model_Mappers_Product_Image
     */
    private function getMapper()
    {
        return Mage::getSingleton('selveointegration/mappers_product_image');
    }

    /**
     * @return Selveo_Integration_Model_Clients_Zend_Http_Client
     */
    public function getClient()
    {
        return $this->client;
    }

    public function getMappedSku(Mage_Catalog_Model_Product $product)
    {
        return $this->getProductMapper()->map($product)['sku'];
    }

    /**
     * @return Selveo_Integration_Model_Mappers_Product
     */
    private function getProductMapper()
    {
        return Mage::getModel('selveointegration/mappers_product');
    }

}