<?php

class Selveo_Integration_Model_Selveo_Api_Product_Attributes
{
    /**
     * @var Selveo_Integration_Model_Clients_Zend_Http_Client
     */
    private $client;

    public function __construct(Selveo_Integration_Model_Clients_Zend_Http_Client $client)
    {
        $this->client = $client;
    }

    public function getArrayCollection()
    {
        try {

            $response = $this->client
                ->setParameterGet('per_page', 1)
                ->get('catalog/products')
                ->getBody();

            return $this->mapAttributes(json_decode($response, true));

        } catch (Selveo_Integration_Model_Exception_Base $e) {

            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            return [];

        } catch (Zend_Http_Client_Exception $e) {

            Mage::getSingleton('adminhtml/session')->addError('Client error: ' . $e->getMessage());
            return [];

        }

    }

    /**
     * @param $response
     * @return array
     */
    private function mapAttributes($response)
    {
        $attributes = [];

        foreach ($response['meta']['schema'] as $attribute) {

            if ($attribute['type'] === 'array') {
                continue;
            }

            $attributes[] = [
                'label' => $this->getAttributeName($attribute),
                'value' => $attribute['code']
            ];

        }

        return $attributes;

    }

    /**
     * @param $attribute
     * @return mixed
     */
    private function getAttributeName($attribute)
    {

        $additionalNaming = '';

        if(in_array('immutable', array_keys($attribute['constraints']))) {
            $additionalNaming = ' (Is immutable)';
        }

        if($attribute['calculated'] == 1) {
            $additionalNaming = ' (Is calculated)';
        }

        if($attribute['name']) {
            return $attribute['name'] . $additionalNaming;
        }

        return  $attribute['code'] . $additionalNaming;
    }

}