<?php

use Selveo_Integration_Model_SerializedCollection as SerializedCollection;
use Selveo_Integration_Model_SerializedModel as SerializedModel;

abstract class Selveo_Integration_Model_Abstract_Job implements Selveo_Integration_Model_Interfaces_Job
{

    const PRIORITY = 1;
    protected $id;

    /**
     * Runs when a job gets serialized
     * @return array
     * @throws ReflectionException
     */
    public function __sleep() {

        $reflection = new ReflectionClass($this);

        $serialize = array();

        foreach ($reflection->getProperties() as $property) {

            $property->setAccessible(true);
            $value = $property->getValue($this);

            $property->setValue($this, $this->serializeValue($value));

            $serialize[] = $property->getName();

        }

        return $serialize;

    }

    /**
     * Runs when a job gets unserialized
     * @throws ReflectionException
     */
    public function __wakeup() {

        $reflection = new ReflectionClass($this);

        foreach ($reflection->getProperties() as $property) {

            $property->setAccessible(true);
            $value = $property->getValue($this);

            $property->setValue($this, $this->unserializeValue($value));

        }

    }

    /**
     * @return null|int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param null|int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }


    /**
     * @param Selveo_Integration_Model_Installation $installation
     * @param iterable $collection
     * @return array
     */
    protected function getEntitiesToSync(Selveo_Integration_Model_Installation $installation, $collection) {

        $entitiesToSync = array();

        /** @var Mage_Catalog_Model_Product $entity */
        foreach ($collection as $entity) {

            if (in_array($entity->getId(), $installation->getContext())) { // Array of entity ID's to sync
                $entitiesToSync[] = $entity;
            }

        }

        return $entitiesToSync;

    }

    /**
     * @param Selveo_Integration_Model_Installation $installation
     */
    protected function successMessage(Selveo_Integration_Model_Installation $installation) {

        $this->getSession()->addSuccess(
            $this->getDataHelper()->__('Successfully synced to %s.selveo.com on view: %s',
                $installation->getEndpoint(),
                $installation->getFriendlyName() ? $installation->getFriendlyName() : $installation->getEndpoint()
            )
        );

    }

    protected function errorMessage(Selveo_Integration_Model_Installation $installation, Exception $e) {

        $this->getSession()->addError(
            $this->getDataHelper()->__('Unable to sync to %s: %s',
                $installation->getFriendlyName() ? $installation->getFriendlyName() : $installation->getEndpoint(),
                $e->getMessage())
        );

        throw $e;

    }

    /**
     * @return Selveo_Integration_Helper_Config
     */
    protected function getConfigHelper() {
        return Mage::helper('selveointegration/config');
    }

    /**
     * @return Selveo_Integration_Model_Clients_Zend_Http_Client
     */
    protected function getClient() {
        return Mage::getModel('selveointegration/clients_zend_http_client');
    }

    /**
     * @return Mage_Adminhtml_Model_Session
     */
    protected function getSession() {
        return Mage::getSingleton('adminhtml/session');
    }

    /**
     * @return Selveo_Integration_Helper_Data
     */
    protected function getDataHelper() {
        return Mage::helper('selveointegration');
    }


    protected function serializeValue($value) {

        if ($value instanceof Mage_Core_Model_Abstract) {
            return new SerializedModel($value);
        }

        if ($value instanceof Varien_Data_Collection_Db) {
            return new SerializedCollection($value);
        }

        if (is_array($value)) {

            foreach ($value as $key => $v) {
                $value[$key] = $this->serializeValue($v);
            }

            return $value;

        }

        return $value;

    }

    protected function unserializeValue($value) {

        if ($value instanceof SerializedModel) {
            return $value->getModel();
        }

        if($value instanceof SerializedCollection){
            return $value->getCollection();
        }

        if (is_array($value)) {

            foreach ($value as $key => $v) {
                $value[$key] = $this->unserializeValue($v);
            }

            return $value;

        }

        return $value;

    }

}