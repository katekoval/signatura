<?php

use Selveo_Integration_Model_Exception_OrderNotFound as OrderNotFoundException;

class Selveo_Integration_Model_Order extends Mage_Sales_Model_Order
{

    use Selveo_Integration_Model_FlagsModels;

    public function update($orderIncrementId, $requestBody) {

        $this->loadByIncrementId($orderIncrementId);

        if (!$this->getId()) {
            throw new OrderNotFoundException("Order #{$orderIncrementId} wasn't found");
        }

        $this->flagModelAsSynced($this);

        $fields = $this->getMapper()->map($this, $requestBody);

        if ($fields['state'] === self::STATE_CANCELED) {
            $this->cancel();
        }

        $this->setData(array_merge($this->getData(), $fields))
            ->save();

        $this->saveAddresses($fields);

        return true;

    }

    /**
     * @return Selveo_Integration_Model_Mappers_Magento_Order
     */
    public function getMapper() {
        return Mage::getModel('selveointegration/mappers_magento_order');
    }

    /**
     * @param $fields
     */
    protected function saveAddresses($fields) {

        $addressClass = get_class(Mage::getModel('sales/order_address'));

        if ($fields['billing_address'] instanceof $addressClass) {

            $this->flagModelAsSynced($fields['billing_address']->getOrder());
            $fields['billing_address']->save();

        }

        if ($fields['shipping_address'] instanceof $addressClass) {

            $this->flagModelAsSynced($fields['shipping_address']->getOrder());
            $fields['shipping_address']->save();

        }

    }

}