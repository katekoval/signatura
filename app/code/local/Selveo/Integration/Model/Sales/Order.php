<?php

class Selveo_Integration_Model_Sales_Order extends Mage_Sales_Model_Order
{
    /**
     * @param $store
     * @return Mage_Sales_Model_Entity_Order_Collection
     */
    public function getNotSyncedForStore($store) {

        return $this
                ->getCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('store_id', array('eq' => $store->getId()))
                ->addFieldToFilter('selveo_synced', array('null' => true));


    }

    /**
     * @param $orderIds
     * @return Mage_Sales_Model_Entity_Order_Collection
     */
    public function loadByIds($orderIds) {

        return $this
                ->getCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('entity_id', array('in' => $orderIds));

    }
}