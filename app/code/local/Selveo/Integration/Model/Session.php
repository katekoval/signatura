<?php

class Selveo_Integration_Model_Session
{

    public function adminhtmlSuccess() {
        $this->getAdminhtmlSession()->addSuccess($this->translate(func_get_args()));
    }

    public function adminhtmlError() {
        $this->getAdminhtmlSession()->addError($this->translate(func_get_args()));
    }

    public function adminhtmlWarning() {
        $this->getAdminhtmlSession()->addWarning($this->translate(func_get_args()));
    }

    /**
     * @return string
     */
    private function translate($arguments) {
        return call_user_func_array([$this->getDataHelper(), '__'],$arguments);
    }

    /**
     * @return Selveo_Integration_Helper_Data
     */
    private function getDataHelper() {
        return Mage::helper('selveointegration');
    }

    /**
     * @return Mage_Adminhtml_Model_Session
     */
    private function getAdminhtmlSession() {
        return Mage::getSingleton('adminhtml/session');
    }

}