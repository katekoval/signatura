<?php

use Selveo_Integration_Model_Clients_Zend_Http_Response as Selveointegration_Response;

class Selveo_Integration_Model_Clients_Zend_Http_Client extends Zend_Http_Client {

    protected $configHelper;
    protected $logHelper;

    protected $key;
    protected $secret;
    protected $slug;

    const PROTOCOL = 'https://';
    const DOMAIN = '.selveo.com';

    public function __construct() {

        parent::__construct(null, null);

    }

    public function generateSignature(array $body) {

        $signer = Mage::getModel('selveointegration/signer', $this->getSecret());

        $timestamp = gmdate('c');

        $this->setHeaders(array(
            'X-API-KEY' => $this->getKey(),
            'X-HMAC' => $signer->sign($timestamp),
            'X-REQUEST-TIMESTAMP' => $timestamp
        ));

        return $this;

    }

    /**
     * The SELVEO API will then not throw errors a 422 if a schema constraint fails
     */
    public function addGracefulFailsHeader() {

        $this->setHeaders('X-GRACEFUL-FAILS', 1);
        return $this;

    }

    /**
     * @param $endpoint
     * @return Selveo_Integration_Model_Clients_Zend_Http_Response|Zend_Http_Response
     * @throws Selveo_Integration_Model_Exception_Client_ErrorResponse
     * @throws Selveo_Integration_Model_Exception_Client_PartialSyncFailure
     * @throws Selveo_Integration_Model_Exception_Client_ServerNotFoundException
     * @throws Selveo_Integration_Model_Exception_Client_UnauthorizedException
     * @throws Selveo_Integration_Model_Exception_SchemaConstraintException
     * @throws Zend_Http_Client_Exception
     * @throws Zend_Http_Exception
     * @throws Zend_Uri_Exception
     */
    public function get($endpoint) {
        return $this->doRequest($endpoint, self::GET);
    }

    /**
     * @param $endpoint
     * @param array $data
     * @return Selveo_Integration_Model_Clients_Zend_Http_Response|Zend_Http_Response
     * @throws Selveo_Integration_Model_Exception_Client_ErrorResponse
     * @throws Selveo_Integration_Model_Exception_Client_PartialSyncFailure
     * @throws Selveo_Integration_Model_Exception_Client_ServerNotFoundException
     * @throws Selveo_Integration_Model_Exception_Client_UnauthorizedException
     * @throws Selveo_Integration_Model_Exception_SchemaConstraintException
     * @throws Zend_Http_Client_Exception
     * @throws Zend_Http_Exception
     * @throws Zend_Uri_Exception
     */
    public function post($endpoint, array $data) {
        return $this->doRequest($endpoint, self::POST, $data);
    }

    /**
     * @param $endpoint
     * @param array $data
     * @return Selveo_Integration_Model_Clients_Zend_Http_Response|Zend_Http_Response
     * @throws Selveo_Integration_Model_Exception_Client_ErrorResponse
     * @throws Selveo_Integration_Model_Exception_Client_PartialSyncFailure
     * @throws Selveo_Integration_Model_Exception_Client_ServerNotFoundException
     * @throws Selveo_Integration_Model_Exception_Client_UnauthorizedException
     * @throws Selveo_Integration_Model_Exception_SchemaConstraintException
     * @throws Zend_Http_Client_Exception
     * @throws Zend_Http_Exception
     * @throws Zend_Uri_Exception
     */
    public function put($endpoint, array $data = []) {
        return $this->doRequest($endpoint, self::PUT, $data);
    }

    /**
     * @param $endpoint
     * @param array $data
     * @return Selveo_Integration_Model_Clients_Zend_Http_Response|Zend_Http_Response
     * @throws Selveo_Integration_Model_Exception_Client_ErrorResponse
     * @throws Selveo_Integration_Model_Exception_Client_PartialSyncFailure
     * @throws Selveo_Integration_Model_Exception_Client_ServerNotFoundException
     * @throws Selveo_Integration_Model_Exception_Client_UnauthorizedException
     * @throws Selveo_Integration_Model_Exception_SchemaConstraintException
     * @throws Zend_Http_Client_Exception
     * @throws Zend_Http_Exception
     * @throws Zend_Uri_Exception
     */
    public function delete($endpoint, array $data = []) {
        return $this->doRequest($endpoint, self::DELETE, $data);
    }

    /**
     * @param $endpoint
     * @param string $method
     * @param array $data
     * @return Selveo_Integration_Model_Clients_Zend_Http_Response|Zend_Http_Response
     * @throws Selveo_Integration_Model_Exception_Client_ErrorResponse
     * @throws Selveo_Integration_Model_Exception_Client_PartialSyncFailure
     * @throws Selveo_Integration_Model_Exception_Client_ServerNotFoundException
     * @throws Selveo_Integration_Model_Exception_Client_UnauthorizedException
     * @throws Selveo_Integration_Model_Exception_SchemaConstraintException
     * @throws Selveo_Integration_Model_Exception_Client_JsonEncodeException
     * @throws Zend_Http_Client_Exception
     * @throws Zend_Http_Exception
     * @throws Zend_Uri_Exception
     */
    private function doRequest($endpoint, $method = self::GET, array $data = []) {

        $requestData = json_encode($data);

        if($requestData === false) {
            throw new Selveo_Integration_Model_Exception_Client_JsonEncodeException('Failed to encode request data. JSON error: '.json_last_error_msg());
        }

        $this->getUri()->setPath("/api/v1/".ltrim($endpoint, "/"));

        $this->setHeaders([
            'Content-Type' => 'application/json'
        ]);

        $this->setRawData($requestData);
        $this->generateSignature($data);

        $requestStart = microtime(true);
        $response = $this->request($method);
        $responseTime = microtime(true) - $requestStart;

        $this->handleResponse($response, $requestData, $responseTime);

        return $response;

    }

    /**
     * @param Selveo_Integration_Model_Clients_Zend_Http_Response $response
     * @param                                                     $requestData
     * @param                                                     $responseTime
     *
     * @throws Selveo_Integration_Model_Exception_Client_ErrorResponse
     * @throws Selveo_Integration_Model_Exception_Client_PartialSyncFailure
     * @throws Selveo_Integration_Model_Exception_Client_ServerNotFoundException
     * @throws Selveo_Integration_Model_Exception_Client_UnauthorizedException
     * @throws Selveo_Integration_Model_Exception_SchemaConstraintException
     */
    public function handleResponse(Selveointegration_Response $response, $requestData, $responseTime) {

        $url = $this->getUri()->getPath();

        $this->getLogHelper()->log(array('url' => $url, 'response time [ms]' => sprintf("%d", $responseTime * 1000), 'selveo response' => $response, 'data requested' => $requestData));

        $response->validate($url);

    }

    /**
     * @param null $method
     * @return Selveo_Integration_Model_Clients_Zend_Http_Response|Zend_Http_Response
     * @throws Zend_Http_Client_Exception
     * @throws Zend_Http_Exception
     */
    public function request($method = null) {

        $baseResponse = parent::request($method);

        return new Selveointegration_Response (
            $baseResponse->getStatus(),
            $baseResponse->getHeaders(),
            $baseResponse->getRawBody(),
            $baseResponse->getVersion(),
            $baseResponse->getMessage()
        );

    }


    /**
     * @return mixed
     */
    public function getKey() {
        return $this->key;
    }

    /**
     * @param mixed $key
     * @return Selveo_Integration_Model_Clients_Zend_Http_Client
     */
    public function setKey($key) {

        $this->key = $key;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getSecret() {
        return $this->secret;
    }

    /**
     * @param mixed $secret
     * @return Selveo_Integration_Model_Clients_Zend_Http_Client
     */
    public function setSecret($secret) {

        $this->secret = $secret;
        return $this;

    }

    /**
     * @return mixed
     */
    public function getSlug() {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     * @return Selveo_Integration_Model_Clients_Zend_Http_Client
     */
    public function setSlug($slug) {

        $this->slug = $slug;

        $this->setUri(
            $this->getConfiguredUri()
        );

        return $this;

    }

    /**
     * @param $secs
     * @return Selveo_Integration_Model_Clients_Zend_Http_Client
     * @throws Zend_Http_Client_Exception
     */
    public function setTimeout($secs) {

        $localTimeout = (string) Mage::getConfig()->getNode('selveo/client_timeout');

        $this->setConfig([
           'timeout' => $localTimeout ? $localTimeout : $secs
        ]);

        return $this;

    }

    /**
     * @return Selveo_Integration_Model_Selveo_Api_Order_Invoice
     */
    public function invoice() {
        return Mage::getModel('selveointegration/selveo_api_order_invoice', $this);
    }

    /**
     * @return Selveo_Integration_Model_Selveo_Api_Product_Attributes
     */
    public function productAttributes()
    {
        return Mage::getModel('selveointegration/selveo_api_product_attributes', $this);
    }

    /**
     * @return Selveo_Integration_Model_Selveo_Api_Products
     */
    public function products() {
        return Mage::getModel('selveointegration/selveo_api_products', $this);
    }

    /**
     * @return Selveo_Integration_Model_Selveo_Api_Orders
     */
    public function orders() {
        return Mage::getModel('selveointegration/selveo_api_orders', $this);
    }

    /**
     * @return Selveo_Integration_Model_Selveo_Api_Order_Shipment
     */
    public function shipment() {
        return Mage::getModel('selveointegration/selveo_api_order_shipment', $this);
    }

    /**
     * @return Selveo_Integration_Model_Selveo_Api_Order_Status
     */
    public function orderStatus() {
        return Mage::getModel('selveointegration/selveo_api_order_status', $this);
    }

    /**
     * @throws Zend_Http_Client_Exception
     * @throws Zend_Http_Exception
     * @throws Zend_Uri_Exception
     */
    public function testConnection()
    {
        try {
            $this->get('me')->json();
        } catch (Exception $e) {
            $this->getSession()->addError('Error connecting to SELVEO. Error message: '.$e->getMessage());
            return;
        }

        $this->getSession()->addSuccess('Successfully connected to SELVEO');
    }

    /**
     * @return Selveo_Integration_Model_Selveo_Api_Preflight_Invalidate
     */
    public function invalidatePreflight()
    {
        return Mage::getModel('selveointegration/selveo_api_preflight_invalidate', $this);
    }

    /**
     * @return Selveo_Integration_Model_Selveo_Api_Images
     */
    public function images()
    {
        return Mage::getModel('selveointegration/selveo_api_images', $this);
    }

    /**
     * Gets the dev values from local.xml or the production values in this class
     * @return string
     */
    public function getConfiguredUri() {

        if(!$this->getSlug()){
            return null;
        }

        if(!$domain = (string) Mage::getConfig()->getNode('selveo/domain')){
            $domain = self::DOMAIN;
        }

        if(!$protocol = (string) Mage::getConfig()->getNode('selveo/protocol')) {
            $protocol = self::PROTOCOL;
        }
        
        return $protocol.$this->getSlug(). $domain;
    }

    /**
     * @return Selveo_Integration_Helper_Config
     */
    protected function getConfigHelper() {
        return Mage::helper('selveointegration/config');
    }

    /**
     * @return Selveo_Integration_Helper_Log
     */
    private function getLogHelper() {
        return Mage::helper('selveointegration/log');
    }

    /**
     * @return Mage_Adminhtml_Model_Session
     */
    protected function getSession()
    {
        return Mage::getSingleton('adminhtml/session');
    }

}