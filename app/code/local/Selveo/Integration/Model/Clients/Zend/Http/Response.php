<?php

use Selveo_Integration_Model_Exception_Client_UnauthorizedException as UnauthorizedException;
use Selveo_Integration_Model_Exception_Client_ServerNotFoundException as ServerNotFoundException;
use Selveo_Integration_Model_Exception_SchemaConstraintException as SchemaConstraintException;
use Selveo_Integration_Model_Exception_Client_ErrorResponse as ErrorResponseException;
use Selveo_Integration_Model_Exception_Client_PartialSyncFailure as PartialSyncFailure;

class Selveo_Integration_Model_Clients_Zend_Http_Response extends Zend_Http_Response
{

    public function json() {
        return json_decode($this->getBody());
    }

    /**
     * @param $url
     * @return $this
     * @throws Selveo_Integration_Model_Exception_Client_ErrorResponse
     * @throws Selveo_Integration_Model_Exception_Client_PartialSyncFailure
     * @throws Selveo_Integration_Model_Exception_Client_ServerNotFoundException
     * @throws Selveo_Integration_Model_Exception_Client_UnauthorizedException
     * @throws Selveo_Integration_Model_Exception_SchemaConstraintException
     */
    public function validate($url) {

        switch ($this->getStatus()) {

            case '422':
                throw new SchemaConstraintException(json_encode($this->json()));
                break;

            case '404':
                throw new ServerNotFoundException("Error 404: Can't communicate with SELVEO");
                break;

            case '401':
                throw new UnauthorizedException('Unable to connect to your SELVEO installation: Unauthorized - invalid api key or secret.');
                break;

        }

        if($this->isError()) {
            throw new ErrorResponseException('Error requesting Selveo on path: '.$url.', response code: '.$this->getStatus());
        }

        $this->gracefulValidate($url);

        return $this;

    }

    private function gracefulValidate($url) {

        $metaErrors = $this->json()->meta->errors;

        if($metaErrors === null || count($metaErrors) === 0) {
            return;
        }

        $exception = new PartialSyncFailure('Error on partial sync to Selveo on path: ' . $url, 0, null, $metaErrors);

        $ids = [];

        foreach($metaErrors as $error){
            $ids[] = $error->attributes->{$this->json()->meta->key};
        }

        $exception->setIds($ids);

        throw $exception;

    }

}