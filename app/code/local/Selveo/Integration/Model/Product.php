<?php

use Selveo_Integration_Model_Exception_ProductNotFound as ProductNotFoundException;

class Selveo_Integration_Model_Product extends Mage_Catalog_Model_Product
{

    use Selveo_Integration_Model_FlagsModels;

    protected $messages = [];

    /**
     * @param $sku
     * @param $requestBody
     * @return bool
     * @throws Exception
     */
    public function update($sku, $requestBody) {

        $this->getEmulatorHelper()->onAdmin(function() use ($sku, $requestBody) {

            $product = $this->loadBySku($sku);

            if($product === false || !$product->getId()) {
                throw new ProductNotFoundException("Product with SKU: {$sku} wasn't found");
            }

            $this->flagModelAsSynced($product);
            $this->saveMapped($product, $requestBody);

        });

        return true;

    }

    /**
     * @param $products
     * @return Selveo_Integration_Model_Product
     * @throws Exception
     */
    public function updateMany($products)
    {
        $this->getEmulatorHelper()->onAdmin(function () use ($products) {
            foreach ($products as $sku => $productAttrs) {

                $product = $this->loadBySku($sku);

                if ($product === false || !$product->getId()) {
                    $this->gracefulFail("Product with SKU: {$sku} wasn't found");
                    continue;
                }

                $this->flagModelAsSynced($product);
                $this->saveMapped($product, $productAttrs);

            }

        });

        return $this;
    }

    /**
     * @param $sku
     * @return Mage_Catalog_Model_Product|false
     */
    public function loadBySku($sku) {
        return $this->loadByAttribute('sku', $this->getMapper()->map_sku($sku));
    }

    /**
     * @param $productIds
     * @return Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection
     */
    public function loadByIds($productIds) {

        return $this
                ->getCollection()
                ->addAttributeToSelect('*')
                ->addFieldToFilter('entity_id', array('in' => $productIds));

    }

    /**
     * @return Selveo_Integration_Model_Mappers_Magento_Product
     */
    private function getMapper() {
        return Mage::getModel('selveointegration/mappers_magento_product');
    }

    /**
     * @return Selveo_Integration_Helper_Emulator
     */
    private function getEmulatorHelper() {
        return Mage::helper('selveointegration/emulator');
    }

    private function gracefulFail($message)
    {
        $this->messages[] = $message;
    }

    public function getFailedMessages()
    {
        return $this->messages;
    }

    private function saveMapped(Mage_Catalog_Model_Product $product, array $attrs)
    {
        $fields = $this->getMapper()->map($product, $attrs);

        $product->setData(array_merge($product->getData(), $fields));
        $product->getResource()->save($product);

    }

}