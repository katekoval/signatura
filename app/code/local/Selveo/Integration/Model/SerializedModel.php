<?php

class Selveo_Integration_Model_SerializedModel
{

    protected $resourceName;
    protected $id;

    /**
     * SerializedModel constructor.
     * @param Mage_Core_Model_Abstract
     */
    public function __construct(Mage_Core_Model_Abstract $model) {
        $this->resourceName = $model->getResourceName();
        $this->id = $model->getId();

    }

    public function getModel() {
        return Mage::getModel($this->resourceName)->load($this->id);
    }

}