<?php

use Selveo_Integration_Model_Exception_UnauthorizedException as UnauthorizedException;

class Selveo_Integration_Controller_Base extends Mage_Core_Controller_Front_Action
{
    use Selveo_Integration_Model_Internal_RouterTrait;

    /** @var Mage_Core_Model_App_Emulation */
    protected $emulation;

    public function __construct(
        Zend_Controller_Request_Abstract $request,
        Zend_Controller_Response_Abstract $response,
        array $invokeArgs = array()
    ) {
        parent::__construct($request, $response, $invokeArgs);
        $this->emulation = Mage::getSingleton('core/app_emulation');
    }

    protected $initialEnvironment;

    public function preDispatch() {

        try {

            $storeCode = $this->getRequest()->get('__store');
            $store     = Mage::getModel('core/store')->load($storeCode);
            $store     = !empty($store->getData()) ? $store : null;

            if (!$this->getConfigHelper()->getModuleEnabled($store)){
                $this->setFlag('', self::FLAG_NO_DISPATCH, true);
                return $this->getResponse()->setHttpResponseCode(403)->setBody('Without access');
            }
            
            if ($store !== null) {
                $this->initialEnvironment = $this->emulation->startEnvironmentEmulation($store->getId(), Mage_Core_Model_App_Area::AREA_GLOBAL);
            }

            $this->getAuthModel()->authorize($this->getRequest());

        } catch (UnauthorizedException $e) {

            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
            return $this->sendForbiddenResponse($e);

        }

        $this->getLogHelper()->logJsonRequest($this->getRequest());

        return parent::preDispatch();

    }

    public function postDispatch()
    {
        if($this->initialEnvironment){
            $this->emulation->stopEnvironmentEmulation($this->initialEnvironment);
        }

        parent::postDispatch();
    }

    /**
     * @param Mage_Core_Controller_Request_Http $request
     * @return mixed
     */
    protected function getBody(Mage_Core_Controller_Request_Http $request)
    {
        return $this->getRequestHelper()->getJsonRawBody($request);
    }

    private function sendForbiddenResponse($e) {

        return $this->getResponse()
            ->setHttpResponseCode(403)
            ->setBody($e->getMessage());

    }

    protected function errorResponse($errorMessage) {

        return $this->getResponse()
            ->setHttpResponseCode(400)
            ->setBody($errorMessage);

    }

    /**
     * @param Exception $e
     * @param int $responseCode
     * @return Zend_Controller_Response_Abstract
     */
    protected function sendErrorResponse($e, $responseCode = 400) {

        $this->getLogHelper()->logWithException("Error handling request.", $e);

        return $this->getResponse()
            ->setHttpResponseCode($responseCode)
            ->setBody($e->getMessage());

    }

    /**
     * @return Selveo_Integration_Helper_Request
     */
    protected function getRequestHelper() {
        return Mage::helper('selveointegration/request');
    }

    /**
     * @return Selveo_Integration_Model_Authenticator
     */
    private function getAuthModel() {
        return Mage::getModel('selveointegration/authenticator');
    }

    /**
     * @return Selveo_Integration_Helper_Log
     */
    private function getLogHelper() {
        return Mage::helper('selveointegration/log');
    }

    /**
     * @return Selveo_Integration_Helper_Config
     */
    private function getConfigHelper()
    {
        return Mage::helper('selveointegration/config');
    }

}