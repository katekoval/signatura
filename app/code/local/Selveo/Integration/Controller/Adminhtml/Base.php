<?php

class Selveo_Integration_Controller_Adminhtml_Base extends Mage_Adminhtml_Controller_Action
{
    
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/system/config/selveointegration_options');
    }

    /**
     * @param $message
     */
    public function addErrorToSession($message) {
        Mage::getSingleton('adminhtml/session')->addError($this->getDataHelper()->__($message));
    }

    /**
     * @param $message
     */
    public function addSuccessToSession($message) {
        Mage::getSingleton('adminhtml/session')->addSuccess($this->getDataHelper()->__($message));
    }

    /**
     * @param $e
     * @return mixed
     */
    public function addExceptionErrorToSession($e) {
        Mage::getSingleton('adminhtml/session')->addError($this->getDataHelper()->__('Error when force syncing entities. Error message: %s', $e->getMessage()));
    }

    /**
     * @param $message
     * @return mixed
     */
    public function addNoticeToSession($message) {
        return Mage::getSingleton('adminhtml/session')->addNotice($this->getDataHelper()->__($message));
    }

    /**
     * @return Selveo_Integration_Helper_Data
     */
    public function getDataHelper() {
        return Mage::helper('selveointegration');
    }

    /**
     * @return Mage_Adminhtml_Controller_Action
     */
    public function redirectBack() {
        return $this->_redirectReferer();
    }

}