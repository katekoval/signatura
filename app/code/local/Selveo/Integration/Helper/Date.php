<?php

class Selveo_Integration_Helper_Date extends Mage_Core_Model_Date
{
    public function now() {
        return $this->gmtDate();
    }

    /**
     * @return Zend_Date
     * @throws Zend_Date_Exception
     */
    public function zend() {
        return new Zend_Date;
    }

    public static function toStrfTime(Zend_Date $date)
    {
        return Varien_Date::convertZendToStrftime(
            $date->get(Varien_Date::DATETIME_INTERNAL_FORMAT)
        );
    }

}