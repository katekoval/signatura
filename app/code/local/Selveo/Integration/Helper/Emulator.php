<?php

class Selveo_Integration_Helper_Emulator implements Selveo_Integration_Model_Interfaces_Emulator
{

    /**
     * @param $callback
     * @return callable
     * @throws Exception
     */
    public function onAdmin($callback) {
        return $this->onStore($callback, Mage_Core_Model_App::ADMIN_STORE_ID);
    }

    /**
     * @param $callback
     * @param $storeId
     * @return callable
     * @throws Exception
     */
    public function onStore($callback, $storeId = Mage_Core_Model_App::ADMIN_STORE_ID) {

        /** @var Mage_Core_Model_App_Emulation $appEmulation */
        $appEmulation           = Mage::getSingleton('core/app_emulation');
        $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);

        try {
            $cb = call_user_func($callback);
        } catch (Exception $e) {

            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
            throw $e;

        }

        $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
        return $cb;

    }
}