<?php

class Selveo_Integration_Helper_Config extends Mage_Core_Helper_Abstract
{
    const SETTING_OPTIONS        = 'selveointegration_options/';
    const SETTING_MODULE_ENABLED = self::SETTING_OPTIONS . 'general/enabled';

    const SETTING_KEY               = self::SETTING_OPTIONS . 'general/key';
    const SETTING_SECRET            = self::SETTING_OPTIONS . 'general/secret';
    const SETTING_SLUG              = self::SETTING_OPTIONS . 'general/slug'; //subdomain

    const SETTING_SHOW_AVAILABILITY = 'general/show_availability';

    const SETTING_ORDER_STATUS_MAP    = 'order_status/map';
    const SETTING_HANDLE_ORDER_STATUS = 'order_status/handle';

    const SETTING_HANDLE_AVAILABILITY = 'advanced/handle_availability';
    const SETTING_BLOCK_SKU_CHANGES   = 'advanced/block_sku_changes';

    const SETTING_SYNC_ORDERS_AUTOMATICALLY   = 'sync/orders_auto';
    const SETTING_SYNC_VIRTUAL_ORDERS         = 'sync/virtual_orders';
    const SETTING_SYNC_NEW_PRODUCTS           = 'sync/new_products';
    const SETTING_CAN_FORCE_SYNC              = 'sync/can_force';
    const SETTING_SYNC_ORDER_STATUS_BLACKLIST = 'sync/order_status_blacklist';

    const SETTING_ORDER_SENDER_SYNC_ENABLED = 'order_sender_address/order_sender_enabled';
    const SETTING_ORDER_SENDER_NAME         = 'order_sender_address/name';
    const SETTING_ORDER_SENDER_STREET       = 'order_sender_address/street';
    const SETTING_ORDER_SENDER_ZIP          = 'order_sender_address/zip';
    const SETTING_ORDER_SENDER_COUNTRY_ID   = 'order_sender_address/country_id';
    const SETTING_ORDER_SENDER_CITY         = 'order_sender_address/city';
    const SETTING_ORDER_SENDER_EMAIL        = 'order_sender_address/email';
    const SETTING_ORDER_SENDER_PHONE        = 'order_sender_address/phone';
    const SETTING_ORDER_SENDER_IDENTIFIER   = 'order_sender_address/identifier';

    protected $coreStoreModel;
    protected $coreWebsiteModel;
    protected $store;

    public function setStore(Mage_Core_Model_Store $store)
    {
        $this->store = $store;
    }

    public function getModuleEnabled($store = null, $website = null)
    {
        return $this->getSetting(self::SETTING_MODULE_ENABLED, $store, $website);
    }

    public function getKey($store = null, $website = null)
    {
        return trim($this->getSetting(self::SETTING_KEY, $store, $website));
    }

    public function getSecret($store = null, $website = null)
    {
        return trim($this->getSetting(self::SETTING_SECRET, $store, $website));
    }

    public function getSlug($store = null, $website = null)
    {
        return trim($this->getSetting(self::SETTING_SLUG, $store, $website));
    }

    public function getUnserialized($setting, $store = null, $website = null)
    {
        return unserialize($this->getSetting($setting, $store, $website));
    }

    public function getOrderStatusMap($store = null)
    {
        return $this->getUnserialized(self::SETTING_OPTIONS . self::SETTING_ORDER_STATUS_MAP, $store);
    }

    public function getHandleOrderStatus($store = null)
    {
        return $this->getSetting(self::SETTING_OPTIONS . self::SETTING_HANDLE_ORDER_STATUS, $store);
    }

    public function getShowAvailability($store = null)
    {
        return (bool)$this->getSetting(self::SETTING_OPTIONS . self::SETTING_SHOW_AVAILABILITY, $store);
    }

    public function getHandleAvailability($store = null) {
        return $this->getSetting(self::SETTING_OPTIONS . self::SETTING_HANDLE_AVAILABILITY, $store);
    }

    public function getBlockSkuChanges($store = null) {
        return $this->getSetting(self::SETTING_OPTIONS . self::SETTING_BLOCK_SKU_CHANGES, $store);
    }

    public function getSyncOrdersAuto($store = null, $website = null)
    {
        return (bool)$this->getSetting(self::SETTING_OPTIONS . self::SETTING_SYNC_ORDERS_AUTOMATICALLY, $store,
            $website);
    }

    public function getSyncVirtualOrders($store = null, $website = null)
    {
        return (bool)$this->getSetting(self::SETTING_OPTIONS . self::SETTING_SYNC_VIRTUAL_ORDERS, $store, $website);
    }

    public function isOrderSenderSyncEnabled($store = null, $website = null)
    {
        return (bool) $this->getSetting(self::SETTING_OPTIONS.self::SETTING_ORDER_SENDER_SYNC_ENABLED, $store, $website);
    }

    public function getOrderSenderName($store = null, $website = null)
    {
        return $this->getSetting(self::SETTING_OPTIONS.self::SETTING_ORDER_SENDER_NAME, $store, $website);
    }

    public function getOrderSenderStreet($store = null, $website = null)
    {
        return $this->getSetting(self::SETTING_OPTIONS . self::SETTING_ORDER_SENDER_STREET, $store, $website);
    }

    public function getOrderSenderZip($store = null, $website = null)
    {
        return $this->getSetting(self::SETTING_OPTIONS . self::SETTING_ORDER_SENDER_ZIP, $store, $website);
    }

    public function getOrderSenderCity($store = null, $website = null)
    {
        return $this->getSetting(self::SETTING_OPTIONS . self::SETTING_ORDER_SENDER_CITY, $store, $website);
    }

    public function getOrderSenderPhone($store = null, $website = null)
    {
        return $this->getSetting(self::SETTING_OPTIONS . self::SETTING_ORDER_SENDER_PHONE, $store, $website);
    }

    public function getOrderSenderEmail($store = null, $website = null)
    {
        return $this->getSetting(self::SETTING_OPTIONS . self::SETTING_ORDER_SENDER_EMAIL, $store, $website);
    }

    public function getOrderSenderCountryId($store = null, $website = null)
    {
        return $this->getSetting(self::SETTING_OPTIONS . self::SETTING_ORDER_SENDER_COUNTRY_ID, $store, $website);
    }

    public function getOrderSenderIdentifier($store = null, $website = null)
    {
        return $this->getSetting(self::SETTING_OPTIONS . self::SETTING_ORDER_SENDER_IDENTIFIER, $store, $website);
    }

    /**
     * @param null $store
     * @param null $website
     * @return array
     */
    public function getSyncOrderStatusBlacklist($store = null, $website = null)
    {
        return $this->getMultipleValuesFor(
            self::SETTING_OPTIONS . self::SETTING_SYNC_ORDER_STATUS_BLACKLIST,
            $store,
            $website
        );
    }

    public function syncNewlyCreatedProducts($store = null, $website = null)
    {
        return $this->getSetting(self::SETTING_OPTIONS . self::SETTING_SYNC_NEW_PRODUCTS, $store, $website);
    }

    public function canForceSync($store = null, $website = null)
    {
        return (bool) $this->getSetting(self::SETTING_OPTIONS. self::SETTING_CAN_FORCE_SYNC, $store, $website);
    }

    protected function getSetting($setting, $store = null, $website = null) {

        if ($store) {
            return $store->getConfig($setting);
        }

        if($website) {
            return $website->getConfig($setting);
        }

        return Mage::getStoreConfig($setting);

    }

    /**
     * @param $store
     * @param $website
     * @return array
     */
    private function getMultipleValuesFor($setting, $store = null, $website = null)
    {
        $value = $this->getSetting($setting, $store, $website);

        if(is_null($value)) {
            return [];
        }

        return explode(',', $value);
    }

    /**
     * @return Mage_Core_Model_Store|null
     */
    public function getCurrentConfigStore() {

        if(!$storeCode = Mage::app()->getRequest()->getParam('store')){
            return null;
        }

        /** @var Mage_Core_Model_Store $store */
        $store = Mage::getModel('core/store')->load($storeCode);

        if(!$store->getId()){
            return null;
        }

        return $store;
    }

    /**
     * @return Mage_Core_Model_Website|null
     */
    public function getCurrentConfigWebsite() {

        if(!$websiteCode = Mage::app()->getRequest()->getParam('website')){
            return null;
        }

        /** @var Mage_Core_Model_Website $website */
        $website = Mage::getModel('core/website')->load($websiteCode);

        if (!$website->getId()) {
            return null;
        }

        return $website;

    }

    /**
     * @return Mage_Core_Model_Store
     */
    protected function getCoreStoreModel()
    {
        return Mage::getModel('core/store');
    }

    /**
     * @return Mage_Adminhtml_Model_Config_Data
     */
    protected function getAdminhtmlConfigDataModel()
    {
        return Mage::getSingleton('adminhtml/config_data');
    }

    /**
     * @return Mage_Core_Model_Website
     */
    protected function getCoreWebsiteModel()
    {
        return Mage::getModel('core/website');
    }

}