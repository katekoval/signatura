<?php

class Selveo_Integration_Helper_Synced extends Mage_Core_Helper_Abstract
{

    public function set(Mage_Core_Model_Abstract $model, Selveo_Integration_Model_Clients_Zend_Http_Response $response)
    {
        if($response->isError()) {
            return;
        }

        $this->setDirectly($model);
    }

    public function isSynced(Mage_Core_Model_Abstract $model)
    {
        return $model->getSelveoSynced() !== null;
    }

    /**
     * @return Selveo_Integration_Helper_Date
     */
    private function getDateHelper()
    {
        return Mage::helper('selveointegration/date');
    }

    /**
     * @param Mage_Core_Model_Abstract $model
     */
    public function setDirectly(Mage_Core_Model_Abstract $model)
    {
        $now = $this->getDateHelper()->now();

        $model->setSelveoSynced($now);
        $model->getResource()->saveAttribute($model, 'selveo_synced');
    }
}