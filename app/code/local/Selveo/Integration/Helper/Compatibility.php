<?php

class Selveo_Integration_Helper_Compatibility extends Mage_Core_Helper_Abstract
{
    public function escapeConfig($label)
    {
        if(version_compare(Mage::getVersion(), '1.9.2.0', '>=')) {
            return $label;
        }

        return Mage::helper('core')->jsQuoteEscape($label);

    }
}
