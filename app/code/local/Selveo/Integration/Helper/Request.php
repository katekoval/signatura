<?php

class Selveo_Integration_Helper_Request
{

    public function getMethod(Mage_Core_Controller_Request_Http $request) {
        return strtolower($request->getHeader('X-METHOD'));
    }

    public function getJsonRawBody(Mage_Core_Controller_Request_Http $request) {
        return json_decode($request->getRawBody(), true);
    }

}