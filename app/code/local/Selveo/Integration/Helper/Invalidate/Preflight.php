<?php

use Selveo_Integration_Helper_Config as ConfigHelper;

class Selveo_Integration_Helper_Invalidate_Preflight extends Mage_Core_Helper_Abstract
{
    public function forAllInstalls()
    {

        /** @var Selveo_Integration_Model_Installation $installation */
        foreach ($this->getAllInstallations() as $installation) {

            if (!$installation->isConfigured()) {
                continue;
            }

            try {

                $installation
                    ->getClient()
                    ->invalidatePreflight()
                    ->destroy();

            } catch (Exception $e) {
                Mage::logException($e);
            }

        }
    }

    /**
     * @return Selveo_Integration_Model_Installation_Collection
     */
    private function getInstallationCollectionModel()
    {
        return Mage::getSingleton('selveointegration/installation_collection');
    }

    /**
     * @return Selveo_Integration_Model_Installation_Collection
     */
    private function getAllInstallations()
    {
        $configRows = Mage::getModel('core/config_data')
            ->getCollection()
            ->addPathFilter('selveointegration_options/general');

        $collection = $this->getInstallationCollectionModel();

        foreach ($this->getConfigScopes($configRows) as $scope) {

            $collection->add(new Selveo_Integration_Model_Installation(
                    $scope[ConfigHelper::SETTING_KEY],
                    $scope[ConfigHelper::SETTING_SECRET],
                    $scope[ConfigHelper::SETTING_SLUG])
            );

        }

        return $collection;
    }

    /**
     * @param $configRows
     * @return array
     */
    private function getConfigScopes($configRows)
    {
        $scopes = array();

        foreach ($configRows as $config) {

            if (!isset($scopes[$config->getScope() . $config->getScopeId()])) {
                $scopes[$config->getScope() . $config->getScopeId()] = [
                    ConfigHelper::SETTING_KEY    => null,
                    ConfigHelper::SETTING_SECRET => null,
                    ConfigHelper::SETTING_SLUG   => null
                ];
            }

            $scopes[$config->getScope() . $config->getScopeId()][$config->getPath()] = $config->getValue();
        }
        return $scopes;
    }
}