<?php

trait Selveo_Integration_Helper_Array {

    protected function array_get(array $data, $key, $default = '') {
        return isset($data[$key]) ? $data[$key] : $default;
    }

}
