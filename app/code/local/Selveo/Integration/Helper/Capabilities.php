<?php

class Selveo_Integration_Helper_Capabilities extends Mage_Core_Helper_Abstract
{
    public function get()
    {
        return [
            'products' => ['batches']
        ];
    }
}