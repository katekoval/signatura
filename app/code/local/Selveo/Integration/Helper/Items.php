<?php

class Selveo_Integration_Helper_Items extends Mage_Core_Helper_Abstract
{

    public function getItems(Mage_Core_Model_Abstract $entity) {

        $items = array();

        foreach ($entity->getAllItems() as $item) {

            $childrenItems = $item->getOrderItem()->getChildrenItems();

            /* Don't ship/invoice items with children eg. bundled products. The children here is already with in getAllItems */
            if(!empty($childrenItems)) {
                continue;
            }

            $items[] = $item;

        }

        return $items;

    }

}