<?php

class Selveo_Integration_Helper_Images extends Mage_Core_Helper_Abstract
{

    public function getDeletedImageIds($images)
    {

        $deleteIds = array();

        foreach($images as $image){
            if (!isset($image['removed']) || $image['removed'] !== 1) {
                continue;
            }

            $deleteIds[] = $image['value_id'];
        }

        return $deleteIds;
    }
}