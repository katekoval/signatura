<?php

class Selveo_Integration_Helper_Log
{
    public function log($message) {
        Mage::log($message, null, 'firtal_selveointegration.log', true);
    }

    /**
     * @param $message
     * @param null|Exception $e
     */
    public function logWithException($message, $e = null) {

        if($e) {
            $message .= ' Exception message: '.$e->getMessage().' trace: '.$e->getTraceAsString();
        }

       $this->log($message);

    }

    public function logJsonRequest(Mage_Core_Controller_Request_Http $request) {

        $this->log("Incoming request {$request->getRequestUri()} with: {$request->getHeader('X-METHOD')} params: {$request->getRawBody()}");

    }
}