<?php

class Selveo_Integration_Block_Frontend_Product_Availability extends Mage_Core_Block_Template
{

    public function getMessage() {

        if(!($product = Mage::registry('current_product'))) {
            return;
        }

        if(!($message = $product->getData('availability_message'))) {
            return;
        }

        return $message;

    }

}