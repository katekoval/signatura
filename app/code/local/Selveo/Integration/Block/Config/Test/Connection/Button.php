<?php

class Selveo_Integration_Block_Config_Test_Connection_Button extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('selveointegration/config/test_connection_button.phtml');
    }

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        return $this->_toHtml();
    }

    protected function getTestUrl()
    {
        return Mage::helper('adminhtml')->getUrl('adminhtml/selveointegration_test/index');
    }

    public function getButtonHtml()
    {
        return $this->getLayout()
            ->createBlock('adminhtml/widget_button')
            ->setType('button')
            ->setLabel($this->__('Test connection'))
            ->setOnclick("setLocation('".$this->getTestUrl()."')")
            ->setClass('testSelveoConnection')
            ->toHtml();
    }
}