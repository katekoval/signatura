<?php

class Selveo_Integration_Block_Config_Order_Status extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    protected $magentoStatusRenderer;
    protected $selveoStatusRenderer;

    protected function getMagentoStatusRenderer()
    {
        if (!$this->magentoStatusRenderer) {
            $this->magentoStatusRenderer = $this->getLayout()->createBlock(
                'selveointegration/config_magento_order_statuses',
                '',
                array('is_render_to_js_template' => true)
            );
        }

        return $this->magentoStatusRenderer;

    }

    protected function getSelveoStatusRenderer() {

        if(!$this->selveoStatusRenderer) {
            $this->selveoStatusRenderer = $this->getLayout()->createBlock(
                'selveointegration/config_selveo_order_statuses',
                '',
                array('is_render_to_js_template' => true)
            );
        }

        return $this->selveoStatusRenderer;

    }

    protected function _prepareToRender()
    {
        $this->addColumn('magento_order_status', array(
            'label' => $this->__('Magento order status'),
            'renderer' => $this->getMagentoStatusRenderer(),
        ));

        $this->addColumn('selveo_order_status', array(
            'label' => $this->__('Selveo order status'),
            'renderer' => $this->getSelveoStatusRenderer(),
        ));

        $this->_addAfter = false;
    }

    protected function _prepareArrayRow(Varien_Object $row)
    {
        $row->setData(
            'option_extra_attr_' . $this->getMagentoStatusRenderer()->calcOptionHash($row->getData('magento_order_status')),
            'selected="selected"'
        );

        $row->setData(
            'option_extra_attr_' . $this->getSelveoStatusRenderer()->calcOptionHash($row->getData('selveo_order_status')),
            'selected="selected"'
        );
    }

}