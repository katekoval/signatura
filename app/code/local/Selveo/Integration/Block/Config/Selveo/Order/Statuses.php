<?php

class Selveo_Integration_Block_Config_Selveo_Order_Statuses extends Mage_Adminhtml_Block_Html_Select
{

    protected $configHelper;

    public function _toHtml() {

        try {

            $installations = (new Selveo_Integration_Model_Installation_Factory)->forCurrentConfigView();

            $statuses = [];

            /** @var Selveo_Integration_Model_Installation $installation */
            foreach ($installations as $installation) {

                $statuses = $installation
                    ->getClient()
                    ->orderStatus()
                    ->getArrayCollection();

            }

            $this->setOptions($statuses);

        } catch(Exception $e) {
            Mage::getSingleton('core/session')->addError('Error getting order statuses to map. Error message: '.$e->getMessage());
            return parent::_toHtml();
        }

        return parent::_toHtml();

    }

    public function setInputName($value) {
        return $this->setName($value);
    }

    /**
     * @return Selveo_Integration_Model_Clients_Zend_Http_Client
     */
    protected function getClient() {
        return Mage::getModel('selveointegration/clients_zend_http_client');
    }

    /**
     * @return Selveo_Integration_Helper_Config
     */
    protected function getConfigHelper() {
        return Mage::helper('selveointegration/config');
    }

}