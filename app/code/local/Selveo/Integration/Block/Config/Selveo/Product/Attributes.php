<?php

class Selveo_Integration_Block_Config_Selveo_Product_Attributes extends Mage_Core_Block_Html_Select
{
    protected function _toHtml()
    {
        try {

            $installations = (new Selveo_Integration_Model_Installation_Factory)->forCurrentConfigView();
            $attributes    = [];

            /** @var Selveo_Integration_Model_Installation $installation */
            foreach ($installations as $installation) {

                $attributes = $installation
                    ->getClient()
                    ->productAttributes()
                    ->getArrayCollection();

            }

            $this->setOptions($attributes);

        } catch (Exception $e) {
            Mage::getSingleton('core/session')->addError('Error getting product attributes to map. Error message: ' . $e->getMessage());
        }

        return parent::_toHtml();
    }

    public function setInputName($value)
    {
        return $this->setName($value);
    }

}