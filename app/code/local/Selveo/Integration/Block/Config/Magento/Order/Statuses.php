<?php

class Selveo_Integration_Block_Config_Magento_Order_Statuses extends Mage_Core_Block_Html_Select
{
    public function _toHtml() {

        $statuses = Mage::getModel('selveointegration/config_order_status')->getArrayCollection();

        $this->setOptions($statuses);

        return parent::_toHtml();

    }

    public function setInputName($value) {

        return $this->setName($value);

    }
}