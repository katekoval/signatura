<?php

class Selveo_Integration_Block_Config_Magento_Product_Attributes extends Mage_Core_Block_Html_Select
{
    protected function _toHtml()
    {
        $this->setOptions($this->getAttributesForMapping()->toOptionArray());

        return parent::_toHtml();
    }

    /**
     * @return Selveo_Integration_Model_Config_Product_Attributes_Mapping
     */
    private function getAttributesForMapping()
    {
        return Mage::getModel('selveointegration/config_product_attributes_mapping');
    }

    public function setInputName($value)
    {
        return $this->setName($value);
    }
}