<?php

class Selveo_Integration_Block_Config_Sync_Products_Button extends Mage_Adminhtml_Block_System_Config_Form_Field
{

    const CURRENT_PAGE_FALLBACK = 1;

    public function _construct() {

        parent::_construct();
        $this->setTemplate('selveointegration/config/sync_products_button.phtml');

    }

    public function getCurrentPage() {

        if (!Mage::getModel('core/cache')->canUse('selveointegration')) {
            return self::CURRENT_PAGE_FALLBACK;
        }

        $currentPage = Mage::app()->getCache()->load('current_page');

        return $currentPage !== false ? $currentPage : self::CURRENT_PAGE_FALLBACK;

    }

    public function getUrlTemplate() {

        $request = Mage::app()->getRequest();

        return Mage::helper('adminhtml')->getUrl('adminhtml/selveointegration_products/sync', array(
            'store'    => $request->getParam('store'),
            'website'  => $request->getParam('website'),
            'form_key' => Mage::getSingleton('core/session')->getFormKey(),
            'currentPage' => ':currentPage'
        ));
    }

    public function getButtonHtml() {

        return $this->getLayout()
            ->createBlock('adminhtml/widget_button')
            ->setType('button')
            ->setLabel($this->__('Sync products and images to Selveo'))
            ->setClass('syncProductsToSelveo')
            ->toHtml();

    }

    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {
        return $this->_toHtml();
    }

}