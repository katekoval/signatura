<?php

class Selveo_Integration_Block_Config_Sync_Direction extends Mage_Core_Block_Html_Select
{
    protected function _toHtml()
    {
        $this->setOptions($this->getDirectionModel()->toOptionArray());

        return parent::_toHtml();
    }

    /**
     * @return Selveo_Integration_Model_Config_Sync_Directions
     */
    protected function getDirectionModel()
    {
        return Mage::getSingleton('selveointegration/config_sync_directions');
    }

    public function setInputName($value)
    {
        return $this->setName($value);
    }
}