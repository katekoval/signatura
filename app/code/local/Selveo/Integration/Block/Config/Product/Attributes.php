<?php

class Selveo_Integration_Block_Config_Product_Attributes extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{

    protected $magentoAttributeRenderer;
    protected $selveoAttributeRenderer;
    protected $directionRenderer;

    protected function _prepareToRender()
    {
        $this->addColumn('magento_attribute', array(
            'label'    => $this->__('Magento attribute'),
            'renderer' => $this->getMagentoAttributeRenderer()
        ));

        $this->addColumn('selveo_attribute', array(
            'label' => $this->__('Selveo attribute'),
            'renderer' => $this->getSelveoAttributeRenderer()
        ));

        $this->addColumn('sync_direction', array(
            'label' => $this->__('Direction to sync'),
            'renderer' => $this->getDirectionRenderer()
        ));
    }

    protected function _prepareArrayRow(Varien_Object $row)
    {
        $row->setData(
            'option_extra_attr_' . $this->getMagentoAttributeRenderer()->calcOptionHash($row->getData('magento_attribute')),
            'selected="selected"'
        );

        $row->setData(
            'option_extra_attr_' . $this->getSelveoAttributeRenderer()->calcOptionHash($row->getData('selveo_attribute')),
            'selected="selected"'
        );

        $row->setData(
            'option_extra_attr_' . $this->getDirectionRenderer()->calcOptionHash($row->getData('sync_direction')),
            'selected="selected"'
        );

    }

    private function getMagentoAttributeRenderer()
    {
        if (!$this->magentoAttributeRenderer) {
            $this->magentoAttributeRenderer = $this->getLayout()->createBlock(
                'selveointegration/config_magento_product_attributes',
                '',
                array('is_render_to_js_template' => true)
            );
        }

        return $this->magentoAttributeRenderer;
    }

    private function getSelveoAttributeRenderer()
    {
        if (!$this->selveoAttributeRenderer) {
            $this->selveoAttributeRenderer = $this->getLayout()->createBlock(
                'selveointegration/config_selveo_product_attributes',
                '',
                array('is_render_to_js_template' => true)
            );
        }

        return $this->selveoAttributeRenderer;
    }

    private function getDirectionRenderer()
    {
        if (!$this->directionRenderer) {
            $this->directionRenderer = $this->getLayout()->createBlock(
                'selveointegration/config_sync_direction',
                '',
                array('is_render_to_js_template' => true)
            );
        }

        return $this->directionRenderer;
    }
}