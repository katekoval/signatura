<?php

try {

    /** @var Mage_Catalog_Model_Resource_Eav_Mysql4_Setup $installer */
    $installer = $this;
    $installer->startSetup();

    $attrName = 'selveo_synced';

    $flatShipmentAttrExistsResult = $installer->getConnection('core_read')->fetchAll("
        SHOW COLUMNS 
        FROM `sales_flat_shipment`
        WHERE FIELD='" . $attrName . "'
        ");

    if (count($flatShipmentAttrExistsResult) === 0) {
        $installer->run("
            ALTER TABLE `sales_flat_shipment` 
            ADD COLUMN " . $attrName . " DATETIME DEFAULT NULL;
            UPDATE `sales_flat_shipment` 
            SET " . $attrName . " = NOW();
        ");
    }

    $flatInvoiceAttrExistsResult = $installer->getConnection('core_read')->fetchAll("
        SHOW COLUMNS 
        FROM `sales_flat_invoice`
        WHERE FIELD='" . $attrName . "' 
        ");

    if (count($flatInvoiceAttrExistsResult) === 0) {
        $installer->run("
            ALTER TABLE `sales_flat_invoice` ADD COLUMN " . $attrName . " DATETIME DEFAULT NULL;
            UPDATE `sales_flat_invoice` SET " . $attrName . " = NOW();
        ");
    }

    $installer->endSetup();

} catch (Exception $e) {
    Mage::logException($e);
}
