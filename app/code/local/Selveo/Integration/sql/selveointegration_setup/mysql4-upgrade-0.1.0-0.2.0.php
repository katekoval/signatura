<?php

/** @var Mage_Catalog_Model_Resource_Eav_Mysql4_Setup $installer */
$installer = $this;
$installer->startSetup();

$installer->run(
    "CREATE TABLE IF NOT EXISTS `selveo_jobs` (
        `entity_id` INT(10) unsigned NOT NULL auto_increment,
        `serialized` MEDIUMBLOB,
        `tries` INT(10) NOT NULL DEFAULT 0,
        `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
        `last_try_at` TIMESTAMP NULL,
        `retry_at` TIMESTAMP NULL,
        `state` INT NOT NULL DEFAULT 0,
        `reserved_at` TIMESTAMP NULL,
        PRIMARY KEY (`entity_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
);

$installer->endSetup();