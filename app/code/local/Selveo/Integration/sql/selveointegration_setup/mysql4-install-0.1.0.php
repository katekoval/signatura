<?php

/** @var Mage_Catalog_Model_Resource_Eav_Mysql4_Setup $installer */
$installer = $this;
$installer->startSetup();

$attrName = 'selveo_synced';

$flatOrderAttrExistsResult = $installer->getConnection('core_read')->fetchAll("
        SHOW COLUMNS 
        FROM `sales_flat_order`
        WHERE FIELD='" . $attrName. "' 
        ");

if(count($flatOrderAttrExistsResult) === 0) {

    $installer->run("
        ALTER TABLE `sales_flat_order` ADD COLUMN ".$attrName." DATETIME DEFAULT NULL;
        UPDATE `sales_flat_order` SET ".$attrName." = NOW()
    ");

}

$installer->endSetup();