<?php

use Mage_Catalog_Model_Product_Type as ProductType;

try {

    $attrName = 'availability_message';

    $attr = Mage::getModel('catalog/resource_eav_attribute')->loadByCode('catalog_product', $attrName);

    if($attr->getId() !== null) {
        Mage::helper('selveointegration/log')->log('The attribute: '.$attrName.' is already installed on the product entity');
        return;
    }

    /** @var Mage_Catalog_Model_Resource_Eav_Mysql4_Setup $installer */
    $installer = Mage::getResourceModel('catalog/setup', 'catalog_setup');
    $installer->startSetup();

    $applyTo = array(
        ProductType::TYPE_SIMPLE,
        ProductType::TYPE_BUNDLE,
        ProductType::TYPE_GROUPED,
        ProductType::TYPE_CONFIGURABLE,
        ProductType::TYPE_VIRTUAL
    );
    
    $installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, $attrName, array(
        'label'                      => 'Availability message',
        'input'                      => 'text',
        'type'                       => 'varchar',
        'required'                   => 0,
        'comparable'                 => 0,
        'filterable'                 => 0,
        'filterable_in_search'       => 0,
        'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'is_configurable'            => 0,
        'is_html_allowed_on_front'   => 0,
        'note'                       => '',
        'searchable'                 => 0,
        'sort_order'                 => 100,
        'unique'                     => 0,
        'used_for_sort_by'           => 0,
        'used_in_product_listing'    => 1,
        'user_defined'               => 1,
        'visible'                    => 1,
        'visible_on_front'           => 1,
        'visible_in_advanced_search' => 0,
        'wysiwyg_enabled'            => 0,
        'apply_to'                   => implode(',', $applyTo),
    ));


    $productAttrSet = Mage::getModel('eav/entity_type')
                        ->getCollection()
                        ->addFieldToFilter('entity_type_code', Mage_Catalog_Model_Product::ENTITY); //the catalog_product attr set

    $attrSetCollection = Mage::getModel('eav/entity_attribute_set')
        ->getCollection()
        ->addFieldToFilter('entity_type_id', $productAttrSet->getFirstItem()->getEntityTypeId());

    //Set the attribute on the sets
    foreach ($attrSetCollection as $attrSet) {

        $installer->addAttributeToSet(
            Mage_Catalog_Model_Product::ENTITY,
            $attrSet->getAttributeSetName(),   // Attribute set name
            'General',   // Attribute set group name
            $attrName,
            100
        );

    }

    $installer->endSetup();

} catch (Exception $e) {
    Mage::logException($e);
}