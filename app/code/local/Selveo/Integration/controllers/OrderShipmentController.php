<?php

use Selveo_Integration_Model_Exception_OrderNotFound as OrderNotFoundException;
use Selveo_Integration_Model_Exception_RequestedItemsNotFoundException as RequestedItemsNotFoundException;
use Selveo_Integration_Model_Exception_NoItemsArePreparedException as NoItemsArePreparedException;

class Selveo_Integration_OrderShipmentController extends Selveo_Integration_Controller_Base
{

    public function indexAction() {

        try {

            $router = $this->route();
            $router->put(array($this, 'shipOrder'));

            return $this->getResponse()->setBody($router->run());

        } catch(OrderNotFoundException $e) {
            return $this->sendErrorResponse($e, 404);
        } catch (RequestedItemsNotFoundException $e) {
            return $this->sendErrorResponse($e);
        } catch (NoItemsArePreparedException $e) {
            return $this->sendErrorResponse($e);
        } catch (Exception $e) {
            return $this->sendErrorResponse($e, 500);
        }

        return $this->errorResponse('Order failed to be shipped');

    }

    /**
     * @return Selveo_Integration_Model_Order_Shipment
     */
    private function getOrderShipmentModel() {
        return Mage::getModel('selveointegration/order_shipment');
    }

    /**
     * @param $request
     * @return string
     * @throws Selveo_Integration_Model_Exception_OrderNotFound
     */
    public function shipOrder(Mage_Core_Controller_Request_Http $request) {

        $orderIncrementId = $request->getQuery('incrementId');
        $body             = $this->getRequestHelper()->getJsonRawBody($request);

        if($this->getOrderShipmentModel()->ship($orderIncrementId, $body)) {
            return "Order #{$orderIncrementId} is successfully shipped";
        }

        return "Order #{$orderIncrementId} failed to be shipped";
        
    }

}
