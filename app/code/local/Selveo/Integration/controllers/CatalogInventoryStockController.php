<?php

use Selveo_Integration_Model_Exception_ProductNotFound as ProductNotFoundException;
use Selveo_Integration_Model_Exception_StockItemNotFound as StockItemNotFoundException;
use Selveo_Integration_Model_Exception_StockItemNotSavedException as StockItemNotSavedException;

class Selveo_Integration_CatalogInventoryStockController extends Selveo_Integration_Controller_Base
{

    public function indexAction() {

        try {

            $router = $this->route();
            $router->put(array($this, 'updateInventory'));

            return $this->getResponse()->setBody($router->run());

        } catch (ProductNotFoundException $e) {
            return $this->sendErrorResponse($e, 404);
        } catch (StockItemNotFoundException $e) {
            return $this->sendErrorResponse($e);
        } catch (StockItemNotSavedException $e) {
            return $this->sendErrorResponse($e);
        } catch (Exception $e) {
            return $this->sendErrorResponse($e, 500);
        }

        return $this->errorResponse('Catalog inventory failed to be updated');

    }

    /**
     * @return Selveo_Integration_Model_Catalog_Inventory_Stock_Item
     */
    private function getCatalogInventoryStockModel() {
        return Mage::getModel('selveointegration/catalog_inventory_stock_item');
    }

    /**
     * @param $request
     * @return string
     */
    public function updateInventory(Mage_Core_Controller_Request_Http $request) {

        $sku = $request->getQuery('sku');

        $requestBody = $this->getRequestHelper()->getJsonRawBody($request);

        if($this->getCatalogInventoryStockModel()->update($sku, $requestBody)) {
            return "Stock status for product with sku: {$sku} is successfully updated";
        }

        return "Stock status for product with sku: {$sku} failed to be updated";

    }

}