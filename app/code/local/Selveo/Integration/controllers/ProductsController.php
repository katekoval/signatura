<?php

class Selveo_Integration_ProductsController extends Selveo_Integration_Controller_Base
{

    public function indexAction() {

        try {

            $router = $this->route();
            $router->put(array($this, 'updateProduct'));

            return $this->getResponse()->setBody($router->run());

        } catch (Exception $e) {
            return $this->sendErrorResponse($e, 500);
        }

        return $this->errorResponse('Product failed to be updated');

    }

    public function bulkAction()
    {
        try {

            $router = $this->route();
            $router->put(array($this, 'bulkUpdateProducts'));

            $productModel   = $router->run();
            $failedMessages = $productModel->getFailedMessages();

            if (count($failedMessages) === 0) {
                return $this->getResponse()->setBody("The products are successfully updated");
            }

            return $this->errorResponse(implode($failedMessages, ', '));

        } catch (Exception $e) {
            return $this->sendErrorResponse($e, 500);
        }

    }

    /**
     * @param Mage_Core_Controller_Request_Http $request
     * @return string
     * @throws Exception
     */
    public function bulkUpdateProducts(Mage_Core_Controller_Request_Http $request)
    {
        $body         = $this->getBody($request);
        $productModel = $this->getProductModel();

        return $productModel->updateMany($body);

    }

    /**
     * @param Mage_Core_Controller_Request_Http $request
     * @return string
     * @throws Exception
     */
    public function updateProduct(Mage_Core_Controller_Request_Http $request) {

        $sku = $request->getQuery('id');

        if ($this->getProductModel()->update($sku, $this->getBody($request))) {
            return "Product with SKU: '".$sku."' is successfully updated";
        }

        return "Product with SKU: '".$sku."' failed to be updated";

    }

    /**
     * @return Selveo_Integration_Model_Product
     */
    private function getProductModel() {
        return Mage::getSingleton('selveointegration/product');
    }

}