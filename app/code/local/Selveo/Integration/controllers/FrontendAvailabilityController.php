<?php

class Selveo_Integration_FrontendAvailabilityController extends Mage_Core_Controller_Front_Action
{

    public function indexAction() {

        if ($this->getRequest()->getPost('formKey') !== $this->getFormKey()) {
            return $this->getResponse()->setHttpResponseCode(401);
        }

        if (!($productIds = $this->getRequest()->getPost('productIds'))) {
            return $this->getResponse()->setHttpResponseCode(400);
        }

        $messages = $this->getProductAvailabilityModel()
            ->getMessagesByIds($productIds);

        return $this->getResponse()
            ->clearHeaders()
            ->setHeader('Content-type', 'application/json')
            ->setBody(json_encode($messages));

    }

    /**
     * @return Selveo_Integration_Model_Product_Availability
     */
    private function getProductAvailabilityModel() {
        return Mage::getModel('selveointegration/product_availability');
    }

    private function getFormKey() {
        return Mage::getSingleton('core/session')->getFormKey();
    }

}