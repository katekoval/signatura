<?php

use Selveo_Integration_Model_Exception_NoItemsArePreparedException as NoItemsArePreparedException;
use Selveo_Integration_Model_Exception_OrderNotFound as OrderNotFoundException;
use Selveo_Integration_Model_Exception_RequestedItemsNotFoundException as RequestedItemsNotFoundException;

class Selveo_Integration_OrderShipAndInvoiceController extends Selveo_Integration_Controller_Base
{

    public function indexAction() {

        try {

            $router = $this->route();

            $router->put(array($this, 'shipAndInvoice'));

            return $this->getResponse()->setBody($router->run());

        } catch (OrderNotFoundException $e) {
            return $this->sendErrorResponse($e, 404);
        } catch (RequestedItemsNotFoundException $e) {
            return $this->sendErrorResponse($e);
        } catch (NoItemsArePreparedException $e) {
            return $this->sendErrorResponse($e);
        } catch (Mage_Core_Exception $e) {
            return $this->sendErrorResponse($e, 500);
        } catch (Exception $e) {
            return $this->sendErrorResponse($e, 500);
        }

        return $this->errorResponse('Order failed to be shipped and invoiced');

    }

    /**
     * @param Mage_Core_Controller_Request_Http $request
     * @return false|string
     * @throws Selveo_Integration_Model_Exception_NoItemsArePreparedException
     * @throws Selveo_Integration_Model_Exception_OrderNotFound
     * @throws Selveo_Integration_Model_Exception_RequestedItemsNotFoundException
     * @throws Selveo_Integration_Model_Exception_OrderIsVirtualException
     */
    public function shipAndInvoice(Mage_Core_Controller_Request_Http $request) {

        $orderIncrementId = $request->getQuery('incrementId');
        $body             = $this->getRequestHelper()->getJsonRawBody($request);

        $response = [
            'bu_shipment_id'    => null,
            'bu_invoice_id'     => null,
            'shipment_item_ids' => [],
            'invoice_item_ids'  => []
        ];

        if (($shipment = $this->getOrderShipmentModel()->ship($orderIncrementId,
                $body)) instanceof Mage_Sales_Model_Order_Shipment) {
            $response = Mage::getSingleton('selveointegration/responders_shipment')->get($shipment, $body, $response);
        }

        if (($invoice = $this->getOrderInvoiceModel()->invoice($orderIncrementId,
                $body)) instanceof Mage_Sales_Model_Order_Invoice) {
            $response = Mage::getSingleton('selveointegration/responders_invoice')->get($invoice, $body, $response);
        }

        return json_encode($response);

    }

    /**
     * @return Selveo_Integration_Model_Order_Shipment
     */
    private function getOrderShipmentModel() {
        return Mage::getModel('selveointegration/order_shipment');
    }

    /**
     * @return Selveo_Integration_Model_Order_Invoice
     */
    private function getOrderInvoiceModel() {
        return Mage::getModel('selveointegration/order_invoice');
    }
}