<?php

class Selveo_Integration_PreflightController extends Selveo_Integration_Controller_Base
{
    /**
     * @return Zend_Controller_Response_Abstract
     * @throws Exception
     */
    public function indexAction()
    {
        try {

            $router = $this->route();
            $router->get(array($this, 'getPreflight'));

            $whitelist = $router->run();
            $this->getResponse()->setBody(json_encode($whitelist));

        } catch (Exception $e) {
            return $this->sendErrorResponse($e);
        }

    }

    public function getPreflight()
    {
        return [
            'whitelists'   => $this->getMagentoMappersRegistry()->getSelveoAttributesWhitelist(),
            'capabilities' => $this->getCapabilitiesHelper()->get()
        ];
    }

    /**
     * @return Selveo_Integration_Model_Mappers_Registry
     */
    private function getMagentoMappersRegistry()
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return Mage::getModel('selveointegration/mappers_registry');
    }

    /**
     * @return Selveo_Integration_Helper_Capabilities
     */
    private function getCapabilitiesHelper()
    {
        return Mage::helper('selveointegration/capabilities');
    }
}