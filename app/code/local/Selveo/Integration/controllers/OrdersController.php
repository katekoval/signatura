<?php

use Selveo_Integration_Model_Exception_OrderNotFound as OrderNotFoundException;

class Selveo_Integration_OrdersController extends Selveo_Integration_Controller_Base
{
    public function indexAction() {

        try {

            $router = $this->route();
            $router->put(array($this, 'updateOrder'));

            return $this->getResponse()->setBody($router->run());

        } catch (OrderNotFoundException $e) {
            return $this->sendErrorResponse($e, 404);
        } catch (Exception $e) {
            return $this->sendErrorResponse($e, 500);
        }

        return $this->errorResponse('Order is not updated');

    }

    /**
     * @return Selveo_Integration_Model_Order
     */
    private function getOrderModel() {
        return Mage::getModel('selveointegration/order');
    }

    /**
     * @param $request
     * @return string
     * @throws Selveo_Integration_Model_Exception_OrderNotFound
     */
    public function updateOrder(Mage_Core_Controller_Request_Http $request) {

        $requestBody = $this->getRequestHelper()->getJsonRawBody($request);

        $orderIncrementId = $request->getQuery('id');

        if($this->getOrderModel()->update($orderIncrementId, $requestBody)) {
            return "Order #{$orderIncrementId} was successfully updated";
        }

        return "Order #{$orderIncrementId} failed to be updated";

    }

}