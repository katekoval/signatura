<?php

use Selveo_Integration_Model_Exception_NoItemsArePreparedException as NoItemsArePreparedException;
use Selveo_Integration_Model_Exception_OrderNotFound as OrderNotFoundException;
use Selveo_Integration_Model_Exception_RequestedItemsNotFoundException as RequestedItemsNotFoundException;

class Selveo_Integration_OrderInvoiceController extends Selveo_Integration_Controller_Base
{
    protected $orderInvoiceModel;

    public function indexAction() {

        try {

            $router = $this->route($this->getRequest());
            $router->put(array($this, 'invoiceOrder'));

            return $this->getResponse()->setBody($router->run());

        } catch (OrderNotFoundException $e) {
            return $this->sendErrorResponse($e, 404);
        } catch (RequestedItemsNotFoundException $e) {
            return $this->sendErrorResponse($e);
        } catch (NoItemsArePreparedException $e) {
            return $this->sendErrorResponse($e);
        } catch (Mage_Core_Exception $e) {
            return $this->sendErrorResponse($e, 500);
        } catch (Exception $e) {
            return $this->sendErrorResponse($e, 500);
        }

        return $this->errorResponse('Order failed to be invoiced');

    }

    /**
     * @return Selveo_Integration_Model_Order_Invoice
     */
    protected function getOrderInvoiceModel() {
        return Mage::getModel('selveointegration/order_invoice');
    }

    /**
     * @param $request
     * @return string
     * @throws Selveo_Integration_Model_Exception_OrderNotFound
     */
    public function invoiceOrder(Mage_Core_Controller_Request_Http $request) {

        $requestBody = $this->getRequestHelper()->getJsonRawBody($request);

        $orderIncrementId = $request->getQuery('incrementId');

        if($this->getOrderInvoiceModel()->invoice($orderIncrementId, $requestBody)) {
            return "Order #{$orderIncrementId} invoiced successfully";
        }

        return "Order #{$orderIncrementId} failed to be invoiced";

    }

}