<?php

use Mage_Catalog_Model_Product_Type as ProductType;

class Selveo_Integration_Adminhtml_Selveointegration_ProductsController extends Selveo_Integration_Controller_Adminhtml_Base
{

    const PAGE_SIZE = 50;

    public function syncAction() {

        $currentPage = $this->getRequest()->getParam('currentPage');
        $this->saveCurrentPageToCache($currentPage);

        /** @var Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection $collection */
        $collection = Mage::getModel('catalog/product')->getCollection();

        $this->syncProducts($collection, $currentPage);

        $lastPage = $collection->getLastPageNumber();
        $this->setSuccessOnLastPage($currentPage, $lastPage);

        return $this->getResponse()
            ->setHeader('Content-Type', 'application/json')
            ->setHttpResponseCode(200)
            ->setBody(json_encode(array(
                'lastPage' => $lastPage
            )));

    }

    private function setSuccessOnLastPage($currentPage, $lastPage) {

        if ($currentPage == $lastPage) {
            $this->addSuccessToSession('The products are successfully synced to Selveo');
        }

    }

    private function saveCurrentPageToCache($currentPage) {

        /** @var Mage_Core_Model_Cache $cache */
        $cache = Mage::app()->getCache();

        $cache->save($currentPage, 'current_page', array('SELVEOINTEGRATION_CACHE'), null);

    }

    /**
     * @param Mage_Eav_Model_Entity_Collection_Abstract $collection
     * @param $currentPage
     */
    private function syncProducts(Mage_Eav_Model_Entity_Collection_Abstract $collection, $currentPage) {

        try {

            /** @var Mage_Eav_Model_Entity_Collection_Abstract $collection */
            $collection = $this->filterCollection($collection, $currentPage);

            /** @var Selveo_Integration_Model_Job_Handler $jobHandler */
            $jobHandler = Mage::getModel('selveointegration/job_handler');

            Mage::register('without_product_success', true);

            $jobHandler->handleNow(new Selveo_Integration_Model_Jobs_SyncProductsToSelveo($collection));

            /** @var Mage_Catalog_Model_Product $product */
            foreach($collection as $product){
                $jobHandler->handleNow(new Selveo_Integration_Model_Jobs_SyncImagesToSelveo($product));
            }

        } catch (Exception $e) {
            $this->getSession()->addError('Error syncing products with ids: ' . json_encode($this->getSkus($collection)));
        }

    }

    /**
     * @param $collection
     * @return mixed
     */
    private function getSkus($collection) {

        $skus = array();

        foreach ($collection as $product) {
            $skus[] = $product->getSku();
        }

        return $skus;
    }

    /**
     * @return Selveo_Integration_Helper_Config
     */
    protected function getConfigHelper() {
        return Mage::helper('selveointegration/config');
    }

    /**
     * @return Mage_Adminhtml_Model_Session
     */
    private function getSession() {
        return Mage::getSingleton('adminhtml/session');
    }

    /**
     * @param Mage_Eav_Model_Entity_Collection_Abstract $collection
     * @param $currentPage
     * @return Mage_Eav_Model_Entity_Collection_Abstract
     */
    private function filterCollection(
        Mage_Eav_Model_Entity_Collection_Abstract $collection,
        $currentPage
    ) {
        $collection
            ->addFieldToFilter('type_id',
                array(
                    'nin' => array(
                        ProductType::TYPE_BUNDLE,
                        ProductType::TYPE_GROUPED,
                        ProductType::TYPE_CONFIGURABLE
                    )
                )
            )
            ->setCurPage($currentPage)
            ->setPageSize(self::PAGE_SIZE);

        return Mage::getModel('selveointegration/mappers_product')->prepareCollection($collection);

    }

}