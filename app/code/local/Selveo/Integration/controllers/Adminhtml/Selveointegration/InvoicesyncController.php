<?php

use Selveo_Integration_Model_Jobs_SyncInvoiceToSelveo as SyncInvoiceToSelveo;

class Selveo_Integration_Adminhtml_Selveointegration_InvoicesyncController extends Selveo_Integration_Controller_Adminhtml_Base
{
    public function forceAction() {

        if (!$this->getConfigHelper()->canForceSync()) {
            return $this->redirectBack();
        }

        $invoiceId = $this->getRequest()->getParam('id');
        $invoice   = $this->getInvoiceModel()->load($invoiceId);

        if ($invoiceId === null || !$invoice->getData()) {

            $this->addErrorToSession("Error force syncing invoice: The invoice was not found");
            return $this->redirectBack();

        }

        return $this->forceSync([$invoice]);

    }

    private function forceSync($invoices) {

        try {

            foreach ($invoices as $invoice) {

                Selveo_Integration_Model_Job_Handler::instance()
                    ->setThrowExceptions(true)
                    ->handleNow(new SyncInvoiceToSelveo($invoice));

            }

            $this->addSuccessToSession('Successfully synced invoice(s) to Selveo');

        } catch (Exception $e) {
            $this->addExceptionErrorToSession($e);
        }

        return $this->redirectBack();

    }

    private function getInvoiceModel()
    {
        return Mage::getModel('sales/order_invoice');
    }

    /**
     * @return Selveo_Integration_Helper_Config
     */
    private function getConfigHelper()
    {
        return Mage::helper('selveointegration/config');
    }

}