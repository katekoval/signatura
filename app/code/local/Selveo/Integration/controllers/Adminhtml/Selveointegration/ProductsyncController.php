<?php

class Selveo_Integration_Adminhtml_Selveointegration_ProductsyncController extends Selveo_Integration_Controller_Adminhtml_Base
{
    public function forceAction() {

        if(!$this->getConfigHelper()->canForceSync()) {
            return $this->_redirectReferer();
        }

        $id = $this->getRequest()->getParam('id');

        $product = $this->getProductModel()->load($id);

        if($id === null || $product->getId() === null) {

            $this->addErrorToSession("Error when force syncing product: Product with id: ".$id." couldn't be found");
            return $this->_redirectReferer();

        }

        $this->forceSyncProducts([$product]);

        return $this->_redirectReferer();

    }

    public function massForceAction() {

        if(!$this->getConfigHelper()->canForceSync()) {
            return $this->_redirectReferer();
        }

        $productIds = $this->getRequest()->getParam('product');

        $products = $this->getProductModel()->loadByIds($productIds);

        if($products->count() === 0) {

            $this->addErrorToSession("No products are force synced to Selveo. No products could be found");
            return $this->_redirectReferer();

        }

        $this->forceSyncProducts($products);

        return $this->_redirectReferer();

    }

    /**
     * @param $products
     */
    private function forceSyncProducts($products) {

        try {

            $jobHandler = Selveo_Integration_Model_Job_Handler::instance()->setThrowExceptions(true);
            $jobHandler->handleNow(new Selveo_Integration_Model_Jobs_SyncProductsToSelveo($products));

            foreach ($products as $product) {
                $jobHandler->handleNow(new Selveo_Integration_Model_Jobs_SyncImagesToSelveo($product));
            }

        } catch (Exception $e) {

            $this->addExceptionErrorToSession($e);
            $this->_redirectReferer();

        }

    }

    /**
     * @return Selveo_Integration_Model_Product
     */
    private function getProductModel() {
        return Mage::getModel('selveointegration/product');
    }

    /**
     * @return Selveo_Integration_Helper_Config
     */
    private function getConfigHelper()
    {
        return Mage::helper('selveointegration/config');
    }

}