<?php

class Selveo_Integration_Adminhtml_Selveointegration_TestController extends Selveo_Integration_Controller_Adminhtml_Base
{
    public function indexAction()
    {
        $this->getJobHandler()->handleNow(new Selveo_Integration_Model_Jobs_TestConnectionToSelveo);

        return $this->redirectBack();
    }

    /**
     * @return Selveo_Integration_Model_Job_Handler
     */
    private function getJobHandler()
    {
        return Mage::getModel('selveointegration/job_handler');
    }
}