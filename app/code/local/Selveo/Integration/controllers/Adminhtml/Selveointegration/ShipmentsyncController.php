<?php

use Selveo_Integration_Model_Jobs_SyncShipmentToSelveo as SyncShipmentToSelveo;

class Selveo_Integration_Adminhtml_Selveointegration_ShipmentsyncController extends Selveo_Integration_Controller_Adminhtml_Base
{

    public function forceAction() {

        if (!$this->getConfigHelper()->canForceSync()) {
            return $this->redirectBack();
        }

        $shipmentId = $this->getRequest()->getParam('id');
        $shipment   = $this->getShipmentModel()->load($shipmentId);

        if ($shipmentId === null || !$shipment->getData()) {

            $this->addErrorToSession("Error when force syncing shipment: Shipment was not found");
            return $this->redirectBack();

        }

        return $this->forceSync([$shipment]);

    }

    private function forceSync($shipments) {

        try {

            foreach ($shipments as $shipment) {

                Selveo_Integration_Model_Job_Handler::instance()
                    ->setThrowExceptions(true)
                    ->handleNow(new SyncShipmentToSelveo($shipment));

            }

            $this->addSuccessToSession('Successfully synced shipment(s) to Selveo');

        } catch (Exception $e) {
            $this->addExceptionErrorToSession($e);
        }

        return $this->redirectBack();

    }

    /**
     * @return Mage_Sales_Model_Order_Shipment
     */
    private function getShipmentModel()
    {
        return Mage::getModel('sales/order_shipment');
    }

    /**
     * @return Selveo_Integration_Helper_Config
     */
    private function getConfigHelper()
    {
        return Mage::helper('selveointegration/config');
    }

}