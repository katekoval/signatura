<?php

use Selveo_Integration_Model_Jobs_SyncOrdersToSelveo as SyncOrdersToSelveo;

class Selveo_Integration_Adminhtml_Selveointegration_OrdersyncController extends Selveo_Integration_Controller_Adminhtml_Base
{
    public function forceAction() {

        if (!$this->getConfigHelper()->canForceSync()) {
            return $this->redirectBack();
        }

        $id    = $this->getRequest()->getParam('id');
        $order = $this->getSalesOrderModel()->load($id);

        if ($id === null || !$order->getData()) {

            $this->addErrorToSession("Error when force syncing order to Selveo: Order couldn't be found");
            return $this->redirectBack();

        }

        return $this->forceSyncOrders(array($order));

    }

    public function massforceAction()
    {
        $orderIds = $this->getRequest()->getParam('order_ids');
        $orders   = $this->getSalesOrderModel()->loadByIds($orderIds);

        if (!$this->getConfigHelper()->canForceSync()) {
            return $this->redirectBack();
        }

        if ($orders->count() === 0) {

            $this->addNoticeToSession('No orders are force synced to Selveo. No orders could be found');
            return $this->redirectBack();

        }

        return $this->forceSyncOrders($orders);

    }

    /**
     * @param $orders
     * @return Mage_Adminhtml_Controller_Action
     */
    private function forceSyncOrders($orders) {

        try {

            Selveo_Integration_Model_Job_Handler::instance()
                ->setThrowExceptions(true)
                ->handleNow((new SyncOrdersToSelveo($orders))
                ->showSuccessMessages());

        } catch (Exception $e) {
            $this->addExceptionErrorToSession($e);
        }

        return $this->redirectBack();
    }

    /**
     * @return Selveo_Integration_Model_Sales_Order
     */
    private function getSalesOrderModel()
    {
        return Mage::getModel('selveointegration/sales_order');
    }

    /**
     * @return Selveo_Integration_Helper_Config
     */
    private function getConfigHelper()
    {
        return Mage::helper('selveointegration/config');
    }

}