<?php require_once(__DIR__.'/../../../../../Mage.php');

error_reporting(E_ALL);
ini_set('display_errors', 1);

class Ignore{
    const CACHE_KEY = "selveoIntegrationIgnore";

    protected $cache;

    public function __construct()
    {
        $this->cache = Mage::app()->getCache();
    }

    public function isIgnored($orderIncrementId)
    {
        return in_array($orderIncrementId, $this->getIgnored());
    }

    public function ignore($orderIncrementId)
    {
        $current = $this->getIgnored();
        $current[] = $orderIncrementId;
        $this->setIgnored($current);
    }

    public function setIgnored($ignored)
    {
        $this->cache->save(json_encode($ignored), static::CACHE_KEY);
    }

    public function getIgnored()
    {
        return json_decode($this->cache->load(static::CACHE_KEY), true) ?: [];
    }
}

/*
 * Create a function which allows easily printing a progressbar.
 */
if(!function_exists('print_progress'))
{
    function print_progress($x, $outOf)
    {
        $percent = round($x/$outOf * 100);
        if($x === $outOf) $percent = 100;
        echo "\r|". str_repeat("+", $percent) . str_repeat("-", max(0, 100-$percent)) . "| {$percent}% [{$x}/{$outOf}] ";
        flush();
    }
}

Mage::init();


/*
 * Script configuration
 */
$chunkSize = 50;

$ignored = new Ignore;

/**
 * First of all, build a store ID filter to reflect progress correctly.
 */
$storeFilter = [];

foreach(Mage::getModel('core/store')->getCollection() as $store) {

    if (!Mage::helper('selveointegration/config')->getModuleEnabled($store)) {
        continue;
    }

    $storeFilter[] = $store->getId();
}

/*
 * Print the initial progressbar.
 */
$synced   = 0;

/** @var Selveo_Integration_Helper_Date $dateHelper */
$dateHelper = Mage::helper('selveointegration/date');

$orders = Mage::getModel('sales/order')->getCollection()
    ->addAttributeToFilter('status', ['eq' => 'complete'])
    ->addAttributeToFilter('state', ['eq' => 'complete'])
    ->addFieldToFilter('store_id', ['in' => $storeFilter])
    ->addFieldToFilter('created_at', [
        'from' => $dateHelper->gmtDate(null, $dateHelper->zend()->sub(30, Zend_Date::WEEK)),
        'to'   => $dateHelper->now(),
    ])
    ->addFieldToFilter('is_virtual', 0)
    ->addFieldToFilter('increment_id', ['nin' => $ignored->getIgnored() ?: [0]])
    ->addFieldToFilter('selveo_synced', ['null' => true]);

$orders->getSelect()->order("created_at", "DESC");

$size = $orders->getSize();

print_progress($synced, $size);
if($size == 0){
    echo "\nNo orders to sync\n";
  exit(2);
}

/**
 * @param $storeId
 * @param $ignored
 * @param $chunkSize
 * @param $ordersCurrentPage
 * @return mixed
 * @throws Zend_Date_Exception
 */
function getOrders($storeId, $ignored, $chunkSize, $ordersCurrentPage) {

    /** @var Selveo_Integration_Helper_Date $dateHelper */
    $dateHelper = Mage::helper('selveointegration/date');

   $orders =  Mage::getModel('sales/order')->getCollection()
        ->addAttributeToFilter('status', ['eq' => 'complete'])
        ->addAttributeToFilter('state', ['eq' => 'complete'])
        ->addFieldToFilter('store_id', ['eq' => $storeId])
        ->addFieldToFilter('increment_id', ['nin' => $ignored->getIgnored() ?: [0]])
        ->addFieldToFilter('created_at', [
            'from' => $dateHelper->gmtDate(null, $dateHelper->zend()->sub(30, Zend_Date::WEEK)),
            'to'   => $dateHelper->now(),
        ])
        ->addFieldToFilter('is_virtual', 0)
        ->addFieldToFilter('selveo_synced', ['null' => true])
        ->setPageSize($chunkSize)
        ->setCurPage($ordersCurrentPage);

	$orders->getSelect()->order("created_at", "DESC");

	return $orders;

}

/*
 * Iterate over all stores and sync orders.
 */
foreach(Mage::getModel('core/store')->getCollection() as $store) {

    if(!Mage::helper('selveointegration/config')->getModuleEnabled($store)) continue;

    Mage::app()->setCurrentStore($store);

    $storeId = $store->getId();

    /** @var Selveo_Integration_Model_Installation_Factory $installationFactory */
    $installationFactory = Mage::getModel('selveointegration/installation_factory');

    $ordersCurrentPage = 1;

    do {

        $partialSyncErrors = array();

        /** @var Mage_Sales_Model_Entity_Order_Collection $orderCollection */
        $orderCollection = getOrders($storeId, $ignored, $chunkSize, $ordersCurrentPage++);

        if($orderCollection->count() === 0) {
            break;
        }

        $currentOrderIds = [];
        foreach($orderCollection as $order){
            $currentOrderIds[] = $order->getId();
        }

        try {

	$start = microtime(true);
            $installationFactory
                ->getForStore($store)
                ->getClient()
                ->addGracefulFailsHeader()
                ->setTimeout(50)
                ->orders()
                ->store($orderCollection);
	echo sprintf("%d ms API call with %d orders", (microtime(true) - $start)* 1000, $orderCollection->count());

            echo PHP_EOL.$orderCollection->getFirstItem()->getIncrementId() . PHP_EOL;

        } catch (Selveo_Integration_Model_Exception_Client_PartialSyncFailure $e) {

            foreach($e->getIds() as $failedId){

                $ignored->ignore($failedId);
                $partialSyncErrors = array_merge($partialSyncErrors, $e->getFailed());

            }


        } catch (Exception $e) {
            fwrite(STDERR, "\rFailed synchronizing orders: ".get_class($e)." ".$e->getMessage().PHP_EOL);
            continue;
        }

        if(count($partialSyncErrors) > 0) {
            fwrite(STDERR, 'Partial sync errors: '.json_encode($partialSyncErrors));
        }

        $synced += $orderCollection->count();

        print_progress($synced, $size);

        echo PHP_EOL."Sleeping now, ctrl+C if you wanna cancel".PHP_EOL;
        sleep(2);
        echo PHP_EOL."Resuming...".PHP_EOL;

    } while ($ordersCurrentPage <= $orderCollection->getLastPageNumber());

}


if($synced === 0) exit(1);
echo PHP_EOL;

