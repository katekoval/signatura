<?php require_once(__DIR__.'/../../../../../Mage.php');

error_reporting(E_ALL);
ini_set('display_errors', 1);

Mage::app();
Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

$product = Mage::getModel('catalog/product');
$product
    ->setWebsiteIds(array(1)) //website ID the product is assigned to, as an array
    ->setAttributeSetId(4)
    //->setAttributeSetId(67) //ID of a attribute set named 'default'
    ->setTypeId('simple') //product type
    ->setCreatedAt(strtotime('now')) //product creation time
    ->setSku('testing-selveo'.date('His')) //SKU
    ->setName('testing-selveo') //product name
    ->setDescription('This is a long description')
    ->setShortDescription('This is a short description')
    ->setWeight(8.00)
    ->setStatus(1) //product status (1 - enabled, 2 - disabled)
    ->setTaxClassId(4) //tax class (0 - none, 1 - default, 2 - taxable, 4 - shipping)
    ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH) //catalog and search visibility
    ->setManufacturer(28) //manufacturer id
    ->setColor(24)
    ->setNewsFromDate(strtotime('now')) //product set as new from
    ->setNewsToDate('06/30/2015') //product set as new to
    ->setCountryOfManufacture('AF') //country of manufacture (2-letter country code)
    ->setPrice(11.22) //price in form 11.22
    ->setCost(11.11) //price in form 11.22
    ->setStockData(array(
            'use_config_manage_stock' => 0, //'Use config settings' checkbox
            'manage_stock'=>1, //manage stock
            'min_sale_qty'=>1, //Minimum Qty Allowed in Shopping Cart
            'max_sale_qty'=>1, //Maximum Qty Allowed in Shopping Cart
            'is_in_stock' => 1, //Stock Availability
            'qty' => 1 //qty
        )
    );

$product->save();

/** @var Selveo_Integration_Model_Job_Handler $handler */
$handler = Mage::getSingleton('selveointegration/job_handler');

$handler->flushQueue();

echo "Product: is created".PHP_EOL;