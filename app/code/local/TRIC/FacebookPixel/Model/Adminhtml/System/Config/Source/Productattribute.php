<?php
/**
* Magento Extension by TRIC Solutions
*
* @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
* @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
* @store       http://store.tric.dk
*/

class TRIC_FacebookPixel_Model_Adminhtml_System_Config_Source_Productattribute
{
    public function toOptionArray()
    {
        return array(
            array(
	            'value' => 'entity_id', 
	            'label' => Mage::helper('facebookpixel')->__('ID')
            ),  
            array(
	            'value' => 'sku', 
	            'label' => Mage::helper('facebookpixel')->__('SKU')
            )
        );
    }
}