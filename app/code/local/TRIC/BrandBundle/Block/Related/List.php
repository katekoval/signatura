<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */
 
class TRIC_BrandBundle_Block_Related_List extends Mage_Catalog_Block_Product_List
{
    public function getLoadedProductCollection() {
        
        if(!$product = $this->getProduct()) {
            $product = Mage::registry('current_product');
        }
        if(!$product->getManufacturer()) {
            return Mage::getModel('catalog/product')->getCollection()->addFieldToFilter('entity_id',-1);
        } 
        return Mage::getModel('catalog/product')->getCollection()
                ->addAttributeToSelect('*')
                ->addFieldToFilter('type_id','simple')
                ->addFieldToFilter('manufacturer',$product->getManufacturer())
                //->addFinalPrice()
                ->setOrder('name','ASC');
        
    }
    
    
    public function getLowestUnitPrice() {
        
        if(!$this->getProduct()->hasData('manufacturer')) {
            $manufacturer = Mage::getResourceModel('catalog/product')->getAttributeRawValue($this->getProduct()->getId(), 'manufacturer',$this->getProduct()->getStoreId());
            $this->getProduct()->setData('manufacturer',$manufacturer);
        }
        $lowestUnitPrice = false;
        foreach($this->getLoadedProductCollection() as $product) {
            
            if(!$product->isSaleable()) {
                continue;
            }
            
            $tierPrice = (float)$product->setData('tier_price',null)->getTierPrice(6);
            if($lowestUnitPrice === false || $tierPrice < $lowestUnitPrice) {
                $lowestUnitPrice = $tierPrice;
            }
        }
        return $lowestUnitPrice;
    }
    
}