<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */
 
class TRIC_BrandBundle_Block_Sales_Order_Items_Renderer extends Mage_Bundle_Block_Sales_Order_Items_Renderer
{
    public function getSelectionAttributes($item) {
        
        $options = parent::getSelectionAttributes($item);
                
        if(isset($options['price'])) {
            $options['price'] = $item->getPriceInclTax();
        }
        
        return $options;
    }
}
