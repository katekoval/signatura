<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */
 
$installer = $this;
$installer->startSetup();
Mage::log("running installer",null,'brandbundle.log',true);
try
{
	$installer->run("
    	CREATE TABLE IF NOT EXISTS {$this->getTable('catalog_product_brandbundle_option')} (	
          `option_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Option Id',
          `parent_id` int(10) unsigned NOT NULL COMMENT 'Parent Id',
          `required` smallint(5) unsigned NOT NULL DEFAULT 0 COMMENT 'Required',
          `position` int(10) unsigned NOT NULL DEFAULT 0 COMMENT 'Position',
          `type` varchar(255) DEFAULT NULL COMMENT 'Type',
          PRIMARY KEY (`option_id`),
          KEY `IDX_CATALOG_PRODUCT_BUNDLE_OPTION_PARENT_ID` (`parent_id`),
          CONSTRAINT `catalog_product_brandbundle_option_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Option';
	");
} catch(Exception $e) {
	Mage::log($e->getMessage(),null,'brandbundle.log',true);
}

try
{
	$installer->run("
    	CREATE TABLE IF NOT EXISTS {$this->getTable('catalog_product_brandbundle_option_value')} (	
          `value_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Value Id',
          `option_id` int(10) unsigned NOT NULL COMMENT 'Option Id',
          `store_id` smallint(5) unsigned NOT NULL COMMENT 'Store Id',
          `title` varchar(255) DEFAULT NULL COMMENT 'Title',
          PRIMARY KEY (`value_id`),
          UNIQUE KEY `UNQ_CATALOG_PRODUCT_BRANDBUNDLE_OPTION_VALUE_OPTION_ID_STORE_ID` (`option_id`,`store_id`),
          CONSTRAINT `catalog_product_brandbundle_option_value_ibfk_1` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_brandbundle_option` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Option Value';  
	");
} catch(Exception $e) {
	Mage::log($e->getMessage(),null,'brandbundle.log',true);
}

try
{
	$installer->run("
    	CREATE TABLE IF NOT EXISTS {$this->getTable('catalog_product_brandbundle_selection')} (	
          `selection_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Selection Id',
          `option_id` int(10) unsigned NOT NULL COMMENT 'Option Id',
          `parent_product_id` int(10) unsigned NOT NULL COMMENT 'Parent Product Id',
          `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
          `position` int(10) unsigned NOT NULL DEFAULT 0 COMMENT 'Position',
          `is_default` smallint(5) unsigned NOT NULL DEFAULT 0 COMMENT 'Is Default',
          `selection_price_type` smallint(5) unsigned NOT NULL DEFAULT 0 COMMENT 'Selection Price Type',
          `selection_price_value` decimal(12,4) NOT NULL DEFAULT 0.0000 COMMENT 'Selection Price Value',
          `selection_qty` decimal(12,4) DEFAULT NULL COMMENT 'Selection Qty',
          `selection_can_change_qty` smallint(6) NOT NULL DEFAULT 0 COMMENT 'Selection Can Change Qty',
          PRIMARY KEY (`selection_id`),
          KEY `IDX_CATALOG_PRODUCT_BUNDLE_SELECTION_OPTION_ID` (`option_id`),
          KEY `IDX_CATALOG_PRODUCT_BUNDLE_SELECTION_PRODUCT_ID` (`product_id`),
          CONSTRAINT `catalog_product_brandbundle_selection_ibfk_1` FOREIGN KEY (`option_id`) REFERENCES `catalog_product_brandbundle_option` (`option_id`) ON DELETE CASCADE ON UPDATE CASCADE,
          CONSTRAINT `catalog_product_brandbundle_selection_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `catalog_product_entity` (`entity_id`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Selection';  
	");
} catch(Exception $e) {
	Mage::log($e->getMessage(),null,'brandbundle.log',true);
}

try
{
	$installer->run("
    	CREATE TABLE IF NOT EXISTS {$this->getTable('catalog_product_brandbundle_selection_price')} (	
          `selection_id` int(10) unsigned NOT NULL COMMENT 'Selection Id',
          `website_id` smallint(5) unsigned NOT NULL COMMENT 'Website Id',
          `selection_price_type` smallint(5) unsigned NOT NULL DEFAULT 0 COMMENT 'Selection Price Type',
          `selection_price_value` decimal(12,4) NOT NULL DEFAULT 0.0000 COMMENT 'Selection Price Value',
          PRIMARY KEY (`selection_id`,`website_id`),
          KEY `IDX_CATALOG_PRODUCT_BUNDLE_SELECTION_PRICE_WEBSITE_ID` (`website_id`),
          CONSTRAINT `catalog_product_brandbundle_selection_price_ibfk_1` FOREIGN KEY (`website_id`) REFERENCES `core_website` (`website_id`) ON DELETE CASCADE ON UPDATE CASCADE,
          CONSTRAINT `catalog_product_brandbundle_selection_price_ibfk_2` FOREIGN KEY (`selection_id`) REFERENCES `catalog_product_bundle_selection` (`selection_id`) ON DELETE CASCADE ON UPDATE CASCADE
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Catalog Product Bundle Selection Price';  
	");
} catch(Exception $e) {
	Mage::log($e->getMessage(),null,'brandbundle.log',true);
}

$installer->endSetup();
