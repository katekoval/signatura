<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */
 
$installer = $this;
$installer->startSetup();
Mage::log("running installer",null,'brandbundle.log',true);
try
{
	// Update price_type attribute to also apply on brandbundles
	$applyTo = explode(',', $installer->getAttribute('catalog_product', 'price_type', 'apply_to'));
	if (!in_array('brandbundle', $applyTo)) {
		$applyTo[] = 'brandbundle';
	    $installer->updateAttribute('catalog_product', 'price_type', 'apply_to', join(',', $applyTo));
	}
	
	// Update existing brandbundles to price_type=0
	$products = Mage::getModel("catalog/product")->getCollection()->addFieldToFilter('type_id', 'brandbundle');
	if ($brandbundleIds = $products->getAllIds()) {
		Mage::getSingleton('catalog/product_action')->updateAttributes($brandbundleIds, ['price_type' => 0], 0);
	}
	
} catch(Exception $e) {
	Mage::log($e->getMessage(),null,'brandbundle.log',true);
}
$installer->endSetup();
