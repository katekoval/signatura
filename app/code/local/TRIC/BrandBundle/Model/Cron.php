<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */
 
class TRIC_BrandBundle_Model_Cron
{
    
    public function updateBrandBundleSelectionsAndOptions() {
        
        
        $collection = Mage::getModel('catalog/product')->getCollection()
                        ->addAttributeToSelect('manufacturer')
                        ->addAttributeToFilter('type_id','brandbundle');
        
        foreach($collection as $product) {
            Mage::helper('brandbundle')->handleAndCheckBrandBundleSelectionsAndOptions($product);
            
        }
        
        
    }
    
}