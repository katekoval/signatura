<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

class TRIC_BrandBundle_Model_Product_Selection extends Varien_Object {
    
    
    public function isSalable() {
        return $this->isSaleable();
    }
    
    public function isSaleable() {
        return $this->getData('is_saleable');   
    }
}