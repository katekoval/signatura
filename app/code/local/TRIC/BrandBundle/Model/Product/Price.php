<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */
 
class TRIC_BrandBundle_Model_Product_Price extends Mage_Bundle_Model_Product_Price
{
    public function getSelectionFinalTotalPrice($bundleProduct, $selectionProduct, $bundleQty, $selectionQty,
                                                $multiplyQty = true, $takeTierPrice = true)
    {
        
        $price = $selectionProduct->getPrice();
        $customOption = $bundleProduct->getCustomOption('bundle_selection_ids');
        if ($customOption) {
            $selectionIds = unserialize($customOption->getValue());
                
            $bundleCount = count($selectionIds);

            $price = (float)$selectionProduct->getFinalPrice($bundleCount);
            //Zend_debug::dump($selectionProduct->getTierPrice($bundleCount));
/*
            $resource = Mage::getSingleton('core/resource');
            $read = $resource->getConnection('core_read');
            $table = $resource->getTableName('brandbundle/prices');
            $query = "SELECT discount_percentage FROM $table WHERE sku = :sku AND qty = :qty";
            
            $binds = array('sku' => $selectionProduct->getSku(), 'qty' => $bundleCount);
            if($discountPercentage = $read->fetchOne($query,$binds)) {
                $price *= 1-($discountPercentage/100);
            }
*/
            
        }   
        
        return $price;
    }   
}