<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */

class TRIC_BrandBundle_Model_Selection extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('brandbundle/selection');
        parent::_construct();
    }
}