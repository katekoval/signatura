<?php
/**
 * @copyright Copyright © TRIC Solutions. All rights reserved.
 * @license   https://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @link      https://www.tric.dk
 */
 
class TRIC_BrandBundle_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function isProductPartOfBrandBundle($product = null) {
        $brandBundle = false;
        
        if(is_null($product)) {
            $product = Mage::registry('current_product');
        }
        
        if($product) {
            
            $resource = Mage::getSingleton('core/resource');
            $read = $resource->getConnection('core_read');
            $catalog_product_brandbundle_selection = $resource->getTableName('brandbundle/selection');;
            $query = "SELECT parent_product_id FROM $catalog_product_brandbundle_selection WHERE product_id = :product_id LIMIT 1";
            
            if($parentProductId = $read->fetchOne($query,array('product_id' => $product->getId()))) {
                $brandBundle = Mage::getModel('catalog/product')->load($parentProductId); 
            }
        }

        return $brandBundle;
    }
    
    
    public function handleAndCheckBrandBundleSelectionsAndOptions($product) {
        
        
        if($manufacturer = $product->getManufacturer()) {
            $optionCollection = Mage::getModel('brandbundle/option')->getCollection()
                            ->addFieldToFilter('parent_id',$product->getId());
            if($optionCollection->getSize() != 6) {
                $i = $optionCollection->getSize();
                while($i < 6) {
                    $i++;
                    $option = Mage::getModel('brandbundle/option')
                        ->setParentId($product->getId())
                        ->setRequired(0)
                        ->setPosition(0)
                        ->setStoreId(0)
                        ->setTitle($i.')')
                        ->save();
                        
                    $optionCollection->addItem($option);
                }
            }
            
            $resource = Mage::getSingleton('core/resource');
            $read = $resource->getConnection('core_read');
            $catalog_product_brandbundle_selection = $resource->getTableName('brandbundle/selection');;
            $query = "SELECT option_id,product_id FROM $catalog_product_brandbundle_selection WHERE parent_product_id = :parent_product_id";
            
            $results = $read->fetchAll($query,array('parent_product_id' => $product->getId()));
            $currentSelections = array();
            foreach($results as $row) {
                $currentSelections[$row['option_id']][$row['product_id']] = true;
            }
            
            $productCollection = Mage::getModel('catalog/product')->getCollection()
                                    ->addAttributeToFilter('manufacturer',$manufacturer)
                                    ->addFieldToFilter('type_id','simple');

            foreach($optionCollection as $option) {
                
                foreach($productCollection as $child) {
                    if(isset($currentSelections[$option->getId()][$child->getId()])) {
                        continue;
                    }
                    Mage::getModel('brandbundle/selection')
                        ->setOptionId($option->getId())
                        ->setParentProductId($product->getId())
                        ->setProductId($child->getId())
                        ->setSelectionQty(1)
                        ->save();
                }
            }
            
            // FINAL CLEAN UP
            $resource = Mage::getSingleton('core/resource');
            $write = $resource->getConnection('core_write');
            $optionIds = $optionCollection->getAllIds();
            $productIds = $productCollection->getAllIds();
            if($productIds && $optionIds) {
                                    
                $productIds = implode(',',$productIds);
                $optionIds = implode(',',$optionIds);
                    
                
                $catalog_product_brandbundle_selection = $resource->getTableName('brandbundle/selection');;
                $query = "DELETE FROM $catalog_product_brandbundle_selection WHERE parent_product_id = :parent_product_id AND option_id IN ($optionIds) AND product_id NOT IN ($productIds)";

                $write->query($query,array('parent_product_id' => $product->getId()));
            } else {
                $query = "DELETE FROM $catalog_product_brandbundle_selection WHERE parent_product_id = :parent_product_id";
                $write->query($query,array('parent_product_id' => $product->getId()));
            }
                
        } else { // CLEAN UP!
            $resource = Mage::getSingleton('core/resource');
            $write = $resource->getConnection('core_write');
            
            $catalog_product_brandbundle_option = $resource->getTableName('brandbundle/option');
            $query = "DELETE FROM $catalog_product_brandbundle_option WHERE parent_id = :parent_id";
            $write->query($query,array('parent_id' => $product->getId()));
            
            $catalog_product_brandbundle_selection = $resource->getTableName('brandbundle/selection');
            $query = "DELETE FROM $catalog_product_brandbundle_option WHERE parent_product_id = :parent_product_id";
            $write->query($query,array('parent_product_id' => $product->getId()));
        }
        
    }
} 