<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

define('ECONOMIC_WDSL_URL', 'https://api.e-conomic.com/secure/api1/EconomicWebService.asmx?wsdl');
define('ECONOMIC_APP_ID', 'Uz4VRhDaY8T7HTiMAMcbh69qFbVm1qy9bpDE18n+eH8=');

class TRIC_Economic_Model_Api
{
	private $client = null;
	public $storeId = 0;
	public $connected;
	
	public function connect(){
		$currentStoreId = Mage::app()->getStore()->getId();
		
		if($store = Mage::app()->getRequest()->getParam('store'))
		{
		    $storeCollection = Mage::getModel('core/store')->getCollection()->addFieldToFilter('code', $store);        
		    $this->storeId = $storeCollection->getFirstItem()->getStoreId();
		    
		    $connectionType = Mage::helper('economic')->getConfig('general/connection_type',$this->storeId);
			$token = Mage::helper('economic')->getConfig('general/api_token',$this->storeId);
			$agreementnumber = Mage::helper('economic')->getConfig('general/agreementnumber',$this->storeId);
			$userid = Mage::helper('economic')->getConfig('general/userid',$this->storeId);
			$password = Mage::helper('economic')->getConfig('general/password',$this->storeId);
			
			// Administrator Login
		    $adminAgreementnumber = Mage::helper('economic')->getConfig('general/admin_agreementnumber',$this->storeId);
		    $adminUserid = Mage::helper('economic')->getConfig('general/admin_userid',$this->storeId);
		    $adminPassword = Mage::helper('economic')->getConfig('general/admin_password',$this->storeId);
		    $adminClientAgreementNumber = Mage::helper('economic')->getConfig('general/admin_client_agreementnumber',$this->storeId);
		}
		else{
			$connectionType = Mage::helper('economic')->getConfig('general/connection_type');
			$token = Mage::helper('economic')->getConfig('general/api_token');
			$agreementnumber = Mage::helper('economic')->getConfig('general/agreementnumber');
			$userid = Mage::helper('economic')->getConfig('general/userid');
			$password = Mage::helper('economic')->getConfig('general/password');
			
			// Administrator Login
		    $adminAgreementnumber = Mage::helper('economic')->getConfig('general/admin_agreementnumber');
		    $adminUserid = Mage::helper('economic')->getConfig('general/admin_userid');
		    $adminPassword = Mage::helper('economic')->getConfig('general/admin_password');
		    $adminClientAgreementNumber = Mage::helper('economic')->getConfig('general/admin_client_agreementnumber');
		}
		
		if($this->storeId == 0 && $website = Mage::app()->getRequest()->getParam('website')){
			$websiteCollection = Mage::getModel('core/website')->getCollection()->addFieldToFilter('code', $website);
			$websiteId = $websiteCollection->getFirstItem()->getWebsiteId();
		    
		    $connectionType = Mage::app()->getWebsite($websiteId)->getConfig('economic/general/connection_type');
		    $token = Mage::app()->getWebsite($websiteId)->getConfig('economic/general/api_token');
		    $agreementnumber = Mage::app()->getWebsite($websiteId)->getConfig('economic/general/agreementnumber');
		    $userid = Mage::app()->getWebsite($websiteId)->getConfig('economic/general/userid');
		    $password = Mage::app()->getWebsite($websiteId)->getConfig('economic/general/password');
		    
		    // Administrator Login
		    $adminAgreementnumber = Mage::app()->getWebsite($websiteId)->getConfig('economic/general/admin_agreementnumber');
		    $adminUserid = Mage::app()->getWebsite($websiteId)->getConfig('economic/general/admin_userid');
		    $adminPassword = Mage::app()->getWebsite($websiteId)->getConfig('economic/general/admin_password');
		    $adminClientAgreementNumber = Mage::app()->getWebsite($websiteId)->getConfig('economic/general/admin_client_agreementnumber');
		}
				
		if((is_null($this->client) && !$this->connected) || ($this->storeId != $currentStoreId)){
			if($connectionType == 'token'){
				if($token){
					$this->client = $this->connectByToken($token);
				}
			}
			elseif($connectionType == 'administrator'){
				if($adminAgreementnumber && $adminUserid && $adminPassword && $adminClientAgreementNumber){
					$this->client = $this->connectAsAdministrator($adminAgreementnumber,$adminUserid,$adminPassword,$adminClientAgreementNumber);
				}
			}
			else{
				if($agreementnumber && $userid && $password){
					$this->client = $this->connectByCredentials($agreementnumber,$userid,$password);
				}
			}
			
			if($this->client){
				$this->connected = true;
			}
		}
		
		$this->storeId = $currentStoreId;
	}
	
	public function getClient(){
		return $this->client;
	}
	
	public function connectByCredentials($agreementnumber,$userid,$password) {
		try
		{
			$this->client = new SoapClient(ECONOMIC_WDSL_URL, array(
				'stream_context' => stream_context_create(array('http' => array('header' => 'X-EconomicAppIdentifier: '.ECONOMIC_APP_ID)))
			));
			$this->client->Connect(array('agreementNumber'=>$agreementnumber, 'userName'=>$userid, 'password'=>$password));
			return $this->client;
		}
		catch(exception $e) {
			$this->client = 0;
			return false;
		}
		
		return false;
	}
	
	public function connectAsAdministrator($agreementnumber,$userid,$password,$clientAgreementNumber) {
		try
		{
			$this->client = new SoapClient(ECONOMIC_WDSL_URL, array(
				'stream_context' => stream_context_create(array('http' => array('header' => 'X-EconomicAppIdentifier: '.ECONOMIC_APP_ID)))
			));
			$this->client->ConnectAsAdministrator(array('adminAgreementNo'=>$agreementnumber, 'adminUserID'=>$userid, 'adminUserPassword'=>$password, 'clientAgreementNo'=>$clientAgreementNumber));
			return $this->client;
		}
		catch(exception $e) {
			$this->client = 0;
			return false;
		}
		
		return false;
	}

	public function connectByToken($token) {
		try{
			$this->client = new SoapClient(ECONOMIC_WDSL_URL);
			$this->client->ConnectWithToken(array('token' => $token, 'appToken' => ECONOMIC_APP_ID));
			return $this->client;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			$this->client = 0;
			return false;
		}
	}

	public function testConnection($token='',$agreementnumber='',$userid='',$password='',$adminAgreementnumber='',$adminUserid='',$adminPassword='',$adminClientAgreementNumber='')
	{
		if($token){
			return $this->connectByToken($token);
		}
		elseif($agreementnumber && $userid && $password){
			return $this->connectByCredentials($agreementnumber,$userid,$password);
		}
		elseif($adminAgreementnumber && $adminUserid && $adminPassword && $adminClientAgreementNumber){
			return $this->connectAsAdministrator($adminAgreementnumber,$adminUserid,$adminPassword,$adminClientAgreementNumber);
		}
		else{
			$this->client = 0;
			return false;
		}
	}
	
	
	/*** ORDER ***/
	
	public function Order_FindByNumber($number){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->Order_FindByNumber(array('number' => $number));
			return $response->Order_FindByNumberResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Order_CreateFromData($orderData){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->Order_CreateFromData(array('data' => $orderData));
			return $response->Order_CreateFromDataResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Order_UpdateFromData($orderData){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->Order_UpdateFromData(array('data' => $orderData));
			return $response->Order_UpdateFromDataResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function OrderLine_CreateFromData($lineData){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->OrderLine_CreateFromData(array('data' => $lineData));
			return $response->OrderLine_CreateFromDataResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			Mage::log('orderLineDataArray: '.print_r($lineData,true),null,'economic.log',true);
			return false;
		}
	}
	
	public function OrderLine_CreateFromDataArray($orderLineDataArray){
		
		Mage::log('$orderLineDataArray: '.serialize($orderLineDataArray),null,'economic.log',true);
		
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->OrderLine_CreateFromDataArray(array('dataArray' => $orderLineDataArray));
			if(!isset($response->OrderLine_CreateFromDataArrayResult) || !isset($response->OrderLine_CreateFromDataArrayResult->OrderLineHandle)){
				return false;
			}
			return $response->OrderLine_CreateFromDataArrayResult;
		}
		catch(Exception $e){
			Mage::log('ERROR OrderLine_CreateFromDataArray: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function OrderLine_SetInventoryLocation($orderId,$itemNumber,$location){
		if(!$this->connected){
			return false;
		}
		
		$orderLineHandle = new stdClass();
		$orderLineHandle->Id = $orderId;
		$orderLineHandle->Number = $itemNumber;
		
		$valueHandle = new stdClass();
		$valueHandle->Number = $location;
		
		try{
			$response = $this->client->OrderLine_SetInventoryLocation(array('orderLineHandle' => $orderLineHandle, 'valueHandle' => $valueHandle));
			return $response->OrderLine_SetInventoryLocationResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function OrderLine_SetDepartment($orderId,$itemNumber,$department){
		if(!$this->connected){
			return false;
		}
		
		$orderLineHandle = new stdClass();
		$orderLineHandle->Id = $orderId;
		$orderLineHandle->Number = $itemNumber;
		
		$valueHandle = new stdClass();
		$valueHandle->Number = $department;
		
		try{
			$response = $this->client->OrderLine_SetDepartment(array('orderLineHandle' => $orderLineHandle, 'valueHandle' => $valueHandle));
			return $response->OrderLine_SetDepartmentResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function OrderLine_FindByOrderList($orderId){
		if(!$this->connected){
			return false;
		}
		
		$orderHandle = new stdClass();
		$orderHandle->Id = $orderId;
		
		try{
			$response = $this->client->OrderLine_FindByOrderList(array('orderHandles' => array($orderHandle)));
			if(isset($response->OrderLine_FindByOrderListResult->OrderLineHandle)){
				return $response->OrderLine_FindByOrderListResult->OrderLineHandle;
			}
			return array();
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function OrderLine_Delete($orderLineHandle){
		if(!$this->connected){
			return false;
		}
		
		try{
			$this->client->OrderLine_Delete(array('orderLineHandle' => $orderLineHandle));
			return true;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Order_UpgradeToInvoice($orderId){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->Order_UpgradeToInvoice(array('orderHandle'=>array('Id'=>$orderId)));
			return $response->Order_UpgradeToInvoiceResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Order_Delete($orderId){
		if(!$this->connected){
			return false;
		}
		
		try{
			return $this->client->Order_Delete(array('orderHandle'=>array('Id'=>$orderId)));
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Order_SetYourReference($orderId,$contactId){
		if(!$this->connected){
			return false;
		}
		
		try{
			return $this->client->Order_SetYourReference(array('orderHandle'=>array('Id'=>$orderId),'valueHandle'=>array('Id'=>$contactId)));
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Order_SetOurReference($orderId,$employeeId){
		if(!$this->connected){
			return false;
		}
		
		try{
			return $this->client->Order_SetOurReference(array('orderHandle'=>array('Id'=>$orderId),'valueHandle'=>array('Number'=>$employeeId)));
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Order_GetDataArray($numbers){
		if(!$this->connected){
			return false;
		}
		
		$orderHandles = new StdClass();
		$orderHandles->OrderHandle = array();
		
		if(!is_array($numbers)){
			$numbers = explode(',', $numbers);
		}
		
		foreach($numbers as $number){
			$orderHandle = new StdClass();
			$orderHandle->Id = $number;
			$orderHandles->OrderHandle[] = $orderHandle;
		}
		
		try{
			if(sizeof($orderHandles->OrderHandle) > 1){
				$response = $this->client->Order_GetDataArray(array('entityHandles' => $orderHandles));
				$orders = array();
				foreach($response->Order_GetDataArrayResult->OrderData as $order){
					$orders[] = $order;
				}
				return $orders;
			}
			else{
				$response = $this->client->Order_GetData(array('entityHandle' => $orderHandles->OrderHandle[0]));
				return array($response->Order_GetDataResult);
			}
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Order_GetData($id){
		if(!$this->connected){
			return false;
		}
		
		$entityHandle = new StdClass();
		$entityHandle->Id = $id;
		
		try{
			$response = $this->client->Order_GetData(array('entityHandle' => $entityHandle));
			return $response->Order_GetDataResult;
		}
		catch(Exception $e){
			Mage::log('Order_GetDataResult - ERROR ID:'.$id,null,'economic.log',true);
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	
	
	/*** INVOICE ***/
	
	public function Invoice_FindByNumber($number){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->Invoice_FindByNumber(array('number' => $number));
			return $response->Invoice_FindByNumberResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Invoice_GetDataArray($numbers){
		if(!$this->connected){
			return false;
		}
		
		$invoiceHandles = new StdClass();
		$invoiceHandles->InvoiceHandle = array();
		
		if(!is_array($numbers)){
			$numbers = explode(',', $numbers);
		}
		
		foreach($numbers as $number){
			$invoiceHandle = new StdClass();
			$invoiceHandle->Number = $number;
			$invoiceHandles->InvoiceHandle[] = $invoiceHandle;
		}
		
		try{
			if(sizeof($invoiceHandles->InvoiceHandle) > 1){
				$response = $this->client->Invoice_GetDataArray(array('entityHandles' => $invoiceHandles));
				$invoices = array();
				foreach($response->Invoice_GetDataArrayResult->InvoiceData as $invoice){
					$invoices[] = $invoice;
				}
				return $invoices;
			}
			else{
				$response = $this->client->Invoice_GetData(array('entityHandle' => $invoiceHandles->InvoiceHandle[0]));
				return array($response->Invoice_GetDataResult);
			}
			
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function CurrentInvoice_GetData($id){
		if(!$this->connected){
			return false;
		}
		
		$entityHandle = new StdClass();
		$entityHandle->Id = $id;
		
		try{
			$response = $this->client->CurrentInvoice_GetData(array('entityHandle' => $entityHandle));
			return $response->CurrentInvoice_GetDataResult;
		}
		catch(Exception $e){
			Mage::log('CurrentInvoice_GetData - ERROR ID:'.$id,null,'economic.log',true);
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function CurrentInvoice_GetLines($invoiceId){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->CurrentInvoice_GetLines(array('currentInvoiceHandle'=>array('Id'=>$invoiceId)));
			if(isset($response->CurrentInvoice_GetLinesResult->CurrentInvoiceLineHandle)){
				return $response->CurrentInvoice_GetLinesResult->CurrentInvoiceLineHandle;
			}
			else{
				return false;
			}
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function CurrentInvoice_CreateFromData($invoiceData){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->CurrentInvoice_CreateFromData(array('data' => $invoiceData));
			return $response->CurrentInvoice_CreateFromDataResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function CurrentInvoiceLine_CreateFromData($lineData){
		if(!$this->connected){
			return false;
		}

		try{	
			$response = $this->client->CurrentInvoiceLine_CreateFromData(array('data' => $lineData));
			return $response->CurrentInvoiceLine_CreateFromDataResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			Mage::log('invoiceLineDataArray: '.print_r($lineData,true),null,'economic.log',true);
			return false;
		}
	}
	
	public function CurrentInvoiceLine_CreateFromDataArray($invoiceLineDataArray){
		
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->CurrentInvoiceLine_CreateFromDataArray(array('dataArray' => $invoiceLineDataArray));
			return $response->CurrentInvoiceLine_CreateFromDataArrayResult;
		}
		catch(Exception $e){
			Mage::log('ERROR (CurrentInvoiceLine_CreateFromDataArray): '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function CurrentInvoiceLine_Create($invoiceId){
		if(!$this->connected){
			return false;
		}
		
		$invoiceHandle = new stdClass();
		$invoiceHandle->Id = $invoiceId;
		
		try{
			$response = $this->client->CurrentInvoiceLine_Create(array('invoiceHandle' => $invoiceHandle));
			return $response->CurrentInvoiceLine_CreateResult;
		}
		catch(Exception $e){
			Mage::log('ERROR (CurrentInvoiceLine_CreateResult): '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function CurrentInvoiceLine_SetProduct($invoiceLineHandle,$sku){
		if(!$this->connected){
			return false;
		}
		
		$valueHandle = new stdClass();
		$valueHandle->Number = $sku;
		
		try{
			$response = $this->client->CurrentInvoiceLine_SetProduct(array('currentInvoiceLineHandle' => $invoiceLineHandle, 'valueHandle' => $valueHandle));
			return $response->CurrentInvoiceLine_SetProductResult;
		}
		catch(Exception $e){
			Mage::log('ERROR (CurrentInvoiceLine_SetProduct): '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function CurrentInvoiceLine_SetInventoryLocation($invoiceId,$itemNumber,$location){
		if(!$this->connected){
			return false;
		}
		
		$currentInvoiceLineHandle = new stdClass();
		$currentInvoiceLineHandle->Id = $invoiceId;
		$currentInvoiceLineHandle->Number = $itemNumber;
		
		$valueHandle = new stdClass();
		$valueHandle->Number = $location;
		
		try{
			$response = $this->client->CurrentInvoiceLine_SetInventoryLocation(array('currentInvoiceLineHandle' => $currentInvoiceLineHandle, 'valueHandle' => $valueHandle));
			return $response->CurrentInvoiceLine_SetInventoryLocationResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function CurrentInvoiceLine_SetDepartment($invoiceId,$itemNumber,$department){
		if(!$this->connected){
			return false;
		}
		
		$currentInvoiceLineHandle = new stdClass();
		$currentInvoiceLineHandle->Id = $invoiceId;
		$currentInvoiceLineHandle->Number = $itemNumber;
		
		$valueHandle = new stdClass();
		$valueHandle->Number = $department;
		
		try{
			$response = $this->client->CurrentInvoiceLine_SetDepartment(array('currentInvoiceLineHandle' => $currentInvoiceLineHandle, 'valueHandle' => $valueHandle));
			return $response->CurrentInvoiceLine_SetDepartmentResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function CurrentInvoice_SetYourReference($invoiceId,$contactId){
		if(!$this->connected){
			return false;
		}
		
		try{
			return $this->client->CurrentInvoice_SetYourReference(array('currentInvoiceHandle'=>array('Id'=>$invoiceId),'valueHandle'=>array('Id'=>$contactId)));
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function CurrentInvoice_SetOurReference($invoiceId,$employeeId){
		if(!$this->connected){
			return false;
		}
		
		try{
			return $this->client->CurrentInvoice_SetOurReference(array('currentInvoiceHandle'=>array('Id'=>$invoiceId),'valueHandle'=>array('Number'=>$employeeId)));
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}


    public function CurrentInvoice_SetOurReference2($invoiceId,$reference){
        if(!$this->connected){
            return false;
        }

        try {
            Mage::log("Tester", null, "tester123.log", true);
            var_dump($this->client->CurrentInvoice_SetOurReference2(array('currentInvoiceHandle'=>array('Id'=>$invoiceId),'valueHandle'=>array('Number'=>$reference))));
            Zend_Debug::dump($response);
            die('111');
        }
        catch(Exception $e){
            Mage::log(__METHOD__ . ' ERROR: '.$e->getMessage(),null,'economic.log',true);
            return false;
        }
    }

    public function CurrentInvoice_SetTermsOfDelivery($invoiceId,$terms){
        if(!$this->connected){
            return false;
        }

        try{
            return $this->client->CurrentInvoice_SetTermsOfDelivery(array('currentInvoiceHandle'=>array('Id'=>$invoiceId),'value'=> $terms));
        }
        catch(Exception $e){
            Mage::log(__METHOD__ . ' ERROR: '.$e->getMessage(),null,'economic.log',true);
            return false;
        }
    }


    public function CurrentInvoice_SetTextLine2($invoiceId,$text){
        if(!$this->connected){
            return false;
        }

        try{
            return $this->client->CurrentInvoice_SetTextLine2(array('currentInvoiceHandle'=>array('Id'=>$invoiceId),'value'=> $text));
        }
        catch(Exception $e){
            Mage::log(__METHOD__ . ' ERROR: '.$e->getMessage(),null,'economic.log',true);
            return false;
        }
    }


	
	public function CurrentInvoice_Book($invoiceId){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->CurrentInvoice_Book(array('currentInvoiceHandle'=>array('Id'=>$invoiceId)));
			return $response->CurrentInvoice_BookResult->Number;
		}
		catch(Exception $e){
			return false;
		}
	}
	
	public function CurrentInvoice_BookWithNumber($invoiceId,$number){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->CurrentInvoice_BookWithNumber(array('number' => $number, 'currentInvoiceHandle' => array('Id'=>$invoiceId)));
			return $response->CurrentInvoice_BookWithNumberResult->Number;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Invoice_GetPdf($invoiceNumber){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->Invoice_GetPdf(array('invoiceHandle' => array('Number'=>$invoiceNumber)));
			return $response->Invoice_GetPdfResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Invoice_GetData($number){
		if(!$this->connected){
			return false;
		}
		
		$entityHandle = new StdClass();
		$entityHandle->Number = $number;
		
		try{
			$response = $this->client->Invoice_GetData(array('entityHandle' => $entityHandle));
			return $response->Invoice_GetDataResult;
		}
		catch(Exception $e){
			Mage::log('Invoice_GetData - ERROR ID:'.$number,null,'economic.log',true);
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function CurrentInvoice_Delete($invoiceId){
		if(!$this->connected){
			return false;
		}
		
		try{
			return $this->client->CurrentInvoice_Delete(array('currentInvoiceHandle'=>array('Id'=>$invoiceId)));
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	
	/*** DEBTOR ***/
	
	public function Debtor_FindByNumber($number){
		if(!$this->connected){
			return false;
		}
		
		try{
			if($response = $this->client->Debtor_FindByNumber(array('number' => $number))){
				if (isset($response->Debtor_FindByNumberResult)) {
					if (is_array($response->Debtor_FindByNumberResult)) {
						return $response->Debtor_FindByNumberResult[0];
					}
					else {
						return $response->Debtor_FindByNumberResult;
					}
				}
			}
			else{
				return false;
			}
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Debtor_FindByEmail($email){
		if(!$this->connected){
			return false;
		}
		
		try{
			if($response = $this->client->Debtor_FindByEmail(array('email' => $email))){
				if (isset($response->Debtor_FindByEmailResult->DebtorHandle)) {
					if (is_array($response->Debtor_FindByEmailResult->DebtorHandle)) {
						return $response->Debtor_FindByEmailResult->DebtorHandle[0];
					}
					else {
						return $response->Debtor_FindByEmailResult->DebtorHandle;
					}
				}
			}
			else{
				return false;
			}
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Debtor_FindByCINumber($ciNumber){
		if(!$this->connected){
			return false;
		}
		
		try{
			if($response = $this->client->Debtor_FindByCINumber(array('ciNumber' => $ciNumber))){
				if (isset($response->Debtor_FindByCINumberResult->DebtorHandle)) {
					if (is_array($response->Debtor_FindByCINumberResult->DebtorHandle)) {
						return $response->Debtor_FindByCINumberResult->DebtorHandle[0];
					}
					else {
						return $response->Debtor_FindByCINumberResult->DebtorHandle;
					}
				}
			}
			else{
				return false;
			}
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Debtor_GetData($number){
		if(!$this->connected){
			return false;
		}
		
		$debtorHandle = new StdClass();
		$debtorHandle->Number = $number;
		
		try{
			$response = $this->client->Debtor_GetData(array('entityHandle' => $debtorHandle));
			return $response->Debtor_GetDataResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Debtor_CreateFromData($debtorData){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->Debtor_CreateFromData(array('data' => $debtorData));
			return $response->Debtor_CreateFromDataResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Debtor_UpdateFromData($debtorData){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->Debtor_UpdateFromData(array('data' => $debtorData));
			return $response->Debtor_UpdateFromDataResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Debtor_GetNextAvailableNumber(){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->Debtor_GetNextAvailableNumber();
			return $response->Debtor_GetNextAvailableNumberResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Debtor_GetDebtorContacts($debtorId){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->Debtor_GetDebtorContacts(array('debtorHandle'=>array('Number'=>$debtorId)));
			return $response->Debtor_GetDebtorContactsResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function DebtorContact_GetName($debtorContactId){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->DebtorContact_GetName(array('debtorContactHandle'=>array('Id'=>$debtorContactId)));
			return $response->DebtorContact_GetNameResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function DebtorContact_Create($debtorNumber,$contactName){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->DebtorContact_Create(array('debtorHandle'=>array('Number'=>$debtorNumber),'name'=>$contactName));
			return $response->DebtorContact_CreateResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Debtor_GetTermOfPayment($debtorId){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->Debtor_GetTermOfPayment(array('debtorHandle'=>array('Number'=>$debtorId)));
			return $response->Debtor_GetTermOfPaymentResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Debtor_GetLayout($debtorId){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->Debtor_GetLayout(array('debtorHandle'=>array('Number'=>$debtorId)));
			return $response->Debtor_GetLayoutResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Debtor_GetIsAccessible($debtorId){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->Debtor_GetIsAccessible(array('debtorHandle'=>array('Number'=>$debtorId)));
			return $response->Debtor_GetIsAccessibleResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Debtor_GetCurrentInvoices($debtorNumber){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->Debtor_GetCurrentInvoices(array('debtorHandle'=>array('Number'=>$debtorNumber)));
			
			if(isset($response->Debtor_GetCurrentInvoicesResult->CurrentInvoiceHandle)){
				if (is_array($response->Debtor_GetCurrentInvoicesResult->CurrentInvoiceHandle)) {
					return $response->Debtor_GetCurrentInvoicesResult->CurrentInvoiceHandle[0]->Id;
				}
				else {
					return $response->Debtor_GetCurrentInvoicesResult->CurrentInvoiceHandle->Id;
				}
			}
			else{
				return false;
			}
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Debtor_GetOurReference($debtorId){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->Debtor_GetOurReference(array('debtorHandle'=>array('Number'=>$debtorId)));
			return $response->Debtor_GetOurReferenceResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	
	/*** PRODUCT ***/
	
	public function Product_FindByNumber($number){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->Product_FindByNumber(array('number' => $number));
			return (isset($response->Product_FindByNumberResult)) ? $response->Product_FindByNumberResult : false;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Product_GetData($number){
		if(!$this->connected){
			return false;
		}
		
		$productHandle = new StdClass();
		$productHandle->Number = $number;
		
		try{
			$response = $this->client->Product_GetData(array('entityHandle' => $productHandle));
			return $response->Product_GetDataResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Product_GetInventoryLocationStatus($number){
		if(!$this->connected){
			return false;
		}
		
		$productHandle = new StdClass();
		$productHandle->Number = $number;
		
		try{
			$response = $this->client->Product_GetInventoryLocationStatus(array('productHandle' => $productHandle));
			return $response->Product_GetInventoryLocationStatusResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Product_CreateFromData($productData){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->Product_CreateFromData(array('data' => $productData));
			return $response->Product_CreateFromDataResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function Product_UpdateFromData($productData){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->Product_UpdateFromData(array('data' => $productData));
			return $response->Product_UpdateFromDataResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	

	
	public function CashBookEntry_CreateFromData($entryData){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->CashBookEntry_CreateFromData(array('data' => $entryData));
			return $response->CashBookEntry_CreateFromDataResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function CashBookEntry_CreateFinanceVoucher($cashBookNumber,$accountNumber,$contraAccountNumber){
		if(!$this->connected){
			return false;
		}
		
		$cashBookHandle = new stdClass();
		$cashBookHandle->Number = $cashBookNumber;
		
		try{
			$response = $this->client->CashBookEntry_CreateFinanceVoucher(array('cashBookHandle'=>$cashBookHandle,'accountHandle'=>array('Number'=>$accountNumber),'contraAccountHandle'=>array('Number'=>$contraAccountNumber)));
			return $response->CashBookEntry_CreateFinanceVoucherResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function CashBookEntry_SetAmount($cashBookEntryHandle,$amount){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->CashBookEntry_SetAmount(array('cashBookEntryHandle' => $cashBookEntryHandle, 'value' => $amount));
			return $response->CashBookEntry_SetAmountResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function CashBookEntry_SetCurrency($cashBookEntryHandle,$currencyCode){
		if(!$this->connected){
			return false;
		}
		
		$valueHandle = new stdClass();
		$valueHandle->Number = $currencyCode;
		
		try{
			$response = $this->client->CashBookEntry_SetCurrency(array('cashBookEntryHandle' => $cashBookEntryHandle, 'valueHandle' => $valueHandle));
			return $response->CashBookEntry_SetCurrencyResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	public function CashBookEntry_SetText($cashBookEntryHandle,$value){
		if(!$this->connected){
			return false;
		}
		
		try{
			$response = $this->client->CashBookEntry_SetText(array('cashBookEntryHandle' => $cashBookEntryHandle, 'value' => $value));
			return $response->CashBookEntry_SetTextResult;
		}
		catch(Exception $e){
			Mage::log('ERROR: '.$e->getMessage(),null,'economic.log',true);
			return false;
		}
	}
	
	
}
