<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Model_Abstract extends Mage_Core_Model_Abstract
{
	protected $api;
	protected $_isCreditmemo;
	protected $_doIntegrityCheck=true;
	protected $_integrityCheckFailed=false;
	protected $_debtorReference;

	public function _construct() {
		$this->api = Mage::getSingleton('economic/api');
		if(!$this->api->connected){
			$this->api->connect();
		}
	}

	protected function getConfig($path,$store=0)
	{
		return Mage::helper('economic')->getConfig($path,$store);
	}

	public function log($str){
		if($this->getConfig('general/debug_log')){
			Mage::log($str,null,'economic.log',true);
		}
	}

	protected function convertPrice($price) {
		return floatval(str_replace(",", ".", $price));
	}

	protected function convertToXsdDate($datetime) {
		$unix_timestamp = strtotime($datetime);
		return strftime("%FT%T", $unix_timestamp);
	}
	
	public function getDateNow(){
		$datetime = Zend_Date::now();
		$datetime->setLocale(Mage::getStoreConfig(Mage_Core_Model_Locale::XML_PATH_DEFAULT_LOCALE))
		         ->setTimezone(Mage::getStoreConfig(Mage_Core_Model_Locale::XML_PATH_DEFAULT_TIMEZONE));
		$dateNow = $datetime->get('YYYY-MM-dd HH:mm:ss');
		return $dateNow;
	}

	protected function __getTermsOfPaymentId($paymentMethodCode=null,$orderIncrementId=null,$debtor=null) {
		
		if($this->getConfig('order_settings/terms_of_payment_from_debtor') && $debtor){
			if($termOfPaymentHandle = $this->api->Debtor_GetTermOfPayment($debtor->Number)){
				return $termOfPaymentHandle->Id;
			}
		}
		
		$termOfPaymentId = $this->getConfig('order_settings/terms_of_payment');
		
		$this->log('__getTermsOfPaymentId - payment:'.$paymentMethodCode.' - order:'.$orderIncrementId);

		if(!$paymentMethodCode){
			return $termOfPaymentId;
		}

		if($termOfPaymentDependingOnPaymentMethod = $this->getConfig('order_settings/terms_of_payment_depending_on_payment_method')){
			$termOfPaymentDependingOnPaymentMethod = unserialize($termOfPaymentDependingOnPaymentMethod);
			foreach($termOfPaymentDependingOnPaymentMethod as $p){

				$paymentTypeId = $p['payment_type_id'];

				if($paymentTypeId == $paymentMethodCode){
					$termOfPaymentId = $p['term_of_payment_id'];
				}

				if(strpos($paymentTypeId, '#')){
					$paymentTypeId = substr($paymentTypeId, 0, strpos($paymentTypeId, '#'));
				}
				
				$this->log('$termOfPaymentId:'.$termOfPaymentId.' - $paymentTypeId:'.$paymentTypeId);

				if($paymentTypeId == $paymentMethodCode){

					if($orderIncrementId && $cardType = substr(strrchr($p['payment_type_id'], "#"), 1)){
						$this->log('$cardType:'.$cardType);
						$cardType = explode(',', $cardType);
						foreach($cardType as $key => $value){
							$cardType[$key] = trim($value);
						}

						if($paymentTypeId == 'epay_standard'){
							$epayCardType = $this->__getEpayCardType($orderIncrementId);
							
							$this->log('$epayCardType:'.$epayCardType);

							if(in_array($epayCardType,$cardType)){
								$termOfPaymentId = $p['term_of_payment_id'];
								break;
							}
						}
						elseif($paymentTypeId == 'quickpaypayment_payment'){
							$quickpayCardType = strtolower($this->__getQuickpayCardType($orderIncrementId));

							if(strpos($quickpayCardType, 'dankort') !== false && in_array('Dankort',$cardType)){
								$termOfPaymentId = $p['term_of_payment_id'];
								break;
							}
							elseif(strpos($quickpayCardType, 'visa') !== false && in_array('VISA',$cardType)){
								$termOfPaymentId = $p['term_of_payment_id'];
								break;
							}
							elseif(strpos($quickpayCardType, 'mastercard') !== false && in_array('MasterCard',$cardType)){
								$termOfPaymentId = $p['term_of_payment_id'];
								break;
							}
							elseif(strpos($quickpayCardType, 'jcb') !== false && in_array('JCB',$cardType)){
								$termOfPaymentId = $p['term_of_payment_id'];
								break;
							}
							elseif(strpos($quickpayCardType, 'maestro') !== false && in_array('Maestro',$cardType)){
								$termOfPaymentId = $p['term_of_payment_id'];
								break;
							}
							elseif(strpos($quickpayCardType, 'american-express') !== false && in_array('American Express',$cardType)){
								$termOfPaymentId = $p['term_of_payment_id'];
								break;
							}
							elseif(strpos($quickpayCardType, 'diners') !== false && in_array('Diners',$cardType)){
								$termOfPaymentId = $p['term_of_payment_id'];
								break;
							}
							elseif(strpos($quickpayCardType, 'paypal') !== false && in_array('PayPal',$cardType)){
								$termOfPaymentId = $p['term_of_payment_id'];
								break;
							}
							elseif(strpos($quickpayCardType, 'viabill') !== false && in_array('ViaBill',$cardType)){
								$termOfPaymentId = $p['term_of_payment_id'];
								break;
							}
						}
					}

				}

			}

		}

		return $termOfPaymentId;
	}

	private function __getCountryCodeByName($countryName)
	{
		$countryList = Mage::getResourceModel('directory/country_collection')
		->loadData()
		->toOptionArray(false);

		foreach ($countryList as $key => $val)
		{
			if (strtolower($val['label']) === strtolower($countryName)) {
				return $val['value'];
			}
		}

		return '';
	}

	private function __getDefaultWebsiteId()
	{
		$websites = Mage::app()->getWebsites();
		foreach($websites as $website){
			return $website->getId();
		}
	}

	private function __getStandardDebtor($order,$country,$vatNumber){
		$ownCountry = $this->getConfig('general/country_default');
		$euCountries = explode(',', $this->getConfig('general/eu_countries'));
		
		$orderIncrementId = $order->getIncrementId();
		$paymentMethod = $order->getPayment()->getMethod();
		
		$this->log('__getStandardDebtor - country:'.$country.' - ownCountry:'.$ownCountry);
				
		if($country == $ownCountry){
			$standardDebtorId = $this->getConfig('debtor_settings/debtor_standard_own_country');
		}
		elseif($vatNumber && in_array($country,$euCountries)){
			$standardDebtorId = $this->getConfig('debtor_settings/debtor_standard_eu_with_vatnumber');
		}
		elseif(in_array($country,$euCountries)){
			$standardDebtorId = $this->getConfig('debtor_settings/debtor_standard_eu');
		}
		else{
			$standardDebtorId = $this->getConfig('debtor_settings/debtor_standard_other');
		}
		
		if($debtorPaymentMapping = $this->getConfig('debtor_settings/debtor_standard_depending_on_payment_method')) {
    		$debtorPaymentMapping = unserialize($debtorPaymentMapping);
			foreach($debtorPaymentMapping as $dpm){

				$paymentTypeId = $dpm['payment_type_id'];

				if($paymentTypeId == $paymentMethod){
					$standardDebtorId = $dpm['debtor_number'];
				}

				if(strpos($paymentTypeId, '#')){
					$paymentTypeId = substr($paymentTypeId, 0, strpos($paymentTypeId, '#'));
				}
				
				if($paymentTypeId == $paymentMethod){

					if($orderIncrementId && $cardType = substr(strrchr($dpm['payment_type_id'], "#"), 1)){
						$cardType = explode(',', $cardType);
						foreach($cardType as $key => $value){
							$cardType[$key] = trim($value);
						}

						if($paymentTypeId == 'epay_standard'){
							$epayCardType = $this->__getEpayCardType($orderIncrementId);
							
							if(in_array($epayCardType,$cardType)){
								$standardDebtorId = $dpm['debtor_number'];
								break;
							}
						}
						elseif($paymentTypeId == 'quickpaypayment_payment'){
							$quickpayCardType = strtolower($this->__getQuickpayCardType($orderIncrementId));

							if(strpos($quickpayCardType, 'dankort') !== false && in_array('Dankort',$cardType)){
								$standardDebtorId = $dpm['debtor_number'];
								break;
							}
							elseif(strpos($quickpayCardType, 'visa') !== false && in_array('VISA',$cardType)){
								$standardDebtorId = $dpm['debtor_number'];
								break;
							}
							elseif(strpos($quickpayCardType, 'mastercard') !== false && in_array('MasterCard',$cardType)){
								$standardDebtorId = $dpm['debtor_number'];
								break;
							}
							elseif(strpos($quickpayCardType, 'jcb') !== false && in_array('JCB',$cardType)){
								$standardDebtorId = $dpm['debtor_number'];
								break;
							}
							elseif(strpos($quickpayCardType, 'maestro') !== false && in_array('Maestro',$cardType)){
								$standardDebtorId = $dpm['debtor_number'];
								break;
							}
							elseif(strpos($quickpayCardType, 'american-express') !== false && in_array('American Express',$cardType)){
								$standardDebtorId = $dpm['debtor_number'];
								break;
							}
							elseif(strpos($quickpayCardType, 'diners') !== false && in_array('Diners',$cardType)){
								$standardDebtorId = $dpm['debtor_number'];
								break;
							}
							elseif(strpos($quickpayCardType, 'paypal') !== false && in_array('PayPal',$cardType)){
								$standardDebtorId = $dpm['debtor_number'];
								break;
							}
							elseif(strpos($quickpayCardType, 'viabill') !== false && in_array('ViaBill',$cardType)){
								$standardDebtorId = $dpm['debtor_number'];
								break;
							}
						}
					}
				}
			}
		}
		
		$this->log('standard debtor: '.$standardDebtorId);

		if(isset($standardDebtorId)){
			if(is_numeric($standardDebtorId)){
				return $this->api->Debtor_FindByNumber($standardDebtorId);
			}
			else{
				return $this->api->Debtor_FindByEmail($standardDebtorId);
			}
		}
		else{
			return false;
		}
	}

	private function __getVatIncluded($country,$vatNumber,$storeId) {

		$ownCountry = $this->getConfig('general/country_default');
		$euCountries = explode(',', $this->getConfig('general/eu_countries'));

		if ($country == $ownCountry || (in_array($country,$euCountries) && !$vatNumber)) {
			return 1;
		}
		else {
			return 0;
		}

	}
	
	private function __getVatZone($country,$vatNumber,$storeId) {

		$ownCountry = $this->getConfig('general/country_default');
		$euCountries = explode(',', $this->getConfig('general/eu_countries'));

		if ($country == $ownCountry || (in_array($country,$euCountries) && !$vatNumber)) {
			return 1; // HomeCountry
		}
		elseif($vatNumber && in_array($country,$euCountries)){
			return 2; // EU
		}
		else{
			return 3; // Abroad
		}
	}

	private function __getLayoutId($country,$paymentMethod,$storeId,$debtor=null,$orderIncrementId=null) {
		
		if($this->getConfig('order_settings/layout_from_debtor') && $debtor){
			if($layoutHandle = $this->api->Debtor_GetLayout($debtor->Number)){
				return $layoutHandle->Id;
			}
		}
		
		$ownCountry = $this->getConfig('general/country_default');
		$euCountries = explode(',', $this->getConfig('general/eu_countries'));

		if ($country == $ownCountry) {
			$layoutId = $this->getConfig('order_settings/layout_own_country');
		}
		elseif (in_array($country,$euCountries)) {
			$layoutId = $this->getConfig('order_settings/layout_eu');
		}
		else {
			$layoutId = $this->getConfig('order_settings/layout_other');
		}


		// LAYOUT BASED ON COUNTRY AND PAYMENT METHOD
		if ($this->getConfig('avanceret/layout_land_betaling') != "") {
			$countryPaymentLayoutArray = array();
			$layout_land_betaling = explode("\n",$this->getConfig('avanceret/layout_land_betaling'));
			foreach ($layout_land_betaling as $line) {
				$line = explode(",",trim($line));
				$countryPaymentLayoutArray[$line[0]][$line[1]] = $line[2];
			}
			$match = false;
			if (isset($countryPaymentLayoutArray[$country])) {
				if (isset($countryPaymentLayoutArray[$country][$paymentMethod])) {
					$layoutId = $countryPaymentLayoutArray[$country][$paymentMethod];
					$match = true;
				}
				elseif (isset($countryPaymentLayoutArray[$country]["*"])) {
					$layoutId = $countryPaymentLayoutArray[$country]["*"];
					$match = true;
				}
			}
			if (isset($countryPaymentLayoutArray["*"]) && !$match) {
				if (isset($countryPaymentLayoutArray["*"][$paymentMethod])) {
					$layoutId = $countryPaymentLayoutArray["*"][$paymentMethod];
				}
				elseif (isset($countryPaymentLayoutArray["*"]["*"])) {
					$layoutId = $countryPaymentLayoutArray["*"]["*"];
				}
			}
		}

		if($layoutDependingOnPaymentMethod = $this->getConfig('order_settings/layout_depending_on_payment_method')){
			$layoutDependingOnPaymentMethod = unserialize($layoutDependingOnPaymentMethod);
			foreach($layoutDependingOnPaymentMethod as $l){

				$paymentTypeId = $l['payment_type_id'];

				if($paymentTypeId == $paymentMethod){
					$layoutId = $l['layout_id'];
				}

				if(strpos($paymentTypeId, '#')){
					$paymentTypeId = substr($paymentTypeId, 0, strpos($paymentTypeId, '#'));
				}
				
				if($paymentTypeId == $paymentMethod){

					if($orderIncrementId && $cardType = substr(strrchr($l['payment_type_id'], "#"), 1)){
						$cardType = explode(',', $cardType);
						foreach($cardType as $key => $value){
							$cardType[$key] = trim($value);
						}

						if($paymentTypeId == 'epay_standard'){
							$epayCardType = $this->__getEpayCardType($orderIncrementId);
							
							if(in_array($epayCardType,$cardType)){
								$layoutId = $l['layout_id'];
								break;
							}
						}
						elseif($paymentTypeId == 'quickpaypayment_payment'){
							$quickpayCardType = strtolower($this->__getQuickpayCardType($orderIncrementId));

							if(strpos($quickpayCardType, 'dankort') !== false && in_array('Dankort',$cardType)){
								$layoutId = $l['layout_id'];
								break;
							}
							elseif(strpos($quickpayCardType, 'visa') !== false && in_array('VISA',$cardType)){
								$layoutId = $l['layout_id'];
								break;
							}
							elseif(strpos($quickpayCardType, 'mastercard') !== false && in_array('MasterCard',$cardType)){
								$layoutId = $l['layout_id'];
								break;
							}
							elseif(strpos($quickpayCardType, 'jcb') !== false && in_array('JCB',$cardType)){
								$layoutId = $l['layout_id'];
								break;
							}
							elseif(strpos($quickpayCardType, 'maestro') !== false && in_array('Maestro',$cardType)){
								$layoutId = $l['layout_id'];
								break;
							}
							elseif(strpos($quickpayCardType, 'american-express') !== false && in_array('American Express',$cardType)){
								$layoutId = $l['layout_id'];
								break;
							}
							elseif(strpos($quickpayCardType, 'diners') !== false && in_array('Diners',$cardType)){
								$layoutId = $l['layout_id'];
								break;
							}
							elseif(strpos($quickpayCardType, 'paypal') !== false && in_array('PayPal',$cardType)){
								$layoutId = $l['layout_id'];
								break;
							}
							elseif(strpos($quickpayCardType, 'viabill') !== false && in_array('ViaBill',$cardType)){
								$layoutId = $l['layout_id'];
								break;
							}
						}
					}
				}
			}
		}
		
		return $layoutId;
	}

	private function __getQuickpayCardType($orderIncrementId){
		$paymentInfo = '';
		$resource = Mage::getSingleton('core/resource');
		$read = $resource->getConnection('core_read');
		$table = $resource->getTableName('quickpaypayment_order_status');

		$row = $this->paymentData = $read->fetchRow("select * from " . $table . " where ordernum = " . $orderIncrementId);

		if (is_array($row)) {
			if ($row['qpstat'] == '000' || $row['qpstat'] == '20000') {
				if ($row['cardtype'] != '0') {
					$paymentInfo .= ucwords($row['cardtype']);
				}
			}
		}

		return $paymentInfo;
	}

	private function __getEpayCardType($orderIncrementId){
		$paymentInfo = '';
		$resource = Mage::getSingleton('core/resource');
		$read = $resource->getConnection('core_read');
		$row = $read->fetchRow("select * from epay_order_status where orderid = '" . $orderIncrementId . "'");

		if ($row['status'] == '1') {
			if ($row['cardid'] != '0') {
				$cardId = $row['cardid'];
				if($cardId==1){ $paymentInfo .= 'Dankort'; }
				elseif($cardId==2){ $paymentInfo .= 'eDankort'; }
				elseif($cardId==3){ $paymentInfo .= 'VISA'; }
				elseif($cardId==4){ $paymentInfo .= 'MasterCard'; }
				elseif($cardId==5){ $paymentInfo .= 'MasterCard'; }
				elseif($cardId==6){ $paymentInfo .= 'JCB'; }
				elseif($cardId==7){ $paymentInfo .= 'Maestro'; }
				elseif($cardId==8){ $paymentInfo .= 'Diners'; }
				elseif($cardId==9){ $paymentInfo .= 'American Express'; }
				elseif($cardId==10){ $paymentInfo .= 'eWire'; }
				elseif($cardId==11){ $paymentInfo .= 'Forbrugsforeningen'; }
				elseif($cardId==12){ $paymentInfo .= 'Nordea e-betaling'; }
				elseif($cardId==13){ $paymentInfo .= 'Danske Netbetaling'; }
				elseif($cardId==14){ $paymentInfo .= 'PayPal'; }
				elseif($cardId==16){ $paymentInfo .= 'Mobilpenge'; }
				elseif($cardId==17){ $paymentInfo .= 'Klarna'; }
				elseif($cardId==23){ $paymentInfo .= 'ViaBill'; }
				elseif($cardId==27){ $paymentInfo .= 'Paii'; }
				elseif($cardId==29){ $paymentInfo .= 'MobilePay Online'; }
				else{  $paymentInfo .= Mage::helper('economic')->__('ePay (ukendt korttype)');  }
			}
		}

		return $paymentInfo;
	}

	private function __getOrderEan($order) {
		$ean = false;
		if ($eanNumber = $order->getPayment()->getData('ean_nummer')) {
			$ean = (Mage::helper('economic')->checkEan($eanNumber)) ? $eanNumber : false;
		}
		return $ean;
	}


	private function __addCustomOptionsToOrderLine($item){

	}
	
	public function createOrderInEconomic($order,$isCreditmemo=false)
	{
		$this->log('createOrderInEconomic - id: '.$order->getIncrementId());
		$orderData = $this->createEconomicOrderInvoiceObject($order,$isCreditmemo);
		
		$invoice = false;
		if($order->getOrder()){
			$invoice = $order;
			$order = $order->getOrder();
		}
		
		if($invoice){
			$economicId = false;
			
			// Check if customer already has an open invoice and then use this 
			if($this->getConfig('order_settings/use_existing_invoice')){
				if($specificDebtorNumbers = $this->getConfig('order_settings/use_existing_invoice_debtor_specific')){
					$specificDebtorNumbers = explode(',', $specificDebtorNumbers);
					foreach($specificDebtorNumbers as $k => $v){
						$specificDebtorNumbers[$k] = trim($v);
					}
					if(!empty($specificDebtorNumbers)){
						if(in_array($debtor->Number, $specificDebtorNumbers)){
							if($economicId = $this->api->Debtor_GetCurrentInvoices($debtor->Number)){
								$this->_doIntegrityCheck = false;
							}
						}
					}
				}
				else{
					if($economicId = $this->api->Debtor_GetCurrentInvoices($debtor->Number)){
						$this->_doIntegrityCheck = false;
					}
				}
			}
			
			if(!$economicId){
				if($result = $this->api->CurrentInvoice_CreateFromData($orderData)){
					$economicId = $result->Id;
				}
			}
			
			if(!$economicId){
				$this->addError(Mage::helper('economic')->__('A problem has occurred when transferring the invoice/creditmemo to e-conomic'));
				$this->_sendErrorEmail($invoice);
				return false;
			}
			
			$this->log('invoice create ok');

			if($this->_debtorReference){
				$this->api->CurrentInvoice_SetYourReference($economicId,$this->_debtorReference);
			}

			if($ourReference = $this->getConfig('order_settings/our_reference')){
				$this->api->CurrentInvoice_SetOurReference($economicId,$ourReference);
			}

			if(!$this->setOrderLines($invoice,$economicId)){
				$this->addError(Mage::helper('economic')->__('A problem has occurred when adding lines to the e-conomic invoice/creditmemo'));
				$this->_sendErrorEmail($invoice);
				return false;
			}
			
			if($this->getConfig('order_settings/auto_book')){
				$preventBook = false;
				if($this->getConfig('order_settings/book_prevent_if_integrity_check_failed') && $this->_integrityCheckFailed){
					$preventBook = true;
				}
				if(!$preventBook){
					$this->__bookInvoiceAutomatic($invoice,$economicId);
				}
			}
		}
		else{
			$economicId = false;
			if($result = $this->api->Order_CreateFromData($orderData)){
				$economicId = $result->Id;
			}
			if(!$economicId){
				$this->addError(Mage::helper('economic')->__('A problem has occurred when transferring the order to e-conomic'));
				$this->_sendErrorEmail($order);
				return false;
			}

			if($this->_debtorReference){
				$this->api->Order_SetYourReference($economicId,$this->_debtorReference);
			}
			
			if($ourReference = $this->getConfig('order_settings/our_reference')){
				$this->api->Order_SetOurReference($economicId,$ourReference);
			}

			if(!$this->setOrderLines($order,$economicId)){
				$this->addError(Mage::helper('economic')->__('A problem has occurred when adding lines to the e-conomic order'));
				$this->_sendErrorEmail($order);
				return false;
			}
		}
		
		return $economicId;
	}


	public function createEconomicOrderInvoiceObject($order,$isCreditmemo=false)
	{
		$this->_isCreditmemo = $isCreditmemo;
		$this->_integrityCheckFailed = false;

		// ORDER DATA
		$invoice = false;
		if($order->getOrder()){
			$invoice = $order;
			$order = $order->getOrder();
		}
		$orderId = $order->getId();
		$orderIncrementId = $order->getIncrementId();
		$storeId = $order->getStoreId();
		$billingAddress = $order->getBillingAddress();
		$shippingAddress = $order->getShippingAddress();
		$paymentMethod = $order->getPayment()->getMethod();
		$country = strtoupper($billingAddress->getData('country_id'));
		$firstName = $billingAddress->getData('firstname');
		$lastName = $billingAddress->getData('lastname');
		if(!$vatNumber = $order->getData('customer_taxvat')){
			$vatNumber = $billingAddress->getData('vat_id');
		}

		// NEW ORDER DATA OBJECT
		$orderData = new StdClass();
		$orderData->Number = ($invoice) ? $invoice->getIncrementId() : $orderIncrementId;


		// DEBTOR
		if($this->getConfig('debtor_settings/use_existing_debtors')){
			$debtor = $this->__getStandardDebtor($order,$country,$vatNumber);
		}
		else{
			$customer = false;
			if($customerId = $order->getCustomerId()){
				$customer = Mage::getModel('customer/customer')->load($customerId);
			}
			if(!$debtor = Mage::getSingleton('economic/customer')->syncCustomerToEconomic($customer,$order)){
				//problems with debtor
				$this->log('debtor problems');
				return false;
			}
		}

		if(!isset($debtor) || !$debtor){
			//problems with debtor
			$this->log('debtor problems');
			return false;
		}
		$this->log('debtor ok');

		$debtorHandle = new stdClass();
		$debtorHandle->Number = $debtor->Number;
		$orderData->DebtorHandle = $debtorHandle;


		// NAME AND ADDRESS
		$debtorAtt = '';
		if ($company = $billingAddress->getData('company')) {
			$orderData->DebtorName = $company;
			$debtorAtt = $firstName." ".$lastName;
		}
		else {
			$orderData->DebtorName = $firstName." ".$lastName;
		}
		
		if ($ean = $this->__getOrderEan($order)) {
			$orderData->DebtorEan = $ean;
			$debtorAtt = $firstName." ".$lastName;
			
			if($eanCompany = trim($order->getPayment()->getData('ean_company'))){
				$orderData->DebtorName = $eanCompany;
			}
			if($eanRekvisitionsnummer = trim($order->getPayment()->getData('rekvisitionsnummer'))){
				$orderData->OtherReference = $eanRekvisitionsnummer;
			}
			if($eanReference = trim($order->getPayment()->getData('reference'))){
				$debtorAtt = $eanReference;
			}
		}

		$this->_debtorReference = false;
		if ($debtorAtt && !$this->getConfig('debtor_settings/use_existing_debtors')) {
			$debtorContacts = $this->api->Debtor_GetDebtorContacts($debtor->Number);
			if(isset($debtorContacts->DebtorContactHandle) && is_array($debtorContacts->DebtorContactHandle)){
				$debtorContacts = $debtorContacts->DebtorContactHandle;
				foreach($debtorContacts as $debtorContact){
					$debtorName = $this->api->DebtorContact_GetName($debtorContact->Id);
					if(trim($debtorName) == trim($debtorAtt)){
						$this->_debtorReference = $debtorContact->Id;
					}
				}
			}

			if(!$this->_debtorReference){
				$debtorContact = $this->api->DebtorContact_Create($debtor->Number,$debtorAtt);
				$this->_debtorReference = $debtorContact->Id;
			}
		}

		$orderData->DebtorAddress = $billingAddress->getData('street');
		$orderData->DebtorPostalCode = $billingAddress->getData('postcode');
		$orderData->DebtorCity = $billingAddress->getData('city');
		$orderData->DebtorCountry = $billingAddress->getCountryModel()->getName();
		
		$addShippingAddress = true;
		if($shippingAddress && $this->getConfig('debtor_settings/shipping_address_if_different')){
			if($billingAddress->getData('street') == $shippingAddress->getData('street') && $billingAddress->getData('postcode') == $shippingAddress->getData('postcode') && $billingAddress->getData('city') == $shippingAddress->getData('city') && $billingAddress->getCountryModel()->getName() == $shippingAddress->getCountryModel()->getName()){
				$addShippingAddress = false;
			}
		}
		elseif(!$shippingAddress){
			$addShippingAddress = false;
		}

		if($addShippingAddress){
			if ($company = $shippingAddress->getData('company')) {
				$orderData->DeliveryAddress = $company."\n".'Att. '.$shippingAddress->getData('firstname').' '.$shippingAddress->getData('lastname')."\n".$shippingAddress->getData('street');
			}
			else {
				$orderData->DeliveryAddress = $shippingAddress->getData('firstname').' '.$shippingAddress->getData('lastname')."\n".$shippingAddress->getData('street');
			}
			$orderData->DeliveryPostalCode = $shippingAddress->getData('postcode');
			$orderData->DeliveryCity = $shippingAddress->getData('city');
			$orderData->DeliveryCountry = $shippingAddress->getCountryModel()->getName();
		}
		
		$this->log('debtor address ok');
		
		if(Mage::getStoreConfig('tax/calculation/based_on',$storeId) == 'shipping'){
			$countryToCheckIfVatIsIncluded = ($shippingAddress && $shippingAddress->getData('country_id')) ? strtoupper($shippingAddress->getData('country_id')) : $country;
		}
		else{
			$countryToCheckIfVatIsIncluded = $country;
		}
		$orderData->IsVatIncluded = $this->__getVatIncluded($countryToCheckIfVatIsIncluded,$vatNumber,$storeId);
		
		$vatzoneHandle = new stdClass();
		$vatzoneHandle->Number = $this->__getVatZone($countryToCheckIfVatIsIncluded,$vatNumber,$storeId);		
		$orderData->VatZone = $vatzoneHandle;

		$layoutHandle = new stdClass();
		$layoutHandle->Id = $this->__getLayoutId($country,$paymentMethod,$storeId,$debtor,$orderIncrementId);
		$orderData->LayoutHandle = $layoutHandle;

		$currencyHandle = new stdClass();
		$currencyCode = $order->getData('order_currency_code');
		$currencyHandle->Code = $currencyCode;
		$orderData->CurrencyHandle = $currencyHandle;
		
		
		// REFERENCE TEXT
		$referenceText = $this->getConfig('order_settings/reference_text');
		$referenceTextPlacement = $this->getConfig('order_settings/reference_text_placement');
		
		$refText = '';
		if($referenceText == "order_invoice") {
			if($invoice){
				$refText = $order->getIncrementId() .' / '.$invoice->getIncrementId();
			}
			else{
				$refText = $order->getIncrementId();
			}
		}
		elseif($referenceText == "invoice_order") {
			if($invoice){
				$refText = $invoice->getIncrementId().' / '.$order->getIncrementId();
			}
			else{
				$refText = $order->getIncrementId();
			}
		}
		elseif($referenceText == "order") {
			$refText = $order->getIncrementId();
		}
		elseif($referenceText == "invoice" && $invoice) {
			$refText = $invoice->getIncrementId();
		}
		
		$this->log('$refText: '.$refText.' - placement: '.$referenceTextPlacement);
		
		if($refText && $this->_isCreditmemo){
			if($referenceCreditmemoPrefix = $this->getConfig('order_settings/reference_creditmemo_prefix')){
				$refText = $referenceCreditmemoPrefix.$refText;
			}
		}

		if($referenceTextPlacement == "other_ref" && !isset($orderData->OtherReference)) {
			$orderData->OtherReference = $refText;
		}
		elseif($referenceTextPlacement == "heading") {
			$orderData->Heading = $refText;
		}
		elseif($referenceTextPlacement == "textline1") {
			$orderData->TextLine1 = $refText;
		}
		elseif($referenceTextPlacement == "textline2") {
			$orderData->TextLine2 = $refText;
		}
		
		$this->log('$refText: '.$refText.' - placement: '.$referenceTextPlacement);

		if($this->getConfig('order_settings/payment_method_on_invoice')){
			$writePaymentMethodOnInvoice = true;
			
			$cardTypeText = $order->getPayment()->getMethodInstance()->getTitle();
			
			if($paymentExceptions = $this->getConfig('order_settings/payment_method_on_invoice_exceptions')){
				$paymentExceptions = explode(',', $paymentExceptions);
				foreach($paymentExceptions as $pEx){
					$pEx = trim($pEx);
					if($pEx == $paymentMethod || $pEx == $cardTypeText){
						$writePaymentMethodOnInvoice = false;
						break;
					}
				}
			}
			
			if($writePaymentMethodOnInvoice){
				$cardTypePlacement = $this->getConfig('order_settings/credit_card_type_placement');

				if($this->getConfig('order_settings/credit_card_type')){
					if($paymentMethod == 'quickpaypayment_payment'){
						if($cardType = $this->__getQuickpayCardType($orderIncrementId)){
							$cardTypeText = $cardType;
						}
					}
					elseif($paymentMethod == 'epay_standard'){
						if($cardType = $this->__getEpayCardType($orderIncrementId)){
							$cardTypeText = $cardType;
						}
					}
				}
				
				if($cardTypeTextPrefix = trim($this->getConfig('order_settings/payment_method_on_invoice_prefix'))){
					$cardTypeText = $cardTypeTextPrefix.' '.$cardTypeText;
				}
				
				if($cardTypeText && $refText && $this->getConfig('order_settings/credit_card_type_prefix_reference_text')){
					$cardTypeText = $refText.' - '.$cardTypeText;
				}
				
				if($cardTypeText){
					if($cardTypePlacement == "other_ref" && !$orderData->OtherReference) {
						$orderData->OtherReference = $cardTypeText;
					}
					elseif($cardTypePlacement == "heading") {
						$orderData->Heading = $cardTypeText;
					}
					elseif($cardTypePlacement == "textline1") {
						$orderData->TextLine1 = $cardTypeText;
					}
					elseif($cardTypePlacement == "textline2") {
						$orderData->TextLine2 = $cardTypeText;
					}
				}
			}
		}
		
		$this->log('reference/heading ok');

		/// CURRENCY RATE
		$baseCurrencyCode = Mage::app()->getBaseCurrencyCode();
		$allowedCurrencies = Mage::getModel('directory/currency')->getConfigAllowCurrencies();
		$currencyRates = Mage::getModel('directory/currency')->getCurrencyRates($baseCurrencyCode, array_values($allowedCurrencies));

		$economicBaseCurrency = $this->getConfig('general/economic_base_currency');
		if($economicBaseCurrency == $baseCurrencyCode)
		{
			$currencyRates[$economicBaseCurrency] = 1;
		}

		if(!isset($currencyRates[$economicBaseCurrency]))
		{
			throw new Exception(Mage::helper('economic')->__("Valuta omskrivning fra %s til %s ikke muligt.<br/>Sæt valutakurser i Magento under System -> Valutakurser",$baseCurrencyCode,$economicBaseCurrency));
		} elseif(!isset($currencyRates[$currencyCode]))
		{
			throw new Exception(Mage::helper('economic')->__("Valuta omskrivning fra %s til %s ikke muligt.<br/>Sæt valutakurser i Magento under System -> Valutakurser",$currencyCode,$economicBaseCurrency));
		}

		if (!empty($currencyRates))
		{
			$exchangeRate = pow($currencyRates[$currencyCode],-1)*$currencyRates[$economicBaseCurrency];
			$orderData->ExchangeRate = number_format(($exchangeRate*100),4,".","");
		}
		else
		{
			$exchangeRate = $currencyRates[$economicBaseCurrency];
			$orderData->ExchangeRate = number_format(($exchangeRate*100),4,".","");
		}
		
		$this->log('currency ok');

		// TERMS OF PAYMENT
		$termOfPaymentHandle = new stdClass();
		if($termOfPaymentId = $this->__getTermsOfPaymentId($paymentMethod,$orderIncrementId,$debtor)){
			$termOfPaymentHandle->Id = $termOfPaymentId;
		}
			
		if(!isset($termOfPaymentHandle->Id)){
			throw new Exception(Mage::helper('economic')->__("Payment Terms were not found :("));
		}
		$orderData->TermOfPaymentHandle = $termOfPaymentHandle;

		$grandTotal = ($invoice) ? $invoice->getGrandTotal() : $order->getGrandTotal();
		$grandTotal = number_format($grandTotal,4,".","");
		$orderData->NetAmount = $grandTotal;
		$orderData->GrossAmount = $grandTotal;
		
		$vatAmount = ($invoice) ? $invoice->getTaxAmount() : $order->getTaxAmount();
		$orderData->VatAmount = number_format($vatAmount,4,".","");

		$orderData->Margin = $grandTotal;
		$orderData->MarginAsPercent = "100.00";
		
		$createdAt = ($invoice) ? $invoice->getCreatedAt() : $order->getCreatedAt();
		$orderData->Date = $this->convertToXsdDate($createdAt);
		$orderData->DueDate = $this->convertToXsdDate($createdAt);

		$orderData->IsArchived = false;
		$orderData->IsSent = false;


		// ORDER COMMENT
		if($orderComment = $order->getData('onestepcheckout_customercomment')){
			$orderData->TextLine1 = "\n".Mage::helper('economic')->__('Comment').":\n".trim($orderComment);
		}
		elseif($orderComment = $order->getData('customer_note')){
			$orderData->TextLine1 = "\n".Mage::helper('economic')->__('Comment').":\n".trim($orderComment);
		}

		return $orderData;

	}


	/* ORDER LINES */
	public function setOrderLines($order,$economicId) {
		
		$this->log('set order lines');
		
		// Existing lines count
		$existingLinesCount = 0;
		if($order->getOrder()){ // Only relevant for invoices
			if($existingLines = $this->api->CurrentInvoice_GetLines($economicId)){
				$existingLinesCount = sizeof($existingLines);
			}
			else{
				$existingLinesCount = 0;
			}
		}
		
		$configProduct = null;
		$bundle = null;
		$lineArray = array();

		$totalItemDiscountAmount = 0;
		$salesLineLocations = array();
		$salesLineDepartments = array();

		foreach ($order->getAllItems() as $item) {

			$itemData = $item->getData();
			$orderItem = ($item->getOrderItem()) ? $item->getOrderItem() : $item;
			
			$this->log('item sku: '.$itemData['sku']);
			
			//$this->log('item data:'.serialize($itemData));

			$productType = $orderItem->getProductType();
			
			$variantAsProduct = $this->getConfig('product_settings/variants_as_products_in_economic');
			$bundleFixedPriceSkipChildren = $this->getConfig('product_settings/bundle_product_fixed_price_skip_children');
			$bundleDynamicPriceSkipBundle = $this->getConfig('product_settings/bundle_product_dynamic_price_skip_bundle');
			
			$this->log('productType: '.$productType);
			
			if($productType == 'simple' || $productType == 'virtual'){
				if($parentItem = $orderItem->getParentItem()){
					if($parentItem->getProductType() == 'configurable'){
						continue;
					}
					elseif($parentItem->getProductType() == 'bundle'){
						if($bundleProduct = Mage::getModel('catalog/product')->load($parentItem->getProductId())){
							if($bundleProduct->getPriceType() == 1 && $bundleFixedPriceSkipChildren){ //Fixed price and skip
								continue;
							}
						}
					}
				}
			}
			elseif($productType == 'configurable'){
				if($this->getConfig('product_settings/variant_name') == 'simple'){
					if($productIdBySku = Mage::getModel('catalog/product')->getIdBySku($itemData['sku'])){
						$simpleProduct = Mage::getModel('catalog/product')->load($productIdBySku);
						$name = $simpleProduct->getName();
						$item->setName($name);
						$itemData['name'] = $name;
					}
				}
				if(!$variantAsProduct){
					if($configurableProduct = Mage::getModel('catalog/product')->load($itemData['product_id'])){
						$sku = $configurableProduct->getSku();
						$item->setSku($sku);
						$itemData['sku'] = $sku;
					}
				}
			}
			elseif($productType == 'bundle'){
				if($bundleProduct = Mage::getModel('catalog/product')->load($itemData['product_id'])){
					if($bundleProduct->getPriceType() == 0){ //Dynamic price
						if($bundleDynamicPriceSkipBundle){
							continue;
						}
						$itemData['price'] = 0;
						$itemData['base_cost'] = 0;
					}
				}

				if($customBundleName = $this->getConfig('product_settings/bundle_product_name')){
					$item->setName($customBundleName);
					$itemData['name'] = $customBundleName;
				}
				if($customBundleSku = $this->getConfig('product_settings/bundle_product_number')){
					$item->setSku($customBundleSku);
					$itemData['sku'] = $customBundleSku;
				}
			}
			
			$this->log('product data ok');

			// Add custom options to order line
			$itemOptions = '';
			if($this->getConfig('product_settings/custom_options')){
				$orderItemOptions = $orderItem->getProductOptions();
				if($orderItemOptions && isset($orderItemOptions['options'])){
					$excludedItemOptions = explode(',', trim($this->getConfig('product_settings/custom_options_excluded')));
					foreach($orderItemOptions['options'] as $option){
						if(in_array($option['label'], $excludedItemOptions)){ continue; }
						$prefix = $this->getConfig('product_settings/custom_options_prefix');
						$itemOptions .= "\n".$prefix.$option['label'].': '.$option['value'];
					}
				}
			}

			$line = new stdClass();
			$line->Number = $existingLinesCount+count($lineArray)+1;

			if($order->getOrder()){
				$invoiceHandle = new stdClass();
				$invoiceHandle->Id = $economicId;
				$line->InvoiceHandle = $invoiceHandle;
			}
			else{
				$orderHandle = new stdClass();
				$orderHandle->Id = $economicId;
				$line->OrderHandle = $orderHandle;
			}


			$handle = new stdClass();
			$handle->Number = count($lineArray)+1;
			$handle->Id = 0;
			$line->Id = 0;
			$line->Handle = $handle;

			$productHandle = new stdClass();
			if ($standardProduct = $this->getConfig('product_settings/product_standard')) {
				$productHandle->Number = $standardProduct;
			}
			else {
				$this->log('get product number');
				if($productType == 'ugiftcert'){
					$productHandle->Number = $this->__getProductNumber($item,'giftvoucher');
				}
				else{
					$productHandle->Number = $this->__getProductNumber($item);
				}
				$this->log('product number ok');
			}
			$line->ProductHandle = $productHandle;

			$name = Mage::helper('core/string')->truncate($itemData['name'], 300);
			$line->Description = $name.$itemOptions;

			if($this->_isCreditmemo){
				$line->Quantity = $itemData['qty']*-1;
			}
			else{
				$line->Quantity = isset($itemData['qty_ordered']) ? $itemData['qty_ordered'] : $itemData['qty'];
			}
			
			$this->log('quantity ok');
			
			// FIX: Handles a problem with wrong item price because of third party extension, e.g. MageParts_Ddq
			if(round($itemData['price'],2) == round($itemData['price_incl_tax'],2) && $itemData['tax_amount'] > 0){
				$itemData['price'] = (($itemData['row_total_incl_tax'] - $itemData['tax_amount']) / $itemData['qty']);
			}

			$line->UnitNetPrice = $this->convertPrice($itemData['price']);

			$cost = ($itemData['base_cost']) ? $itemData['base_cost'] : 0;
			$line->UnitCostPrice = $this->convertPrice($cost);
			
			if($cost){
				$percentMargin = $line->UnitNetPrice ? (($line->UnitNetPrice - $line->UnitCostPrice) / $line->UnitNetPrice) * 100 : 0;
				$line->MarginAsPercent = number_format($percentMargin,2,'.','');
				$line->TotalMargin = $line->UnitNetPrice - $line->UnitCostPrice;
			}
			else{
				$line->MarginAsPercent = "100.00";
				$line->TotalMargin = $line->UnitNetPrice;
			}

			$unitHandle = new stdClass();
			$unitHandle->Number = $this->getConfig('product_settings/unit_type');
			$line->UnitHandle = $unitHandle;
			
			$this->log('unit ok');

			$line->DiscountAsPercent = "0";
			
			if(!$this->getConfig('discount_settings/product_discount_total_as_discount_line')){
				if($discountPercent = floatval($orderItem->getDiscountPercent())){
					if(isset($itemData['discount_amount']) && $itemData['discount_amount'] != 0){
						$itemDiscountAmount = $itemData['discount_amount'];
						$itemDiscountAmount = ($itemDiscountAmount > 0) ? $itemDiscountAmount*-1 : $itemDiscountAmount;
						$totalItemDiscountAmount += $itemDiscountAmount;
						$line->DiscountAsPercent = $discountPercent;
					}
				}
			}
			
			$this->log('discount ok');
			
			// Location
			if($locationType = $this->getConfig('product_settings/product_location_source')){
				if($locationType == 'store' && $location = $this->getConfig('product_settings/product_location_store')){
					$salesLineLocations[$line->Number] = $location;
				}
				elseif($locationType == 'product_attribute' && $locationAttribute = $this->getConfig('product_settings/product_location_attribute')){
					if($productIdBySku = Mage::getModel('catalog/product')->getIdBySku($productHandle->Number)){
						$product = Mage::getModel('catalog/product')->load($productIdBySku);
						if($product = Mage::getModel('catalog/product')->load($product->getId())){
							if(trim($location = $product->getData($locationAttribute))){
								$salesLineLocations[$line->Number] = $location;
							}
						}
						
					}
				}
			}
			
			// Department
			if($departmentType = $this->getConfig('product_settings/product_department_source')){
				if($departmentType == 'store' && $department = $this->getConfig('product_settings/product_department_store')){
					$salesLineDepartments[$line->Number] = $department;
				}
				elseif($departmentType == 'product_attribute' && $departmentAttribute = $this->getConfig('product_settings/product_department_attribute')){
					if($productIdBySku = Mage::getModel('catalog/product')->getIdBySku($productHandle->Number)){
						$product = Mage::getModel('catalog/product')->load($productIdBySku);
						if(trim($department = $product->getData($departmentAttribute))){
							$salesLineDepartments[$line->Number] = $department;
						}
					}
				}
			}
			
			$lineArray[] = $line;
			$this->log('line ok');

		}

		// rabatinfo
		if($orderDiscountAmount = floatval($order->getData('discount_amount'))){
			$totalItemDiscountAmount = ($totalItemDiscountAmount > 0) ? $totalItemDiscountAmount*-1 : $totalItemDiscountAmount;
			$orderDiscountAmount = ($orderDiscountAmount > 0) ? $orderDiscountAmount*-1 : $orderDiscountAmount;

			if($orderDiscountAmount != $totalItemDiscountAmount){
				$orderDiscountAmount = number_format($orderDiscountAmount-$totalItemDiscountAmount,4,'.','');
				if ($orderDiscountAmount != 0) {
					$lineNumber = $existingLinesCount+count($lineArray)+1;
					$line = $this->__createDiscountLine($order,$economicId,$lineNumber,$orderDiscountAmount);
					// Department
					if($departmentType = $this->getConfig('product_settings/product_department_source')){
						if($departmentType == 'store' && $department = $this->getConfig('product_settings/product_department_store')){
							$salesLineDepartments[$line->Number] = $department;
						}
					}
					$lineArray[] = $line;
				}
			}
		}

		if($order->getGiftcertAmount()){
			$lineNumber = $existingLinesCount+count($lineArray)+1;
			$line = $this->__createGiftvoucherLine($order,$economicId,$lineNumber);
			// Department
			if($departmentType = $this->getConfig('product_settings/product_department_source')){
				if($departmentType == 'store' && $department = $this->getConfig('product_settings/product_department_store')){
					$salesLineDepartments[$line->Number] = $department;
				}
			}
			$lineArray[] = $line;
		}
		
		// REWARD POINT DISCOUNT		
		if ($order->getData('mw_rewardpoint') != 0) {
			$lineNumber = $existingLinesCount+count($lineArray)+1;
			$line = $this->__createRewardPointDiscountLine($order,$economicId,$lineNumber);
			// Department
			if($departmentType = $this->getConfig('product_settings/product_department_source')){
				if($departmentType == 'store' && $department = $this->getConfig('product_settings/product_department_store')){
					$salesLineDepartments[$line->Number] = $department;
				}
			}
			$lineArray[] = $line;
		}
		
		// SURCHARGE
		$surchargeAmount = 0;
		
		if($foomanSurcharge = $order->getData('fooman_surcharge_amount')){
			$surchargeAmount = $foomanSurcharge;
		}elseif($gremlinSurcharge = $order->getData('gremlin_simplesurcharge_total')){
			$surchargeAmount = $gremlinSurcharge;
		}
		
		if ($surchargeAmount != 0) {
			$lineNumber = $existingLinesCount+count($lineArray)+1;
			$line = $this->__createSurchargeLine($order,$economicId,$lineNumber,$surchargeAmount);
			// Department
			if($departmentType = $this->getConfig('product_settings/product_department_source')){
				if($departmentType == 'store' && $department = $this->getConfig('product_settings/product_department_store')){
					$salesLineDepartments[$line->Number] = $department;
				}
			}
			$lineArray[] = $line;
		}

		// ADJUSTMENT
		if ($order->getData('adjustment') != 0) {
			$lineNumber = $existingLinesCount+count($lineArray)+1;
			$line = $this->__createAdjustmentLine($order,$economicId,$lineNumber);
			// Department
			if($departmentType = $this->getConfig('product_settings/product_department_source')){
				if($departmentType == 'store' && $department = $this->getConfig('product_settings/product_department_store')){
					$salesLineDepartments[$line->Number] = $department;
				}
			}
			$lineArray[] = $line;
		}
		
		// PAYMENT FEE
		$paymentMethod = ($order->getOrder()) ? $order->getOrder()->getPayment()->getMethod() : $order->getPayment()->getMethod();
		if ($paymentMethod == 'klarna_invoice') {
			$payment = ($order->getOrder()) ? $order->getOrder()->getPayment() : $order->getPayment();
			$info = $payment->getMethodInstance()->getInfoInstance();
			if($info->getAdditionalInformation('invoice_fee')){
				if($paymentFee = $info->getAdditionalInformation('invoice_fee_exluding_vat')){
					$lineNumber = $existingLinesCount+count($lineArray)+1;
					$line = $this->__createPaymentFeeLine($order,$economicId,$lineNumber,$paymentFee);
					// Department
					if($departmentType = $this->getConfig('product_settings/product_department_source')){
						if($departmentType == 'store' && $department = $this->getConfig('product_settings/product_department_store')){
							$salesLineDepartments[$line->Number] = $department;
						}
					}
					$lineArray[] = $line;
				}
			}
		}
		elseif ($paymentMethod == 'vaimo_klarna_invoice') {
			$payment = ($order->getOrder()) ? $order->getOrder()->getPayment() : $order->getPayment();
			$info = $payment->getMethodInstance()->getInfoInstance();
			if($info->getAdditionalInformation('vaimo_klarna_fee')){
				if($paymentFee = ($info->getAdditionalInformation('vaimo_klarna_fee')-$info->getAdditionalInformation('vaimo_klarna_fee_tax'))){
					$lineNumber = $existingLinesCount+count($lineArray)+1;
					$line = $this->__createPaymentFeeLine($order,$economicId,$lineNumber,$paymentFee);
					// Department
					if($departmentType = $this->getConfig('product_settings/product_department_source')){
						if($departmentType == 'store' && $department = $this->getConfig('product_settings/product_department_store')){
							$salesLineDepartments[$line->Number] = $department;
						}
					}
					$lineArray[] = $line;
				}
			}
		}
		elseif ($paymentMethod == 'epay_standard') {
			$resource = Mage::getSingleton('core/resource');
			$read = $resource->getConnection('core_read');
			$orderIncrementId = ($order->getOrder()) ? $order->getOrder()->getIncrementId() : $order->getIncrementId();
			$query = "SELECT * FROM epay_order_status WHERE orderid = '$orderIncrementId'";
			$row = $read->fetchRow($query);
			if($row && $row['transfee']){
				if($paymentFee = ($row['transfee']/100)){
					$lineNumber = $existingLinesCount+count($lineArray)+1;
					$line = $this->__createPaymentFeeLine($order,$economicId,$lineNumber,$paymentFee);
					// Department
					if($departmentType = $this->getConfig('product_settings/product_department_source')){
						if($departmentType == 'store' && $department = $this->getConfig('product_settings/product_department_store')){
							$salesLineDepartments[$line->Number] = $department;
						}
					}
					$lineArray[] = $line;
					
					// Substract the paymentfee from shipping amount
					$shippingAmount = $order->getData('shipping_amount')-$paymentFee;
					$order->setData('shipping_amount',$shippingAmount);
				}
			}
		}
		else{
			$paymentFee = ($order->getOrder()) ? $order->getOrder()->getData('paymentfee_total') : $order->getData('paymentfee_total'); 
			if($paymentFee){
				$lineNumber = $existingLinesCount+count($lineArray)+1;
				$line = $this->__createPaymentFeeLine($order,$economicId,$lineNumber,$paymentFee);
				// Department
				if($departmentType = $this->getConfig('product_settings/product_department_source')){
					if($departmentType == 'store' && $department = $this->getConfig('product_settings/product_department_store')){
						$salesLineDepartments[$line->Number] = $department;
					}
				}
				$lineArray[] = $line;
			}
		}
		
		// SHIPPING
		$isVirtual = ($order->getOrder()) ? $order->getOrder()->getIsVirtual() : $order->getIsVirtual();
		if (!$isVirtual) {
			$lineNumber = $existingLinesCount+count($lineArray)+1;
			$line = $this->__createShippingLine($order,$economicId,$lineNumber);
			// Department
			if($departmentType = $this->getConfig('product_settings/product_department_source')){
				if($departmentType == 'store' && $department = $this->getConfig('product_settings/product_department_store')){
					$salesLineDepartments[$line->Number] = $department;
				}
			}
			$lineArray[] = $line;
		}

		
		if($order->getOrder()){ //invoice
			if(!$this->api->CurrentInvoiceLine_CreateFromDataArray($lineArray)){
				$this->log('Failed when creating invoice lines...');
				//$this->api->CurrentInvoice_Delete($economicId);
				return false;
			}
		}
		else{
			if(!$response = $this->api->OrderLine_CreateFromDataArray($lineArray)){
				$this->log('Failed when creating order lines...');
				//$this->api->Order_Delete($economicId);
				return false;
			}
		}
		
		
		// INVENTORY LOCATION ON SALES LINES
		if(!empty($salesLineLocations)){
			foreach($salesLineLocations as $number => $location){
				try{
					if($order->getOrder()){ //invoice
						$this->api->CurrentInvoiceLine_SetInventoryLocation($economicId,$number,$location);
					}
					else{
						$this->api->OrderLine_SetInventoryLocation($economicId,$number,$location);
					}
				}
				catch(Exception $e){
					$this->__addError(Mage::helper('economic')->__('Problems in e-conomic with setting location %s on line number %s',$location,$number));
				}
			}
		}
		// DEPARTMENT ON SALES LINES
		if(!empty($salesLineDepartments)){
			foreach($salesLineDepartments as $number => $department){
				try{
					if($order->getOrder()){ //invoice
						$this->api->CurrentInvoiceLine_SetDepartment($economicId,$number,$department);
					}
					else{
						$this->api->OrderLine_SetDepartment($economicId,$number,$department);
					}
				}
				catch(Exception $e){
					$this->__addError(Mage::helper('economic')->__('Problems in e-conomic with setting department %s on line number %s',$department,$number));
				}
			}
		}

		// INTEGRITY CHECK
		if($this->_doIntegrityCheck && $this->getConfig('order_settings/integrity_check')){
			$this->log('Performing integrity check');
			
			if($order->getOrder()){ // invoices / creditmemos
				$economicData = $this->api->CurrentInvoice_GetData($economicId);
			}
			else{ // order
				$economicData = $this->api->Order_GetData($economicId);
			}
			$orderGrandTotal = $order->getGrandTotal();
			if($this->_isCreditmemo){
				$orderGrandTotal = $orderGrandTotal*-1;
			}
			
			$differenceAmount = round($orderGrandTotal-$economicData->GrossAmount,2);
			$this->log('Magento Grand Total = '.$orderGrandTotal.' | '.'e-conomic Gross Amount = '.$economicData->GrossAmount.' | '.'Difference = '.$differenceAmount);
			
			$checkMoreThan = str_replace(',', '.', $this->getConfig('order_settings/integrity_check_difference_more_than'));
			if(!$checkMoreThan){ $checkMoreThan = 0; }
			
			if($differenceAmount != 0 && abs($differenceAmount) > $checkMoreThan){
				$this->_integrityCheckFailed = true;

				if($integrityCheckEmail = $this->getConfig('order_settings/integrity_check_email')){
					$mail = Mage::getModel('core/email');
					$mail->setToName('');
					$mail->setToEmail($integrityCheckEmail);
					$mail->setFromName(Mage::getStoreConfig('trans_email/ident_general/email'));
					$mail->setFromEmail(Mage::getStoreConfig('trans_email/ident_general/name'));
					$mail->setSubject(Mage::helper('economic')->__('Magento e-conomic integrity check failed - #%s',$order->getIncrementId()));
					$mail->setType('text');

					$content = Mage::helper('economic')->__("Integrity check failed - see information below:\n\n");

					
					if($order->getOrder()){ //invoice
						$content .= Mage::helper('economic')->__("Magento Order: %s",$order->getOrder()->getIncrementId())."\n";
						$content .= Mage::helper('economic')->__("Magento Invoice/creditmemo: %s",$order->getIncrementId())."\n\n";
					}
					$content .= Mage::helper('economic')->__("Magento Grand Total: %s",$orderGrandTotal)."\n";
					$content .= Mage::helper('economic')->__("E-conomic Grand Total: %s",$economicData->GrossAmount)."\n\n";
					
					$content .= Mage::helper('economic')->__("Difference: %s",$differenceAmount)."\n\n\n";

					$content .= Mage::helper('economic')->__("This e-mail was sent automatically from the Magento e-conomic extension.")."\n\n";
					$content .= Mage::helper('economic')->__("Have a nice day...")."\n\n";

					$mail->setBody($content);

					try {
						$mail->send();
						$this->log('Integrity check mail sent OK');
					}
					catch (Exception $e) {
						$this->log('Error sending mail: '.$e->getMessage());
					}
				}

				if($this->getConfig('order_settings/integrity_correction_line')){
					$lineNumber = $existingLinesCount+count($lineArray)+1;
					try
					{
						$line = $this->__createCorrectionLine($order,$economicId,$lineNumber,$differenceAmount);
						if($order->getOrder()){
							$this->api->CurrentInvoiceLine_CreateFromData($line);
						}
						else{
							$this->api->OrderLine_CreateFromData($line);
						}
					}
					catch(Exception $e) {
						$this->addError($e->getMessage());
					}
				}
			}
		}

		return true;

	}
	
	protected function _sendErrorEmail($order)
	{
		$mail = Mage::getModel('core/email');
		$mail->setToName('Store Manager');
		$mail->setToEmail($integrityCheckEmail);
		$mail->setFromName(Mage::getStoreConfig('trans_email/ident_general/email'));
		$mail->setFromEmail(Mage::getStoreConfig('trans_email/ident_general/name'));
		$mail->setSubject(Mage::helper('economic')->__('Magento e-conomic ERROR occurred - #%s',$order->getIncrementId()));
		$mail->setType('text');

		$content = Mage::helper('economic')->__("Integrity check failed - see information below:\n\n");

		if($order->getOrder()){ //invoice
			$content .= Mage::helper('economic')->__("Magento Order: %s",$order->getOrder()->getIncrementId())."\n";
			$content .= Mage::helper('economic')->__("Magento Invoice/creditmemo: %s",$order->getIncrementId())."\n\n";
		}
		else{
			$content .= Mage::helper('economic')->__("Magento Order: %s",$order->getIncrementId())."\n";
		}

		$content .= Mage::helper('economic')->__("This e-mail was sent automatically from the Magento e-conomic extension.")."\n\n";
		$content .= Mage::helper('economic')->__("Have a nice day...")."\n\n";

		$mail->setBody($content);

		try {
			$mail->send();
			$this->log('Integrity check mail sent OK');
		}
		catch (Exception $e) {
			$this->log('Error sending mail: '.$e->getMessage());
		}
	}

	private function __getProductNumber($item,$type=false) {
		$specialProductTypes = array('discount','correction','adjustment','shipping','giftvoucher','rewardpoint','surcharge','paymentfee');
		if(in_array($type, $specialProductTypes)){
			$product = $item;
		}
		else{
			if(is_array($item)){
				$sku = $item['sku'];
			}
			else{
				$sku = $item->getSku();
			}
						
			if($productIdBySku = Mage::getModel('catalog/product')->getIdBySku($sku)){
				$product = Mage::getModel('catalog/product')->load($productIdBySku);
			}
			else{
				if(!is_array($item)){
					$product = $item->getData();
				}
				else{
					$product = $item;
				}
			}

		}

		if($economicProduct = Mage::getSingleton('economic/product')->syncProductToEconomic($product,$type)){
			return $economicProduct->Number;
		}
		else{
			return false;
		}
	}


	private function __createGiftvoucherLine($order,$economicId,$lineNumber) {
		$giftCode = ($order->getOrder()) ? $order->getOrder()->getGiftcertCode() : $order->getGiftcertCode();
		$giftAmount = $order->getGiftcertAmount();

		$line = new stdClass();
		$line->Number = $lineNumber;

		if($order->getOrder()){ //invoice
			$invoiceHandle = new stdClass();
			$invoiceHandle->Id = $economicId;
			$line->InvoiceHandle = $invoiceHandle;
		}
		else{
			$orderHandle = new stdClass();
			$orderHandle->Id = $economicId;
			$line->OrderHandle = $orderHandle;
		}

		$handle = new stdClass();
		$handle->Number = $lineNumber;
		$handle->Id = 0;
		$line->Id = 0;
		$line->Handle = $handle;

		$productHandle = new stdClass();
		
		if(!$sku = $this->getConfig('giftvoucher_settings/giftvoucher_product_number')){
			$sku = Mage::helper('economic')->__('giftvoucher');
		}
		if(!$name = $this->getConfig('giftvoucher_settings/giftvoucher_product_name')){
			$name = Mage::helper('economic')->__('Gift Voucher');
		}

		$item = array(
			"sku" => $sku,
			"price" => $giftAmount,
			"cost" => 0,
			"name" => $name
		);
		$productHandle->Number = $this->__getProductNumber($item,'giftvoucher');

		$line->ProductHandle = $productHandle;

		$line->Description = $name." ($giftCode)";

		if($this->_isCreditmemo){
			$line->Quantity = -1;
			$giftAmount = $giftAmount;
		}
		else{
			$line->Quantity = 1;
			$giftAmount = $giftAmount*-1;
		}

		$line->UnitNetPrice = $this->convertPrice($giftAmount);
		$line->UnitCostPrice = $this->convertPrice($giftAmount);

		$line->MarginAsPercent = "100.00";
		$line->TotalMargin = $line->UnitNetPrice;

		$unitHandle = new stdClass();
		$unitHandle->Number = $this->getConfig('product_settings/unit_type');
		$line->UnitHandle = $unitHandle;

		$line->DiscountAsPercent = "0";
		return $line;
	}


	private function __createDiscountLine($order,$economicId,$lineNumber,$orderDiscountAmount) {
		
		if(Mage::getStoreConfig('tax/calculation/apply_after_discount') == '1' && Mage::getStoreConfig('tax/calculation/discount_tax') == '0'){
			$amount = $orderDiscountAmount;
		}
		else{
			$amount = $this->__getAmountExcludingTaxBasedOnOrderItemTaxRate($order,$orderDiscountAmount);
		}
		
		$amount = ($amount > 0) ? $amount*-1 : $amount;
		
		$line = new stdClass();
		$line->Number = $lineNumber;

		if($order->getOrder()){ //invoice
			$invoiceHandle = new stdClass();
			$invoiceHandle->Id = $economicId;
			$line->InvoiceHandle = $invoiceHandle;
		}
		else{
			$orderHandle = new stdClass();
			$orderHandle->Id = $economicId;
			$line->OrderHandle = $orderHandle;
		}

		$handle = new stdClass();
		$handle->Number = $lineNumber;
		$handle->Id = 0;
		$line->Id = 0;
		$line->Handle = $handle;

		$productHandle = new stdClass();
		
		if(!$sku = $this->getConfig('discount_settings/product_discount_sku')){
			$sku = Mage::helper('economic')->__('discount');
		}
		if(!$name = $this->getConfig('discount_settings/product_discount_name')){
			$name = Mage::helper('economic')->__('Discount');
		}

		$item = array("sku" => $sku,
			"price" => $amount,
			"base_cost" => $amount,
			"name" => $name
		);
		$productHandle->Number = $this->__getProductNumber($item,'discount');

		$line->ProductHandle = $productHandle;
		
		if($this->getConfig('discount_settings/product_discount_description_as_name') && $order->getData('discount_description')){
			$name = $order->getData('discount_description');
		}

		$line->Description = $name;

		if($this->_isCreditmemo){
			$line->Quantity = -1;
		}
		else{
			$line->Quantity = 1;
		}

		$line->UnitNetPrice = $this->convertPrice($amount);
		$line->UnitCostPrice = $this->convertPrice($amount);

		$line->MarginAsPercent = "100.00";
		$line->TotalMargin = $line->UnitNetPrice;

		$unitHandle = new stdClass();
		$unitHandle->Number = $this->getConfig('product_settings/unit_type');
		$line->UnitHandle = $unitHandle;

		$line->DiscountAsPercent = "0";
		return $line;
	}
	
	private function __createRewardPointDiscountLine($order,$economicId,$lineNumber) {
		
		$amount = $order->getData('mw_rewardpoint');
		$amount = $this->__getAmountExcludingTaxBasedOnOrderItemTaxRate($order,$amount);
		$amount = ($amount > 0) ? $amount*-1 : $amount;

		$line = new stdClass();
		$line->Number = $lineNumber;

		if($order->getOrder()){ //invoice
			$invoiceHandle = new stdClass();
			$invoiceHandle->Id = $economicId;
			$line->InvoiceHandle = $invoiceHandle;
		}
		else{
			$orderHandle = new stdClass();
			$orderHandle->Id = $economicId;
			$line->OrderHandle = $orderHandle;
		}

		$handle = new stdClass();
		$handle->Number = $lineNumber;
		$handle->Id = 0;
		$line->Id = 0;
		$line->Handle = $handle;

		$productHandle = new stdClass();
		
		if(!$sku = $this->getConfig('rewardpoint_settings/rewardpoint_product_name')){
			$sku = Mage::helper('economic')->__('rewardpoint');
		}
		if(!$name = $this->getConfig('rewardpoint_settings/rewardpoint_product_number')){
			$name = Mage::helper('economic')->__('Reward Point');
		}

		$item = array("sku" => $sku,
			"price" => $amount,
			"base_cost" => $amount,
			"name" => $name
		);
		$productHandle->Number = $this->__getProductNumber($item,'rewardpoint');

		$line->ProductHandle = $productHandle;

		$line->Description = $name;

		if($this->_isCreditmemo){
			$line->Quantity = -1;
			$amount = $amount*-1;
		}
		else{
			$line->Quantity = 1;
		}

		$line->UnitNetPrice = $this->convertPrice($amount);
		$line->UnitCostPrice = $this->convertPrice($amount);

		$line->MarginAsPercent = "100.00";
		$line->TotalMargin = $line->UnitNetPrice;

		$unitHandle = new stdClass();
		$unitHandle->Number = $this->getConfig('product_settings/unit_type');
		$line->UnitHandle = $unitHandle;

		$line->DiscountAsPercent = "0";
		return $line;
	}



	private function __createAdjustmentLine($order,$economicId,$lineNumber) {
		
		$amount = $order->getData('adjustment');
		$amount = $this->__getAmountExcludingTaxBasedOnOrderItemTaxRate($order,$amount);
		$amount = $amount*-1;
		
		$line = new stdClass();
		$line->Number = $lineNumber;

		if($order->getOrder()){ //invoice
			$invoiceHandle = new stdClass();
			$invoiceHandle->Id = $economicId;
			$line->InvoiceHandle = $invoiceHandle;
		}
		else{
			$orderHandle = new stdClass();
			$orderHandle->Id = $economicId;
			$line->OrderHandle = $orderHandle;
		}

		$handle = new stdClass();
		$handle->Number = $lineNumber;
		$handle->Id = 0;
		$line->Id = 0;
		$line->Handle = $handle;

		$productHandle = new stdClass();
		
		if(!$sku = $this->getConfig('adjustment_settings/adjustment_product_number')){
			$sku = Mage::helper('economic')->__('adjustment');
		}
		if(!$name = $this->getConfig('adjustment_settings/adjustment_product_name')){
			$name = Mage::helper('economic')->__('Adjustment');
		}

		$item = array("sku" => $sku,
			"price" => $amount,
			"base_cost" => 0,
			"name" => $name
		);

		$productHandle->Number = $this->__getProductNumber($item,'adjustment');
		
		$line->ProductHandle = $productHandle;

		$line->Description = $name;

		$line->Quantity = 1;

		$line->UnitNetPrice = $this->convertPrice($amount);
		$line->UnitCostPrice = $this->convertPrice($amount);

		$line->MarginAsPercent = "100.00";
		$line->TotalMargin = $line->UnitNetPrice;

		$unitHandle = new stdClass();
		$unitHandle->Number = $this->getConfig('product_settings/unit_type');
		$line->UnitHandle = $unitHandle;

		$line->DiscountAsPercent = "0";
		return $line;

	}

	private function __createShippingLine($order,$economicId,$lineNumber) {

		$line = new stdClass();
		$line->Number = $lineNumber;
		
		$shippingMethodDescription = $this->getConfig('shipping_settings/shipping_product_name');

		if($order->getOrder()){ //invoice
			$invoiceHandle = new stdClass();
			$invoiceHandle->Id = $economicId;
			$line->InvoiceHandle = $invoiceHandle;
			if(!$shippingMethodDescription){
				$shippingMethodDescription = $order->getOrder()->getShippingDescription();
			}
		}
		else{
			$orderHandle = new stdClass();
			$orderHandle->Id = $economicId;
			$line->OrderHandle = $orderHandle;
			if(!$shippingMethodDescription){
				$shippingMethodDescription = $order->getShippingDescription();
			}
		}

		$handle = new stdClass();
		$handle->Number = $lineNumber;
		$handle->Id = 0;
		$line->Id = 0;
		$line->Handle = $handle;

		$productHandle = new stdClass();

		$amount = $order->getData('shipping_amount');
		
		if($order->getOrder()){ //invoice
			$shippingMethod = $order->getOrder()->getData('shipping_method');
		}
		else{
			$shippingMethod = $order->getData('shipping_method');
		}
		$shippingProductNumber = $this->getConfig('shipping_settings/shipping_product_number');
		
		if($shippingNumberDependingOnShippingMethod = $this->getConfig('shipping_settings/shipping_product_depending_on_shipping_method')){
			$shippingNumberDependingOnShippingMethod = unserialize($shippingNumberDependingOnShippingMethod);		
			sort($shippingNumberDependingOnShippingMethod);
			
			foreach($shippingNumberDependingOnShippingMethod as $m){
				$shippingMethodId = $m['shipping_method_id'];
				
				if($shippingMethodId == $shippingMethod){
					$shippingProductNumber = $m['shipping_product_number'];
				}
				else{
					$shippingMethodExploded = explode('_', $shippingMethod);
					if($shippingMethodId == $shippingMethodExploded[0]){
						$shippingProductNumber = $m['shipping_product_number'];
					}
				}
			}
		}

		if ($shippingProductNumber) {
			$item = array(
				"sku" => $shippingProductNumber,
				"price" => $amount,
				"cost" => $amount,
				"name" => $shippingMethodDescription
			);
			$productHandle->Number = $this->__getProductNumber($item,'shipping');
		}
		else {
			if($order->getOrder()){ //invoice
				$shippingMethod = $order->getOrder()->getData('shipping_method');
				$countryId = strtoupper($order->getOrder()->getShippingAddress()->getData('country_id'));
			}
			else{
				$shippingMethod = $order->getData('shipping_method');
				$countryId = strtoupper($order->getShippingAddress()->getData('country_id'));
			}
			$item = array(
				"sku" => substr($shippingMethod,0,20).'_'.$countryId,
				"price" => $amount,
				"cost" => $amount,
				"name" => $shippingMethodDescription
			);
			$productHandle->Number = $this->__getProductNumber($item,'shipping');
		}
		
		$line->ProductHandle = $productHandle;

		$name = Mage::helper('core/string')->truncate($shippingMethodDescription, 300);

		$line->Description = $name;

		if($this->_isCreditmemo){
			$line->Quantity = -1;
		}
		else{
			$line->Quantity = 1;
		}

		$line->UnitNetPrice = $this->convertPrice($amount);
		$line->UnitCostPrice = $this->convertPrice($amount);

		$line->MarginAsPercent = "100.00";
		$line->TotalMargin = $line->UnitNetPrice;

		$unitHandle = new stdClass();
		$unitHandle->Number = $this->getConfig('product_settings/unit_type');
		$line->UnitHandle = $unitHandle;

		$line->DiscountAsPercent = "0";
		return $line;
	}

	private function __createCorrectionLine($order,$economicId,$lineNumber,$amount) {
		
		$amount = $this->__getAmountExcludingTaxBasedOnOrderItemTaxRate($order,$amount);
		
		$line = new stdClass();
		$line->Number = $lineNumber;

		if($order->getOrder()){ //invoice
			$invoiceHandle = new stdClass();
			$invoiceHandle->Id = $economicId;
			$line->InvoiceHandle = $invoiceHandle;
		}
		else{
			$orderHandle = new stdClass();
			$orderHandle->Id = $economicId;
			$line->OrderHandle = $orderHandle;
		}

		$handle = new stdClass();
		$handle->Number = $lineNumber;
		$handle->Id = 0;
		$line->Id = 0;
		$line->Handle = $handle;

		$productHandle = new stdClass();
		
		if(!$sku = $this->getConfig('order_settings/integrity_correction_line_sku')){
			$sku = Mage::helper('economic')->__('correction');
		}
		if(!$name = $this->getConfig('order_settings/integrity_correction_line_name')){
			$name = Mage::helper('economic')->__('Correction');
		}

		$item = array(
			"sku" => $sku,
			"price" => $amount,
			"cost" => 0,
			"name" => $name
		);
		$productHandle->Number = $this->__getProductNumber($item,'correction');

		$line->ProductHandle = $productHandle;

		$line->Description = $name;

		$line->Quantity = 1;

		$line->UnitNetPrice = $this->convertPrice($amount);
		$line->UnitCostPrice = $this->convertPrice($amount);

		$line->MarginAsPercent = "100.00";
		$line->TotalMargin = $line->UnitNetPrice;

		$unitHandle = new stdClass();
		$unitHandle->Number = $this->getConfig('product_settings/unit_type');
		$line->UnitHandle = $unitHandle;

		$line->DiscountAsPercent = "0";
		return $line;
	}
	
	private function __createPaymentFeeLine($order,$economicId,$lineNumber,$amount) {
				
		$line = new stdClass();
		$line->Number = $lineNumber;

		if($order->getOrder()){ //invoice
			$invoiceHandle = new stdClass();
			$invoiceHandle->Id = $economicId;
			$line->InvoiceHandle = $invoiceHandle;
		}
		else{
			$orderHandle = new stdClass();
			$orderHandle->Id = $economicId;
			$line->OrderHandle = $orderHandle;
		}

		$handle = new stdClass();
		$handle->Number = $lineNumber;
		$handle->Id = 0;
		$line->Id = 0;
		$line->Handle = $handle;

		$productHandle = new stdClass();

		if(!$sku = $this->getConfig('payment_fee_settings/payment_fee_product_number')){
			$sku = Mage::helper('economic')->__('paymentfee');
		}
		if(!$name = $this->getConfig('payment_fee_settings/payment_fee_product_name')){
			$name = Mage::helper('economic')->__('Payment Fee');
		}
		
		$item = array(
			"sku" => $sku,
			"price" => $amount,
			"cost" => 0,
			"name" => $name
		);
		$productHandle->Number = $this->__getProductNumber($item,'paymentfee');

		$line->ProductHandle = $productHandle;

		$line->Description = $name;

		if($this->_isCreditmemo){
			$line->Quantity = -1;
		}
		else{
			$line->Quantity = 1;
		}

		$line->UnitNetPrice = $this->convertPrice($amount);
		$line->UnitCostPrice = $this->convertPrice($amount);

		$line->MarginAsPercent = "100.00";
		$line->TotalMargin = $line->UnitNetPrice;

		$unitHandle = new stdClass();
		$unitHandle->Number = $this->getConfig('product_settings/unit_type');
		$line->UnitHandle = $unitHandle;

		$line->DiscountAsPercent = "0";
		return $line;
	}
	
	private function __createSurchargeLine($order,$economicId,$lineNumber,$surchargeAmount) {
		
		$amount = $surchargeAmount;
		
		$line = new stdClass();
		$line->Number = $lineNumber;

		if($order->getOrder()){ //invoice
			$invoiceHandle = new stdClass();
			$invoiceHandle->Id = $economicId;
			$line->InvoiceHandle = $invoiceHandle;
		}
		else{
			$orderHandle = new stdClass();
			$orderHandle->Id = $economicId;
			$line->OrderHandle = $orderHandle;
		}

		$handle = new stdClass();
		$handle->Number = $lineNumber;
		$handle->Id = 0;
		$line->Id = 0;
		$line->Handle = $handle;

		$productHandle = new stdClass();

		if(!$sku = $this->getConfig('surcharge_settings/surcharge_product_number')){
			$sku = Mage::helper('economic')->__('surcharge');
		}
		if(!$name = $this->getConfig('surcharge_settings/surcharge_product_name')){
			$name = Mage::helper('economic')->__('Surcharge');
		}
		
		$item = array(
			"sku" => $sku,
			"price" => $amount,
			"cost" => 0,
			"name" => $name
		);
		$productHandle->Number = $this->__getProductNumber($item,'surcharge');

		$line->ProductHandle = $productHandle;

		$line->Description = $name;

		if($this->_isCreditmemo){
			$line->Quantity = -1;
		}
		else{
			$line->Quantity = 1;
		}

		$line->UnitNetPrice = $this->convertPrice($amount);
		$line->UnitCostPrice = $this->convertPrice($amount);

		$line->MarginAsPercent = "100.00";
		$line->TotalMargin = $line->UnitNetPrice;

		$unitHandle = new stdClass();
		$unitHandle->Number = $this->getConfig('product_settings/unit_type');
		$line->UnitHandle = $unitHandle;

		$line->DiscountAsPercent = "0";
		return $line;
	}
	
	

	private function __getShippingAmountExcludingTax($order){

		$invoice = false;
		if($order->getOrder()){
			$invoice = $order;
			$order = $order->getOrder();
		}

		$store = Mage::app()->getStore($order->getStoreId());
		$customer = Mage::getModel('customer/customer')
		->load($order->getCustomerId());

		$tax_calc = Mage::getSingleton('tax/calculation');

		$tax_rate_req = $tax_calc->getRateRequest(
			$order->getShippingAddress(),
			$order->getBillingAddress(),
			$customer->getTaxClassId(),
			$store);

		if($invoice){
			$amount = (float)$invoice->getData('shipping_incl_tax');
			$discountAmount = (float)$invoice->getData('shipping_discount_amount');
		}
		else{
			$amount = (float)$order->getData('shipping_incl_tax');
			$discountAmount = (float)$order->getData('shipping_discount_amount');
		}

		if($amount > 0){
			$tax_mod = $tax_calc->getRate($tax_rate_req->setProductClassId(
					Mage::getStoreConfig('tax/classes/shipping_tax_class')));
			$tax_mod /= 100;

			$amount -= $discountAmount;
			$amount = ($amount / (1 + $tax_mod));
			$amount = round($amount, 2);
		}

		return $amount;
	}
	
	
	private function __getAmountExcludingTaxBasedOnOrderItemTaxRate($order,$amount){
		$invoice = false;
		if($order->getOrder()){
			$invoice = $order;
			$order = $order->getOrder();
		}

		$store = Mage::app()->getStore($order->getStoreId());
		$customer = Mage::getModel('customer/customer')
		->load($order->getCustomerId());

		$tax_calc = Mage::getSingleton('tax/calculation');

		$tax_rate_req = $tax_calc->getRateRequest(
			$order->getShippingAddress(),
			$order->getBillingAddress(),
			$customer->getTaxClassId(),
			$store);

		$tax_class_id = false;
		$tax_percent = false;
		foreach($order->getAllItems() as $item){
			if($item->getTaxPercent() && $item->getTaxPercent() > 0){
				$tax_percent = $item->getTaxPercent();
				break;
			}
			if($product = Mage::getModel('catalog/product')->load($item->getProductId())){
				if($tax_class_id = $product->getTaxClassId()){
					break;
				}
			}
		}

		$tax_mod = 0;
		if($tax_percent){
			$tax_mod = $tax_percent;
			$tax_mod /= 100;
		}
		elseif($tax_class_id){
			$tax_mod = $tax_calc->getRate($tax_rate_req->setProductClassId($tax_class_id));
			$tax_mod /= 100;
		}

		$amount = ($amount / (1 + $tax_mod));
		$amount = round($amount, 2);

		return $amount;
	}
	
	protected function __bookInvoiceAutomatic($order,$economicId) {
		$paymentExceptions = explode(',', $this->getConfig('order_settings/book_payment_exceptions'));
		$paymentMethod = ($order->getOrder()) ? $order->getOrder()->getPayment()->getMethod() : $order->getPayment()->getMethod();
		
		if(in_array($paymentMethod, $paymentExceptions)){
			$this->log('Auto book skipped because of payment method');
			return false;
		}
		
		if($this->getConfig('order_settings/book_with_number') == "economic"){
			$invoiceNumber = $this->api->CurrentInvoice_Book($economicId);
		}
		elseif($this->getConfig('order_settings/book_with_number') == "magento"){
			$invoiceNumber = $this->api->CurrentInvoice_BookWithNumber($economicId,$order->getIncrementId());
		}
		
		if($this->getConfig('order_settings/send_invoice_pdf')){
			try{
				$this->__sendInvoicePdf($order,$invoiceNumber);
			}
			catch(Exception $e){
				$this->log('sendInvoicePdf error: '.$e->getMessage());
			}
		}
		
		return $invoiceNumber;
	}
	
	protected function __sendInvoicePdf($order,$invoiceNumber){
		$this->log('Send invoice PDF');
		if(!$invoicePdf = $this->api->Invoice_GetPdf($invoiceNumber)){
			$this->log('Problems with getting the invoice PDF from e-conomic');
			return false;
		}
		
		if($order->getOrder()){
			$invoice = $order;
			$order = $order->getOrder();
		}
		else{
			$relation = Mage::getModel('economic/order')->load($order->getIncrementId(), 'ord_num');
			if($invoice = Mage::getModel('sales/order_invoice')->loadByIncrementId($relation->getData('inv_num'))){
				$this->log('Invoice found: '.$invoice->getId());
			}
			else{
				$invoice = null;
			}
		}
		
		if(!$invoice){
			return false;
		}
		
		$storeId = $order->getStoreId();
		$billingAddress = $order->getBillingAddress();
		$firstName = $billingAddress->getData('firstname');
		$lastName = $billingAddress->getData('lastname');
		
		$customerName = $firstName;
		$customerEmail = $order->getCustomerEmail();
		if($invoiceEmail = $billingAddress->getData('invoice_email')){
			$customerEmail = $invoiceEmail;
		}
		
		$mailTemplate = Mage::getModel('core/email_template');
		
		if($invoice instanceof Mage_Sales_Model_Order_Creditmemo || $this->_isCreditmemo){
			$emailParamKey = 'creditmemo';
			$pdfName = Mage::helper('sales')->__('Credit Memo').'-'.$invoiceNumber.'.pdf';
			if ($order->getCustomerIsGuest()) {
				$template = Mage::getStoreConfig(Mage_Sales_Model_Order_Creditmemo::XML_PATH_EMAIL_GUEST_TEMPLATE, $storeId);
				$customerName = $order->getBillingAddress()->getName();
			} else {
				$template = Mage::getStoreConfig(Mage_Sales_Model_Order_Creditmemo::XML_PATH_EMAIL_TEMPLATE, $storeId);
				$customerName = $order->getCustomerName();
			}
		}
		else{
			$emailParamKey = 'invoice';
			$pdfName = Mage::helper('sales')->__('Invoice').'-'.$invoiceNumber.'.pdf';
			if ($order->getCustomerIsGuest()) {
				$template = Mage::getStoreConfig(Mage_Sales_Model_Order_Invoice::XML_PATH_EMAIL_GUEST_TEMPLATE, $storeId);
				$customerName = $order->getBillingAddress()->getName();
			} else {
				$template = Mage::getStoreConfig(Mage_Sales_Model_Order_Invoice::XML_PATH_EMAIL_TEMPLATE, $storeId);
				$customerName = $order->getCustomerName();
			}
		}
		
		$copyTo = explode(',',Mage::getStoreConfig(Mage_Sales_Model_Order_Invoice::XML_PATH_EMAIL_COPY_TO, $storeId));			
		$copyMethod = Mage::getStoreConfig(Mage_Sales_Model_Order_Invoice::XML_PATH_EMAIL_COPY_METHOD, $storeId);
		
		$payment = $order->getPayment();
		$paymentBlock = Mage::helper('payment')->getInfoBlock($payment)->setIsSecureMode(true);
		$paymentBlock->getMethod()->setStore($storeId);

		
		$sendTo[] = array(
						'name'  => $customerName,
						'email' => $customerEmail
					);
		
		if ($copyTo && $copyMethod == 'bcc') {
		    foreach ($copyTo as $email) {
		    	if(trim($email)) {
		        	$mailTemplate->addBcc($email);
		        }
		    }
		}
		
		if ($copyTo && $copyMethod == 'copy') {
		    foreach ($copyTo as $email) {
		    	if(trim($email)) {
		            $sendTo[] = array(
		                'name'  => null,
		                'email' => $email
		            );
				}
		    }
		}
		
		$comment = '';
		
		foreach ($sendTo as $recipient) {
			$mailTemplate->getMail()->createAttachment($invoicePdf,'application/pdf',Zend_Mime::DISPOSITION_ATTACHMENT,Zend_Mime::ENCODING_BASE64, $pdfName);
			$mailTemplate->setDesignConfig(array('area'=>'frontend', 'store'=>$storeId))
				->sendTransactional(
					$template,
					Mage::getStoreConfig(Mage_Sales_Model_Order_Invoice::XML_PATH_EMAIL_IDENTITY, $storeId),
					$recipient['email'],
					$recipient['name'],
					array(
						'order'       => $order,
						$emailParamKey     => $invoice,
						'comment'     => $comment,
						'billing'     => $billingAddress,
						'payment_html'=> $paymentBlock->toHtml(),
					)
				);
		}
		$invoice->_getResource()->saveAttribute($invoice, 'email_sent');
		
		$order->addStatusHistoryComment(Mage::helper('economic')->__('Invoice PDF from e-conomic were successfully sent to customer'));
		$order->save();
		
		$this->log('Invoice PDF from e-conomic were successfully sent to customer');
		
		return true;
	}
	
	public function addSuccess($string) {
		return Mage::getSingleton('adminhtml/session')->addSuccess($string);
	}

	public function addWarning($string) {
		return Mage::getSingleton('adminhtml/session')->addWarning($string);
	}

	public function addError($string) {
		return Mage::getSingleton('adminhtml/session')->addError($string);
	}
}
