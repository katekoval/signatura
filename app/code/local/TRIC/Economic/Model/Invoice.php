<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Model_Invoice extends TRIC_Economic_Model_Abstract
{
	public function _construct()
	{
		//parent::_construct();
		$this->_init('economic/economic');
	}
	
	public function massProcessToEconomic($economicIds=array(),$asSingleInvoice=false){		
		foreach($economicIds as $economicId){
			$economic = Mage::getModel('economic/invoice')->load($economicId);
			$orderNumber = $economic->getData('ord_num');
			$invoiceNumber = $economic->getData('inv_num');
			$type = $economic->getData('type');
			if($type == 0){
				if($invoice = Mage::getModel('sales/order_invoice')->loadByIncrementId($invoiceNumber)){
					$this->processInvoiceToEconomic($invoice,$type,$force=true);
				}
			}
			elseif($type == 1){
				$creditMemo = Mage::getResourceModel('sales/order_creditmemo_collection')->addAttributeToFilter('increment_id', $invoiceNumber)->getAllIds();
				if($invoice = Mage::getModel('sales/order_creditmemo')->load($creditMemo[0])){
					$this->processInvoiceToEconomic($invoice,$type,$force=true);
				}
			}
			elseif($type == -1){
				$order = Mage::getModel('sales/order')->loadByIncrementId($orderNumber);
				Mage::getModel('economic/order')->processOrderToEconomic($order);
			}
		}
		
		return true;
	}

	public function processInvoiceToEconomic($invoice,$type,$force=false)
	{
		$this->log('processInvoiceToEconomic - id: '.$invoice->getIncrementId());
		$date = $invoice->getCreatedAt();
		
		if($this->getConfig('order_settings/skip_transfer_if_zero_total') && $invoice->getGrandTotal() == 0){
			$this->log('Skip transferring because grand total of invoice is 0!');
			$message = Mage::helper('economic')->__('%s was skipped transferring to e-conomic because grand total of invoice is 0.',$invoice->getIncrementId());
			$this->addWarning($message);
			return false;
		}
		
		$eInvoice = false;
		
		$collection = Mage::getModel('economic/invoice')->getCollection()
			->addFieldToFilter('inv_num', $invoice->getIncrementId())
			->addFieldToFilter('type', $type);
		
		if($collection->getSize()) {        
			$eInvoice = $collection->getLastItem();
		} else {
			$this->log('Not found in economic-table: '.$invoice->getIncrementId().' type: '.$type.' - then create it!');
		}
		
		if($eInvoice && $eInvoice->getData('economic_id')){
			if(!$force && $eInvoice->getData('status') == 1){
				return false;
			}
		}
		else{
			$customerName = trim($invoice->getOrder()->getBillingAddress()->getData('firstname'))." ".trim($invoice->getOrder()->getBillingAddress()->getData('lastname'));
			
			$eInvoice = Mage::getModel('economic/invoice')
				->setInvNum($invoice->getIncrementId())
				->setOrdNum($invoice->getOrder()->getIncrementId())
				->setBillTo($customerName)
				->setCreatedTime($date)
				->setType($type)
				->setStatus(0)
				->setEntityId($invoice->getEntityId())
				->save();
		}
		
		$isCreditmemo = ($type == 1) ? true : false;
		
		// Start store emulation process
		$storeId = $invoice->getStoreId();
		$appEmulation = Mage::getSingleton('core/app_emulation');
		$initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($storeId);
		
		
		//Connect to e-conomic with creditentials regarding the orders store-id
		$this->api = Mage::getSingleton('economic/api');
		if(!$this->api->connected || $this->api->storeId != $storeId){
			$this->api->connect();
		}
		
		if($this->createOrderInEconomic($invoice,$isCreditmemo)){
			$message = ($type==1) ? Mage::helper('sales')->__('Creditmemo') : Mage::helper('sales')->__('Invoice');
			$message .= ' '.Mage::helper('economic')->__('%s was transferred successfully to e-conomic',$invoice->getIncrementId());
			$this->addSuccess($message);
			
			// Stop store emulation process
			$appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
			
			$date = date("Y-m-d H:i:s");
			$eInvoice->setStatus(1)
				->setUpdateTime($date)
				->save();
			
			$this->log('DONE - id: '.$invoice->getIncrementId());
			return true;
		}
		else{
			$message = Mage::helper('economic')->__('%s encountered a problem when transferring to e-conomic',$invoice->getIncrementId());
			$this->addError($message);
			
			// Stop store emulation process
			$appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
			
			$date = date("Y-m-d H:i:s");
			$eInvoice->setStatus(99)
				->setUpdateTime($date)
				->save();
			
			$this->log('FAIL - id: '.$invoice->getIncrementId());
			return false;
		}
	}
}
