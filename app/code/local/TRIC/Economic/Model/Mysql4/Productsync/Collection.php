<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2017 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */
class TRIC_Economic_Model_Mysql4_Productsync_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
	public function _construct()
	{
		$this->_init('economic/productsync');
	}
	
	public function massUpdate(array $data)
	{
		if(!empty($this->getAllIds())){
			$this->getConnection()->update($this->getResource()->getMainTable(), $data, $this->getResource()->getIdFieldName() . ' IN(' . implode(',', $this->getAllIds()) . ')');
		}
	    return $this;
	}
}