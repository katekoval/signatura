<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2017 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Model_Mysql4_Productsync extends Mage_Core_Model_Mysql4_Abstract
{
	public function _construct()
	{
		$this->_init('economic/productsync', 'id');
	}
}