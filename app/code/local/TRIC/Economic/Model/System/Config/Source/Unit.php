<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Model_System_Config_Source_Unit
{

	private function __getOptions()
	{
		$api = Mage::getModel('economic/api');
		if(!$api->connected){
			$api->connect();
		}
		if(!$client = $api->getClient()){
			return false;
		}

		$entityHandles = new stdClass();
		$entityHandles->entityHandles = $client->Unit_GetAll()->Unit_GetAllResult;

		$result = $client->Unit_GetDataArray($entityHandles)->Unit_GetDataArrayResult;
		$units = isset($result->UnitData) ? $result->UnitData : false;

		if(is_array($units))
		{
			foreach($units as $unit)
			{
				$array[] = array('value' => $unit->Handle->Number, 'label'=>$unit->Handle->Number ." (". $unit->Name . ")");
			}
		}
		elseif(isset($units) && $units)
		{
			$array[] = array('value' => $units->Handle->Number, 'label'=>$units->Handle->Number ." (". $units->Name . ")");
		}
		else
		{
			$array[] = array('value' => '', 'label'=>Mage::helper('economic')->__('No units in e-conomic'));
		}

		return $array;
	}


	public function toOptionArray()
	{
		if($options = $this->__getOptions()){
			return $options;
		}
		else{
			$array = array(array('value' => '', 'label'=>'--- '.Mage::helper('economic')->__('No connection to e-conomic').' ---'));
			return $array;
		}
	}

}
