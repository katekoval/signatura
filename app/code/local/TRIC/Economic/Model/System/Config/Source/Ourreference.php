<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */
class TRIC_Economic_Model_System_Config_Source_Ourreference
{

	private function __getOptions()
	{
		$api = Mage::getSingleton('economic/api');
		if(!$api->connected){
			$api->connect();
		}
		if(!$client = $api->getClient()){
			return false;
		}

		$entityHandles = new stdClass();
		$entityHandles->entityHandles = $client->Employee_GetAll()->Employee_GetAllResult;
		
		if(!$client->Employee_GetDataArray($entityHandles)->Employee_GetDataArrayResult){
			$array[] = array('value' => "", 'label'=>"Ingen medarbejdere i e-conomic");
			return $array;
		}
		
		if(isset($client->Employee_GetDataArray($entityHandles)->Employee_GetDataArrayResult->EmployeeData)){
			$employees = $client->Employee_GetDataArray($entityHandles)->Employee_GetDataArrayResult->EmployeeData;
		}
		
		if(isset($employees) && is_array($employees))
		{
			$array[] = array('value' => "", 'label'=>Mage::helper('economic')->__('None'));
			foreach($employees as $employee)
			{
				$array[] = array('value' => $employee->Handle->Number, 'label'=>$employee->Handle->Number ." (". $employee->Name . ")");
			}
		}
		elseif(isset($employees))
		{
			$array[] = array('value' => "", 'label'=>Mage::helper('economic')->__('None'));
			$array[] = array('value' => $employees->Handle->Number, 'label'=>$employees->Handle->Number ." (". $employees->Name . ")");
		}
		else
		{
			$array[] = array('value' => "", 'label'=>Mage::helper('economic')->__('Ingen medarbejdere i e-conomic'));
		}

		return $array;
	}


	public function toOptionArray()
	{
		if($options = $this->__getOptions()){
			return $options;
		}
		else{
			$array = array(array('value' => '', 'label'=>'--- '.Mage::helper('economic')->__('No connection to e-conomic').' ---'));
			return $array;
		}
	}

}
