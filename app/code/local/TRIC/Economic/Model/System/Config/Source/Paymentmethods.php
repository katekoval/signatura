<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */
class TRIC_Economic_Model_System_Config_Source_Paymentmethods
{
	public function toOptionArray()
	{
		if($this->__getMagentoVersion() < 1410){
			$methods = array();
			$methods[] = array('value'=>'','label'=>Mage::helper('economic')->__('Available only from Magento version 1.4.1.0'));
		}
		else{
			$methods = Mage::helper('payment')->getPaymentMethodList(true, true, true);
			array_unshift($methods, array('value'=>'','label'=>''));
		}

		return $methods;
	}

	private function __getMagentoVersion()
	{
		$v = Mage::getVersion();
		$digits = @explode(".", $v);
		$version = 0;
		if (is_array($digits)) {
			foreach ($digits as $k => $v) {
				$version += ($v * pow(10, max(0, (3 - $k))));
			}
		}
		return $version;
	}
}
