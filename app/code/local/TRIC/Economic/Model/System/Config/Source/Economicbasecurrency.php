<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Model_System_Config_Source_Economicbasecurrency
{
	public function toOptionArray()
	{
		$return = array();
		
		foreach(Mage::getModel('directory/currency')->getConfigAllowCurrencies() as $currency)
		{
			$return[] = array('value' => $currency, 'label'=>$currency);
		}
		
		return $return;
	}
}