<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Model_System_Config_Source_Refplacement
{
	public function toOptionArray()
	{
		return array(
			array('value' => 'other_ref', 'label'=>Mage::helper('economic')->__('Other ref.')),
			array('value' => 'heading', 'label'=>Mage::helper('economic')->__('Heading')),
			array('value' => 'textline1', 'label'=>Mage::helper('economic')->__('Text Line 1')),
			array('value' => 'textline2', 'label'=>Mage::helper('economic')->__('Text Line 2'))
		);
	}

}
