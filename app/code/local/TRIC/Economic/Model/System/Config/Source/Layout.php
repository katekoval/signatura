<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */
class TRIC_Economic_Model_System_Config_Source_Layout
{
	private function __getOptions()
	{
		$api = Mage::getSingleton('economic/api');
		if(!$api->connected){
			$api->connect();
		}
		if(!$client = $api->getClient()){
			return false;
		}

		$entityHandles = new stdClass();
		$entityHandles->entityHandles = $client->TemplateCollection_GetAll()->TemplateCollection_GetAllResult;

		$TemplateCollections = $client->TemplateCollection_GetDataArray($entityHandles)->TemplateCollection_GetDataArrayResult->TemplateCollectionData;

		if(is_array($TemplateCollections))
		{
			foreach($TemplateCollections as $layout)
			{
				$array[] = array('value' => $layout->Handle->Id, 'label'=>$layout->Handle->Id ." (". $layout->Name . ")");
			}
		}
		elseif(isset($TemplateCollections))
		{
			$array[] = array('value' => $TemplateCollections->Handle->Id, 'label'=>$TemplateCollections->Handle->Id ." (". $TemplateCollections->Name . ")");
		}
		else
		{
			$array[] = array('value' => '', 'label'=>Mage::helper('economic')->__('No layouts in e-conomic'));
		}

		return $array;
	}

	public function toOptionArray()
	{
		if($options = $this->__getOptions()){
			return $options;
		}
		else{
			$array = array(array('value' => '', 'label'=>'--- '.Mage::helper('economic')->__('No connection to e-conomic').' ---'));
			return $array;
		}
	}
}
