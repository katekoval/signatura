<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Model_System_Config_Source_Transfertype
{
	public function toOptionArray()
	{
		return array(
			array('value' => 'invoice', 'label'=>Mage::helper('economic')->__('Invoices / Creditmemos')),
			array('value' => 'order', 'label'=>Mage::helper('economic')->__('Orders')),
		);
	}

}
