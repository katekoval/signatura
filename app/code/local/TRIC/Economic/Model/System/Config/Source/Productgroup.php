<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */
class TRIC_Economic_Model_System_Config_Source_Productgroup
{
	private function __getOptions()
	{
		$api = Mage::getSingleton('economic/api');
		if(!$api->connected){
			$api->connect();
		}
		if(!$client = $api->getClient()){
			return false;
		}

		$entityHandles = new stdClass();
		$entityHandles->entityHandles = $client->ProductGroup_GetAll()->ProductGroup_GetAllResult;

		$result = $client->ProductGroup_GetDataArray($entityHandles)->ProductGroup_GetDataArrayResult;
		$productGroups = isset($result->ProductGroupData) ? $result->ProductGroupData : false;

		if(is_array($productGroups))
		{
			foreach($productGroups as $productGroup)
			{
				$array[] = array('value' => $productGroup->Handle->Number, 'label'=>$productGroup->Handle->Number ." (". $productGroup->Name . ")");
			}
		}
		elseif(isset($productGroups) && $productGroups)
		{
			$array[] = array('value' => $productGroups->Handle->Number, 'label'=>$productGroups->Handle->Number ." (". $productGroups->Name . ")");
		}
		else
		{
			$array[] = array('value' => "", 'label'=>Mage::helper('economic')->__('No product groups in e-conomic'));
		}

		return $array;
	}


	public function toOptionArray()
	{
		if($options = $this->__getOptions()){
			return $options;
		}
		else{
			$array = array(array('value' => '', 'label'=>'--- '.Mage::helper('economic')->__('No connection to e-conomic').' ---'));
			return $array;
		}
	}

}
