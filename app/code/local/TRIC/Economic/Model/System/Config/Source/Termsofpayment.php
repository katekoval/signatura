<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */
class TRIC_Economic_Model_System_Config_Source_Termsofpayment
{

	private function __getOptions()
	{
		$api = Mage::getSingleton('economic/api');
		if(!$api->connected){
			$api->connect();
		}
		if(!$client = $api->getClient()){
			return false;
		}

		$entityHandles = new stdClass();
		$entityHandles->entityHandles = $client->TermOfPayment_GetAll()->TermOfPayment_GetAllResult;

		$terms = $client->TermOfPayment_GetDataArray($entityHandles)->TermOfPayment_GetDataArrayResult->TermOfPaymentData;
		if(is_array($terms))
		{
			foreach($terms as $term)
			{
				$array[] = array('value' => $term->Handle->Id, 'label'=>$term->Handle->Id ." (". $term->Name . ")");
			}
		}
		elseif(isset($terms))
		{
			$array[] = array('value' => $terms->Handle->Id, 'label'=>$terms->Handle->Id ." (". $terms->Name . ")");
		}
		else
		{
			$array[] = array('value' => "", 'label'=>Mage::helper('economic')->__('No layouts in e-conomic'));
		}

		return $array;
	}


	public function toOptionArray()
	{
		if($options = $this->__getOptions()){
			return $options;
		}
		else{
			$array = array(array('value' => '', 'label'=>'--- '.Mage::helper('economic')->__('No connection to e-conomic').' ---'));
			return $array;
		}
	}

}
