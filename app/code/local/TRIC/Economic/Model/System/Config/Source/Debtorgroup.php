<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */
class TRIC_Economic_Model_System_Config_Source_Debtorgroup
{
	private function __getOptions()
	{
		$api = Mage::getSingleton('economic/api');
		if(!$api->connected){
			$api->connect();
		}
		if(!$client = $api->getClient()){
			return false;
		}

		$entityHandles = new stdClass();
		$entityHandles->entityHandles = $client->DebtorGroup_GetAll()->DebtorGroup_GetAllResult;

		if(isset($client->DebtorGroup_GetDataArray($entityHandles)->DebtorGroup_GetDataArrayResult->DebtorGroupData)){
			$debtorGroups = $client->DebtorGroup_GetDataArray($entityHandles)->DebtorGroup_GetDataArrayResult->DebtorGroupData;
		}
		else{
			$debtorGroups = false;
		}

		if(is_array($debtorGroups))
		{
			foreach($debtorGroups as $debtorGroup)
			{
				$array[] = array('value' => $debtorGroup->Handle->Number, 'label'=>$debtorGroup->Handle->Number ." (". $debtorGroup->Name . ")");
			}
		}
		elseif(isset($debtorGroups) && $debtorGroups)
		{
			$array[] = array('value' => $debtorGroups->Handle->Number, 'label'=>$debtorGroups->Handle->Number ." (". $debtorGroups->Name . ")");
		}
		else
		{
			$array[] = array('value' => '', 'label'=>Mage::helper('economic')->__('No debtor groups in e-conomic'));
		}

		return $array;
	}

	public function toOptionArray()
	{
		if($options = $this->__getOptions()){
			return $options;
		}
		else{
			$array = array(array('value' => '', 'label'=>'--- '.Mage::helper('economic')->__('No connection to e-conomic').' ---'));
			return $array;
		}
	}
}
