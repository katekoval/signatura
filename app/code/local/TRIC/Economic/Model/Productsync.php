<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2017 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Model_Productsync extends TRIC_Economic_Model_Abstract
{
	public function _construct() {
		$this->_init('economic/productsync');
	}
	
	public function process() {
		
		// Prevent to run again if cronjob already running or started
		$cronScheduleCollection = Mage::getModel('cron/schedule')->getCollection()
									->addFieldToFilter('job_code','economic_productsync');
		
		$cronScheduleCollection->getSelect()->where(new Zend_Db_Expr("(status = 'running' OR (status = 'pending' AND executed_at IS NOT NULL AND finished_at IS NULL))"));
						
		if($cronScheduleCollection->getSize()){
			// Check when started, and only run if diff is below 5 sec (because then it is the real job we're looking at)
			$executedAt = $cronScheduleCollection->getFirstItem()->getData('executed_at');
			$now = date('Y-m-d H:i:s');
			if(strtotime($now) - strtotime($executedAt) > 5){
				$this->log('Product sync skipped because job is already running');
				return true;
			}
		}
		
		$syncCollection = $this->getCollection()->addFieldToFilter('status',0);
		$syncCollection->getSelect()->group('sku')->order('id');
		
		foreach($syncCollection as $item){
			$sku = $item->getSku();
			try{
				Mage::getSingleton('economic/product')->syncProductFromEconomic($sku);
				$item->setStatus(1)->setUpdateTime(date('Y-m-d H:i:s'))->save();
			}
			catch(Exception $e){
			}
			
		}
				
		return true;
	}
}
