<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Adminhtml_EconomicController extends Mage_Adminhtml_Controller_Action
{
	protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('economic/economic');
    }
	
	protected function _initAction(){
		$this->loadLayout()
		->_setActiveMenu('economic/items')
		->_addBreadcrumb(Mage::helper('adminhtml')->__('Items Manager'), Mage::helper('adminhtml')->__('Invoices And Credit Memos List'));
		return $this;
	}
	
	public function gridAction()
	{
	    $this->loadLayout();
	    $this->getResponse()->setBody(
	        $this->getLayout()->createBlock('economic/adminhtml_economic_grid')->toHtml()
	    );
	}

	public function indexAction(){
		$this->_initAction();
		$this->renderLayout();
	}

	public function newAction(){
		Mage::getModel('economic/economic')->sendQueueToEconomic();
		$this->_redirect('*/*/');
	}
	
	
	/** PRODUCT GRID **/
	public function addProductToEconomicAction()
	{
		$params = $this->getRequest()->getParams();
		
		if(isset($params['product']))
		{
			$numberOfProductsAdded = 0;
			$product_ids = $params['product'];
			foreach($product_ids as $product_id){
				$product = Mage::getModel('catalog/product')->load($product_id);
				if(Mage::getModel('economic/product')->syncProductToEconomic($product)){
					$numberOfProductsAdded++;
				}
			}
			Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('economic')->__('%s products were added to e-conomic',$numberOfProductsAdded));
			$this->_redirect('adminhtml/catalog_product/index/',array());
		}
		else
		{
			$product = Mage::getModel('catalog/product')->load($this->getRequest()->getParam('id'));
			Mage::getModel('economic/product')->syncProductToEconomic($product);
			$this->_redirect('adminhtml/catalog_product/view/',array('id' => $this->getRequest()->getParam('id')));
		}
	}
	
	public function updateProductFromEconomicAction()
	{
		$params = $this->getRequest()->getParams();
		
		if(isset($params['product']))
		{
			$numberOfProductsUpdated = 0;
			$product_ids = $params['product'];
			
			if(Mage::helper('economic')->getConfig('product_settings/product_sync_queue')){
				
				$productsCollection = Mage::getModel('catalog/product')->getCollection()
                    ->addAttributeToFilter('entity_id', array('in' => $product_ids));
				
				foreach($productsCollection as $product) {
				    Mage::getModel('economic/productsync')->setSku($product->getSku())->setUpdateTime(date('Y-m-d H:i:s'))->save();
				    $numberOfProductsUpdated++;
				}
				
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('economic')->__('%s products are scheduled to be updated from e-conomic',$numberOfProductsUpdated));
			}
			else{
				foreach($product_ids as $product_id){
					if(Mage::getModel('economic/product')->syncProductFromEconomic($product_id,'id')){
						$numberOfProductsUpdated++;
					}
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('economic')->__('%s products were updated from e-conomic',$numberOfProductsUpdated));
			}
			
			$this->_redirect('adminhtml/catalog_product/index/',array());
		}
		else
		{
			$product_id = $this->getRequest()->getParam('id');
			Mage::getModel('economic/product')->syncProductFromEconomic($product_id,'id');
			$this->_redirect('adminhtml/catalog_product/view/',array('id' => $this->getRequest()->getParam('id')));
		}
	}

	
	/** ORDER GRID **/
	public function addOrderToEconomicAction()
	{
		$params = $this->getRequest()->getParams();
		$forceNewOrder = true;

		if(isset($params['order_ids']))
		{
			$order_ids = $params['order_ids'];
			foreach($order_ids as $order_id){
				$order = Mage::getModel('sales/order')->load($order_id);
				Mage::getSingleton('economic/order')->processOrderToEconomic($order,-1,$forceNewOrder);
			}
			Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('economic')->__('Orders were sent to e-conomic'));
			$this->_redirect('adminhtml/sales_order/index/',array());
		}
		else
		{
			$order = Mage::getModel('sales/order')->load($this->getRequest()->getParam('id'));
			Mage::getSingleton('economic/order')->processOrderToEconomic($order,-1,$forceNewOrder);
			Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('economic')->__('Orders were sent to e-conomic'));
			$this->_redirect('adminhtml/sales_order/view/',array('order_id' => $this->getRequest()->getParam('id')));
		}
		
		
	}
	
	
	/** INVOICE GRID **/
	public function addInvoiceToEconomicAction()
	{
		$params = $this->getRequest()->getParams();

		if(isset($params['invoice_ids']))
		{
			$invoice_ids = $params['invoice_ids'];
			foreach($invoice_ids as $invoice_id){
				$invoice = Mage::getModel('sales/order_invoice')->load($invoice_id);
				Mage::getSingleton('economic/invoice')->processInvoiceToEconomic($invoice,0,$rerun=true);
			}
			$this->_redirect('adminhtml/sales_invoice/index/',array());
		}
		else
		{
			$invoice = Mage::getModel('sales/order_invoice')->load($this->getRequest()->getParam('id'));
			Mage::getSingleton('economic/invoice')->processInvoiceToEconomic($invoice,0);
			$this->_redirect('adminhtml/sales_invoice/view/',array('invoice_id' => $this->getRequest()->getParam('id')));
		}
	}
	
	
	/** CREDITMEMO GRID **/
	public function addCreditmemoToEconomicAction()
	{
		$params = $this->getRequest()->getParams();

		if(isset($params['creditmemo_ids']))
		{
			$creditmemo_ids = $params['creditmemo_ids'];
			foreach($creditmemo_ids as $creditmemo_id){
				$creditmemo = Mage::getModel('sales/order_creditmemo')->load($creditmemo_id);
				Mage::getSingleton('economic/invoice')->processInvoiceToEconomic($creditmemo,1,$rerun=true);
			}
			$this->_redirect('adminhtml/sales_creditmemo/index/',array());
		}
		else
		{
			$creditmemo = Mage::getModel('sales/order_creditmemo')->load($this->getRequest()->getParam('id'));
			Mage::getSingleton('economic/invoice')->processInvoiceToEconomic($creditmemo,1);
			$this->_redirect('adminhtml/sales_creditmemo/view/',array('creditmemo_id' => $this->getRequest()->getParam('id')));
		}
	}
	
	
	/** ECONOMIC GRID **/
	public function massTransferAction()
	{
		$economicIds = $this->getRequest()->getParam('economic_id');

		if(!is_array($economicIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('economic')->__('Please select one or more items.'));
		} else {
			try {
				$asSingleInvoice = false;
				Mage::getSingleton('economic/invoice')->massProcessToEconomic($economicIds,$asSingleInvoice);
				
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}
	
	public function massTransferAsSingleAction()
	{
		$economicIds = $this->getRequest()->getParam('economic_id');

		if(!is_array($economicIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('economic')->__('Please select one or more items.'));
		} else {
			try {
				$asSingleInvoice = true;
				Mage::getModel('economic/invoice')->massProcessToEconomic($economicIds,$asSingleInvoice);
				
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}

	public function massDeleteAction()
	{
		$economicIds = $this->getRequest()->getParam('economic_id');

		if(!is_array($economicIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('economic')->__('Please select one or more items.'));
		} else {
			try {
				$economicModel = Mage::getModel('economic/economic');
				foreach ($economicIds as $economicId) {
					$economicModel->load($economicId)->delete();
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(
					Mage::helper('economic')->__('Total of %d record(s) were deleted.', count($economicIds))
				);
			} catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
		}
		$this->_redirect('*/*/index');
	}
	
	
	public function testconnectionAction()
	{
		$type = $this->getRequest()->getParam('type');
		$token = $this->getRequest()->getParam('token');
		$agreementnumber = $this->getRequest()->getParam('agreementnumber');
		$userid = $this->getRequest()->getParam('userid');
		$password = $this->getRequest()->getParam('password');
		
		// Login as administrator
		$adminAgreementnumber = $this->getRequest()->getParam('admin_agreementnumber');
		$adminUserid = $this->getRequest()->getParam('admin_userid');
		$adminPassword = $this->getRequest()->getParam('admin_password');
		$adminClientAgreementNumber = $this->getRequest()->getParam('admin_client_agreementnumber');
		
		$api = Mage::getSingleton('economic/api');
		
		if($type == 'token' && $token){
			if($client = $api->testConnection($token,false,false,false)){
				$this->getResponse()->setBody('true');
			}
			else{
				$this->getResponse()->setBody('false');
			}
		}
		elseif($type == 'administrator' && $adminAgreementnumber && $adminUserid && $adminPassword && $adminClientAgreementNumber){
			if($client = $api->testConnection(false,false,false,false,$adminAgreementnumber,$adminUserid,$adminPassword,$adminClientAgreementNumber)){
				$this->getResponse()->setBody('true');
			}
			else{
				$this->getResponse()->setBody('false');
			}
		}
		elseif($agreementnumber && $userid && $password){
			if($client = $api->testConnection(false,$agreementnumber,$userid,$password)){
				$this->getResponse()->setBody('true');
			}
			else{
				$this->getResponse()->setBody('false');
			}
		}
		else{
			$this->getResponse()->setBody('false');
		}
	}
	
	public function tokenAction()
	{
		$token = $this->getRequest()->getParam('token');

		if(isset($token) && $token){
			$api = Mage::getSingleton('economic/api');
			if($client = $api->testConnection($token,false,false,false)){
				if($store = $this->getRequest()->getParam('store')){
					$storeCollection = Mage::getModel('core/store')->getCollection()->addFieldToFilter('code', $store);        
					$storeId = $storeCollection->getFirstItem()->getStoreId();
					Mage::getConfig()->saveConfig('economic/general/api_token', $token, 'stores', $storeId);
				}
				elseif($website = $this->getRequest()->getParam('website')){
					$websiteCollection = Mage::getModel('core/website')->getCollection()->addFieldToFilter('code', $website);
					$websiteId = $websiteCollection->getFirstItem()->getWebsiteId();
					Mage::getConfig()->saveConfig('economic/general/api_token', $token, 'websites', $websiteId);
				}
				else{
					Mage::getConfig()->saveConfig('economic/general/api_token', $token);
				}
				
				Mage::getConfig()->reinit();
				Mage::app()->reinitStores();
				
				$scope = '';
				if($website){
					$scope .= 'website/'.$website.'/';
				}
				if($store){
					$scope .= 'store/'.$store.'/';
				}

				header('Location: '.Mage::helper("adminhtml")->getUrl('adminhtml/system_config/edit', array('section' => 'economic')).$scope.'key/'.$this->getRequest()->getParam('magekey'));

			}else{
				$this->getResponse()->setBody(Mage::helper('economic')->__('Error in token from e-conomic!'));
			}
		}

	}

}