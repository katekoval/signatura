<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Helper_Data extends Mage_Core_Helper_Abstract
{
	protected $_magentoModules = false;
	
	public function getConfig($path,$storeId=0){
		$value = null;
		if($storeId){
			$value = Mage::getStoreConfig(sprintf('economic/%s', $path),$storeId);
		}
		else{
			$value = Mage::getStoreConfig(sprintf('economic/%s', $path));
		}
		if(strpos($path, 'prefix') === false){
			$value = trim($value);
		}
		return $value;
	}

	public function getLicenseKey()
	{
		return Mage::getStoreConfig('economic/general/licensekey');
	}


	public function checkEan($ean){
		$origEan = $ean;
		if(is_numeric($ean) && (strlen($ean) == (int)13))
		{
			$ean=str_split($ean);

			$pair = 0;
			$impair = 0;
			for($i=0;$i<count($ean)- 1;$i++){
				if($i%2==0) $impair+=$ean[$i];
				else $pair +=$ean[$i];
			}
			$pair *= 3;
			$checksum = $pair + $impair;
			$dcontrol = 10 - ($checksum % 10);
			if($dcontrol == 10) $dcontrol = 0;
			if($dcontrol == $ean[12]){
				return true;
			}
		}

		return false;
	}

	public function checkEconomic($invoice,$type)
	{
		$resource = Mage::getSingleton('core/resource');
		$read = $resource->getConnection('core_read');

		$query = "SELECT status FROM " . $resource->getTableName('economic') . " WHERE inv_num = '".$invoice->getIncrementId()."' AND type = $type";
		$results = $read->fetchAll($query);
		$inQueue = false;
		$inEconomic = false;
		foreach($results as $row)
		{
			if($row['status'] == 0)
			{
				$inQueue = true;
			}
			$inEconomic = true;
		}
		if($inEconomic)
		{
			if($inQueue)
			{
				return 2;
			}
			return 1;
		}
		return 0;
	}

	public function addInvoiceToEconomicButton()
	{
		$url = Mage::helper("adminhtml")->getUrl("adminhtml/adminhtml_economic/addinvoicetoeconomic/", array('id'=>Mage::registry('current_invoice')->getId()));
		$allow = $this->checkEconomic(Mage::registry('current_invoice'),0);
		if($allow == 0)
		{
			$onClick = "setLocation('$url')";
		}
		elseif($allow == 1)
		{
			$onClick = "confirmSetLocation('Faktura er allerede sendt til e-conomic, vil du sende den igen?','$url')";
		}
		else
		{
			$onClick = "alert('Faktura er allerede i e-conomic kø')";
		}

		return array(
			'label'  => "Overfør til e-conomic",
			'onclick'   => $onClick,
		);
	}

	public function addCreditmemoToEconomicButton()
	{

		$url = Mage::helper("adminhtml")->getUrl("adminhtml/adminhtml_economic/addcreditmemotoeconomic/", array('id'=>Mage::registry('current_creditmemo')->getId()));
		$allow = $this->checkEconomic(Mage::registry('current_creditmemo'),1);
		if($allow == 0)
		{
			$onClick = "setLocation('$url')";
		}
		elseif($allow == 1)
		{
			$onClick = "confirmSetLocation('Kreditnota er allerede sendt til e-conomic, vil du sende den igen?','$url')";
		}
		else
		{
			$onClick = "alert('Kreditnota er allerede i e-conomic kø')";
		}

		return array(
			'label'  => "Overfør til e-conomic",
			'onclick'   => $onClick,
		);
	}

	public function isModuleActive($module) {
		if (!$this->_magentoModules) {
			$this->_magentoModules = Mage::getConfig()->getNode('modules')->children();
		}
		$modulesArray = (array)$this->_magentoModules;

		if(isset($modulesArray[$module])){
			if ($modulesArray[$module]->is('active')) {
				return true;
			} else {
				return false;
			}
		}
	}

}