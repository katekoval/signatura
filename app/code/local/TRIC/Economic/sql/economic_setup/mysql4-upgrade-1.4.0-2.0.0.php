<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

$installer = $this;
$installer->startSetup();

Mage::getConfig()->cleanCache();


$serienr = Mage::getStoreConfig('economic/settings/serienr',0);
if(isset($serienr))
{
	Mage::getModel('core/config')->saveConfig('economic/general/serienr',$serienr);
}

$aftalenr = Mage::getStoreConfig('economic/settings/aftalenr',0);
if(isset($aftalenr))
{
	Mage::getModel('core/config')->saveConfig('economic/general/aftalenr',$aftalenr);
}

$brugernavn = Mage::getStoreConfig('economic/settings/brugernavn',0);
if(isset($brugernavn))
{
	Mage::getModel('core/config')->saveConfig('economic/general/brugernavn',$brugernavn);
}

$adgangskode = Mage::getStoreConfig('economic/settings/adgangskode',0);
if(isset($adgangskode))
{
	Mage::getModel('core/config')->saveConfig('economic/general/adgangskode',$adgangskode);
}

Mage::getConfig()->cleanCache();
$installer->endSetup();
