<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

$installer = $this;

$installer->startSetup();

$installer->run("

  -- DROP TABLE IF EXISTS {$this->getTable('economic')};
  CREATE TABLE IF NOT EXISTS {$this->getTable('economic')} (
    `economic_id` int(11) unsigned NOT NULL auto_increment,
	`entity_id` int(11) unsigned NOT NULL default '0',
    `inv_num` varchar(255) NOT NULL default '',
    `ord_num` varchar(255) NOT NULL default '',
    `bill_to` text NOT NULL default '',
    `status` smallint(6) NOT NULL default '0',
	`type` smallint(6) NOT NULL default '0',
    `created_time` datetime NULL,
    `update_time` datetime NULL,
    PRIMARY KEY (`economic_id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	ALTER TABLE {$this->getTable('economic')} ADD UNIQUE `UNIQUE` ( `entity_id` , `inv_num` , `ord_num` );
      ");

$installer->endSetup();