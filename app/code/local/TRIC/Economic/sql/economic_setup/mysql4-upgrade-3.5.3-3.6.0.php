<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

$installer = $this;
$installer->startSetup();

$economicCustomerAttributeCode = 'economic_customer_id';

if(!$installer->getConnection()->fetchOne("SELECT * FROM {$this->getTable('eav_attribute')} WHERE `attribute_code`='$economicCustomerAttributeCode'"))
{
	$setup = Mage::getModel('customer/entity_setup', 'core_setup');
	$entityTypeId = $setup->getEntityTypeId('customer');
	
	$setup->addAttribute('customer', $economicCustomerAttributeCode, array(
	    'type' => 'int',
	    'input' => 'text',
	    'label' => 'E-conomic Customer ID',
	    'global' => 1,
	    'visible' => 1,
	    'required' => 0,
	    'user_defined' => 0,
	    'default' => NULL,
	    'visible_on_front' => 0,
	));
	
	Mage::getSingleton('eav/config')
		->getAttribute('customer', $economicCustomerAttributeCode)
		->setSortOrder(200)
		->setData('used_in_forms', array('adminhtml_customer'))
		->save();
		
	if($attributeId = $setup->getAttributeId('customer', $economicCustomerAttributeCode)){
		$query = "SELECT * FROM `{$installer->getTable('economic_customer')}`
		        WHERE magento_customer_id IS NOT NULL AND magento_customer_id != ''
		        AND economic_customer_id IS NOT NULL AND economic_customer_id != ''";
		$economicCustomerRelations = $installer->getConnection()->fetchAssoc($query);
			
		foreach($economicCustomerRelations as $economicCustomerRelation){
			$magentoCustomerId = $economicCustomerRelation['magento_customer_id'];
			$economicCustomerId = $economicCustomerRelation['economic_customer_id'];
			
			$installer->run("
				INSERT INTO `{$installer->getTable('customer_entity_int')}`
				(`entity_type_id`, `attribute_id`, `entity_id`, `value`)
				VALUES ($entityTypeId,$attributeId,$magentoCustomerId,$economicCustomerId)
				ON DUPLICATE KEY UPDATE value=$economicCustomerId
			");
		}
	}
}

$installer->endSetup();

Mage::getConfig()->cleanCache();