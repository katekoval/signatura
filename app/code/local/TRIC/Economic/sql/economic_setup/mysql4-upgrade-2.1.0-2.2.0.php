<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

$installer = $this;
$installer->startSetup();

$installer->run("UPDATE {$this->getTable('core_config_data')} SET path = 'economic/debitor/debitorgruppe_id' 		WHERE path = 'economic/settings/debitorgruppe_id'");
$installer->run("UPDATE {$this->getTable('core_config_data')} SET path = 'economic/debitor/debitorgruppe_id_udland' WHERE path = 'economic/settings/debitorgruppe_id_udland'");
$installer->run("UPDATE {$this->getTable('core_config_data')} SET path = 'economic/debitor/debitorgruppe_id_land' 	WHERE path = 'economic/settings/debitorgruppe_id_land'");

Mage::getConfig()->cleanCache();

$installer->endSetup();
