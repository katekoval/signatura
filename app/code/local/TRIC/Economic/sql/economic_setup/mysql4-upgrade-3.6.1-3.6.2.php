<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2017 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

$installer = $this;
$installer->startSetup();

$installer->run("
  CREATE TABLE IF NOT EXISTS {$this->getTable('economic_productsync')} (
    `id` int(11) unsigned NOT NULL auto_increment,
    `sku` varchar(255) NOT NULL,
    `update_time` datetime NULL,
    `status` tinyint(1) NOT NULL DEFAULT '0',
    PRIMARY KEY (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
      ");

$installer->endSetup();
