<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Block_System_Config_FieldMappingPayment extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    protected $_paymentTypeRenderer;
    protected $_economicTermOfPaymentRenderer;

    protected function _getPaymentTypeRenderer()
    {
    	if (!$this->_paymentTypeRenderer) {
            $this->_paymentTypeRenderer = $this->getLayout()->createBlock(
                'economic/magentoPaymentTypes', '',
                array('is_render_to_js_template' => true)
            );
            $this->_paymentTypeRenderer->setClass('payment_type_select');
            $this->_paymentTypeRenderer->setExtraParams('style="width:120px"');
        }
        return $this->_paymentTypeRenderer;
    }

    protected function _getEconomicTermOfPaymentRenderer()
    {
        if (!$this->_economicTermOfPaymentRenderer) {
            $this->_economicTermOfPaymentRenderer = $this->getLayout()->createBlock(
                'economic/economicTermOfPayment', '',
                array('is_render_to_js_template' => true)
            );
            $this->_economicTermOfPaymentRenderer->setClass('economic_term_of_payment_select');
            $this->_economicTermOfPaymentRenderer->setExtraParams('style="width:120px"');
        }
        return $this->_economicTermOfPaymentRenderer;
    }
    
    protected function _prepareToRender()
    {
        $this->addColumn('payment_type_id', array(
            'label' => Mage::helper('economic')->__('Payment type'),
            'renderer' => $this->_getPaymentTypeRenderer(),
        ));
        $this->addColumn('economic_term_of_payment_id', array(
            'label' => Mage::helper('economic')->__('Term of payment'),
            'renderer' => $this->_getEconomicTermOfPaymentRenderer(),
        ));
        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('economic')->__('Add Mapping');
    }
    
    protected function _prepareArrayRow(Varien_Object $row)
    {
        $row->setData(
            'option_extra_attr_' . $this->_getPaymentTypeRenderer()->calcOptionHash($row->getData('payment_type_id')),
            'selected="selected"'
        );
        $row->setData(
            'option_extra_attr_' . $this->_getEconomicTermOfPaymentRenderer()->calcOptionHash($row->getData('economic_term_of_payment_id')),
            'selected="selected"'
        );
    }
}
