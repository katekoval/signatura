<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Block_System_Config_MagentoShippingMethods extends Mage_Core_Block_Html_Select
{
    private $_shippingMethods;

    private function _getShippingMethods(){
    	
    	$shippingMethods = Mage::getSingleton('shipping/config')->getAllCarriers();
    	if(is_null($this->_shippingMethods)){
			foreach ($shippingMethods as $code => $method){
				$label = ($method->getConfigData('title') ? $method->getConfigData('title') : $method->getConfigData('name'));
				if($label){
					$this->_shippingMethods[$code] = $label;
				}
				if($code == 'postdk' && class_exists('TRIC_Pacsoft_Model_Mysql4_Rate_Collection')){
					if($postdkRates = new TRIC_Pacsoft_Model_Mysql4_Rate_Collection()){
						$postdkRates->load();
						foreach($postdkRates as $rate){
							if(!$rate->getActive()){
								continue;
							}
							$this->_shippingMethods[$code.'_'.$code.'_'.$rate->getPk()] = ' -- '.$rate->getData('title').' ('.$rate->getTitle().')';
						}
					}
				}
				elseif($code == 'gls' && class_exists('TRIC_GLS_Model_Mysql4_Rate_Collection')){
					if($glsRates = new TRIC_GLS_Model_Mysql4_Rate_Collection()){
						$glsRates->load();
						foreach($glsRates as $rate){
							if(!$rate->getActive()){
								continue;
							}
							$this->_shippingMethods[$code.'_'.'bestway'.'_'.$rate->getPk()] = ' -- '.$rate->getData('method_name').' ('.$rate->getTitle().')';
						}
					}
				}
				elseif($code == 'matrixrate' && class_exists('Webshopapps_Matrixrate_Model_Mysql4_Carrier_Matrixrate_Collection')){
					if($matrixRates = new Webshopapps_Matrixrate_Model_Mysql4_Carrier_Matrixrate_Collection()){
						$matrixRates->load();
						foreach($matrixRates as $rate){
							$this->_shippingMethods[$code.'_'.$code.'_'.$rate->getPk()] = ' -- '.$rate->getDeliveryType();
						}
					}
				}
			}
    	}

    	return $this->_shippingMethods;
    }
    
    public function setInputName($value)
    {
        return $this->setName($value);
    }
	
    public function _toHtml()
    {
    	$this->_getShippingMethods();
        
    	if (!$this->getOptions()) {
    		foreach ($this->_getShippingMethods() as $key => $label) {
                $this->addOption($key, $label);
            }
        }
        
        return parent::_toHtml();
    }
}
