<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Block_System_Config_WebhookurlProduct extends Mage_Adminhtml_Block_System_Config_Form_Field
{
	protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
    	$html = '';
    	
    	$agreementnumber = Mage::helper('economic')->getConfig('general/agreementnumber',0);
    	$userid = Mage::helper('economic')->getConfig('general/userid',0);
    	$api_token = Mage::helper('economic')->getConfig('general/api_token',0);
    	
    	if((!$agreementnumber || !$userid) && !$api_token){
	    	$webhookUrl = Mage::helper('economic')->__('Please connect to e-conomic first...');
    	}
    	else{
	    	$key = md5($agreementnumber.$userid.$api_token);
	    	$webhookUrl = $this->getBaseUrl()."economic/product/update/key/$key/new/[NEWNUMBER]/old/[OLDNUMBER]/number/[NUMBER]/";
    	}

    	$html .= '<input id="economic_product_settings_economic_webhook_url_product" value="'.$webhookUrl.'" class="input-text" readonly />';
    	return $html;
	}
}
