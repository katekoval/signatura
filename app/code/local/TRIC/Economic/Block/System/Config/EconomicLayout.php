<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Block_System_Config_EconomicLayout extends Mage_Core_Block_Html_Select
{
	private $_economicLayout;
	
	private function _getEconomicLayout()
	{
		$api = Mage::getSingleton('economic/api');
		
		$array = array();
		if(!$api->connected){
			$api->connect();
		}
		if(!$client = $api->getClient()){
			return $array;
		}

		$entityHandles = new stdClass();
		$entityHandles->entityHandles = $client->TemplateCollection_GetAll()->TemplateCollection_GetAllResult;

		$layouts = $client->TemplateCollection_GetDataArray($entityHandles)->TemplateCollection_GetDataArrayResult->TemplateCollectionData;
		if(is_array($layouts))
		{
			foreach($layouts as $layout)
			{
				$this->_economicLayout[$layout->Handle->Id] = htmlspecialchars($layout->Name, ENT_NOQUOTES | ENT_QUOTES);
			}
		}
		elseif(isset($layouts))
		{
			$this->_economicLayout[$layouts->Handle->Id] = htmlspecialchars($layouts->Name, ENT_NOQUOTES | ENT_QUOTES);
		}
		else
		{
			
		}

		return $this->_economicLayout;
	}    

    public function setInputName($value)
    {
        return $this->setName($value);
    }
	
    public function _toHtml()
    {
    	$this->_getEconomicLayout();
        
    	if (!$this->getOptions()) {
    		foreach ($this->_getEconomicLayout() as $key => $label) {
                $this->addOption($key, $label);
            }
        }
        
        return parent::_toHtml();
    }
    
}