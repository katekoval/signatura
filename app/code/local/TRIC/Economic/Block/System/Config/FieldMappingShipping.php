<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Block_System_Config_FieldMappingShipping extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    protected $_shippingMethodRenderer;
    protected $_economicTermOfPaymentRenderer;

    protected function _getShippingMethodRenderer()
    {
    	if (!$this->_shippingMethodRenderer) {
            $this->_shippingMethodRenderer = $this->getLayout()->createBlock(
                'economic/system_config_magentoShippingMethods', '',
                array('is_render_to_js_template' => true)
            );
            $this->_shippingMethodRenderer->setClass('shipping_method_select');
            $this->_shippingMethodRenderer->setExtraParams('style="width:120px"');
        }
        return $this->_shippingMethodRenderer;
    }
    
    protected function _prepareToRender()
    {
        $this->addColumn('shipping_method_id', array(
            'label' => Mage::helper('economic')->__('Shipping method'),
            'renderer' => $this->_getShippingMethodRenderer(),
        ));
        $this->addColumn('shipping_product_number', array(
            'label' => Mage::helper('economic')->__('Shipping product number in e-conomic')
        ));
        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('economic')->__('Add');
    }
    
    protected function _prepareArrayRow(Varien_Object $row)
    {
        $row->setData(
            'option_extra_attr_' . $this->_getShippingMethodRenderer()->calcOptionHash($row->getData('shipping_method_id')),
            'selected="selected"'
        );
    }
}
