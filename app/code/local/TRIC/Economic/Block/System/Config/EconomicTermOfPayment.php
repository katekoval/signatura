<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Block_System_Config_EconomicTermOfPayment extends Mage_Core_Block_Html_Select
{
	private $_economicTermOfPayment;
	
	private function _getEconomicTermOfPayment()
	{
		$api = Mage::getSingleton('economic/api');
		
		$array = array();
		if(!$api->connected){
			$api->connect();
		}
		if(!$client = $api->getClient()){
			return $array;
		}

		$entityHandles = new stdClass();
		$entityHandles->entityHandles = $client->TermOfPayment_GetAll()->TermOfPayment_GetAllResult;

		$terms = $client->TermOfPayment_GetDataArray($entityHandles)->TermOfPayment_GetDataArrayResult->TermOfPaymentData;
		if(is_array($terms))
		{
			foreach($terms as $term)
			{
				$this->_economicTermOfPayment[$term->Handle->Id] = htmlspecialchars($term->Name, ENT_NOQUOTES | ENT_QUOTES);
			}
		}
		elseif(isset($terms))
		{
			$this->_economicTermOfPayment[$terms->Handle->Id] = htmlspecialchars($terms->Name, ENT_NOQUOTES | ENT_QUOTES);
		}
		else
		{
			
		}

		return $this->_economicTermOfPayment;
	}    

    public function setInputName($value)
    {
        return $this->setName($value);
    }
	
    public function _toHtml()
    {
    	$this->_getEconomicTermOfPayment();
        
    	if (!$this->getOptions()) {
    		foreach ($this->_getEconomicTermOfPayment() as $key => $label) {
                $this->addOption($key, $label);
            }
        }
        
        return parent::_toHtml();
    }
    
}