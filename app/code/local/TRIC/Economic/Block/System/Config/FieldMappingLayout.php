<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Economic_Block_System_Config_FieldMappingLayout extends Mage_Adminhtml_Block_System_Config_Form_Field_Array_Abstract
{
    protected $_paymentTypeRenderer;
    protected $_economicLayoutRenderer;

    protected function _getPaymentTypeRenderer()
    {
    	if (!$this->_paymentTypeRenderer) {
            $this->_paymentTypeRenderer = $this->getLayout()->createBlock(
                'economic/system_config_magentoPaymentTypes', '',
                array('is_render_to_js_template' => true)
            );
            $this->_paymentTypeRenderer->setClass('payment_type_select');
            $this->_paymentTypeRenderer->setExtraParams('style="width:120px"');
        }
        return $this->_paymentTypeRenderer;
    }

    protected function _getEconomicLayoutRenderer()
    {
        if (!$this->_economicLayoutRenderer) {
            $this->_economicLayoutRenderer = $this->getLayout()->createBlock(
                'economic/system_config_economicLayout', '',
                array('is_render_to_js_template' => true)
            );
            $this->_economicLayoutRenderer->setClass('layout_select');
            $this->_economicLayoutRenderer->setExtraParams('style="width:120px"');
        }
        return $this->_economicLayoutRenderer;
    }
    
    protected function _prepareToRender()
    {
        $this->addColumn('payment_type_id', array(
            'label' => Mage::helper('economic')->__('Payment type'),
            'renderer' => $this->_getPaymentTypeRenderer(),
        ));
        $this->addColumn('layout_id', array(
            'label' => Mage::helper('economic')->__('Betalingsbetingelse'),
            'renderer' => $this->_getEconomicLayoutRenderer(),
        ));
        $this->_addAfter = false;
        $this->_addButtonLabel = Mage::helper('economic')->__('Add');
    }
    
    protected function _prepareArrayRow(Varien_Object $row)
    {
        $row->setData(
            'option_extra_attr_' . $this->_getPaymentTypeRenderer()->calcOptionHash($row->getData('payment_type_id')),
            'selected="selected"'
        );
        $row->setData(
            'option_extra_attr_' . $this->_getEconomicLayoutRenderer()->calcOptionHash($row->getData('layout_id')),
            'selected="selected"'
        );
    }
}
