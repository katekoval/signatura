<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */
class TRIC_Economic_Block_Adminhtml_Economic_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('economicGrid');
		$this->setDefaultSort('created_time');
		$this->setDefaultDir('DESC');
		//$this->setDefaultFilter(array('status'=>0));
		$this->setSaveParametersInSession(true);
		$this->setUseAjax(true);
	}
	
	protected function _prepareMassaction()
	{
		if(!Mage::helper('economic')->getConfig('general/active')){
			return false;
		}
		
		$this->setMassactionIdField('economic_id');
		$this->getMassactionBlock()->setFormFieldName('economic_id');
		
		$this->getMassactionBlock()->addItem('transfer', array(
			'label'=> Mage::helper('economic')->__('Transfer to e-conomic'),
			'url'  => $this->getUrl('*/*/massTransfer', array('' => '')),
			'confirm' => Mage::helper('economic')->__('Are you sure?')
		));
		
		//$this->getMassactionBlock()->addItem('transfer_as_single', array(
		//	'label'=> Mage::helper('economic')->__('Overfør til e-conomic som én faktura'),
		//	'url'  => $this->getUrl('*/*/massTransferAsSingle', array('' => '')),
		//	'confirm' => Mage::helper('economic')->__('Er du sikker?')
		//));
		
		$this->getMassactionBlock()->addItem('delete', array(
			'label'=> Mage::helper('economic')->__('Delete'),
			'url'  => $this->getUrl('*/*/massDelete', array('' => '')),
			'confirm' => Mage::helper('economic')->__('Are you sure?')
		));
		return $this;
	}

	protected function _prepareCollection()
	{		
		$collection = Mage::getModel('economic/economic')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}

	protected function _prepareColumns()
	{

		$this->addColumn('inv_num', array(
				'header'    => Mage::helper('economic')->__('Invoice / Creditmemo no.'),
				'align'     =>'left',
				'index'     => 'inv_num',
			));

		$this->addColumn('ord_num', array(
				'header'    => Mage::helper('economic')->__('Order no.'),
				'align'     =>'left',
				'index'     => 'ord_num',
			));

		$this->addColumn('bill_to', array(
				'header'    => Mage::helper('economic')->__('Customer name'),
				'width'     => '300px',
				'index'     => 'bill_to',
			));

		$this->addColumn('created_time', array(
				'header'    => Mage::helper('economic')->__('Created'),
				'align'     => 'left',
				'width'     => '180px',
				'type'      => 'datetime',
				'default'   => '--',
				'index'     => 'created_time',
			));

		$this->addColumn('update_time', array(
				'header'    => Mage::helper('economic')->__('Transferred'),
				'align'     => 'left',
				'width'     => '180px',
				'type'      => 'datetime',
				'default'   => '--',
				'index'     => 'update_time',
			));

		$this->addColumn('type', array(
				'header'    => Mage::helper('economic')->__('Type'),
				'align'     => 'center',
				'width'     => '100px',
				'index'     => 'type',
				'type'      => 'options',
				'options'   => array(
					1 => $this->__('Credit Memo'),
					0 => $this->__('Invoice'),
					-1 => $this->__('Order')
				),
			));

		$this->addColumn('status', array(
				'header'    => Mage::helper('economic')->__('Status'),
				'align'     => 'center',
				'width'     => '100px',
				'index'     => 'status',
				'type'      => 'options',
				'options'   => array(
					1 => Mage::helper('economic')->__('Success'),
					0 => Mage::helper('economic')->__('Pending'),
					99 => Mage::helper('economic')->__('Error')
				),
			));

		return parent::_prepareColumns();
	}

	public function getRowUrl($row)
	{
		if(!$row->getData('inv_num')){
			return $this->getUrl('adminhtml/sales_order/view', array('order_id' =>$row->getEntityId()));
		}
		elseif($row->getData('type') == 1){
			return $this->getUrl('adminhtml/sales_creditmemo/view', array('creditmemo_id' =>$row->getEntityId()));
		}
		else{
			return $this->getUrl('adminhtml/sales_invoice/view', array('invoice_id' =>$row->getEntityId()));
		}
	}
	
	public function getGridUrl()
	{
	    return $this->getUrl('*/*/grid', array('_current'=>true));
	}

}