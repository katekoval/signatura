<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

$installer = $this;

$installer->startSetup();

if (!Mage::getStoreConfig('tric_info/install/date')) {
	$config = Mage::getModel('core/config_data');
	$config	->setScope('default')
			->setPath('tric_info/install/date')
			->setValue(time())
			->save();
}

$installer->endSetup();