<?php
/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Info_Model_Feed_Abstract extends Mage_Core_Model_Abstract
{

	public function getFeedData()
	{
		try{
			$data = false;
			if(extension_loaded('curl')) {
				$ch = curl_init($this->getFeedUrl());
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
				curl_setopt($ch, CURLOPT_TIMEOUT, 1);
				curl_setopt($ch, CURLOPT_TIMEOUT_MS, 500);

				$data = curl_exec($ch);

				$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				$curl_errno = curl_errno($ch);

				curl_close($ch);

				if($httpCode != 200 || $curl_errno > 0) {
					return false;
				}
			}

			if(!$data){
				return false;
			}
		}
		catch (Exception $e) {
			return false;
		}

		try {
			$xml = new SimpleXMLElement($data);
		}
		catch (Exception $e) {
			return false;
		}

		return $xml;
	}

	public function getDate($rssDate)
	{
		return gmdate('Y-m-d H:i:s', strtotime($rssDate));
	}

}