<?php
/**
* Magento Extension by TRIC Solutions
*
* @copyright  Copyright (c) 2011 TRIC Solutions (http://www.tric.dk)
* @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
* @store       http://store.tric.dk
*/

class TRIC_Info_Model_Feed extends Mage_AdminNotification_Model_Feed
{
    const XML_FEED_URL_PATH = 'tric_info/feed/url';
    const XML_FREQUENCY_PATH = 'tric_info/feed/check_frequency';
    const XML_FREQUENCY_ENABLE = 'tric_info/feed/enabled';
    const XML_LAST_UPDATE_PATH = 'tric_info/feed/last_update';
	const CACHE_LASTCHECK = "tric_info_feeds_lastcheck";

    public static function check()
    {
        if (($this->getFrequency() + $this->getLastUpdate()) > time()) {
            return $this;
        }

        $feedData = array();

        $feedXml = $this->getFeedData();
        if ($feedXml && $feedXml->channel && $feedXml->channel->item) {
            foreach ($feedXml->channel->item as $item) {
                $feedData[] = array(
                    'severity' => ((int)$item->severity) ? (int)$item->severity : 3,
                    'date_added' => $this->getDate(trim((string)$item->pubDate)),
                    'title' => trim((string)$item->title),
                    'description' => trim((string)$item->description),
                    'url' => trim((string)$item->link),
                );
            }
            if ($feedData) {
                Mage::getModel('adminnotification/inbox')->parse(array_reverse($feedData));
            }

        }
        Mage::app()->saveCache(time(), self::CACHE_LASTCHECK);

        return $this;
    }
    
    public function getFeedUrl()
    {
        if (is_null($this->_feedUrl)) 
        {
            $this->_feedUrl = 'http://' . Mage::getStoreConfig(self::XML_FEED_URL_PATH);
        }
        return $this->_feedUrl;
    }

    public function getFrequency()
    {
        return Mage::getStoreConfig(self::XML_FREQUENCY_PATH) * 3600;
    }

    public function getLastUpdate()
    {
        return Mage::app()->loadCache(self::CACHE_LASTCHECK);
    }
}
