<?php

/**
 * Magento Extension by TRIC Solutions
 *
 * @copyright  Copyright (c) 2016 TRIC Solutions (http://www.tric.dk)
 * @license    http://www.tric.dk/TRIC-LICENSE-COMMUNITY.txt
 * @store       http://store.tric.dk
 */

class TRIC_Info_Helper_Data extends Mage_Core_Helper_Abstract
{
	private function __getAllStoreDomains()
	{
		$domains = array();

		foreach (Mage::app()->getWebsites() as $website)
		{
			foreach($website->getStores() as $store)
			{
				$url = $store->getConfig('web/unsecure/base_url');
				if($domain = trim(preg_replace('/^.*?\\/\\/(.*)?\\//', '$1', $url))){
					$domain = explode("/",$domain);
					if(count($domain) > 1) {
						$domain = array($domain[0]);
					}
					$domains[] = $domain[0];
				}

				$url = $store->getConfig('web/secure/base_url');
				if($domain = trim(preg_replace('/^.*?\\/\\/(.*)?\\//', '$1', $url))){
					$domain = explode("/",$domain);
					if(count($domain) > 1) {
						$domain = array($domain[0]);
					}
					$domains[] = $domain[0];
				}
			}
		}

		$url = Mage::getStoreConfig('web/unsecure/base_url',Mage_Core_Model_App::ADMIN_STORE_ID);
		if($domain = trim(preg_replace('/^.*?\\/\\/(.*)?\\//', '$1', $url))){
			$domain = explode("/",$domain);
			if(count($domain) > 1) {
				$domain = array($domain[0]);
			}
			$domains[] = $domain[0];
		}

		$url = Mage::getStoreConfig('web/secure/base_url',Mage_Core_Model_App::ADMIN_STORE_ID);
		if($domain = trim(preg_replace('/^.*?\\/\\/(.*)?\\//', '$1', $url))){
			$domain = explode("/",$domain);
			if(count($domain) > 1) {
				$domain = array($domain[0]);
			}
			$domains[] = $domain[0];
		}

		return array_unique($domains);
	}

	private function __getParamsForValidation($extension)
	{
		$config = Mage::getConfig()->getModuleConfig($extension);

		$version = isset($config->version) ? $config->version : "";
		if((string)$config->active == 'false'){
			$licensekey = "Disabled!";
		}
		else{
			$licensekey = $this->__getLicenseKeyForExtension($extension);
		}
		$domains = implode(";",$this->__getAllStoreDomains());
		$params = array('extension' => $extension,
			'license' => $licensekey,
			'domains' => $domains,
			'version' => $version);
		return $params;

	}

	private function __getLicenseKeyForExtension($extension)
	{
		$licensekey = '';

		try
		{
			if(class_exists($extension.'_Helper_Data')){
				if(method_exists($extension.'_Helper_Data','getLicenseKey')){
					$licensekey = Mage::helper($extension)->getLicenseKey();
					return $licensekey;
				}
			}
		}
		catch(Exception $e)
		{
			$licensekey = 'Exception: '.$e->getMessage();
		}

		return $licensekey;
	}

	public function getValidDomains($extension)
	{
		try{
			$cache = Mage::getSingleton('core/cache');

			if(!$cache->load('TRIC_Info_Valid_Domains_For_'.$extension))
			{
				$domain = "https://services.tric.dk/license/validate.php?";
				$params = $this->__getParamsForValidation($extension);

				foreach($params as $k => $v)
				{
					$domain .= "$k=$v&";
				}
				$domain = rtrim($domain,"&");
				
				$json = false;
				if(extension_loaded('curl')) {
					$ch = curl_init($domain);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
					curl_setopt($ch, CURLOPT_TIMEOUT, 1);
					curl_setopt($ch, CURLOPT_TIMEOUT_MS, 500);

					$json = curl_exec($ch);

					$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
					$errorNo = curl_errno($ch);

					curl_close($ch);

					if($httpCode != 200 || $errorNo > 0) {
						return array();
					}
				}

				if(!$json){
					return array();
				}

				$array = json_decode($json,true);
				if(isset($array['live']) && isset($array['dev']))
				{
					$domains = array_unique(array_merge($array['live'],$array['dev']));
					if(!empty($domains)) {
						$cache->save(serialize($domains),'TRIC_Info_Valid_Domains_For_'.$extension, array(), 24*60*60);
					}
					return $domains;
				}
				else
				{
					return array();
				}
			}
			else
			{
				$domains = unserialize($cache->load('TRIC_Info_Valid_Domains_For_'.$extension));
				return $domains;
			}
		}
		catch(Exception $e){
			return array();
		}
	}
}
