<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Cart
 */


class Amasty_Cart_Model_Cart_ProductObserver
{
    /**
     * @var bool
     */
    protected $_added = null;

    /**
     * event checkout_cart_product_add_after
     *
     * @param $observer
     */
    public function productAdded($observer)
    {
        if ($observer->getQuoteItem() && !$observer->getQuoteItem()->getData('has_error')) {
            $this->_added = true;
        } else {
            $this->_added = false;
        }
    }

    /**
     * @return bool
     */
    public function isProductAdded()
    {
        return (bool)$this->_added;
    }
}
