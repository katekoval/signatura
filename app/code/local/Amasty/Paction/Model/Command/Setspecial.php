<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Paction
 */


class Amasty_Paction_Model_Command_Setspecial extends Amasty_Paction_Model_Command_Modifyspecial
{
    public function __construct($type, $dateFrom = '', $dateTo = '')
    {
        parent::__construct($type, $dateFrom, $dateTo);
        $this->_label = 'Set Special Price';
        $this->_fieldLabel = 'Value';
    }

    public function execute($ids, $storeId, $val)
    {
        $hlp = Mage::helper('ampaction');

        $dates = $this->getDates();

        if (strtotime($dates['to']) < strtotime($dates['from'])) {
            throw new Exception($hlp->__('Please provide correct From/To'));
        }

        $val = floatval($val);
        if ($val < 0.00001) {
            throw new Exception($hlp->__('Please provide a non empty difference'));
        }

        $attrCode = $this->_getAttrCode();
        $this->_setAttribute($attrCode, $ids, $storeId, $val);

        if ($dates['from']) {
            $fromDateCode = $this->_getFromDateCode();
            $this->_setDate($fromDateCode, $ids, $storeId, $dates['from']);
        }

        if ($dates['to']) {
            $toDateCode = $this->_getToDateCode();
            $this->_setDate($toDateCode, $ids, $storeId, $dates['to']);
        }

        if (version_compare(Mage::getVersion(), '1.4.1.0') > 0) {
            if (Mage::getStoreConfigFlag('ampaction/general/reindex')) {
                $obj = new Varien_Object();
                $obj->setData(
                    array(
                        'product_ids'       => array_unique($ids),
                        'attributes_data'   => array($attrCode => true), // known indexers use just keys
                        'store_id'          => $storeId,
                    )
                );
                // register mass action indexer event
                Mage::getSingleton('index/indexer')->processEntityAction(
                    $obj, Mage_Catalog_Model_Product::ENTITY, Mage_Index_Model_Event::TYPE_MASS_ACTION
                );

                Mage::getResourceModel('catalog/product_indexer_price')->reindexProductIds($ids);
            } else {
                $process = Mage::getSingleton('index/indexer')->getProcessByCode('catalog_product_price');
                $process->setStatus(Mage_Index_Model_Process::STATUS_REQUIRE_REINDEX);
                $process->save();
                if (Mage::helper('catalog/category_flat')->isEnabled()) {
                    $process = Mage::getSingleton('index/indexer')->getProcessByCode('catalog_product_flat');
                    $process->setStatus(Mage_Index_Model_Process::STATUS_REQUIRE_REINDEX);
                    $process->save();
                }
            }
        }

        $success = $hlp->__('Total of %d products(s) have been successfully updated', count($ids));

        return $success;
    }

    /**
     * Mass set attribute value
     *
     * @param string $attrCode attribute code, price or special price
     * @param array $productIds applied product ids
     * @param int $storeId store id
     * @param array $diff difference data (sign, value, if percentage)
     * @return bool true
     */
    protected function _setAttribute($attrCode, $productIds, $storeId, $value)
    {
        $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $attrCode);

        $db     = Mage::getSingleton('core/resource')->getConnection('core_write');
        $table  = $attribute->getBackend()->getTable();

        $where = array(
            $db->quoteInto('entity_id IN(?)', $productIds),
            $db->quoteInto('attribute_id=?', $attribute->getAttributeId()),
        );

        $defaultStoreId = Mage_Catalog_Model_Abstract::DEFAULT_STORE_ID;
        if (Mage::app()->isSingleStoreMode()) {
            $db->delete(
                $table,
                join(' AND ', array_merge($where, array($db->quoteInto('store_id <> ?', $defaultStoreId))))
            );
        }

        $storeIds  = array();
        if ($attribute->isScopeStore()) {
            $where[] = $db->quoteInto('store_id = ?', $storeId);
            $storeIds[] = $storeId;
        } elseif ($attribute->isScopeWebsite() && $storeId != $defaultStoreId) {
            $storeIds = Mage::app()->getStore($storeId)->getWebsite()->getStoreIds(true);
            $where[] = $db->quoteInto('store_id IN(?)', $storeIds);
        } else {
            $where[] = $db->quoteInto('store_id = ?', $defaultStoreId);
        }

        if ($storeIds) {
            $cond = array(
                $db->quoteInto('t.entity_id IN(?)', $productIds),
                $db->quoteInto('t.attribute_id=?', $attribute->getAttributeId()),
                't.store_id = ' . $defaultStoreId,
            );
            foreach ($storeIds as $id) {
                // copy attr value from global scope if current attr value does not exists.
                $sql = "INSERT IGNORE INTO $table (`entity_type_id`, `attribute_id`, `store_id`, `entity_id`, `value`) "
                    . " SELECT t.`entity_type_id`, t.`attribute_id`, $id, t.`entity_id`, t.`value` FROM $table AS t"
                    . " WHERE " . join(' AND ', $cond);
                $db->raw_query($sql);
            }
        }

        $sql = $this->_prepareQuery($table, $value, $where);
        $db->raw_query($sql);

        return true;
    }
}
