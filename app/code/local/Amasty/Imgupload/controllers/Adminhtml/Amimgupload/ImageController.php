<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Imgupload
 */
class Amasty_Imgupload_Adminhtml_Amimgupload_ImageController extends Mage_Adminhtml_Controller_Action
{
    public function uploadAction()
    {
        $productId = $this->getRequest()->getParam('product_id');
        $product   = Mage::getModel('catalog/product')->load($productId);
        
        try {
            if (class_exists("Mage_Core_Model_File_Uploader")) {
                $uploader = new Mage_Core_Model_File_Uploader('file_select');
            } else {
                $uploader = new Varien_File_Uploader('file_select');
            }
            $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
            $uploader->addValidateCallback(
                'catalog_product_image',
                Mage::helper('catalog/image'),
                'validateUploadFile'
            );

            if (class_exists("Mage_Core_Model_File_Validator_Image")) {
                $uploader->addValidateCallback(
                    Mage_Core_Model_File_Validator_Image::NAME,
                    Mage::getModel('core/file_validator_image'),
                    'validate'
                );
            }

            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(true);
            $result = $uploader->save(
                Mage::getSingleton('catalog/product_media_config')->getBaseTmpMediaPath()
            );
            $result['url'] = Mage::getSingleton('catalog/product_media_config')->getTmpMediaUrl($result['file']);
            $result['file'] = $result['file'] . '.tmp';
            
            if ($product->getId()) {
                // now saving image to product info
                $mediaGallery = $product->getMediaGallery();
                $mediaGallery['images'][] = array(
                    'file'  => $result['file'],
                    'url'   => $result['url'],
                    'disabled' => 0,
                    'removed' => 0,
                    'position' => count($mediaGallery['images']) + 1,
                    'label' => '',
                );
                $product->setMediaGallery($mediaGallery);
                $product->save();
            }
            $response = array(
                'url'	=> $result['url'],
                'file'	=> $result['file'],
            );
            
        } catch (Exception $e) {
            $response = array(
                'error' => $e->getMessage(),
                'errorcode' => $e->getCode());
        }
       
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
    }
    
    public function reloadtabAction()
    {
        $productId = $this->getRequest()->getParam('product_id');
        $storeId   = $this->getRequest()->getParam('store');
        
        try {
            if (!$productId) {
                Mage::throwException($this->__('No product ID specified'));
            }
            $product = Mage::getModel('catalog/product')->load($productId);
            if (!$product->getId()) {
                Mage::throwException($this->__('Error occured while loading product'));
            }
            if ($storeId) {
                $product->setStoreId($storeId);
            }

            // will save product image data first
            $imgUploadData = $this->getRequest()->getParam('amimgupload');
            $productData   = $this->getRequest()->getParam('product');
            Mage::getModel('amimgupload/observer')->saveMediaGallery($product, $imgUploadData, $productData);

            $product->save();
            $block = Mage::app()->getLayout()->createBlock(
                'amimgupload/adminhtml_catalog_product_edit_tab_images',
                'amimgupload_tab_images',
                array('product' => $product)
            );
            $response = $block->toHtml();
        } catch (Mage_Core_Exception $e) {
            $response = $e->getMessage();
        } catch (Exception $e) {
            $response = Mage::helper('catalog')->__('Error saving product information. ' . $e->getMessage());
            Mage::logException($e);
        }

        $this->getResponse()->setBody($response);
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('catalog/products');
    }
}
