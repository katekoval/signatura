<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_InvisibleCaptcha
 */


$this->startSetup();

$this->run("
DELETE FROM `{$this->getTable('core/config_data')}` WHERE `path`='aminvisiblecaptcha/frontend/enabled';
UPDATE `{$this->getTable('core/config_data')}` SET `path`='aminvisiblecaptcha/frontend/enabled' WHERE `path`='aminvisiblecaptcha/general/enabled';

DELETE FROM `{$this->getTable('core/config_data')}` WHERE `path`='aminvisiblecaptcha/general/invisible_key';
UPDATE `{$this->getTable('core/config_data')}` SET `path`='aminvisiblecaptcha/general/invisible_key' WHERE `path`='aminvisiblecaptcha/general/captcha_key';

DELETE FROM `{$this->getTable('core/config_data')}` WHERE `path`='aminvisiblecaptcha/general/invisible_secret';
UPDATE `{$this->getTable('core/config_data')}` SET `path`='aminvisiblecaptcha/general/invisible_secret' WHERE `path`='aminvisiblecaptcha/general/captcha_secret';

DELETE FROM `{$this->getTable('core/config_data')}` WHERE `path`='aminvisiblecaptcha/frontend/language';
UPDATE `{$this->getTable('core/config_data')}` SET `path`='aminvisiblecaptcha/frontend/language' WHERE `path`='aminvisiblecaptcha/general/language';

DELETE FROM `{$this->getTable('core/config_data')}` WHERE `path`='aminvisiblecaptcha/frontend/badge_theme';
UPDATE `{$this->getTable('core/config_data')}` SET `path`='aminvisiblecaptcha/frontend/badge_theme' WHERE `path`='aminvisiblecaptcha/general/badge_theme';

DELETE FROM `{$this->getTable('core/config_data')}` WHERE `path`='aminvisiblecaptcha/frontend/badge_position';
UPDATE `{$this->getTable('core/config_data')}` SET `path`='aminvisiblecaptcha/frontend/badge_position' WHERE `path`='aminvisiblecaptcha/general/badge_position';

DELETE FROM `{$this->getTable('core/config_data')}` WHERE `path`='aminvisiblecaptcha/frontend/badge_styles';
UPDATE `{$this->getTable('core/config_data')}` SET `path`='aminvisiblecaptcha/frontend/badge_styles' WHERE `path`='aminvisiblecaptcha/general/badge_styles';

DELETE FROM `{$this->getTable('core/config_data')}` WHERE `path`='aminvisiblecaptcha/general/ip_white_list';
UPDATE `{$this->getTable('core/config_data')}` SET `path`='aminvisiblecaptcha/general/ip_white_list' WHERE `path`='aminvisiblecaptcha/advanced/ip_white_list';
");

$this->endSetup();
