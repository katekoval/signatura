<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_InvisibleCaptcha
 */


class Amasty_InvisibleCaptcha_Model_System_Config_Source_Default_Form_Frontend
{
    const CUSTOMER_CREATE = 'customer/account/createpost';
    const CUSTOMER_LOGIN = 'customer/account/loginPost';
    const CUSTOMER_FORGOTPASSWORD = 'customer/account/forgotpasswordpost';
    const CUSTOMER_RESETPASSWORD = 'customer/account/resetpasswordpost';
    const NEWSLETTER_SUBSCRIBE = 'newsletter/subscriber/new';
    const PRODUCT_REVIEW = 'review/product/post';
    const CONTACT_US = 'contacts/index/post';

    public function toOptionArray()
    {
        $hlp = Mage::helper('aminvisiblecaptcha');

        return array(
            array(
                'value' => '',
                'label' => ''
            ),
            array(
                'value' => self::CUSTOMER_CREATE,
                'label' => $hlp->__('Customer Create Account')
            ),
            array(
                'value' => self::CUSTOMER_LOGIN,
                'label' => $hlp->__('Customer Login')
            ),
            array(
                'value' => self::CUSTOMER_FORGOTPASSWORD,
                'label' => $hlp->__('Customer Forgot Password')
            ),
            array(
                'value' => self::CUSTOMER_RESETPASSWORD,
                'label' => $hlp->__('Customer Reset Password')
            ),
            array(
                'value' => self::NEWSLETTER_SUBSCRIBE,
                'label' => $hlp->__('Newsletter Subscription')
            ),
            array(
                'value' => self::PRODUCT_REVIEW,
                'label' => $hlp->__('Product Review')
            ),
            array(
                'value' => self::CONTACT_US,
                'label' => $hlp->__('Contact Us')
            )
        );
    }
}
