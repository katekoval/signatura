<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_InvisibleCaptcha
 */


class Amasty_InvisibleCaptcha_Model_System_Config_Source_Default_Form_Backend
{
    const LOGIN = 'login';
    const FORGOTPASSWORD = 'forgotpassword';

    public function toOptionArray()
    {
        $hlp = Mage::helper('aminvisiblecaptcha');

        return array(
            array(
                'value' => self::LOGIN,
                'label' => $hlp->__('Admin Login')
            ),
            array(
                'value' => self::FORGOTPASSWORD,
                'label' => $hlp->__('Admin Forgot Password')
            )
        );
    }
}
