<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_InvisibleCaptcha
 */


class Amasty_InvisibleCaptcha_Model_System_Config_Source_Badge_Size
{
    const NORMAL = 'normal';
    const COMPACT = 'compact';

    public function toOptionArray()
    {
        $hlp = Mage::helper('aminvisiblecaptcha');

        return array(
            array(
                'value' => self::NORMAL,
                'label' => $hlp->__('Normal')
            ),
            array(
                'value' => self::COMPACT,
                'label' => $hlp->__('Compact')
            )
        );
    }
}
