<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_InvisibleCaptcha
 */


class Amasty_InvisibleCaptcha_Model_System_Config_Source_Captcha_Type
{
    const CHECKBOX = 'checkbox';
    const INVISIBLE = 'invisible';
    const V3 = 'v3';

    public function toOptionArray()
    {
        $hlp = Mage::helper('aminvisiblecaptcha');

        return array(
            array(
                'value' => self::CHECKBOX,
                'label' => $hlp->__('V2 Checkbox')
            ),
            array(
                'value' => self::INVISIBLE,
                'label' => $hlp->__('V2 Invisible')
            ),
            array(
                'value' => self::V3,
                'label' => $hlp->__('V3')
            )
        );
    }
}
