<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_InvisibleCaptcha
 */


class Amasty_InvisibleCaptcha_Model_Captcha
{
    const GOOGLE_VERIFY_URL = 'https://www.google.com/recaptcha/api/siteverify';

    /**
     * @var array
     */
    protected $_supportedExtensionUrl;

    protected $_errorCodes = array(
        'missing-input-secret' => 'The secret parameter is missing.',
        'invalid-input-secret' => 'The secret parameter is invalid or malformed.',
        'missing-input-response' => 'The response parameter is missing (please complete the reCAPTCHA).',
        'invalid-input-response' => 'The response parameter is invalid or malformed.',
        'bad-request' => 'The request is invalid or malformed.',
        'invalid-keys' => 'The Site Key and the Secret Key are incorrect or does not match.'
    );

    protected $_forbiddenSelectorsOnCheckout = array(
        'customer/account/loginPost'
    );

    protected $_checkouts = array(
        'checkout'
    );

    protected $_urls = array();
    protected $_selectors = array();

    /**
     * Amasty_InvisibleCaptcha_Model_Captcha constructor.
     * Set Urls for supported extensions
     */
    public function __construct()
    {
        $this->_supportedExtensionUrl = array(
            'amasty_customform' => 'customform/form/submit'
        );

        if (0 < Mage::getStoreConfig('aminvisiblecaptcha/amasty/magpleasure_blogpro')) {
            $this->_supportedExtensionUrl['magpleasure_blogpro'] = 'mpblog/index/postForm';
        }

        if (0 < Mage::getStoreConfig('aminvisiblecaptcha/amasty/amasty_faq')) {
            $faqLink = Mage::getStoreConfig('amfaq/general/url_prefix');
            $this->_supportedExtensionUrl['amasty_faq'] = '/' . $faqLink . '/';
        }
    }

    /**
     * Validation of token from Google
     *
     * @param string $token
     * @return array
     * @throws Zend_Http_Client_Exception
     */
    public function verify($token)
    {
        $verification = array(
            'success' => false,
            'error' => ''
        );

        $postOptions = array(
            'secret' => $this->_getSecretKey(),
            'response' => $token
        );

        try {
            $googleVerify = new Varien_Http_Adapter_Curl();
            $googleVerify->write(Zend_Http_Client::POST,
                self::GOOGLE_VERIFY_URL,
                '1.1',
                array(),
                $postOptions
            );
            $googleResponse = $googleVerify->read();
            $body = Zend_Http_Response::extractBody($googleResponse);
            $answer = json_decode($body);

            if (array_key_exists('success', $answer)) {
                $success = $answer->success;

                if (isset($answer->{'score'})
                    && Mage::getStoreConfig('aminvisiblecaptcha/frontend/type') ===
                        \Amasty_InvisibleCaptcha_Model_System_Config_Source_Captcha_Type::V3
                    && $answer->{'score'} < (float)Mage::getStoreConfig('aminvisiblecaptcha/frontend/v3_score')
                ) {
                    $verification['error'] = Mage::helper('core')->escapeHtml(
                        Mage::getStoreConfig('aminvisiblecaptcha/frontend/error_message')
                    );
                    $verification['success'] = false;
                } elseif ($success) {
                    $verification['success'] = true;
                } elseif (array_key_exists('error-codes', $answer)) {
                    $error = $answer->{'error-codes'};
                    $verification['error'] = $this->_errorCodes[$error[0]];
                }
            }
        } catch (Exception $e) {
            Mage::log($e->__toString(), null, 'Amasty_InvisibleCaptcha.log');
        }

        return $verification;
    }

    /**
     * Return URLs to verify
     *
     * @return array
     */
    public function getUrls()
    {
        if (empty($this->_urls)) {
            $urls = trim(Mage::getStoreConfig('aminvisiblecaptcha/advanced/captcha_urls'));
            $urlsList = preg_split('|\s*[\r\n]+\s*|', $urls, -1, PREG_SPLIT_NO_EMPTY);
            foreach ($this->_supportedExtensionUrl as $configKey => $extensionUrl) {
                if (0 < Mage::getStoreConfig('aminvisiblecaptcha/amasty/' . $configKey)) {
                    $urlsList[] = $extensionUrl;
                }
            }
            $defaultUrls = $this->_getDefaultValues();
            $this->_urls = array_unique(array_merge($defaultUrls, $urlsList));
        }

        return $this->_urls;
    }

    public function isEnabled($area = 'frontend')
    {
        if (!Mage::getStoreConfig('aminvisiblecaptcha/' . $area . '/enabled')) {
            return false;
        }

        if ('frontend' == $area
            && ((Mage::getStoreConfig('aminvisiblecaptcha/advanced/enabled_captcha_for_guests_only')
                    && Mage::getSingleton('customer/session')->isLoggedIn())
                || $this->_isAdmin())
        ) {
            return false;
        }

        if ('backend' == $area) {
            $forms = explode(',', Mage::getStoreConfig('aminvisiblecaptcha/backend/default_forms'));
            $action = Mage::app()->getRequest()->getActionName();

            if ('index' != $action && !in_array($action, $forms)) {
                return false;
            }
        }

        $ips = trim(Mage::getStoreConfig('aminvisiblecaptcha/general/ip_white_list'));

        if ($ips) {
            $ips = $ips ? preg_split('|\s*[\r\n]+\s*|', $ips, -1, PREG_SPLIT_NO_EMPTY) : array();

            if (!empty($ips)) {
                if (class_exists('Amasty_Base_Model_Ip')
                    && Mage::helper('core')->isModuleEnabled('Amasty_Base')
                ) {
                    $remoteAddress = Mage::getModel('ambase/ip')->getCustomerIp();
                } else {
                    $remoteAddress = Mage::helper('core/http')->getRemoteAddr();
                }

                if (in_array($remoteAddress, $ips)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Return selectors of forms to protect
     *
     * @return array
     */
    public function getSelectors()
    {
        if (empty($this->_selectors)) {
            $selectors = trim(Mage::getStoreConfig('aminvisiblecaptcha/advanced/captcha_selectors'));
            $selectors = preg_split('|\s*[\r\n]+\s*|', $selectors, -1, PREG_SPLIT_NO_EMPTY);

            foreach ($this->_supportedExtensionUrl as $configKey => $extensionUrl) {
                if (0 < Mage::getStoreConfig('aminvisiblecaptcha/amasty/' . $configKey)) {
                    switch ($configKey) {
                        case 'amasty_customform':
                            $selectors[] = 'form[action*="' . $extensionUrl . '"]';
                            break;
                        case 'amasty_faq':
                            $selectors = array_merge(array('#amfaq-ask-form-inline', '#amfaq-ask-form'), $selectors);
                            break;
                        case 'magpleasure_blogpro' :
                            $postId = Mage::app()->getRequest()->getParam('id', null);
                            if ($postId) {
                                $selectors = array_merge(array('#mpblog-form-' . $postId), $selectors);
                            }
                            break;
                    }
                }
            }

            $defaultSelectors = $this->_getDefaultValues(true);
            $this->_selectors = array_unique(array_merge($defaultSelectors, $selectors));
        }

        return $this->_selectors;
    }

    protected function _isAdmin()
    {
        if (Mage::app()->getStore()->isAdmin()) {
            return true;
        }
        // for some reason isAdmin does not work here
        if ('sales_order_create' == Mage::app()->getRequest()->getControllerName()) {
            return true;
        }

        return false;
    }

    protected function _getDefaultValues($isSelector = false)
    {
        $defaultValues = Mage::getStoreConfig('aminvisiblecaptcha/frontend/default_forms');

        if ($defaultValues) {
            $defaultValues = explode(',', Mage::getStoreConfig('aminvisiblecaptcha/frontend/default_forms'));
            if ($isSelector) {
                $controller = Mage::app()->getRequest()->getModuleName();

                foreach ($defaultValues as $key => $value) {
                    if (in_array($controller, $this->_checkouts)
                        && in_array($value, $this->_forbiddenSelectorsOnCheckout)
                    ) {
                        continue;
                    } else {
                        $defaultValues[$key] = 'form[action*="' . $value . '"]';
                    }
                }
            }
        } else {
            $defaultValues = array();
        }

        return $defaultValues;
    }

    protected function _getSecretKey()
    {
        if ($this->_isAdmin()) {
            $captchaType = Mage::getStoreConfig('aminvisiblecaptcha/backend/type');
        } else {
            $captchaType = Mage::getStoreConfig('aminvisiblecaptcha/frontend/type');
        }

        if (Amasty_InvisibleCaptcha_Model_System_Config_Source_Captcha_Type::CHECKBOX == $captchaType) {
            $secretKey = Mage::getStoreConfig('aminvisiblecaptcha/general/checkbox_secret');
        } else if (Amasty_InvisibleCaptcha_Model_System_Config_Source_Captcha_Type::INVISIBLE == $captchaType) {
            $secretKey = Mage::getStoreConfig('aminvisiblecaptcha/general/invisible_secret');
        } else {
            $secretKey = Mage::getStoreConfig('aminvisiblecaptcha/general/v3_secret');
        }

        return $secretKey;
    }
}
