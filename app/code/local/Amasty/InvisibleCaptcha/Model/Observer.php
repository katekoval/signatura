<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_InvisibleCaptcha
 */


class Amasty_InvisibleCaptcha_Model_Observer
{
    const PARAM_NAME_REFERER_URL = 'referer_url';
    const PARAM_NAME_BASE64_URL = 'r64';
    const PARAM_NAME_URL_ENCODED = 'uenc';

    protected $_forbiddenUrls = array(
        'checkout',
        'onepage',
        'cart'
    );

    protected $_forbiddenUrlsOnCheckout = array(
        'customer/account/loginPost',
        'customer/account/login'
    );

    protected function _redirectToRefererUrl($action, $validation)
    {
        $errorText = $validation['error'];

        if (!$errorText) {
            $errorText = 'Something is wrong.';
        }

        Mage::getSingleton('core/session')->addError(
            Mage::helper('aminvisiblecaptcha')->__($errorText)
        );

        $action
            ->getResponse()
            ->setRedirect($this->_getRefererUrl($action))
            ->sendResponse();

        Mage::helper('ambase/utils')->_exit();
    }

    public function handleControllers($observer)
    {
        /** @var Mage_Core_Controller_Front_Action $action */
        $action = $observer->getControllerAction();
        $request = $action->getRequest();

        if ('forgotpassword' == $request->getActionName()
            && $request->isPost()
            && Mage::getModel('aminvisiblecaptcha/captcha')->isEnabled('backend')
        ) {
            $validation = $this->_getAndVerifyToken();

            if (!$validation['success']) {
                $this->_redirectToRefererUrl($action, $validation);
            }
        }

        if (Mage::getModel('aminvisiblecaptcha/captcha')->isEnabled()) {
            foreach (Mage::getSingleton('aminvisiblecaptcha/captcha')->getUrls() as $captchaUrl) {
                if (false !== strpos($request->getRequestUri(), $captchaUrl)) {
                    if (false !== strpos($request->getServer('HTTP_REFERER'), 'checkout')
                        && $this->_isUrlForbidden($captchaUrl, $this->_forbiddenUrlsOnCheckout)
                    ) {
                        break;
                    }

                    if ($request->isPost()) {
                        $validation = $this->_getAndVerifyToken();

                        if (!$validation['success']) {
                            $this->_redirectToRefererUrl($action, $validation);
                        }
                    }

                    break;
                }
            }
        }
    }

    protected function _getRefererUrl($action)
    {
        $refererUrl = $action->getRequest()->getServer('HTTP_REFERER');
        if ($url = $action->getRequest()->getParam(self::PARAM_NAME_REFERER_URL)) {
            $refererUrl = $url;
        }
        if ($url = $action->getRequest()->getParam(self::PARAM_NAME_BASE64_URL)) {
            $refererUrl = Mage::helper('core')->urlDecodeAndEscape($url);
        }
        if ($url = $action->getRequest()->getParam(self::PARAM_NAME_URL_ENCODED)) {
            $refererUrl = Mage::helper('core')->urlDecodeAndEscape($url);
        }

        if (!$this->_isUrlInternal($refererUrl)) {
            $refererUrl = Mage::app()->getStore()->getBaseUrl();
        }
        return $refererUrl;
    }

    protected function _isUrlInternal($url)
    {
        if (false !== strpos($url, 'http')) {
            if (0 === strpos($url, Mage::app()->getStore()->getBaseUrl())
                || 0 === strpos($url, Mage::app()->getStore()->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK, true))
            ) {
                return true;
            }
        }
        return false;
    }

    public function settingsSaving($observer)
    {
        if ('aminvisiblecaptcha' == $observer->getObject()->getSection()) {
            $settings = $observer->getObject()->getData();

            if (isset($settings['groups']['advanced']['fields']['captcha_urls']['value'])) {
                $urls = $settings['groups']['advanced']['fields']['captcha_urls']['value'];
                $urls = trim($urls);
                $urlsList = preg_split('|\s*[\r\n]+\s*|', $urls, -1, PREG_SPLIT_NO_EMPTY);
                $forbidden = array();

                foreach ($urlsList as $url) {
                    if ($this->_isUrlForbidden($url, $this->_forbiddenUrls)) {
                        $forbidden[] = $url;
                    }
                }

                if (!empty($forbidden)) {
                    $urlsList = array_diff($urlsList, $forbidden);
                    $urlsList = implode("\r\n", $urlsList);
                    $settings['groups']['advanced']['fields']['captcha_urls']['value'] = $urlsList;
                    $observer->getObject()->setData($settings);
                    $message = Mage::helper('aminvisiblecaptcha')->__('Notice: Module is not compatible with the forms at checkout page: appropriate urls have been deleted from the `URLs to Enable` setting.');
                    Mage::getSingleton('adminhtml/session')->addError($message);
                }
            }
        }
    }

    protected function _isUrlForbidden($url, $forbiddenUrls)
    {
        $found = false;

        foreach ($forbiddenUrls as $forbidden) {
            if (false !== strpos($url, $forbidden)) {
                $found = true;
                break;
            }
        }

        return $found;
    }

    protected function _getAndVerifyToken()
    {
        $request = Mage::app()->getRequest();
        $token = $request->getPost('amasty_invisible_token');

        if (!$token) {
            $token = $request->getPost('g-recaptcha-response');
        }

        return Mage::getSingleton('aminvisiblecaptcha/captcha')->verify($token);
    }

    public function adminUserAuthenticateAfter(Varien_Event_Observer $observer)
    {
        if (Mage::getModel('aminvisiblecaptcha/captcha')->isEnabled('backend')) {
            $validation = $this->_getAndVerifyToken();

            if (!$validation['success']) {
                $errorText = $validation['error'];

                if (!$errorText) {
                    $errorText = 'Something is wrong.';
                }

                if (Mage::getStoreConfig('aminvisiblecaptcha/backend/test')) {
                    Mage::getSingleton('core/session')->addError(
                        Mage::helper('aminvisiblecaptcha')->__($errorText) . ' '
                        . Mage::helper('aminvisiblecaptcha')->__('Please re-check configuration of the Amasty Google reCAPTCHA v2 extension or contact our support team')
                    );
                } else {
                    /** @var $adminSession Mage_Admin_Model_Session */
                    $adminSession = Mage::getSingleton('admin/session');
                    $adminSession->unsetAll();
                    $adminSession->getCookie()->delete($adminSession->getSessionName());

                    Mage::throwException(Mage::helper('aminvisiblecaptcha')->__($errorText));
                    $adminFrontName = (string) Mage::app()->getConfig()->getNode('admin/routers/adminhtml/args/frontName');
                    Mage::app()->getResponse()->setRedirect('*/' . $adminFrontName);
                }
            } elseif (Mage::getStoreConfig('aminvisiblecaptcha/backend/test')) {
                Mage::getSingleton('core/session')->addSuccess(
                    Mage::helper('aminvisiblecaptcha')->__('The Amasty Google reCAPTCHA v2 extension configured correctly, you can disable the Test mode in the configuration of the extension')
                );
            }
        }
    }

    public function replaceRetrieveButton(Varien_Event_Observer $observer)
    {
        $action = Mage::app()->getRequest()->getActionName();
        $block = $observer->getBlock();
        if ('forgotpassword' == $action
            && Mage::getModel('aminvisiblecaptcha/captcha')->isEnabled('backend')
            && 'Amasty_InvisibleCaptcha_Block_Adminhtml_Captcha' != get_class($block)
        ) {
            $transport = $observer->getTransport();
            $html = $transport->getHtml();
            $buttonText = Mage::helper('adminhtml')->__('Retrieve Password');
            $html = preg_replace(
                '#<button(.*?)</button>#s',
                '<input type="submit" class="forgot-password form-button" value="' . $buttonText . '" title="' . $buttonText . '">',
                $html,
                1
            );
            $transport->setHtml($html);
        }
    }

    public function onCoreBlockAbstractToHtmlAfter($observer)
    {
        if (!Mage::registry('need_amasty_captcha')) {
            $block = $observer->getBlock();
            $blockClass = Mage::getConfig()->getBlockClassName('aminvisiblecaptcha/captcha');

            if ($blockClass != get_class($block)) {
                $captcha = Mage::getModel('aminvisiblecaptcha/captcha');

                if ($captcha->isEnabled()) {
                    $transport = $observer->getTransport();
                    $html = $transport->getHtml();
                    $urls = $captcha->getUrls();

                    foreach ($urls as $url) {
                        if (false !== strpos($html, $url)) {
                            Mage::register('need_amasty_captcha', 1);

                            break;
                        }
                    }
                }
            }
        }
    }
}
