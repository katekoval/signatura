<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_InvisibleCaptcha
 */


class Amasty_InvisibleCaptcha_Block_Adminhtml_System_Config_Fieldset_Securityauth extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        if (Mage::helper('core')->isModuleEnabled('Amasty_SecurityAuth')) {
            $cssClass = 'amasty_is_instaled';
            $value = 'Installed';
            $element->setComment($this->__('Specify Two-Factor Authentication settings properly. See more details'
                . ' <a href="%1$s" target="_blank">here</a>.',
                Mage::helper("adminhtml")->getUrl('adminhtml/system_config/edit', array('section'=>'amsecurityauth'))
            ));
        } else {
            $cssClass = 'amasty_not_instaled';
            $value = 'Not installed';
            $element->setComment('Add extra security level to boost data protection for your e-business.'
                . ' Prevent your store from common Internet threats like keyloggers, data sniffing and unsecured wi-fi connections.'
                . ' Stay convinced that your Magento account is available only to your staff members.'
                . ' <a href="https://amasty.com/magento-two-factor-authentication.html'
                . '?utm_source=extension&utm_medium=backend&utm_campaign=m1_captcha_to_twofactor"'
                . ' target="_blank">here</a>.'
            );
        }
        $html = '<span class="' . $cssClass . '">' . $value . ' </span>' . "\n";

        return $html;
    }
}
