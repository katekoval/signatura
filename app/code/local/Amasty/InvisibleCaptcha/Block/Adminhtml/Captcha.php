<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_InvisibleCaptcha
 */


class Amasty_InvisibleCaptcha_Block_Adminhtml_Captcha extends Mage_Core_Block_Template
{
    protected function _construct()
    {
        parent::_construct();

        $this->setTemplate('amasty/aminvisiblecaptcha/captcha.phtml');
    }

    public function isEnabled()
    {
        return Mage::getModel('aminvisiblecaptcha/captcha')->isEnabled('backend');
    }
}
