<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2018 Amasty (https://www.amasty.com)
 * @package Amasty_Smtp
 */
class Amasty_Smtp_EmailController extends Mage_Adminhtml_Controller_Action
{
    public function _isAllowed()
    {
        return true;
    }
}
