<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */


class Amasty_Faq_Model_Question extends Mage_Core_Model_Abstract
{
    protected $_related = null;

    protected function _construct()
    {
        $this->_init('amfaq/question');
    }

    public function getUrl()
    {
        if ('' !== $this->getUrlKey()) {
            return Mage::helper('amfaq')->getFaqUrl() . $this->getUrlKey();
        }

        return Mage::helper('amfaq')->getFaqUrl() . 'question/view/id/' . $this->getId();
    }

    public function vote($mark, $session, $customer_id)
    {
        $vote = Mage::getModel('amfaq/question_rating')->myVote($this->getId(), $session, $customer_id);

        $vote->setUsed(1)
            ->setRatingType(Mage::getStoreConfig('amfaq/rating/type'))
            ->setRate($mark)
            ->save();

        $this->getResource()->updateVotes($this);
        $this->save();
    }

    protected function _beforeSave()
    {
        if ($this->getData('rating')
            && $this->getData('rating') != $this->getOrigData('rating')
        ) {
            $this->getResource()->updateVotes($this, true);
        }
    }

    public function getFiles()
    {
        $collection = Mage::getResourceModel('amfaq/file_collection');
        $collection->addFieldToFilter('question_id', $this->getId());

        return $collection;
    }

    public function canVote($customer)
    {
        $whoCan = Mage::getStoreConfig('amfaq/rating/allowed');

        switch ($whoCan) {
            case 'public':
                return true;
            case 'auth':
                return is_numeric($customer->getId());
            default:
                return false;
        }
    }

    public function getRelatedQuestions()
    {
        if (!$this->_related) {
            $collection = Mage::getResourceModel('amfaq/question_collection');

            $twoWay = Mage::getStoreConfig('amfaq/answer_page/two_way_relations');

            $adapter = $collection->getSelect()->getAdapter();

            $condition = '(r.a = ? AND r.b = main_table.question_id)';
            if ($twoWay) {
                $condition .= ' OR (r.b = ? AND r.a = main_table.question_id)';
            }

            $collection->getSelect()
                ->join(
                    array('r' => $collection->getResource()->getTable('amfaq/question_relation')),
                    $adapter->quoteInto($condition, $this->getId()),
                    array()
                )
                ->group('question_id');

            $this->_related = $collection;
        }

        return $this->_related;
    }

    public function getTopics()
    {
        $topics = Mage::getResourceModel('amfaq/topic_collection')
            ->addFieldToFilter('topic_id', array('in' => $this->getTopicIds()));

        return $topics;
    }

    public function getDefaultTopic()
    {
        $topics = Mage::getResourceModel('amfaq/topic_collection')
            ->addFieldToFilter('topic_id', array('in' => $this->getTopicIds()));

        $topics->getSelect()->limit(1);

        if (0 < sizeof($topics)) {
            return $topics->getFirstItem();
        }
    }

    public function getShortAnswer()
    {
        $strip = Mage::getStoreConfig('amfaq/answer_page/strip_tags');

        $answer = $this->getAnswer();

        $isFullAnswer = false;
        if ($divider = preg_match('/<!--\s*more\s*-->/i', $answer, $matches, PREG_OFFSET_CAPTURE)) {
            $answer = trim(substr($answer, 0, $matches[0][1]));
            if ($strip) {
                $answer = strip_tags(nl2br($answer), '<br>');
            }
        } else {
            if (strlen($answer) <= Mage::getStoreConfig('amfaq/answer_page/short_limit')) {
                $isFullAnswer = true;
            }
            $answer = $this->trimText($answer);
        }

        $isFullAnswer = ($isFullAnswer && Mage::getStoreConfig('amfaq/faq_page/short_is_full'));
        $ret = array(
            'answer' => $answer,
            'is_full_answer' => $isFullAnswer,
        );

        return $ret;
    }

    private function trimText($text)
    {
        $limit = Mage::getStoreConfig('amfaq/answer_page/short_limit');

        $text = strip_tags($text, '<br>');
        $text = str_replace('<br>', "\n", $text);
        $text = str_replace('<br />', "\n", $text);
        $textLength = strlen($text);
        if ($textLength > $limit) {
            $text = mb_substr($text, 0, $limit) . '...';
        }

        return nl2br($text);
    }

    public function getVote()
    {
        if (!$this->hasVote()) {
            $customer = Mage::getSingleton('customer/session')->getCustomer()->getId();

            $vote = Mage::getModel('amfaq/question_rating')->myVote($this->getId(), session_id(), $customer);
            $this->setVote($vote);
        }

        return $this->getData('vote');
    }

    public function getAllVoted()
    {
        return Mage::getModel('amfaq/question_rating')->getAllVoted($this->getId());
    }

    protected function _afterSave()
    {
        $this->_clearCacheByTag();
    }

    protected function _clearCacheByTag()
    {
        $productIds = $this->getProductIds();
        $tags = array();
        if (!empty($productIds)) {
            foreach ($productIds as $id) {
                $tags[] = 'catalog_product_' . $id;
            }
            Mage::app()->cleanCache($tags);
        }

        return true;
    }
}