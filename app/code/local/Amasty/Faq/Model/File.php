<?php

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */
class Amasty_Faq_Model_File extends Mage_Core_Model_Abstract {

    public function _construct() 
    {
        $this->_init('amfaq/file', 'file_id');
    }

    protected function _beforeSave()
    {
        parent::_beforeSave();

        $helper = Mage::helper('amfaq');

        if ($tmp = $this->getTmpName())
        {
            $new = $this->newFileName($this->getName());
            $dest = $helper->getUploadFilename($new);

            if (move_uploaded_file($tmp, $dest))
            {
                $this->_unlink();
                $this->setName($new);
            }
            else
                Mage::getSingleton('adminhtml/session')->addError("cant upload file to " . dirname($dest) . "check the permissions and directory exists");
        }
        else
        {
            if ($this->getOrigData('name') != $this->getData('name'))
            {
                $new = $this->newFileName($this->getName());
                rename(
                    $helper->getUploadFilename($this->getOrigData('name')),
                    $helper->getUploadFilename($new)
                );
                $this->setName($new);
            }
        }

        return $this;
    }

    protected function newFileName($fileName)
    {
        $helper = Mage::helper('amfaq');
        $i = 0;
        $newFileName = preg_replace('/([^[:alnum:]_\.-]*)/', '', $fileName);
        
        if (false === strpos($newFileName, '.')) {
            if (false !== strpos($this->getUploadName(), '.')) {
                $pos = strrpos($this->getUploadName(), '.');
                $newFileName .= substr($this->getUploadName(), $pos);
            }
        }

        while (file_exists($helper->getUploadFilename($newFileName))) {
            $i++;
            $newFileName = "(" . $i . ")" . $fileName;
        }
        return $newFileName;
    }

    public function getTabData()
    {
        $questionId = Mage::registry('current_question')->getId();
        $results = Mage::getResourceModel('amfaq/file_collection')->getTabData($questionId);
        return $results;
    }

    protected function _unlink()
    {
        $fileName = Mage::helper('amfaq')->getUploadFilename($this->getOrigData('name'));
        if (is_file($fileName))
            unlink($fileName);
    }

    public function delete()
    {
        $this->_unlink();
        parent::delete();
    }

}

