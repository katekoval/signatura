<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Model_Resource_Question_Rating extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('amfaq/question_rating', 'rating_id');
    }

    public function myVote($object, $question_id, $session, $customer)
    {
        $read = $this->_getReadAdapter();
        $select = $this->_getLoadSelect('question_id', $question_id, $object);

        $object->setQuestionId($question_id);

        if ($customer)
        {
            $object->setCustomerId($customer);
            $select->where('customer_id=?', $customer);
        }
        else
        {
            $object->setSessionId($session);
            $select->where('session_id=?', $session);
        }

        $data = $read->fetchRow($select);

        if ($data) {
            unset($data['session_id']);
            unset($data['question_id']);
            unset($data['customer_id']);
            $object->setData($data);
        }

        $this->unserializeFields($object);
        $this->_afterLoad($object);

        return $this;
    }

    public function getAllVoted($questionId)
    {
        $adapter = $this->_getReadAdapter();
        $table = $this->getTable('amfaq/question_rating');
        $ratingType = Mage::getStoreConfig('amfaq/rating/type');
        $select = $adapter
            ->select()
            ->from($table, 'COUNT(*)')
            ->where("question_id=?", $questionId)
            ->where('rating_type = ?', $ratingType)
        ;
        $votes = $adapter->fetchRow($select);
        return $votes['COUNT(*)'];
    }
}