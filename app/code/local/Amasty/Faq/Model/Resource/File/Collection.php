<?php

/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */
class Amasty_Faq_Model_Resource_File_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    public function _construct() 
    {
        $this->_init('amfaq/file');
    }

    public function getTabData($questionId)
     {
        $this->getSelect()
                ->where('main_table.question_id = ?', $questionId)
        ;

        return $this;
    }
}

