<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Model_Resource_Tag extends Mage_Core_Model_Resource_Db_Abstract
{
    public function _construct()
    {
        $this->_init('amfaq/tag', 'tag_id');
    }
}
