<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */
class Amasty_Faq_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract
{
    public function match(Zend_Controller_Request_Http $request)
    {
        if (Mage::app()->getStore()->isAdmin()) {
            return false;
        }

        $prefix = Mage::getStoreConfig('amfaq/general/url_prefix');

        $path = explode('/', trim($request->getPathInfo(), '/'));

        if ($path[0] == $prefix)
        {
            if (isset($path[1]))
            {
                $key = $path[1];
                $topicPrefix = Mage::helper('amfaq')->getTopicPrefix();

                if ($key == 'tag')
                {
                    if (!isset($path[2]))
                        return false;

                    $request->setParam('tag', $path[2]);
                    $this->_initModule($request, 'search', 'index');
                }
                if ($key == 'search')
                {
                    $this->_initModule($request, 'search', 'index');
                }
                else if (isset($path[2]))
                {
                    for ($i = 3, $l = sizeof($path); $i < $l; $i += 2) {
                        $request->setParam($path[$i], isset($path[$i+1]) ? urldecode($path[$i+1]) : '');
                    }

                    return $this->_initModule($request, $path[1], $path[2]);
                }
                else if (substr($key, 0, strlen($topicPrefix)) == $topicPrefix)
                {
                    $key = substr($key, strlen($topicPrefix));
                    $topic = Mage::getModel('amfaq/topic')->load($key, 'url_key');
                    if ($topic->getId())
                    {
                        $request->setParam('id', $topic->getId());
                        $this->_initModule($request, 'topic', 'view');
                    }
                    else
                        return false;
                }
                else
                {
                    $question = Mage::getModel('amfaq/question')->load($key, 'url_key');
                    if ($question->getId())
                    {
                        $request->setParam('id', $question->getId());
                        $this->_initModule($request, 'question', 'view');
                    }
                    else
                        return false;
                }
            }
            else
                $this->_initModule($request); // index
        }
        else
            return false;

        return true;
    }

    protected function _initModule(Zend_Controller_Request_Http $request, $controller = 'index', $action = 'index')
    {
        $realModule = 'Amasty_Faq';

        $file = Mage::getModuleDir('controllers', $realModule) . DS . ucfirst($controller) . 'Controller.php';

        if (!is_readable($file))
            return false;

        Mage::register('amfaq_route_dispatched', true, true);

        $request->setModuleName('amfaq');
        $request->setRouteName('amfaq');
        $request->setControllerName($controller);
        $request->setActionName($action);
        $request->setControllerModule($realModule);

        include $file;

        //compatibility with 1.3
        $class = $realModule . '_'.ucfirst($controller).'Controller';
        $controllerInstance = new $class($request, $this->getFront()->getResponse());

        $request->setDispatched(true);
        $controllerInstance->dispatch($action);
    }
}
