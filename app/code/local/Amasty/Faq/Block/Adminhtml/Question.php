<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */


class Amasty_Faq_Block_Adminhtml_Question extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_blockGroup = 'amfaq';
        $this->_controller = 'adminhtml_question';
        $this->_headerText = $this->__('Questions List');

        $this->_addButton('export_m2', array(
            'label' => Mage::helper('amfaq')->__('Export For M2'),
            'onclick' => 'setLocation(\'' . Mage::helper('adminhtml')->getUrl('adminhtml/amfaq_question/export') . '\')',
            'class' => 'go'
        ));

        parent::__construct();
    }
}
