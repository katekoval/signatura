<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Block_Adminhtml_Question_Edit_Tab_Meta extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $model = Mage::registry('current_question');

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getData('action'),
            'method' => 'post'
        ));


        $fieldset = $form->addFieldset('meta_fieldset',
            array('legend' => $this->__('Meta Tags'))
        );

        $fieldset->addField('meta_title', 'text', array(
            'name'  => 'meta_title',
            'label' => $this->__('Meta Title'),
            'title' => $this->__('Meta Title'),
        ));

        $fieldset->addField('meta_description', 'editor', array(
            'name'  => 'meta_description',
            'label' => $this->__('Meta Description'),
            'title' => $this->__('Meta Description'),
        ));

        $fieldset->addField('canonical_url', 'text', array(
            'name'  => 'canonical_url',
            'label' => $this->__('Canonical URL'),
            'title' => $this->__('Canonical URL'),
        ));

        $fieldset->addField('meta_robots', 'text', array(
            'name'  => 'meta_robots',
            'label' => $this->__('Robots'),
            'title' => $this->__('Robots'),
        ));

        $form->addValues($model->getData());

        $this->setForm($form);
        return parent::_prepareForm();
    }
}
