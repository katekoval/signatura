<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Block_Adminhtml_Question_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('question_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle($this->__('Question Information'));
    }

    protected function _prepareLayout()
    {
        $products = $this->getLayout()->createBlock('amfaq/adminhtml_question_edit_tab_products');
        $serializer = $this->getLayout()->createBlock('adminhtml/widget_grid_serializer');
        $serializer->initSerializerBlock($products, 'getSavedProducts', 'product_ids', 'product_ids');


        $related = $this->getLayout()->createBlock('amfaq/adminhtml_question_edit_tab_related');
        $relatedSerializer = $this->getLayout()->createBlock('adminhtml/widget_grid_serializer');
        $relatedSerializer->initSerializerBlock($related, 'getSavedQuestions', 'question_ids', 'question_ids');

        $this->addTab('main', array(
            'label'     => $this->__('Main Info'),
            'title'     => $this->__('Main Info'),
            'content'   => $this->getLayout()->createBlock('amfaq/adminhtml_question_edit_tab_main')->toHtml(),
            'active'    => true
        ));

        $this->addTab('topics', array(
            'label'     => $this->__('Topics'),
            'title'     => $this->__('Topics'),
            'content'   => $this->getLayout()->createBlock('amfaq/adminhtml_question_edit_tab_topics')->toHtml(),
        ));

        $this->addTab('related', array(
            'label'     => $this->__('Related Questions'),
            'title'     => $this->__('Related Questions'),
            'content'   => $related->toHtml().$relatedSerializer->toHtml(),
        ));

        $this->addTab('products', array(
            'label'     => $this->__('Products'),
            'title'     => $this->__('Products'),
            'content'   => $products->toHtml().$serializer->toHtml(),
        ));

        $this->addTab('sender', array(
            'label'     => $this->__('Customer Info'),
            'title'     => $this->__('Customer Info'),
            'content'   => $this->getLayout()->createBlock('amfaq/adminhtml_question_edit_tab_sender')->toHtml(),
        ));

        $this->addTab('files', array(
            'label'     => $this->__('Files'),
            'title'     => $this->__('Files'),
            'content'   => $this->getLayout()->createBlock('amfaq/adminhtml_question_edit_tab_files')->toHtml(),
        ));

        $this->addTab('meta_tags', array(
            'label'     => $this->__('Meta Tags'),
            'title'     => $this->__('Meta Tags'),
            'content'   => $this->getLayout()->createBlock('amfaq/adminhtml_question_edit_tab_meta')->toHtml(),
        ));

        return parent::_prepareLayout();
    }

}