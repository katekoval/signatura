<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Block_Adminhtml_Question_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('amfaqQuestionGrid');
        $this->setDefaultSort('question_id');
        $this->setDefaultDir('ASC');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('amfaq/question')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        parent::_prepareColumns();

        $this->addColumn('question_id',
            array(
                'header'=> Mage::helper('catalog')->__('ID'),
                'width' => '50px',
                'type'  => 'number',
                'index' => 'question_id',
                'filter_index' => 'main_table.question_id',
            ));

        $this->addColumn('title', array(
            'header'    => $this->__('Question Title'),
            'index'     => 'title',
        ));

        $this->addColumn('visibility', array(
            'header'    => $this->__('Visibility'),
            'index'     => 'visibility',
            'type'      => 'options',
            'options'   => Mage::helper('amfaq')->getVisibilities(),
        ));

/*        $this->addColumn('customer_id', array(
            'header'    => $this->__('Sender'),
            'index'     => 'customer_id',
            'type'      => 'text',
        ));*/

        $this->addColumn('name', array(
            'header'    => $this->__('Sender Name'),
            'index'     => 'name',
        ));

        $this->addColumn('email', array(
            'header'    => $this->__('Sender Email'),
            'index'     => 'email',
        ));

        $this->addColumn('status', array(
            'header'    => $this->__('Status'),
            'index'     => 'status',
            'type'      => 'options',
            'options'   => array(
                'pending' => $this->__('Pending'),
                'answered' => $this->__('Answered')
            ),
        ));


        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn('store_ids', array(
                'header'        => Mage::helper('cms')->__('Store View'),
                'index'         => 'store_ids',
                'type'          => 'store',
                'store_all'     => true,
                'store_view'    => true,
                'sortable'      => false,
                'filter_condition_callback'
                => array($this, '_filterStoreCondition'),
            ));
        }

        $this->addColumn('action',
            array(
                'header'    =>  $this->__('Action'),
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => $this->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    ),
                    array(
                        'caption'   => $this->__('Delete'),
                        'url'       => array('base'=> '*/*/delete'),
                        'field'     => 'id',
                        'confirm'  => Mage::helper('catalog')->__('Are you sure?')
                    ),
                ),
                'filter'    => false,
                'sortable'  => false,
                'is_system' => true,
                'width'	    => '200px',
            ));

        return $this;
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('main_table.question_id');
        $this->getMassactionBlock()->setFormFieldName('question');

        $this->getMassactionBlock()->addItem('delete', array(
            'label'=> Mage::helper('catalog')->__('Delete'),
            'url'  => $this->getUrl('*/*/massDelete'),
            'confirm' => Mage::helper('catalog')->__('Are you sure?')
        ));

        return $this;
    }

    protected function _filterStoreCondition($collection, $column)
    {
        if (!$value = $column->getFilter()->getValue()) {
            return;
        }

        $this->getCollection()->addStoreFilter($value);
    }

    protected function _afterLoadCollection()
    {
        $this->getCollection()->walk('afterLoad');

        parent::_afterLoadCollection();
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array(
                'id'=>$row->getId())
        );
    }
}