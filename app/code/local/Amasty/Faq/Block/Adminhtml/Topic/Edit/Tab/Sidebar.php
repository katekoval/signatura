<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Block_Adminhtml_Topic_Edit_Tab_Sidebar extends Mage_Adminhtml_Block_Widget_Form
{

    protected function _prepareForm()
    {
        $model = Mage::registry('current_topic');

        $form = new Varien_Data_Form(array(
            'id' => 'edit_form',
            'action' => $this->getData('action'),
            'method' => 'post'
        ));


        $fieldset = $form->addFieldset('sidebar_fieldset',
            array('legend' => $this->__('Sidebar'))
        );

        $fieldset->addField('sidebar', 'select', array(
            'name'  => 'sidebar',
            'label' => $this->__('Show in Sidebar'),
            'title' => $this->__('Show in Sidebar'),
            'values' => array(
                0 => $this->__('No'),
                1 => $this->__('Yes'),
            ),
        ));

        $fieldset->addField('sidebar_position', 'text', array(
            'name'  => 'sidebar_position',
            'label' => $this->__('Position'),
            'title' => $this->__('Position'),
        ));

        $form->addValues($model->getData());

        $this->setForm($form);
        return parent::_prepareForm();
    }
}
