<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Block_Buttons extends Mage_Core_Block_Template
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('amasty/amfaq/buttons.phtml');
    }

    public function getName()
    {
        return $this->escapeHtml($this->helper('contacts')->getUserName());
    }

    public function getEmail()
    {
        return $this->escapeHtml($this->helper('contacts')->getUserEmail());
    }

    public function getCaptcha()
    {
        $captcha = $this->getLayout()
            ->createBlock('captcha/captcha')
            ->setFormId('amfaq_ask_form')
            ->setImgWidth(230)
            ->setImgHeight(50)
        ;

        return $captcha;
    }
}
