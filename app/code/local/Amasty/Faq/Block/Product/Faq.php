<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Faq
 */

class Amasty_Faq_Block_Product_Faq extends Mage_Core_Block_Template
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('amasty/amfaq/product_faq.phtml');
    }

    protected function _prepareLayout()
    {
        if (Mage::getStoreConfig('amfaq/product_page/ask_link')
            && !Mage::registry('amfaq_link_added')
            && $alertUrlsBlock = $this->getLayout()->getBlock('alert.urls')
        ) {
            $link = $this->_getLinkBlock();
            $alertUrlsBlock->append($link);
            Mage::register('amfaq_link_added', 1, true);
        }

        if (!Mage::registry('amfaq_popup_added')
            && $beforeBodyEndBlock = $this->getLayout()->getBlock('before_body_end')
        ) {
            $popup = $this->getLayout()
                ->createBlock('core/template', 'amfaq.popup')
                ->setTemplate('amasty/amfaq/popup.phtml');
            $beforeBodyEndBlock->append($popup);
            Mage::register('amfaq_popup_added', 1, true);
        }

        return parent::_prepareLayout();
    }

    public function getQuestions()
    {
        static $result = null;

        if (!$result) {
            $resource = Mage::getSingleton('core/resource');
            $product = Mage::registry('current_product') ?: Mage::registry('product');
            $customer = Mage::getSingleton('customer/session')->getCustomer()->getId();

            $questionCollection = Mage::getModel('amfaq/question')->getCollection();
            $questionCollection
                ->getSelect()
                ->joinLeft(
                    array('question_product' => $resource->getTableName('amfaq/question_product')),
                    'question_product.question_id = main_table.question_id',
                    array('product_id')
                )
                ->where('status != ?', 'pending')
                ->where('product_id = ?', +$product->getId())
                ->group('main_table.question_id')
                ->order('position');

            if (!Mage::app()->isSingleStoreMode()) {
                $store = array(0, Mage::app()->getStore()->getId());

                $questionCollection->addFilter('store_table.store_id', array('in' => $store), 'public');
                $questionCollection->getSelect()
                    ->join(
                        array('store_table' => $resource->getTableName('amfaq/question_store')),
                        'main_table.question_id = store_table.question_id'
                    );
            }

            $visibilities = array('public');

            if ($customer) {
                $visibilities[] = 'auth';
            }
            $questionCollection->addFilter('visibility', array('in' => $visibilities), 'public');

            $result = $questionCollection->load();
        }
        return $result;
    }

    protected function _getLinkBlock()
    {
        $text = $this->__('Ask a question');

        $questions = $this->getQuestions();
        if (sizeof($questions) > 0) {
            $first = $questions->getFirstItem();
            $first->afterLoad(); //load product ids
            $text .= ' ('.sizeof($questions).')';
        }

        $link = $this->getLayout()->createBlock('core/text_tag')
            ->setTagName('a')
            ->setTagParams(array(
                'href'  => '#',
                'class' => 'amfaq-ask-link',
            ))
            ->setTagContents($text);

        return $link;
    }

    public function getAskLink()
    {
        Mage::register('amfaq_link_added', 1, true);
        $link = $this->_getLinkBlock();
        return $link->toHtml();
    }
}
