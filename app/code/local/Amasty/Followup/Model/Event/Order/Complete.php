<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Followup
 */

class Amasty_Followup_Model_Event_Order_Complete extends Amasty_Followup_Model_Event_Order_Status
{
    protected $_statusKey = 'complete';
}