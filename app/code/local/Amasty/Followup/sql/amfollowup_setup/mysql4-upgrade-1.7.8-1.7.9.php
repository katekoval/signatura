<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Followup
 */


/* @var $installer Mage_Core_Model_Resource_Setup */
$installer = $this;

$this->startSetup();

$this->run("
      alter table `{$this->getTable('amfollowup/history')}`
      add column `coupon_expiration_date` datetime DEFAULT NULL after coupon_id;
");

$this->endSetup();