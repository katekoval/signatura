<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Followup
 */


$this->startSetup();

$this->run("
    alter table `{$this->getTable('amfollowup/history')}`
    change column status `status` ENUM('pending', 'processing', 'sent', 'cancel', 'no_crossell');
");

$this->endSetup();
