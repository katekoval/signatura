<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Followup
 */


/**
 * Class Amasty_Followup_Block_Adminhtml_Notifications
 */
class Amasty_Followup_Block_Adminhtml_Notifications extends Mage_Adminhtml_Block_Template
{
    /**
     * @return bool
     */
    public function canShow()
    {
        return Mage::getStoreConfig("amfollowup/test/safe_mode");;

    }

    /**
     * @return string
     */
    public function getSecurityAdminUrl()
    {
        return Mage::helper("adminhtml")->getUrl('adminhtml/system_config/edit/section/amfollowup');
    }
}
