<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Segments
 */

$this->startSetup();

$this->run("
    ALTER TABLE `{$this->getTable('amsegments/customer')}`
    DROP FOREIGN KEY `FK_AMSEGMENTS_CUSTOMER_ENTITY_CUSTOMER_ID_CUSTOMER_ENTITY_ID`;
");

$this->endSetup();
