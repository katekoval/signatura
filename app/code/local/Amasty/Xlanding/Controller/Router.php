<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Xlanding
 */


class Amasty_Xlanding_Controller_Router extends Mage_Core_Controller_Varien_Router_Abstract
{
    /**
     * Initialize Controller Router
     *
     * @param Varien_Event_Observer $observer
     */
    public function initControllerRouters($observer)
    {
        /* @var $front Mage_Core_Controller_Varien_Front */
        $front = $observer->getEvent()->getFront();
        $front->addRouter('amlanding', $this);
    }

    /**
     * Validate and Match Cms Page and modify request
     *
     * @param Zend_Controller_Request_Http $request
     * @return bool
     */
    public function match(Zend_Controller_Request_Http $request)
    {
        if (Mage::app()->getStore()->isAdmin()) {
            return false;
        }

        $page = Mage::getModel('amlanding/page');
        $pathInfo = $request->getPathInfo();
        $result = false;
        $allPageIdentifiers = array();
        foreach ($page->getCollection()->getItems() as $item) {
            $allPageIdentifiers[] = $item->getIdentifier();
        }

        usort($allPageIdentifiers, array($this, 'sortIdentifiers'));

        foreach ($allPageIdentifiers as $identifier) {
            if ($this->checkLandingUrl($pathInfo, $identifier)) {
                $result = $this->checkLandingPage($pathInfo, $identifier, $page, $request);
                break;
            }
        }

        return $result;
    }

    /**
     * @param $firstItem
     * @param $secondItem
     * @return int
     */
    protected function sortIdentifiers($firstItem, $secondItem)
    {
        return substr_count($firstItem, '/') > substr_count($secondItem, '/') ? -1 : 1;
    }

    /**
     * @param $pathInfo
     * @param $identifier
     * @return bool
     */
    protected function checkLandingUrl($pathInfo, $identifier)
    {
        return strpos("/" . $pathInfo . "/", "/" . $identifier. "/") !== false
            || strpos("/" . $pathInfo . ".", "/" . $identifier . ".") !== false;
    }

    /**
     * @param $pathInfo
     * @param $identifier
     * @param $page
     * @param $request
     * @return bool
     */
    protected function checkLandingPage($pathInfo, $identifier, $page, $request)
    {
        // remove suffix if any
        $suffix = Mage::helper('amlanding/url')->getSuffix();
        if ($suffix && '/' != $suffix) {
            $pathInfo = str_replace($suffix, '', $pathInfo);
        }

        $pathInfo = preg_replace(
            '#'.preg_quote($identifier, '/').'#',
            '',
            $pathInfo,
            1
        );

        $pathInfo = array($identifier, trim($pathInfo, '/ '));
        $params = (isset($pathInfo[1]) ? $pathInfo[1] : '');
        $pageId = $page->checkIdentifier($identifier, Mage::app()->getStore()->getId());
        if (!$pageId) {
            return false;
        }

        if ($shortParsed = Mage::registry('amshopby_short_parsed')) {
            if (is_array($shortParsed) && isset($shortParsed['query'])) {
                $request->setQuery($shortParsed['query']);
            }
        } else {
            $params = trim($params, '/ ');
            if ($params) {
                $params = explode('/', $params);
                Mage::register('amshopby_current_params', $params);
                if (Mage::helper('core')->isModuleEnabled('Amasty_Shopby')) {
                    $parsed = Mage::helper('amshopby/url')->saveParams($request);
                    if (!$parsed) {
                        return false;
                    }
                }
            }
        }

        $request->setModuleName('amlanding')
            ->setControllerName('page')
            ->setActionName('view')
            ->setParam('page_id', $pageId)
            ->setParam('am_landing', $identifier);

        return true;
    }
}
