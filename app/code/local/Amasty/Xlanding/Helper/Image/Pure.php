<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Xlanding
 */


if (Mage::helper('core')->isModuleEnabled('Amasty_Shopby')) {
    $autoloader = Varien_Autoload::instance();
    $autoloader->autoload('Amasty_Xlanding_Helper_Image_Shopby');
} else {
    class Amasty_Xlanding_Helper_Image_Pure extends Mage_Catalog_Helper_Image {}
}
