<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Xlanding
 */


class Amasty_Xlanding_Helper_Image extends Amasty_Xlanding_Helper_Image_Pure
{
    /** @var  Amasty_Xlanding_Model_Page|null */
    protected $page;

    /** @var array|null */
    protected $conditions;

    /** @var string|null */
    protected $aggregator;

    /**
     * @param Mage_Catalog_Model_Product $product
     * @param string $attributeName
     * @param mixed $imageFile
     * @return Mage_Catalog_Helper_Image
     */
    public function init(Mage_Catalog_Model_Product $product, $attributeName, $imageFile = null)
    {
        $this->page = Mage::registry('amlanding_page');
        if ($this->page) {
            $this->aggregator = $this->page->getConditions()->getAggregator();
        }
        return parent::init($product, $attributeName, $imageFile);
    }

    /**
     * Use Amasty_Shopby methods if is possible
     * @return bool
     */
    protected function isShopbySubclass()
    {
        return is_subclass_of($this, 'Amasty_Shopby_Helper_Image');
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return Mage_Catalog_Helper_Image
     */
    public function setProduct($product)
    {
        if (!$this->page || !$product->isConfigurable() || !$product->isSaleable()) {
            return parent::setProduct($product);
        }

        $child = null;

        //duplicate a part of parent class functionality
        if($this->isShopbySubclass()) {
            if (method_exists($this, 'getRequestConfigurableMap')) {
                if ($this->getRequestConfigurableMap()) {
                    $child = $this->getMatchingSimpleProduct($this->getChildrenCollection($product));
                    if (!$child) {
                        // If simple options haven't an image, try to receive it from mapped.
                        $child = $this->getMatchingSimpleProductByMappedOptions($this->getChildrenCollection($product));
                    }
                }
            } else {
                Mage::throwException(Mage::helper('amlanding')->__('Please update Amasty Improved Layered Navigation'));
            }
        }

        if (!$child) {
            $child = $this->getMatchingSimpleProductByPage($product);
        }

        if ($child) {
            $product = $child;
        }

        return Mage_Catalog_Helper_Image::setProduct($product);
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return Mage_Catalog_Model_Product|null
     */
    protected function getMatchingSimpleProductByPage($product)
    {
        $conditions = $this->getConditions();
        if (!$conditions) {
            return null;
        }
        $childCollection = $this->getChildrenCollection($product);

        if ('any' == $this->aggregator) {
            $childCollection->addFieldToFilter($conditions);
        } else {
            foreach ($conditions as $condition) {
                $childCollection->addFieldToFilter(array($condition));
            }
        }

        return $this->getImageFromChildrenCollection($childCollection);
    }

    /**
     * @return array
     */
    protected function getConditions()
    {
        if (null === $this->conditions) {
            $this->conditions = array();
            $topLvlOptions = $this->page->getConditions()->getValueOption();
            $optionKeys = is_array($topLvlOptions) ? array_keys($topLvlOptions) : array();
            if (!is_array($topLvlOptions) || array_shift($optionKeys) != 1) {
                return $this->conditions;
            }

            $swatchAttributes = explode(
                ",", trim(Mage::getStoreConfig('amlanding/advanced/configurable_images'))
            );
            if (!$swatchAttributes) {
                return $this->conditions;
            }
            $allowedOperators = array('==' => 'eq', '()' => 'in');

            foreach ($this->page->getConditions()->getConditions() as $condition) {
                if (in_array($condition->getOperator(), array_keys($allowedOperators))
                    && in_array($condition->getAttribute(), $swatchAttributes)
                ) {
                    $this->conditions[] = array(
                        'attribute' => $condition->getAttribute(),
                        $allowedOperators[$condition->getOperator()] => $condition->getValue()
                    );
                }
            }
        }

        return $this->conditions;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @return Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Type_Configurable_Product_Collection
     */
    protected function getChildrenCollection($product)
    {
        if ($this->isShopbySubclass()) {
            return parent::getChildrenCollection($product);
        }

        $productTypeIns = $product->getTypeInstance(true);
        return $productTypeIns->getUsedProductCollection($product)
            ->addFieldToFilter('small_image', array('notnull' => true))
            ->addFieldToFilter('small_image', array('neq' => 'no_selection'));
    }

    /**
     * @param Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Type_Configurable_Product_Collection $childrenCollection
     * @return Mage_Catalog_Model_Product|null
     */
    protected function getImageFromChildrenCollection($childrenCollection)
    {
        if ($this->isShopbySubclass()) {
            return parent::getImageFromChildrenCollection($childrenCollection);
        }

        if ($childrenCollection->getSize()) {
            return $childrenCollection->getFirstItem();
        }

        return null;
    }
}
