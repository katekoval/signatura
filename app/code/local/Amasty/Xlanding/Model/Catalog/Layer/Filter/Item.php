<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Xlanding
 */ 
class Amasty_Xlanding_Model_Catalog_Layer_Filter_Item extends Amasty_Xlanding_Model_Catalog_Layer_Filter_Item_Pure
{
    const FIRST_PAGE = 1;

    public function getUrl($urlBuilder = null)
    {
        if ($key = Mage::app()->getRequest()->getParam('am_landing')) {
            if (!Mage::helper('amlanding')->seoLinksActive()) {
                return Mage::helper('amlanding/url')->getLandingUrl(
                    array(
                        'p' => self::FIRST_PAGE,
                        $this->getFilter()->getRequestVar() => $this->getValue()
                    )
                );
            }
        }
        return parent::getUrl($urlBuilder); //compatibility with Amasty parent class
    }

    public function getRemoveUrl()
    {
        if ($key = Mage::app()->getRequest()->getParam('am_landing')) {
            $exclude = array(
                $this->getFilter()->getRequestVar()
            );
            if (!Mage::helper('amlanding')->seoLinksActive()) {
                return Mage::helper('amlanding/url')->getLandingUrl(array('p' => self::FIRST_PAGE), $exclude);
            }
        }
        return parent::getRemoveUrl();                
    }

}