<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Xlanding
 */


class Amasty_Xlanding_Model_Page extends Mage_Rule_Model_Rule
{
    /**
     * Page's Statuses
     */
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    const ON_SALE_BY_RULE_YES = 2;
    const ON_SALE_BY_RULE_NO = 1;

    const IS_NEW_YES = 2;
    const IS_NEW_NO = 1;

    const IS_INSTOCK_YES = 2;

    protected $_attributeCache;
    protected $_cacheTag = 'amlanding_page';

    const FILTER_CONDITION_AND = 1;
    const FILTER_CONDITION_OR = 0;

    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('amlanding/page');
    }

    /**
     * Check if page identifier exist for specific store
     * return page id if page exists
     *
     * @param string $identifier
     * @param int $storeId
     * @return int
     */
    public function checkIdentifier($identifier, $storeId)
    {
        return $this->_getResource()->checkIdentifier($identifier, $storeId);
    }

    protected function _digCategories(array $ids)
    {
        $allIds = $ids;

        do {
            /** @var Mage_Catalog_Model_Resource_Category_Collection $categories */
            $categories = Mage::getModel('catalog/category')->getCollection();
            $categories->addAttributeToFilter('parent_id', array('in' => $ids));
            $ids = $categories->getAllIds();
            $allIds = array_merge($allIds, $ids);
        } while ($ids);

        return $allIds;
    }

    /**
     * Get catalog layer model
     *
     * @return Mage_Catalog_Model_Layer
     */
    private function getLayer()
    {
        $layer = Mage::registry('current_layer');
        if (!$layer) {
            $layer = Mage::getSingleton('catalog/layer');
            Mage::register('current_layer', $layer);
        }

        return $layer;
    }

    public function applyPageRules()
    {
        $deepCategories = $this->_digCategories(explode(",", $this->getCategory()));
    	$categoryId = Mage::app()->getStore()->getRootCategoryId();
        if(count($deepCategories) == 1) {
            $categoryId = array_shift($deepCategories);
        }

        $category = Mage::getModel('catalog/category')->load($categoryId);
        $layer = $this->getLayer();
        $layer->setCurrentCategory($category);
        $collection = $layer->getProductCollection();

        $this->prepareCollection($collection);

        if (count($deepCategories) > 0) {
            $join = Mage::getSingleton('core/resource')->getConnection('core_read')
                ->quoteInto("amlanding_cp.product_id = e.entity_id AND amlanding_cp.category_id IN (?)", $deepCategories);
            $collection->getSelect()->joinInner(
                array('amlanding_cp' =>
                    Mage::getResourceModel('catalog/product')->getTable('catalog/category_product')
                ),
                $join,
                array()
            );

            /*  Use this category limitation instead if there is an error related with product ids duplication
            $table = $collection->getTable('catalog/category_product');
            $collection->getSelect()
                ->where('e.entity_id IN(SELECT product_id FROM `' . $table . '` WHERE category_id IN (?))', $deepCategories);
            */
        }

        if (Mage::app()->getRequest()->getParam('xlanding_debug_page')) {
            Mage::helper('ambase/utils')->_echo($layer->getProductCollection()->getSelect());
        }
    }
    
    public function prepareCollection($collection){
        $collection->distinct(true);
                
        $collection->addStoreFilter();
        
        $this->applyAttributesFilter($collection);

        $this->applyStockStatusFilter($collection);

        $this->applyNewCriteriaFilter($collection);

        $this->applyIsSaleByRuleFilter($collection);
    }

    function applyNewCriteriaFilter(&$collection){
        $newCriteriaDays = Mage::getStoreConfig('amlanding/advanced/new_criteria');
        if ($isNew = $this->getIsNew()) {
            if ($isNew == self::IS_NEW_YES) {
                if ($newCriteriaDays) {
                    $threshold = Mage::getStoreConfig('amlanding/advanced/new_threshold');
                    $collection->getSelect()->where('datediff(now(), created_at) < ?', $threshold);
                } else {
                    $todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATE_INTERNAL_FORMAT);
                    
                    $collection
                            ->addAttributeToFilter('news_from_date', array('date'=>true, 'to'=> $todayDate))
                            ->addAttributeToFilter(array(
                                    array(
                                        'attribute'=>'news_to_date', 
                                        'date'=>true, 
                                        'from'=>$todayDate
                                    ), 
                                    array(
                                        'attribute'=>'news_to_date', 
                                        'is' => new Zend_Db_Expr('null')
                                    )
                            ),'','left');
                }
            }

            if ($isNew == self::IS_NEW_NO) {
                if ($newCriteriaDays) {
                    $threshold = Mage::getStoreConfig('amlanding/advanced/new_threshold');
                    $collection->getSelect()->where('datediff(now(), created_at) > ?', $threshold);
                } else {
                    $todayDate  = Mage::app()->getLocale()->date()->toString(Varien_Date::DATE_INTERNAL_FORMAT);
                    
                    $collection
                            ->addAttributeToFilter(array(
                                    array(
                                        'attribute'=>'news_from_date', 
                                        'date'=>true, 
                                        'gt'=>$todayDate
                                    ), 
                                    array(
                                        'attribute'=>'news_from_date', 
                                        'is' => new Zend_Db_Expr('null')
                                    )
                            ), '','left')
                            ->addAttributeToFilter(array(
                                    array(
                                        'attribute'=>'news_to_date', 
                                        'date'=>true, 
                                        'lt'=>$todayDate
                                    ), 
                                    array(
                                        'attribute'=>'news_to_date', 
                                        'is' => new Zend_Db_Expr('null')
                                    )
                            ),'','left');
                }
            }
        }
    }

    function applyIsSaleByRuleFilter(&$collection){
        $sale = $this->getIsSaleByRule();
        if (!$sale) {
            return;
        }

        if ($sale == self::ON_SALE_BY_RULE_YES) {
            $operator = ' < ';
        } elseif ($sale == self::ON_SALE_BY_RULE_NO) {
            $operator = ' >= ';
        }

        if (isset($operator)) {
            /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
            $collection->addFinalPrice();
            $whereCondition =  ' price_index.final_price ' . $operator . ' price_index.price ';
            if (Mage::getStoreConfig('amlanding/advanced/onsale_with_childs') && $sale == self::ON_SALE_BY_RULE_YES) {
                $this->applyOnSaleParents($collection, $whereCondition);
            } else {
                $collection->getSelect()->where($whereCondition);
            }
        }
    }

    /**
     * @param $collection
     * @param $condition
     * @return $this
     */
    protected function applyOnSaleParents($collection, $condition)
    {
        $bundleAlias = 'amlanding_bundle';
        $groupedAlias = 'amlanding_grouped';

        /** Select all children ids (from bundle OR grouped) OR simple product ids
         *  @todo also include parents
         */
        $productIds = "IFNULL(IFNULL($bundleAlias.product_id,$groupedAlias.linked_product_id),e.entity_id)";

        $onSaleIdsSelect = $this->getOnSaleIdsSelect($collection, $condition);

        /** @var Mage_Catalog_Model_Resource_Product_Collection $collection */
        $collection->getSelect()
            ->joinLeft(
                array($bundleAlias => $collection->getTable('bundle/selection')),
                "$bundleAlias.parent_product_id = e.entity_id",
                array()
            )->joinLeft(
                array($groupedAlias => $collection->getTable('catalog/product_link')),
                "$groupedAlias.product_id = e.entity_id AND $groupedAlias.link_type_id = "
                . Mage_Catalog_Model_Product_Link::LINK_TYPE_GROUPED,
                array()
            )->where("$productIds IN (?)", $onSaleIdsSelect)
            ->distinct(true);

        return $this;
    }

    /**
     * Return select that fetches on_sale product ids
     *
     * @param $collection
     * @param $condition
     * @return Zend_Db_Select
     */
    protected function getOnSaleIdsSelect($collection, $condition)
    {
        $select = clone $collection->getSelect();
        $select->where($condition);

        $select->reset(Zend_Db_Select::ORDER);
        $select->reset(Zend_Db_Select::LIMIT_COUNT);
        $select->reset(Zend_Db_Select::LIMIT_OFFSET);
        $select->reset(Zend_Db_Select::COLUMNS);
        $select->columns('e.' . $collection->getEntity()->getIdFieldName());
        $select->resetJoinLeft();

        $from = $select->getPart(Zend_Db_Select::FROM);
        $from = array_intersect_key($from, array('e' => null, 'price_index' => null)); //unset visibility and category conditions for children (cat_index and perm)

        $select->setPart(Zend_Db_Select::FROM, $from);

        return $select;
    }

    function applyStockStatusFilter(&$collection){
        if ($stock = $this->getStockStatus()){
            if ($stock == self::IS_INSTOCK_YES) {
                Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);
            }
        }
    }
        

  	public function massChangeStatus($ids, $status)
    {
        return $this->getResource()->massChangeStatus($ids, $status);
    }
    
    public function getUploadPath()
    {
        return  'amasty' . DS .'amxlanding';
    }
    
    public function getLayoutFileUrl(){
        return Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA) . DS . $this->getLayoutFile();
    }
    
    protected function _beforeSave(){
        
        if(isset($_FILES['layout_file']) &&
                $_FILES['layout_file']['name'] != '') {
        
            try{
                $uploader = new Varien_File_Uploader('layout_file');
                $uploader->setAllowedExtensions(array('jpg','jpeg','gif','png'));
                $uploader->setAllowRenameFiles(true);
                $uploader->setFilesDispersion(true);

                $this->setLayoutFileName($uploader->getCorrectFileName($_FILES['layout_file']['name']));
                $ext = pathinfo($_FILES['layout_file']['name'], PATHINFO_EXTENSION);

                $result = $uploader->save(
                    Mage::getBaseDir('media') . DS . $this->getUploadPath(), uniqid().".".$ext
                );

                $this->setLayoutFile($this->getUploadPath() . $result['file']); 
            } catch (Exception $e){
                Mage::throwException($this->__('Invalid image format'));
            }
            
        } else {
            
            $layoutFile = $this->getLayoutFile();
            
            if (isset($layoutFile['delete']) &&
                    $layoutFile['delete'] == 1
            ) {
                $this->setLayoutFile(NULL);
            } else {
                $this->setLayoutFile($this->layout_file["value"]);
            }
            
        }
        return parent::_beforeSave(); 
    }
    
    public function getAvailableSortBy()
    {
        $available = $this->getData('available_sort_by');
        if (empty($available)) {
            return array();
        }
        if ($available && !is_array($available)) {
            $available = explode(',', $available);
        }
        return $available;
    }

    public function getAvailableSortByOptions() {
        $availableSortBy = array();
        $defaultSortBy   = Mage::getSingleton('catalog/config')
            ->getAttributeUsedForSortByArray();
        if ($this->getAvailableSortBy()) {
            foreach ($this->getAvailableSortBy() as $sortBy) {
                if (isset($defaultSortBy[$sortBy])) {
                    $availableSortBy[$sortBy] = $defaultSortBy[$sortBy];
                }
            }
        }

        if (!$availableSortBy) {
            $availableSortBy = $defaultSortBy;
        }

        return $availableSortBy;
    }

    public function getDefaultSortBy() {
        if (!$sortBy = $this->getData('default_sort_by')) {
            $sortBy = Mage::getSingleton('catalog/config')
                ->getProductListDefaultSortBy();
        }
        
        $available = $this->getAvailableSortByOptions();
        if (!isset($available[$sortBy])) {
            $sortBy = array_keys($available);
            $sortBy = $sortBy[0];
        }

        return $sortBy;
    }
    
    public function getActionsInstance()
    {
        return Mage::getModel('rule/action_collection');
    }
    
    public function getConditionsInstance()
    {
        return Mage::getModel('amlanding/filter_condition_combine');
    }
    
    public function applyAttributesFilter($productCollection){
        $conditions = $this->getConditions();
        
        if ($conditions instanceof Amasty_Xlanding_Model_Filter_Condition_Combine){
            $this->getConditions()->collectValidatedAttributes($productCollection);
            $condition = $this->getConditions()->collectConditionSql($productCollection);  
            
            if (!empty($condition))
                $productCollection->getSelect()->where($condition);
        }
    }
    
    public function getAttributesAsArray()
    {
    	$array = array();
    	$attributes = $this->getData('attributes');
    	if (!empty($attributes)) {
    		$array = (array) Mage::helper('amlanding')->unserialize($attributes);
    	}
    	return $array;
    }
    
    function getModifedAttributes(){
        $attributes = $this->getAttributesAsArray();
        $filters = array();
        if ($attributes){
            foreach ($attributes as $value) {
                $filter = $this->getAttributeFilter($value);
                
                if ($filter) {
                    
                    if ($value['cond'] == 'like' && count($filter['like']) > 1){
                        
                        foreach($filter['like'] as $like){
                            $filters[] = array_merge($filter, array(
                                "like" => array($like)
                            ));
                        }
                        
                    } else if (($value['cond'] == 'in' || $value['cond'] == 'nin') 
                            && $filter['type'] != 'text') {
                        
                        $found = false;
                        foreach ($filters as $ind => $exist){
                            if ($exist['attribute'] == $filter['attribute']){
                                
                                $filters[$ind][$value['cond']][] = $value['value'];
                                
                                $found = true;
                                break;
                            }
                        }
                        if (!$found){
                            $filters[] = array(
                                'attribute' => $filter['attribute'],
                                'cond' => $value['cond'],
                                $value['cond'] => array(
                                    $value['value']
                                )
                            );
                        }
                    } else {
                        $filters[] = $filter;
                    }
                }
                
            }
        }
        
        foreach($filters as &$filter){
            if ($filter['cond'] == 'like' && strpos($filter['like'], "%") === FALSE){
                $filter['like'] = "%" . $filter['like'] . "%";
            }
        }
        
        return $filters;
    }
    
    function getAttributeFilter($param)
    {
    	$code  = $param['code'];
    	$value = $this->_prepareValue($param);
    	$cond  = $param['cond'];
    	
    	if (!isset($this->_attributeCache[$code])) {
    		$attribute = Mage::getModel('catalog/product')->getResource()->getAttribute($code);
    		$this->_attributeCache[$code] = $attribute;
    	}

    	$attribute = $this->_attributeCache[$code];
    	
       $code = $attribute->getAttributeCode();
                
        $ret = array(
            "attribute" => $code,
            'cond' => $cond,
            'type' => $attribute->getBackendType(),
            $cond => $value
        ); 
       return $ret; 
    }
    
    protected function _prepareValue($param){
        $value = $param['value'];
    	$cond  = $param['cond'];
        
        if ($cond == "like"){
            if (is_array($value)){
                foreach($value as $index => $el){
                    if (strpos($el, "%") === FALSE){
                        $value[$index] = "%" . $el . "%";
                    }
                    
                }
            }
        }
        
        return $value;
    }

    public function isHideConditionFilters()
    {
        $isHide = $this->getIsHideConditionFilters();
        $isHide = is_null($isHide) ? true : (bool)$isHide;
        return $isHide;
    }
    
}
