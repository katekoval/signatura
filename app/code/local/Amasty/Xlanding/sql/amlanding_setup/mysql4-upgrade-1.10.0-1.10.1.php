<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_Xlanding
 */

/* @var $this Mage_Core_Model_Resource_Setup */
$this->startSetup();
$table = $this->getTable('amlanding/page');
if ($this->getConnection()->tableColumnExists($table, 'category')) {
    $this->run(
      "ALTER TABLE `$table`
      CHANGE COLUMN `category` `category` TEXT DEFAULT NULL;"
    );
}
$this->endSetup();
