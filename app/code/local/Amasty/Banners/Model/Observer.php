<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Banners
 */
class Amasty_Banners_Model_Observer
{
    protected $_firstTime = true;

    /**
     * Append rule product attributes to select by quote item collection
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function addProductAttributes(Varien_Event_Observer $observer)
    {
        // @var Varien_Object
        $attributesTransfer = $observer->getEvent()->getAttributes();

        $attributes = Mage::getResourceModel('ambanners/rule')->getAttributes();
        
        $result = array();
        foreach ($attributes as $code) {
            $result[$code] = true;
        }
        $attributesTransfer->addData($result);
        
        return $this;
    }

    public function coreBlockAbstractToHtmlAfter(Varien_Event_Observer $observer)
    {
        $block = $observer->getBlock();
        $isScroll = Mage::app()->getRequest()->getParam('is_scroll');

        if($actionController = Mage::app()->getFrontController()->getAction()) {
            $action = $actionController->getFullActionName();

            // AMBANNER BREADCRUMB POSITION
            if ($block->getNameInLayout() === 'breadcrumbs' && $action !== 'onestepcheckout_index_index' && $action !== 'checkout_cart_index') {
                //add banner to media block
                $transport = $observer->getEvent()->getTransport();
                $html = $transport->getHtml();
                $transport->setHtml($block->getChildHtml('ambanners.breadcrumbs') . $html);
            }

            // AMBANNER CHECKOUT REMOVE EXTREME CALLOUT
            if ($block->getNameInLayout() === 'ambanners.head' && ($action === 'onestepcheckout_index_index' || $action === 'checkout_cart_index'))
            {
                $transport = $observer->getEvent()->getTransport();
                $transport->setHtml(null);
            }
        }

        // AMBANNER MEDIA POSITION
        if ($block->getNameInLayout() === 'product.info.media') {
            //add banner to media block
            $transport = $observer->getEvent()->getTransport();
            $html = $transport->getHtml();
            $transport->setHtml($block->getChildHtml('ambanners.products.above.product.picture') . $html);
        }

        // AMBANNER CATEGORY PRODUCTS (BRAND) POSITION
        if ($block->getNameInLayout() === 'category.products' && $block instanceof Amasty_Brands_Block_Brand) {
            //add banner to category.products block
            $transport = $observer->getEvent()->getTransport();
            $html = $transport->getHtml();
            $transport->setHtml($block->getChildHtml('ambanners.brands.top') . $html);

            //add banner to category_top block
            $transport = $observer->getEvent()->getTransport();
            $html = $transport->getHtml();
            $transport->setHtml($html . $block->getChildHtml('ambanners.brands.bottom'));
        }

        // AMBANNER PRODUCT INJECT POSITION
        if ($block instanceof Mage_Catalog_Block_Product_List
            && $this->_firstTime
            && !$isScroll
        ) {
            $position = ($block->getToolbarBlockName() == "ambrands_product_list_toolbar") ? 29 : 14;
            $this->_firstTime = false;
            $transport = $observer->getTransport();
            if (is_object($transport)) {
                $html = $transport->getHtml();

                /** @var Amasty_Banners_Block_Inject $injectBlock */
                $injectBlock = Mage::getBlockSingleton('ambanners/inject');
                if  (!$injectBlock) {
                    return $this;
                }
                $injectBlock->setTemplate('ambanners/inject.phtml');
                $injectBlock->setParentBlock($block);
                $injectBlock->setPosition($position);
                $injectBlockHtml = $injectBlock->toHtml();

                /** @var Amasty_Banners_Block_Container $containerBlock */
                $containerBlock = Mage::getBlockSingleton('ambanners/container');
                $containerBlock->setPosition($position);
                $containerBlock->setTemplate('ambanners/container.phtml');
                $containerBlock->append($injectBlock);

                $containerBlockHtml = $containerBlock->toHtml();
                $isFirstSpan = '<span id="ambanner_first_time" style="display: none">1</span>';
                $html = $html . $isFirstSpan . $containerBlockHtml . $injectBlockHtml;

                $transport->setHtml($html);
            }
        }

        return $this;
    }
    
}
