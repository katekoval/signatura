<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Banners
 */

class Amasty_Banners_Model_Rule extends Mage_Rule_Model_Rule
{
    const POS_ABOVE_CART = 0;

    const POS_BELOW_CART = 15;

    const POS_SIDEBAR_RIGHT = 1;

    const POS_SIDEBAR_LEFT = 2;

    const POS_PROD_PAGE = 3;

    const POS_CATEGORY_PAGE = 4;

    const POS_CATEGORY_PAGE_BOTTOM = 5;

    const POS_PROD_PAGE_BOTTOM = 6;

    const POS_PROD_PAGE_BELOW_CART = 7;

    const POS_CHECKOUT_ABOVE_TOTAL = 8;

    const POS_CATALOG_SEARCH_TOP = 9;

    const POS_TOP_PAGE = 10;

    const POS_TOP_INDEX = 11;

    const POS_PROD_PAGE_RIGHT = 12;

    const POS_PROD_PAGE_LEFT = 13;

    const POS_AFTER_N_PRODUCT_ROW = 14;

    const BRAND_PAGE = 16;

    /*
     * Custom Firtal positions
     */

    const POS_PROD_PAGE_ABOVE_PRODUCT_IMAGE = 17; //Product page (Above product image)

    const POS_PROD_PAGE_ABOVE_BRANDBUNDLE = 18; //Product page (Above bundle offer block)

    const POS_CATEGORY_PAGE_BETWEEN_PRODUCTNAME_PRICE = 19; //Category page - listing (Between product name and price)

    const BRAND_PAGE_BOTTOM = 20; //Brand page bottom

    const BRAND_PAGE_BETWEEN_PRODUCTNAME_PRICE = 21; //Brand page - listing (Between product name and price)

    const POS_INDEX_TOP_LEFT = 22;

    const POS_INDEX_TOP_RIGHT = 23;

    const POS_INDEX_BELOW_TOP_LEFT = 24;

    const POS_INDEX_BELOW_TOP_RIGHT = 25;

    const POS_INDEX_BELOW_BOTTOM_LEFT = 26;

    const POS_INDEX_BELOW_BOTTOM_RIGHT = 27;

    const POS_ALL_EXTREME = 28;

    const POS_AFTER_N_BRAND_PRODUCT_ROW = 29;

    /*
     * Display Types
     */
    const TYPE_IMAGE = 'image';

    const TYPE_CMS = 'cms';

    const TYPE_HTML = 'html';

    const TYPE_PRODUCTS = 'products';

    /*
     * Products List Options
     */
    const SHOW_PRODUCTS_NO = 0;

    const SHOW_PRODUCTS_YES = 1;

    protected $_googleAnalyticsParams = array(
        'utm_source',
        'utm_medium',
        'utm_term',
        'utm_content',
        'utm_campaign'
    );

    public function _construct()
    {
        parent::_construct();
        $this->_init('ambanners/rule');
    }

    public function getConditionsInstance()
    {
        return Mage::getModel('ambanners/rule_condition_combine');
    }

    public function massChangeStatus($ids, $status)
    {
        return $this->getResource()->massChangeStatus($ids, $status);
    }

    /**
     * Initialize rule model data from array
     *
     * @param   array $rule
     *
     * @return  Mage_SalesRule_Model_Rule
     */

    public function loadPost(array $rule)
    {
        $arr = $this->_convertFlatToRecursive($rule);
        if (isset($arr['conditions'])) {
            $this->getConditions()->setConditions(array())->loadArray($arr['conditions'][1]);
        }

        return $this;
    }

    /**
     * Get array of attributes for product where banner should be shown
     *
     * @return array
     */
    public function getAttributesAsArray()
    {
        $array = array();
        $attributes = $this->getData('attributes');
        if (!empty($attributes)) {
            $array = Mage::helper('ambanners')->unserialize($attributes);
        }

        return $array;
    }

    public function match($rate)
    {
        return false;
    }


    protected function _afterSave()
    {
        //Saving attributes used in rule
        $ruleProductAttributes = array_merge(
            $this->_getUsedAttributes($this->getConditionsSerialized())
        );
        if (count($ruleProductAttributes)) {
            $this->getResource()->saveAttributes($this->getId(), $ruleProductAttributes);
        }

        return parent::_afterSave();
    }

    /**
     * Return all product attributes used on serialized action or condition
     *
     * @param string $serializedString
     *
     * @return array
     */
    protected function _getUsedAttributes($serializedString)
    {
        $result = array();

        $pattern = '~s:32:"salesrule/rule_condition_product";s:9:"attribute";s:\d+:"(.*?)"~s';
        $matches = array();
        if (preg_match_all($pattern, $serializedString, $matches)) {
            foreach ($matches[1] as $attributeCode) {
                $result[] = $attributeCode;
            }
        }

        return $result;
    }

    public function getProducts($ruleId)
    {
        return $this->getResource()->getProducts($ruleId);
    }

    public function assignProducts($productIds)
    {
        $this->getResource()->assignProducts($this->getId(), $productIds);

        return $this;
    }

    /**
     * build with with extra params
     */
    public function buildUrl()
    {
        $url = $this->getBannerLink();
        $params = $this->getGoogleParams();
        if (count($params) > 0) {
            $url .= (strpos($url, '?') ? '&' : '?') . implode('&', $params);
        }

        return $url;
    }

    /**
     * @return array
     */
    protected function getGoogleParams()
    {
        $params = array();
        foreach ($this->_googleAnalyticsParams as $param) {
            $val = $this->getData($param);
            if (!empty($val)) {
                $params[] = $param . '=' . $val;
            }
        }

        return $params;
    }

    /**
     * @param int $columnCount
     *
     * @return int
     */
    public function getAfterProductNum($columnCount)
    {
        $rowNum = $this->getAfterProductRow();

        $productPosition = $this->getAfterProduct();
        if ($productPosition > 0) {
            $productPosition = $productPosition - 1;
        }
        $afterProduct = ($rowNum * $columnCount - 1);
        $afterProduct = $afterProduct + $productPosition;

        return $afterProduct;
    }

    /**
     * @return int
     */
    public function getAfterProductRow()
    {
        return (int)$this->getData('after_n_product_row') > 0 ?
            (int)$this->getData('after_n_product_row') - 1 : 0;
    }
}
