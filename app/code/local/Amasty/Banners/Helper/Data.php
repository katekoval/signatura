<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Banners
 */ 
class Amasty_Banners_Helper_Data extends Mage_Core_Helper_Abstract
{
    const ERROR_MESSAGE = 'If there is the following text it means that Amasty_Base is not updated to the latest 
                             version.<p>In order to fix the error, please, download and install the latest version of 
                             the Amasty_Base, which is included in all our extensions.
                        <p>If some assistance is needed, please submit a support ticket with us at: 
                        <a href="https://amasty.com/contacts/" target="_blank">https://amasty.com/contacts/</a>';

    public function getAllGroups()
    {
        $customerGroups = Mage::getResourceModel('customer/group_collection')
            ->load()->toOptionArray();

        $found = false;
        foreach ($customerGroups as $group) {
            if ($group['value']==0) {
                $found = true;
            }
        }
        if (!$found) {
            array_unshift($customerGroups, array('value'=>0, 'label'=>Mage::helper('salesrule')->__('NOT LOGGED IN')));
        } 
        
        return $customerGroups;
    }

    public function getStatuses()
    {
        return array(
            '0' => $this->__('Inactive'),
            '1' => $this->__('Active'),
        );       
    }

    public function getPosition()
    {
        $a = array(
            Amasty_Banners_Model_Rule::POS_ABOVE_CART => $this->__('Above cart'),
            Amasty_Banners_Model_Rule::POS_BELOW_CART => $this->__('Below cart'),
            Amasty_Banners_Model_Rule::POS_SIDEBAR_RIGHT => $this->__('Sidebar-Right'),
            Amasty_Banners_Model_Rule::POS_SIDEBAR_LEFT => $this->__('Sidebar-Left'),
            Amasty_Banners_Model_Rule::POS_PROD_PAGE_ABOVE_PRODUCT_IMAGE  => $this->__('Product Page (Above product image)'),
            Amasty_Banners_Model_Rule::POS_PROD_PAGE_ABOVE_BRANDBUNDLE  => $this->__('Product Page (Above brand bundle offer)'),
            Amasty_Banners_Model_Rule::POS_PROD_PAGE  => $this->__('Product Page (Top)'),
			Amasty_Banners_Model_Rule::POS_PROD_PAGE_BOTTOM => $this->__('Product Page (Bottom)'),
			Amasty_Banners_Model_Rule::POS_PROD_PAGE_BELOW_CART => $this->__('Product Page (Near Cart Button)'),
            Amasty_Banners_Model_Rule::POS_CATEGORY_PAGE_BETWEEN_PRODUCTNAME_PRICE => $this->__('Category Product list after name'),
            Amasty_Banners_Model_Rule::POS_CATEGORY_PAGE => $this->__('Category Page (Top)'),
            Amasty_Banners_Model_Rule::POS_CATEGORY_PAGE_BOTTOM => $this->__('Category Page (Bottom)'),
            Amasty_Banners_Model_Rule::POS_CHECKOUT_ABOVE_TOTAL => $this->__('Cart Page (Totals Sidebar)'),
            Amasty_Banners_Model_Rule::POS_CATALOG_SEARCH_TOP => $this->__('Catalog Search (Top)'),
            Amasty_Banners_Model_Rule::POS_TOP_PAGE => $this->__('Superbanner (All pages)'),
            Amasty_Banners_Model_Rule::POS_TOP_INDEX => $this->__('Home Page under Menu'),
            Amasty_Banners_Model_Rule::POS_AFTER_N_PRODUCT_ROW => $this->__('Among category products'),
            Amasty_Banners_Model_Rule::POS_AFTER_N_BRAND_PRODUCT_ROW => $this->__('Among brand products'),
            Amasty_Banners_Model_Rule::POS_PROD_PAGE_RIGHT => $this->__('Product Page (Sidebar Right)'),
            Amasty_Banners_Model_Rule::POS_PROD_PAGE_LEFT => $this->__('Product Page (Sidebar Left)'),
            Amasty_Banners_Model_Rule::POS_INDEX_TOP_LEFT => $this->__('Home (Top Left)'),
            Amasty_Banners_Model_Rule::POS_INDEX_TOP_RIGHT => $this->__('Home (Top Right)'),
            Amasty_Banners_Model_Rule::POS_INDEX_BELOW_TOP_LEFT => $this->__('Home Below Popular Categories (Top Left)'),
            Amasty_Banners_Model_Rule::POS_INDEX_BELOW_TOP_RIGHT => $this->__('Home Below Popular Categories (Top Right)'),
            Amasty_Banners_Model_Rule::POS_INDEX_BELOW_BOTTOM_LEFT => $this->__('Home Below Popular Categories (Bottom Left)'),
            Amasty_Banners_Model_Rule::POS_INDEX_BELOW_BOTTOM_RIGHT => $this->__('Home Below Popular Categories (Bottom Right)'),
            Amasty_Banners_Model_Rule::POS_ALL_EXTREME=> $this->__('Extreme Callout (All pages)'),
            
        );

        if ($this->needBrands()) {
            $a[Amasty_Banners_Model_Rule::BRAND_PAGE] = $this->__('Brand Page (Top)');
            $a[Amasty_Banners_Model_Rule::BRAND_PAGE_BOTTOM] = $this->__('Brand Page (Bottom)');
            $a[Amasty_Banners_Model_Rule::BRAND_PAGE_BETWEEN_PRODUCTNAME_PRICE] = $this->__('Brand product list after name');
        }

        return $a;
    }

    public function needBrands()
    {
        $needBrands = false;

        $brandCode = $this->getBrandCode();
        if (Mage::getConfig()->getModuleConfig('Amasty_Shopby')->is('active', 'true') && !empty($brandCode)) {
            $needBrands = true;
        }

        return $needBrands;
    }

    public function getBrandCode()
    {
        return Mage::getStoreConfig('amshopby/brands/attr');
    }

    public function getBrands()
    {
        $brandCollection = Mage::getModel('ambrands/brand')
            ->getCollection()
            ->addAttributeToSelect('name')
            ->addAttributeToFilter('is_active', '1');

        $brands = array();

        foreach($brandCollection as $brand) {
            $brands[] = array(
                'value' => $brand->getEntityId(),
                'label' => $brand->getName(),
            );
        }

        return $brands;
    }

    /**
     * @return array
     */
    public function getPositionMulti()
    {
        $pos = $this->getPosition();
        $result = array();
        foreach ($pos as $k => $v) {

            if($this->removeUnwantedPosition($k) === true) {
                continue;
            }

            $result[] = array(
                "label" => $v,
                "value" => $k,
            );
        }
        sort($result);
        return $result;
    }

	public function showProductsListOptions()
    {
    	return array(
    		Amasty_Banners_Model_Rule::SHOW_PRODUCTS_NO => $this->__('No'),
    		Amasty_Banners_Model_Rule::SHOW_PRODUCTS_YES => $this->__('Yes'),
    	);
    }
    
    public function getBannerTypes()
    {
    	return array(
    		Amasty_Banners_Model_Rule::TYPE_IMAGE => $this->__('Image'),
    		Amasty_Banners_Model_Rule::TYPE_CMS => $this->__('CMS Block'),
    		Amasty_Banners_Model_Rule::TYPE_HTML => $this->__('HTML text'),
    	);
    }
    
    public function getBlock()
    {
        $a = array(
            Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE
        );
        return $a;       
    }

    public function getCountries()
    {
        $countries = Mage::getModel('directory/country')->getCollection()->toOptionArray();

        unset($countries[0]);

        return $countries;
    }

    /**
     * @param string $string
     *
     * @return array|null
     */
    public function unserialize($string)
    {
        if (!@class_exists('Amasty_Base_Helper_String')) {
            Mage::logException(new Exception(self::ERROR_MESSAGE));

            if (Mage::app()->getStore()->isAdmin()) {
                Mage::helper('ambase/utils')->_exit(self::ERROR_MESSAGE);
            } else {
                Mage::throwException($this->__('Sorry, something went wrong. Please contact us or try again later.'));
            }
        }

        return \Amasty_Base_Helper_String::unserialize($string);
    }

    /**
     * @param $pos
     * @return bool
     *
     * Check if we want to list the position in backend
     */
    private function removeUnwantedPosition($pos) {
        $unwantedPositions = array(1,2,8,11,12,13);

        if(in_array($pos, $unwantedPositions)) {
            return true;
        }

        return false;
    }
}
