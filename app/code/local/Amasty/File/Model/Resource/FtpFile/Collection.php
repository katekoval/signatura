<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_File
 */


class Amasty_File_Model_Resource_FtpFile_Collection extends Varien_Data_Collection
{
	public function __construct() {
		$this->setItemObjectClass('amfile/ftpFile');
		parent::__construct();
	}

	public function addFieldToFilter($field, $condition)
	{
		if (is_array($condition)) {
			if (isset($condition['in'])) {
				$values = $condition['in'];
			} elseif (isset($condition['eq'])) {
				$values = array($condition['eq']);
			} elseif (isset($condition['like'])) {
				$values = $condition['like']->__toString();
				$values = mb_substr($values, 2);
				$values = mb_substr($values, 0, -2);
			}
		} else {
			$values = array($condition);
		}
		$this->addFilter($field, $values);

		return $this;
	}

	public function loadData($printQuery = false, $logQuery = false)
	{
		if ($this->isLoaded()) {
			return $this;
		}
		$dir = $this->_getFtpDir();

		if (is_dir($dir)) {
			if ($dh = opendir($dir)) {
				while (($file = readdir($dh)) !== false) {
					if (!is_file($dir.$file)) {
						continue;
					}
					if ($file == '.htaccess') {
						continue;
					}

					$data = array(
						'name' => $file,
						'path' => $dir."/".$file,
					);

					$filterName = $this->getFilter('name');

					if (!empty($filterName['value'])) {
						if (is_array($filterName['value'])) {
							if (!in_array($data['name'], $filterName['value'])) {
								continue;
							}
						} elseif (is_string($filterName['value'])
							&& false === strpos(mb_strtolower($data['name']), mb_strtolower($filterName['value']))
						) {
							continue;
						}
					}

					$item = $this->getNewEmptyItem();
					$item->addData($data);
					$this->addItem($item);
				}
				closedir($dh);
			}
		}

		$this->_setIsLoaded();

		return $this;
	}


	protected function _getFtpDir()
	{
		return Mage::helper('amfile')->getFtpImportDir();
	}
}
