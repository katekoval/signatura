<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2019 Amasty (https://www.amasty.com)
 * @package Amasty_File
 */
class Amasty_File_Model_Mysql4_Icon extends Mage_Core_Model_Mysql4_Abstract {

    public function _construct() {
        $this->_init('amfile/icon', 'id');
    }

    public function getIcon($fileUrl)
    {
        $fileExt = '';
        if (version_compare(phpversion(), '5.3.6', '>=')) {
            $fileInfo = new SplFileInfo($fileUrl);
            $fileExt = $fileInfo->getExtension();
        } else {
            $pos = strrpos($fileUrl, '.');
            if (false !== $pos) {
                $fileExt = substr($fileUrl, $pos + 1);
            }
        }
        $select = $this->_getReadAdapter()
            ->select()
            ->from($this->getTable('amfile/icon'), 'image')
            ->where('active = 1')
            ->where('FIND_IN_SET(?, type)', $fileExt)
        ;
        return $this->_getReadAdapter()->fetchOne($select);
    }
}
