<?php
require_once 'Amasty/Banners/controllers/Adminhtml/Ambanners/RuleController.php';

class Amasty_BannersOverride_Adminhtml_Ambanners_RuleController extends Amasty_Banners_Adminhtml_Ambanners_RuleController
{
    public function editAction() 
    {
        $id     = (int) $this->getRequest()->getParam('id');
        $model  = Mage::getModel('ambanners/' . $this->_modelName)->load($id);
        
        if ($id && !$model->getId()) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('ambanners')->__('Record does not exist'));
            $this->_redirect('*/*/');
            return;
        }
        
        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }
        else {
            $this->prepareForEdit($model);
        }
        
        Mage::register('ambanners_' . $this->_modelName, $model);

        $this->loadLayout(array('default', 'editor'));
        
        $this->_setActiveMenu('promo/ambanners/' . $this->_modelName . 's');
        $this->_title($this->__('Edit'));
        
        $head = $this->getLayout()->getBlock('head');
        $head->setCanLoadExtJs(1);
        $head->setCanLoadRulesJs(1);
        
        $this->_addContent($this->getLayout()->createBlock('ambanners/adminhtml_' . $this->_modelName . '_edit'));
        $this->_addLeft($this->getLayout()->createBlock('ambanners/adminhtml_' . $this->_modelName . '_edit_tabs'));
        
        $this->
          _addJs(
			$this->getLayout()->createBlock('adminhtml/template')
                    ->setTemplate('ambannersoverride/rule/js.phtml')
		)->renderLayout();
    }
}
