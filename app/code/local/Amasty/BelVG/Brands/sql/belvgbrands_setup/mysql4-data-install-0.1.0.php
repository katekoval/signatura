<?php

$installer = $this;
$installer->startSetup();

$installer->addAttribute('ambrands_brand', 'brand_colour', array(
    'type'                       => 'varchar',
    'label'                      => 'Brand Colour',
    'input'                      => 'text',
    'sort_order'                 => 210,
    'required'                   => false,
    'note'                       => 'Brand Colour. CSS code.',
));

$installer->addAttribute('ambrands_brand', 'brand_colour_bottom', array(
    'type'                       => 'varchar',
    'label'                      => 'Brand Colour Bottom',
    'input'                      => 'text',
    'sort_order'                 => 220,
    'required'                   => false,
    'note'                       => 'Brand Colour Bottom. CSS code.',
));

$installer->addAttribute('ambrands_brand', 'brand_card_background', array(
    'type'                       => 'varchar',
    'label'                      => 'Card background colour',
    'input'                      => 'text',
    'sort_order'                 => 230,
    'required'                   => false,
    'note'                       => 'Card background colour. CSS code.',
));
$installer->addAttribute('ambrands_brand', 'view_types', array(
    'type'                       => 'int',
    'label'                      => 'Show only main image',
    'input'                      => 'select',
    'source'                     => 'eav/entity_attribute_source_boolean',
    'sort_order'                 => 240,
    'required'                   => false,
));
$installer->addAttribute('ambrands_brand', 'slider_cms_block_id', array(
    'type'                       => 'int',
    'input'                      => 'select',
    'label'                      => 'Slider CMS block',
    'required'                   => false,
    'sort_order'                 => 250,
    'input'                      => 'select',
    'source'                     => 'ambrands/attribute_source_brand_cmsblock',
));
$installer->endSetup();
