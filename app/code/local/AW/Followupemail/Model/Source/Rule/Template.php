<?php
class AW_Followupemail_Model_Source_Rule_Template
{
    const TEMPLATE_SOURCE_EMAIL = 'email';
    const TEMPLATE_SOURCE_NEWSLETTER = 'nsltr';
    const TEMPLATE_SOURCE_AW_ADVANCED_NEWSLETTER = 'awadvnsltr';
    const TEMPLATE_SOURCE_SEPARATOR = ':';

    /*
     * Returns email template names
     * @return array
     */
    public function getEmailTemplates()
    {
        $templates = array();
        $templates[self::TEMPLATE_SOURCE_EMAIL] = Mage::helper('followupemail')->__('Email Templates');

        $templateArray = Mage::getResourceSingleton('core/email_template_collection')->toArray();
        foreach ($templateArray['items'] as $value) {
            $key = self::TEMPLATE_SOURCE_EMAIL . self::TEMPLATE_SOURCE_SEPARATOR . $value['template_id'];
            $templates[$key] = $value['template_code'];
        }

        $templates[self::TEMPLATE_SOURCE_NEWSLETTER] = Mage::helper('followupemail')->__('Newsletter Templates');
        $templateArray = Mage::getResourceModel('newsletter/template_collection')->load();
        foreach ($templateArray as $item) {
            $key = self::TEMPLATE_SOURCE_NEWSLETTER . self::TEMPLATE_SOURCE_SEPARATOR . $item->getData('template_id');
            $templates[$key] = $item->getData('template_code');
        }
        if(Mage::helper('followupemail')->canUseAN()){
            $templates[self::TEMPLATE_SOURCE_AW_ADVANCED_NEWSLETTER] = Mage::helper('followupemail')->__('Advanced Newsletter Templates');
            $templateArray = Mage::getResourceModel('advancednewsletter/template_collection')->load();
            foreach ($templateArray as $item) {
                $key = self::TEMPLATE_SOURCE_AW_ADVANCED_NEWSLETTER . self::TEMPLATE_SOURCE_SEPARATOR . $item->getData('template_id');
                $templates[$key] = $item->getData('template_code');
            }
        }
        return $templates;
    }

}