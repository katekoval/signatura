<?php
class AW_Followupemail_Model_Salesrule_Rule extends Mage_SalesRule_Model_Rule
{
    protected function _afterLoad()
    {
        //It's necessary to show correct value of "Uses per Coupon" parameter for "FUE Generated Coupons" coupon type
        if (($this->getCouponType() == Mage::helper('followupemail/coupon')->getFUECouponsCode())
            && ($this->getPrimaryCoupon()->getId() === null)
            && ($this->getPrimaryCoupon()->getUsageLimit() === null)
        ) {
            $this->getPrimaryCoupon()->setUsageLimit($this->getUsesPerCoupon());
        }
        return parent::_afterLoad();
    }
}
