<?php
class AW_Followupemail_Model_Coupons extends Mage_SalesRule_Model_Coupon
{
    public function fueLoadByRules($fueRule)
    {
        $prefix = $fueRule->getCouponPrefix();
        $expires = date('Y-m-d', strtotime('+' . ((int)$fueRule->getCouponExpireDays()) . ' day', time()));
        $coupons = $this->getCollection();
        $coupons->addRuleToFilter($fueRule->getCouponSalesRuleId());
        $coupons->getSelect()
            ->where("code LIKE ?", $prefix . dechex($fueRule->getId()) . 'X%')
            ->where("expiration_date = ?", $expires);

        foreach ($coupons as $coupon) {
            return $coupon;
        }
        return null;
    }

    public function fueLoadByCode($code)
    {
        $this->load($code, 'code');
        return $this;
    }
}
