<?php

class AW_Followupemail_Model_Visitorsemail extends Mage_Core_Model_Abstract
{
    /*
     * @var bool Whether removing is currently running
     */
    protected static $_isRunning = false;

    /*
     * @var int Removing run interval (in seconds)
     */
    protected static $runInterval = 1800; // 30 minutes = 1800 seconds

    public function _construct()
    {
        $this->_init('followupemail/visitorsemail');
    }

    public function loadByQuoteId($quoteId)
    {
        $id = $this->getResource()->getVisitorByQuoteId($quoteId);
        if (!$id) {
            return false;
        }
        return $this->load($id);
    }

    public function loadBySessionId($sessionId)
    {
        $id = $this->getResource()->getVisitorBySessionId($sessionId);
        if (!$id) {
            return false;
        }
        return $this->load($id);
    }

    public function flushProcessedQuotes($datePoint)
    {
        $config = Mage::getModel('followupemail/config');
        $lastExecTime = $config->getParam(AW_Followupemail_Model_Config::LAST_EXEC_TIME);

        if (self::$_isRunning || (time() - $lastExecTime < self::$runInterval)) {
            return;
        }

        self::$_isRunning = true;

        $this->getResource()->flushProcessedQuotes($datePoint);

        self::$_isRunning = false;
    }
}