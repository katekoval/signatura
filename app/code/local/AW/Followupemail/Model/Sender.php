<?php
class AW_Followupemail_Model_Sender
{
    /*
     * @var bool Whether sending job is currently running
     */
    protected static $_isRunning = false;

    /*
     * @var int Sender run interval (in seconds)
     */
    public static $runInterval = 600; // 10 minutes = 600 seconds

    /*
     * Sends prepared emails from email queue
     */
    public static function sendPrepared()
    {
        $config = Mage::getModel('followupemail/config');

        if (!$lastExecTime = $config->getParam(AW_Followupemail_Model_Config::EMAIL_SENDER_LAST_EXEC_TIME)) {
            $config->setParam(AW_Followupemail_Model_Config::EMAIL_SENDER_LAST_EXEC_TIME, time());
            return;
        }

        if (self::$_isRunning || (time() - $lastExecTime < self::$runInterval)) {
            return;
        }

        self::$_isRunning = true;

        $config->setParam(AW_Followupemail_Model_Config::EMAIL_SENDER_LAST_EXEC_TIME, time());

        $emails = Mage::getModel('followupemail/mysql4_queue')->getPreparedEmailIds();
        $model = Mage::getModel('followupemail/queue');

        AW_Followupemail_Model_Log::log('email sender started, ' . count($emails) . ' email(s) prepared');

        foreach ($emails as $emailId) {
            $model->load($emailId)->send();
        }

        Mage::dispatchEvent('aw_fue_check_email_queue');

        self::$_isRunning = false;
    }
}