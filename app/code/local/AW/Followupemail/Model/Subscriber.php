<?php
class AW_Followupemail_Model_Subscriber extends Mage_Newsletter_Model_Subscriber
{
    public function unsubscribe()
    {
        parent::unsubscribe();

        if (self::STATUS_UNSUBSCRIBED == $this->getSubscriberStatus()
            && Mage::getStoreConfig('followupemail/general/sendonlytosubscribers')
        ) {
            Mage::getModel('followupemail/mysql4_queue')->deleteByCustomerEmail($this->getSubscriberEmail());

            AW_Followupemail_Model_Log::logWarning(
                'email ' . $this->getSubscriberEmail() . ' unsubscribed, all emails that were scheduled '
                . 'from now and not sent were deleted from the email queue'
            );
        }
        return $this;
    }
}