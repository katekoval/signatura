<?php
class AW_Followupemail_Model_Observer
{
    /*
     * Runs after customer checkout, subscribes customer
     */
    public function processSubscription()
    {
        if (!Mage::app()->getRequest()->getPost('newsletter-subscribed')) {
            return;
        }

        $quote = Mage::getSingleton('checkout/type_onepage')->getQuote();
        $email = $quote->getBillingAddress()->getEmail();

        $subscriber = Mage::getModel('newsletter/subscriber');
        $session = Mage::getSingleton('core/session');

        try {
            $subscriber->subscribe($email);
            if ($subscriber->getIsStatusChanged()) {
                $session->addSuccess(Mage::helper('followupemail')->__('You have been subscribed to newsletters'));
            }
        } catch (Exception $e) {
            $session->addException(
                $e, Mage::helper('followupemail')->__('There was a problem with the newsletter subscription')
                . ($e instanceof Mage_Core_Exception) ? ': ' . $e->getMessage() : ''
            );
        }
    }

    public function checkEmailQueue()
    {
        Mage::helper('followupemail/notification')->updateOlderEmailNotif();
    }
}