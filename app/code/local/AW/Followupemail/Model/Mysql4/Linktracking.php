<?php
class AW_Followupemail_Model_Mysql4_Linktracking extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('followupemail/linktracking', 'id');
    }

    public function getCustomerByEmail($email)
    {
        $db = $this->_getReadAdapter();

        $select = $db->select()
            ->from($this->getTable('customer/entity'), array('entity_id'))
            ->where('email=?', $email);

        return $db->fetchOne($select);
    }

}