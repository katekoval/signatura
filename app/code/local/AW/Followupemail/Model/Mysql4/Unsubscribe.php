<?php
class AW_Followupemail_Model_Mysql4_Unsubscribe extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('followupemail/unsubscribe', 'id');
    }
}