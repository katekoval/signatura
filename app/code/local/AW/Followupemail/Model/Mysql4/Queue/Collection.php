<?php
class AW_Followupemail_Model_Mysql4_Queue_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('followupemail/queue');
    }

    public function getSelectCountSql() // Covers original bug in Varien_Data_Collection_Db
    {
        $this->_renderFilters();

        $countSelect = clone $this->getSelect();
        $countSelect->reset(Zend_Db_Select::ORDER);
        $countSelect->reset(Zend_Db_Select::LIMIT_COUNT);
        $countSelect->reset(Zend_Db_Select::LIMIT_OFFSET);
        $countSelect->reset(Zend_Db_Select::COLUMNS);
        $countSelect->reset(Zend_Db_Select::GROUP);
        $countSelect->reset(Zend_Db_Select::HAVING);

        $countSelect->from('', 'COUNT(*)');
        return $countSelect;
    }

    /**
     * Process loaded collection data
     *
     * @return AW_Followupemail_Model_Mysql4_Queue_Collection
     */
    protected function _afterLoadData()
    {
        foreach ($this->getItems() as $item) {
            $item->setParams($item->unPackArray($item->getData('params')));
            $item->setDataChanges(false);
        }
        return parent::_afterLoadData();
    }
}