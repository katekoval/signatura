<?php

class AW_Followupemail_Model_Mysql4_Visitorsemail extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('followupemail/visitorsemail', 'item_id');
    }

    public function getVisitorBySessionId($sessionId)
    {
        $db = $this->_getReadAdapter();

        $select = $db->select()
            ->from($this->getMainTable(), 'item_id')
            ->where('session_id=?', $sessionId);

        return $db->fetchOne($select);
    }

    public function getVisitorByQuoteId($quoteId)
    {
        $db = $this->_getReadAdapter();

        $select = $db->select()
            ->from($this->getMainTable(), 'item_id')
            ->where('quote_id=?', $quoteId);

        return $db->fetchOne($select);
    }

    public function flushProcessedQuotes($datePoint)
    {
        $this->_getWriteAdapter()->delete(
            $this->getMainTable(),
            array(
                'quote_updated_at < ?' => $datePoint
            )
        );

        return $this;
    }
}