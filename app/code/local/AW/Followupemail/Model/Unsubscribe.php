<?php
class AW_Followupemail_Model_Unsubscribe extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        $this->_init('followupemail/unsubscribe');
    }

    public function checkIsUnsubscribed($storeId, $customerId, $customerEmail, $ruleId)
    {
        $collection = $this->getCollection()
            ->addStoreFilter($storeId)
            ->addEmailFilter($customerEmail)
            ->addRuleFilter($ruleId)
            ->addIsUnsubscribedFilter(true);
        if ($customerId) {
            $collection->addCustomerFilter($customerId);
        }
        if ($collection->getSize()) {
            return true;
        }
        return false;
    }
}