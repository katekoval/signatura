<?php
class AW_Followupemail_Adminhtml_Awfollowupemail_CouponsController extends Mage_Adminhtml_Controller_Action
{
    protected function _initAction()
    {
        if (Mage::helper('followupemail')->checkVersion('1.4')) {
            $this->_title("Manage Coupons");
        }
        return $this->loadLayout()->_setActiveMenu('followupemail/coupons');
    }

    protected function indexAction()
    {
        $this->_initAction()->renderLayout();
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('followupemail/coupons');
    }

    protected function deleteAction()
    {
        if ($this->getRequest()->getParam('id')) {
            $coupon = Mage::getModel('salesrule/coupon')->load($this->getRequest()->getParam('id'));
            if ($coupon->getData()) {
                if (!$coupon->getIsPrimary()) {
                    $this->_getSession()->addSuccess(
                        Mage::helper('followupemail')->__('Coupon "%s" successfully removed', $coupon->getCode())
                    );
                    $coupon->delete();
                } else {
                    $this->_getSession()->addError(Mage::helper('followupemail')->__('Can\'t remove primary coupon'));
                }
            } else {
                $this->_getSession()->addError(Mage::helper('followupemail')->__('Can\'t load coupon by given ID'));
            }
        } else {
            $this->_getSession()->addError(Mage::helper('followupemail')->__('ID isn\'t specified'));
        }

        $this->_redirect('*/*/index');
    }
}