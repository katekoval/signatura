<?php
$installer = $this;
$installer->startSetup();

/**
 * Adding new template for "Customer group changed" event
 */

$templateResource = Mage::getResourceModel('newsletter/template');
$modelTemplate = Mage::getModel('newsletter/template');
$templateResource->loadByCode($modelTemplate, 'AW Customer group changed');
if ($modelTemplate->getData() == array()) {
    $template = array(
        'template_code'         => 'AW Customer group changed',
        'template_subject'      => 'Your group has been changed',
        'template_sender_name'  => 'AW',
        'template_sender_email' => 'aw@aw.com',
        'template_text'         => '<p>Dear {{var customer_name}}!</p>
    <p>Your new group is {{var customer_new_group}}</p>
    {{depend has_coupon}}<p>Your coupon code is: {{var coupon.code}}, expires at {{var coupon.expiration_date}}</p>{{/depend}}'
    );

    $modelTemplate->setData($template)
        ->setTemplateType(Mage_Newsletter_Model_Template::TYPE_TEXT)
        ->setTemplateActual(1)
        ->save();
}

$this->getConnection()
    ->addColumn($this->getTable('followupemail/rule'), 'coupon_enabled', "TINYINT( 1 ) NOT NULL default '0'");
$this->getConnection()
    ->addColumn($this->getTable('followupemail/rule'), 'coupon_sales_rule_id', "INT( 10 ) UNSIGNED NOT NULL");
$this->getConnection()
    ->addColumn($this->getTable('followupemail/rule'), 'coupon_prefix', "TINYTEXT NOT NULL");
$this->getConnection()
    ->addColumn($this->getTable('followupemail/rule'), 'coupon_expire_days', "INT( 10) UNSIGNED NOT NULL");

$installer->endSetup();
