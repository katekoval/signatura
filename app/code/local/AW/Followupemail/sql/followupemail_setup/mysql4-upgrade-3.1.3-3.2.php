<?php
$installer = $this;

$installer->startSetup();
try {
    $installer->run("
        ALTER TABLE `{$this->getTable('followupemail/rule')}` ADD `mss_rule_id` INT( 10 ) NOT NULL DEFAULT '0' COMMENT 'aheadWorks Market Segmentation Suite rule ID';
    ");
} catch (Exception $e) {
    Mage::logException($e);
}
$installer->endSetup();