<?php
$installer = $this;

$installer->startSetup();
$this->getConnection()->addColumn($this->getTable('followupemail/rule'), 'unsubscribed_customers', "TEXT NOT NULL");
$installer->endSetup();