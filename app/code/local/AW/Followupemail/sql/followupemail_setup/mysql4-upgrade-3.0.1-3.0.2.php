<?php
$installer = $this;
$installer->startSetup();

$coreConfig = Mage::getModel('core/config');
if ($coreConfig instanceof Mage_Core_Model_Config) {
    try {
        $tablePrefix = $coreConfig->getTablePrefix();
        if ($tablePrefix) {
            $ruleTableName = $this->getTable('followupemail/rule');
            $queueTableName = $this->getTable('followupemail/queue');
            $linktrackingTableName = $this->getTable('followupemail/linktracking');

            $installer->run("
                ALTER TABLE $queueTableName DROP FOREIGN KEY `FK_queue_to_rule`;
                ALTER TABLE $queueTableName ADD CONSTRAINT `FK_queue_to_rule` FOREIGN KEY (`rule_id`) REFERENCES $ruleTableName (`id`) ON DELETE CASCADE;

                ALTER TABLE $linktrackingTableName DROP FOREIGN KEY `FK_link_to_queue`;
                ALTER TABLE $linktrackingTableName ADD CONSTRAINT `FK_link_to_queue` FOREIGN KEY (`rule_id`) REFERENCES $queueTableName (`id`) ON DELETE CASCADE;
            ");
        }
    } catch (Exception $e) {
    }
}

$installer->endSetup();
