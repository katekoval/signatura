<?php
$installer = $this;
$installer->startSetup();
$installer->run("
    ALTER TABLE  {$this->getTable('followupemail/unsubscribe')} CHANGE `customer_id` `customer_id` INT(11) NULL
");
$installer->endSetup();