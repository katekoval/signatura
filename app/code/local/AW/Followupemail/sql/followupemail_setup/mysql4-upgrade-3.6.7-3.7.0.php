<?php

$installer = $this;

$installer->startSetup();
$this->getConnection()->dropColumn($this->getTable('followupemail/rule'), 'unsubscribed_customers');

try {
    $installer->run("
        CREATE TABLE IF NOT EXISTS {$this->getTable('followupemail/visitorsemail')} (
        `item_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        `session_id` varchar(64) NOT NULL,
        `quote_id` int(10) unsigned NOT NULL,
        `email` varchar(255) DEFAULT NULL,
        `quote_updated_at` datetime NOT NULL,
        PRIMARY KEY (`item_id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    ");
} catch (Exception $e) {
    Mage::logException($e);
}

$installer->endSetup();