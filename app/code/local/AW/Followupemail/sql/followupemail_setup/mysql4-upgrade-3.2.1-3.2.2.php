<?php
$installer = $this;
$installer->startSetup();

/**
 * Changing field type to TEXT for increase capacity
 */
$this->getConnection()
    ->changeColumn($this->getTable('followupemail/queue'), 'params', 'params', 'TEXT');

/**
 * If sender_email field in first row of collection contains not valid email
 * address - columns `sender_name` and `sender_email` will be interchanged
 * #2648
 */
$_emailValidator = new Zend_Validate_EmailAddress();
$_firstEmail = Mage::getModel('followupemail/queue')->getCollection()
    ->setPageSize(1)
    ->setCurPage(1)
    ->load()
    ->getData();
if (!isset($_firstEmail[0]['sender_email']) || !$_emailValidator->isValid($_firstEmail[0]['sender_email'])) {
    $this->getConnection()
        ->changeColumn($this->getTable('followupemail/queue'), 'sender_name', 'sender_name2', 'VARCHAR(255)');
    $this->getConnection()
        ->changeColumn($this->getTable('followupemail/queue'), 'sender_email', 'sender_name', 'VARCHAR(255)');
    $this->getConnection()
        ->changeColumn($this->getTable('followupemail/queue'), 'sender_name2', 'sender_email', 'VARCHAR(255)');
}

/**
 * Adding column into rules table
 */
$this->getConnection()
    ->addColumn(
        $this->getTable('followupemail/rule'), 'email_send_to_customer',
        "TINYINT( 1 ) NOT NULL DEFAULT '1' AFTER `email_copy_to`"
    );

$installer->endSetup();