<?php
$installer = $this;
$installer->startSetup();
$installer->run("ALTER TABLE {$this->getTable('followupemail/rule')}
    ADD `cross_active` int NOT NULL,
    ADD `cross_source` varchar(20) NOT NULL,
    ADD `active_from` datetime NULL,
    ADD `active_to` datetime NULL
    ");
$installer->endSetup();