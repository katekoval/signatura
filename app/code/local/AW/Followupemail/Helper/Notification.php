<?php
class AW_Followupemail_Helper_Notification extends Mage_Core_Helper_Data
{
    const OLDER_EMAIL_PERIOD = 3;

    public function getNotification()
    {
        $notifications = array();
        if ($this->isNeedToShowOlderEmailNotif()) {
            $notifications[] = $this->__(
                'Follow Up Email queue has unsent items older than %s days. Please check your cron settings.',
                self::OLDER_EMAIL_PERIOD
            );
        }
        return $notifications;
    }

    public function isNeedToShowOlderEmailNotif()
    {
        return Mage::getModel('followupemail/config')->getParam(AW_Followupemail_Model_Config::NEED_TO_SHOW_OLDER_EMAIL_NOTIF, false);
    }

    public function updateOlderEmailNotif()
    {
        return Mage::getModel('followupemail/config')->setParam(AW_Followupemail_Model_Config::NEED_TO_SHOW_OLDER_EMAIL_NOTIF, $this->isQueueHasOlderEmail());
    }

    public function isQueueHasOlderEmail()
    {
        $date = new Zend_Date();
        $date->subDay(self::OLDER_EMAIL_PERIOD);
        $queue = Mage::getModel('followupemail/queue')->getCollection()
            ->addFieldToFilter('scheduled_at', array('lt' => $date->toString(Varien_Date::DATETIME_INTERNAL_FORMAT)))
            ->addFieldToFilter('status', AW_Followupemail_Model_Source_Queue_Status::QUEUE_STATUS_READY);
        $result = true;
        if ($queue->getSize() === 0) {
            $result = false;
        }
        return $result;
    }

}