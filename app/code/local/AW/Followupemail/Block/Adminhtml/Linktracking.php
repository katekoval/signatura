<?php
class AW_Followupemail_Block_Adminhtml_Linktracking extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_linktracking';
        $this->_blockGroup = 'followupemail';
        $this->_headerText = $this->__('Follow Up Email Link Tracking');
        parent::__construct();
        $this->_removeButton('add');
    }
}