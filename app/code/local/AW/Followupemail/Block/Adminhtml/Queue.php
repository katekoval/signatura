<?php
class AW_Followupemail_Block_Adminhtml_Queue extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_queue';
        $this->_blockGroup = 'followupemail';
        $this->_headerText = $this->__('Follow Up Email Queue');
        parent::__construct();
        $this->_removeButton('add');
    }
}