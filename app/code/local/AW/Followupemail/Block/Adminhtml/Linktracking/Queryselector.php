<?php
class AW_Followupemail_Block_Adminhtml_Linktracking_Queryselector extends Mage_Adminhtml_Block_Template
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('followupemail/linktracking/queryselector.phtml');
        $this->setQueryType(AW_Followupemail_Helper_Data::getLinktrackingQueryType());
    }
}
