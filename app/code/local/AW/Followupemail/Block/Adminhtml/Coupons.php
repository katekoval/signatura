<?php
class AW_Followupemail_Block_Adminhtml_Coupons extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_coupons';
        $this->_blockGroup = 'followupemail';
        $this->_headerText = $this->__('Manage Coupons');
        parent::__construct();
        $this->_removeButton('add');
    }
}