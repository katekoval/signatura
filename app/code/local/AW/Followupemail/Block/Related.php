<?php
class AW_Followupemail_Block_Related extends Mage_Catalog_Block_Product_List
{
    protected function _toHtml()
    {
        $this->setTemplate('followupemail/related.phtml');
        return $this->renderView();
    }

    public function getItems()
    {
        return $this->_productCollection;
    }
}