<?php
class AW_Core_Block_Adminhtml_Log extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_controller = 'adminhtml_log';
        $this->_blockGroup = 'awcore';
        $this->_headerText = Mage::helper('awcore')->__('aheadWorks Extensions Log');

        parent::__construct();

        $this->setTemplate('widget/grid/container.phtml');
        $this->_removeButton('add');
        $this->_addButton('clear', array(
            'label'   => Mage::helper('awcore')->__('Clear Log'),
            'onclick' =>
            'if(confirm(\'' . Mage::helper('awcore')->__('Are you sure to clear all log entries?')
            . '\'))setLocation(\'' . $this->getClearUrl() . '\')',
            'class'   => 'delete',
        ));
    }

    /**
     * Return url for log cleanup
     *
     * @return string
     */
    public function getClearUrl()
    {
        return Mage::getSingleton('adminhtml/url')->getUrl('adminhtml/awcore_viewlog/clear');
    }

}