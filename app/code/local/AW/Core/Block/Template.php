<?php
class AW_Core_Block_Template extends Mage_Core_Block_Template
{
    /**
     * Logs entry wrapper
     *
     * @param object $message
     * @param object $severity [optional]
     *
     * @return void
     */
    public function log($message, $severity = null)
    {
        Mage::helper('awcore/logger')->log($this, $message, $severity);
    }
}