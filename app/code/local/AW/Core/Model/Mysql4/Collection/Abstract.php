<?php
class AW_Core_Model_Mysql4_Collection_Abstract extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    /**
     * Logs entry wrapper
     *
     * @param object $message
     * @param object $severity [optional]
     *
     * @return
     */
    public function log($message, $severity = null)
    {
        Mage::helper('awcore/logger')->log($this, $message, $severity);
    }
}