<?php
class AW_Core_Model_Mysql4_Logger_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('awcore/logger');
    }

    /**
     * Add filter to find records older than specified date
     *
     * @param Zend_Date $Date
     *
     * @return AW_Core_Model_Mysql4_Logger_Collection
     */
    public function addOlderThanFilter(Zend_Date $Date)
    {
        $this->getSelect()->where('date<?', $Date->toString(AW_Core_Model_Abstract::DB_DATETIME_FORMAT));
        return $this;
    }
}