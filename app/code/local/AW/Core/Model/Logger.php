<?php
class AW_Core_Model_Logger extends Mage_Core_Model_Abstract
{
    /** Notice */
    const LOG_SEVERITY_NOTICE = 1;
    /** Strict notice */
    const LOG_SEVERITY_STRICT_NOTICE = 2;
    /** Warning */
    const LOG_SEVERITY_WARNING = 4;
    /** Error */
    const LOG_SEVERITY_ERROR = 8;
    /** Fatal */
    const LOG_SEVERITY_FATAL = 8;

    protected function _construct()
    {
        $this->_init('awcore/logger');
    }

    /**
     * Prepares log entry for saving
     *
     * @return AW_Core_Model_Log
     */
    public function _beforeSave()
    {
        if (!$this->getSeverity()) {
            $this->setSeverity(self::LOG_SEVERITY_NOTICE);
        }
        if (!$this->getDate()) {
            $this->setDate(now());
        }
        return parent::_beforeSave();
    }

    /**
     * Exorcise wrapper
     *
     * @return
     */
    public function exorcise()
    {
        return Mage::helper('awcore/logger')->exorcise();
    }
}