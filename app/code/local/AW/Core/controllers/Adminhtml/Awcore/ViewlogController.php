<?php
class AW_Core_Adminhtml_Awcore_ViewlogController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
        $this
            ->loadLayout()
            ->_addContent($this->getLayout()->createBlock('awcore/adminhtml_log'))
            ->renderLayout();
    }

    /**
     * Clears all records in log table
     *
     * @return AW_Core_ViewlogController
     */
    public function clearAction()
    {
        try {
            Mage::getResourceSingleton('awcore/logger')->truncateAll();
            Mage::getSingleton('adminhtml/session')->addSuccess("Log successfully cleared");
            $this->_redirect('*/*');

        } catch (Mage_Core_Exception $E) {
            Mage::getSingleton('adminhtml/session')->addError($E->getMessage());
            $this->_redirectReferer();
        }
        return $this;
    }
}