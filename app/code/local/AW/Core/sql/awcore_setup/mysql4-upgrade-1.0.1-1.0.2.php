<?php
$installer = $this;

/* $installer Mage_Core_Model_Resource_Setup */

$installer->startSetup();
$installer->run("
	ALTER TABLE {$this->getTable('awcore/logger')} ADD `custom_field_4` VARCHAR( 255 ) NOT NULL AFTER `custom_field_3`;
	ALTER TABLE {$this->getTable('awcore/logger')} ADD INDEX ( `custom_field_4` );
");
$installer->endSetup();