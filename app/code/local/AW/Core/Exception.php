<?php
class AW_Core_Exception extends Mage_Core_Exception
{
    /**
     * @deprecated
     * @static
     * @var mixed
     */
    protected static $_log;

    public function __construct($message, $details = '', $level = 1, $module = '')
    {
        Mage::helper('awcore/logger')->log(
            $this, $message, AW_Core_Model_Logger::LOG_SEVERITY_WARNING, $details, $this->getLine()
        );
        return parent::__construct($message);
    }
}