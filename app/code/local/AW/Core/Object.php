<?php
class AW_Core_Object extends Varien_Object
{
    /**
     * Logs entry wrapper
     *
     * @param object $message
     * @param object $severity [optional]
     *
     * @return
     */
    public function log($message, $severity = null, $details = '')
    {
        Mage::helper('awcore/logger')->log($this, $message, $severity, $details);
    }
}