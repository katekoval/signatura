<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Pmatch
 * @version    2.1.5
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Pmatch_Block_Popup extends AW_Pmatch_Block_Form
{
    public function canShow()
    {
        $requestFormType = Mage::helper('pmatch/config')->getRequestFormType();
        return (
            $requestFormType == AW_Pmatch_Model_Source_Form_Type::POPUP
            && Mage::helper('pmatch')->isProductTypeValid($this->getProduct())
            && !Mage::helper('pmatch')->isApprovedRequest($this->getProduct())
        );
    }
}
