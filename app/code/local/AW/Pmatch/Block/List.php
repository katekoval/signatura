<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Pmatch
 * @version    2.1.5
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Pmatch_Block_List extends Mage_Core_Block_Template
{
    /**
     * @return AW_Pmatch_Model_Mysql4_Requests_Collection
     */
    public function getRequestCollection()
    {
        $customerId = Mage::getSingleton('customer/session')->getCustomer()->getData('entity_id');
        $collection = Mage::getResourceModel('pmatch/requests_collection')->addCustomerFilter($customerId);
        return $collection;
    }

    public function getFormattedDate($date)
    {
        return $this->formatDate($date, Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM, true);
    }

    public function getBackUrl()
    {
        if ($this->getRefererUrl()) {
            return $this->getRefererUrl();
        }
        return $this->getUrl(
            'customer/account/', array('_secure' => Mage::app()->getStore(true)->isCurrentlySecure())
        );
    }

    public function getRequestStatusLabel($request)
    {
        return Mage::getModel('pmatch/source_status')->getStatusLabelById($request->getStatus());
    }

    public function getDiscountStatusLabel($request)
    {
        $statusModel = Mage::getModel('pmatch/source_discountstatus');
        if ($request->getDiscountStatus() == AW_Pmatch_Model_Source_Discountstatus::USED) {
            return $statusModel->getStatusLabelByCode(AW_Pmatch_Model_Source_Discountstatus::CODE_USED);
        } elseif ($request->isExpired()) {
            return $this->__('Expired');
        }
        return $this->__('Active');
    }

    public function formatPrice($amount)
    {
        $amount = Mage::app()->getStore()->convertPrice($amount);
        $currency = Mage::getModel('directory/currency')->load(Mage::app()->getStore()->getCurrentCurrencyCode());
        return $currency->format($amount, array(), false);
    }
}
