<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Pmatch
 * @version    2.1.5
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Pmatch_Block_Adminhtml_Pmatch_Edit_Tab_Form
    extends Mage_Adminhtml_Block_Widget_Form
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    public function getTabLabel()
    {
        return $this->__('Request Information');
    }

    public function getTabTitle()
    {
        return $this->__('Request Information');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }

    protected function _prepareForm()
    {
        $data = Mage::registry('pmatch_data');
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset(
            'pmatch_form', array('legend' => $this->__('Request Information'))
        );
        $fieldset->addType('customer', 'AW_Pmatch_Block_Adminhtml_Widget_Form_Element_Customer');
        $fieldset->addType('product', 'AW_Pmatch_Block_Adminhtml_Widget_Form_Element_Product');
        $fieldset->addType('link', 'AW_Pmatch_Block_Adminhtml_Widget_Form_Element_Link');
        $fieldset->addType('price', 'AW_Pmatch_Block_Adminhtml_Widget_Form_Element_Price');
        $fieldset->addType('store_name', 'AW_Pmatch_Block_Adminhtml_Widget_Form_Element_Storename');
        $fieldset->addType('datetime', 'AW_Pmatch_Block_Adminhtml_Widget_Form_Element_Datetime');
        $fieldset->addType(
            'difference_competitor_vs_store', 'AW_Pmatch_Block_Adminhtml_Widget_Form_Element_Difference_Competitor'
        );
        $fieldset->addType(
            'difference_customer_vs_store', 'AW_Pmatch_Block_Adminhtml_Widget_Form_Element_Difference_Customer'
        );

        $statuses = Mage::getModel('pmatch/source_status')->getShortOptionArray();
        $fieldset->addField(
            'status',
            'select',
            array(
                'label'  => $this->__('Status'),
                'name'   => 'status',
                'values' => $statuses,
            )
        );

        $fieldset->addField(
            'created_time',
            'datetime',
            array(
                'label' => $this->__('Created on'),
                'name'  => 'created_time',
            )
        );

        if (Mage::app()->isSingleStoreMode()) {
            $data->setStoreId(Mage_Catalog_Model_Abstract::DEFAULT_STORE_ID);
            $fieldset->addField(
                'store_id',
                'hidden',
                array(
                    'name' => 'store_id',
                )
            );
        } else {
            if ($this->isNewRequest()) {
                $fieldset->addField(
                    'store_id',
                    'select',
                    array(
                        'label'  => $this->__('Store'),
                        'name'   => 'store_id',
                        'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, false),
                    )
                );
            } else {
                $fieldset->addField(
                    'store_id',
                    'store_name',
                    array(
                        'label' => $this->__('Store'),
                        'name'  => 'store_id',
                    )
                );
            }
        }

        $productLink = Mage::getModel('adminhtml/url')->getUrl(
            'adminhtml/catalog_product/edit',
            array('id' => (int)Mage::registry('pmatch_data')->getProductId())
        );

        $fieldset->addField(
            'product_name',
            'product',
            array(
                'label' => $this->__('Product'),
                'name'  => 'product_name',
                'href'  => $productLink,
                'value' => Mage::registry('pmatch_data')->getProductName(),
            )
        );

        if ($this->isNewRequest()) {
            $fieldset->addField(
                'customer_name',
                'text',
                array(
                    'label'    => $this->__('Customer Name'),
                    'name'     => 'customer_name',
                    'required' => true,
                )
            );
            $fieldset->addField(
                'customer_email',
                'text',
                array(
                    'label'    => $this->__('Customer Email'),
                    'name'     => 'customer_email',
                    'class'    => 'validate-email',
                    'required' => true,
                )
            );
        } else {
            $fieldset->addField(
                'request_id',
                'customer',
                array(
                    'label' => $this->__('Customer'),
                    'name'  => 'request_id',
                )
            );
        }

        $fieldset->addField(
            'product_price',
            'price',
            array(
                'label' => $this->__('Current price for customer'),
                'name'  => 'product_price',
            )
        );

        if (
            $data->getData('product_price_original')
            && $data->getData('product_price_original') > $data->getData('product_price')
        ) {
            $fieldset->addField(
                'product_price_original',
                'price',
                array(
                    'label' => $this->__('Product original price'),
                    'name'  => 'product_price_orignal',
                )
            );
        }

        $fieldset->addField(
            'customer_price',
            'price',
            array(
                'label' => $this->__('Requested price'),
                'name'  => 'customer_price',
            )
        );
        if (isset($data['competitor_price'])) {
            $data['competitor_price'] = round($data['competitor_price'], 2);
        }
        $baseCurrencyCode = Mage::app()->getStore()->getBaseCurrency()->getCode();
        $fieldset->addField(
            'competitor_price',
            'text',
            array(
                'label'    => $this->__("Competitor's price, %s", $baseCurrencyCode),
                'name'     => 'competitor_price',
                'class'    => $this->isNewRequest() ? 'required-entry' : '',
                'required' => $this->isNewRequest() ? true : false,
            )
        );

        $fieldset->addField(
            'difference_competitor_vs_store',
            'difference_competitor_vs_store',
            array(
                'label' => $this->__("Difference between competitor's price and current price"),
                'name'  => 'difference_competitor_vs_store',
                'value' => array('competitor_price' => 'competitor_price', 'product_price' => 'product_price'),
            )
        );

        $fieldset->addField(
            'difference_customer_vs_store',
            'difference_customer_vs_store',
            array(
                'label' => $this->__('Difference between requested price and current price'),
                'name'  => 'difference_customer_vs_store',
            )
        );

        $fieldset->addField(
            'discount',
            'text',
            array(
                'label' => $this->__('Discount, %s', $baseCurrencyCode),
                'name'  => 'discount',
            )
        );

        $outputFormat = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
        try {
            if (isset($data['discount_use_before']) && !empty($data['discount_use_before'])) {
                $data['discount_use_before'] = Mage::app()->getLocale()->date(
                    $data['discount_use_before'], Varien_Date::DATETIME_INTERNAL_FORMAT
                );
            }
        } catch (Exception $e) {
            unset ($data['discount_use_before']);
            throw $e;
        }
        $fieldset->addField(
            'discount_use_before',
            'date',
            array(
                'label'        => $this->__('Discount active to'),
                'name'         => 'discount_use_before',
                'title'        => $this->__('Discount active to'),
                'image'        => $this->getSkinUrl('images/grid-cal.gif'),
                'format'       => $outputFormat,
                'input_format' => Varien_Date::DATETIME_INTERNAL_FORMAT,
                'time'         => true,
                'required'     => true,
            )
        );

        $fieldset->addField(
            'discount_status',
            'select',
            array(
                'label'  => $this->__('Discount usage'),
                'name'   => 'discount_status',
                'values' => Mage::getModel('pmatch/source_discountstatus')->toOptionArray(),
            )
        );

        $fieldset->addField(
            'competitor_link',
            $this->isNewRequest() ? 'text' : 'link',
            array(
                'label'    => $this->__('Link'),
                'name'     => 'competitor_link',
                'value'    => Mage::registry('pmatch_data')->getCompetitorLink(),
                'class'    => $this->isNewRequest() ? 'required-entry' : '',
                'required' => $this->isNewRequest() ? true : false,
            )
        );

        $fieldset->addField(
            'customer_phone',
            'text',
            array(
                'label' => $this->__('Phone Number'),
                'name'  => 'customer_phone',
            )
        );

        if (is_object($data) && $data->getGuestCode()) {
            $fieldset->addField(
                'guest_code',
                'label',
                array(
                    'label' => $this->__('Discount Code'),
                    'name'  => 'guest_code',
                )
            );
        }

        $fieldset->addField(
            'info',
            'editor',
            array(
                'name'   => 'info',
                'label'  => $this->__('Additional information'),
                'title'  => $this->__('Additional information'),
                'style'  => 'width:700px; height:500px;',
                'config' => $this->_getWysiwygConfig(),
            )
        );

        $fieldset->addField(
            'product_id',
            'hidden',
            array(
                'name' => 'product_id',
            )
        );

        $form->setValues($data);
        return parent::_prepareForm();
    }

    public function isNewRequest()
    {
        return !Mage::registry('pmatch_data')->getId();
    }


    protected function _getWysiwygConfig()
    {
        $config = Mage::getSingleton('cms/wysiwyg_config')->getConfig();
        $data = $this->_recursiveUrlUpdate($config->getData());
        $config->setData($data);
        return $config;
    }

    protected function _recursiveUrlUpdate($data)
    {
        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $data[$key] = $this->_recursiveUrlUpdate($value);
            }
            if (is_string($value)) {
                $data[$key] = str_replace('pmatch_admin', 'admin', $value);
            }
        }
        return $data;
    }
}
