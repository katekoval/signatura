<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Pmatch
 * @version    2.1.5
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Pmatch_Block_Adminhtml_Pmatch_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('pmatchGrid');
        $this->setDefaultSort('created_time');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('pmatch/requests')->getCollection();
        $collection->joinCustomerTable();
        if ($statusCode = $this->getRequest()->getParam('only', false)) {
            $status = Mage::getModel('pmatch/source_status')->getStatusIdByCode($statusCode);
            $collection->addStatusFilter($status);
        }
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn(
            'request_id',
            array(
                'header' => $this->__('ID'),
                'align'  => 'right',
                'width'  => '50px',
                'index'  => 'request_id',
            )
        );

        if (!Mage::app()->isSingleStoreMode()) {
            $this->addColumn(
                'store_id',
                array(
                    'header'          => Mage::helper('sales')->__('Store'),
                    'index'           => 'store_id',
                    'width'           => '150px',
                    'align'           => 'left',
                    'type'            => 'store',
                    'store_view'      => true,
                    'display_deleted' => false,
                )
            );
        }

        $this->addColumn(
            'product_name',
            array(
                'header'   => $this->__('Product'),
                'align'    => 'left',
                'index'    => 'product_name',
                'type'     => 'action',
                'renderer' => 'pmatch/adminhtml_widget_grid_column_renderer_product',
            )
        );

        $this->addColumn(
            'customer_name',
            array(
                'header'   => $this->__('Customer Name'),
                'align'    => 'left',
                'index'    => 'customer_name',
            )
        );

        $this->addColumn(
            'customer_email',
            array(
                'header'   => $this->__('Customer Email'),
                'align'    => 'left',
                'index'    => 'customer_email',
            )
        );
        $groups = Mage::getResourceModel('customer/group_collection')
            ->load()
            ->toOptionHash()
        ;
        $this->addColumn(
            'customer_group_id',
            array(
                 'filter_index' => 'IF(ce.group_id IS NULL, 0, ce.group_id)',
                 'header'       => Mage::helper('customer')->__('Group'),
                 'width'        => '120',
                 'index'        => 'customer_group_id',
                 'type'         => 'options',
                 'options'      => $groups,
            )
        );

        $store = Mage::app()->getStore();
        $this->addColumn(
            'current_price',
            array(
                'header'        => $this->__('Current Price for Customer'),
                'align'         => 'right',
                'index'         => 'product_price',
                'type'          => 'price',
                'currency_code' => $store->getBaseCurrency()->getCode(),
            )
        );

        if (!$this->getRequest()->getParam('only')) {
            $this->addColumn(
                'discount',
                array(
                    'header'        => $this->__('Discount'),
                    'align'         => 'right',
                    'index'         => 'discount',
                    'type'          => 'price',
                    'currency_code' => $store->getBaseCurrency()->getCode(),
                )
            );
        }

        $this->addColumn(
            'competitor_price',
            array(
                'header'        => $this->__("Competitor's Price"),
                'align'         => 'right',
                'index'         => 'competitor_price',
                'type'          => 'price',
                'currency_code' => $store->getBaseCurrency()->getCode(),
            )
        );

        $this->addColumn(
            'competitor_link',
            array(
                'header'   => $this->__('Link'),
                'align'    => 'left',
                'index'    => 'competitor_link',
                'type'     => 'action',
                'renderer' => 'pmatch/adminhtml_widget_grid_column_renderer_link',
            )
        );


        $this->addColumn(
            'created_time',
            array(
                'header' => $this->__('Created On'),
                'align'  => 'left',
                'width'  => '80px',
                'index'  => 'created_time',
                'type'   => 'datetime',
            )
        );

        if (!$this->getRequest()->getParam('only')) {
            $this->addColumn(
                'discount_status',
                array(
                    'header'  => $this->__('Discount Status'),
                    'align'   => 'left',
                    'width'   => '80px',
                    'index'   => 'discount_status',
                    'type'    => 'options',
                    'options' => Mage::getModel('pmatch/source_discountstatus')->getShortOptionArray(),
                )
            );

            $this->addColumn(
                'status',
                array(
                    'header'  => $this->__('Status'),
                    'align'   => 'left',
                    'width'   => '80px',
                    'index'   => 'status',
                    'type'    => 'options',
                    'options' => Mage::getModel('pmatch/source_status')->getShortOptionArray(),
                )
            );
        }

        $this->addColumn(
            'action',
            array(
                'header'    => $this->__('Action'),
                'width'     => '50',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption' => $this->__('Edit'),
                        'url'     => array('base' => '*/*/edit'),
                        'field'   => 'id',
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
            )
        );
        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('request_id');
        $this->getMassactionBlock()->setFormFieldName('pmatch');

        $this->getMassactionBlock()->addItem(
            'delete',
            array(
                'label'   => $this->__('Delete'),
                'url'     => $this->getUrl('*/*/massDelete'),
                'confirm' => $this->__('Are you sure that you want to delete selected requests?'),
            )
        );

        $statuses = Mage::getSingleton('pmatch/source_status')->getShortOptionArray();
        $statuses[''] = '';
        unset($statuses[AW_Pmatch_Model_Source_Status::APPROVED]);
        ksort($statuses);
        $this->getMassactionBlock()->addItem(
            'status',
            array(
                'label'      => $this->__('Change Status'),
                'url'        => $this->getUrl('*/*/massStatus', array('_current' => true)),
                'additional' => array(
                    'visibility' => array(
                        'name'   => 'status',
                        'type'   => 'select',
                        'class'  => 'required-entry',
                        'label'  => $this->__('Status'),
                        'values' => $statuses,
                    )
                )
            )
        );
        return $this;
    }

    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}
