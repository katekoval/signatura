<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Pmatch
 * @version    2.1.5
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Pmatch_Block_Adminhtml_Pmatch_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();

        $this->_objectId = 'id';
        $this->_blockGroup = 'pmatch';
        $this->_controller = 'adminhtml_pmatch';

        $this->_updateButton('save', 'label', Mage::helper('pmatch')->__('Save Request'));
        $this->_updateButton('delete', 'label', Mage::helper('pmatch')->__('Delete Request'));

        $this->_addButton(
            'saveandcontinue',
            array(
                'label'   => Mage::helper('adminhtml')->__('Save And Continue Edit'),
                'onclick' => 'saveAndContinueEdit()',
                'class'   => 'save',
            ),
            -100
        );
        $this->_addButton(
            'saveandemail',
            array(
                'label'   => Mage::helper('adminhtml')->__('Save And Send Email'),
                'onclick' => 'saveAndSendMail()',
                'class'   => 'save',
            ),
            -100
        );

        $productPart = '';
        if ((int)$this->getRequest()->getParam('product') > 0) {
            $productPart = 'product/' . (int)$this->getRequest()->getParam('product');
        }

        $this->_formScripts[] = "
            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/{$productPart}');
            }
            function saveAndSendMail(){
                editForm.submit($('edit_form').action+'send/1/{$productPart}');
            }
        ";
    }

    public function getHeaderText()
    {
        if (Mage::registry('pmatch_data') && Mage::registry('pmatch_data')->getId()) {
            return Mage::helper('pmatch')->__(
                "Edit Request # %d", $this->escapeHtml(Mage::registry('pmatch_data')->getId())
            );
        } else {
            return Mage::helper('pmatch')->__('Add Request');
        }
    }
}