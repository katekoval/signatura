<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Pmatch
 * @version    2.1.5
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Pmatch_Block_Adminhtml_Widget_Form_Element_Difference_Customer extends Varien_Data_Form_Element_Abstract
{
    public function getElementHtml()
    {
        $data = Mage::registry('pmatch_data');
        if (($data->getProductPrice() == 0) || !$data->getCustomerPrice()) {
            return Mage::helper('pmatch')->__('No difference');
        }
        $diffAmount = $data->getProductPrice() - $data->getCustomerPrice();
        $diffFormattedAmount = Mage::app()->getStore()->formatPrice($diffAmount, true);
        $diffPercent = 100 - $data->getCustomerPrice() / $data->getProductPrice() * 100;
        return sprintf('%s (%.2f %%)', $diffFormattedAmount, $diffPercent);
    }
}