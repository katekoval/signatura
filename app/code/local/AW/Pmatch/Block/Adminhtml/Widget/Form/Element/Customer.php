<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Pmatch
 * @version    2.1.5
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Pmatch_Block_Adminhtml_Widget_Form_Element_Customer extends Varien_Data_Form_Element_Abstract
{
    public function getElementHtml()
    {
        $id = $this->getValue();
        $requestModel = Mage::getModel('pmatch/requests')->load($id);
        $customerId = (int)$requestModel->getCustomerId();
        $html = $this->getBeforeElementHtml();
        if ($customerId) {
            $customerUrl = Mage::getModel('adminhtml/url')->getUrl(
                'adminhtml/customer/edit', array('id' => $customerId)
            );

            $html .= '<a href="' . $customerUrl . '" title="'
                . Mage::helper('pmatch')->escapeHtml($requestModel->getCustomerName()) . '">'
                . Mage::helper('pmatch')->escapeHtml($requestModel->getCustomerName()) . '</a>'
            ;
            $html .= '   |   ';
        } else {
            $html .= '<b>' . Mage::helper('pmatch')->escapeHtml($requestModel->getCustomerName()) . '</b>';
            $html .= '   |   ';
        }

        $email = $requestModel->getCustomerEmail();

        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $html .= '&lt;&nbsp;<a href="mailto:' . $email . '" title="' . $email . '">'. $email . '</a>&nbsp;&gt;';
        } else {
            $html .= Mage::helper('pmatch')->escapeHtml($email);
        }
        $html .= $this->getAfterElementHtml();
        return $html;
    }
}