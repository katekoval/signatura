<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Pmatch
 * @version    2.1.5
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Pmatch_Block_Form extends Mage_Core_Block_Template
{
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('pmatch/request_form.phtml');
    }

    public function getAction()
    {
        return Mage::getUrl('pmatch/request/new');
    }

    public function getOriginalProductPrice($product)
    {
        $finalPrice = $product->getFinalPrice();
        $discount = $this->helper('pmatch')->getValueFromActiveRequest($product->getId(), 'discount');
        if ($discount) {
            $finalPrice += $discount;
        }
        return $finalPrice;
    }

    public function getProduct()
    {
        $productId = $this->getData('product_id');
        if ($productId === null) {
            $productId = $this->getRequest()->getParam('product_id', null);
        }
        if ($productId === null) {
            return Mage::registry('current_product');
        }
        return Mage::getModel('catalog/product')->load($productId);
    }

    public function getHtmlBeforeForm()
    {
        return Mage::helper('pmatch/config')->getHtmlBeforeForm();
    }

    public function getHtmlAfterForm()
    {
        return Mage::helper('pmatch/config')->getHtmlAfterForm();
    }

    public function getCanShowOptionCustomerPrice()
    {
        return Mage::helper('pmatch/config')->getCanShowOptionCustomerPrice();
    }

    public function getBackUrl()
    {
        return Mage::app()->getFrontController()->getRequest()->getServer('HTTP_REFERER');
    }

}
