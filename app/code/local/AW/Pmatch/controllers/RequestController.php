<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Pmatch
 * @version    2.1.5
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Pmatch_RequestController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            Mage::getSingleton('customer/session')->setAfterAuthUrl(Mage::getUrl('pmatch/request/index'));
            $this->_redirect('customer/account/login/');
            return;
        }
        $this->loadLayout();
        $block = $this->getLayout()->getBlock('pmatch');
        if ($block) {
            $block->setRefererUrl($this->_getRefererUrl());
        }
        $this->renderLayout();
    }

    public function newAction()
    {
        if ($this->getRequest()->getPost('antibot-field', null) === null) {
            Mage::getSingleton('core/session')->addError(
                $this->__('Request cannot be submitted. Please, check that Java Script is enabled in your browser.')
            );
            $this->_redirectUrl($this->_getRefererUrl());
            return;
        }

        $session = Mage::getSingleton('core/session');
        if (!$data = $this->getRequest()->getPost()) {
            $session->addError($this->__('Oops. Your request has not been submitted. Sorry.'));
            $this->_redirectUrl($this->_getRefererUrl());
            return;
        }

        $validator = new Zend_Validate_EmailAddress();
        if (!$validator->isValid($data['customer_email'])) {
            $session->addError($this->__('Please validate your email address.'));
            $this->_redirectUrl($this->_getRefererUrl());
            return;
        }

        if ($data['competitor_price'] <= 0) {
            $session->addError($this->__('Please specify correct competitor price.'));
            $this->_redirectUrl($this->_getRefererUrl());
            return;
        }

        $data['competitor_price'] = Mage::helper('pmatch')->convertPrice($data['competitor_price']);
        if (!empty($data['customer_price'])) {
            $data['customer_price'] = Mage::helper('pmatch')->convertPrice($data['customer_price']);
        }

        $data['product_name'] = Mage::helper('pmatch')->stripTags($data['product_name']);
        $data['customer_name'] = Mage::helper('pmatch')->stripTags($data['customer_name']);
        $data['customer_email'] = Mage::helper('pmatch')->stripTags($data['customer_email']);
        $data['customer_phone'] = Mage::helper('pmatch')->stripTags($data['customer_phone']);
        $data['competitor_link'] = Mage::helper('pmatch')->stripTags($data['competitor_link']);
        $data['info'] = nl2br(Mage::helper('pmatch')->stripTags($data['info']));

        $request = Mage::getModel('pmatch/requests');
        $request
            ->setData($data)
            ->setCreatedTime(Mage::getModel('core/date')->gmtDate())
            ->setUpdateTime(Mage::getModel('core/date')->gmtDate())
            ->setStatus(AW_Pmatch_Model_Source_Status::PENDING)
            ->setStoreId(Mage::app()->getStore()->getId())
        ;
        try {
            $request->save();
        } catch (Exception $e) {
            $session->addError($this->__('Oops. Your request has not been submitted. Sorry.'));
            $this->_redirectUrl($this->_getRefererUrl());
            return;
        }

        if (Mage::helper('pmatch/config')->isNotificationEnabled()) {
            Mage::getModel('pmatch/notifications')->send($request);
        }
        if (Mage::getSingleton('customer/session')->isLoggedIn()) {
            $pmatchLink = Mage::getUrl(
                'pmatch/request/index',
                array('_secure' => Mage::app()->getStore(true)->isCurrentlySecure())
            );
            $session->addSuccess(
                $this->__('Your <a href="%s">request</a> has been submitted. Thank you.', $pmatchLink)
            );
        } else {
            $session->addSuccess($this->__('Your request has been submitted. Thank you.'));
        }
        $product = Mage::getModel('catalog/product')->load($data['product_id']);
        if ($product->getId()) {
            $this->_redirectUrl($product->getProductUrl());
        } else {
            $this->_redirectUrl($this->_getRefererUrl());
        }
    }

    public function addAction()
    {
        $this->loadLayout();
        $this->getLayout()->createBlock('catalog/breadcrumbs');
        if ($breadcrumbsBlock = $this->getLayout()->getBlock('breadcrumbs')) {
            $product = Mage::getModel('catalog/product')->load($this->getRequest()->getParam('product_id'));

            if (!Mage::helper('pmatch')->isProductTypeValid($product)) {
                Mage::getSingleton('core/session')->addError(
                    $this->__('You can not create Price Match request for this product type.')
                );
                $this->_redirectUrl($this->_getRefererUrl());
                return;
            }

            if (Mage::helper('pmatch')->isApprovedRequest($product)) {
                Mage::getSingleton('core/session')->addError(
                    $this->__('You have already submitted a Price Match request for this product.')
                );
                $this->_redirectUrl($this->_getRefererUrl());
                return;
            }

            $breadcrumbsBlock->addCrumb(
                'product', array(
                    'label'    => $product->getName(),
                    'link'     => $product->getProductUrl(),
                    'readonly' => true,
                )
            );
            $breadcrumbsBlock->addCrumb(
                'pmatch', array(
                    'label' => $this->__('Price Match Request'),
                )
            );
        }
        $this->getLayout()->getBlock('head')->setTitle(Mage::helper('pmatch')->__('Add a new price match request'));
        $this->renderLayout();
    }
}
