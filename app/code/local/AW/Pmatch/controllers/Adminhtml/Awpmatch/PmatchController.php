<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Pmatch
 * @version    2.1.5
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Pmatch_Adminhtml_Awpmatch_PmatchController extends Mage_Adminhtml_Controller_Action
{
    const MYSQL_DATETIME_FORMAT = 'Y-m-d H:i:s';

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/catalog/pmatch');
    }

    protected function _initAction()
    {
        $this
            ->loadLayout()
            ->_setActiveMenu('catalog/pmatch')
        ;
        return $this;
    }

    public function indexAction()
    {
        $title = $this->__('Requests List');
        if ($this->getRequest()->getParam('only')) {
            $title = $this->__('Pending requests');
        }
        $this
            ->_initAction()
            ->_setTitle($title)
            ->renderLayout()
        ;
    }

    public function editAction()
    {
        $requestId = $this->getRequest()->getParam('id', null);
        $requestModel = Mage::getModel('pmatch/requests')->load($requestId);
        $title = $requestId ? 'Edit Request' : 'New Request';
        if (!$requestModel->getId() && $requestId !== null) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('pmatch')->__('Request does not exist'));
            return $this->_redirect('*/*/');
        }

        $data = Mage::getSingleton('adminhtml/session')->getPmatchData();
        if (null !== $data) {
            $requestModel->addData($data);
            Mage::getSingleton('adminhtml/session')->setPmatchData(null);
        }
        if ($productId = $this->getRequest()->getParam('product', false)) {
            $product = Mage::getModel('catalog/product')->load($productId);
            $productData = array(
                'product_id'    => $product->getId(),
                'product_name'  => $product->getName(),
                'product_price' => $product->getPrice(),
            );
            $requestModel->setData($productData);
        }
        Mage::register('pmatch_data', $requestModel);
        $this
            ->_initAction()
            ->_setTitle($this->__($title))
            ->renderLayout()
        ;
        return $this;
    }

    public function selectProductAction()
    {
        $this
            ->_initAction()
            ->renderLayout()
        ;
    }

    public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('pmatch/adminhtml_products_grid')->toHtml()
        );
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function saveAction()
    {
        if (!$data = $this->getRequest()->getPost()) {
            Mage::getSingleton('adminhtml/session')->addError(
                Mage::helper('pmatch')->__('Unable to find request to save')
            );
            return $this->_redirect('*/*/');
        }

        $format = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_SHORT);
        $data['discount_use_before'] = Mage::app()->getLocale()
            ->utcDate(null, $data['discount_use_before'], true, $format)
            ->toString(Varien_Date::DATETIME_INTERNAL_FORMAT)
        ;

        $requestId = (int)$this->getRequest()->getParam('id');
        $requestModel = Mage::getModel('pmatch/requests')->load($requestId);
        $requestModel
            ->addData($data)
            ->setUpdateTime(Mage::getModel('core/date')->gmtDate())
        ;
        if (null === $requestModel->getId()) {
            $requestModel
                ->setCreatedTime(Mage::getModel('core/date')->gmtDate())
            ;
        }

        if (!$requestModel->getProductName()) {
            $product = Mage::getModel('catalog/product')->load($requestModel->getProductId());
            $requestModel
                ->setProductName($product->getName())
                ->setProductPrice($product->getPrice())
            ;
        }
        if (!$requestModel->getGuestCode()) {
            $generator = Mage::getSingleton('salesrule/coupon_codegenerator', array('length' => 5));
            $requestModel->setGuestCode(AW_Pmatch_Model_Requests::COUPON_PREFFIX . $generator->generateCode());
        }
        try {
            $requestModel->save();
            Mage::getSingleton('adminhtml/session')->addSuccess(
                Mage::helper('pmatch')->__('Request was successfully saved')
            );
            if ($this->getRequest()->getParam('send', false)) {
                if (Mage::helper('pmatch/config')->isNotificationEnabled()) {
                    Mage::getModel('pmatch/notifications')->send($requestModel);
                }
            }
            if ($this->getRequest()->getParam('back', false)) {
                return $this->_redirect('*/*/edit', array('id' => $requestModel->getId()));
            }
        } catch (Mage_Core_Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('pmatch')->__($e->getMessage()));
        }

        if (!$requestModel->getId() && $this->getRequest()->getParam('product', false)) {
            $product = Mage::getModel('catalog/product')->load($this->getRequest()->getParam('product'));
            $data["product_name"] = $product->getName();
            $data["product_price"] = $product->getPrice();
        }
        Mage::getSingleton('adminhtml/session')->setPmatchData($data);
        if ($requestModel->getId()) {
            return $this->_redirect('*/*/index', array('id' => $requestModel->getId()));
        }
        return $this->_redirect('*/*/new', array('product' => $this->getRequest()->getParam('product')));
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getParam('id') > 0) {
            try {
                $model = Mage::getModel('pmatch/requests');
                $model
                    ->setId($this->getRequest()->getParam('id'))
                    ->delete()
                ;
                Mage::getSingleton('adminhtml/session')->addSuccess(
                    Mage::helper('adminhtml')->__('Item was successfully deleted')
                );
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }

    public function massDeleteAction()
    {
        $pmatchIds = $this->getRequest()->getParam('pmatch');
        if (!is_array($pmatchIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
            return $this->_redirect('*/*/index');
        }

        try {
            foreach ($pmatchIds as $pmatchId) {
                $pmatch = Mage::getModel('pmatch/requests')->load($pmatchId);
                $pmatch->delete();
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(
                Mage::helper('adminhtml')->__(
                    'Total of %d record(s) were successfully deleted', count($pmatchIds)
                )
            );
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirect('*/*/index');
        return $this;
    }

    public function massStatusAction()
    {
        $pmatchIds = $this->getRequest()->getParam('pmatch');
        if (!is_array($pmatchIds)) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('adminhtml')->__('Please select item(s)'));
            return $this->_redirect('*/*/index');
        }
        try {
            foreach ($pmatchIds as $pmatchId) {
                Mage::getSingleton('pmatch/requests')
                    ->load($pmatchId)
                    ->setStatus($this->getRequest()->getParam('status'))
                    ->setIsMassupdate(true)
                    ->save();
            }
            Mage::getSingleton('adminhtml/session')->addSuccess(
                $this->__('Total of %d record(s) were successfully updated', count($pmatchIds))
            );
        } catch (Exception $e) {
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        }
        $this->_redirect('*/*/index');
        return $this;
    }

    protected function _setTitle($action)
    {
        if (method_exists($this, '_title')) {
            $this->_title($this->__('Price Match Requests'))->_title($this->__($action));
        }
        return $this;
    }
}