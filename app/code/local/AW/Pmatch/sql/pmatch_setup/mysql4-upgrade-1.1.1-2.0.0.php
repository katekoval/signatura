<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Pmatch
 * @version    2.1.5
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

$installer = $this;
$installer->startSetup();
$installer->run("
    ALTER TABLE {$this->getTable('pmatch/requests')}
    ADD `discount` decimal(12,2) NOT NULL,
    ADD `discount_status` int NOT NULL,
    ADD `customer_price` decimal(12,2) NOT NULL,
    ADD `guest_code` tinytext NOT NULL,
    ADD `discount_use_before` datetime NULL,
    ADD `product_price_original` decimal(12,2) NOT NULL;
");
$installer->endSetup();