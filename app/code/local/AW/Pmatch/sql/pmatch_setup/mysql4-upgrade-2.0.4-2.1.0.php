<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Pmatch
 * @version    2.1.5
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */

$installer = $this;
$installer->startSetup();

// Rename node "intergation" -> "integration"
$installer->run("
    UPDATE {$this->getTable('core_config_data')}
        SET path = REPLACE (path, 'intergation', 'integration')
        WHERE path LIKE 'pmatch/intergation%';
");

// Rename node "appearance_where" -> "request_form_type"
$installer->run("
    UPDATE {$this->getTable('core_config_data')}
        SET path = REPLACE (path, 'appearance_where', 'request_form_type')
        WHERE path = 'pmatch/appearance/appearance_where';
");

// Rename node "appearance_how" -> "is_insert_request_button_automatically"
$installer->run("
    UPDATE {$this->getTable('core_config_data')}
        SET path = REPLACE (path, 'appearance_how', 'is_insert_request_button_automatically')
        WHERE path = 'pmatch/appearance/appearance_how';
");

// Rename node "email_notifications" -> "send_to"
$installer->run("
    UPDATE {$this->getTable('core_config_data')}
        SET path = REPLACE (path, 'email_notifications', 'send_to')
        WHERE path = 'pmatch/notifications/email_notifications';
");

// Rename node "notifications_recipient" -> "sender"
$installer->run("
    UPDATE {$this->getTable('core_config_data')}
        SET path = REPLACE (path, 'notifications_recipient', 'sender')
        WHERE path = 'pmatch/notifications/notifications_recipient';
");

// Add new data to node "recipient" as copy from "sender"
$installer->run("
    INSERT INTO {$this->getTable('core_config_data')} (`config_id`, `scope`, `scope_id`, `path`, `value`)
        SELECT NULL, `scope`, `scope_id`, REPLACE (path, 'sender', 'recipient'), `value`
        FROM {$this->getTable('core_config_data')} WHERE path = 'pmatch/notifications/sender';
");

// Rename node "integration_aw_fbintegrator" -> "is_integration_with_fbi_enabled"
$installer->run("
    UPDATE {$this->getTable('core_config_data')}
        SET path = REPLACE (path, 'integration_aw_fbintegrator', 'is_integration_with_fbi_enabled')
        WHERE path = 'pmatch/integration/integration_aw_fbintegrator';
");
$installer->endSetup();