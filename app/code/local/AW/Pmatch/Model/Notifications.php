<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Pmatch
 * @version    2.1.5
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Pmatch_Model_Notifications
{
    public function send($request)
    {
        $sendTo = Mage::helper('pmatch/config')->getNotificationSendTo($request->getStoreId());
        $status = $request->getStatus();

        if (($sendTo == AW_Pmatch_Model_Source_Notification_Send::EMAIL_SENDS_TO_BOTH
            || $sendTo == AW_Pmatch_Model_Source_Notification_Send::EMAIL_SENDS_TO_ADMIN)
            && $status == AW_Pmatch_Model_Source_Status::PENDING
        ) {
            $this->_sendNotifyToAdmin($request);
        }

        if ($sendTo != AW_Pmatch_Model_Source_Notification_Send::EMAIL_SENDS_TO_ADMIN) {
            $this->_sendNotifyToCustomer($request);
        }
        return $this;
    }

    protected function _sendNotifyToAdmin($request)
    {
        if (null !== $this->_getEmailSubject($request)) {
            $recipient = Mage::helper('pmatch/config')->getRecipient($request->getStoreId());
            $sender = Mage::helper('pmatch/config')->getSender($request->getStoreId());
            $emailTemplates = Mage::helper('pmatch/config')->getEmailTemplates($request->getStoreId());
            $variables = $this->_prepareVariables($request);
            return $this->_sendNotify(
                $recipient['mail'], $recipient['name'], $sender['mail'], $sender['name'],
                $this->_getEmailSubject($request), $emailTemplates['admin'], $variables
            );
        }
        return false;
    }

    protected function _sendNotifyToCustomer($request)
    {
        if (null !== $this->_getEmailSubject($request)) {
            $sender = Mage::helper('pmatch/config')->getSender($request->getStoreId());
            $emailTemplates = Mage::helper('pmatch/config')->getEmailTemplates($request->getStoreId());
            $template = $emailTemplates['customer'];
            if ($request->getStatus() == AW_Pmatch_Model_Source_Status::APPROVED) {
                $template = $emailTemplates['approved'];
            }
            if ($request->getStatus() == AW_Pmatch_Model_Source_Status::REJECTED) {
                $template = $emailTemplates['rejected'];
            }
            $variables = $this->_prepareVariables($request);
            return $this->_sendNotify(
                $request->getCustomerEmail(), $request->getCustomerName(), $sender['mail'],
                $sender['name'], $this->_getEmailSubject($request), $template, $variables
            );
        }
        return false;
    }

    protected function _getEmailSubject($request)
    {
        $subject = null;
        switch ($request->getStatus()) {
            case AW_Pmatch_Model_Source_Status::PENDING :
                $subject = Mage::helper('pmatch')->__('Ny prismatch anmodning');
                break;
            case AW_Pmatch_Model_Source_Status::APPROVED :
                $subject = Mage::helper('pmatch')->__('Dit pris match er blevet godkendt');
                break;
            case AW_Pmatch_Model_Source_Status::REJECTED :
                $subject = Mage::helper('pmatch')->__('Dit prismatch er desværre ikke blevet godkendt');
                break;
        }
        return $subject;
    }

    protected function _sendNotify($toEmail, $toName, $fromEmail, $fromName, $subject, $template, $variables)
    {
        $locale = Mage::getSingleton('core/locale');
        $locale->emulate($variables['store_id']);

        $emailTemplate = Mage::getModel('core/email_template')->loadDefault($template);
        $emailTemplate->getProcessedTemplate($variables);
        $locale->revert();

        $emailTemplate
            ->setSenderName($fromName)
            ->setDesignConfig(array('area' => 'frontend', 'store' => $variables['store_id']))
            ->setSenderEmail($fromEmail)
            ->setTemplateSubject($subject)
        ;
        return $emailTemplate->send($toEmail, $toName, $variables);
    }

    private function _prepareVariables($request)
    {
        /** @var $requestTmp AW_Pmatch_Model_Requests */
        $requestTmp = clone $request;

        $createdTime = Mage::app()->getLocale()->date(
            $requestTmp->getCreatedTime(), Varien_Date::DATETIME_INTERNAL_FORMAT
        );
        $requestTmp->setCreatedTime($createdTime->toString(Varien_Date::DATETIME_INTERNAL_FORMAT));

        $updateTime = Mage::app()->getLocale()->date(
            $requestTmp->getUpdateTime(), Varien_Date::DATETIME_INTERNAL_FORMAT
        );
        $requestTmp->setUpdateTime($updateTime->toString(Varien_Date::DATETIME_INTERNAL_FORMAT));

        if (is_null($requestTmp->getDiscountUseBefore())) {
            $discountUseBefore = '';
        } else {
            $discountUseBeforeDate = Mage::app()->getLocale()->date(
                $requestTmp->getDiscountUseBefore(), Varien_Date::DATETIME_INTERNAL_FORMAT
            );
            $format = Mage::app()->getLocale()->getDateTimeFormat(Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM);
            $discountUseBefore = $discountUseBeforeDate->toString($format);
        }
        $currencyCode = Mage::app()->getStore()->getBaseCurrency()->getCode();

        $linkToCustomer = null;
        if (($requestTmp->getCustomerId() != null) && ($requestTmp->getCustomerId() != 0)) {
            $linkToCustomer = Mage::app()->getStore($request->getStoreId())->getUrl('pmatch/request');
        }
        $linkToRequest = Mage::helper("adminhtml")->getUrl(
            "adminhtml/awpmatch_pmatch/edit/", array("id" => $request->getId())
        );
        $variables = array(
            'request'           => $requestTmp,
            'customerName'      => trim($request->getCustomerName()),
            'productName'       => $request->getProductName(),
            'discount'          => $currencyCode . ' ' . $request->getDiscount(),
            'customerPhone'     => trim($request->getCustomerPhone()),
            'linkToRequest'     => $linkToRequest,
            'discountUseBefore' => $discountUseBefore,
            'discountCode'      => $requestTmp->getGuestCode(),
            'productLink'       => Mage::helper('pmatch')->getProductUrlByRequest($request),
            'linkToCustReq'     => $linkToCustomer,
            'customerId'        => $requestTmp->getCustomerId(),
            'store_id'          => $request->getStoreId()
        );
        return $variables;
    }
}
