<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Pmatch
 * @version    2.1.5
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Pmatch_Model_Source_Discountstatus extends AW_Pmatch_Model_Source_Abstract
{
    const UNLIMITED = 0;
    const ONLY_ONCE = 1;
    const USED      = 2;

    const CODE_UNLIMITED = 'unlimited';
    const CODE_ONLY_ONCE = 'only_once';
    const CODE_USED      = 'used';

    const LABEL_UNLIMITED = 'Unlimited';
    const LABEL_ONLY_ONCE = 'Only once';
    const LABEL_USED      = 'Used';

    public function getShortOptionArray()
    {
        $helper = Mage::helper("pmatch");
        $discountStatuses = array(
            self::UNLIMITED => $helper->__(self::LABEL_UNLIMITED),
            self::ONLY_ONCE => $helper->__(self::LABEL_ONLY_ONCE),
            self::USED      => $helper->__(self::LABEL_USED),
        );
        return $discountStatuses;
    }

    public function getStatusIdByCode($code)
    {
        $statusId = null;
        switch ($code) {
            case self::CODE_UNLIMITED:
                $statusId = self::UNLIMITED;
                break;
            case self::CODE_ONLY_ONCE:
                $statusId = self::ONLY_ONCE;
                break;
            case self::CODE_USED:
                $statusId = self::USED;
                break;
        }
        return $statusId;
    }

    public function getStatusLabelByCode($code)
    {
        $label = '';
        $statusId = $this->getStatusIdByCode($code);
        $options = $this->getShortOptionArray();
        if (!is_null($statusId) && array_key_exists($statusId, $options)) {
            $label =  $options[$statusId];
        }
        return $label;
    }
}