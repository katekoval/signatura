<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Pmatch
 * @version    2.1.5
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Pmatch_Model_Source_Form_Type
{
    const POPUP         = 'popup';
    const SEPARATE_PAGE = 'separate_page';
    const CUSTOM        = 'custom';

    public function toOptionArray()
    {
        return array(
            array(
                'value' => self::POPUP,
                'label' => Mage::helper('pmatch')->__('Popup'),
            ),
            array(
                'value' => self::SEPARATE_PAGE,
                'label' => Mage::helper('pmatch')->__('Separate page'),
            ),
            array(
                'value' => self::CUSTOM,
                'label' => Mage::helper('pmatch')->__('Custom (manual injection)'),
            ),
        );
    }
}