<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Pmatch
 * @version    2.1.5
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Pmatch_Model_Source_Status extends AW_Pmatch_Model_Source_Abstract
{
    const PENDING  = 1;
    const APPROVED = 2;
    const REJECTED = 3;

    const CODE_PENDING  = 'pending';
    const CODE_APPROVED = 'approved';
    const CODE_REJECTED = 'rejected';

    const LABEL_PENDING  = 'Pending';
    const LABEL_APPROVED = 'Approved';
    const LABEL_REJECTED = 'Rejected';

    public function getShortOptionArray()
    {
        $helper = Mage::helper("pmatch");
        $statuses = array(
            self::PENDING  => $helper->__(self::LABEL_PENDING),
            self::APPROVED => $helper->__(self::LABEL_APPROVED),
            self::REJECTED => $helper->__(self::LABEL_REJECTED),
        );
        return $statuses;
    }

    public function getStatusIdByCode($code)
    {
        $statusId = null;
        switch ($code) {
            case self::CODE_APPROVED:
                $statusId = self::APPROVED;
                break;
            case self::CODE_PENDING:
                $statusId = self::PENDING;
                break;
            case self::CODE_REJECTED:
                $statusId = self::REJECTED;
                break;
        }
        return $statusId;
    }
}