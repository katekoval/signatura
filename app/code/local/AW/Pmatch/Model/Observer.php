<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Pmatch
 * @version    2.1.5
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Pmatch_Model_Observer
{
    const TEMPLATE_ITEM_NAME = '{item_name}';
    const TEMPLATE_ITEM_PRICE = '{item_price}';
    const TEMPLATE_ITEM_NEW_PRICE = '{item_new_price}';
    const TEMPLATE_ITEM_LINK = '{item_link}';

    //Here we should check is code is used and valid
    //If it valid progation stop
    public function checkPmatchCoupon($observer)
    {
        if (!Mage::helper('pmatch')->isModuleOutputEnabled()) {
            return $this;
        }
        $controller = Mage::app()->getFrontController();
        $coupon = Mage::app()->getFrontController()->getRequest()->getParam('coupon_code');
        if (strpos($coupon, AW_Pmatch_Model_Requests::COUPON_PREFFIX) !== 0) {
            if (
                Mage::app()->getFrontController()->getRequest()->getParam('remove')
                || Mage::helper('pmatch')->isOtherCouponsAllowed()
            ) {
                return $this;
            }
            Mage::getSingleton('checkout/session')->addError(
                Mage::helper('pmatch')->__(
                    'You can not apply this coupon since you\'ve been granted the Price Match discount.'
                )
            );
        } else {
            $request = Mage::getSingleton('pmatch/requests');
            try {
                $request->applyCoupon($coupon);
                Mage::getSingleton('checkout/session')->addSuccess(
                    Mage::helper('pmatch')->__('Price Match coupon "%s" has been applied.', $coupon)
                );
            } catch(Mage_Core_Exception $e) {
                Mage::getSingleton('checkout/session')->addError($e->getMessage());
            } catch (Exception $e) {
                Mage::getSingleton('checkout/session')->addError(
                    Mage::helper('pmatch')->__($e->getMessage())
                );
            }
        }
        $controllerAction = $observer->getControllerAction();
        $controllerAction->setFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true);
        return $controller->getResponse()->setRedirect(Mage::getUrl('checkout/cart'));
    }

    public function checkPmatchCouponOSC($observer)
    {
        if (!Mage::helper('pmatch')->isModuleOutputEnabled()) {
            return $this;
        }
        $controller = Mage::app()->getFrontController();
        $coupon = Mage::app()->getFrontController()->getRequest()->getParam('coupon_code');
        if (strpos($coupon, AW_Pmatch_Model_Requests::COUPON_PREFFIX) !== 0) {
            if (
                Mage::app()->getFrontController()->getRequest()->getParam('remove')
                || Mage::helper('pmatch')->isOtherCouponsAllowed()
            ) {
                return $this;
            }
            $result = array(
                'success' => true,
                'coupon_applied' => false,
                'messages' => array(),
                'blocks' => array(),
                'grand_total' => "",
            );
            $result['success'] = false;
            $result['messages'][] = 'You can not apply this coupon since you\'ve been granted the Price Match discount.';
            Mage::getSingleton('checkout/session')->addError(
                Mage::helper('pmatch')->__(
                    'You can not apply this coupon since you\'ve been granted the Price Match discount.'
                )
            );
        } else {
            $request = Mage::getSingleton('pmatch/requests');
            $result = array(
                'success' => true,
                'coupon_applied' => false,
                'messages' => array(),
                'blocks' => array(),
                'grand_total' => "",
            );
            try {
                $request->applyCoupon($coupon);
                $result['coupon_applied'] = true;
                $result['messages'][] = Mage::helper('pmatch')->__('Price Match coupon "%s" has been applied.', $coupon);
            } catch(Mage_Core_Exception $e) {
                $result['success'] = false;
                $result['messages'][] = $e->getMessage();
                Mage::getSingleton('checkout/session')->addError($e->getMessage());
            } catch (Exception $e) {
                $result['success'] = false;
                $result['messages'][] = Mage::helper('pmatch')->__($e->getMessage());
            }
        }
        $controllerAction = $observer->getControllerAction();
        $controllerAction->setFlag('', Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true);
        return $controller->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    public function getFinalPrice($event)
    {
        return $this->_prepareFinalPrice($event->getProduct());
    }

    protected function _prepareFinalPrice($product)
    {
        $requestModel = false;
        $requestSingleton = Mage::getSingleton('pmatch/requests');
        if (Mage::helper('pmatch/config')->isApplyDiscountAutomatically()
            && Mage::getSingleton('customer/session')->isLoggedIn()
            && Mage::getSingleton('customer/session')->getCustomer()
            && null !== Mage::getSingleton('customer/session')->getCustomer()->getId()
        ) {
            $requestModel = Mage::helper('pmatch')->getCustomerRequestByProductId(
                $product->getId(), Mage::getSingleton('customer/session')->getCustomer()->getId()
            );
            if ($requestModel) {
                try {
                    $requestSingleton->applyCoupon($requestModel->getGuestCode());
                } catch (Exception $e) {
                    //already applied
                    $requestModel = false;
                }
            }
        }

        //check applied coupons
        if (false === $requestModel) {
            $requests = Mage::helper('pmatch')->getAppliedRequests();
            if ($requests) {
                foreach ($requests as $request) {
                    if ($request->getProductId() == $product->getId()) {
                        $requestModel = $request;
                        break;
                    }
                }
            }
        }

        if ($requestModel && $requestModel->getId()) {
            if ($requestModel->isExpired()) {
                $this->_wipeAppliedRequests();
            } else {
                if ($product->getData('final_price') !== null) {
                    $product->setFinalPrice($product->getData('final_price') - $requestModel->getDiscount());
                }
                $currentProduct = Mage::registry('current_product');
                if ($currentProduct) {
                    $currentProduct->setData('pmatch_notice', 1);
                }
            }
        }
        return $this;
    }

    private function _correctGroupedPrices($product) {
        if ($product->isGrouped()) {
            $initialMinimalPrice = $product->getMinimalPrice();
            $appliedRequests = Mage::helper('pmatch')->getAppliedRequests();
            if (!empty($appliedRequests)) {
                foreach ($product->getGroupedLinkCollection() as $childProduct) {
                    foreach ($appliedRequests as $request) {
                        if ($childProduct->getLinkedProductId() == $request->getProductId()) {
                            $supposedMinimalPrice = $request->getProductPrice() - $request->getDiscount();
                            if ($supposedMinimalPrice < $initialMinimalPrice) {
                                $product->setMinimalPrice($supposedMinimalPrice);
                            }
                        }
                    }
                }
            }
        }
        elseif ($product->getTypeId() == 'bundle') {
            $initialMinimalPrice = $product->getMinPrice();
            $appliedRequests = Mage::helper('pmatch')->getAppliedRequests();
            if (!empty($appliedRequests)) {
                $children = $product->getTypeInstance(true)->getChildrenIds($product->getId(), false);
                if (!empty($children) && isset($children[0]) && isset($children[1])) {
                    $children = array_merge($children[0], $children[1]);
                }
                $totalDiscounted = 0;
                foreach ($children as $childProductID) {
                    foreach ($appliedRequests as $request) {
                        if ($childProductID == $request->getProductId()) {
                            $supposedMinimalPrice = $request->getProductPrice() - $request->getDiscount();
                            $totalDiscounted += $request->getDiscount();
                            if ($supposedMinimalPrice < $initialMinimalPrice) {
                                $product->setMinPrice($supposedMinimalPrice);
                            }
                        }
                    }
                }
                if ($totalDiscounted > 0) {
                    $product->setMaxPrice(max(0, $product->getMaxPrice() - $totalDiscounted));
                }
            }

        }
        return $this;
    }

    public function fbLink($observer)
    {
        if (Mage::helper('pmatch/config')->isIntegrationWithFacebookLinkEnabled()) {

            $order = $observer->getEvent()->getObserver()->getOrder();
            $params = $observer->getEvent()->getParams();
            $store = Mage::app()->getStore();

            $urlConfig = array(
                '_secure'       => Mage::helper('fbintegrator')->isSecure(),
                '_use_rewrite'  => Mage::helper('fbintegrator')->useRewrite(),
                '_store_to_url' => Mage::helper('fbintegrator')->addCode(),
            );
            $storeLink = Mage::getUrl('', $urlConfig);

            $productCount = 0;
            $description = '';
            $attachment = array();
            $allVisibleItems = $order->getAllVisibleItems();
            $template = Mage::helper('pmatch/config')->getWallTemplate();
            foreach ($allVisibleItems as $item) {
                if (Mage::helper('pmatch')->getValueFromActiveRequest($item->getProductId(), 'discount')) {
                    $productCount++;
                    if (Mage::helper('fbintegrator')->useRewrite()) {
                        $productLink = Mage::helper('fbintegrator')->getProductRewriteUrl($item->getProductId());
                    } else {
                        $productLink = 'catalog/product/view/id/' . $item->getProductId();
                    }

                    $basePrice = $item->getBasePrice();
                    $discountPrice = Mage::helper('pmatch')->getValueFromActiveRequest(
                        $item->getProductId(), 'discount'
                    );
                    $productInfo = array(
                        self::TEMPLATE_ITEM_NAME      => $item->getName(),
                        self::TEMPLATE_ITEM_PRICE     => $store->convertPrice($basePrice + $discountPrice, true, false),
                        self::TEMPLATE_ITEM_NEW_PRICE => $store->convertPrice($basePrice, true, false),
                        self::TEMPLATE_ITEM_LINK      => $storeLink . $productLink,
                    );

                    $description .= ((strlen($description) > 0) ? '<center></center>' : ''); //facebook line break
                    $description .= str_replace(array_keys($productInfo), array_values($productInfo), $template);

                    if (Mage::helper('fbintegrator')->postImagesToWall()) {
                        $attachment['media'][] = array(
                            'type' => 'image',
                            'src'  => Mage::getModel('catalog/product')->load($item->getProductId())->getImageUrl(),
                            'href' => $storeLink . $productLink,
                        );
                    }
                }
            }

            if ($productCount > 0) {
                $attachment['description'] = $description;
                $params->setMessage(
                    Mage::helper('pmatch')->__(
                        "I've just purchased %s item(s) at Magento Store %s", $productCount, $storeLink
                    )
                );
                $params->setAttachment($attachment);
            }
        }
    }

    public function afterPlaceOrder($event)
    {
        $order = $event->getEvent()->getOrder();
        $allVisibleItems = $order->getAllVisibleItems();
        foreach ($allVisibleItems as $item) {
            $requestId = Mage::helper('pmatch')->getValueFromActiveRequest($item->getProductId(), 'request_id');
            $requestModel = Mage::getModel('pmatch/requests')->load($requestId);
            if (null === $requestModel->getId()) {
                continue;
            }
            if ($requestModel->getDiscountStatus() == AW_Pmatch_Model_Source_Discountstatus::ONLY_ONCE) {
                $requestModel
                    ->setDiscountStatus(AW_Pmatch_Model_Source_Discountstatus::USED)
                    ->save()
                ;
                Mage::getSingleton('customer/session')->setAppliedRequests(null);
            }
        }
    }

    public function catalogProductCollectionLoadAfter($observer)
    {
        $collection = $observer->getEvent()->getCollection();
        foreach ($collection as $product) {
            $this->_prepareFinalPrice($product);
        }
        foreach ($collection as $product) {
            $this->_correctGroupedPrices($product);
        }
        return $this;
    }

    public function checkOtherCouponsBlock($observer)
    {
        if (Mage::helper('pmatch')->isOtherCouponsDenied()) {
            $update = $observer->getEvent()->getLayout()->getUpdate();
            $update->addHandle('pmatch_remove_coupon_block');
        }
        return $this;
    }

    public function wipeAppliedRequests($observer)
    {
        return $this->_wipeAppliedRequests();
    }

    protected function _wipeAppliedRequests()
    {
        Mage::helper('pmatch')->unsAppliedRequests();
        /** @var AW_Pmatch_Model_Mysql4_Requests_Collection $requests */
        $customer = Mage::getSingleton('customer/session');
        if ($customer && $customer->getId()) {
            $requests = Mage::helper('pmatch')->getAllActiveRequestByCustomerId($customer->getId());
            if (!empty($requests)) {
                Mage::helper('pmatch')->setNoCacheCookie();
                return $this;
            }
        }
        Mage::helper('pmatch')->deleteNoCacheCookie();
        return $this;
    }

    public function banProductFPC($observer)
    {
        $pmRequests = Mage::getSingleton('customer/session')->getAppliedRequests();
        if (!empty($pmRequests) && is_array($pmRequests)) {
            $productId = Mage::app()->getRequest()->getParam('id');
            if ($productId) {
                foreach ($pmRequests as $pmRequest) {
                    if (
                        $productId == $pmRequest->getProductId()
                        && $pmRequest->getStatus() == AW_Pmatch_Model_Source_Status::APPROVED
                        && $pmRequest->getDiscountStatus() != AW_Pmatch_Model_Source_Discountstatus::USED
                    ) {
                        Mage::app()->getCacheInstance()->banUse('full_page');
                        break;
                    }
                }
            }
        }
        return $this;
    }

    public function banCategoryFPC($observer)
    {
        $pmRequests = Mage::getSingleton('customer/session')->getAppliedRequests();
        if (!empty($pmRequests)) {
            Mage::app()->getCacheInstance()->banUse('full_page');
        }
        return $this;
    }

}
