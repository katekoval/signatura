<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Pmatch
 * @version    2.1.5
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Pmatch_Model_Requests extends Mage_Core_Model_Abstract
{
    const COUPON_PREFFIX = 'PMATCH';

    public function _construct()
    {
        parent::_construct();
        $this->_init('pmatch/requests');
    }

    public function applyCoupon($code)
    {
        if ($requestModel = $this->_verifyAndGetRequest($code)) {
            $appliedRequests = Mage::helper('pmatch')->getAppliedRequests();
            $appliedRequests[$requestModel->getProductId()] = clone $requestModel;
            Mage::getSingleton('customer/session')->setAppliedRequests($appliedRequests);
            Mage::helper('pmatch')->setNoCacheCookie();
        }
        return $this;
    }

    public function isExpired()
    {
        if ($this->getId()) {
            $expired = Mage::app()->getLocale()->date(strtotime($this->getDiscountUseBefore()), null, null);
            $now = Mage::app()->getLocale()->date();
            return $now->getTimestamp() > $expired->getTimestamp();
        }
        return true;
    }

    protected function _verifyAndGetRequest($coupon)
    {
        $appliedRequests = Mage::helper('pmatch')->getAppliedRequests();
        $requestModel = $this->load($coupon, 'guest_code');
        if (null !== $requestModel->getId()
            && $requestModel->getDiscountStatus() != AW_Pmatch_Model_Source_Discountstatus::USED
            && $requestModel->getStatus() == AW_Pmatch_Model_Source_Status::APPROVED
            && !array_key_exists($requestModel->getProductId(), $appliedRequests)
        ) {
            if ($requestModel->isExpired()) {
                throw new Exception('You can not apply this coupon because it has expired');
            }
            return $requestModel;
        }
        throw new Exception('You can apply only one Price Match coupon code for each product');
    }

    protected function _beforeSave()
    {
        if (!Mage::helper('pmatch')->isProductTypeValid($this->getProductId())) {
            throw new Exception('You can not create Price Match request for this product type.');
        }
        if ($this->getId() == null && Mage::helper('pmatch')->isApprovedRequest($this->getProductId())) {
            throw new Exception('You can not place more than one active Price Match request for the same product.');
        }
        if ($this->getStatus() == AW_Pmatch_Model_Source_Status::PENDING
            && Mage::helper('pmatch/config')->getDiscountAutomaticAcceptRequests($this->getStoreId())
        ) {
            $priceInterval = 100;
            if ($this->getCompetitorPrice() > 0 && $this->getProductPrice() > 0) {
                $priceInterval = 100 - ($this->getCompetitorPrice() / $this->getProductPrice()) * 100;
            }
            if ($priceInterval <= Mage::helper('pmatch/config')->getDiscountPriceInterval($this->getStoreId())) {
                $generator = Mage::getSingleton('salesrule/coupon_codegenerator', array('length' => 5));
                $this
                    ->setDiscount(round($this->getProductPrice() - $this->getCompetitorPrice(), 2))
                    ->setDiscountStatus(AW_Pmatch_Model_Source_Discountstatus::ONLY_ONCE)
                    ->setStatus(AW_Pmatch_Model_Source_Status::APPROVED)
                    ->setGuestCode(AW_Pmatch_Model_Requests::COUPON_PREFFIX . $generator->generateCode())
                ;
            }
        }
        return parent::_beforeSave();
    }
}
