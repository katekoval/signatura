<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Pmatch
 * @version    2.1.5
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Pmatch_Model_Mysql4_Requests_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('pmatch/requests');
    }

    public function addStoreFilter($storeIds)
    {
        if (!is_array($storeIds)) {
            $storeIds = array(0, $storeIds);
        }
        $this->addFieldToFilter('store_id', array('in' => $storeIds));
        return $this;
    }

    public function addProductFilter($productIds)
    {
        $this->addFieldToFilter('product_id', array('in' => $productIds));
        return $this;
    }

    public function addCustomerFilter($customerId)
    {
        $this->addFieldToFilter('customer_id', array('eq' => $customerId));
        return $this;
    }

    public function addStatusFilter($status)
    {
        $this->addFieldToFilter('status', array('eq' => $status));
        return $this;
    }

    public function addEndDateFilter(Zend_Date $date = null)
    {
        /** $date Zend_Date */
        if (is_null($date)) {
            $date = new Zend_Date();
        }
        $this->addFieldToFilter(
            'discount_use_before', array('gt' => $date->toString(AW_Pmatch_Helper_Data::ISO_DATETIME_FORMAT))
        );
        return $this;
    }

    public function joinCustomerTable()
    {
        $this
            ->getSelect()
            ->joinLeft(array('ce' => $this->getTable('customer/entity')),
                'ce.entity_id = main_table.customer_id',
                array('customer_group_id' => 'IF(ce.group_id IS NULL, 0, ce.group_id)')
            )
        ;
        return $this;
    }
}
