<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Pmatch
 * @version    2.1.5
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Pmatch_Helper_Config extends Mage_Core_Helper_Abstract
{
    /**
     * "Request Form Type" from system config
     */
    const APPEARANCE_REQUEST_FORM_TYPE = 'pmatch/appearance/request_form_type';

    /**
     * "Insert request button automatically on product page" from system config
     */
    const APPEARANCE_IS_INSERT_REQUEST_BUTTON = 'pmatch/appearance/is_insert_request_button_automatically';

    /**
     * "Request button label" from system config
     */
    const APPEARANCE_REQUEST_BUTTON_LABEL = 'pmatch/appearance/button_title';

    /**
     * "Text before form" from system config
     */
    const APPEARANCE_TEXT_BEFORE_FORM = 'pmatch/appearance/text_before';

    /**
     * "Text after form" from system config
     */
    const APPEARANCE_TEXT_AFTER_FORM            = 'pmatch/appearance/text_after';
    const APPEARANCE_SHOW_OPTION_CUSTOMER_PRICE = 'pmatch/appearance/show_option_customer_price';

    /**
     * "Send To" from system config
     */
    const NOTIFICATION_SEND_TO = 'pmatch/notifications/send_to';

    /**
     * "Sender" from system config
     */
    const NOTIFICATION_SENDER = 'pmatch/notifications/sender';

    /**
     * "Recipient" from system config
     */
    const NOTIFICATION_RECIPIENT = 'pmatch/notifications/recipient';

    /**
     * "Is Integration with Facebook Link Enabled" from system config
     */
    const INTEGRATION_IS_ENABLED_WITH_FBI = 'pmatch/integration/is_integration_with_fbi_enabled';

    /**
     * "Wall post" from system config
     */
    const INTEGRATION_WALL_TEMPLATE = 'pmatch/integration/wall_template';

    /**
     * "Apply discount coupons automatically" from system config
     */
    const DISCOUNT_APPLY_AUTOMATICALLY = 'pmatch/discounts/automatic';

    /**
     * "Block other coupons, if Price Match coupon was applied" from system config
     */
    const DISCOUNT_DENY_USE_OF_OTHER_COUPONS = 'pmatch/discounts/block';
    const DISCOUNT_AUTOMATIC_ACCEPT_REQUESTS     = 'pmatch/discounts/automatic_accept_requests';
    const DISCOUNT_PRICE_INTERVAL                = 'pmatch/discounts/price_interval';

    /**
     * Email templates from system config
     */
    const EMAIL_TEMPLATE_TO_ADMIN = 'pmatch/email/to_admin_email';
    const EMAIL_TEMPLATE_TO_CUSTOMER = 'pmatch/email/to_customer_email';
    const EMAIL_TEMPLATE_APPROVED = 'pmatch/email/approved_email';
    const EMAIL_TEMPLATE_REJECTED = 'pmatch/email/rejected_email';


    /**
     * @param string $moduleName Full module name
     * @return bool
     */
    public function isEnabled($moduleName = null)
    {
        return $this->isModuleOutputEnabled($moduleName);
    }

    /**
     * @param null|int|Mage_Core_Model_Store $store
     * @return string
     */
    public function getRequestFormType($store = null)
    {
        return Mage::getStoreConfig(self::APPEARANCE_REQUEST_FORM_TYPE, $store);
    }

    /**
     * @param null|int|Mage_Core_Model_Store $store
     * @return bool
     */
    public function isInsertRequestButton($store = null)
    {
        return Mage::getStoreConfigFlag(self::APPEARANCE_IS_INSERT_REQUEST_BUTTON, $store);
    }

    /**
     * @param null|int|Mage_Core_Model_Store $store
     * @return string
     */
    public function getButtonLabel($store = null)
    {
        return Mage::getStoreConfig(self::APPEARANCE_REQUEST_BUTTON_LABEL, $store);
    }

    /**
     * @param null|int|Mage_Core_Model_Store $store
     * @return string
     */
    public function getHtmlBeforeForm($store = null)
    {
        return Mage::getStoreConfig(self::APPEARANCE_TEXT_BEFORE_FORM, $store);
    }

    /**
     * @param null|int|Mage_Core_Model_Store $store
     * @return string
     */
    public function getHtmlAfterForm($store = null)
    {
        return Mage::getStoreConfig(self::APPEARANCE_TEXT_AFTER_FORM, $store);
    }

    // TODO: add yesno element to config
    /**
     * @param null|int|Mage_Core_Model_Store $store
     * @return bool
     */
    public function isNotificationEnabled($store = null)
    {
        $recipient = $this->getNotificationSendTo($store);
        return $recipient !== AW_Pmatch_Model_Source_Notification_Send::EMAIL_SENDS_TO_DISABLE;
    }

    /**
     * @param null|int|Mage_Core_Model_Store $store
     * @return string
     */
    public function getNotificationSendTo($store = null)
    {
        return Mage::getStoreConfig(self::NOTIFICATION_SEND_TO, $store);
    }

    /**
     * @param null|int|Mage_Core_Model_Store $store
     * @return string
     */
    public function getSenderCode($store = null)
    {
        return Mage::getStoreConfig(self::NOTIFICATION_SENDER, $store);
    }

    /**
     * @param null|int|Mage_Core_Model_Store $store
     * @return array
     */
    public function getSender($store = null)
    {
        $senderCode = $this->getSenderCode($store);
        return $this->_getStoreEmailAddressByCode($senderCode, $store);
    }

    /**
     * @param null|int|Mage_Core_Model_Store $store
     * @return string
     */
    public function getRecipientCode($store = null)
    {
        return Mage::getStoreConfig(self::NOTIFICATION_RECIPIENT, $store);
    }

    /**
     * @param null|int|Mage_Core_Model_Store $store
     * @return array
     */
    public function getRecipient($store = null)
    {
        $recipientCode = $this->getRecipientCode($store);
        return $this->_getStoreEmailAddressByCode($recipientCode, $store);
    }

    /**
     * @param null|int|Mage_Core_Model_Store $store
     * @return bool
     */
    public function isIntegrationWithFacebookLinkEnabled($store = null)
    {
        return Mage::getStoreConfigFlag(self::INTEGRATION_IS_ENABLED_WITH_FBI, $store);
    }

    /**
     * @param null|int|Mage_Core_Model_Store $store
     * @return string
     */
    public function getWallTemplate($store = null)
    {
        return Mage::getStoreConfig(self::INTEGRATION_WALL_TEMPLATE, $store);
    }

    /**
     * @param null|int|Mage_Core_Model_Store $store
     * @return bool
     */
    public function isApplyDiscountAutomatically($store = null)
    {
        return Mage::getStoreConfigFlag(self::DISCOUNT_APPLY_AUTOMATICALLY, $store);
    }

    /**
     * @param null|int|Mage_Core_Model_Store $store
     * @return bool
     */
    public function isDenyUseOfOtherCoupons($store = null)
    {
        return Mage::getStoreConfigFlag(self::DISCOUNT_DENY_USE_OF_OTHER_COUPONS, $store);
    }

    /**
     * @param null|int|Mage_Core_Model_Store $store
     * @return array
     */
    public function getEmailTemplates($store = null)
    {
        return array(
            'admin'    => Mage::getStoreConfig(self::EMAIL_TEMPLATE_TO_ADMIN, $store),
            'customer' => Mage::getStoreConfig(self::EMAIL_TEMPLATE_TO_CUSTOMER, $store),
            'approved' => Mage::getStoreConfig(self::EMAIL_TEMPLATE_APPROVED, $store),
            'rejected' => Mage::getStoreConfig(self::EMAIL_TEMPLATE_REJECTED, $store),
        );
    }

    /**
     * @param string                         $code
     * @param null|int|Mage_Core_Model_Store $store
     * @return array
     */
    protected function _getStoreEmailAddressByCode($code, $store = null)
    {
        return array(
            'name' => Mage::getStoreConfig('trans_email/ident_' . $code . '/name', $store),
            'mail' => Mage::getStoreConfig('trans_email/ident_' . $code . '/email', $store),
        );
    }

    public function getDiscountAutomaticAcceptRequests($store = null)
    {
        return Mage::getStoreConfig(self::DISCOUNT_AUTOMATIC_ACCEPT_REQUESTS, $store);
    }

    public function getDiscountPriceInterval($store = null)
    {
        return (int)Mage::getStoreConfig(self::DISCOUNT_PRICE_INTERVAL, $store);
    }

    public function getCanShowOptionCustomerPrice($store = null)
    {
        return Mage::getStoreConfig(self::APPEARANCE_SHOW_OPTION_CUSTOMER_PRICE, $store);
    }
}
