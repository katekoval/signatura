<?php
/**
 * aheadWorks Co.
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://ecommerce.aheadworks.com/AW-LICENSE.txt
 *
 * =================================================================
 *                 MAGENTO EDITION USAGE NOTICE
 * =================================================================
 * This software is designed to work with Magento community edition and
 * its use on an edition other than specified is prohibited. aheadWorks does not
 * provide extension support in case of incorrect edition use.
 * =================================================================
 *
 * @category   AW
 * @package    AW_Pmatch
 * @version    2.1.5
 * @copyright  Copyright (c) 2010-2012 aheadWorks Co. (http://www.aheadworks.com)
 * @license    http://ecommerce.aheadworks.com/AW-LICENSE.txt
 */


class AW_Pmatch_Helper_Data extends Mage_Core_Helper_Abstract
{
    const ISO_DATE_FORMAT = 'yyyy-MM-dd';
    const ISO_DATETIME_FORMAT = 'yyyy-MM-dd HH-mm-ss';

    public static $identity = array();

    public function convertPrice($price)
    {
        $price = preg_replace("/[^0-9\.,Ee\-+]/", '', $price);
        if ($price = floatval($price)) {
            $store = Mage::app()->getStore();
            $currentCurrency = Mage::getModel('directory/currency')->load(
                $store->getCurrentCurrencyCode(), 'currency_code'
            );
            $baseCurrency = Mage::getModel('directory/currency')->load($store->getBaseCurrencyCode(), 'currency_code');

            $rate = $baseCurrency->getRate($currentCurrency->getCurrencyCode());
            $backRate = $currentCurrency->getRate($baseCurrency->getCurrencyCode());

            if ($backRate === false) {
                $backRate = 1 / $rate;
            }
            $price = (float)sprintf('%.2f', $price * $backRate);
        }
        return $price;
    }

    public function getPMatchButton()
    {
        $buttonBlock = Mage::app()->getLayout()->createBlock('pmatch/button');
        $buttonBlock->setTemplate('pmatch/request_button.phtml');
        return $buttonBlock->toHtml();
    }

    public function isUrl($url)
    {
        $urlPattern = "#^\b(([\w-]+://?|www[.])[^\s()<>]+(?:\([\w\d]+\)|([^[:punct:]\s]|/)))$#si";
        return preg_match($urlPattern, $url);
    }

    public function getCustomerActiveRequestsCollection($customerId = null, $storeId = null)
    {
        if (null === $storeId) {
            $storeId = Mage::app()->getStore()->getId();
        }

        $collection = Mage::getModel('pmatch/requests')->getCollection()
            ->addStatusFilter(AW_Pmatch_Model_Source_Status::APPROVED)
            ->addStoreFilter($storeId)
            ->addFieldToFilter('discount', array('gt' => '0'))
            ->addFieldToFilter('discount_status', array('neq' => AW_Pmatch_Model_Source_Discountstatus::USED))
            ->addOrder('discount', Zend_Db_Select::SQL_DESC);

        /* Direct SQL for old Magento versions. Problem with addFieldToFilter() first observed on 1.4.x.x, none on 1.7.x.x and later */
        if (version_compare(Mage::getVersion(), '1.7.0.2', 'ge')) {
            $collection
                ->addFieldToFilter(
                    array('discount_use_before', 'discount_use_before'),
                    array(
                        array('null' => true),
                        array('gteq' => Mage::getModel('core/date')->gmtDate())
                    )
                );
        } else {
            $collection
                ->getSelect()
                ->where(
                    "discount_use_before IS NULL OR discount_use_before>='"
                    . Mage::getModel('core/date')->gmtDate() . "'"
                );
        }

        if (null !== $customerId) {
            $collection->addCustomerFilter($customerId);
        }
        return $collection;
    }

    public function getProductUrlByRequest($request)
    {
        $product = Mage::getModel('catalog/product')->setStoreId($request->getStoreId())->load(
            $request->getProductId()
        );
        return $product->getProductUrl();
    }

    public function getAppliedRequests()
    {
        $requests = Mage::getSingleton('customer/session')->getAppliedRequests();
        if (!is_array($requests)) {
            $requests = array();
        }
        return $requests;
    }

    public function unsAppliedRequests() {
        return Mage::getSingleton('customer/session')->unsAppliedRequests();
    }

    public function getAllActiveRequestByCustomerId($customerId = null)
    {
        $allActiveRequests = $this->getAppliedRequests();
        $collection = $this->getCustomerActiveRequestsCollection($customerId);
        foreach ($collection as $item) {
            if (array_key_exists($item->getProductId(), $allActiveRequests)) {
                continue;
            }
            $allActiveRequests[$item->getProductId()] = $item;
        }
        return $allActiveRequests;
    }

    public function getCustomerRequestByProductId($productId, $customerId)
    {
        $activeRequests = $this->getAllActiveRequestByCustomerId($customerId);
        $requestModel = false;
        if (count($activeRequests) > 0 && array_key_exists($productId, $activeRequests)) {
            $requestModel = $activeRequests[$productId];
        }
        return $requestModel;
    }

    public function getValueFromActiveRequest($productId, $key = null)
    {
        $activeRequests = $this->getAllActiveRequestByCustomerId();
        if (null === $productId || !array_key_exists($productId, $activeRequests)) {
            return null;
        }
        return $activeRequests[$productId]->getData($key);
    }

    public function isProductTypeValid($product)
    {
        if (!$product instanceof Mage_Catalog_Model_Product) {
            $product = Mage::getModel('catalog/product')->load($product);
        }
        return (
            !$product->isGrouped()
            && $product->getTypeId() !== Mage_Catalog_Model_Product_Type::TYPE_BUNDLE
        );
    }

    public function isApprovedRequest($product)
    {
        if (!$product instanceof Mage_Catalog_Model_Product) {
            $product = Mage::getModel('catalog/product')->load($product);
        }
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        if (
            $product
            && $product->getId() !== null
            && $customer
            && $customer->getId() !== null
            && $this->getCustomerRequestByProductId($product->getId(), $customer->getId())
        ) {
            return true;
        }
        return false;
    }

    public function isOtherCouponsDenied()
    {
        if (Mage::helper('pmatch/config')->isDenyUseOfOtherCoupons()) {
            $appliedRequests = $this->getAppliedRequests();
            if (count($appliedRequests) > 0) {
                return true;
            }
        }
        return false;
    }

    public function isOtherCouponsAllowed()
    {
        return !$this->isOtherCouponsDenied();
    }

    public function setNoCacheCookie()
    {
        if (@class_exists('Enterprise_PageCache_Model_Processor')) {
            Mage::app()->getCookie()->set(Enterprise_PageCache_Model_Processor::NO_CACHE_COOKIE, 1);
        }
    }

    public function deleteNoCacheCookie()
    {
        if (@class_exists('Enterprise_PageCache_Model_Processor')) {
            Mage::app()->getCookie()->delete(Enterprise_PageCache_Model_Processor::NO_CACHE_COOKIE);
        }
    }

}
