<?php
$installer = $this;
$installer->startSetup();
$installer->run("
ALTER TABLE {$this->getTable('product_price_logs')} ADD COLUMN `admin_user` VARCHAR(200) DEFAULT NULL;
"			
);
$installer->endSetup();