<?php
$installer = $this;
$installer->startSetup();
$installer->run("
ALTER TABLE {$this->getTable('product_price_logs')} ADD COLUMN  `old_special_price` decimal(12,4) DEFAULT NULL COMMENT 'Old special price';
ALTER TABLE {$this->getTable('product_price_logs')} ADD COLUMN  `new_special_price` decimal(12,4) DEFAULT NULL COMMENT 'New special price';


ALTER TABLE {$this->getTable('product_price_logs')} ADD COLUMN  `old_group_price` decimal(12,4) DEFAULT NULL COMMENT 'Old group price';
ALTER TABLE {$this->getTable('product_price_logs')} ADD COLUMN  `new_group_price` decimal(12,4) DEFAULT NULL COMMENT 'New group price';

ALTER TABLE {$this->getTable('product_price_logs')} ADD COLUMN  `old_tier_price` decimal(12,4) DEFAULT NULL COMMENT 'Old tier price';
ALTER TABLE {$this->getTable('product_price_logs')} ADD COLUMN  `new_tier_price` decimal(12,4) DEFAULT NULL COMMENT 'New tier price';
ALTER TABLE {$this->getTable('product_price_logs')} ADD COLUMN  `store_id` decimal(12,4) DEFAULT NULL COMMENT 'Store id';
"			
);
$installer->endSetup();