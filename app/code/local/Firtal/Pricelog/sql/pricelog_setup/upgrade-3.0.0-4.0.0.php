<?php
$installer = $this;
$installer->startSetup();
$installer->run("
ALTER TABLE {$this->getTable('product_price_logs')} ADD COLUMN  `sku` varchar(255) DEFAULT NULL COMMENT 'Product SKU';
ALTER TABLE {$this->getTable('product_price_logs')} ADD COLUMN  `type` varchar(255) DEFAULT NULL COMMENT 'Price change type';
ALTER TABLE {$this->getTable('product_price_logs')} ADD COLUMN  `old_type` varchar(255) DEFAULT NULL COMMENT 'Price change type';

"			
);
$installer->endSetup();