<?php

$installer = $this;
$installer->startSetup();
$installer->run("
	DROP TABLE IF EXISTS product_price_logs;
    CREATE TABLE `{$installer->getTable('product_price_logs')}` (
      `id` int(11) NOT NULL auto_increment,
      `product_id` int(11) NOT NULL,
	  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Updated At',
	  `old_price` decimal(12,4) DEFAULT NULL COMMENT 'old price',
	  `new_price` decimal(12,4) DEFAULT NULL COMMENT 'new price',
      PRIMARY KEY  (`id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    
");


$installer->endSetup();

?>