<?php
class Firtal_Pricelog_Block_Adminhtml_Pricelog extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_pricelog';
    $this->_blockGroup = 'pricelog';
    $this->_headerText = Mage::helper('pricelog')->__('Product Price History');
    
    parent::__construct();
	$this->_removeButton('add');
	 $url  = $this->getUrl('pricelog/adminhtml_pricelog/clearhistory/');
	$this->_addButton('clear_history', array(
					'label'     => Mage::helper('pricelog')->__('Clear full history'),
					"onclick"   => "deleteConfirm('Are you sure to clear full history?','" . $url . "')",
					'class'     => 'delete'
				), 0, 100, 'header', 'header');
  }
}