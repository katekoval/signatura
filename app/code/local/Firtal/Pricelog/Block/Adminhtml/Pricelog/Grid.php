<?php

class Firtal_Pricelog_Block_Adminhtml_Pricelog_Grid extends Mage_Adminhtml_Block_Widget_Grid {

    public function __construct() {
        parent::__construct();
        $this->setId('pricelogGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
		$this->setUseAjax(true);
    }
	protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }
    protected function _prepareCollection() {
        $product_id = 0;
        if($this->getRequest()->getParam('id') > 0){
             $product_id = $this->getRequest()->getParam('id');
			   $collection = Mage::getModel('pricelog/pricelog')->getCollection()
             ->addFieldToFilter('product_id',$product_id);
        }else{
			 $collection = Mage::getModel('pricelog/pricelog')->getCollection();
		}
         $collection->setOrder('id','desc');
       $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
      
		if(!$this->getRequest()->getParam('id')){
			$this->addColumn('product_id', array(
				'header' => Mage::helper('pricelog')->__('Product ID'),
				'align' => 'center',
				'width' => '50px',
				'filter_index' => 'main_table.product_id',
				'index' => 'product_id',
				'type'=>'range'
			));
			$this->addColumn('sku', array(
				'header' => Mage::helper('pricelog')->__('SKU'),
				'align' => 'center',
				'width' => '200px',
				'filter_index' => 'main_table.sku',
				'index' => 'sku',
			));
		}
        $this->addColumn('updated_at', array(
            'header' => Mage::helper('pricelog')->__('Modified Date'),
            'align' => 'left',
            'width' => '150px',
            'type' => 'datetime',
            'index' => 'updated_at',
        ));
		  $store = $this->_getStore();
        $this->addColumn('old_price', array(
            'header' => Mage::helper('pricelog')->__('Previous Price'),
            'align' => 'left',
            'width' => '100px',
            'filter_index' => 'main_table.old_price',
            'index' => 'old_price',
			'type'  => 'price',
            'currency_code' => $store->getBaseCurrency()->getCode(),
        ));

        $this->addColumn('new_price', array(
            'header' => Mage::helper('pricelog')->__('New Price'),
            'align' => 'left',
            'width' => '100px',
            'filter_index' => 'main_table.new_price',
            'index' => 'new_price',
			'type'  => 'price',
            'currency_code' => $store->getBaseCurrency()->getCode(),
        ));
		$this->addColumn('old_special_price', array(
            'header' => Mage::helper('pricelog')->__('Old Special Price'),
            'align' => 'left',
            'width' => '100px',
            'filter_index' => 'main_table.old_special_price',
            'index' => 'old_special_price',
			'type'  => 'price',
            'currency_code' => $store->getBaseCurrency()->getCode(),
        ));

        $this->addColumn('new_special_price', array(
            'header' => Mage::helper('pricelog')->__('New Special Price'),
            'align' => 'left',
            'width' => '100px',
            'filter_index' => 'main_table.new_special_price',
            'index' => 'new_special_price',
			'type'  => 'price',
            'currency_code' => $store->getBaseCurrency()->getCode(),
        ));
		
		
		
		$this->addColumn('old_tier_price', array(
            'header' => Mage::helper('pricelog')->__('Old Tier Price'),
            'align' => 'left',
            'width' => '100px',
            'filter_index' => 'main_table.old_tier_price',
            'index' => 'old_tier_price',
			'type'  => 'price',
            'currency_code' => $store->getBaseCurrency()->getCode(),
        ));

        $this->addColumn('new_tier_price', array(
            'header' => Mage::helper('pricelog')->__('New Tier Price'),
            'align' => 'left',
            'width' => '100px',
            'filter_index' => 'main_table.new_tier_price',
            'index' => 'new_tier_price',
			'type'  => 'price',
            'currency_code' => $store->getBaseCurrency()->getCode(),
        ));
		$this->addColumn('admin_user', array(
            'header' => Mage::helper('pricelog')->__('Updated By'),
            'align' => 'left',
            'width' => '200px',
            'filter_index' => 'main_table.admin_user',
            'index' => 'admin_user',
        ));
        $this->addColumn('store_id', array(
            'header' => Mage::helper('pricelog')->__('Store id'),
            'align' => 'left',
            'width' => '100px',
            'filter_index' => 'main_table.store_id',
            'index' => 'store_id',
        ));
        $this->addColumn('type', array(
            'header' => Mage::helper('pricelog')->__('Reason/Type'),
            'align' => 'left',
            'width' => '200px',
            'index' => 'type',
        ));
		$this->addExportType('*/*/exportCsv', Mage::helper('pricelog')->__('CSV'));
        return parent::_prepareColumns();
    }
	 protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('id');
 
        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('pricelog')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('pricelog')->__('Are you sure?')
        ));
        return $this;
    } 
    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=> true));
    }
    
	 public function getRowUrl($row)
    {
        return $this->getUrl('adminhtml/catalog_product/edit', array(
            'store'=>$this->_getStore()->getId(),
            'id'=>$row->getProductId())
        );
    }
   
}