<?php
class Firtal_Pricelog_Block_Adminhtml_Catalog_Product_Edit_Tabs extends Mage_Adminhtml_Block_Catalog_Product_Edit_Tabs
{
	/**
	 * @var
	 */
	private $parent;
	/**
	 * @return mixed Mage_Core_Block_Abstract
	 */
	protected function _prepareLayout()
	{
		// get all existing tabs
		$this->parent = parent::_prepareLayout();
		// add new tab
		$product = $this->getProduct();

        if (!($setId = $product->getAttributeSetId())) {
            $setId = $this->getRequest()->getParam('set', null);
        }
		if ($setId) {
			$this->addTab('pricelog', array(
				'label'   => Mage::helper('catalog')->__('Price History'),
				'content' => $this->getLayout()->createBlock('pricelog/adminhtml_catalog_product_edit_tab_pricelog')->toHtml(),
			));

		}
		return $this->parent;
	}
}