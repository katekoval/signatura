<?php class Firtal_Pricelog_Block_Adminhtml_Catalog_Product_Edit_Tab_Pricelog
    extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct() {
        parent::__construct();
        $this->setId('pricelogGrid');
        $this->setDefaultSort('id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
		$this->setUseAjax(true);
    }
	protected function _getStore()
    {
        $storeId = (int) $this->getRequest()->getParam('store', 0);
        return Mage::app()->getStore($storeId);
    }
    protected function _prepareCollection() {
        $product_id = 0;
        if($this->getRequest()->getParam('id') > 0){
             $product_id = $this->getRequest()->getParam('id');
			   $collection = Mage::getModel('pricelog/pricelog')->getCollection()
             ->addFieldToFilter('product_id',$product_id);
        }else{
			 $collection = Mage::getModel('pricelog/pricelog')->getCollection()->addFieldToFilter('product_id',0);
		}
         $collection->setOrder('id','desc');
       $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns() {
        $this->addColumn('pricelog_id', array(
            'header' => Mage::helper('pricelog')->__('ID'),
            'align' => 'center',
            'width' => '20px',
            'filter_index' => 'main_table.id',
            'index' => 'id',
			'type'=>'range'
        ));
		 $this->addColumn('updated_at', array(
            'header' => Mage::helper('pricelog')->__('Modified Date'),
            'align' => 'left',
            'width' => '300px',
            'type' => 'datetime',
            'index' => 'updated_at',
        ));
		  $store = $this->_getStore();
        $this->addColumn('old_price', array(
            'header' => Mage::helper('pricelog')->__('Previous Price'),
            'align' => 'left',
            'width' => '100px',
            'filter_index' => 'main_table.old_price',
            'index' => 'old_price',
			'type'  => 'price',
            'currency_code' => $store->getBaseCurrency()->getCode(),
        ));

        $this->addColumn('new_price', array(
            'header' => Mage::helper('pricelog')->__('New Price'),
            'align' => 'left',
            'width' => '100px',
            'filter_index' => 'main_table.new_price',
            'index' => 'new_price',
			'type'  => 'price',
            'currency_code' => $store->getBaseCurrency()->getCode(),
        ));
		$this->addColumn('old_special_price', array(
            'header' => Mage::helper('pricelog')->__('Old Special Price'),
            'align' => 'left',
            'width' => '100px',
            'filter_index' => 'main_table.old_special_price',
            'index' => 'old_special_price',
			'type'  => 'price',
            'currency_code' => $store->getBaseCurrency()->getCode(),
        ));

        $this->addColumn('new_special_price', array(
            'header' => Mage::helper('pricelog')->__('New Special Price'),
            'align' => 'left',
            'width' => '100px',
            'filter_index' => 'main_table.new_special_price',
            'index' => 'new_special_price',
			'type'  => 'price',
            'currency_code' => $store->getBaseCurrency()->getCode(),
        ));
		
		$this->addColumn('admin_user', array(
            'header' => Mage::helper('pricelog')->__('Updated By'),
            'align' => 'left',
            'width' => '200px',
            'filter_index' => 'main_table.admin_user',
            'index' => 'admin_user',
        ));
        $this->addColumn('store_id', array(
            'header' => Mage::helper('pricelog')->__('Store Id'),
            'align' => 'left',
            'width' => '200px',
            'filter_index' => 'main_table.store_id',
            'index' => 'store_id',
        ));
        $this->addColumn('type', array(
            'header' => Mage::helper('pricelog')->__('Reason/Type'),
            'align' => 'left',
            'width' => '200px',
            'index' => 'type',
        ));
        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('pricelog/adminhtml_pricelog/priceloggrid', array('_current'=> true));
    }
    
}