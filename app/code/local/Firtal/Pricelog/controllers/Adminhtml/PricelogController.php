<?php
class Firtal_Pricelog_Adminhtml_PricelogController extends Mage_Adminhtml_Controller_Action {

   

    /**
     * index action
     */
    public function indexAction() {
       $this->loadLayout();
		$this->_setActiveMenu('catalog/pricelog');
		$this->getLayout()->getBlock('head')->setTitle('Product Price History');
		$this->renderLayout();
    }

    /**
     * grid action
     */
    public function gridAction() {
	   $this->loadLayout(false);
       $this->renderLayout();
    }
    /**
	 * Get product order gride and serializer block
	 */
	public function pricelogAction()
	{
		$this->_initProduct();
		$this->loadLayout();
		$this->getLayout()->getBlock('catalog.product.edit.tab.pricelog');
		$this->renderLayout();
	}

	/**
	 * Product order grid for AJAX request
	 */
	public function priceloggridAction()
	{
		$this->_initProduct();
		$this->loadLayout();
		$this->getLayout()->getBlock('catalog.product.edit.tab.pricelog');
		$this->renderLayout();
	}
	 /**
     * Initialize product from request parameters
     *
     * @return Mage_Catalog_Model_Product
     */
    protected function _initProduct()
    {
        $this->_title($this->__('Catalog'))
             ->_title($this->__('Manage Products'));

        $productId  = (int) $this->getRequest()->getParam('id');
        $product    = Mage::getModel('catalog/product')
            ->setStoreId($this->getRequest()->getParam('store', 0));

        if (!$productId) {
            if ($setId = (int) $this->getRequest()->getParam('set')) {
                $product->setAttributeSetId($setId);
            }

            if ($typeId = $this->getRequest()->getParam('type')) {
                $product->setTypeId($typeId);
            }
        }

        $product->setData('_edit_mode', true);
        if ($productId) {
            try {
                $product->load($productId);
            } catch (Exception $e) {
                $product->setTypeId(Mage_Catalog_Model_Product_Type::DEFAULT_TYPE);
                Mage::logException($e);
            }
        }

        $attributes = $this->getRequest()->getParam('attributes');
        if ($attributes && $product->isConfigurable() &&
            (!$productId || !$product->getTypeInstance()->getUsedProductAttributeIds())) {
            $product->getTypeInstance()->setUsedProductAttributeIds(
                explode(",", base64_decode(urldecode($attributes)))
            );
        }

        // Required attributes of simple product for configurable creation
        if ($this->getRequest()->getParam('popup')
            && $requiredAttributes = $this->getRequest()->getParam('required')) {
            $requiredAttributes = explode(",", $requiredAttributes);
            foreach ($product->getAttributes() as $attribute) {
                if (in_array($attribute->getId(), $requiredAttributes)) {
                    $attribute->setIsRequired(1);
                }
            }
        }

        if ($this->getRequest()->getParam('popup')
            && $this->getRequest()->getParam('product')
            && !is_array($this->getRequest()->getParam('product'))
            && $this->getRequest()->getParam('id', false) === false) {

            $configProduct = Mage::getModel('catalog/product')
                ->setStoreId(0)
                ->load($this->getRequest()->getParam('product'))
                ->setTypeId($this->getRequest()->getParam('type'));

            /* @var $configProduct Mage_Catalog_Model_Product */
            $data = array();
            foreach ($configProduct->getTypeInstance()->getEditableAttributes() as $attribute) {

                /* @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
                if(!$attribute->getIsUnique()
                    && $attribute->getFrontend()->getInputType()!='gallery'
                    && $attribute->getAttributeCode() != 'required_options'
                    && $attribute->getAttributeCode() != 'has_options'
                    && $attribute->getAttributeCode() != $configProduct->getIdFieldName()) {
                    $data[$attribute->getAttributeCode()] = $configProduct->getData($attribute->getAttributeCode());
                }
            }

            $product->addData($data)
                ->setWebsiteIds($configProduct->getWebsiteIds());
        }

        Mage::register('product', $product);
        Mage::register('current_product', $product);
        Mage::getSingleton('cms/wysiwyg_config')->setStoreId($this->getRequest()->getParam('store'));
        return $product;
    }
	/**
	* Export order grid to CSV format
	*/
	public function exportCsvAction()
	{
		    $fileName   = 'product_pricelog.csv';
			$content    = $this->getLayout()->createBlock('pricelog/adminhtml_pricelog_grid')
			->getCsvFile();
			$this->_prepareDownloadResponse($fileName, $content);
	}
	
	/**
		Delete Alteration Sizes
		**/
	public function massDeleteAction() {
		try {
			$ids = $this->getRequest()->getParam('id');
			
			foreach ($ids as $id){
				$model = Mage::getModel('pricelog/pricelog')->load($id);
				if($model){
					$model->delete();
				}
			}
			Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Product price history successfully deleted'));
			$this->_redirect('*/*/');

		} catch (Exception $e) {
			Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			$this->_redirect('*/*/');
		}
			
	}
	protected function _getWriteAdapter(){
		$resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');
		return $writeConnection;
	}
	public function clearhistoryAction(){
		try {
			$this->_getWriteAdapter()->query('TRUNCATE TABLE product_price_logs');
			Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('adminhtml')->__('Price history successfully truncated'));
			$this->_redirect('*/*/');
		} catch (Exception $e) {
			Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			$this->_redirect('*/*/');
		}
	}
	 protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('catalog/pricelog');
    }
}