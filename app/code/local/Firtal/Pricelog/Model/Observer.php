<?php
class Firtal_Pricelog_Model_Observer
{

    public function catalog_product_save_before($observer)
    {
        $product = $observer->getProduct(); 
		$old_product =  Mage::getModel('catalog/product')->load($product->getId());
		
        $product_price = $product->getOrigData("price");
		
		if($product_price==""){
			$product_price = $old_product->getPrice();
		}
		$product_new_price = $product->getData("price");
		
		$product_special_price = $product->getOrigData("special_price");
		if($product_special_price==""){
			$product_special_price = $old_product->getSpecialPrice();
		}
        $product_new_special_price = $product->getData("special_price");
		
		$product_group_price = $product->getOrigData("group_price");
		if(count($product_group_price)==0){
			$product_group_price = $old_product->getData("group_price");
		}
        $product_new_group_price = $product->getData("group_price");
		
		$product_tier_price = $product->getOrigData("tier_price");
		if(count($product_tier_price)==""){
			$product_tier_price = $old_product->getData("tier_price");
		}
        $product_new_tier_price = $product->getData("tier_price");
		
		$product_id = $product->getId();
		
		if($product_id) {
			$data = array('product_id'=>$product_id,'sku'=>$product->getSku(), 'store_id'=>$product->getStoreId(), 'old_price'=>$product_price,'admin_user'=>'',
			'old_special_price'=>$product_special_price);
			if(count($product_group_price) > 0){
				$data['old_group_price'] = $product_group_price[0]['price'];
			}
			if(count($product_tier_price) > 0){
				$data['old_tier_price'] = $product_tier_price[0]['price'];
			}

			Mage::getSingleton('core/session', array('name'=>'adminhtml'));
			if(Mage::getSingleton('admin/session')->isLoggedIn()){
				$user = Mage::getSingleton('admin/session'); 
				$name = $user->getUser()->getFirstname().' '.$user->getUser()->getLastname();
				$data['admin_user'] = $name;
			}
			if(Mage::getSingleton('customer/session')->isLoggedIn()){
				$user = Mage::getSingleton('customer/session')->getCustomer(); 
				$name = $user->getFirstname().' '.$user->getLastname();
				$data['admin_user'] = $name;
			}
			$data['store_id'] = $product->getStoreId();
			try {
				$group_old_price = $product_group_price[0]['price']!="" ? $product_group_price[0]['price'] : 0;
				$group_new_price = $product_new_group_price[0]['price']!="" ? $product_new_group_price[0]['price'] : 0;
				$tier_old_price = $product_tier_price[0]['price']!="" ? $product_tier_price[0]['price'] : 0;
				$tier_new_price = $product_new_tier_price[0]['price']!="" ? $product_new_tier_price[0]['price'] : 0;
                if($product_price  != $product_new_price && $product_new_price || $product_special_price != $product_new_special_price && $product_new_special_price || $product_special_price != $product_new_special_price
				|| (count($product_group_price) > 0 && $group_old_price!=$group_new_price) ||
				(count($product_tier_price) > 0 && $tier_old_price!=$tier_new_price)){
					
					$model = Mage::getModel('pricelog/pricelog')->addData($data);
                    $insertId = $model->save()->getId();
                    Mage::getSingleton('core/session')->setProductId($product_id);
                    Mage::getSingleton('core/session')->setProductLogId($insertId);
                }             
            } catch (Exception $e){
                error_log($e->getMessage());
            }
        }
    }
    
    public function catalog_product_save_after($observer)
    {
        $productId = Mage::getSingleton('core/session')->getProductId();
        $ProductLogId =  Mage::getSingleton('core/session')->getProductLogId();

        $product = $observer->getProduct();
        $product_price = $product->getData("price");
		$product_special_price = $product->getData("special_price");
        $product_group_price = $product->getData("group_price");
		$product_tier_price = $product->getData("tier_price");
		
        if((int)$productId > 0){
			Mage::getSingleton('core/session')->unsProductId();
            Mage::getSingleton('core/session')->unsProductLogId();

            $data = array('updated_at'=>date('Y-m-d H:i:s'),'new_price'=>$product_price,'admin_user'=>'',
			'new_special_price'=>$product_special_price);
			if(count($product_group_price) > 0){
				$data['new_group_price'] = $product_group_price[0]['price'];
			}
			if(count($product_tier_price) > 0){
				$data['new_tier_price'] = $product_tier_price[0]['price'];
			}
			if ($product->getUrgentPriceSet() == 1) {
				$data['type'] = "Expiry";
			} else {
				$data['type'] = $product->getSpecialPriceReason();
			}
			Mage::getSingleton('core/session', array('name'=>'adminhtml'));
			if(Mage::getSingleton('admin/session')->isLoggedIn()){
				$user = Mage::getSingleton('admin/session'); 
				$name = $user->getUser()->getFirstname().' '.$user->getUser()->getLastname();
				$data['admin_user'] = $name;
			}
			if(Mage::getSingleton('customer/session')->isLoggedIn()){
				$user = Mage::getSingleton('customer/session')->getCustomer(); 
				$name = $user->getFirstname().' '.$user->getLastname();
				$data['admin_user'] = $name;
			}
            $model = Mage::getModel('pricelog/pricelog')->load($ProductLogId);
			// if($model->getOldSpecialPrice() < 1 ){
			// 	$data['new_special_price'] = NULL;
			// }
			$data['store_id'] = $product->getStoreId();

			$model = $model->addData($data);
            try {
                $model->setId($ProductLogId)->save();
			} catch (Exception $e){
                error_log($e->getMessage());
            }
        }
        
    }

}
