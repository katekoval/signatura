<?php


class Firtal_Shippingpicker_Model_DropPoint {
    protected $dropPointData;

    /**
     * @param $name
     * @param $address
     * @param $city
     * @param $zip
     * @param array $hours
     * @param array $position
     */
    public function __construct($name, $address, $city, $zip, array $hours, array $position){
        $this->dropPointData = [
            'name'      =>$name,
            'address'   =>$address,
            'city'      =>$city,
            'zip'       =>$zip,
            'hours'     =>$hours,
            'position'  =>$position
        ];

        return $this;
    }

    /**
     * @param $var
     * @param $val
     */
    public function setCustomVar($var, $val){
        $this->dropPointData[$var] = $val;
    }


    /**
     * @param $method
     * @param $arguement
     * @return $this
     *
     * DropPoint data only accessible through get__ & set__ calls
     */
    function __call($method, $arguement){
        if (substr(strtoupper($method),0,3)=='SET'){
            $value = substr($method, 3, strlen($method));
            if (in_array($value, $this::$dropPointVars)){
                $this->dropPointData[$value] = $arguement;
            }
        }

        if (substr(strtoupper($method),0,3)=='GET'){
            $value = strtolower(substr($method, 3, strlen($method)));
            return $this->dropPointData[$value];
        }

        return $this;
    }



    /**
     * @return bool
     * Checks if all vars are set.
     */
    public function isComplete(){
        foreach($this->dropPointData as $key=>$value){
            if (empty($value)) {
                return false;
            }
        }
        return true;
    }


    public function toArray(){
        return $this->dropPointData;
    }
}

