<?php

class Firtal_Shippingpicker_Model_Providers_Denmark_PostDK extends Firtal_Shippingpicker_Model_Provider
    implements Firtal_Shippingpicker_Model_Interface_DropPointProvider{

    /**
     * @param $zip
     * @return bool
     */
    public function loadByZip($zip){
        $this->zip = $zip;

        if (!$this->loadShippingRateToZip($zip)){
            $this->status = $this::PROVIDER_NO_RATES;
            return false;
        }

        $url = "https://pacsoft.services.tric.dk/api.php?postalCode=".$zip."&countryCode=DK";

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);

        $result = utf8_encode(curl_exec($curl));
        $errornum = curl_errno($curl);
        curl_close($curl);

        if (!$result || $errornum > 0){
            unset($this->droppoints);
            $this->status = $this::PROVIDER_TIMED_OUT;
            return false;
        }

        if ($this->formatDroppoints($result)) {
            $this->status = $this::PROVIDER_LOADED_SUCCESFULLY;
            return true;
        }

        $this->status = $this::PROVIDER_SOMETHING_WENT_WRONG;
        return false;
    }


    public function formatDroppoints($results){
        $results = (array)json_decode($results, true);
        $droppoints = $results['servicePointInformationResponse']['servicePoints'];

        if (sizeof($droppoints)<1){
            $this->status = $this::PROVIDER_API_NO_DROPPOINTS;
            return false;
        }

        foreach($droppoints as $dropPoint){
            $dropPoint = (array)$dropPoint;

            $newDropPoint = new Firtal_Shippingpicker_Model_DropPoint(
                $dropPoint['name'],
                $dropPoint['deliveryAddress']['streetName'] . ' ' . $dropPoint['deliveryAddress']['streetNumber'],
                ucwords(strtolower($dropPoint['deliveryAddress']['city'])),
                $dropPoint['deliveryAddress']['postalCode'],
                $this->formatOpeningHours($dropPoint),
                $this->coordinates($dropPoint['coordinate']['northing'], $dropPoint['coordinate']['easting'])
            );
            $newDropPoint->setCustomVar('serviceId', $dropPoint['servicePointId']);

            if (!$newDropPoint->isComplete()) continue;
            if ($this->isDuplicateDroppoint($newDropPoint)) continue;

            $this->addDropPoint($newDropPoint);
        }

        if ($this->getDropPointAmount()<1){
            $this->status = $this::PROVIDER_API_MAPPING_ERROR;
            return false;
        }
        return true;
    }



    protected $days = [
        "MO" => "Mandag",
        "TU" => "Tirsdag",
        "WE" => "Onsdag",
        "TH" => "Torsdag",
        "FR" => "Fredag",
        "SA" => "Lørdag",
        "SU" => "Søndag"
    ];
    public function formatOpeningHours($droppoint){
        $days = $droppoint['openingHours'];
        $openingHours = [];
        foreach($days as $day){
            $openingHours[] = $this->days[$day['day']] . ": "
                . substr($day['from1'],0,2) . '.' . substr($day['from1'],2,4)
                . '-'
                . substr($day['to1'],0,2) . '.' . substr($day['to1'],2,4);
        }

        return $openingHours;
    }

}
