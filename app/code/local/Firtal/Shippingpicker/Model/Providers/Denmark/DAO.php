<?php


class Firtal_Shippingpicker_Model_Providers_Denmark_DAO extends Firtal_Shippingpicker_Model_Provider
    implements Firtal_Shippingpicker_Model_Interface_DropPointProvider{

    public function initVariables()
    {
        $this->addVariable('dao_api', 'text', false, "Sepereate with colon. fx: xxxx:xxxxxxxx");
    }

    protected $days = [
        "man" => "Mandag",
        "tir" => "Tirsdag",
        "ons" => "Onsdag",
        "tor" => "Torsdag",
        "fre" => "Fredag",
        "lor" => "Lørdag",
        "son" => "Søndag"
    ];
    public function formatOpeningHours($droppoint){
        $openingHours = [];
        $weekdays = $droppoint['aabningstider'];

        foreach($weekdays as $day=>$hours) {
            $openingHours[] = $this->days[$day] . ': ' . $hours;
        }
        return $openingHours;
    }

    public function formatDroppoints($results){
        //$results = (array)json_decode($results, true);
        $formatted = [];
        $droppoints = $results['resultat']['pakkeshops'];
        //Mage::Mage::log($droppoints, null, 'firtal_shippingpicker.log', true);


        if (sizeof($droppoints)<1){
            $this->status = $this::PROVIDER_API_NO_DROPPOINTS;
            return false;
        }

        foreach($droppoints as $dropPoint){
            $dropPoint = (array)$dropPoint;

            $newDropPoint = new Firtal_Shippingpicker_Model_DropPoint(
                $dropPoint['navn'],
                $dropPoint['adresse'],
                $dropPoint['bynavn'],
                $dropPoint['postnr'],
                $this->formatOpeningHours($dropPoint),
                $this->coordinates($dropPoint['latitude'], $dropPoint['longitude'])
            );

            $newDropPoint->setCustomVar('serviceId', $dropPoint['shopId']);

            if (!$newDropPoint->isComplete()) continue;
            if ($this->isDuplicateDroppoint($newDropPoint)) continue;

            $this->addDropPoint($newDropPoint);


        }

        if ($this->getDropPointAmount()<1){
            $this->status = $this::PROVIDER_API_MAPPING_ERROR;
            return false;
        }
        return true;
    }


    public function loadByZip($zip){
        $this->zip = $zip;
        if (!$this->loadShippingRateToZip($zip)){
            $this->status = $this::PROVIDER_NO_RATES;
            return false;
        }

        $dao_api = $this->getConfigVar('dao_api');
        if (!$dao_api) return false;
        $dao_api = explode(':', $dao_api);
        $dao_api = ['user'=>$dao_api[0], 'pass'=>$dao_api[1]];
        $url = "https://api.dao.as/DAOPakkeshop/FindPakkeshop.php";

        $params = array("kundeid" => $dao_api['user'],
            "kode" => $dao_api['pass'],
            "postnr" => $zip,
            "format" => "json",
            "antal" => 20,
        );
        $url .= "?".http_build_query($params);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $errornum = curl_errno($ch);
        $result = json_decode(curl_exec($ch), true);

        if (!$result || $errornum > 0){
            unset($this->droppoints);
            $this->status = $this::PROVIDER_TIMED_OUT;
            return false;
        }


        if ($this->formatDroppoints($result)) {
            $this->status = $this::PROVIDER_LOADED_SUCCESFULLY;
            return true;
        }

        $this->status = $this::PROVIDER_SOMETHING_WENT_WRONG;
        return false;
    }
}