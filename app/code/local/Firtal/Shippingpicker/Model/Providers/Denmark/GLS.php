<?php


class Firtal_Shippingpicker_Model_Providers_Denmark_GLS extends Firtal_Shippingpicker_Model_Provider
    implements Firtal_Shippingpicker_Model_Interface_DropPointProvider{

    protected $days = [
        "Monday" => "Mandag",
        "Tuesday" => "Tirsdag",
        "Wednesday" => "Onsdag",
        "Thursday" => "Torsdag",
        "Friday" => "Fredag",
        "Saturday" => "Lørdag",
        "Sunday" => "Søndag"
    ];
    public function formatOpeningHours($droppoint){
        $openingHours = [];
        $weekdays = $droppoint['OpeningHours']['Weekday'];

        foreach($weekdays as $day){
            $openingHours[] = $this->days[$day['day']]
                . ': ' . $day['openAt']['From']
                . ' - ' . $day['openAt']['To'];
        }

        return $openingHours;
    }

    public function formatDroppoints($results){
        $droppoints = $results['parcelshops']['PakkeshopData'];

        if(!is_array(reset($droppoints))){
            $droppoints = array($droppoints);
        }

        if (sizeof($droppoints)<1){
            $this->status = $this::PROVIDER_API_NO_DROPPOINTS;
            return false;
        }

        foreach($droppoints as $dropPoint){
            $dropPoint = (array)$dropPoint;

            $newDropPoint = new Firtal_Shippingpicker_Model_DropPoint(
                $dropPoint['CompanyName'],
                $dropPoint['Streetname'],
                $dropPoint['CityName'],
                $dropPoint['ZipCode'],
                $this->formatOpeningHours($dropPoint),
                $this->coordinates($dropPoint['Latitude'], $dropPoint['Longitude'])
            );

            $newDropPoint->setCustomVar('serviceId', $dropPoint['Number']);

            if (!$newDropPoint->isComplete()) continue;
            if ($this->isDuplicateDroppoint($newDropPoint)) continue;

            $this->addDropPoint($newDropPoint);
        }

        if ($this->getDropPointAmount()<1){
            $this->status = $this::PROVIDER_API_MAPPING_ERROR;
            return false;
        }
        return true;
    }


    public function loadByZip($zip){
        $this->zip = $zip;
        if (!$this->loadShippingRateToZip($zip)){
            $this->status = $this::PROVIDER_NO_RATES;
            return false;
        }


        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL,"http://www.gls.dk/webservices_v4/wsShopFinder.asmx/GetParcelShopDropPoint");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POSTFIELDS,
            "zipcode=" . $zip . "&countryIso3166A2=DK&street=&amount=30");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $errornum = curl_errno($ch);
        $result = json_decode(json_encode((array)simplexml_load_string(curl_exec($ch))),1);

        if (!$result || $errornum > 0){
            unset($this->droppoints);
            $this->status = $this::PROVIDER_TIMED_OUT;
            return false;
        }


        if ($this->formatDroppoints($result)) {
            $this->status = $this::PROVIDER_LOADED_SUCCESFULLY;
            return true;
        }

        $this->status = $this::PROVIDER_SOMETHING_WENT_WRONG;
        return false;
    }
}