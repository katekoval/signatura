<?php

class Firtal_Shippingpicker_Model_Providers_Norway_Bring
    extends Firtal_Shippingpicker_Model_Provider
    implements Firtal_Shippingpicker_Model_Interface_DropPointProvider {


    public function loadByZip($zip){


        // Magento has a tendency to trim zipcodes, which is a problem as Norway has some 3 digit zips and Bring's API require 4 digit zipcode input.
        if (strlen((string)$zip)<4){
            $zip = '0' . (string)$zip;
        }

        $this->zip = $zip;


        if(!$this->loadShippingRateToZip($zip)){
            $this->status = $this::PROVIDER_NO_RATES;
            return false;
        }



        $url = "https://api.bring.com/pickuppoint/api/pickuppoint/NO/postalCode/".$zip.".json";

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);
        curl_setopt($curl, CURLOPT_ENCODING ,"");

        $result = curl_exec($curl);
        $errornum = curl_errno($curl);
        curl_close($curl);


        if (!$result || $errornum > 0){
            unset($this->droppoints);
            $this->status = $this::PROVIDER_TIMED_OUT;
            return false;
        }

        if ($this->formatDroppoints($result)) {
            $this->status = $this::PROVIDER_LOADED_SUCCESFULLY;
            return true;
        }

        $this->status = $this::PROVIDER_SOMETHING_WENT_WRONG;
        return false;
    }


    public function formatDroppoints($results){
        $results = json_decode($results, true);

        if (sizeof($results['pickupPoint'])<1){
            $this->status = $this::PROVIDER_API_NO_DROPPOINTS;
            return false;
        }

        foreach($results['pickupPoint'] as $dropPoint){
            $dropPoint = (array)$dropPoint;

            $newDropPoint = new Firtal_Shippingpicker_Model_DropPoint(
                $dropPoint['name'],
                $dropPoint['address'],
                $dropPoint['city'],
                $dropPoint['postalCode'],
                $this->formatOpeningHours($dropPoint['openingHoursNorwegian']),
                $dropPoint['position'] = $this->coordinates($dropPoint['latitude'], $dropPoint['longitude'])
            );

            $newDropPoint->setCustomVar('serviceId', $dropPoint['id']);

            if (!$newDropPoint->isComplete()) continue;
            if ($this->isDuplicateDroppoint($newDropPoint)) continue;

            $this->addDropPoint($newDropPoint);
        }

        if ($this->getDropPointAmount()<1){
            $this->status = $this::PROVIDER_API_MAPPING_ERROR;
            return false;
        }

        return true;
    }



    public function formatOpeningHours($hours){
        return explode(",", $hours);

    }

}
