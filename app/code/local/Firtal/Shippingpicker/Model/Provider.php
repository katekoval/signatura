<?php



/**
 * Class Provider
 * $params ['title', 'icon']
 *
 * Helpful construct for creating a new droppoint provider.
 */
class Firtal_Shippingpicker_Model_Provider {

    const PROVIDER_LOADED_SUCCESFULLY = 0;
    const PROVIDER_TIMED_OUT = 1;
    const PROVIDER_NO_RATES = 2;
    const PROVIDER_API_MAPPING_ERROR = 3;
    const PROVIDER_API_NO_DROPPOINTS = 4;
    const PROVIDER_SOMETHING_WENT_WRONG = 5;
    const PROVIDER_NOT_ACTIVATED = 6;

    const CACHE_LOAD_TAG = 'shippingpicker_zipdata';
    const CACHE_LOAD_PREFIX = 'shippingpicker_zipdata_';
    const CACHE_LIFETIME = 20;


    protected $zip;
    protected $provider_title;
    protected $country;
    protected $rate;
    protected $droppoints = array();
    protected $status;
    protected $cached = false;
    protected $variables;


    public function __construct($provider_title, $icon_url, $country){
        $this->provider_title = $provider_title;
        $this->icon_url = $icon_url;
        $this->country = $country;

        $this->addVariable('active');
        $this->addVariable('meter_penalty', 'text', true, 'The visual ranking of droppoints is based on distance. Penalty will rank providers lower. This penalty is in meters and useful for penalizing providers in city areas. ');
        $this->addVariable('percentage_penalty', 'text', true, 'This penalty is a percentage of the distance of the farthest droppoint. This is useful for penalizing providers in rural areas.');
        $this->initVariables();
    }

    public function initVariables(){}

    public function addVariable($varName, $type="yesno", $sendToFrontend=false, $description=false)
    {
        $this->variables[] = ['name' => $varName, 'type'=>strtolower($type), 'sendToFrontend'=>$sendToFrontend, 'description'=>$description];
    }

    public function getVariables()
    {
        return $this->variables;
    }

    public function getConfigVar($varName)
    {
        return Mage::helper('shippingpicker/config')->getProviderConfig($this->provider_title . '_' . $this->country, $varName);
    }

    public function getVariablesToSendToFrontend()
    {
        $t = [];
        foreach ($this->variables as $variable){
            if ($variable['sendToFrontend']) $t[$variable['name']] = $this->getConfigVar($variable['name']);
        }
        return $t;
    }

    public function isActive()
    {
        return $this->getConfigVar('active');
    }

    public function coordinates($lat, $lng){
        return array(
            'lat'=>(float) $lat,
            'lng'=>(float) $lng
        );
    }

    public function addDropPoint(Firtal_Shippingpicker_Model_DropPoint $dropPoint){
        $this->droppoints[] = $dropPoint;
        return $this;
    }

    public function isDuplicateDroppoint(Firtal_Shippingpicker_Model_DropPoint $droppoint){
        foreach($this->droppoints as $existingDrop){
            if ($existingDrop->getPosition() == $droppoint->getPosition()){
                return true;
            }
        }
        return false;
    }

    /**
     * @param $zip
     * @return bool
     *
     * Premiumrates implementation.
     */
    public function loadShippingRateToZip($zip){

        $carriers = Mage::getSingleton('checkout/cart')
            ->getQuote()
            ->getShippingAddress()
            ->collectShippingRates()
            ->getGroupedAllShippingRates();
        if (!$carriers) return false;


        $available = [];
        foreach($carriers as $carrier){
            foreach ($carrier as $rate){
                if ($rate->getCarrier() =="premiumrate"){
                    $available[] = $rate;
                }
            }
        }
        if (empty($available)) return false;


        try {
            $filtered_collection = array_filter($available, function($rate){
                $identifier = Mage::helper('shippingpicker/config')->getPremiumrateIdentifier();
                if (!$identifier) return false;
                return (stristr($rate->getMethodTitle(), $identifier)!=false && stristr($rate->getMethodTitle(), $this->provider_title)!=false);
            });
        }catch (Exception $e){
            Mage::log($e, null, 'firtal_shippingpicker.log', true);
            return false;
        }

        if (empty($filtered_collection)) return false;


        $cheapest=null;
        foreach($filtered_collection as $rate){
            if (!$cheapest || $rate->getPrice()<$cheapest->getPrice()){
                $cheapest = $rate;
            }
        }
        if (empty($cheapest) || !$cheapest->getData()) return false;


        $this->rate = $cheapest->getData();
        return true;
    }


    public function getDropPointAmount(){
        return sizeof($this->droppoints);
    }


    public function getDropPointsInArray(){
        $t = array();
        foreach($this->droppoints as $droppoint) {
            $t[] = $droppoint->toArray();
        }
        return $t;
    }


    public function getInfo(){
        if (!$this->isActive()) $this->status = $this::PROVIDER_NOT_ACTIVATED;

        $info = [];
        $info['provider_title'] = $this->provider_title;
        $info['icon_url'] = $this->icon_url;
        $info['rate'] = $this->rate;
        $info['status'] = $this->status;
        $info['dropPoints'] = $this->getDropPointsInArray();
        $info['variables'] = $this->getVariablesToSendToFrontend();
        $info['country'] = $this->country;

        return $info;
    }



    public function saveCacheInfo(){

        if ($this->getCacheInfo($this->zip)) return false; // Don't overwrite
        if ($this->status != 0) return false; // Don't save failed data


        return Mage::app()->getCache()->save(serialize($this->getInfo()), $this::CACHE_LOAD_PREFIX . $this->provider_title . (string)$this->zip, array($this::CACHE_LOAD_TAG), $this::CACHE_LIFETIME);
    }


    public function getCacheInfo($zip){
        return unserialize(Mage::app()->getCache()->load($this::CACHE_LOAD_PREFIX . $this->provider_title . (string)$zip));
    }

}






