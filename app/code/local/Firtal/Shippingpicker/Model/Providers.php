<?php



/**
 * Class Firtal_Shippingpicker_Model_Providers
 *
 * Contains a collection of droppoint providers.
 */
class Firtal_Shippingpicker_Model_Providers extends Mage_Core_Model_Abstract{
    protected static $providers = [];
    protected $zip;

    protected $zipMap = [
        "8100" => "8000"
    ];


    public function getProviders()
    {
        return $this::$providers;
    }

    public static function registerProvider(Firtal_Shippingpicker_Model_Interface_DropPointProvider $provider){
        static::$providers[] = $provider;
    }

    public function loadByZip($zip){
        if(array_key_exists($zip, $this->zipMap)) {
            $zip = $this->zipMap[$zip];
        }

        $shouldCache = Mage::helper('shippingpicker/config')->shouldCacheData();

        foreach(static::$providers as $provider){
            if (!$provider->isActive()) continue;
            if ($shouldCache && $provider->getCacheInfo($zip)) continue;

            $loadSuccess = $provider->loadByZip($zip);

            if ($shouldCache && $loadSuccess){
                $provider->saveCacheInfo();
            }

        }

        $this->zip = $zip;
        return $this;
    }


    public function getProvidersInArray(){
        $shouldCache = Mage::helper('shippingpicker/config')->shouldCacheData();

        $providersInfo = [];
        foreach(static::$providers as $provider){

            $cacheInfo = $provider->getCacheInfo($this->zip);
            if ($shouldCache && $cacheInfo){
                $providersInfo[] = $cacheInfo;
                continue;
            }

            $providersInfo[] = $provider->getInfo();
        }

        return $providersInfo;
    }

}



/*
 *  Register Providers below
 */


Firtal_Shippingpicker_Model_Providers::registerProvider(new Firtal_Shippingpicker_Model_Providers_Denmark_PostDK(
    'PostDanmark',
    "https://www.helsebixen.dk/shop/skin/frontend/fliva/helsebixen-v2/images/pin-postnord.png",
    "Denmark"
));


Firtal_Shippingpicker_Model_Providers::registerProvider(new Firtal_Shippingpicker_Model_Providers_Denmark_GLS(
   'GLS',
   'https://www.helsebixen.dk/shop/skin/frontend/fliva/helsebixen-v2/images/pin-gls.png',
   "Denmark"
));


Firtal_Shippingpicker_Model_Providers::registerProvider(new Firtal_Shippingpicker_Model_Providers_Denmark_DAO(
    'DAO',
    'https://www.helsebixen.dk/shop/skin/frontend/fliva/helsebixen-v2/images/pin-dao.png',
    "Denmark"
));


Firtal_Shippingpicker_Model_Providers::registerProvider(new Firtal_Shippingpicker_Model_Providers_Denmark_Bring(
    'Bring',
    'https://www.helsebixen.dk/shop/skin/frontend/fliva/helsebixen-v2/images/pin-bring.png',
    "Denmark"
));

Firtal_Shippingpicker_Model_Providers::registerProvider(new Firtal_Shippingpicker_Model_Providers_Norway_Bring(
    'Bring',
    'https://www.helsebixen.dk/shop/skin/frontend/fliva/helsebixen-v2/images/pin-posten.png',
    "Norway"
));
