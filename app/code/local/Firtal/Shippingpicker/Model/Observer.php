<?php


class Firtal_Shippingpicker_Model_Observer {
    public function placeOrderBefore($observer){
        $order = $observer->getEvent()->getOrder();
        $shippingHelper = Mage::helper('shippingpicker');
        $quoteId = $order->getQuoteId();
        $quote = Mage::getModel('sales/quote')->load($quoteId);

        $drop_quote_address = $shippingHelper->getDroppointAddress($quote);
        $hasDroppointShipping = $shippingHelper->hasDroppointShipping($quote);
        if (!$drop_quote_address || !$hasDroppointShipping) return;

        $order_address = $order->getShippingAddress();
        $drop_order_address = Mage::getModel('sales/order_address')
            ->setParentId($order_address->getParentId())
            ->setAddressType('droppoint')
            ->setStreet($drop_quote_address->getStreet())
            ->setPostcode($drop_quote_address->getPostcode())
            ->setCity($drop_quote_address->getCity())
            ->setCompany($drop_quote_address->getCompany())
            ->setFax($drop_quote_address->getFax())
            ->setDroppoint(true)
            ->save();
    }


    const ONESTEP_ALWAYS_OVERRIDE = false;
    public function oneStepCheckoutPreDispatch(){
        $helper = Mage::helper('shippingpicker');
        $session = Mage::getSingleton('customer/session');
        if (!$session->isLoggedIn()) return;


        // Check if address empty
        $quote_address = Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress();
        if ($helper->verifyShippingAddress($quote_address) && !$this::ONESTEP_ALWAYS_OVERRIDE) return;


        // Insert default address into quote address
        $saved_address = $session->getCustomer()->getDefaultShippingAddress();
        if ($helper->verifyShippingAddress($saved_address)){
            $quote_address->setStreetFull($saved_address->getStreetFull())
                ->setCity($saved_address->getCity())
                ->setPostcode($saved_address->getPostcode())
                ->setCountryId($saved_address->getCountryId())
                ->save();
        }
    }



}







