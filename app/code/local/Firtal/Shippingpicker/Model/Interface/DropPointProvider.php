<?php

interface Firtal_Shippingpicker_Model_Interface_DropPointProvider{

    public function loadByZip($zip);

    public function getInfo();

    public function __construct($provider_title, $icon_url, $country);

    public function loadShippingRateToZip($zip);

    public function formatDroppoints($data);

    public function formatOpeningHours($droppoint);

}
