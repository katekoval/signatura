<?php


class Firtal_Shippingpicker_Block_Shippingoption extends Mage_Core_Block_Template {
    public function _prefix(){
        return Mage::helper('shippingpicker/config')->getPrefix();
    }


    public function getSettings(){
        $config = Mage::helper('shippingpicker/config');

        $settings = array(
            'useClosest' => (bool)$config->recommendClosestDroppoint(),
            'autoSelect' => (bool)$config->getAutoSelectRecommended(),
        );
        return json_encode($settings);
    }
}