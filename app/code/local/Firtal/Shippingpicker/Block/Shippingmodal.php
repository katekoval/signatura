<?php

class Firtal_Shippingpicker_Block_Shippingmodal extends Mage_Core_Block_Template {
    public function _prefix(){
        return Mage::helper('shippingpicker/config')->getPrefix();
    }


    public function getGoogleApiKey(){
        return Mage::helper('shippingpicker/config')->getGoogleApiKey();
    }
}