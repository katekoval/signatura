<?php

class Firtal_Shippingpicker_Helper_Data extends Mage_Core_Helper_Abstract{

    const GEOCODER_CACHE_PREFIX = 'SP_GEOCODER_';
    const GEOCODER_CACHE_LIFETIME = 60 * 60 * 24 * 60;

    public function requestSessionCoordinates(){
        $api_key = Mage::helper('shippingpicker/config')->getGeocoderApiKey();
        $sessionAddress = Mage::getSingleton('checkout/session')
            ->getQuote()
            ->getShippingAddress();

        // Get shipping address coords from Google Geocoder API.
        $search = $sessionAddress->getStreetFull() .
            ", " . $sessionAddress->getPostcode() .
            " " . $sessionAddress->getCity() .
            ", " . $sessionAddress->getCountryId();
        $search = urlencode($search);

        // Cache check
        $cache_id = self::GEOCODER_CACHE_PREFIX . sha1(str_replace('+', '', strtolower($search)));
        $cache = Mage::app()->getCache()->load($cache_id);
        if ($cache){
            $cache = unserialize($cache);
            return (array) $cache;
        }


        $url = "https://maps.googleapis.com/maps/api/geocode/json?address=" . $search . "&key=" . $api_key;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_TIMEOUT, 10);

        $result = (array) json_decode(utf8_encode(curl_exec($curl)), true);
        curl_close($curl);

        $location = $result['results'][0]['geometry']['location'];

        if ($location) Mage::app()->getCache()->save(serialize($location), $cache_id, array(), self::GEOCODER_CACHE_LIFETIME);

        return (array) $location;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return null
     */
    public function getOrderDroppointAddress(Mage_Sales_Model_Order $order)
    {
        $address = Mage::getModel('sales/order_address')->getCollection()
                    ->addFieldToFilter('parent_id', $order->getId())
                    ->addFieldToFilter('address_type', 'droppoint')
                    ->getFirstItem();

        if(!$address->getId())
        {
            return null;
        }

        return $address;
    }


    public function getMDNOrderDroppointAddress(MDN_AdvancedStock_Model_Sales_Order $order){
        $address = Mage::getModel('sales/order_address')->getCollection()
            ->addFieldToFilter('parent_id', $order->getId())
            ->addFieldToFilter('address_type', 'droppoint')
            ->getFirstItem();

        if(!$address->getId())
        {
            return null;
        }

        return $address;
    }

    public function getDroppointAddress(Mage_Sales_Model_Quote $quote){
        $address = Mage::getModel('sales/quote_address')->getCollection()
            ->addFieldToFilter('quote_id', $quote->getId())
            ->addFieldToFilter('address_type', 'droppoint')
            ->getFirstItem();

        if(!$address->getId())
        {
            return null;
        }

        return $address;
    }


    public function hasDroppointShipping(Mage_Sales_Model_Quote $quote){
        $description = $quote->getShippingAddress()->getShippingDescription();
        $identifier = Mage::helper('shippingpicker/config')->getPremiumrateIdentifier();

        if (!$identifier) return false;
        if (stristr($description, $identifier)) return true;
        return false;
    }


    public function shouldPriotizeMethod(){
        $priorityWeight = (int)Mage::helper('shippingpicker/config')->getPriorityWeight();
        if (!$priorityWeight) return false;

        $weight = Mage::helper('checkout/cart')->getQuote()
            ->getShippingAddress()
            ->getWeight();
        return $weight > $priorityWeight;
    }

    public function verifyShippingAddress($address){
        if(!$address) return false;
        
        $verify = ['street', 'city', 'postcode', 'country_id'];
        foreach($verify as $val){
            if (!$address->getData($val) || $address->getData($val) == '-' ) return false;
        }
        return true;
    }


    /**
     * Dynamically create config for all providers
     *
     * No fitting hook has been found for this function as of yet
     */
    public function generateProviderConfigs()
    {
        $config = Mage::getConfig()->loadModulesConfiguration('system_base.xml');
        $sectionNode = $config->getNode('sections/shippingpicker_section/groups');
        if (!$sectionNode){ return;}

        $providers = Mage::getModel('shippingpicker/providers')->getProviders();
        $i = 1;
        foreach ($providers as $provider){
            $i++;
            $info = $provider->getInfo();
            $title = $info['provider_title'];
            $country = $info['country'];
            $variables = $provider->getVariables();

            $rawXml = "<" . strtolower($title . '_' . $country) . " translate=\"label\">
                    <label>" . $title . " (". $country.")</label>
                    <frontend_type>text</frontend_type>
                    <sort_order>" . $i . "</sort_order>
                    <show_in_default>1</show_in_default>
                    <show_in_website>1</show_in_website>
                    <show_in_store>1</show_in_store>
                    <fields>";

            $sort = 50;
            foreach ($variables as $variable){
                $sort = $sort + 1;
                $descriptionXml = $variable['description']?"<comment>" . $variable['description'] . "</comment>":"";
                $prettyName = ucwords(str_replace('_', ' ', $variable['name']));

                if ($variable['type']=='yesno'){
                    $rawXml .= "<". $variable['name'] ." translate='label'>
                            <label> " . $prettyName . ": </label>
                            <frontend_type>select</frontend_type>
                            <sort_order>" . $sort . " </sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                            <source_model>adminhtml/system_config_source_yesno</source_model>
                            " . $descriptionXml . "
                            </". $variable['name'] .">";
                }
                if ($variable['type']=='text'){
                    $rawXml .= "<". $variable['name'].">
                            <label> " . $prettyName . ":</label>
                            <frontend_type>text</frontend_type>
                            <sort_order> " . $sort ." </sort_order>
                            <show_in_default>1</show_in_default>
                            <show_in_website>1</show_in_website>
                            <show_in_store>1</show_in_store>
                            " . $descriptionXml . "
                        </". $variable['name'].">";
                }
            }
            $rawXml .= "</fields>
                </" . strtolower($title . '_' . $country) . ">";

            $xml = new Mage_Core_Model_Config_Element($rawXml);
            $sectionNode->appendChild($xml);
        }

        $toString = $config->getXmlString();
        $system = new Varien_Simplexml_Config;
        $system->loadString($toString);

        $savePath = Mage::getBaseDir() . '/app/code/local/Firtal/Shippingpicker/etc/system.xml';
        $system->getNode()->asNiceXml($savePath, false);
    }


}







