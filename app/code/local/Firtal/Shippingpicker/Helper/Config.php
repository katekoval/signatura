<?php

class Firtal_Shippingpicker_Helper_Config extends Mage_Core_Helper_Abstract
{
    const SHIPPINGPICKER_PREFIX = 'SP_';


    public function isActive(){
        return Mage::getStoreConfig('shippingpicker_section/settings/active');
    }

    public function isEnabledInCheckout(){
        return Mage::getStoreConfig('shippingpicker_section/settings/enable_method_button');
    }

    public function shouldHidePremiumrates(){
        return Mage::getStoreConfig('shippingpicker_section/settings/hide_premiumrates');
    }

    public function shouldCacheData(){
        return Mage::getStoreConfig('shippingpicker_section/settings/cache_data');
    }

    public function recommendClosestDroppoint(){
        return Mage::getStoreConfig('shippingpicker_section/settings/use_closest_droppoint');
    }



    public function getGoogleApiKey(){
        return Mage::getStoreConfig('shippingpicker_section/settings/google_api');
    }

    public function getDaoApiInfo(){
        $info = explode(':', Mage::getStoreConfig('shippingpicker_section/settings/dao_api'));
        if (sizeof($info) != 2) return false;
        return ['user' => $info[0], 'pass' => $info[1]];
    }

    public function getGeocoderApiKey(){
        return Mage::getStoreConfig('shippingpicker_section/settings/google_geocoder_api');
    }

    public function getPremiumrateIdentifier($storeId=null){
        return Mage::getStoreConfig('shippingpicker_section/settings/premiumrate_identifier', $storeId);
    }

    public function getPriorityWeight(){
        return Mage::getStoreConfig('shippingpicker_section/settings/priority_weight');
    }

    public function getAutoSelectRecommended()
    {
        return Mage::getStoreConfig('shippingpicker_section/settings/auto_select_recommended');
    }

    public function getPrefix(){
        return $this::SHIPPINGPICKER_PREFIX;
    }

    public function getProviderConfig($provider_name, $var)
    {
        return Mage::getStoreConfig('shippingpicker_section/' . strtolower($provider_name) . '/' . strtolower($var));
    }



}