<?php

class Firtal_Shippingpicker_Adminhtml_ShippingpickerController extends Mage_Adminhtml_Controller_Action {

    public function refreshconfigAction()
    {
        Mage::log('refreshing config', null, 'firtal_shippingpicker.log');
        Mage::helper('shippingpicker')->generateProviderConfigs();
        Mage::app()->getResponse()->setBody(1);
    }
}