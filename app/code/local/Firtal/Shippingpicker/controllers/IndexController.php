<?php


class Firtal_Shippingpicker_IndexController extends Mage_Core_Controller_Front_Action {


    const SAVE_DROPPOINT_ERROR = 0;
    const GET_DROPPOINTS_MISSING_PARAMS = 1;
    const GET_DROPPOINTS_NO_PROVIDERS = 2;
    const GET_SHIPPING_NO_GEOCODER_API = 3;
    const GET_SHIPPING_ADDRESS_UNCOMPLETE = 4;
    const GET_DROPPOINTS_ALL_PROVIDERS_DEFECT = 5;
    const GET_SELECTED_DROPPOINT_NONE = 6;

    protected $error_messages = array(
        self::SAVE_DROPPOINT_ERROR => "Unable to save shipping method",
        self::GET_DROPPOINTS_MISSING_PARAMS => "Missing parameters",
        self::GET_DROPPOINTS_NO_PROVIDERS => "No providers registered",
        self::GET_SHIPPING_NO_GEOCODER_API => "No Google Geocoder API key set in config",
        self::GET_SHIPPING_ADDRESS_UNCOMPLETE => "Shipping address is not filled out",
        self::GET_DROPPOINTS_ALL_PROVIDERS_DEFECT => "All providers returned errors",
        self::GET_SELECTED_DROPPOINT_NONE => 'No droppoint selected'
    );

    public function getErrorArray($errornum){
        return json_encode(array('ERROR'=>$this->error_messages[$errornum], 'CODE'=>$errornum));
    }

    public function indexAction(){
        $this->getResponse()->setHeader('Content-type', 'text/json; charset=UTF-8');
    }


    public function saveDroppointShippingAction(){
        $params = $this->getRequest()->getParams();
        $quote = Mage::getSingleton('checkout/session')->getQuote();

        try{
          $quote->getShippingAddress()
              ->setStreet($params['street'])
              ->setCity($params['city'])
              ->setPostcode($params['zip'])
              ->setCompany($params['name'])
              ->setDroppoint(1)
              ->save();

            $quote->setShippingMethod($params['code'])
                ->save();

        } catch (Exception $e){
            Mage::log($e->getMessage(), null, 'firtal_shippingpicker.log', true);
            $this->getResponse()->setHeader('HTTP/1.0', '422', true);
            $this->getResponse()->setBody($this->getErrorArray($this::SAVE_DROPPOINT_ERROR));
            return;
        }
    }


    // Saves the droppoint address as a new row in the quote.
    public function saveDroppointAsNewRowAction(){
        $params = $this->getRequest()->getParams();
        $quote = Mage::getSingleton('checkout/session')->getQuote();

        $selectedDroppoint = Mage::helper('shippingpicker')->getDroppointAddress($quote);
        try{
            if (!$selectedDroppoint){
                $selectedDroppoint = Mage::getModel('sales/quote_address')
                    ->setAddressType('droppoint')
                    ->setDroppoint(true)
                    ->setQuoteId($quote->getEntityId());
            }

            $selectedDroppoint->setStreet($params['street'])
                ->setCity($params['city'])
                ->setPostcode($params['zip'])
                ->setCompany($params['name'])
                ->setShippingMethod($params['code'])
                ->setFax($params['serviceId'])
                ->save();

        } catch (Exception $e){
            Mage::log($e->getMessage(), null, 'firtal_shippingpicker.log', true);
            $this->getResponse()->setHeader('HTTP/1.0', '422', true);
            $this->getResponse()->setBody($this->getErrorArray($this::SAVE_DROPPOINT_ERROR));
            return;
        }
    }


    public function getSelectedDroppointInfoAction(){
        $quote = Mage::getSingleton('checkout/session')->getQuote();
        $selectedDroppoint = Mage::helper('shippingpicker')->getDroppointAddress($quote);

        if (!$selectedDroppoint){
            return $this->getResponse()->setBody($this->getErrorArray($this::GET_SELECTED_DROPPOINT_NONE));
        }

        $info = [
            'name' => $selectedDroppoint->getCompany(),
            'street' => $selectedDroppoint->getStreet(),
            'method' => $selectedDroppoint->getShippingMethod()
        ];

        $this->getResponse()->setBody(json_encode($info));
    }


    public function getAllDroppointsAction(){
        $params = $this->getRequest()->getParams();

        if (!$params['postcode']){
            $this->getResponse()->setHeader('HTTP/1.0','422',true);
            $this->getResponse()->setBody($this->getErrorArray($this::GET_DROPPOINTS_MISSING_PARAMS));
            return;
        }

        $zip = (int)$params['postcode'];
        $providerModel = Mage::getSingleton('shippingpicker/providers');
        $providers = $providerModel->loadByZip($zip)
            ->getProvidersInArray();


        if (empty($providers)) {
            $this->getResponse()->setHeader('HTTP/1.0','422',true);
            $this->getResponse()->setBody($this->getErrorArray($this::GET_DROPPOINTS_NO_PROVIDERS));
            return;
        }
        
        $providers = json_encode($providers);
        $this->getResponse()->setBody($providers);

    }

    /**
     * Check if any shippingpicker rate available
     * response: JSON 'hasRates' bool
     *
     * @return Zend_Controller_Response_Abstract
     */
    public function getHasShippingRatesAction(){
        $this->getResponse()->setHeader('Content-type', 'application/json');

        $carriers = Mage::getSingleton('checkout/cart')
            ->getQuote()
            ->getShippingAddress()
            ->collectShippingRates()
            ->getGroupedAllShippingRates();
        if (!$carriers) return $this->getResponse()->setBody(json_encode(array('hasRates'=>false)));;


        $available = [];
        foreach($carriers as $carrier){
            foreach ($carrier as $rate){
                if ($rate->getCarrier() =="premiumrate"){
                    $available[] = $rate;
                }
            }
        }
        if (empty($available)) return $this->getResponse()->setBody(json_encode(array('hasRates'=>false)));;


        try {
            $filtered_collection = array_filter($available, function($rate){
                $identifier = Mage::helper('shippingpicker/config')->getPremiumrateIdentifier();
                if (!$identifier) return false;
                return (stristr($rate->getMethodTitle(), $identifier)!=false);
            });
        }catch (Exception $e){
            Mage::log($e, null, 'firtal_shippingpicker.log', true);
            return $this->getResponse()->setBody(json_encode(array('hasRates'=>false)));
        }
        if (empty($filtered_collection)) return $this->getResponse()->setBody(json_encode(array('hasRates'=>false)));;
        $this->getResponse()->setBody(json_encode(array('hasRates'=>true)));
    }



    public function getShippingAddressAction(){
        $this->getResponse()->setHeader('Content-type', 'text/json; charset=UTF-8');

        $api_key = Mage::helper('shippingpicker/config')->getGeocoderApiKey();
        if (!$api_key){
            $this->getResponse()->setHeader('HTTP/1.0', '422', true);
            $this->getResponse()->setBody($this->getErrorArray($this::GET_SHIPPING_NO_GEOCODER_API));
            return;
        };


        try {
            $checkout = Mage::getSingleton('checkout/session')
                ->getQuote()
                ->getShippingAddress();

            $address = array(
                "street" => $checkout->getStreetFull(),
                "city" => $checkout->getCity(),
                "zip" => (int)$checkout->getPostcode(),
                "country" => $checkout->getCountryId()
            );
        }catch (Exception $e){
            $this->getResponse()->setHeader('HTTP/1.0', '422', true);
            $this->getResponse()->setBody(json_encode(array('ERROR'=>$e)));
            Mage::log($e, null, 'firtal_shippingpicker.log', true);
            return;
        }


        // All shipping info we need must be filled out.
        foreach($address as $value){
            if (!$value){
                $this->getResponse()->setHeader('HTTP/1.0', '422', true);
                $this->getResponse()->setBody($this->getErrorArray($this::GET_SHIPPING_ADDRESS_UNCOMPLETE));
                return;
            }
        }

        // Try to fetch coordinates from session customer's billing address.
        $location = Mage::helper('shippingpicker')->requestSessionCoordinates();
        $address['coordinates'] = $location; // Will always set coordinates to null if request failed.
        $address['droppoint'] = $checkout->getDroppoint();

        $this->getResponse()->setBody(json_encode($address));
    }

}