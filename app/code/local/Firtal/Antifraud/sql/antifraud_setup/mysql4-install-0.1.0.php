<?php

$installer = $this;

$installer->startSetup();

$installer->run("
    ALTER TABLE `sales_flat_order`
    ADD COLUMN `firtal_fraud_checked` INT(1) NOT NULL
");

$installer->endSetup();