<?php

class Firtal_Antifraud_Block_Adminhtml_Order_Message extends Mage_Adminhtml_Block_Messages
{

    protected $configHelper;

    public function _construct() {

        if(!Mage::helper('antifraud/module')->isActive($this->getOrder()->getStore())
            || $this->getOrder()->getStatus() != $this->getConfigHelper()->getEpayNewOrderStatus()
            || (bool) $this->getOrder()->getFirtalFraudChecked()) {
            return;
        }

        $configWarning = Mage::helper('antifraud/config')->getOrderWarningMessage(
            $this->getOrder()->getStore()
        );

        $this->addWarning($configWarning);

    }

    protected function getOrder() {
        return Mage::registry('sales_order');
    }

    /**
     * @return Firtal_Antifraud_Helper_Config
     */
    protected function getConfigHelper() {

        if(!$this->configHelper) {
            $this->configHelper = Mage::helper('antifraud/config');
        }

        return $this->configHelper;

    }

}