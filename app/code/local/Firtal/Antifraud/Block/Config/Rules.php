<?php

class Firtal_Antifraud_Block_Config_Rules extends Mage_Adminhtml_Block_Template implements Varien_Data_Form_Element_Renderer_Interface
{

    const RULE_INDEX               = ':RULE';
    const CONDITION_TEMPLATE_INDEX = ':CONDITION';

    protected $_element;

    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('firtal/antifraud/array_form_field_wrapper.phtml');
    }

    /**
     * @param mixed $element
     */
    public function setElement(Varien_Data_Form_Element_Abstract $element)
    {
        $this->_element = $element;
    }

    public function getRules()
    {
        $rules = $this->getElement()->getValue();

        foreach($rules as &$rule){
            $rule[self::CONDITION_TEMPLATE_INDEX] = ['validator' => '', 'value' => ''];
        }

        $rules[self::RULE_INDEX] = [self::CONDITION_TEMPLATE_INDEX => ['validator' => '', 'value' => '']];

        return $rules;

    }

    public function isRuleTemplate($ruleIndex)
    {
        return $ruleIndex === self::RULE_INDEX;
    }

    public function isConditionTemplate($ruleIndex)
    {
        return $ruleIndex === self::CONDITION_TEMPLATE_INDEX;
    }

    /**
     * @return mixed
     */
    public function getElement()
    {
        return $this->_element;
    }

    protected function getAddRowButtonText()
    {
        return $this->__('Add rule');
    }

    protected function getValidators()
    {
        return Mage::getModel('antifraud/source_validators')->toOptionArray();
    }

    protected function getConfigName()
    {
        return $this->getElement()->getData('name');
    }

    /**
     * @param Varien_Data_Form_Element_Abstract $element
     * @return string
     */
    public function render(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        return $this->toHtml();
    }

    /**
     * @return mixed
     * @throws Exception
     */
    private function getStoreByRequest()
    {
        if($this->getRequest()->has('store')) {
            return Mage::app()->getStore($this->getRequest()->get('store'));
        }

        if($this->getRequest()->has('website')) {
            return $this->getWebsite($this->getRequest()->get('website'));
        }

        //return Mage::app()->getStore() Mage::app()->getStore(Mage::app()->getDefaultStoreView()->getCode());

    }

    private function getWebsite($code)
    {
        /** @var Mage_Core_Model_Website $website */
        foreach (Mage::app()->getWebsites() as $website) {
            if($website->getCode() === $code) {
                return $website;
            }
        }
        return Mage::getModel('core/website');
    }

}