<?php

class Firtal_Antifraud_Block_Config_Validators extends Mage_Adminhtml_Block_Html_Select
{

    /**
     * @return string|void
     * @throws Varien_Exception
     */
    protected function _toHtml()
    {
        $this->setOptions((new Firtal_Antifraud_Model_Source_Validators)->toOptionArray());
        return parent::_toHtml();
    }

}