<?php

class Firtal_Antifraud_Model_FraudValidatorException extends Exception
{
    protected $orderHelper;
    protected $configHelper;

    function __construct(Firtal_Antifraud_Model_FraudValidatorInterface $validator, $order, $description = null) {

        $humanValidatorName = Mage::helper('antifraud/validator')->resolveValidatorName($validator);

        $message = "{$order->getIncrementId()}: {$humanValidatorName} validator failed.";

        if(!$description){
            $description = $this->getConfighelper()->getOrderStatusComment($order->getStore(), strtolower($humanValidatorName));
        }

        $message .= " Further information: $description";

        $this->getOrderHelper()->setFraudOrderStatusAndComment($order, $message);

        parent::__construct($message);
    }


    /**
     * @return Firtal_Antifraud_Helper_Order
     */
    protected function getOrderHelper() {

        if(!$this->orderHelper) {

            $this->orderHelper = Mage::helper('antifraud/order');

        }

        return $this->orderHelper;

    }

    /**
     * @return Firtal_Antifraud_Helper_Config
     */
    protected function getConfigHelper() {

        if(!$this->configHelper) {

            $this->configHelper = Mage::helper('antifraud/config');

        }

        return $this->configHelper;

    }

}