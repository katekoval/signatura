<?php

class Firtal_Antifraud_Model_Config_Source_Order_Status extends Mage_Sales_Model_Order_Status {
	
	public function toOptionArray(){
		
		$orderStatusCollection = $this->getResourceCollection()->getData();

		$options = [];

		foreach ($orderStatusCollection as $key => $orderStatus) {
			
			$options[$key] = [
				'label' => $orderStatus['label'],
				'value' => $orderStatus['status']
			];

		}

		return $options;

	}
}