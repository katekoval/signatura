<?php

class Firtal_Antifraud_Model_Config_Source_Tld_Listing
{
    const BLACKLISTING = 0;
    const WHITELISTING = 1;

    public function toOptionArray()
    {
        return [
            ['value' => 0, 'label' => "Blacklisting - All the TLD's written are considered fraud"],
            ['value' => 1, 'label' => "Whitelisting - All others but the TLD's written are considered fraud"]
        ];
    }
}