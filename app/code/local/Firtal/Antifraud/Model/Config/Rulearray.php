<?php
/**
 * Created by PhpStorm.
 * User: slowmose
 * Date: 29/05/2018
 * Time: 16.57
 */

class Firtal_AntiFraud_Model_Config_Rulearray extends Firtal_Businesslogic_Model_Config_Array
{
    protected function _afterLoad()
    {
        $res = parent::_afterLoad();

        $this->setValue(
            $this->sanitizeRules($this->getValue())
        );

        return $res;
    }

    protected function _beforeSave()
    {
        $this->setValue(
            $this->sanitizeRules($this->getValue())
        );

        return parent::_beforeSave();
    }

    /**
     * @param $rules
     * @return array
     */
    protected function sanitizeRules($rules)
    {
        if (!is_array($rules)) {
            return $rules;
        }

        $returnRules = [];

        foreach ($rules as $ruleIndex => $conditions) {
            if ($ruleIndex === ":RULE") {
                continue;
            }

            $tmpConditions = [];

            foreach ($conditions as $conditionIndex => $condition) {
                if ($conditionIndex === ":CONDITION") {
                    continue;
                }

                $tmpConditions[] = $condition;
            }

            $returnRules[] = $tmpConditions;
        }

        return $returnRules;
    }
}