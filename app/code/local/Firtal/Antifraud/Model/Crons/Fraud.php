<?php

class Firtal_Antifraud_Model_Crons_Fraud
{
    const MYSQL_DATE_FORMAT = 'YYYY-MM-dd HH:mm:ss';

    protected $moduleHelper;
    protected $orderHelper;
    protected $logHelper;

    public function check() {

        foreach (Mage::getModel('core/store')->getCollection() as $store) {

            if(!$this->getModuleHelper()->isActive($store)) {
                continue;
            }

            $this->handleOrders($store);

        }

    }

    private function handleOrders($store) {
        $orders = $this->getLatestOrders($store);

        $this->getLogHelper()->log("Handling orders " . count($orders));

        foreach ($orders as $order) {
            $this->getOrderHelper()->validateOrder($order);
        }

    }

    protected function getLatestOrders($store) {

        $now = new Zend_Date();

        $latestTime = $now->subDay(1)->toString(self::MYSQL_DATE_FORMAT);

        return Mage::getModel('sales/order')
                ->getCollection()
                ->addAttributeToSelect('*')
                ->addAttributeToFilter('store_id', array('eq' => $store->getId()))
                ->addAttributeToFilter('created_at', array('gteq' => $latestTime))
                ->addAttributeToFilter('status', array('eq' => Mage::helper('antifraud/config')->getEpayNewOrderStatus()))
                ->addAttributeToFilter('firtal_fraud_checked', array('eq' => 0));

    }

    /**
     * @return Firtal_Antifraud_Helper_Module
     */
    protected function getModuleHelper() {
        return Mage::helper('antifraud/module');
    }

    /**
     * @return Firtal_Antifraud_Helper_Order
     */
    protected function getOrderHelper() {
        return Mage::helper('antifraud/order');
    }

    /**
     * @return Firtal_Antifraud_Helper_Log
     */
    protected function getLogHelper() {
        return Mage::helper('antifraud/log');
    }
}