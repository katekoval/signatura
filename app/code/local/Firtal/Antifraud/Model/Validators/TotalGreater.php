<?php

class Firtal_Antifraud_Model_Validators_TotalGreater extends Firtal_Antifraud_Model_FraudValidator implements Firtal_Antifraud_Model_FraudValidatorInterface
{
    private $totalThreshold;

    /**
     * Firtal_Antifraud_Model_Validators_TotalGreater constructor.
     * @param $totalThreshold
     */
    public function __construct($totalThreshold)
    {
        $this->totalThreshold = $totalThreshold;
    }

    public function describe()
    {
        return 'Order total is more than';
    }

    public function validate(Firtal_Antifraud_Model_FraudDTO $dto)
    {
        $orderGrandTotal = $dto->getOrder()->getGrandTotal();

        Mage::log(json_encode([$orderGrandTotal, $this->totalThreshold, $orderGrandTotal > $this->totalThreshold]), null, 'test.log', true);

        if (!$orderGrandTotal) {
            $dto->addError('Order total is not existing!');
            return false;
        }

        if ($orderGrandTotal > $this->totalThreshold) {
            $dto->addError(sprintf("Order total of %.2f exceeds %.2f", $orderGrandTotal, $this->totalThreshold));
            return false;
        }

        return true;
    }
}
