<?php

class Firtal_Antifraud_Model_Validators_PhoneContains extends Firtal_Antifraud_Model_FraudValidator
    implements Firtal_Antifraud_Model_FraudValidatorInterface, Firtal_Antifraud_Model_Validator_HasArrayValue
{
     /**
      * @var array
      */
     private $blacklist;

     public function __construct(array $blacklist)
     {
         $this->blacklist = $blacklist;
     }

     public function validate(Firtal_Antifraud_Model_FraudDTO $dto) {

         if(!$phone = $dto->getOrder()->getShippingAddress()->getTelephone()) {
             $dto->addError('Order phone does not exist');
             return false;
         }

         foreach($this->blacklist as $blacklisted){
             if(stristr($phone, $blacklisted)){
                 $dto->addError(sprintf("%s is on the phone blacklist", $blacklisted));
                 return false;
             }
         }

         return true;
     }

     public function describe()
     {
         return 'Order customer phone contains';
     }

 }
