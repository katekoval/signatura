<?php

class Firtal_Antifraud_Model_Validators_Ip extends Firtal_Antifraud_Model_FraudValidator implements Firtal_Antifraud_Model_FraudValidatorInterface
{
    protected $disallowCheckIps = ['unknown', '127.0.0.1', ''];

    public function requiresValue()
    {
        return false;
    }

    public function describe()
    {
        return "Validate IP country against billing/shipping country";
    }

    public function validate(Firtal_Antifraud_Model_FraudDTO $dto)
    {
        $forwardIps = $this->getOrderIpsToCheck($dto);

        if (empty($forwardIps)) {
            $this->getLogHelper()->logMessage($dto->getOrder(), "doesn't have a forward IP address");
            return true;
        }

        foreach ($forwardIps as $forwardIp) {

            $ipCountryCode = $this->getApiChainModel()->lookup($forwardIp);

            if(!$ipCountryCode) {

                $this->getLogHelper()->logMessage($dto->getOrder(), "Empty response received from IP APIs");
                return true;

            }

            if(!$this->matchesBillingAndShippingCountryCode($dto->getOrder(), $ipCountryCode)){
                $dto->addError(sprintf("IP address: [%s] with country code: [%s] doesn't match order billing or shipping country code", $forwardIp, $ipCountryCode));
                return false;
            }
        }

        return true;
    }

    protected function matchesBillingAndShippingCountryCode(Mage_Sales_Model_Order $order, $ipCountryCode)
    {
        $shippingAddressCode = $order->getShippingAddress()->getCountry();
        $billingAddressCode  = $order->getBillingAddress()->getCountry();

        return $ipCountryCode != $shippingAddressCode
            || $ipCountryCode != $billingAddressCode;
    }

    /**
     * @param $dto
     * @return array
     */
    public function getOrderIpsToCheck(Firtal_Antifraud_Model_FraudDTO $dto)
    {
        $ipsToCheck = [];

        $ips = $dto->getOrder()->getXForwardedFor();

        foreach (explode(', ', $ips) as $ip) {

            if(in_array($ip, $this->disallowCheckIps)) {
                continue;
            }

            $ipsToCheck[] = $ip;

        }

        return $ipsToCheck;

    }

    protected function getApiChainModel() {
        return Mage::getModel('antifraud/ip_apichain');
    }
}