<?php

use Firtal_Antifraud_Model_Zend_Http_Client as Client;

class Firtal_Antifraud_Model_Validators_Bin extends Firtal_Antifraud_Model_FraudValidator implements Firtal_Antifraud_Model_FraudValidatorInterface
{
    protected $_apiEndpoint = 'https://neutrinoapi.com/bin-lookup';
    /** @var Firtal_Antifraud_Helper_Config $_configHelper */
    protected $_configHelper;

    /** @var  Mage_Core_Model_Cache */
    private $cache;

    public function __construct() {
        $this->cache = Mage::getModel('core/cache');
        $this->_configHelper = Mage::helper('antifraud/config');
    }

    public function validate(Firtal_Antifraud_Model_FraudDTO $dto)
    {
        return $this->checkCardBinAgainstOrderCountry($dto);
    }

    public function describe()
    {
        return 'Compare the payment card BIN code with the billing/shipping country code';
    }

    public function requiresValue()
    {
        return false;
    }

    protected function checkCardBinAgainstOrderCountry(Firtal_Antifraud_Model_FraudDTO $dto) {

        $order = $dto->getOrder();

        $epayCardNumber = $this->getEpayOrderStatus($order)->getCardnopostfix();

        if(!$epayCardNumber) {
            // Let's ignore this.
            $this->getLogHelper()->logMessage($dto->getOrder(), "BIN is not validated: No epay order status");
           // $dto->addError('Card doesn\'t exist in epay order status');
            return true;
        }

        $cardBin = substr($epayCardNumber, 0, 6);

        try {
            $apiCardData = $this->lookup($cardBin);
        } catch (Zend_Http_Client_Adapter_Exception $e) {
            $this->getLogHelper()->logMessage($dto->getOrder(),
                "BIN is not validated: Couldn't communicate to BIN API; " . $e->getMessage());
            return true;

        }

        if(!is_object($apiCardData)){
            $this->getLogHelper()->logMessage($dto->getOrder(), "BIN is not validated: ApiCardData is not an object: ". json_encode($apiCardData));
            return true;
        }

        if(!$country = $apiCardData->{"country-code"}){
            $this->getLogHelper()->logMessage($dto->getOrder(), "BIN is not validated: No country-code node found in response: ". json_encode($apiCardData));
            return true;
        }

        if(!$this->validateBinData($order, $country)){
            $dto->addError(sprintf("The card country code [%s] does not match the shipping or billing country code.", $country));
            return false;
        }

        return true;

    }

    /**
     * @param $cardBin
     * @return mixed
     * @throws Zend_Http_Client_Adapter_Exception
     * @throws Zend_Http_Client_Exception
     */
    protected function lookBinUpInApi($cardBin) {

        /** @var Firtal_Antifraud_Model_Zend_Http_Client $client */
        $client = new Client($this->_apiEndpoint);

        $client->setRawData(json_encode([
            'bin-number' => $cardBin,
            'user-id' => $this->_configHelper->getBinCodeApiUserId(Mage::app()->getStore()->getId()),
            'api-key' => $this->_configHelper->getBinCodeApiKey(Mage::app()->getStore()->getId())
        ]));

        $client->setHeaders('Content-type', 'application/json');

        return $client->getJsonResponseBody('GET');

    }

    protected function getEpayOrderStatus($order) {

        return Mage::getModel('antifraud/epay_order_status')->load($order->getIncrementId());

    }

    private function lookup($cardBin)
    {
        $cacheKey = "cc_v2_bin_$cardBin";

        if(Mage::app()->useCache('firtal_antifraud')) {

            $cachedResponse = $this->getBinFromCache($cacheKey);

            if($cachedResponse) {
                return json_decode($cachedResponse);
            }

        }

        if($apiResponse = $this->lookBinUpInApi($cardBin)){
            $this->cache->save(json_encode($apiResponse), $cacheKey, ['ANTIFRAUD_BIN'], 1814400);
        }

        return $apiResponse;
    }

    protected function getBinFromCache($cacheKey)
    {
        if($cacheEntry = $this->cache->load($cacheKey)) {

            return $cacheEntry;
        }

        return false;
    }

    /**
     * @param $order
     * @param $apiCountryCode
     * @return bool
     */
    protected function validateBinData($order, $apiCountryCode)
    {
        $shippingCountryCode = $order->getShippingAddress()->getCountry();
        $billingCountryCode = $order->getBillingAddress()->getCountry();

        if($apiCountryCode != $shippingCountryCode){
            $this->getLogHelper()->logMessage($order, sprintf("BIN country [%s] does not match shippping country [%s]", $apiCountryCode, $shippingCountryCode));
            return false;
        }

        if($apiCountryCode != $billingCountryCode){
            $this->getLogHelper()->logMessage($order, sprintf("BIN country [%s] does not match billing country [%s]", $apiCountryCode, $billingCountryCode));
            return false;
        }

        return true;
    }

}