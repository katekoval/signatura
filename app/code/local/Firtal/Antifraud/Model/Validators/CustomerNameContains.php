<?php

class Firtal_Antifraud_Model_Validators_CustomerNameContains extends Firtal_Antifraud_Model_FraudValidator
    implements Firtal_Antifraud_Model_FraudValidatorInterface, Firtal_Antifraud_Model_Validator_HasArrayValue
{
    /**
     * @var array
     */
    private $blacklist;

    /**
     * Firtal_Antifraud_Model_Validators_CustomerNameContains constructor.
     * @param array $blacklist
     */
    public function __construct(array $blacklist)
    {
        $this->blacklist = $blacklist;
    }

    public function validate(Firtal_Antifraud_Model_FraudDTO $dto)
    {
        if(!($customerName = $dto->getOrder()->getCustomerName())) {
            $dto->addError('Order customer name does not exist');
            return false;
        }

        foreach ($this->blacklist as $blacklisted) {

            if(stristr($customerName, $blacklisted)) {
                $dto->addError(sprintf("%s is on the customer name blacklist", $blacklisted));
                return false;
            }

        }

        return true;

    }

    /**
     * Describes this validator
     *
     * @return string
     */
    public function describe()
    {
        return 'Order customer name contains';
    }
}