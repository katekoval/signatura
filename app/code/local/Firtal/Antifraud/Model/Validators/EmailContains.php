<?php

class Firtal_Antifraud_Model_Validators_EmailContains extends Firtal_Antifraud_Model_FraudValidator
    implements Firtal_Antifraud_Model_FraudValidatorInterface, Firtal_Antifraud_Model_Validator_HasArrayValue
{
     /**
      * @var array
      */
     private $blacklist;

     public function __construct(array $blacklist)
     {
         $this->blacklist = $blacklist;
     }

     public function validate(Firtal_Antifraud_Model_FraudDTO $dto) {

         if(!$email = $dto->getOrder()->getCustomerEmail()) {
             $dto->addError('Order customer email does not exist');
             return false;
         }

         foreach($this->blacklist as $blacklisted){
             if(stristr($email, $blacklisted)){
                 $dto->addError(sprintf("%s is on the email blacklist", $blacklisted));
                 return false;
             }
         }

         return true;
     }

     public function describe()
     {
         return 'Order customer e-mail contains';
     }

 }
