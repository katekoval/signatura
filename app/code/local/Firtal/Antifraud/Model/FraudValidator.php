<?php


abstract class Firtal_Antifraud_Model_FraudValidator implements Firtal_Antifraud_Model_FraudValidatorInterface
{
    protected $next;

    protected $validationDataModel;

    abstract public function validate(Firtal_Antifraud_Model_FraudDTO $dto);

    final public function handle(Firtal_Antifraud_Model_FraudDTO $dto, $next)
    {
        $array = explode("_", get_class($this));

        return end($array);
    }

    public function requiresValue()
    {
        return true;
    }

    public function description()
    {
        return $this->describe() . ($this instanceof Firtal_Antifraud_Model_Validator_HasArrayValue ? ' (can be comma-seperated)' : '');
    }

    protected function throwValidatorException($order = "", $description=null)
    {
        throw new Firtal_Antifraud_Model_FraudValidatorException($this, $order, $description);
    }

    /**
     * @return Firtal_Antifraud_Helper_Log
     */
    protected function getLogHelper() {
        return Mage::helper('antifraud/log');
    }

    /**
     * @return Firtal_Antifraud_Helper_Order
     */
    protected function getOrderHelper()
    {
        return Mage::helper('antifraud/order');
    }
}