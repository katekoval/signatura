<?php

class Firtal_Antifraud_Model_Mysql4_Epay_Order_Status extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {
        $this->_init('antifraud/epay_order_status', 'orderid');
        $this->isPkAutoIncrement = false;
    }
}