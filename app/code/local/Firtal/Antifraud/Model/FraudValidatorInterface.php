<?php

interface Firtal_Antifraud_Model_FraudValidatorInterface {

	/**
	 * Handle next function in stack
	 *
	 * @return bool
	 **/

	public function validate(Firtal_Antifraud_Model_FraudDTO $dto);


    /**
     * Does this require a value?
     *
     * @return bool
     */
    public function requiresValue();


    /**
     * Describes this validator
     *
     * @return string
     */
    public function describe();

    /**
     * A description with helpers
     * @return string
     */
    public function description();

}