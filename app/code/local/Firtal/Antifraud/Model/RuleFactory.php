<?php

use Firtal_Antifraud_Model_Rule as Rule;
use Firtal_Antifraud_Model_Condition as Condition;

class Firtal_Antifraud_Model_RuleFactory
{
    /**
     * @param Rule[] $config
     */
    public function fromConfigArray(array $config)
    {
        $rules = [];

        foreach($config as $ruleConfig){
            $rule = new Rule;

            foreach($ruleConfig as $condition){
                $rule->addCondition(
                    new Condition(
                        $this->getValidatorFactory()->build($condition['validator'], $condition['value'])
                    )
                );
            }

            $rules[] = $rule;
        }

        return $rules;
    }

    /**
     * @return Firtal_Antifraud_Model_ValidatorFactory
     */
    protected function getValidatorFactory()
    {
        return Mage::getSingleton('antifraud/validatorFactory');
    }
}