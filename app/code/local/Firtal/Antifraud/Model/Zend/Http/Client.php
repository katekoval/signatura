<?php

class Firtal_Antifraud_Model_Zend_Http_Client extends Zend_Http_Client {

    public function getJsonResponseBody($method = 'GET') {

        $response = $this->request($method);

        if($response->getStatus() >= 300){
            throw new Zend_Http_Client_Adapter_Exception("Response code from API was " . $response->getStatus() . " body: " .  var_export($response->getMessage(), true));
        }

        return json_decode($response->getBody());

    }

}