<?php

class Firtal_Antifraud_Model_Source_Validators
{
    protected $_validators = [];

    public function toOptionArray() {

        $this->_validators = [];

        foreach ($this->getValidatorHelper()->getAllValidators() as $validator) {

            $model = $this->getValidatorHelper()->getValidatorModelByFileName($validator['text']);
            $this->addValidatorOption($model);

        }

        return $this->_validators;

    }

    protected function addValidatorOption(Firtal_Antifraud_Model_FraudValidatorInterface $model)
    {
        $validator = get_class($model);

        $this->_validators[] = [
            'value' => $validator,
            'label' => $model->description(),
            'requires_value' => $model->requiresValue()
        ];
    }

    /**
     * @return Firtal_Antifraud_Helper_Validator
     */
    protected function getValidatorHelper()
    {
        return Mage::helper('antifraud/validator');
    }
}