<?php

abstract class Firtal_Antifraud_Model_Ip_ApiChainLink
    extends Firtal_Antifraud_Model_Zend_Http_Client
    implements Firtal_Antifraud_Model_Ip_ApiChainLinkContract
{
    protected $logHelper;

    public function __construct($uri, $config)
    {
        parent::__construct($uri, $config);

        $this->setConfig(array(
           'storeresponse' => true
        ));
    }

    public function setTimeout($seconds) {

        $this->setConfig(array(
            'timeout' => $seconds
        ));

    }

    /**
     * @return Firtal_Antifraud_Helper_Log
     */
    public function getLogHelper() {

        if(!$this->logHelper) {
            $this->logHelper = Mage::helper('antifraud/log');
        }

        return $this->logHelper;

    }

}