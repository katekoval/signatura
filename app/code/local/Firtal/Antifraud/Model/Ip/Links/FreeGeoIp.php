<?php

class Firtal_Antifraud_Model_Ip_Links_FreeGeoIp extends Firtal_Antifraud_Model_Ip_ApiChainLink
{
    protected $apiEndpoint = 'http://freegeoip.net/json/';

    protected $ip;
    protected $client;

    public function handle($ip, \Closure $next) {

        try {

            $this->setUri($this->apiEndpoint.$ip);

            $result = $this->getJsonResponseBody();

        }
        catch (Zend_Http_Client_Adapter_Exception $e) {
            return $next($ip);
        }
        catch (Zend_Uri_Exception $e) {
            return $next($ip);
        }

        if($this->getLastResponse()->isError()) {

            $this->getLogHelper()->log($this->apiEndpoint.': bad response: '.$this->getLastResponse()->getStatus().', continuing');
            return $next($ip);

        }

        return $result->country_code;

    }

}