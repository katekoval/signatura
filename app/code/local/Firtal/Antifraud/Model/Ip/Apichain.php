<?php

class Firtal_Antifraud_Model_Ip_Apichain
{
    protected $links = [
        'Firtal_Antifraud_Model_Ip_Links_IpApi',
        'Firtal_Antifraud_Model_Ip_Links_FreeGeoIp'
    ];

    public function lookup($ip) {

        return $this->run($ip, function($ip) {
        });

    }

    public function run($ip, \Closure $lastCallback) {

        $currentCallback = $lastCallback;

        foreach (array_reverse($this->links) as $chainLink) {

            $chainLink = new $chainLink;
            $chainLink->setTimeout(4);

            $currentCallback = function($ip) use ($chainLink, $currentCallback) {

                return $chainLink->handle($ip, $currentCallback);

            };

        }

        return $currentCallback($ip);

    }
}