<?php

interface Firtal_Antifraud_Model_Ip_ApiChainLinkContract
{
    public function handle($ip, \Closure $next);

}