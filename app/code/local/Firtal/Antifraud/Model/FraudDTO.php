<?php

class Firtal_Antifraud_Model_FraudDTO
{
    protected $order;
    protected $errors;

    public function __construct($order) {
        $this->order = $order;
    }

    public function getOrder() {
        return $this->order;
    }

    public function addError($errorMsg) {
        $this->errors[] = $errorMsg;
    }

    public function errorsToString() {
        $errors = $this->errors;

        $last  = array_pop($errors);

        if(count($errors) === 0){
            return $last;
        }

        return implode(", ", $errors) . " and " . $last;
    }

    public function hasErrors() {
        return count($this->errors) > 0;
    }

    public function clearErrors()
    {
        $this->errors = [];
    }
}