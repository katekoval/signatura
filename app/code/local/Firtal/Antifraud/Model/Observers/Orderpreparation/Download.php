<?php

class Firtal_Antifraud_Model_Observers_Orderpreparation_Download {

    protected $dataHelper;
    protected $configHelper;
    protected $moduleHelper;

    protected $pageHeight = 820; // from MDN_Orderpreparation_Model_Pdf_Pdfhelper
    protected $x;
    protected $y;

    public function addComment($observer) {

        $event = $observer->getEvent();

        $orderId = $event->getOrderToPrepare()->getOrderId();
        $order = Mage::getModel('sales/order')->load($orderId);

        if(!$this->getModuleHelper()->isActive($order->getStore())
            || !$this->hasFraudStatus($order)) {
            return;
        }

        $pdf = $event->getPdf();

        $this->incrementOrderCountProperty($pdf);
        $page = $this->setPageSettings($pdf);

        $this->addCommentHeading($pdf, $page);
        $this->addPdfComment($pdf, $page, $order);


    }

    protected function hasFraudStatus($order) {

        return $order->getStatus() == $this->getConfigHelper()->getOrderStatus($order->getStore());

    }

    protected function addCommentHeading($pdf, $page) {

        if($pdf->properties['fraudOrderCount'] > 1) {
            return;
        }

        $page->drawText($this->getDataHelper()->__('SUSPEKTE ORDRE(R):'), $this->x, $this->y, 'UTF-8');

    }

    protected function addPdfComment($pdf, $page, $order) {

        $this->y -= ($pdf->properties['fraudOrderCount'] * 15);

        $page->drawText('#'.$order->getIncrementId(), $this->x, $this->y, 'UTF-8');

    }

    protected function setPageSettings($pdf) {

        $page = $pdf->pages[0];
        $page->setFont(Zend_Pdf_Font::fontWithName(Zend_Pdf_Font::FONT_HELVETICA_BOLD), 10);

        $this->x = $page->getWidth() - 120;
        $this->y = $this->pageHeight - 160;

        return $page;

    }

    /**
     * @return Firtal_Antifraud_Helper_Module
     */
    protected function getModuleHelper() {
        return Mage::helper('antifraud/module');
    }

    /**
     * @return Firtal_Antifraud_Helper_Config
     */
    protected function getConfigHelper() {
        return Mage::helper('antifraud/config');
    }

    /**
     * @return Firtal_Antifraud_Helper_Data
     */
    protected function getDataHelper() {
        Mage::helper('antifraud');
    }

    protected function incrementOrderCountProperty($pdf) {

        if (!$pdf->properties['fraudOrderCount']) {
            $pdf->properties['fraudOrderCount'] = 0;
        }

        $pdf->properties['fraudOrderCount']++;
    }


}