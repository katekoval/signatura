<?php

class Firtal_Antifraud_Model_Condition
{
    /**
     * @var Firtal_Antifraud_Model_FraudValidatorInterface
     */
    private $validator;

    public function __construct(Firtal_Antifraud_Model_FraudValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    public function matches(Firtal_Antifraud_Model_FraudDTO $dto)
    {
        return ! $this->validator->validate($dto);
    }
}