<?php

class Firtal_Antifraud_Model_Rule
{
    /** @var Firtal_Antifraud_Model_Condition[] */
    protected $conditions = [];

    public function addCondition(Firtal_Antifraud_Model_Condition $condition)
    {
        $this->conditions[] = $condition;
    }

    public function matches(Firtal_Antifraud_Model_FraudDTO $dto)
    {
        foreach($this->conditions as $condition){

            if(!$condition->matches($dto)){
                return false;
            }

        }

        return true;
    }
}