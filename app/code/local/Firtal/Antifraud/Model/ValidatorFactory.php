<?php

class Firtal_Antifraud_Model_ValidatorFactory
{
    use Firtal_Antifraud_Helper_Reflects;
    /**
     * @param string $validatorClass
     * @param mixed $value
     * @return Firtal_Antifraud_Model_FraudValidatorInterface
     * @throws Exception
     */
    public function build($validatorClass, $value = null)
    {
        /** @var ReflectionClass $reflection */
        $reflection = $this->reflect($validatorClass);

        if(!$reflection->implementsInterface('Firtal_Antifraud_Model_FraudValidatorInterface')){
            throw new Exception("Not a valid validator: ".$validatorClass);
        }

        if($reflection->implementsInterface('Firtal_Antifraud_Model_Validator_HasArrayValue')) {
            $value = array_map('trim', explode(",", $value));
        }

        return new $validatorClass($value);

    }

}