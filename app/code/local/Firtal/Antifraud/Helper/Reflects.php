<?php

trait Firtal_Antifraud_Helper_Reflects
{
    protected function reflect($className)
    {
        return new ReflectionClass($className);
    }
}