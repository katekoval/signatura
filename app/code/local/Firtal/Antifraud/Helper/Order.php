<?php

use Firtal_Antifraud_Model_FraudDTO as FraudDTO;

class Firtal_Antifraud_Helper_Order
{

    protected $fraudValidatorClassName = 'Firtal_Antifraud_Model_FraudValidator';

    public function validateOrder($order)
    {
        $dto = new FraudDTO($order);

        $validationResult = $this->validateOrderAgainstRules($dto);

        $this->setValidated($dto->getOrder());

        if(!$validationResult){
            $this->getLogHelper()->logMessage($dto->getOrder(), 'Has errors on the DTO. Fraud status is set');
            $this->setFraudOrderStatusAndComment($dto->getOrder(), $dto->errorsToString());
            return;
        }

        $this->validationSuccess($dto->getOrder());
    }


    public function setFraudOrderStatusAndComment($order, $comment)
    {
        $order
            ->addStatusToHistory($this->getConfigHelper()->getOrderStatus($order->getStore()), $comment)
            ->save();
    }


    protected function setValidated($order) {
        $order
            ->setFirtalFraudChecked(1)
            ->save();
    }

    protected function validationSuccess($order) {

        $configSuccessMessage = $this->getConfigHelper()->getSuccessMessage($order->getStore());

        if(!$configSuccessMessage) {
            $this->getLogHelper()->logMessage($order, 'order passed fraud validation');
        }

        $this->getLogHelper()->logMessage($order, $configSuccessMessage);

    }

    /**
     * @return Firtal_Antifraud_Helper_Order
     */
    protected function getOrderHelper()
    {
        return Mage::helper('antifraud/order');
    }

    /**
     * @return Firtal_Antifraud_Helper_Log
     */
    protected function getLogHelper()
    {
        return Mage::helper('antifraud/log');
    }

    /**
     * @return Firtal_Antifraud_Helper_Validator
     */
    protected function getValidatorHelper()
    {
        return Mage::helper('antifraud/validator');
    }

    /**
     * @return Firtal_Antifraud_Helper_Config
     */
    protected function getConfigHelper()
    {
        return Mage::helper('antifraud/config');
    }

    /**
     * @return Firtal_Antifraud_Model_RuleFactory
     */
    private function getRuleFactory()
    {
        return Mage::getModel('antifraud/ruleFactory');
    }

    /**
     * @param Firtal_Antifraud_Model_FraudDTO
     * @return bool
     */
    private function validateOrderAgainstRules(Firtal_Antifraud_Model_FraudDTO $dto)
    {
        $last = function () {
            return true;
        };

        $rulesArray = $this->getConfigHelper()->getRules($dto->getOrder()->getStore());

        $rules = $this->getRuleFactory()->fromConfigArray($rulesArray);

        $next = $last;
        /** @var Firtal_Antifraud_Model_Rule $rule */
        foreach ($rules as $rule) {
            $next = function (FraudDTO $dto) use ($next, $rule) {
                $dto->clearErrors();

                if ($rule->matches($dto)) {
                    return false;
                }

                return $next($dto);
            };
        }

        return $next($dto);
    }

}