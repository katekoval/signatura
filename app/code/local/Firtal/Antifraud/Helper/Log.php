<?php

class Firtal_Antifraud_Helper_Log extends Mage_Core_Helper_Abstract
{
    public function logMessage($order, $message) {
        Mage::log($order->getIncrementId().': '.$message, null, 'firtal_antifraud.log', true);
    }

    public function log($message) {
        Mage::log($message, null, 'firtal_antifraud.log', true);
    }
}