<?php

class Firtal_Antifraud_Helper_Config extends Mage_Core_Helper_Abstract {

    const SETTING_SUCCESS_MESSAGE  = 'validation/success_message';
    const SETTING_VALIDATION_RULES = 'validation/rules';

    const SETTING_ORDER_STATUS  = 'order/status';
    const SETTING_ORDER_WARNING = 'order/warning';

    const SETTING_BINCODE_API_USER_ID = 'bincode_api/user_id';
    const SETTING_BINCODE_API_KEY = 'bincode_api/api_key';

    protected function getSettingOptions() {
		return 'antifraud_options/';
	}

    public function getSuccessMessage(Mage_Core_Model_Store $store)
    {
        return $store->getConfig($this->getSettingOptions().self::SETTING_SUCCESS_MESSAGE);
    }

    public function getOrderWarningMessage(Mage_Core_Model_Store $store)
    {
	    return $store->getConfig($this->getSettingOptions().self::SETTING_ORDER_WARNING);
    }

    public function getOrderStatus(Mage_Core_Model_Store $store)
    {
	    return $store->getConfig($this->getSettingOptions().self::SETTING_ORDER_STATUS);
	}

    public function getOrderStatusComment(Mage_Core_Model_Store $store, $validatorName)
    {
		return $store->getConfig(($this->getSettingOptions().'order_status_comments/'.$validatorName));
	}

    public function getEpayNewOrderStatus() {
        return Mage::getStoreConfig('payment/epay_standard/order_status_after_payment');
    }

    public function getBinCodeApiUserId() {
        return Mage::getStoreConfig(($this->getSettingOptions().self::SETTING_BINCODE_API_USER_ID));
    }

    public function getBinCodeApiKey() {
        return Mage::getStoreConfig(($this->getSettingOptions().self::SETTING_BINCODE_API_KEY));
    }

    public function getRules($store)
    {

        if(!method_exists($store, 'getConfig')) {
            return [];
        }

        return json_decode($store->getConfig($this->getSettingOptions() . self::SETTING_VALIDATION_RULES), true);
    }

}