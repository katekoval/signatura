<?php


class Firtal_Antifraud_Helper_Validator
{
    protected $validatorModels = [];

    public function getValidatorModel($validatorClass)
    {
        if (!$this->validatorModels[$validatorClass]) {
            $name = $this->getValidatorName($validatorClass);
            $this->validatorModels[$validatorClass] = Mage::getModel('antifraud/validators_' . $name);
        }

        return $this->validatorModels[$validatorClass];
    }

    public function getValidatorModelByFileName($fileName)
    {
        $name = explode('.', $fileName)[0];

        if (!array_key_exists($name, $this->validatorModels)) {
            $this->validatorModels[$name] = Mage::getModel('antifraud/validators_' . $name);
        }

        return $this->validatorModels[$name];
    }


    public function getReflection($validator) {
        return new ReflectionClass($validator);
    }

    public function resolveValidatorName($validator)
    {
        $validatorName = explode("_", $this->getReflection($validator)->getShortName());
        return end($validatorName);
    }

    public function getAllValidators()
    {
        $file = new Varien_Io_File();

        $dir = Mage::getModuleDir('model', 'Firtal_Antifraud') . '/Model/Validators/';

        $file->open(['path' => $dir]);
        return $file->ls();
    }

    private function getValidatorName($validatorClass)
    {
        return end(explode("_", $validatorClass));
    }

}