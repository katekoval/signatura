<?php

class Firtal_Antifraud_Helper_Module extends Mage_Core_Helper_Abstract {

	const SETTING_MODULE_ACTIVE = 'antifraud_options/general/active';

	public function isActive($store = null) {

	    if($store) {
            return (bool) $store->getConfig(self::SETTING_MODULE_ACTIVE);
        }

		return (bool) Mage::getStoreConfig(self::SETTING_MODULE_ACTIVE);

	}

}