<?php

class Firtal_Productcreator_Helper_Data extends Mage_Core_Helper_Data
{

    /**
     * @return string[]
     *
     * Returns list of allowed ips
     */
    private function getAllowedIplist(): array
    {
        // List of allowed ips
        return [
            '82.208.14.110',
            '82.208.14.111',
            '82.208.14.112',
            '82.208.14.113',
            '82.208.14.114',
            '82.208.14.115',
            '82.208.14.116',
            '82.208.14.117',
            '82.208.14.118',
            '82.208.14.119'
        ];
    }

    /**
     * @param $key
     * @return false|string
     *
     * Decodes auth token and returns the decoded token
     */
    public function decodeAuth($key)
    {
        $key = base64_decode($key); // Base64 decode key
        $key = substr($key, 5); // Remove 5 first characters
        $key = base64_decode(substr($key, 0, -12)); // base64 decode timestamp, Remove 12 last characters

        return $key;
    }

    /**
     * @param $visitorIp
     * @return bool
     *
     * Check if visitor ip is allowed
     */
    public function isIpAllowed($visitorIp): bool
    {
        if(in_array($visitorIp, $this->getAllowedIplist(), true)) //Check if ip is whitelisted
        {
            return true;
        }

        return false;
    }

    /**
     * @return string
     *
     * Creates a new auth token for 10min use
     * @throws Exception
     */
    public function getNewAuthToken(): string
    {
        return base64_encode(substr(md5(time()),random_int(0,26),5) . base64_encode(time()) . substr(md5(time()),random_int(0,26),12)); // New token
    }

    /**
     * @param $params
     * @return string
     *
     * Check if required params is provided
     */
    public function checkParams($params)
    {
        // List of required params
        $required_params = [
            'product_sku',
            'product_name',
            'product_weight',
            'product_ssin',
            'product_enabled',
            'product_taxclassid',
            'product_price',
            'product_cost',
            'product_barcode',
            'product_stores',
        ];

        foreach($required_params as $required_param)
        {

            if(!isset($params[$required_param])) // Check if param is set
            {
                $err_msg = 'Parameter ' . $required_param . ' is empty or missing';
                Mage::log($err_msg, null, ERROR_LOG_PATH, true);
                return $err_msg;
            }

        }

        return true;
    }

    /**
     * @param $token
     * @return bool
     *
     * Check if token is allowed
     */
    public function securityCheck($token): bool
    {
        $visitorIp = substr($_SERVER['HTTP_X_FORWARDED_FOR'], 0, strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ','));
        $auth_key = $this->decodeAuth($token);

        return $this->isIpAllowed($visitorIp) === true && time() - $auth_key < 2400; // Check if auth key is available (available for 40 minutes)
    }

    /**
     * @return int[][]
     */
    public function allowedStores(): array
    {
        return array(
            'billigvoks_dk' => array('website_id' => 1, 'store_id' => 1),
            'signatura_dk'  => array('website_id' => 2, 'store_id' => 2),
        );
    }

    /**
     * @param $department
     * @return mixed
     *
     * Get category parameter from category mapping
     */
    public function getCatMapping($department)
    {
        $catMapping = array(
            'beauty' => 'billigvoks_cat_ids',
            'green'  => 'helsebixen_cat_ids',
            'men'    => 'made4men_dk_cat_ids',
        );

        return $catMapping[$department];
    }

    /**
     * @param $stores
     * @return array|int[]
     */
    public function getBranchTo($stores): array
    {
        $branchToIds = array();
        $stores = explode(',', $stores);

        $branchToFields = array(
            'billigvoks_dk' => 752,
            'signatura_dk'  => 753,
        );

        foreach($stores as $store)
        {
            if(isset($branchToFields[$store]))
            {
                $branchToIds[] = (int)$branchToFields[$store];
            }
        }

        if(count($branchToIds) === 1) {
            return array($branchToIds[0]);
        }

        return (array)$branchToIds;
    }

}