<?php
const client_id = '89$$+x2%3%M6AY93sg9S8KePu&y_Hsc$@?vRwfMBH2U=amRJ&r28fkBXbUaWqC_k@XsF7CqPgXKrQ@tU3P*+EW_z6=7hJdLbUTgkExa$!jH-HAvZ+bECRPJL*^A_cpjk';
const client_secret = 'c@e@wshz5Y8&BX?N?SHND?MsG4usSuL%yab$=k#93w9$J4j9C-BN$+#fZ?QHWFGq2FYYAscX!5qHnJ!wMdrvwzg*r2z#T&D%45w_=z2fcKh&zTrA5gBKfh*@8qWyPVkw';

class Firtal_Productcreator_ApiController extends Mage_Core_Controller_Front_Action
{

    /**
     * Create product if auth token and IP address is allowed
     */
    public function createAction()
    {
        $this->modify('create');
    }

    /**
     * Update product if auth token and IP address is allowed
     */
    public function updateAction()
    {
        $this->modify('edit');
    }

    /**
     * @param $method
     * @return Mage_Core_Controller_Response_Http|Zend_Controller_Response_Abstract
     * @throws Zend_Controller_Response_Exception
     *
     * Logic for create/update action
     */
    private function modify($method)
    {
        $helper = Mage::helper('firtal_productcreator');
        $token = $this->getRequest()->getParam('token');
        $product_data = $this->getRequest()->getParams();

        if($helper->securityCheck($token) === true)
        {
            //If debug run, lets show the send parameters
            if($product_data['debug'] === 'true') {
                return $this->sendResponse(201, print_r($product_data));
            }

            switch ($method) {
                case "create":
                    if(($paramErrors = $helper->checkParams($product_data)) !== true) { // Check if any missing params
                        return $this->sendResponse(400, $paramErrors); // Show which parameters is missing
                    }

                    $createResponse = Mage::getModel('productcreator/product')->createProduct($product_data); // Create product
                    break;

                case "edit":
                    $createResponse = Mage::getModel('productcreator/product')->updateProduct($product_data); // Update product
                    break;
            }

            return $this->sendResponse($createResponse[0], $createResponse[1]);
        }

        return $this->sendResponse(401, 'Access denied.');
    }

    /**
     * @return Mage_Core_Controller_Response_Http|Zend_Controller_Response_Abstract
     * @throws Zend_Controller_Response_Exception
     *
     * Generate auth token
     */
    public function authgenerateAction()
    {
        // Define params
        $helper = Mage::helper('firtal_productcreator');
        $clientId = $this->getRequest()->getParam('client_id');
        $clientSecret = $this->getRequest()->getParam('client_secret');
        $clientUuid = $this->getRequest()->getParam('uuid');
        $visitorIp = substr($_SERVER['HTTP_X_FORWARDED_FOR'], 0, strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ','));

        if(
            $helper->isIpAllowed($visitorIp) === true && // Check if ip is allowed
            $clientId === client_id && // Check if client id is matching
            $clientSecret === client_secret && // Check if client secret is matching
            $clientUuid) // Check if client uuid exists
        {
            return $this->sendResponse(201, $helper->getNewAuthToken()); // Show generated token
        }

        return $this->sendResponse(401, 'Access denied.'); // Deny access
    }

    public function updatePriceAction()
    {
        $helper = Mage::helper('firtal_productcreator');
        $token = $this->getRequest()->getParam('token');
        $product_data = $this->getRequest()->getParams();

        if($helper->securityCheck($token) === true)
        {
            $updated = Mage::getModel('productcreator/product')->updateProductPrice($product_data); // Create product
            return $this->sendResponse((int)$updated[0], (string)$updated[1]);
        }

        return $this->sendResponse(401, 'Access denied.');
    }

    /**
     * @param $code
     * @param $msg
     * @return Mage_Core_Controller_Response_Http|Zend_Controller_Response_Abstract
     * @throws Zend_Controller_Response_Exception
     *
     * Send http response
     */
    private function sendResponse($code, $msg)
    {
        return $this->getResponse()
            ->setHttpResponseCode($code)
            ->setBody($msg);
    }

}