<?php
const ERROR_LOG_PATH = 'firtal_productcreator_error.log';
const CURRENT_DEPARTMENT = 'beauty';

class Firtal_Productcreator_Model_Product extends Mage_Core_Model_Abstract
{

    /**
     * @param $product_data
     * @return array
     *
     * Creates product in catalog_product
     */
    public function createProduct($product_data)
    {
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        $product = Mage::getModel('catalog/product');
        $websiteIds = $this->getWebsiteStoreIds($product_data['product_stores'], 'website_id');
        $storeIds = $this->getWebsiteStoreIds($product_data['product_stores'], 'store_id');
        $productExistsSSIN = Mage::getModel('catalog/product')->loadByAttribute('ssin', $product_data['product_ssin']);
        $productExistsSKU = Mage::getModel('catalog/product')->loadByAttribute('sku', $product_data['product_sku']);

        //Check if any websites matches
        if(count($websiteIds) < 1) {
            $err_msg = 'No matching store for product with SSIN ' . $product_data['product_ssin'];
            return array(401, $err_msg);
        }

        if($productExistsSSIN) {
          $err_msg = 'Product with SSIN ' . $product_data['product_ssin'] . ' already exists';
          return array(401, $err_msg);
        }

        if($productExistsSKU) {
            $err_msg = 'Product with SKU ' . $product_data['product_sku'] . ' already exists';
            return array(401, $err_msg);
        }

        try {
            $product
                ->setWebsiteIds($websiteIds) // Website ids in array
                ->setStoreIds((array)$storeIds) // Store ids in array
                ->setAttributeSetId(4)
                ->setTypeId('simple') // Product type
                ->setSku($product_data['product_sku']) // Firtal SKU
                ->setName($product_data['product_name'])
                ->setWeight($product_data['product_weight'])
                ->setSsin($product_data['product_ssin'])
                ->setStatus((int)$product_data['product_enabled']) // Product status (1 - enabled, 2 - disabled)
                ->setTaxClassId((int)$product_data['product_taxclassid']) // Tax class (0 - none, 1 - default, 2 - taxable, 4 - shipping)
                ->setVisibility(Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH) // Catalog and search visibility
                ->setPrice($product_data['product_price']) // Price in form 11.22
                ->setCost($product_data['product_cost']) // Cost in form 11.22
                ->setBarcode($product_data['product_barcode'])
                ->setStockData(array('manage_stock' => 1))
            ;

            //Set optional data (special price, branch_to etc.)
            $product = $this->setOptionalData($product_data, $product);

            if (strpos($product_data['product_image'], 'http') !== false) // Check if product image exists
            {
                $product_images = array_reverse(explode(",", $product_data['product_image'])); // Split in to array

                foreach($product_images as $pimage)
                {
                    $product_image = $this->createProductImage($pimage, $product_data['product_name']); // Download product image and save locally
                    $product->addImageToMediaGallery($product_image, array('image','thumbnail','small_image'), false, false); // Assigning image, thumb and small image to media gallery
                }
            }

            try {
                $product->save(); // Save product
            } catch(Exception $e) {
                $err_msg = 'Failed creating product with SSIN ' . $product_data['product_ssin'] . '. Trail: ' . $e;
                return array(401, $err_msg);
            }

            $product->save(); // Save product

            // Check if we have a product id
            if(is_numeric($product->getId()))
            {
                return array(201, 'Product created with id ' . $product->getId());
            }

            $err_msg = 'Product with SSIN ' . $product_data['product_ssin'] . ' could not be created';
            return array(401, $err_msg);

        } catch(Exception $e) {
            $err_msg = 'Failed creating product with SSIN ' . $product_data['product_ssin'] . '. Trail: ' . $e;
            return array(401, $err_msg);
        }
    }

    /**
     * @param $product_data
     * @return array
     *
     * Creates product in catalog_product
     */
    public function updateProduct($product_data)
    {
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        $product = Mage::getModel('catalog/product')
            ->loadByAttribute('ssin', $product_data['product_ssin']);

        // Check if Numeric store value found
        if(!is_numeric((int)$product_data['store'])) {
            $err_msg = 'Numeric store value not found. Tried to update product with SSIN ' . $product_data['product_ssin'];
            return array(401, $err_msg);
        }

        // Check if product exists on specified store, with specified SSIN
        if(!$product) {
            $err_msg = 'Product with SSIN ' . $product_data['product_ssin'] . ' doesnt exist';
            return array(401, $err_msg);
        }

        $product->setStoreId((int)$product_data['store']); //Set store ID on product

        try {
            $previousPrice = $product->getPrice();
            $previousSpecialPrice = $product->getSpecialPrice();

            eval($product_data['product_data']); // Set optional data (special price, branch_to etc.)
            $product->save(); // Save product

            return array(201, json_encode(array(
                'message' => 'Product with id ' . $product->getId() . ' updated',
                'previousPrice' => $previousPrice,
                'previousSpecialPrice' => $previousSpecialPrice
            )));
        } catch(Exception $e) {
            $err_msg = 'Failed creating product with SSIN ' . $product_data['product_ssin'] . '. Trail: ' . $e;
            return array(401, $err_msg);
        }
    }

    /**
     * @param $data
     * @return array
     *
     * Update price for specific product.
     * Product is loaded by SSIN
     */
    public function updateProductPrice($data)
    {
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        $save = false;

        if(!isset($data['ssin']))
        {
            return array(400, 'Missing ssin');
        }

        if(!isset($data['store_id']))
        {
            return array(400, 'Missing store_id');
        }

        if((!$data['price']) > 0 && (!$data['special_price']) > 0)
        {
            return array(400, 'Missing price or special_price');
        }

        //Load product by SSIN
        $product = Mage::getModel('catalog/product')
            ->loadByAttribute('ssin', $data['ssin']);

        if(!$product) {
            return array(400, 'Product not found');
        }

        if($data['price'] > 0) {
            $product->setPrice($data['price']);
        }

        if($data['special_price'] > 0) {
            $product->setSpecialPrice($data['special_price']);
        }

        //Save product for store id
        $product->setStoreId($data['store_id'])
            ->save();

        return array(201, 'Product with SSIN ' . $data['ssin'] . ' saved');
    }

    /**
     * @param $product_image
     * @param $product_name
     * @return string
     *
     * Download product image and save locally
     */
    private function createProductImage($product_image, $product_name)
    {
        $tmp_save_subfolders = 'tmp' . DS . 'catalog' . DS . 'import';
        $tmp_save_folder = Mage::getBaseDir('media') . DS . $tmp_save_subfolders;

        try {
            $image_type = substr(strrchr($product_image,"."),1); // Get file type

            // Lowercase and remove special characters from filename, to rename. Add _time() in end to make unique URL
            $filename   = strtolower(preg_replace("/[^A-Za-z0-9]/", "", $product_name)) . '_' . time() .'.'.$image_type;
            $filepath   = $tmp_save_folder . DS . $filename;

            if (!is_dir($tmp_save_folder)) { // Check if folder already exists
                if (!mkdir($tmp_save_folder, 0777, true) && !is_dir($tmp_save_folder)) {
                    throw new \RuntimeException(sprintf('Directory "%s" was not created', $tmp_save_folder));
                }
            }

            copy($product_image, $filepath); // Upload image to temp folder

            return 'media' . DS . $tmp_save_subfolders . DS . $filename;
        } catch(Exception $e) {
            $err_msg = 'Failed creating product. Trail: ' . $e;
            Mage::log($err_msg, null, ERROR_LOG_PATH, true);
            return $err_msg;
        }
    }

    /**
     * @param $stores
     * @return array|bool
     */
    private function getWebsiteStoreIds($stores, $type)
    {
        $stores = preg_replace("/[^a-z-_,]/", '', $stores); // Remove spaces
        $stores = explode(",", $stores); // Split in to array
        $allowedStores = Mage::helper('firtal_productcreator')->allowedStores();
        $storesToAdd = array();

        foreach($stores as $store)
        {
            if(isset($allowedStores[$store]))
            {
                $storesToAdd[] = (int)$allowedStores[$store][$type];
            }
        }

        if(count($storesToAdd) == 1) {
            return array($storesToAdd[0]);
        }

        return (array)$storesToAdd;
    }

    private function setOptionalData($data, $product)
    {
        // Optional option. Could for example contain $product->setTextStatus((int)1335)
        if(isset($data['product_optional_option']))
        {
            eval($data['product_optional_option']);
        }

        // Get posted categories from mapping
        $postedCategories = Mage::helper('firtal_productcreator')->getCatMapping(CURRENT_DEPARTMENT);

        // Handle special_price
        if($data['product_specialprice'] !== '')
        {
            $product->setSpecialPrice($data['product_specialprice']);
        }

        // Handle branch_to
        if($data['product_stores'] !== '')
        {
            $branchTo = Mage::helper('firtal_productcreator')->getBranchTo($data['product_stores']);
            $product->setBranchTo($branchTo);
        }

        // Handle categories
        if($data[$postedCategories] !== '')
        {
            // Avoid multiple commas
            $categories = preg_replace("/,+/", ",", $data[$postedCategories]);

            // Array map converts string values to int values
            $categories = array_map('intval', explode(',', $categories));

            // Set category IDS
            $product->setCategoryIds(array($categories));
        }

        // Handle brand
        if(isset($data['product_brand']))
        {
            $brandId = 0;

            $attribute = Mage::getSingleton('eav/config')
                ->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'manufacturer')
                ->getSource()
                ->getAllOptions(false);

            foreach($attribute as $brand)
            {
                // Check if labels matching
                if($brand['label'] == $data['product_brand'])
                {
                    $brandId = $brand['value'];
                    break;
                }
            }

            if($brandId == 0)
            {
                $attribute = Mage::getModel('eav/entity_attribute')->loadByCode('catalog_product', 'manufacturer');
                $value['option'] = array($data['product_brand']);
                $result = array('value' => $value);
                $attribute->setData('option', $result);
                $attribute->save();

                //get its option_id
                $brandId = Mage::getModel('eav/entity_attribute_source_table')
                    ->setAttribute($attribute)
                    ->getOptionId($data['product_brand']);
            }

            $product->setManufacturer($brandId);
        }

        return $product;
    }

}