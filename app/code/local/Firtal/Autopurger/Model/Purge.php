<?php
class Firtal_Autopurger_Model_Purge extends Mage_Core_Model_Abstract
{

    /**
     * Check all active banners with banner start or banner end
     */
    public function checkBannersForPurge()
    {
        $currentTime = Mage::getModel('core/date')->date('Y-m-d H:i:s'); //Current time
        $currentTimestamp = strtotime($currentTime); //Convert to timestamp

        $banners = Mage::getModel('ambanners/rule')->getCollection()
            ->addFieldToFilter('is_active', 1)
            ->addFieldToFilter(
                array('from_date', 'to_date'),
                array(
                    array('lt' => $currentTime),
                    array('lt' => $currentTime)
                )
            );

        if(!$banners) {
            return false;
        }

        foreach($banners as $banner) {

            $bannerStart = strtotime(Mage::getModel('core/date')->date($banner->getFromDate())); //Convert banner start to strtotime
            $bannerEnd = strtotime(Mage::getModel('core/date')->date($banner->getToDate())); //Convert banner end to strtotime

            //If both empty, theres no job to do here
            if(empty($bannerStart) && empty($bannerEnd)) {
                continue;
            }

            if($currentTimestamp > $bannerStart) //If current time is higher than banner start and lower than end, then purge at set fromdate to null
            {
                //Remove from date and save
                $banner->setFromDate(null)->save();
            }
            else if($currentTimestamp > $bannerEnd) //If current time is higher than banner end, then purge at set todate to null
            {
                //Remove to date and set banner to inactive
                $banner->setToDate(null)->setIsActive(0)->save();
            }

            //Purge banner
            $this->purgeBanner($banner->getCats(), $banner->getShowOnProducts(), $banner->getBannerPosition(), $banner->getBrands(), $banner->getStores());

        }
    }

    /**
     * @param $categories
     * @param $products
     * @param $positions
     * @param $brands
     * @param $stores
     */
    private function purgeBanner($categories, $products, $positions, $brands, $stores)
    {
        $categories = explode(",", str_replace(" ", "", $categories)); //Explode categories into array
        $products = explode(",", str_replace(" ", "", $products)); //Explode products into array
        $positions = explode(",", str_replace(" ", "", $positions)); //Explode positions into array
        $brands = explode(",", str_replace(" ", "", $brands)); //Explode brands into array
        $stores = explode(",", str_replace(" ", "", $stores)); //Explode stores into array

        $this->_purgeCategories($categories); //Purge categories
        $this->_purgeProducts($products); //Purge products
        $this->_purgePositions($positions, $stores); //Purge homepage if positions matches
        $this->_purgeBrands($brands, $stores); //Purge brand pages
    }

    /**
     * @param $positions
     * @param $stores
     * @return bool
     *
     * Purge homepage if positions correct
     */
    private function _purgePositions($positions, $stores)
    {
        //Walk through every position and purge if its a homepage position
        foreach($positions as $position)
        {
            if(empty($position)) {
                continue;
            }

            //Check if its a homepage position
            if($position >= 22 || $position <= 27) {
                $this->_purgeHomepage($stores);
            }
        }

        return true;
    }

    /**
     * @param $products
     * @return bool
     *
     * Purge products
     */
    private function _purgeProducts($products)
    {
        //Walk through every product sku and purge it
        foreach($products as $product)
        {
            if(empty($product)) {
                continue;
            }

            //Load product from SKU
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $product);

            //Purge URL
            Mage::getModel('varnishcache/control_catalog_product')->purge($product, true, true);
        }

        return true;
    }

    /**
     * @param $categories
     * @return bool
     *
     * Purge categories
     */
    private function _purgeCategories($categories)
    {
        //Walk through every category and purge it
        foreach($categories as $category)
        {
            if(empty($category)) {
                continue;
            }

            //Load category
            $category = Mage::getModel('catalog/category')->load($category);

            //Purge URL
            Mage::getModel('varnishcache/control_catalog_category')->purge($category);
        }

        return true;
    }

    /**
     * @param $stores
     * @return bool
     *
     * Purge homepage
     */
    private function _purgeHomepage($stores)
    {
        foreach($stores as $storeId)
        {
            if(empty($storeId)) {
                continue;
            }

            $homepageIdentifier = Mage::getStoreConfig('web/default/cms_home_page', $storeId);
            $homepage = Mage::getModel('cms/page')->load($homepageIdentifier, 'identifier');

            Mage::getModel('varnishcache/control_cms_page')->purge($homepage);
        }

        return true;
    }

    /**
     * @param $brands
     * @param $stores
     * @return bool
     *
     * Purge brand pages
     */
    private function _purgeBrands($brands, $stores)
    {
        foreach($stores as $storeId) {

            if (empty($storeId)) {
                continue;
            }

            //Walk through every category and purge it
            foreach ($brands as $brand) {
                if (empty($brand)) {
                    continue;
                }

                //Load brand
                $brand = Mage::getModel('ambrands/brand')->load($brand, 'entity_id');

                //Purge URL
                Mage::getModel('varnishcache/control_cms_brand')->purge($brand, $storeId);
            }
        }

        return true;
    }

}