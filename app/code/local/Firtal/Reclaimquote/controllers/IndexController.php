<?php
class Firtal_Reclaimquote_IndexController extends Mage_Core_Controller_Front_Action
{
    public function resumeAction()
    {
        $this->loadLayout();
        $this->renderLayout();
        $this->resumeCart();
    }

    /**
     * Replace quote with id and redirect to cart page
     */
    private function resumeCart()
    {
        //Get quote ID
        $qId = $this->getRequest()->getParam('qid');

        //Get quote code
        $qCode = $this->getRequest()->getParam('qcode');

        //Wanted qCode
        $qWanted = hash("sha1", hash("sha256", 'xxx:fw' . $qId . 'fw:xxx'));

        //Get UTM params and add to string
        $utmParams = sprintf('?utm_campaign=%s&utm_medium=%s&utm_source=%s',
            urlencode($this->getRequest()->getParam('utm_campaign')),
            urlencode($this->getRequest()->getParam('utm_medium')),
            urlencode($this->getRequest()->getParam('utm_source'))
        );

        //Check if match
        if($qWanted == $qCode)
        {
            //Load quote
            $quote = Mage::getModel('sales/quote')->load($qId);

            //Replace quote with loaded quote
            Mage::getSingleton('checkout/session')->replaceQuote($quote);
        }

        //Redirect to cart
        $this->getResponse()->setRedirect(Mage::getUrl('checkout/cart') . $utmParams);
    }
}