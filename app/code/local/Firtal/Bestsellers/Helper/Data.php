<?php

class Firtal_Bestsellers_Helper_Data extends Mage_Core_Helper_Abstract
{

    protected static $config_ideal_stock_level = null;
    private $_bestsellers_mean;
    /** @var Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Action */
    private $communicator;

    public function __construct()
    {
        $this->_bestsellers_mean = Mage::getStoreConfig( 'bestsellers_options/bestsellers_group/bestsellers_mean' );
        $this->communicator      = new Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Action;
    }

    /**
     * Is module Active on store view
     *
     * @param $store
     * @return bool
     */
    public function isModuleActiveOnStore($store = false)
    {
        return Mage::getStoreConfigFlag('bestsellers_options/bestsellers_group/active', $store);
    }

    public function getAllEnabledProducts($store = null)
    {
        /** @var Mage_Catalog_Model_Resource_Eav_Mysql4_Product_Collection $collection */
        $collection = Mage::getModel('catalog/product')
            ->getCollection();

        if ($this->isModuleActiveOnStore($store)) {
            $collection->addStoreFilter($store)
                        ->setStore($store);
        }

        $storeId = null;

        if(is_numeric($store)){
            $storeId = $store;
        }

        if($store instanceof Mage_Core_Model_Store){
            $storeId = $store->getId();
        }

        $collection->getSelect()->group('e.entity_id');

        $collection->addAttributeToSelect( ['price','special_price', 'cost', 'created_at', 'sku', 'bestseller_factor', 'sort_adjustment'] )
            ->addAttributeToFilter('status', ['eq' => 1]);

        // getting bestseller rating from am_Sorting
        $collection
            ->getSelect()
            ->joinLeft( ['bs_table' => 'am_sorting_bestsellers'], 'e.entity_id = bs_table.id' . ($storeId !== null ? " and bs_table.store_id = " . $storeId : ""), ['bs_bestsellers' => 'bestsellers'] );

        // getting ideal_stock_level (attribute_id 173 = ideal_stock_level attribute)
        $collection
            ->getSelect()
            ->joinLeft( ['ci_table' => 'catalog_product_entity_varchar'], 'e.entity_id = ci_table.entity_id and ci_table.attribute_id = 173', ['ideal_stock_level' => 'value'] );


        return $collection;
    }

    public function calculateAge($createdAt)
    {
        
        $createdAtDate = explode( ' ', $createdAt )[0];

        $createdAt = date_create( $createdAtDate );
        $now       = date_create( date( 'Y-m-d' ) );

        $age = date_diff( $createdAt, $now );

        return $age->format( '%a' );

    }

    public function normalEquation($age_in_days)
    {

//        return $age_in_days;



        /*
         * For a linear, falling curve (New products rank higher, old products rank lower)
         */

        $factor = ((90 - $age_in_days) / 90) + 1;

        $min_factor = 1;

        return $factor < $min_factor ? $min_factor : $factor;



        /*
         * For a bell-formed curve (New products gets ranked low, old products gets ranked low, the ones in between gets ranked higher)
         */
//		$mean = $this->_bestsellers_mean;
//		return abs($mean-$age)/($mean*2)+1;
    }

    private function cap($value, $min, $max){
        if($value < $min){
            return $min;
        }

        if($value > $max){
            return $max;
        }

        return $value;
    }

    public function calculateRating($bsBestsellers, $idealStockLevel, $margin, $availableQty, $ageFactor, $factor = 0, $sellAdjustment = 0)
    {
        $factor = floatval( $factor );

        $bsBestsellers = $this->cap($bsBestsellers + $sellAdjustment, 1, 500);
        $margin = $this->cap($margin, 0, 200);
        $stockTurn = $this->cap($availableQty, 1, 90);

        $rating = (($bsBestsellers/500) * 0.65) + (($margin/200) * 0.2) + (($ageFactor) * 0.1) + (($stockTurn/90) * 0.05) ;


        
        
        return intval( $rating * 10000 );
    }

    public function setRatings($product, $rating)
    {
        $product->setBestseller_rating($rating);
    }

    public function unsetBestsellerRating(Mage_Catalog_Model_Product $product)
    {
        $this->unsetAttributeOnStore($product, array('bestseller_rating'));
    }

    public function save(Mage_Catalog_Model_Product $product)
    {
        $product->addAttributeUpdate('bestseller_rating', $product->getData('bestseller_rating'), $product->getStoreId());
    }

    public function getIdealStockLevel($product)
    {

        $value = $product->getideal_stock_level();

        if ( $value == '' )
        {
            $value = 0;
        }

        return (int)$value;

    }

    private function getConfigIdealStockLevel()
    {

        if ( !is_null( self::$config_ideal_stock_level ) )
        {
            return self::$config_ideal_stock_level;
        }

        return self::$config_ideal_stock_level = (Mage::getStoreConfig( 'advancedstock/prefered_stock_level/ideal_stock_default_value' ) ?: 0);

    }

    /**
     * Attribute Helper to make it possible to unset attribute on specific store
     *
     * @param Mage_Catalog_Model_Product $product
     * @param array $attributeCodes
     */
    protected function unsetAttributeOnStore(Mage_Catalog_Model_Product $product, $attributeCodes = array())
    {
        $storeId = $product->getStoreId();

        if($storeId == Mage_Core_Model_App::ADMIN_STORE_ID){
            $product->setBestsellerRating("");
            return $this->save($product);
        }

        if (!$attributeCodes) {
            return;
        }

        $attributeCodesToReset = $attributeCodes;

        $model = Mage::getModel('catalog/product');
        $resource = $model->getResource();
        $entityTypeId = $resource->getEntityType()->getEntityTypeId();
        $adapter = $resource->getWriteConnection();

        $productId = $product->getId();

        foreach ($attributeCodesToReset as $attributeCode) {
            $attribute = $resource->getAttribute($attributeCode);
            $condition = array(
                'entity_id IN(?)' => $productId,
                'attribute_id = ?' => $attribute->getId(),
                'store_id = ?' => $storeId,
                'entity_type_id = ?' => $entityTypeId
            );
            $adapter->delete($attribute->getBackendTable(), $condition);
        }
    }
}