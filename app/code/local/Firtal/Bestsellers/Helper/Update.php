<?php

class Firtal_Bestsellers_Helper_Update extends Mage_Core_Helper_Abstract
{
    /**
     * @throws Mage_Core_Model_Store_Exception
     */
    public function updateRatings()
    {
        $globalstart = microtime(true);

        $this->_log("Observer/helper started!");

        $store = Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        /** @var Firtal_Bestsellers_Helper_Data $helper */
        $helper = Mage::helper('bestsellers');
        $storeName = Mage::app()->getStore()->getName();

        foreach (Mage::app()->getStores(true) as $store) {

            $products = $helper->getAllEnabledProducts($store);

            $isModuleActiveOnStore = $helper->isModuleActiveOnStore($store);

            foreach ($products as $product) {
                $product->setStoreId($store->getId());

                if (!$isModuleActiveOnStore) {
                    Mage::log(sprintf("unsetting bestseller rating on store %s for product %s", $store->getName(), $product->getSku()), null, 'test.log', true);
                    $helper->unsetBestsellerRating($product);
                    continue;
                }

                $this->_setBestsellersOnProducts($product, $helper);
            }
        }

        $this->_log(sprintf("Treatment ended in %s seconds",
            number_format(microtime(true) - $globalstart, 2, ",", ".")));
    }

    private function _log($message)
    {
        if (!Mage::getStoreConfigFlag('bestsellers_options/bestsellers_group/logging')) {
            return;
        }

        Mage::log($message, null, 'Bestseller.log', true);
    }

    /**
     * @param $product
     * @param $helper
     */
    protected function _setBestsellersOnProducts($product, $helper)
    {
        $start = microtime(true);
        $id = $product->getData('entity_id');
        $idealStockLevel = $helper->getIdealStockLevel($product);
        switch ($idealStockLevel) {
            case 0:
            case "0":
            case null:
                $idealStockLevel = 1;
                break;
        }
        $bsBestsellers = $product->getData('bs_bestsellers');
        if ($product->getData('special_price') !== null) {
            $price = $product->getData('special_price');
        } else {
            $price = $product->getData('price');
        }
        $name = $product->getData('name');

        if($product->getTypeId() == 'bundle' || $product->getTypeId() == 'brandbundle'){
            $cost = $price*0.7;
        } else {
            $cost = $product->getData('cost');
        };
        if ($cost == null) {
            $cost = $price*0.7;
        }
        $margin = ($price - $cost) * $idealStockLevel;
        $sellAdjustment = $product->getData('sort_adjustment');

        //available qty vs ideal_stock_level
        $availableQty = $product->getData('available_qty') / $idealStockLevel;

        $age = $helper->calculateAge($product->getData('created_at'));

        $ageNormal = $helper->normalEquation($age);

        $factor = $product->getData('bestseller_factor');

        if ($factor === null || $factor === "") {
            $factor = 1;
        }
        $rating = $helper->calculateRating($bsBestsellers, $idealStockLevel, $margin, $availableQty, $ageNormal, $factor, $sellAdjustment);
        // echo 'Price';
        // echo $price;
        // echo 'idealStockLevel<br>';
        // echo $idealStockLevel;
        // echo 'margin<br>';
        // echo $margin;
        // echo 'availableQty<br>';
        // echo $availableQty;
        // echo 'ageNormal<br>';
        // echo $ageNormal;
        // echo 'factor<br>';
        // echo $factor;
        // echo 'sortAdjustment<br>';
        // echo $sortAdjustment;
        // echo 'id<br>';
        // echo $id;
        // echo 'rating<br>';
        // echo $rating;
        // die;
        $helper->setRatings($product, $rating);

        $duration = microtime(true) - $start;

        Mage::log(sprintf("idealStockLevel: %s | margin %s | sku %s | stockturns %s | agenormal %s | cost %s | name %s", $idealStockLevel,$margin, $product->getSku(), $availableQty, $ageNormal, $cost, $name), null, 'bestsellertracking.log', true);

        $this->_log(sprintf("Product %s ended in %s seconds", $product->getSku(),
            number_format($duration, 6, ",", ".")));
        $start = microtime(true);
        $helper->save($product);

        $duration = microtime(true) - $start;
        $this->_log(sprintf("Product %s saved in %s seconds. Info: margin: %s | idealStockLevel %s | stockturns %s | $agenormal %s | cost %s | $name", $product->getSku(),
            number_format($duration, 6, ",", "."), $margin, $idealStockLevel, $availableQty, $ageNormal, $cost, $name));
    }
}