<?php

class Firtal_Bestsellers_Model_Observer
{
    /**
     * Run the cronjob as a helper method
     * to make php script work
     */
    public function setBestsellerRatings()
    {
        Mage::helper('bestsellers/update')->updateRatings();
    }
}