<?php

$app = Mage::app();

/** @var Mage_Core_Model_Config $setup */
$setup = new Mage_Core_Model_Config();
$path = 'bestsellers_options/bestsellers_group/active';

$setup->saveConfig($path, true, 'default', 0);

foreach ($app->getWebsites() as $website) {
    $configData = Mage::getModel('core/config_data');
    $configData->setValue(0);
    $configData->setPath($path);
    $configData->setScope('websites');
    $configData->setScopeId($website->getId());
    $configData->save();
}
