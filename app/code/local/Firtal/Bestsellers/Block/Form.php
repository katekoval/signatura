<?php

class Firtal_Bestsellers_Block_Form extends Mage_Adminhtml_Block_System_Config_Form_Field
{
    protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element)
    {
        $this->setElement($element);
        $url = $this->helper('adminhtml')->getUrl('adminhtml/rating/update');

        $html = $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setType('button')
            ->setClass('scalable')
            ->setLabel('Update ratings')
            ->setOnClick("setLocation('$url')")
            ->toHtml();

        return $html;
    }
}
