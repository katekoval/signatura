<?php

class Firtal_Bestsellers_Adminhtml_RatingController extends Mage_Adminhtml_Controller_Action
{
    public function updateAction()
    {
        if (!Mage::helper('core')->isModuleEnabled('Firtal_Bestsellers')
            || !Mage::getSingleton('admin/session')->isAllowed('bestsellers_options')) {
            return;
        }

        try {
            Mage::helper('bestsellers/update')->updateRatings();
        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
        }

        Mage::getSingleton('adminhtml/session')->addNotice(Mage::helper('bestsellers')->__('Check logs in Kibana to see if it ran or not.'));
        return $this->_redirectReferer();
    }
}