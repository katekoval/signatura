<?php
/**
 * PageCache powered by Varnish
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to support@phoenix-media.eu so we can send you a copy immediately.
 *
 * @category   Phoenix
 * @package    Firtal_Shellac
 * @copyright  Copyright (c) 2011-2015 PHOENIX MEDIA GmbH (http://www.phoenix-media.eu)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Firtal_Shellac_Model_Control_Cms_Brand
    extends Firtal_Shellac_Model_Control_Abstract
{
    protected $_helperName = 'varnishcache/control_cms_page';

    public function purge($brand, $storeId)
    {
        //Get brand URL
        $url = Mage::app()->getStore($storeId)
            ->getUrl(null, array('_direct' => $brand->getUrlKey()));

        //Get brands url (/brands/, /m/ etc.)
        $brandsUrlKey = trim(Mage::getStoreConfig('amshopby/brands/url_key'));

        //Extract url and trim
        extract(parse_url($url));
        $path = rtrim('/' . $brandsUrlKey . $path, '/');

        //Clean varnish path
        $this->_getCacheControl()->clean($host, '^' . $path . '/{0,1}$');
    }
}
