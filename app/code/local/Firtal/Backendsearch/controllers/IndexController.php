<?php

class Firtal_Backendsearch_Indexcontroller extends Mage_Core_Controller_Front_Action {

   public function orderAction()
   {
        $order_id = Mage::app()->getRequest()->getQuery('order_id');

        if(!preg_match('/[0-9]{4,10}/',$order_id )){
            return $this->respond(false);
        }

        $orders = Mage::getModel("sales/order")->getCollection()
                                               ->addFieldToFilter(
                                                 "increment_id",
                                                 array('like' => '%'.$order_id)
                                               )
                                               ->addFieldToSelect(array("entity_id", "increment_id"));

        $orders->getSelect()->limit(5);

        if($orders->count() === 0){
          return $this->respond(false);
        }

        $return = array();

        foreach($orders as $order){
          $return[] = array(
            "url"         => Mage::helper('adminhtml')->getUrl("adminhtml/sales_order/view", array('order_id'=>$order->getId())),
            "incrementId" => $order->getIncrementId()
          );
        }

       return $this->respond($return);

   }

   protected function respond($data){
     $this->getResponse()->setHeader("Content-type", "application/json")
                         ->setBody(json_encode($data));
   }

}
