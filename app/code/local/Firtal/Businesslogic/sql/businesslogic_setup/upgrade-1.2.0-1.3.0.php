<?php
try {

    /** @var Mage_Catalog_Model_Resource_Eav_Mysql4_Setup $installer */
    $installer = $this;
    $installer->startSetup();

    $attrName = 'regular_price';

    $salesFlatOrderItemAttrExistsResult = $installer->getConnection('core_read')->fetchAll("
        SHOW COLUMNS 
        FROM `sales_flat_order_item`
        WHERE FIELD='" . $attrName . "'
        ");

    if (count($salesFlatOrderItemAttrExistsResult) === 0) {
        $installer->run("
            ALTER TABLE `sales_flat_order_item` 
            ADD COLUMN " . $attrName . " DECIMAL(12,4) DEFAULT NULL;
        ");
    }

    $installer->endSetup();

} catch (Exception $e) {
    Mage::logException($e);
}