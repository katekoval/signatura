<?php

class Firtal_Businesslogic_Helper_Logger extends Firtal_Businesslogic_Helper_Data
{

    public function logMessage($message)
    {
        Mage::log($message, null, 'firtal_businesslogic.log', true);
    }

    /**
     * Force logs stuff.
     *
     * @param $data
     */
    public function log($data)
    {
        Mage::log( json_encode( func_get_args() ), null, 'firtal_businesslogic.log', true );
    }
}