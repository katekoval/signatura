<?php

class Firtal_Businesslogic_Helper_Config extends Firtal_Businesslogic_Helper_Data
{
    const CONFIG_NAMESPACE = "firtal_businesslogic_options";
    const NO_CUSTOMER_GROUP = "NONE";


    /**
     * Get a config value relative to the config namespace.
     *
     * @param string                            $path
     * @param Mage_Core_Model_Store|string|null $store
     *
     * @return mixed
     */
    public function get($path, $store = null)
    {
        return Mage::getStoreConfig(self::CONFIG_NAMESPACE . "/" . $path, $store);
    }

    /**
     * Gets the customer group ID for employees.
     *
     * @return mixed
     */
    public function getEmployeeCustomerGroup()
    {
        $groupId = $this->get('general/employee_customer_group');

        if($groupId === static::NO_CUSTOMER_GROUP)
        {
            return null;
        }

        return $groupId;
    }

    /**
     * Gets the attribute code to limit on.
     *
     * @return string|null
     */
    public function getProductLimitAttributeCode()
    {
        return $this->get('product_limits/attribute_code') ?: null;
    }

    /**
     * Gets the limit of products across multiple attributes.
     *
     * @return string|null
     */
    public function getProductCrossAttributeLimit()
    {
        return $this->get('product_limits/cross_attribute_limit') ?: null;
    }
}