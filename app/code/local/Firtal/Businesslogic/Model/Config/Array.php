<?php

class Firtal_Businesslogic_Model_Config_Array extends Mage_Core_Model_Config_Data
{
    protected function _afterLoad()
    {
        if (is_array($this->getValue())) {
            return;
        }

        $serializedValue   = $this->getValue();
        $unserializedValue = false;

        if($serializedValue instanceof Mage_Core_Model_Config_Element) {
            $serializedValue = $serializedValue->asArray();
        }

        if (!empty($serializedValue)) {
            try {
                $unserializedValue = json_decode($serializedValue, true);
            } catch (Exception $e) {
                Mage::logException($e);
            }
        }

        $this->setValue($unserializedValue);
    }

    protected function _beforeSave()
    {
        if (is_array($value = $this->getValue())) {
            $this->setValue(json_encode($value));
        }
    }
}