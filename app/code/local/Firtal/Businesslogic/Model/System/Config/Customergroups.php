<?php

class Firtal_Businesslogic_Model_System_Config_Customergroups
{
    protected $_options;

    public function toOptionArray()
    {
        if (!$this->_options) {
            $options        = Mage::getResourceModel('customer/group_collection')
                                  ->loadData()->toOptionArray();

            array_unshift($options, [
                "label" => " -- none -- ",
                "value" => Firtal_Businesslogic_Helper_Config::NO_CUSTOMER_GROUP
            ]);

            $this->_options = $options;
        }
        return $this->_options;
    }
}