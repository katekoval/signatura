<?php

class Firtal_Businesslogic_Model_System_Config_Product_Attributes
{
    public function toOptionArray()
    {
        $options = array(
            array(
                'value' => null,
                'label' => ' -- Do not enable product limits --'
            )
        );

        $collection = Mage::getResourceModel('eav/entity_attribute_collection')
            ->setItemObjectClass('catalog/resource_eav_attribute')
            ->setEntityTypeFilter(Mage::getResourceModel('catalog/product')->getTypeId());

        foreach ($collection as $attribute) {
            $label = $attribute->getFrontendLabel();
            if ($label) { // skip system and `exclude` attributes
                $options[] = array(
                    'value' => $attribute->getAttributeCode(),
                    'label' => $label
                );
            }
        }

        return $options;
    }
}