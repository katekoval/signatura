<?php

class Firtal_Businesslogic_Model_Observer_Cart
{

    /** @var  Firtal_Businesslogic_Helper_Config */
    protected $_config;

    /** @var  Firtal_Businesslogic_Helper_Data */
    protected $_helper;

    public function __construct()
    {
        $this->_config = Mage::helper('firtal_businesslogic/config');
        $this->_helper = Mage::helper('firtal_businesslogic');
    }

    /**
     *
     * @param $observer
     * @return $this|void
     * @throws Mage_Core_Exception
     */
    public function item_qty_set_after($observer)
    {

        /** @var Mage_Sales_Model_Quote_Item $quoteItem */
        $quoteItem = $observer->getEvent()->getItem();

        if (!$quoteItem || !$quoteItem->getProductId() || !$quoteItem->getQuote()
            || $quoteItem->getQuote()->getIsSuperMode()) {
            return $this;
        }

        if(!$attributeCode = $this->_config->getProductLimitAttributeCode()){
            return;
        }

        $limit = $this->_config->getProductCrossAttributeLimit();


        /** @var Mage_Catalog_Model_Product $product */
        $product = $quoteItem->getProduct();

        try{
            if(!$attributeValue = $this->getAttributeValue($product, $attributeCode)){
                return;
            }
        }catch (\Exception $e){
            return;
        }

        $current = 0;

        /** @var Mage_Sales_Model_Quote_Item $item */
        foreach($quoteItem->getQuote()->getAllItems() as $item){
            if($this->getAttributeValue($item->getProductId(), $attributeCode) !== $attributeValue){
                continue;
            }

            $current += $item->getQty();

            if($current > $limit){
                Mage::throwException(
                    $this->_helper->__("It's not possible to add more than %d of this type of product.", $limit)
                );
            }
        }
    }


    /**
         * @param int|Mage_Catalog_Model_Product $productId
         * @param string $attributeCode
         *
         * @return mixed
         */
        protected function getAttributeValue($productId, $attributeCode)
        {
            if($productId instanceof Mage_Catalog_Model_Product){
                $productId = $productId->getId();
            }

            return Mage::getResourceModel('catalog/product')->getAttributeRawValue($productId, $attributeCode, Mage::app()->getStore()->getId());
        }
    }