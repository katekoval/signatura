<?php

class Firtal_Businesslogic_Model_Observer_Order
{
    /**
     * @var  Firtal_Businesslogic_Helper_Config
     */
    protected $configHelper;

    /**
     * @var Firtal_Businesslogic_Helper_Logger
     */
    protected $logger;

    /**
     * Firtal_Businesslogic_Model_Observer_Order constructor.
     */
    public function __construct()
    {
        $this->configHelper = Mage::helper('firtal_businesslogic/config');
        $this->logger       = Mage::helper('firtal_businesslogic/logger');
    }

    /**
     * After order placement.
     *
     * @event sales_order_place_after
     *
     * @param $event
     */
    public function place_after($event)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $event->getOrder();

        $this->setRegularPrice($order);
        $this->createEmployeeOrganizer($order);
    }

    /**
     * Set regular/original price (not special price)
     * on sales_flat_order_items
     *
     * @param $order
     */
    private function setRegularPrice($order)
    {
        $resource = Mage::getSingleton('core/resource'); // Get the resource model
        $writeConnection = $resource->getConnection('core_write'); // Retrieve the write connection
        $items = $order->getAllItems(); // Get all visible items
        $itemListProductIds = array(); // Define array for storing product ids
        $prices = array();
        $reasons = array();

        // Make lists with useful ids
        foreach($items as $item) {
            $itemListProductIds[] = $item->getProductId();

            $products[$item->getItemId()] = array(
                'item_id' => $item->getItemId(),
                'product_id' => $item->getProductId(),
                'product_type' => $item->getProductType(),
            );

            //If has parent, then add id as child
            if(!empty($item->getParentItemId()))
            {
                if(!empty($products[$item->getParentItemId()]['children']))
                {
                    $products[$item->getParentItemId()]['children'] .= ',';
                }

                $products[$item->getParentItemId()]['children'] .= $item->getProductId();
            }
        }

        // Get all product prices
        $productsCollection = Mage::getModel('catalog/product')
            ->getCollection()
            ->addAttributeToSelect('price')
            ->addAttributeToSelect('special_price_reason')
            ->addAttributeToFilter('entity_id', array('in' => $itemListProductIds));

        // Make array with prices and reasons
        foreach($productsCollection as $product)
        {
            $prices[$product->getEntityId()] = $product['price'];
            $reasons[$product->getEntityId()] = $product['special_price_reason'];
        }

        // Lets make some changes to the DB
        foreach($products as $product)
        {
            $price = $prices[$product['product_id']];
            $special_price_reason = $reasons[$product['product_id']];

            // If brand bundle found and item has children, calculate regular price
            if($product['product_type'] == "brandbundle" && isset($product['children']))
            {
                $price = 0;
                $children = explode(',', $product['children']);

                foreach($children as $child)
                {
                    $price += $prices[$child];
                }
            }

            // Get table name (sales_flat_order_item)
            $tableName = $resource->getTableName('sales/order_item');

            // Define query
            $query = "UPDATE {$tableName} SET regular_price = '{$price}', special_price_reason = '{$special_price_reason}' WHERE item_id = '{$product['item_id']}'";

            // Execute query
            $writeConnection->query($query);
        }
    }

    /**
     * Automatically create an organizer task if the order was placed
     * by a member of the Employee customer group.
     *
     * @param Mage_Sales_Model_Order $order
     */
    private function createEmployeeOrganizer($order)
    {
        $customerGroupId = $this->configHelper->getEmployeeCustomerGroup();

        if (!$customerGroupId || $order->getCustomerGroupId() != $customerGroupId) {
            return;
        }

        // ========================================================
        // This part has ruthlessly been taken from its mother, Ms.
        // "TaskController", as she never made a helper to create a
        // task. Yes. She's a bad mother. Darn french.
        // ========================================================
        try {
            $this->logger->log("Order #{$order->getIncrementId()} is an employee order.");

            $organizerTask = Mage::getModel('Organizer/Task');

            if(!$organizerTask)
            {
                $this->logger->log("Model Organizer/Task not found.");
                return;
            }

            $organizerTask
                ->setData([
                    'ot_author_user'        => Mage::getModel('admin/user')->getCollection()->getFirstItem()->getId(),
                    'ot_target_user'        => '0',
                    'ot_caption'            => '(AUTO) EMPLOYEE ORDER',
                    'ot_description'        => 'This is an employee order. Pick and scan products as usual, but do not pack and ship! Place the order under the table at the main computer. The employee will pick it up later.',
                    'ot_entity_id'          => $order->getId(),
                    'ot_entity_type'        => 'order',
                    'ot_entity_description' => 'Order #' . $order->getIncrementId(),
                    'ot_created_at'         => date('Y-m-d H:i')
                ])
                ->save();
            $this->logger->log("Order #{$order->getIncrementId()}: organizer task added.");

        } catch (Exception $e) {
            $this->logger->log("Creating an organizer task failed. Sry. Message: {$e->getMessage()}");
        }
    }
}