<?php

class Firtal_Businesslogic_Model_Observer_Clerk
{
    public function clerk_get_export_data(Varien_Event_Observer $observer)
    {
        $product = $observer->getData('product');
        $clerkData = $observer->getData('data');
        
        $clerkData->setProductType($product->getTypeId());
    }
}