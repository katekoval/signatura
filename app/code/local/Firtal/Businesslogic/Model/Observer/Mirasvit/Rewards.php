<?php

class Firtal_Businesslogic_Model_Observer_Mirasvit_Rewards
{
    public function sendPointsToKlaviyo($event)
    {
        $obj = $event->getObject();

        if (!$obj instanceof Mirasvit_Rewards_Model_Transaction) {
            return;
        }

        $original = intval($obj->getOrigData('amount'));
        $amount = intval($obj->getData('amount'));

        if($original === $amount){
            return;
        }

        if (!$customer = $obj->getCustomer()) {
            return;
        }

        $klaviyo = Mage::getModel('klaviyo_reclaim/api');

        try {
            $balanceInPoints = Mage::helper("rewards/balance")->getBalancePoints($customer);
            $balance = round($balanceInPoints/20,2);
          //  $balance = str_replace('.', ',',$balanceWithDecimals);

            Mage::log(sprintf("Sending point balance [%s] for customer with ID [%s]", $balance, $customer->getId()), null, 'klaviyo-rewards.log', true);

            $res = $this->updateKlaviyoProfile($customer, $balance);

            Mage::log(sprintf("Sent point balance [%s] for customer with ID [%s]. Response from Klaviyo API: %s", $balance, $customer->getId(), $res), null, 'klaviyo-rewards.log', true);
        } catch (\Throwable $e) {
            Mage::log(sprintf("Unable to send the balance for customer # %s : ", $customer->getId(), $e->getMessage()), null, 'klaviyo-rewards.log', true);
        }
    }

    protected function updateKlaviyoProfile(Mage_Customer_Model_Customer $customer, float $balance)
    {
        $client = new Zend_Http_Client('https://a.klaviyo.com/api/track');

        $payload = [
            'customer_properties' => [
                '$email' => $customer->getEmail(),
                '$reward_balance' => $balance,
            ],
            'event' => 'Rewardpoint balance updated',
            'token' => $this->getKlaviyoApiKey()
        ];

        $maskedPayload = $payload;
        $maskedPayload['customer_properties']['$email'] = "REDACTED (cust id: ". $customer->getId() .")" ;
        $maskedPayload['token'] = "REDACTED";

        Mage::log(sprintf("Sending payload to Klaviyo: \n %s ", json_encode($maskedPayload, JSON_PRETTY_PRINT)), null, 'klaviyo-rewards.log', true);

        return $client->setMethod('GET')
                      ->setParameterGet('data', base64_encode(json_encode($payload)))->request()->getBody();
    }

    protected function getKlaviyoApiKey()
    {
        $store_name = Mage::app()->getRequest()->getParam('store');

        if (strlen($store_name)) {
            $store_id = Mage::getModel('core/store')->load($store_name)->getId();

            return Mage::getStoreConfig('reclaim/general/public_api_key', $store_id);
        }
         
        return Mage::helper('klaviyo_reclaim')->getPublicApiKey();
    }
}
