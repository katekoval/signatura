<?php

class Firtal_Businesslogic_Model_Observer_Selveo_Order_Address
{
    public function addToKeyMap(Varien_Event_Observer $observer)
    {
        /** @var Varien_Object $keyMap */
        $keyMap = $observer->getEvent()->getKeyMap();

        // We're using the fax field for a droppoint identifier sagga
        $keyMap->setData('fax', 'identifier');
    }
}
