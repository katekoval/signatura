<?php

class Firtal_Businesslogic_Model_Observer_Selveo_Order
{

    public function mapToSelveo(Varien_Event_Observer $event)
    {
        $attributes = $event->getMapped()->getAttributes();
        $customerGroupId = $event->getModel()->getCustomerGroupId();

        if($customerGroupId == 4)  // 4 = Employee group
        {

            $attributes['annotations'][] = [
                'bu_id' => 'employee-order',
                'internal' => false,
                'message' => 'Order is an employee order'
            ];

            $event->getMapped()->setAttributes($attributes);

        }
    }
}