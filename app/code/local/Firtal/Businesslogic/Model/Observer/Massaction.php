<?php

class Firtal_Businesslogic_Model_Observer_Massaction
{

    protected $isAllowed;
    /** @var Firtal_Businesslogic_Helper_Logger */
    protected $logHelper;

    public function __construct()
    {
        $this->isAllowed = Mage::getSingleton('admin/session')->isAllowed('sales/order/actions/mass_cancel');
        $this->logHelper = Mage::helper('firtal_businesslogic/logger');
    }

    public function controller_cancel_predispatch()
    {
        if($this->isAllowed) {
            return;
        }

        Mage::app()->getFrontController()->getAction()->setFlag(
            Mage::app()->getRequest()->getActionName(),
            Mage_Core_Controller_Varien_Action::FLAG_NO_DISPATCH, true
        );

        $redirectBackUrl = Mage::helper('adminhtml')->getUrl('adminhtml/sales_order');
        Mage::app()->getResponse()->setRedirect($redirectBackUrl);

        $this->logHelper->logMessage('User is not allowed to mass cancel orders. User is redirected back');

    }

    public function can_cancel($event)
    {
        $block = $event->getBlock();

        /** @var Mage_Adminhtml_Block_Widget_Grid_Massaction $block */
        if(!($block instanceof Mage_Adminhtml_Block_Widget_Grid_Massaction)) {
            return;
        }

        if ($this->isAllowed) {
            return;
        }
        $block->removeItem('cancel_order');

    }

}