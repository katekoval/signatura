<?php

class Firtal_Businesslogic_Model_Observer_Tierprices
{

    public function logTierEventAfter(Varien_Event_Observer $observer)
    {
        $item = $observer->getEvent()->getProduct();
        $tierPrice =Mage::getModel('catalog/product')->load($item->getId())->getTierPrice();

        Mage::log('Product save after with id ' . $item->getId() . print_r($tierPrice, true), null, 'tier_prices_debug.log', true);
        Mage::log('Debug trace for product with id ' . $item->getId() . ':' . Mage::printDebugBacktrace(), null, 'tier_prices_debug2.log', true);
    }

    public function logTierEventBefore(Varien_Event_Observer $observer)
    {
        $item = $observer->getEvent()->getProduct();
        $tierPrice =Mage::getModel('catalog/product')->load($item->getId())->getTierPrice();

        Mage::log('Product save before with id ' . $item->getId() . print_r($tierPrice, true), null, 'tier_prices_debug.log', true);
    }

}