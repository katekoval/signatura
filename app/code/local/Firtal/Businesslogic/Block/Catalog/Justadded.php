<?php
/**
 * Avalanche for Magento 1.6+
 * Designed by Fast Division (http://fastdivision.com)
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the EULA
 * that is bundled with this package in the file license.txt.
 * It is also available through the world-wide-web at this URL:
 * http://fastdivision.com/legal/license.txt
 *
 * @author     Fast Division
 * @version    1.3.2
 * @copyright  Copyright 2012 Fast Division
 * @license    http://fastdivision.com/legal/license.txt
 */

class Firtal_Businesslogic_Block_Catalog_Justadded extends Mage_Catalog_Block_Product_List
{
    protected function _getProductCollection()
    {
        $date = new DateTime();
        $daysCount = $this->getDaysOld();

        if (!is_numeric($daysCount)) {
            $daysCount = 30; // deafault value
        }

        $dateDiff = (new DateTime)->sub(new DateInterval("P{$daysCount}D"));

        $this->_productCollection = Mage::getModel("catalog/product")
            ->getCollection()
            ->addAttributeToSelect("*")
            ->addAttributeToFilter('created_at', array(
                'from' => date_format($dateDiff, 'Y-m-d H:i:s'),
                'to' => date_format($date, 'Y-m-d H:i:s')
            ))
            ->addAttributeToSort('created_at', 'desc');
        $this->_productCollection->setOrder('created_at');
        $this->_productCollection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());
        $this->_productCollection->getSelect()->limit($this->getData('display_count') ?: 20);

        if ($sorter = Mage::getModel('amsorting/method_instock')) {
            $sorter->apply($this->_productCollection, null);
        }

        return $this->_productCollection;
    }

    public function getToolbarHtml()
    {
        return '';
    }
}