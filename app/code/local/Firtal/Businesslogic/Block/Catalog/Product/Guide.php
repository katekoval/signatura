<?php

class Firtal_Businesslogic_Block_Catalog_Product_Guide extends Mage_Catalog_Block_Product
{
    public function parse($profile, $actions)
    {

        $apply = [];

        foreach ($actions as $action) {
            $combination = $action['combination'];
            unset($action['combination']);

            if (!$this->profileMatches($profile, $combination)) {
                continue;
            }

            if (!isset($action['actions'])) {
                continue;
                throw new Exception("Combination does not provide any actions: " . json_encode($combination));
            }

            $apply = array_merge($apply, $action['actions']);
        }

        $topPriority = ['@prioritize_products', '@prioritize_categories'];

        usort($apply, function ($a, $b) use ($topPriority) {
            $aAction = array_keys($a)[0];
            $bAction = array_keys($b)[0];

            if (in_array($aAction, $topPriority) xor in_array($bAction, $topPriority)) {
                return in_array($aAction, $topPriority) ? 1 : -1;
            }


            $a = $a[$aAction];
            $b = $b[$bAction];

            $aP = isset($a['priority']) ? $a['priority'] : 0;
            $bP = isset($b['priority']) ? $b['priority'] : 0;

            if ($aP === $bP) {
                return 0;
            }

            return $aP > $bP ? 1 : 0;
        });

        $result = [];

        foreach ($apply as $action) {
            if(count($action) > 1){
                Mage::throwException('You can only specify one action per array, i.e. "actions" => [[@action_name=>config], [@action_name_2=>config], [@action_name_3=>config]]. You specified '. json_encode($action));
            }
            $actionName = array_keys($action)[0];
            $config     = $action[$actionName];

            if (!method_exists($this, $method = 'action_' . substr($actionName, 1))) {
                Mage::throwException("Unknown action {$actionName}");
            }

            $result = call_user_func_array([$this, $method], [$result, $config]);
        }

        return $this->prepareResult($result);
    }

    protected function profileMatches(array $profile, array $combination)
    {
        foreach ($combination as $key => $val) {
            if (!isset($profile[$key])) {
                return false;
            }
            if (!stristr($profile[$key], $val)) {
                return false;
            }
        }

        return true;
    }

    protected function action_remove_categories(array $result, array $config)
    {
        foreach($config['categories'] as $cat){
            unset($result['products_by_category'][$cat]);
        }

        return $result;
    }

    protected function action_replace_products_in_category(array $result, array $config)
    {
        $category          = $config['category'];
        $productIds        = $config['product_ids'];
        $description       = isset($config['description']) ? $config['description'] : null;

        $result['products_by_category'][$category] = $productIds;

        if($description){
            foreach($productIds as $productId){
                $result = $this->action_set_product_description($result, ['product_id' => $productId, 'description' => $description]);
            }
        }

        return $result;
    }

    protected function action_set_product_description(array $result, array $config){
        $productId = $config['product_id'];
        $description = $config['description'];

        $result['product_descriptions'][$productId] = $description;

        return $result;
    }

    protected function action_prioritize_categories(array $result, array $config)
    {
        $priorityMap = $config['priority_map'];

        uksort($result['products_by_category'], function($a, $b) use ($priorityMap){

            $aP = isset($priorityMap[$a]) ? $priorityMap[$a] : 0;
            $bP = isset($priorityMap[$b]) ? $priorityMap[$b] : 0;

            if($aP === $bP){
                return 0;
            }

            return $bP - $aP;
        });

        return $result;
    }

    protected function action_add_products_to_category(array $result, array $config)
    {
        $cat        = $config['category'];
        $productIds = $config['product_ids'];

        if (!isset($result['products_by_category'][$cat])) {
            $result['products_by_category'][$cat] = [];
        }
        $result[$cat] = array_unique(array_merge($result['products_by_category'][$cat], $productIds));

        return $result;
    }

    protected function action_prioritize_products(array $result, array $config)
    {
        $map = $config['priority_map'];

        foreach ($result['products_by_category'] as $category => $productIds) {
            usort($productIds, function ($a, $b) use ($map) {
                $aP = isset($map[$a]) ? $map[$a] : 0;
                $bP = isset($map[$b]) ? $map[$b] : 0;

                if ($aP === $bP) {
                    return 0;
                }

                return $aP > $bP ? -1 : 1;
            });

            $result['products_by_category'][$category] = $productIds;
        }

        return $result;

    }

    private function prepareResult(array $result)
    {
        $allProductIds = [];
        foreach($result['products_by_category'] as $category => $product_ids){
            $allProductIds = array_merge($allProductIds, $product_ids);
        }

        $result['all_product_ids'] = array_filter(array_unique($allProductIds));

        return $result;
    }
}