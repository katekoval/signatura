<?php

interface Firtal_Reepay_Model_Repository_SubscriptionRepositoryInterface
{
    /**
     * Creates a subscription
     *
     * @param Mage_Customer_Model_Customer $customer
     * @param Firtal_Reepay_Model_Interface_SubscriptionInterface $subscriptions
     * @return mixed
     */
    public function create(
        Mage_Customer_Model_Customer $customer,
        Firtal_Reepay_Model_Interface_SubscriptionInterface $subscriptions
    );

    /**
     * Cancels a subscription
     *
     * @param $id
     * @return bool
     */
    public function cancel($id): bool;

    /**
     * Updates the subscriptions
     *
     * @param $id
     * @return int
     */
    public function update($id): ?int;

    /**
     * Gets the periods of a specific subscription
     *
     * @return Firtal_Reepay_Model_Interface_PeriodInterface[]
     */
    public function getAvailablePeriods(): array;

    /**
     * Adds product to a subscription
     *
     * @param $id
     * @param Firtal_Reepay_Model_DTO_ProductSubscription $product
     * @return mixed
     */
    public function add($id, Firtal_Reepay_Model_DTO_ProductSubscription $product): void;

    /**
     * Removes a product from a subscription
     *
     * @param $id
     * @param Firtal_Reepay_Model_DTO_ProductSubscription $subscription
     * @return mixed
     */
    public function remove($id, Firtal_Reepay_Model_DTO_ProductSubscription $subscription);

    /**
     * Get subscriptions from a customer
     *
     * @param Mage_Customer_Model_Customer $customer
     * @return Firtal_Reepay_Model_DTO_Subscription|array
     */
    public function getSubscriptions(Mage_Customer_Model_Customer $customer): array;

    /**
     * Gets a specific subscription from a customer
     *
     * @param Mage_Customer_Model_Customer $customer
     * @return Firtal_Reepay_Model_DTO_Subscription
     */
    public function getSubscription(Mage_Customer_Model_Customer $customer): Firtal_Reepay_Model_DTO_Subscription;
}