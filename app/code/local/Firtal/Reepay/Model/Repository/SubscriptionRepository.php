<?php

class Firtal_Reepay_Model_Repository_SubscriptionRepository implements Firtal_Reepay_Model_Repository_SubscriptionRepositoryInterface
{
    const TIMING_ON_SUBSCRIPTION_CHANGE = 'immediate';
    const PARTIAL_PERIOD_HANDLING = 'no_bill';
    const BILLING_ON_SUBSCRIPTION_CHANGE = 'none';
    const COMPENSATION_METHOD_ON_SUBSCRIPTION_CHANGE = 'none';

    /**
     * Creates a subscription
     *
     * @param Mage_Customer_Model_Customer $customer
     * @param Firtal_Reepay_Model_Interface_SubscriptionInterface $subscriptions
     * @return mixed
     */
    public function create(
        Mage_Customer_Model_Customer $customer,
        Firtal_Reepay_Model_Interface_SubscriptionInterface $subscriptions
    ) {
        //
    }

    /**
     * Cancels a subscription
     *
     * @param $id
     * @return bool
     * @throws Mage_Core_Exception
     */
    public function cancel($id): bool
    {
        return Mage::helper('firtal_reepay/subscription')->cancelSubscription($id);
    }

    /**
     * Updates the subscriptions
     *
     * @param $id
     * @return int
     */
    public function update($id): ?int
    {
        return Mage::helper('firtal_reepay/subscription')->reactivate($id);
    }

    /**
     * Gets the periods of a specific subscription
     *
     * @return Firtal_Reepay_Model_Interface_PeriodInterface[]
     */
    public function getAvailablePeriods(): array
    {
        return Mage::helper('firtal_reepay')->getSubscriptionPeriods();
    }

    /**
     * Adds product to a subscription
     *
     * @param $id
     * @param Firtal_Reepay_Model_DTO_ProductSubscription $product
     * @return mixed
     * @throws Exception
     */
    public function add($id, Firtal_Reepay_Model_DTO_ProductSubscription $product): void
    {
        Mage::helper('firtal_reepay/reepay')->createAddon($product->getProduct());

        $addon = new Firtal_Reepay_Model_Reepay_Addon([
            'handle' => $product->getId(),
            'add_on' => $product->getId(),
            'amount' => $product->getPrice() * 100,
            'quantity' => (int)$product->getQty()
        ]);

        $oldAddOns = $this->getOldAddOnsFromSubscription($id);
        $oldAddOns->addItem($addon);

        foreach ($oldAddOns->getItems() as $item) {
            /** @var Firtal_Reepay_Helper_Client $response */
            $response = Mage::helper('firtal_reepay/client')->put(
                Mage::helper('firtal_reepay/api')->getPrivateKey(),
                "subscription/{$id}", [
                    'handle' => $id,
                    'timing' => self::TIMING_ON_SUBSCRIPTION_CHANGE,
                    'billing' => self::BILLING_ON_SUBSCRIPTION_CHANGE,
                    'compensation_method' => self::COMPENSATION_METHOD_ON_SUBSCRIPTION_CHANGE,
                    'partial_period_handling' => self::PARTIAL_PERIOD_HANDLING, // makes sure not to invoice the change until next period.
                    'add_ons' => [
                        $item->getData()
                    ]
                ]
            );
        }
    }

    /**
     * Removes a product from a subscription
     *
     * @param $id
     * @param Firtal_Reepay_Model_DTO_ProductSubscription $subscription
     * @return mixed
     */
    public function remove($id, Firtal_Reepay_Model_DTO_ProductSubscription $subscription)
    {
        Mage::helper('firtal_reepay/subscription')->removeProductSubscription($id, $subscription);
    }

    /**
     * Gets the specific subscriptions from a customer
     *
     * @param Mage_Customer_Model_Customer $customer
     * @return Firtal_Reepay_Model_DTO_Subscription[]
     * @throws Exception
     */
    public function getSubscriptions(Mage_Customer_Model_Customer $customer): array
    {
        $subscriptions = [];

        try {
            $subs = Mage::helper('firtal_reepay/reepay')->getSubscriptions($customer->getId());
        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
            return null;
        }

        /** @var Firtal_Reepay_Model_Reepay_Subscription $subscription */
        foreach ($subs as $subscription) {
            Mage::log(json_encode(['$subscription' => $subscription->getData()], JSON_PRETTY_PRINT), null, 'test.log', true);
            $subscriptions[] = new Firtal_Reepay_Model_DTO_Subscription(
                $subscription->getHandle(),
                Mage::helper('firtal_reepay')->getSubscriptionPeriod($subscription->getPlan()),
                $subscription->getSubscriptionAddOns(),
                $subscription->getPendingChange() ?: null
            );
        }

        return $subscriptions;
    }

    /**
     * Gets a specific subscription from a customer
     *
     * @param $handle
     * @return Firtal_Reepay_Model_DTO_Subscription
     * @throws Mage_Core_Exception
     * @throws Exception
     */
    public function getSubscription($handle): Firtal_Reepay_Model_DTO_Subscription
    {
        $subscription = Mage::helper('firtal_reepay/subscription')->getSubscriptionByHandle($handle);
        $products = [];

        if ($subscription->hasSubscriptionChanges()) {
            foreach ($subscription->getSubscriptionChanges() as $product) {
                foreach ($product['subscription_add_ons'] as $addon) {
                    $products[] = new Firtal_Reepay_Model_Reepay_Addon($addon);
                }
            }

            $sub = new Firtal_Reepay_Model_DTO_Subscription(
                $subscription->getHandle(),
                Mage::helper('firtal_reepay')->getSubscriptionPeriod($subscription->getPlan()),
                $products
            );

            return $sub;
        }
    }


    /**
     * @param $id
     * @return Varien_Data_Collection
     * @throws Exception
     */
    private function getOldAddOnsFromSubscription($id): Varien_Data_Collection
    {
        $subscription = Mage::helper('firtal_reepay/client')->getJson("subscription/{$id}");
        $addons = new Varien_Data_Collection();

        if (empty($subscription['subscription_add_ons'])) {
            return $addons;
        }

        for ($i = 0; count($subscription['subscription_changes'][0]['subscription_add_ons']) > $i; $i++) {
            $addons->addItem(new Firtal_Reepay_Model_Reepay_Addon([
                    'handle' => $subscription['subscription_changes'][0]['subscription_add_ons'][$i]['handle'],
                    'quantity' => $subscription['subscription_changes'][0]['subscription_add_ons'][$i]['quantity'],
                    'amount' => $subscription['subscription_changes'][0]['subscription_add_ons'][$i]['amount'],
                    'add_on' => $subscription['subscription_changes'][0]['subscription_add_ons'][$i]['handle']
                ])
            );
        }

        return $addons;
    }
}