<?php

class Firtal_Reepay_Model_Observer
{
    const REEPAY_SUBSCRIPTION_NAME_TO_CHECK_IN_CART = 'rp_subscription';

    public function salesOrderPlaceAfter(Varien_Event_Observer $observer)
    {

        /** @var Mage_Sales_Model_Order $order */
        if (!$order = $observer->getOrder()) {
            return;
        }

        if ($order->getPayment()->getMethodInstance()->getCode() !== Firtal_Reepay_Helper_Order::PAYMENT_METHOD) {
            return;
        }

        Mage::helper('firtal_reepay/session')->setOrderStatus($order, 'order_status_before_payment');
    }

    public function salesQuoteCollectTotalsBefore(Varien_Event_Observer $observer): void
    {
        if (!Mage::helper('firtal_reepay')->isReepayEnabled()) {
            return;
        }

        /** @var Mage_Sales_Model_Quote $quote */
        $quote = $observer->getQuote();

        /** @var Mage_Sales_Model_Quote_Item $item */
        foreach ($quote->getAllVisibleItems() as $item) {
            if ($this->isQuoteItemAddedAsAProductSubscription($item)) {
                $this->addProductSubscriptionDiscount($item);
            }
        }
    }

    public function checkoutCartSaveBefore(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('firtal_reepay')->isReepayEnabled()) {
            return;
        }

        /** @var Mage_Checkout_Model_Cart $cart */
        if (!$cart = $observer->getCart()) {
            return;
        }

        foreach ($cart->getItems() as $item) {
            if ($this->isQuoteItemAddedAsAProductSubscription($item)) {
                $this->addProductSubscriptionDiscount($item);
            }
        }
    }

    /**
     * @param Varien_Event_Observer $observer
     * @return null
     */
    public function catalogProductGetFinalPrice(Varien_Event_Observer $observer)
    {
        if (!$action = Mage::app()->getFrontController()->getAction()) {
            return;
        }

        // only way to make logic on whehter your in adminhtml-land or not.
        if (!strstr($action->getFullActionName(), 'adminhtml')) {
            return;
        }

        if (!$product = $observer->getProduct()) {
            return;
        }

        // makes sure not to update an add-on price since it doesn't exists in Reepay
        if ((bool)Mage::helper('firtal_reepay/reepay')->getAddon($product->getId()) === false) {
            return;
        }

        try {
            Mage::helper('firtal_reepay/addon')->updatePrice($product);
        } catch (Exception $e) {
            // TODO: idea - set a cookie if the $e->getMessage() and then browser.cookie.onChange.addEventListener() to listen for it in the template and ofc show it in an alert e.g.
            Mage::logException($e);
            return null;
        }
    }

    /**
     * Setting addtional options to the product before
     * being converted to a quote item
     *
     * @param Varien_Event_Observer $observer
     */
    public function catalogProductLoadAfter(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('firtal_reepay')->isReepayEnabled()) {
            return;
        }

        $action = Mage::app()->getFrontController()->getAction();

        $product = $observer->getProduct();
        $request = $action->getRequest();

        if ($action->getFullActionName() === 'checkout_cart_add') {
            if (!($productId = Mage::getStoreConfig('reepaysubscription_options/plus_subscriptions/plus_subscription_product'))
                || !($plan = Mage::getStoreConfig('reepaysubscription_options/plus_subscriptions/plus_subscription_product_plan'))
                || !Mage::helper('core')->isModuleEnabled('Firtal_Customerpremium')
                || !Mage::helper('core')->isModuleEnabled('Firtal_Businesslogic')) {
                Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
                return;
            }

            Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
            $period = Mage::helper('firtal_reepay')->getSubscriptionPeriod($plan);
            Mage::log(json_encode(['$period' => $period->getId() . ':' . $period->getTitle()], JSON_PRETTY_PRINT), null, 'test.log', true);
            Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);

            if ($productId === $request->getParam('product')) {
                Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
                $this->setOptionsOnProduct($product, $period);
            }
        }

        if ($action->getFullActionName() === 'checkout_cart_ajaxcartpreview') {
            // Makes sure that Amasty's Promo Items auto add products to whole cart
            //  doesn't get the custom option text on the subscription
            if (($reepaySubscription = $request->getParam('reepay_subscription')) && $request->getParam('product') == $product->getId()) {
                Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
                foreach (Mage::helper('firtal_reepay')->getSubscriptionPeriods() as $period) {
                    Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
                    if ($period->getId() === $reepaySubscription) {
                        Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
                        $this->setOptionsOnProduct($product, $period);
                        Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
                        break;
                    }
                }
            }
        }
    }

    /**
     * Saving the quote item's custom additional option to the order item
     *
     * @param Varien_Event_Observer $observer
     */
    public function salesConvertQuoteItemToOrderItem(Varien_Event_Observer $observer)
    {
        if (!Mage::helper('firtal_reepay')->isReepayEnabled()) {
            return;
        }

        /** @var Mage_Sales_Model_Quote_Item $quoteItem */
        $quoteItem = $observer->getItem();
        if ($additionalOptions = $quoteItem->getOptionByCode('additional_options')) {
            /** @var Mage_Sales_Model_Order_Item $orderItem */
            $orderItem = $observer->getOrderItem();

            $options = $orderItem->getProductOptions();
            $options['additional_options'] = unserialize($additionalOptions->getValue());

            $orderItem->setProductOptions($options);
        }
    }

    /**
     * Adds subscription discount to a quote item
     *
     * @param Mage_Sales_Model_Quote_Item $item
     * @throws Exception
     */
    private function addProductSubscriptionDiscount(Mage_Sales_Model_Quote_Item $item): void
    {
        if (!Mage::helper('firtal_reepay')->isReepayEnabled()) {
            return;
        }

        $item = ($item->getParentItem() ? $item->getParentItem() : $item);
        $price = Mage::helper('firtal_reepay')->getLowestDiscountedPrice($item->getProduct());
        $item->setCustomPrice($price);
        $item->setOriginalCustomPrice($price);

        $item->getProduct()->setIsSuperMode(true);
    }

    /**
     * @return float|int|void
     */
    private function getFixedDiscount()
    {
        return Mage::helper('firtal_reepay')->getFixedDiscount();
    }

    /**
     * @param Mage_Sales_Model_Quote_Item $item
     * @return bool
     */
    private function isQuoteItemAddedAsAProductSubscription(Mage_Sales_Model_Quote_Item $item): bool
    {
        foreach ($item->getOptions() as $option) {

            if ($option->getCode() === 'additional_options') {
                $values = unserialize($option->getValue())[0];

                if ($values['type'] === 'rp_subscription') {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @param string $txnType
     * @return array
     */
    private function getTxnTypeByOrder(Mage_Sales_Model_Order $order, string $txnType)
    {
        $transactions = Mage::getModel('sales/order_payment_transaction')->getCollection();
        $transactions->addAttributeToFilter('order_id', ['eq' => $order->getId()]);

        return $transactions->getItemsByColumnValue('txn_type', $txnType);
    }

    /**
     *  Sets the payment information seen on adminhtml orders.
     *  3 cases:
     *      1. If order has an authorized payment   - authorization
     *      2. If order has been captured           - capture
     *      3. If order is waiting for customer     - pending
     *
     * @param Varien_Event_Observer $observer
     */
    public function paymentInfoBlockPrepareSpecificInformation(Varien_Event_Observer $observer)
    {
        if (!Mage::getStoreConfig('payment/reepay_standard/active')) {
           return;
        }

        if (!$payment = $observer->getPayment()) {
            return;
        }

        if (!$transport = $observer->getTransport()) {
            return;
        }

        if (!$block = $observer->getBlock()) {
           return;
        }

        if (empty($payment->getOrder()->getTotalPaid()) && $this->getTxnTypeByOrder($payment->getOrder(), Mage_Sales_Model_Order_Payment_Transaction::TYPE_AUTH)) {
            $transport->setData([
                Mage::helper('firtal_reepay')->__('Payment status') =>
                Mage::helper('firtal_reepay')->__('payment authorized')
            ]);

            return;
        }

        if (empty($payment->getOrder()->getTotalPaid())) {
            $transport->setData([
                Mage::helper('firtal_reepay')->__('Payment status') =>
                    Mage::helper('firtal_reepay')->__('pending payment')
            ]);

            return;
        }


        if ($this->getTxnTypeByOrder($payment->getOrder(), Mage_Sales_Model_Order_Payment_Transaction::TYPE_CAPTURE)) {
            $transport->setData([
                Mage::helper('firtal_reepay')->__('Payment status') =>
                Mage::helper('firtal_reepay')->__('payment paid')
            ]);
        }
    }

    /*
     * @param Mage_Catalog_Model_Product $product
     * @param Firtal_Reepay_Model_DTO_Period $period
     */
    private function setOptionsOnProduct(Mage_Catalog_Model_Product $product, Firtal_Reepay_Model_DTO_Period $period): void
    {
        $additionalOptions = [];

        if ($additionalOptions = $product->getCustomOption('additional_options')) {
            $additionalOptions = (array)unserialize($additionalOptions->getValue());
        }

        $additionalOptions[] = [
            'type' => self::REEPAY_SUBSCRIPTION_NAME_TO_CHECK_IN_CART,
            'label' => Mage::helper('firtal_reepay')->__('Product subscription every'),
            'value' => $period->getTitle(),
            'planHandle' => $period->getId()
        ];

        $product->addCustomOption('additional_options', serialize($additionalOptions), $product);
    }

}









