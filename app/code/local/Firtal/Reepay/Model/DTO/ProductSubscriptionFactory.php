<?php

class Firtal_Reepay_Model_DTO_ProductSubscriptionFactory
{
    public static function create(
        Mage_Catalog_Model_Product $product,
        Firtal_Reepay_Model_Interface_PeriodInterface $period,
        $qty
    ): Firtal_Reepay_Model_DTO_ProductSubscription {
        return new Firtal_Reepay_Model_DTO_ProductSubscription(
            $product,
            $period,
            $qty
        );
    }
}