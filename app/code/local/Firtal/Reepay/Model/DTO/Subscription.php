<?php


class Firtal_Reepay_Model_DTO_Subscription implements Firtal_Reepay_Model_Interface_SubscriptionInterface
{
    const STATUS_ACTIVE = 'active';

    private $id;
    private $productSubscriptions;
    private $period;
    private $pendingStatus;

    /**
     * Firtal_Reepay_Model_DTO_Subscription constructor.
     * @param $id
     * @param $period
     * @param $productSubscriptions
     * @param null $pendingStatus
     */
    public function __construct($id, $period, $productSubscriptions, $pendingStatus = null)
    {
        $this->id = $id;
        $this->period = $period;
        $this->productSubscriptions = $productSubscriptions;
        $this->pendingStatus = $pendingStatus;
    }

    /**
     * Get the products from a subscription
     *
     * @return Firtal_Reepay_Model_DTO_ProductSubscription[]
     * @throws Exception
     */
    public function getProductSubscriptions(): array
    {
        $products = [];

        foreach ($this->productSubscriptions as $subscriptionAddOnHandle) {

            if ($subscriptionAddOnHandle instanceof Firtal_Reepay_Model_Reepay_Addon) {
                break;
            }

            $addOn = Mage::helper('firtal_reepay')->getProductSubscriptionFromAddOn(
                $this->id,
                $subscriptionAddOnHandle
            );

            $products[] = new Firtal_Reepay_Model_DTO_ProductSubscription(
                Mage::getModel('catalog/product')->load((int)$subscriptionAddOnHandle),
                $this->period,
                $addOn->getQuantity()
            );
        }

        return $products;
    }

    /**
     * Gets the id from the subscription
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     * @throws Mage_Core_Exception
     */
    public function getNextDeliveryDate()
    {
        $msg = Mage::helper('firtal_reepay')->__('Unknown');
        $subscription = Mage::helper('firtal_reepay/subscription')->getSubscriptionByHandle($this->id);

        if ($subscription->getError()) {
            return $msg;
        }

        $leadTime = Mage::getStoreConfig('reepaysubscription_options/subscriptions/lead_time') ?: 1;
        $deliveryTime = Mage::getStoreConfig('reepaysubscription_options/subscriptions/delivery_time') ?: 1;
        $nextPeriod = Mage::helper('core')->formatDate($subscription->getNextPeriodStart(), Mage_Core_Model_Locale::FORMAT_TYPE_MEDIUM);

        try {
            $date = new Zend_Date($nextPeriod);
            $nextPeriod = $date->addDay($leadTime + $deliveryTime);
        } catch (Zend_Date_Exception $e) {
            Mage::logException($e);
        }

        return $nextPeriod ?: $msg;
    }

    /**
     * @return null
     */
    public function getPendingStatus()
    {
        return $this->pendingStatus['quantity'];
    }

    /**
     * @return mixed
     */
    public function getPeriod()
    {
        return $this->period;
    }

    public function getProductPriceFromSubscription($addonHandle): float
    {
        $response = Mage::helper('firtal_reepay/client')->getJson("subscription/{$this->getId()}/add_on/{$addonHandle}");
        return $response['amount'] / 100;
    }
}