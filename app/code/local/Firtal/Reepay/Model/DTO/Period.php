<?php

class Firtal_Reepay_Model_DTO_Period implements Firtal_Reepay_Model_Interface_PeriodInterface
{

    private $id;

    /**
     * @var string
     */
    private $title;


    public function __construct($id, string $title)
    {
        $this->id = $id;
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }
}