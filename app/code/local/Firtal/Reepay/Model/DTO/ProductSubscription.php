<?php

class Firtal_Reepay_Model_DTO_ProductSubscription implements Firtal_Reepay_Model_Interface_ProductSubscriptionInterface
{
    /**
     * @var Mage_Sales_Model_Order_Item
     */
    protected $product;
    protected $period;
    protected $qty;

    /**
     * Firtal_Reepay_Model_DTO_ProductSubscription constructor.
     * @param $id
     * @param Mage_Catalog_Model_Product $product
     * @param Firtal_Reepay_Model_Interface_PeriodInterface $period
     * @param $qty
     */
    public function __construct(Mage_Catalog_Model_Product $product, Firtal_Reepay_Model_Interface_PeriodInterface $period, $qty)
    {
        $this->product = $product;
        $this->period = $period;
        $this->qty = $qty;
    }

    /**
     * Gets the id of the add-on (the handle)
     * @return string
     */
    public function getId()
    {
        return $this->product->getId();
    }

    /**
     * Gets a subscription product
     *
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct(): Mage_Catalog_Model_Product
    {
        return $this->product;
    }

    /**
     * Gets the subscription's period
     *
     * @return Firtal_Reepay_Model_Interface_PeriodInterface
     */
    public function getPeriod(): Firtal_Reepay_Model_Interface_PeriodInterface
    {
        return $this->period;
    }

    /**
     * Gets the price of the product subscription
     *
     * @return float
     * @throws Exception
     * @throws Exception
     */
    public function getPrice(): float
    {
        return Mage::helper('firtal_reepay')->getLowestDiscountedPrice($this->product);
    }

    /**
     * @return mixed
     */
    public function getQty()
    {
        return $this->qty;
    }
}