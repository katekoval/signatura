<?php

class Firtal_Reepay_Model_Standard extends Mage_Payment_Model_Method_Abstract
{
    const AUTHORIZED = 'authorized';
    const SETTLED = 'settled';
    const REFUNDED = 'refunded';
    const FAILED = 'failed';
    const CANCELLED = 'cancelled';

    // Makes Reepay control the capturing of the payment
    protected $_canCapture = true;
    protected $_canCapturePartial = true;
    protected $_isGateway = true;
    protected $_canRefund = true;
    protected $_canUseInternal = true;
    protected $_canRefundInvoicePartial = true;

    protected $_code = 'reepay_standard';
    protected $_formBlockType = 'firtal_reepay/form_standard';

    public function getOrderPlaceRedirectUrl()
    {
        Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
        return Mage::getUrl('reepay/standard/redirect');
    }


    /**
     * @param Varien_Object $payment
     * @param float $amount
     * @return Mage_Payment_Model_Abstract|void
     * @throws Exception
     */
    public function capture(Varien_Object $payment, $amount)
    {
        Mage::helper('firtal_reepay/charge')->settle($payment->getOrder()->getIncrementId(), $amount * 100);
    }

    /**
     * @param Varien_Object $payment
     * @param float $amount
     * @return Firtal_Reepay_Model_Standard
     * @throws Exception
     */
    public function refund(Varien_Object $payment, $amount)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $payment->getOrder();

        $transaction = Mage::helper('firtal_reepay/refund')->createRefund($order->getIncrementId(), $amount);

        if ($transaction['state'] === SELF::REFUNDED) {
            $payment = $order->getPayment();

            $payment->setAdditionalData(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS, $transaction['state']);
            $payment->save();
            $order->save();
        }

        return $this;
    }
}