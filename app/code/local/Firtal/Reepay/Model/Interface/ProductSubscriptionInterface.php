<?php


interface Firtal_Reepay_Model_Interface_ProductSubscriptionInterface
{
    /**
     * Gets the id of a product subscription
     *
     * @return mixed
     */
    public function getId();

    /**
     * Gets a subscription product
     *
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct(): Mage_Catalog_Model_Product;

    /**
     * Gets the subscription's period
     *
     * @return Firtal_Reepay_Model_Interface_PeriodInterface
     */
    public function getPeriod(): Firtal_Reepay_Model_Interface_PeriodInterface;

    /**
     * Gets the price of the product subscription
     *
     * @return float
     */
    public function getPrice(): float;
}