<?php

interface Firtal_Reepay_Model_Interface_SubscriptionInterface
{
    /**
     * Get the products from a subscription
     *
     * @return array
     */
    public function getProductSubscriptions(): array;

    /**
     * Gets the id from the subscription
     *
     * @return int
     */
    public function getId();
}