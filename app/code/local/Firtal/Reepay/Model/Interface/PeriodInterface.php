<?php

interface Firtal_Reepay_Model_Interface_PeriodInterface
{
    /**
     * Gets the name of the subscription period
     *
     * @return string
     */
    public function getTitle(): string;

    /**
     * Gets the id of the period
     *
     * @return mixed
     */
    public function getId();
}