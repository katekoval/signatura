<?php

class Firtal_Reepay_Model_System_Config_Source_Agreements
{

    public function toOptionArray()
    {
        return $this->getAllOptions();
    }

    public function getAllOptions()
    {
        /** @var Mage_Adminhtml_Model_Config_Data $code */
        $code = Mage::getSingleton('adminhtml/config_data')->getStore();

        /** @var Mage_Checkout_Model_Resource_Agreement_Collection $collection */
        $collection = Mage::getModel('checkout/agreement')
            ->getCollection()
            ->addStoreFilter($this->_getStoreViewId($code))
            ->addFieldToSelect(array('agreement_id', 'name'));
        $collection->load();

        $result = [
            'label' => Mage::helper('firtal_reepay')->__('---- SELECT ----'),
        ];

        foreach ($collection as $agreement) {
            $result[] = [
                'label' => $agreement->getName(),
                'value' => $agreement->getAgreementId()
            ];
        }

        return $result;
    }

    /**
     * @param $code
     * @return int|mixed
     */
    private function _getStoreViewId($code)
    {
        if(!$code) {
            return Mage_Core_Model_App::ADMIN_STORE_ID;
        }

        return Mage::getModel('core/store')->load($code)->getId();
    }
}