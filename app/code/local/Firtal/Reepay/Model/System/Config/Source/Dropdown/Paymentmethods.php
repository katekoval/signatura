<?php

class Firtal_Reepay_Model_System_Config_Source_Dropdown_Paymentmethods
{
    const PAYMENT_METHODS = [
        ['value' => 'card', 'label' => 'All available debit / credit cards'],
        ['value' => 'dankort', 'label' => 'Dankort'],
        ['value' => 'visa', 'label' => 'VISA'],
        ['value' => 'visa_dk', 'label' => 'VISA/Dankort'],
        ['value' => 'visa_elec', 'label' => 'VISA Electron'],
        ['value' => 'mc', 'label' => 'MasterCard'],
        ['value' => 'amex', 'label' => 'American Express'],
        ['value' => 'mobilepay', 'label' => 'MobilePay'],
        ['value' => 'viabill', 'label' => 'ViaBill'],
        ['value' => 'diners', 'label' => 'Diners Club'],
        ['value' => 'maestro', 'label' => 'Maestro'],
        ['value' => 'laser', 'label' => 'Laser'],
        ['value' => 'discover', 'label' => 'Discover'],
        ['value' => 'jcb', 'label' => 'JCB'],
        ['value' => 'china_union_pay', 'label' => 'China Union Pay']
    ];

    public function toOptionArray()
    {
       return self::PAYMENT_METHODS;
    }
}