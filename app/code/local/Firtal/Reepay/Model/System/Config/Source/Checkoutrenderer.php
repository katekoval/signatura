<?php

class Firtal_Reepay_Model_System_Config_Source_Checkoutrenderer
{
    public function toOptionArray(): array
    {
        return [
            ['value' =>  'window', 'label' => Mage::helper('firtal_reepay')->__('Display checkout in a seperate window')],
            ['value' => 'modal', 'label' => Mage::helper('firtal_reepay')->__('Display the checkout as a modal')],
        ];
    }
}