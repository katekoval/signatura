<?php

class Firtal_Reepay_Block_Form_Standard extends Mage_Payment_Block_Form
{
    CONST checkoutRenderers = [
        'window' => 'window',
        'modal' => 'modal'
    ];

    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('firtal/reepay/form/standard.phtml');
    }

    /**
     * @return array
     */
    public function getPaymentMethodFromAvailableMethods(): array
    {
        $availableMethods = Mage::helper('firtal_reepay')->getPaymentMethods();
        if ($availableMethods === 'card') {
            return Firtal_Reepay_Model_System_Config_Source_Dropdown_Paymentmethods::PAYMENT_METHODS;
        }

        return $availableMethods;
    }

    public function getPaymentMethodDescription()
    {
       return trim(Mage::getStoreConfig('payment/reepay_standard/description'));
    }
}