<?php

class Firtal_Reepay_Block_Agreements extends Mage_Checkout_Block_Agreements
{
    public function getAgreements()
    {
        /** @var Mage_Checkout_Block_Agreements $agreements */
        $agreements =  parent::getAgreements();

        $quote = Mage::getSingleton('checkout/cart')->getQuote()
            ?? Mage::getModel('sales/quote')->load(Mage::getSingleton('core/session')->getVisitorData()['quote_id']);

        /** @var Mage_Sales_Model_Quote_Item $item */
        foreach ($quote->getAllVisibleItems() as $item) {
            if (Mage::helper('firtal_reepay')->isQuoteItemAddedAsAProductSubscription($item) && $extra = $this->getExtraAgreement()) {
                $this->setAgreement($agreements, $extra);
            }
        }

        return $agreements;
    }

    protected function getExtraAgreement()
    {
        $agreementId = Mage::getStoreConfig('reepaysubscription_options/subscriptions/toc');
        $agreement = Mage::getModel('checkout/agreement')->load($agreementId);

        if (!$agreement->getId()) {
            return null;
        }

        $agreement->setIsActive(true);

        return $agreement;
    }

    /**
     * @param $agreements
     * @param $extra
     * @return array
     */
    public function setAgreement($agreements, $extra)
    {
        if (is_array($agreements)) {
            $agreements[] = $extra;
        }

        if ($agreements instanceof Varien_Data_Collection) {
            try {
                $agreements->addItem($extra);
            } catch (Exception $e) {
                // The agreement was already in the collection.
            }
        }

        return $agreements;
    }
}