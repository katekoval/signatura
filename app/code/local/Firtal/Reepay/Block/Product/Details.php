<?php

class Firtal_Reepay_Block_Product_Details extends Mage_Core_Block_Template
{
    /**
     * @var Firtal_Reepay_Helper_Data|Mage_Core_Helper_Abstract
     */
    private $helper;

    /**
     * @var Mage_Catalog_Model_Product|mixed
     */
    private $product;

    public function __construct()
    {
        $this->product = Mage::registry('current_product');
        $this->helper = Mage::helper('firtal_reepay');
    }

    public function getSubscriptionPeriods()
    {
        return Mage::helper('firtal_reepay')->getSubscriptionPeriods();
    }

    /**
     * @return mixed
     */
    public function getDiscountAndFixedDiscount()
    {
        if (!$subcriptionPriceOnProduct = $this->helper->getProductAttributeFromCode($this->product, Firtal_Reepay_Helper_Subscription::SUBSCRIPTION_ATTRIBUTE)) {
            if (!$fixed = Mage::getStoreConfig('reepaysubscription_options/subscriptions/fixed_discount')) {
                if ($subcriptionPriceOnProduct) {
                    return $this->getSubscriptionDiscount($subcriptionPriceOnProduct);
                }

                return false;
            }

            if ($fixed && !$subcriptionPriceOnProduct) {
                return $this->getFixedDiscount();
            }

            return false;
        }



        return $this->getSubscriptionDiscount($subcriptionPriceOnProduct);
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function getSaving()
    {
        return Mage::helper('core')->formatCurrency($this->helper->getLowestDiscountedPrice($this->product));
    }

    public function getFixedDiscount()
    {
        return Mage::helper('firtal_reepay')->getFixedPercentageDiscount();
    }

    public function isFirtalSubscriptionInCart(): bool
    {
        $quote = Mage::getSingleton('checkout/session')->getQuote();

        return (bool) Mage::helper('subscription/product')->isInCart($quote);
    }

    /**
     * @param $subcriptionPriceOnProduct
     * @return float|int
     */
    private function getSubscriptionDiscount($subcriptionPriceOnProduct)
    {
        $saving = $this->product->getFinalPrice() - $subcriptionPriceOnProduct;

        return $saving / $this->product->getFinalPrice() * 100;
    }

    /**
     * Specific to Firtal Helse shops (Firtal_Deadstock)
     *
     * @return bool|void
     */
    public function isUrgentPriceSet()
    {
        if (!Mage::helper('core')->isModuleEnabled('Firtal_Deadstock')) {
            return;
        }

        if ($this->product->getUrgentPriceSet() == 1) {
           return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isExcluded(): bool
    {
        return (bool) Mage::helper('firtal_reepay')
            ->getProductAttributeFromCode($this->product,
                Firtal_Reepay_Helper_Subscription::EXCLUDE_PRODUCT_AS_SUBSCRITION_PRODUCT);
    }
}