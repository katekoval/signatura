<?php

class Firtal_Reepay_Block_Customer_Subscription extends Mage_Core_Block_Template
{
    /**
     * @var Firtal_Reepay_Model_Repository_SubscriptionRepository|Mage_Core_Model_Abstract
     */
    private $repository;

    public function __construct()
    {
        $this->repository = Mage::getSingleton('firtal_reepay/repository_subscriptionRepository');
    }

    /**
     * @return array
     * @throws Exception
     */
    public function getSubscriptions()
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            Mage::getSingleton('core/session')->addError('You are not logged in');
            return;
        }

        $customer = Mage::getSingleton('customer/session')->getCustomer();
        $subscriptions = $this->repository->getSubscriptions($customer);

        if ($subscriptions) {
            return $this->getOnlySubscriptionWithProducts($subscriptions);
        }
    }

    /**
     * @return array|Firtal_Reepay_Model_Interface_PeriodInterface[]
     */
    public function getAvailablePeriods()
    {
        return $this->repository->getAvailablePeriods();
    }

    /**
     * @param $subscriptions[]
     * @return array
     */
    private function getOnlySubscriptionWithProducts(array $subscriptions): array
    {
        return array_filter($subscriptions, function ($subscription) {
            if (!empty($subscription->getProductSubscriptions())) {
                return $subscription;
            }
        });
    }
}