<?php

class Firtal_Reepay_Block_Customer_Subscription_Pending extends Mage_Core_Block_Template
{
    public function getPendingSubscriptions()
    {
        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            Mage::getSingleton('core/session')->addError('You are not logged in');
            return;
        }

        $customer = Mage::getSingleton('customer/session')->getCustomer();
        return Mage::helper('firtal_reepay/subscription')->getPendingSubscriptions($customer);
    }
}