<?php

class Firtal_Reepay_Block_Redirect extends Mage_Core_Block_Template
{
    /**
     * @param $orderId
     * @return string
     */
    public function getAcceptUrl($orderId)
    {
        return Mage::getUrl("reepay/standard/accept/order-{$orderId}");
    }

    /**
     * @param $orderId
     * @return string
     */
    public function getCancelUrl($orderId)
    {
        return Mage::getUrl("reepay/standard/decline/order-{$orderId}");
    }

    /**
     * @return string
     */
    public function getCheckoutCartPage()
    {
        return Mage::getUrl('checkout/cart');
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function renderModalCheckout()
    {
        if ($renderer = Mage::getStoreConfig('payment/reepay_standard/checkoutrenderer') === Firtal_Reepay_Block_Form_Standard::checkoutRenderers['modal']) {
           return $renderer;
        }
    }
}