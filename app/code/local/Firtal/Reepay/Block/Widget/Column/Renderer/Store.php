<?php

class Firtal_Reepay_Block_Widget_Column_Renderer_Store extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
   public function render(Varien_Object $row)
   {
       return Mage::app()->getStore($row->getCustomer()->getStoreId())->getName();
   }
}