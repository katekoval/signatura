<?php

class Firtal_Reepay_Block_Widget_Column_Renderer_Products extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row)
    {
        $html = '';

        foreach ($row->getProducts() as $product) {
            $html .= nl2br($product . "\n");
        }

        return $html;
    }
}