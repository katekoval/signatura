<?php

class Firtal_Reepay_Block_Widget_Column_Renderer_Cancel extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row)
    {
        $subscriptionId = $row->getId();
        $link = Mage::getUrl('reepay/subscriptions/cancel/subscriptionId/' . $subscriptionId);

        return "<a href={$link}>Cancel</a>";
    }
}