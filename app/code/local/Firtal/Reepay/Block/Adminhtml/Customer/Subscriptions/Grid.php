<?php

class Firtal_Reepay_Block_Adminhtml_Customer_Subscriptions_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    /**
     * @var Mage_Core_Model_Abstract|Mage_Customer_Model_Customer
     */
    protected $customer;

    /**
     * @var Firtal_Reepay_Model_Repository_SubscriptionRepository|Mage_Core_Model_Abstract
     */
    protected $repo;

    public function __construct($attributes=array())
    {
        parent::__construct($attributes);
        $this->setId('productSubscriptionsGrid');
        $this->setUseAjax(true);
        $this->_emptyText = Mage::helper('adminhtml')->__('No records found.');

        $this->customer = Mage::getModel('customer/customer')->load(Mage::app()->getRequest()->getParam('id'));
        $this->repo = Mage::getSingleton('firtal_reepay/repository_subscriptionRepository');
    }

    protected function _prepareCollection()
    {
        $collection = new Varien_Data_Collection();
        $subscriptions = $this->repo->getSubscriptions($this->customer);

        /** @var Firtal_Reepay_Model_DTO_Subscription $subscription */
        foreach ($subscriptions as $subscription) {
            $data = new Varien_Object();
            $data->setId($subscription->getId());
            $data->setCustomer($this->customer);
            $data->setProductSubscriptions($subscription->getProductSubscriptions());
            $data->setPeriod($subscription->getPeriod());
            $data->setStatus(Mage::helper('firtal_reepay/subscription')->getStatus($subscription->getId()));
            /** @var Firtal_Reepay_Model_DTO_ProductSubscription $product */
            $products = [];

            foreach ($subscription->getProductSubscriptions() as $product) {
                $data->setPeriod($product->getPeriod()->getTitle());
                $products[] = $product->getProduct()->getName();
            }

            $data->setProducts($products);

            $collection->addItem($data);
        }

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $helper = Mage::helper('firtal_reepay');

        $this->addColumn('subscriptionId', [
            'header' => $helper->__('Id'),
            'index' => 'id'
        ]);

        $this->addColumn('store', [
            'header' => $helper->__('Store'),
            'renderer' => 'Firtal_Reepay_Block_Widget_Column_Renderer_Store'
        ]);

        $this->addColumn('product_subscriptions', [
            'header' => $helper->__('Products'),
            'type' => 'text',
            'renderer' => 'Firtal_Reepay_Block_Widget_Column_Renderer_Products',
        ]);

        $this->addColumn('subscription_period', [
            'header' => $helper->__('Period'),
            'index' => 'period'
        ]);

        $this->addColumn('subscription_status', [
            'header' => $helper->__('Status'),
            'index' => 'status'
        ]);

        $this->addColumn('action', [
            'header' => $helper->__('Action'),
            'type' => 'text',
            'renderer' => 'Firtal_Reepay_Block_Widget_Column_Renderer_Cancel',
        ]);

        return parent::_prepareColumns();
    }

}