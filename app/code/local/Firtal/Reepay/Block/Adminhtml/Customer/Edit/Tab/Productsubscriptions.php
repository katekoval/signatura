<?php

class Firtal_Reepay_Block_Adminhtml_Customer_Edit_Tab_Productsubscriptions extends Mage_Adminhtml_Block_Template implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    /**
     * Set the template for the block
     *
     */
    public function _construct()
    {
        parent::_construct();
        $this->setTemplate('firtal/reepay/customer-tab.phtml');
    }

    /**
     * Return Tab label
     *
     * @return string
     */
    public function getTabLabel()
    {
        return Mage::helper('firtal_reepay')->__('Product Subscriptions');
    }

    /**
     * Return Tab title
     *
     * @return string
     */
    public function getTabTitle()
    {
        return Mage::helper('firtal_reepay')->__('Product Subscriptions');
    }

    /**
     * Can show tab in tabs
     *
     * @return boolean
     * @throws Exception
     */
    public function canShowTab()
    {
        if (!$id = Mage::app()->getRequest()->getParam('id')) {
           return false;
        }

        $customer = Mage::getSingleton('customer/customer')->load($id);

        try {
            if (!$subscriptions = Mage::getSingleton('firtal_reepay/repository_subscriptionRepository')->getSubscriptions($customer)) {
                return false;
            }

            if (count($subscriptions) === 0) {
                return false;
            }

        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * Tab is hidden
     *
     * @return boolean
     * @throws Exception
     */
    public function isHidden()
    {
        return false;
    }
}