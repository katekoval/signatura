<?php

$installer = $this;
$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$setup->addAttribute(
    Mage_Catalog_Model_Product::ENTITY,
    Firtal_Reepay_Helper_Subscription::SUBSCRIPTION_ATTRIBUTE,
    [
        'group'                 => 'Prices',
        'type'					=> 'decimal',
        'input'					=> 'price',
        'label'					=> 'Reepay Subscription',
        'attribute_set'         => 'Price',
        'visible'				=> true,
        'required'				=> false,
        'user_defined'         	=> true,
        'searchable'           	=> true,
        'filterable'           	=> true,
        'comparable'           	=> true,
        'visible_on_front'     	=> true,
        'unique'				=> false,
    ]
);

$installer->endSetup();
