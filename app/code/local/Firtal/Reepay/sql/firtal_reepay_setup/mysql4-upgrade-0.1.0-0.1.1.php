<?php

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;

$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$setup->addAttribute(
    Mage_Catalog_Model_Product::ENTITY,
    Firtal_Reepay_Helper_Subscription::EXCLUDE_PRODUCT_AS_SUBSCRITION_PRODUCT,
    [
        'type'              => 'int',
        'backend'           => '',
        'frontend'          => '',
        'label'             => 'Exclude product from product subscriptions',
        'note'              => 'Reepay related',
        'input'             => 'select',
        'class'             => '',
        'source'            => 'eav/entity_attribute_source_boolean',
        'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'visible'           => true,
        'required'          => false,
        'user_defined'      => false,
        'default'           => false,
        'searchable'        => false,
        'filterable'        => false,
        'comparable'        => false,
        'visible_on_front'  => false,
        'unique'            => false,
        'is_configurable'   => false
    ]
);

$installer->endSetup();