<?php

class Firtal_Reepay_WebhookController extends Mage_Core_Controller_Front_Action
{
    /**
     * @return void|null
     * @throws Mage_Core_Exception
     */
    public function indexAction()
    {
        $response = json_decode($this->getRequest()->getRawBody(), true);
        $customer = Mage::getModel('customer/customer')->load($response['customer']);
        $invoice = $response['invoice'];

        if ($this->isAuthorized($response) === false) {
            return;
        }

        $eventType = $response['event_type'];
        switch ($eventType) {
            case 'invoice_failed':
                Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);
                if (!$order = $this->getOrderFromInvoiceId($invoice)) {
                    Mage::throwException('No order retrieved from Reepay\'s metadata on invoice');
                }
                Mage::helper('firtal_reepay/session')->setOrderStatus($order, 'order_status_before_payment');
                Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);
                break;
            case 'invoice_cancelled':
                Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
                if (!$order = $this->getOrderFromInvoiceId($invoice)) {
                    Mage::throwException('No order retrieved from Reepay\'s metadata on invoice');
                    return;
                }

                try {
                    $order->setStatus(Mage_Sales_Model_Order::STATE_CANCELED)->save();
                } catch (Exception $e) {
                    Mage::throwException($e->getMessage());
                }

                break;
            case 'invoice_authorized':
                Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
                if (!$order = $this->getOrderFromCustomerByInvoiceId($customer, $invoice)) {
                    Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
                    Mage::throwException('Cannot retrieve order from customer by invoice id');
                    return;
                }

                // check if any transactions on order.
                // if any, return - simply do nothing. Everything is fine here.
                if ($this->isAnyTransactionsOnOrder($order) === true) {
                    Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
                    return;
               }

               // if none -> add transaction on the order and set order status after_payment.
                try {
                    $this->addTransactinoOnOrder($this->getInvoiceFromReepay($invoice), $order);
                } catch (Mage_Core_Exception $e) {
                    Mage::log(json_encode(['error on addTransactionOnOrder' => $e->getMessage(), 'increment id' => $order->getIncrementId()], JSON_PRETTY_PRINT), null, 'test.log', true);
                }

                break;
            case 'subscription_renewal':
                try {
                    Mage::helper('firtal_reepay/order')->createOrder(
                        $customer,
                        $response['subscription'],
                        $invoice
                    );
                } catch (Mage_Core_Model_Store_Exception $e) {
                    Mage::log(json_encode([
                        'customer' => $customer->getId(),
                        'subscription' => $response['subscription'],
                        'invoice' => $invoice
                    ], JSON_PRETTY_PRINT), null, 'test.log', true);
                    Mage::throwException($e->getMessage());
                } catch (Mage_Core_Exception $e) {
                    Mage::log(json_encode([
                        'customer' => $customer->getId(),
                        'subscription' => $response['subscription'],
                        'invoice' => $invoice
                    ], JSON_PRETTY_PRINT), null, 'test.log', true);
                    Mage::throwException($e->getMessage());
                }
               break;
            default:
                Mage::throwException('Something went wrong...');
        }
    }

    /**
     * @param $response
     * @return bool
     */
    private function isAuthorized($response)
    {
        $id = $response['id'];
        $timestamp = $response['timestamp'];

        // we need to make sure the timestamp is within +/- 5 seconds
        // If not there's a possibility for MITM attack.
        $time = strtotime($timestamp);
        $timeNow = time();

//        if ($timeNow - $time > 5 || $timeNow - $time < -5) {
//            return false;
//        }

        $webhook_secret = Mage::helper('core')->decrypt(Mage::getStoreConfig('reepaysubscription_options/api/webhook_secret'));
        $hmac = hash_hmac('sha256', $timestamp . $id, $webhook_secret);

        if (hash_equals($hmac, $response['signature'])) {
            return true;
        }

        return false;
    }

    /**
     * @param $invoice
     * @return bool|Mage_Sales_Model_Order
     * @throws Mage_Core_Exception
     */
    private function getOrderFromInvoiceId($invoice)
    {
        if (!$incrementId = Mage::helper('firtal_reepay/invoice')->getOrderIncrementId($invoice)) {
            Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);
            return false;
        }

        if (!$order = Mage::getModel('sales/order')->loadByIncrementId($incrementId)) {
            Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
            return false;
        }

        Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
        Mage::log(json_encode(['$order' => $order->getData()], JSON_PRETTY_PRINT), null, 'test.log', true);
        return $order;
    }

    /**
     * @param Mage_Customer_Model_Customer $customer
     * @param $invoiceId
     * @return Mage_Sales_Model_Order|MDN_AdvancedStock_Model_Sales_Order|void|null
     */
    private function getOrderFromCustomerByInvoiceId(Mage_Customer_Model_Customer $customer, $invoiceId)
    {
        try {
            if (!$invoice = $this->getInvoiceFromReepay($invoiceId)) {
                Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
                return;
            }

            Mage::log(json_encode([
                '$invoice' => $invoice,
                'customer' => $customer->getId()
                ], JSON_PRETTY_PRINT), null, 'test.log', true);

            /** @var Mage_Sales_Model_Order $order */
            if (!$order = Mage::getModel('sales/order')->loadByIncrementId($invoice['handle'])) {
                Mage::throwException('No order with increment id of: #' . $invoice['handle'] );
                return;
            }

            Mage::log(json_encode(['$order->getData()' => $order->getData()], JSON_PRETTY_PRINT), null, 'test.log', true);
            return $order;
        } catch (Exception $e) {
            Mage::log(json_encode(['error in getting invoice from invoice id' => $e->getMessage()], JSON_PRETTY_PRINT), null, 'test.log', true);
            return null;
        }
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return bool
     */
    private function isAnyTransactionsOnOrder(Mage_Sales_Model_Order $order): bool
    {
        $transactions = Mage::getModel('sales/order_payment_transaction')
            ->getCollection()
            ->addAttributeToFilter('order_id', ['eq' => $order->getEntityId()]);

        if (!empty($transactions)) {
            return true;
        }

        return false;
    }

    /**
     * Adds a transaction on the order, since MobilePay doesn't update Reepay right away,
     * or this prevents the Magento state not to take effect,
     *
     * @param $invoice
     * @param Mage_Sales_Model_Order $order
     * @throws Mage_Core_Exception
     */
    private function addTransactinoOnOrder($invoice, Mage_Sales_Model_Order $order): void
    {
        $chargeObject = []; // This is a hack around my own code to imitate the chargeObject from Reepay :/

        Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
        $transaction = $this->getLatestTransaction($invoice);
        Mage::log(json_encode(['$transaction' => $transaction], JSON_PRETTY_PRINT), null, 'test.log', true);
        Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
        Mage::log(json_encode(['$transaction state' => $transaction['state']], JSON_PRETTY_PRINT), null, 'test.log', true);
        Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);

        if ($transaction['state'] === Firtal_Reepay_Model_Standard::SETTLED
            || $transaction['state'] === Firtal_Reepay_Model_Standard::AUTHORIZED) {
            Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
            Mage::helper('firtal_reepay/session')->setOrderStatus($order, 'order_status_after_payment');
        }

        Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);

        $chargeObject['transaction'] = $transaction['card_transaction']['card']['id'];
        Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
        $chargeObject['paymentInfo'] = $transaction;
        $chargeObject['state'] = $transaction['state'];
        Mage::log(json_encode(['$chargeObject heR MISDJS' => $chargeObject], JSON_PRETTY_PRINT), null, 'test.log', true);

        Mage::log(json_encode(['$transaction' => $transaction], JSON_PRETTY_PRINT), null, 'test.log', true);
        Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);

        $payment = Mage::helper('firtal_reepay/payment')
            ->setAdditionalInformationOnPayment($order, $chargeObject);

        Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);

        $stateArr = Mage::helper('firtal_reepay/payment')->getMagentoStateForPayment($chargeObject);
        Mage::log(json_encode(['$stateArr' => $stateArr], JSON_PRETTY_PRINT), null, 'test.log', true);

        Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);
        Mage::helper('firtal_reepay/payment')->createTransactionOnOrder(
            $payment,
            $chargeObject,
            $stateArr['state'],
            $stateArr['isTransactionClosed']
        );

        Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);
    }

    private function getLatestTransaction($invoice)
    {
        Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
        return Mage::helper('firtal_reepay/invoice')->getLastCreatedTransaction($invoice['handle']);
    }

    /**
     * @param $invoiceId
     * @return mixed
     * @throws Exception
     */
    private function getInvoiceFromReepay($invoiceId)
    {
        Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
        $invoice = Mage::helper('firtal_reepay/client')->getJson("invoice/{$invoiceId}");
        if (array_key_exists('error', $invoice)) {
            Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
            Mage::log(json_encode(['$invoice' => $invoice], JSON_PRETTY_PRINT), null, 'test.log', true);
            return;
        }

        Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
        return $invoice;
    }

}
