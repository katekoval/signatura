<?php

class Firtal_Reepay_CustomerController extends Mage_Core_Controller_Front_Action
{
    /**
     * Creates the block in which the subscriptions can be rendered to
     * Also sets data (the subscriptions) on the block
     * So the template can render each subscription
     *
     * @throws Exception
     */
    public function subscriptionsAction()
    {
        $this->isLoggedIn();

        $customer = Mage::getSingleton('customer/session')->getCustomer();

        /** @var Firtal_Reepay_Model_DTO_Subscription $subscriptions */
        $subscriptions = Mage::getSingleton('firtal_reepay/repository_subscriptionRepository')->getSubscriptions($customer);

        $this->loadLayout();

        $block = $this->getLayout()
            ->createBlock('firtal_reepay/customer_subscription', 'rp_customer_subscriptions')
            ->setSubscriptions($subscriptions);

        $this->getLayout()->getBlock('content')->append($block);
        
        $this->renderLayout();
    }

    /**
     * @return Mage_Core_Controller_Varien_Action
     */
    private function isLoggedIn()
    {
        if (!Mage::helper('customer')->isLoggedIn()) {
            Mage::getSingleton('core/session')->addError(
                Mage::helper('firtal_reepay')->__('You\'re not logged in. Please login to proceeed.')
            );

            return $this->_redirect('customer/account/login');
        }
    }

    public function getCancelledAndExpiredSubscriptions($handle)
    {
        $this->isLoggedIn();

        $subscriptions = Mage::helper('firtal_reepay/client')->getJson('subscription', [
            'search' => "handle:{$handle},is_cancelled=true"
        ]);

        return $subscriptions;
    }
}