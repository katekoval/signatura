<?php

class Firtal_Reepay_SubscriptionsController extends Mage_Core_Controller_Front_Action
{
    private $products = [];

    /**
     * @var Firtal_Reepay_Model_Repository_SubscriptionRepository|Mage_Core_Model_Abstract
     */
    private $repository;

    public function __construct(Zend_Controller_Request_Abstract $request, Zend_Controller_Response_Abstract $response, array $invokeArgs = array())
    {
        $this->_request = $request;
        $this->_response= $response;

        Mage::app()->getFrontController()->setAction($this);

        $this->_construct();

        $this->repository = Mage::getSingleton('firtal_reepay/repository_subscriptionRepository');
    }

    /**
     *  Checks if customer is logged in.
     *  If not, the request gets redirected to login screen
     */
    private function isCustomerLoggedIn()
    {
        if (!Mage::helper('customer')->isLoggedIn()) {
            $this->_redirect('customer/account/login');
        };
    }

    public function addAction()
    {
        $this->isCustomerLoggedIn();

        if (!$productId = $_COOKIE['rp_add_productSubscriptionId']) {
            Mage::getSingleton('core/session')->addError(
                Mage::helper('firtal_reepay')->__('Product does not exist.')
            );

            return $this->_redirectReferer();
        }

        if (!$customer = $this->getCustomer()) {
            return $this->_redirectReferer();
        }

        if (!$subscriptionId = Mage::app()->getRequest()->getParam('rp_subscriptions_select_subscription')) {
            return $this->_redirectReferer();
        }

        if (!$product = Mage::getModel('catalog/product')->load($productId)) {
            return $this->_redirectReferer();
        }

        $subscriptionData = [
            'subscriptionId' => $subscriptionId,
            'productSubscriptionId' => $productId
        ];

        /** @var Firtal_Reepay_Model_DTO_Subscription $subscription */
        foreach ($this->repository->getSubscriptions($customer) as $subscription) {
            if ($subscriptionId == $subscription->getId()) {
                Mage::helper('firtal_reepay/reepay')->createAddon($product);

                $subscriptionProduct = Firtal_Reepay_Model_DTO_ProductSubscriptionFactory::create(
                   $product,
                   $subscription->getPeriod(),
                   1
               );

                foreach ($subscription->getProductSubscriptions() as $product) {
                    if ($product->getId() == $productId) {
                        $subscriptionData['productSubscriptionQty'] = $product->getQty() + 1;

                        Mage::helper('firtal_reepay/subscription')->changeProductSubscriptionQty($subscriptionData);

                        Mage::getSingleton('core/session')->addSuccess(
                            Mage::helper('firtal_reepay')->__("%s has been added to subscription: %s which will be renewed every %s", $subscriptionProduct->getProduct()->getName(), $subscriptionId, $subscriptionProduct->getPeriod()->getTitle())
                        );

                        return $this->_redirectReferer();
                    }
                }

                $this->repository->add(
                    $subscriptionId,
                    $subscriptionProduct
                );

                Mage::getSingleton('core/session')->addSuccess(
                    Mage::helper('firtal_reepay')->__("%s has been added to subscription: %s which will be renewed every %s", $subscriptionProduct->getProduct()->getName(), $subscriptionId, $subscriptionProduct->getPeriod()->getTitle())
                );

                $this->_redirectReferer();
            }
        }
    }

    /**
     * @return Mage_Core_Controller_Varien_Action
     * @throws Mage_Core_Exception
     * @throws Exception
     */
    public function cancelAction()
    {
        if (!$subscriptionId = $this->getRequest()->getParam('subscriptionId')) {
            Mage::getSingleton('customer/session')->addError(Mage::helper('firtal_reepay')->__('No subscription id provided. Contact customer service.'));
            return $this->_redirectReferer();
        }

        if (!$productSubscriptionId = $this->getRequest()->getParam('productSubscriptionId')) {
            Mage::getSingleton('customer/session')->addError(Mage::helper('firtal_reepay')->__('No product subscription id provided. Contact customer service.'));
            return $this->_redirectReferer();
        }

        $subscriptions = $this->repository->getSubscriptions(Mage::helper('customer')->getCurrentCustomer());

        /** @var Firtal_Reepay_Model_DTO_Subscription $subscription */
        foreach ($subscriptions as $subscription) {
            if ($subscription->getId() == $subscriptionId) {
                foreach ($subscription->getProductSubscriptions() as $product) {
                    if ($product->getId() === $productSubscriptionId) {
                        $this->repository->remove($subscriptionId, $product);
                    }
                }
            }
        }

        return $this->_redirectReferer();
    }

    public function reactivate()
    {
        $this->isCustomerLoggedIn();

        $subscriptionId = $this->getRequest()->getParam('subscriptionId');
        $productSubscriptionId = $this->getRequest()->getParam('productSubscriptionId');

        Mage::helper('firtal_reepay/subscription')->reactivate($subscriptionId);
    }

    /**
     * @return Mage_Core_Controller_Varien_Action
     * @throws Exception
     */
    public function editAction()
    {
        if (!$this->getRequest()->getPost()) {
            Mage::getSingleton('adminhtml/session')->addWarning(
                Mage::helper('firtal_reepay')->__('No data provided to change the quantity.')
            );

            return $this->_redirectReferer();
        }

         try {
            Mage::helper('firtal_reepay/subscription')->changeProductSubscriptionQty($this->getRequest()->getPost());
         } catch (Exception $e) {
            Mage::getSingleton('core/session')->addError(
                Mage::helper('firtal_reepay')->__('You need to log in.')
            );

             $this->_redirectReferer();
         }

        $this->_redirectReferer();
    }

    /**
     * @return Mage_Customer_Model_Customer
     */
    private function getCustomer(): Mage_Customer_Model_Customer
    {
        return Mage::getSingleton('customer/session')->getCustomer();
    }
}