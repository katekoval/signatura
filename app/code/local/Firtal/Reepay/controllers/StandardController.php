<?php

class Firtal_Reepay_StandardController extends Mage_Core_Controller_Front_Action
{
    /**
     * @var string
     */
    protected $_state;

    /**
     * @var bool
     */
    protected $_isTransactionClosed;

    public function redirectAction()
    {
        Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
        $this->loadLayout();
        Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);

        /** @var Mage_Checkout_Model_Session $session */
        $session = Mage::getSingleton('checkout/session');
        $quoteId = $session->getQuoteId();
        Mage::log(__METHOD__.":".__LINE__ . ":quote_id:".$quoteId, null, 'test.log', true);


        Mage::log(__METHOD__.":".__LINE__ . ":quote_id:".$quoteId, null, 'test.log', true);
        $order = Mage::getModel('sales/order')->loadByIncrementId($session->getLastRealOrderId());
        Mage::log(__METHOD__.":".__LINE__ . ":quote_id:".$quoteId, null, 'test.log', true);

        /** @var Mage_Sales_Model_Order_Item $item */
        foreach ($order->getAllItems() as $item) {
            if ($item->getProductId() == Mage::getStoreConfig('reepaysubscription_options/plus_subscriptions/plus_subscription_product')) {
                Mage::dispatchEvent('set_product_options_to_make_recurring_product', compact('item'));
            }

            if (Mage::helper('firtal_reepay/payment')->isItemRecurring($item)) {
                Mage::log(__METHOD__.":".__LINE__ . ":quote_id:".$quoteId, null, 'test.log', true);
                $recurringChargeSession = Mage::helper('firtal_reepay/session')->getChargeSessionId($order, true)['id'];
            }
        }

        Mage::log(__METHOD__.":".__LINE__ . ":quote_id:".$quoteId, null, 'test.log', true);
        $chargeSessionId = Mage::helper('firtal_reepay/session')->getChargeSessionId($order)['id'];
        Mage::log(__METHOD__.":".__LINE__ . ":quote_id:".$quoteId, null, 'test.log', true);

        /** @var Mage_Core_Block_Template $block */
        $block = $this->getLayout()->createBlock(
            'Firtal_Reepay_Block_Redirect',
            'reepay_standard_redirect',
            ['template' => 'firtal/reepay/redirect.phtml']
        );
        Mage::log(__METHOD__.":".__LINE__ . ":quote_id:".$quoteId, null, 'test.log', true);

        $block->setOrder($order);
        Mage::log(__METHOD__.":".__LINE__ . ":quote_id:".$quoteId, null, 'test.log', true);

        if (!Mage::app()->getLayout()->getBlock('reepay_standard_redirect')) {
            Mage::log(__METHOD__.":".__LINE__ . ":quote_id:".$quoteId, null, 'test.log', true);
            return;
        }

        $this->getLayout()->getBlock('content')->append($block);
        Mage::log(__METHOD__.":".__LINE__ . ":quote_id:".$quoteId, null, 'test.log', true);
        $block->setChargeSessionId($recurringChargeSession ?: $chargeSessionId);
        Mage::log(__METHOD__.":".__LINE__ . ":quote_id:".$quoteId, null, 'test.log', true);

        $this->renderLayout();
    }

    /**
     * Checking the actual status of the payment with server-to-server API call.
     * and redirecting to success page
     *
     * @throws Mage_Core_Exception
     */
    public function acceptAction()
    {
        try {
            Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
            $session = Mage::getSingleton('checkout/session');
            $quoteId = $session->getQuoteId();
            Mage::log(__METHOD__.":".__LINE__.":quote_id:".$quoteId, null, 'test.log', true);
            /** @var Mage_Sales_Model_Order $order */
            $order = Mage::getModel('sales/order')->loadByIncrementId($session->getLastRealOrderId());
            Mage::log(__METHOD__.":".__LINE__.":quote_id:".$quoteId, null, 'test.log', true);
            $this->_state = Mage::helper('firtal_reepay/charge')->getState($order);
            Mage::log(__METHOD__.":".__LINE__.":quote_id:".$quoteId, null, 'test.log', true);

            if (Mage::helper('firtal_reepay/validator')->validate($this->_state) === false) {
                Mage::getSingleton('core/session')->addError('You\'re not authorized.');
                Mage::log(__METHOD__.":".__LINE__.":quote_id:".$quoteId, null, 'test.log', true);
                return $this->_redirect('checkout/cart');
            }

            Mage::log(__METHOD__.":".__LINE__.":quote_id:".$quoteId, null, 'test.log', true);
            $chargeObject = Mage::helper('firtal_reepay/charge')->getChargeObject($order->getIncrementId());
            Mage::log(__METHOD__.":".__LINE__.":quote_id:".$quoteId, null, 'test.log', true);

            if ($recurringPaymentMethod = $chargeObject['recurring_payment_method']) {
                Mage::log(__METHOD__.":".__LINE__.":quote_id:".$quoteId, null, 'test.log', true);
                $this->createSubscriptionProducts($order, $recurringPaymentMethod);
            }

            if ($chargeObjectState = Mage::helper('firtal_reepay/payment')->getMagentoStateForPayment($chargeObject)) {
                Mage::log(__METHOD__.":".__LINE__.":quote_id:".$quoteId, null, 'test.log', true);
                $this->_state = $chargeObjectState['state'];
                Mage::log(__METHOD__.":".__LINE__.":quote_id:".$quoteId, null, 'test.log', true);
                $this->_isTransactionClosed = $chargeObjectState['isTransactionClosed'];
                Mage::log(__METHOD__.":".__LINE__.":quote_id:".$quoteId, null, 'test.log', true);
            }

            Mage::log(__METHOD__.":".__LINE__.":quote_id:".$quoteId, null, 'test.log', true);
            $payment = Mage::helper('firtal_reepay/payment')->setAdditionalInformationOnPayment($order, $chargeObject);
            Mage::log(__METHOD__.":".__LINE__.":quote_id:".$quoteId, null, 'test.log', true);

            Mage::helper('firtal_reepay/payment')
                ->createTransactionOnOrder($payment, $chargeObject, $this->_state, $this->_isTransactionClosed);
            Mage::log(__METHOD__.":".__LINE__.":quote_id:".$quoteId, null, 'test.log', true);
            Mage::helper('firtal_reepay/invoice')->setMetadata($order->getIncrementId(), [
                'order' => $order->getIncrementId()]
            );

            Mage::log(__METHOD__.":".__LINE__.":quote_id:".$quoteId, null, 'test.log', true);
            try {
                Mage::log(__METHOD__.":".__LINE__.":quote_id:".$quoteId, null, 'test.log', true);
                Mage::helper('firtal_reepay/session')->setOrderStatus($order, 'order_status_after_payment');
            } catch (Exception $e) {
                Mage::log(__METHOD__.":".__LINE__.":quote_id:".$quoteId, null, 'test.log', true);
                Mage::log(json_encode(['$e->getMessage()' => $e->getMessage()], JSON_PRETTY_PRINT), null, 'test.log', true);
            }

            Mage::log(__METHOD__.":".__LINE__.":quote_id:".$quoteId, null, 'test.log', true);
            $order->save();

            Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
            if (Mage::getStoreConfigFlag('payment/reepay_standard/order_email')) {
                    Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
                    if ($order->getCanSendNewEmailFlag() === true) {
                        Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
                        try {
                            Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
                            $order->setEmailSent(true);
                            Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
                            $order->sendNewOrderEmail();
                            Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
                            $order->save();
                            Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
                        } catch (Exception $e) {
                            Mage::log(json_encode(['sendNewOrderEmail $e->getMessage()' => $e->getMessage()], JSON_PRETTY_PRINT), null, 'test.log', true);
                        }
                    }
                Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
            }
            Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);

            Mage::getSingleton('checkout/cart')->truncate()->save();
            Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
            return $this->_redirect('checkout/onepage/success');
        } catch (Exception $e) {
            Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
            Mage::log(json_encode(['$e->getMessage()' => $e->getMessage()], JSON_PRETTY_PRINT), null, 'test.log', true);
            return $this->_redirect('checkout/cart');
        }
    }

    public function declineAction()
    {
        Mage::getModel('core/session')->addError('The payment was declined. Please try again or contact our customer service');
        $this->_redirect('checkout/cart');
    }

    /**
     * @param $chargeObject
     */
    protected function getMagentoStateForPayment($chargeObject): void
    {
        switch ($chargeObject['state']) {
            case Firtal_Reepay_Model_Standard::AUTHORIZED:
                $this->_state = Mage_Sales_Model_Order_Payment_Transaction::TYPE_AUTH;
                $this->_isTransactionClosed = false;
                break;
            case Firtal_Reepay_Model_Standard::SETTLED:
                $this->_state = Mage_Sales_Model_Order_Payment_Transaction::TYPE_CAPTURE;
                $this->_isTransactionClosed = true;
                break;
            default:
                break;
        }
    }

    /**
     * @param Mage_Sales_Model_Order_Payment $payment
     * @param $chargeObject
     * @throws Exception
     */
    protected function createTransactionOnOrder(Mage_Sales_Model_Order_Payment $payment, $chargeObject): void
    {
        $transaction = $payment->addTransaction($this->_state);
        $transaction->setAdditionalData(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS, (array)$chargeObject);
        $transaction->setTxnId($chargeObject['transaction']);
        $transaction->setIsClosed($this->_isTransactionClosed);
        $transaction->save();
    }

    /**
     * @param Mage_Core_Model_Abstract $order
     * @param $chargeObject
     * @return Mage_Sales_Model_Order_Payment
     * @throws Exception
     */
    protected function setAdditionalInformationOnPayment(
        Mage_Core_Model_Abstract $order,
        $chargeObject
    ): Mage_Sales_Model_Order_Payment {
        /** @var Mage_Sales_Model_Order_Payment $payment */
        $payment = $order->getPayment();
        $payment->setTransactionId($chargeObject['transaction']);
        $payment->setAddtionalInformation(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS,
            (array)$chargeObject);
        $payment->save();

        return $payment;
    }

    /**
     * @param $orderItem
     * @param Firtal_Reepay_Model_DTO_Period $period
     * @return Firtal_Reepay_Model_DTO_ProductSubscription
     */
    private function getProductSubscription(
        Mage_Sales_Model_Order_Item $orderItem,
        Firtal_Reepay_Model_DTO_Period $period
    ): Firtal_Reepay_Model_DTO_ProductSubscription {
        return new Firtal_Reepay_Model_DTO_ProductSubscription(
            Mage::getModel('catalog/product')->load($orderItem->getProductId()),
            $period,
            $orderItem['qty_ordered']
        );
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @param $recurringPaymentMethod
     * @return null
     * @throws Mage_Core_Exception
     * @throws Exception
     */
    private function createSubscriptionProducts(Mage_Sales_Model_Order $order, $recurringPaymentMethod)
    {
        /** @var Firtal_Reepay_Model_Repository_SubscriptionRepository $repo */
        $repo = Mage::getSingleton('firtal_reepay/repository_subscriptionRepository');
        $subs = [];
        $plans = [];

        /** @var Mage_Sales_Model_Order_Item $orderItem */
        foreach ($order->getAllItems() as $orderItem) {
            foreach ($orderItem->getProductOptionByCode('additional_options') as $option) {
                if ($option['type'] === Firtal_Reepay_Model_Observer::REEPAY_SUBSCRIPTION_NAME_TO_CHECK_IN_CART) {
                    $plans[] = $option['planHandle'];
                    $subs[] = new Varien_Object([
                        'plan' => $option['planHandle'],
                        'item' => $orderItem
                    ]);
                }
            }
            if (Mage::helper('firtal_reepay/payment')->isItemRecurring($orderItem)) {
                if ($plan = $orderItem->getProductOptionByCode('info_buyRequest')['reepay_subscription'] ?: $orderItem->getProductOptionByCode('additional_options')['planHandle']) {
                    $plans[] = $plan;
                    $subs[] = new Varien_Object([
                        'plan' => $plan,
                        'item' => $orderItem
                    ]);
                }
            }
        }

        $countedPlans = array_count_values($plans);

        foreach ($countedPlans as $plan => $count) {
            /** @var Firtal_Reepay_Model_DTO_Subscription $subscription */
            $subscription = Mage::helper('firtal_reepay/subscription')
                ->getSubscription($plan, $order, $recurringPaymentMethod);

            foreach ($subs as $sub) {
                if ($sub->getPlan() === $plan && $count > 1) {
                    $repo->add(
                        $subscription->getHandle(),
                        $this->getProductSubscription(
                            $sub->getItem(),
                            Mage::helper('firtal_reepay')->getSubscriptionPeriod($plan)
                        )
                    );
                }
            }

            foreach ($subs as $sub) {
                if ($sub->getPlan() === $plan) {
                    $repo->add(
                        $subscription->getHandle(),
                        $this->getProductSubscription(
                            $sub->getItem(),
                            Mage::helper('firtal_reepay')->getSubscriptionPeriod($plan)
                        )
                    );
                }
            }
        }
    }
}