<?php

class Firtal_Reepay_Helper_Order extends Mage_Core_Helper_Abstract
{
    const PAYMENT_METHOD = 'reepay_standard';
    const SHIPPING_METHOD = 'flatrate';
    const REEPAY_SHIPMENT = 'productsubscription_shipment';
    private $store;

    /**
     * @return mixed
     * @throws Mage_Core_Exception
     */
    public function getShippingMethod()
    {
        $carriers = Mage::getSingleton('shipping/config')->getAllCarriers();

        foreach ($carriers as $carrier) {
            Mage::log(json_encode(['$carrier->getId()' => $carrier->getId()], JSON_PRETTY_PRINT), null, 'test.log',
                true);
            if ($carrier->getId() !== self::SHIPPING_METHOD) {
                Mage::throwException('Reepay subscription shipment not found in flatrate');
                return;
            }

            $methods = Mage::getModel('shipping/carrier_flatrate')->getAllowedMethods();
            Mage::log(json_encode(['$methods' => $methods], JSON_PRETTY_PRINT), null, 'test.log', true);

            if ($methods[$carrier->getId()] !== self::REEPAY_SHIPMENT) {
                Mage::throwException('Shipping method and reepay shipment const is not from the same carrier');
                return;
            }

            return $carrier;
        }
    }

    /**
     * @param Mage_Customer_Model_Customer $customer
     * @param $subscriptionHandle
     * @param $invoice
     * @return void
     * @throws Mage_Core_Exception
     * @throws Mage_Core_Model_Store_Exception
     * @throws Exception
     */
    public function createOrder(Mage_Customer_Model_Customer $customer, $subscriptionHandle, $invoice)
    {
        Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);

        $this->store = $customer->getStore();
        Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
        Mage::log(json_encode(['$this->store->getFrontendName()' => $this->store->getFrontendName()], JSON_PRETTY_PRINT), null, 'test.log', true);

        $shippingMethod = self::SHIPPING_METHOD;
        $paymentMethod = self::PAYMENT_METHOD;
        $repo = Mage::getSingleton('firtal_reepay/repository_subscriptionRepository');

        if ($billingAddress = $customer->getDefaultBillingAddress()) {
            $billingAddress = $billingAddress->getData();
        } else {
            $billingAddress = Mage::helper('firtal_reepay/customer')->getOrderAddress($customer)->getData();
        }

        if ($shippingAddress = $customer->getDefaultShippingAddress()) {
            $shippingAddress = $shippingAddress->getData();
        } else {
            $shippingAddress = Mage::helper('firtal_reepay/customer')->getOrderAddress($customer)->getData();
        }

        try {
            $subscriptions = $repo->getSubscriptions($customer);
            Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);
        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
            return null;
        }
        Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);

        Mage::log(json_encode(['$subscriptionHandle' => $subscriptionHandle], JSON_PRETTY_PRINT), null, 'test.log',
            true);

        Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);

        /** @var Mage_Sales_Model_Quote $quote */
        $quote = Mage::getModel('sales/quote')->setStoreId($this->store->getId());
        Mage::log(json_encode(['$quote' => $quote], JSON_PRETTY_PRINT), null, 'test.log', true);
        $quote->setCurrency($this->store->getBaseCurrencyCode());
        Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);
        $quote->assignCustomer($customer);
        Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);
        /** @var Firtal_Reepay_Model_DTO_Subscription $subscription */
        foreach ($subscriptions as $subscription) {
            Mage::log(json_encode(['$subscription->getId()' => $subscription->getId()], JSON_PRETTY_PRINT), null, 'test.log', true);
            if ($subscription->getId() == $subscriptionHandle) {
                /** @var Firtal_Reepay_Model_DTO_ProductSubscription $productSubscription */
                Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
                foreach ($subscription->getProductSubscriptions() as $productSubscription) {
                    Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);

                    $quote->addProduct($productSubscription->getProduct(), $productSubscription->getQty());
                    $quote->save();
                    $discountedPrice = $subscription->getProductPriceFromSubscription($productSubscription->getId());

                    $quoteItem = $quote->getItemByProduct($productSubscription->getProduct());
                    $quoteItem->setOriginalCustomPrice($discountedPrice);
                    $quoteItem->setCustomPrice($discountedPrice);
                    $quoteItem->setIsSuperMode(true);
                    $quoteItem->save();
                    Mage::log(json_encode(['$quote->getItemByProduct' => $quote->getItemByProduct($productSubscription->getProduct())->getData()],
                        JSON_PRETTY_PRINT), null, 'test.log', true);
                }
            }
        }

        Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);

        $billingAddressData = $quote->getBillingAddress()->addData($billingAddress);
        Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);
        $shippingAddressData = $quote->getShippingAddress()->addData($shippingAddress);
        Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);

        $quote->getPayment()->importData([
            'method' => $paymentMethod
        ]);

        Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);

        Mage::log(json_encode(['$quote' => $quote], JSON_PRETTY_PRINT), null, 'test.log', true);

        Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);

        $carrier = $this->getShippingMethod();
        Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);
        try {
            Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);
            $quote->collectTotals()->save();
            $shippingAddressData->setCollectShippingRates(true)
                ->collectShippingRates();
            Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);
            $shippingAddressData->setShippingMethod('flatrate_flatrate')
                ->setPaymentMethod($paymentMethod);
            Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);
            $quote->save();

            // don't create order if already existing.
            if (!$this->isOrderCreatedForInvoice($invoice)) {
                // creates the order from the quote
                $service = Mage::getModel('sales/service_quote', $quote);
                Mage::log(json_encode(['$service' => $service->getQuote()->getData()], JSON_PRETTY_PRINT), null,
                    'test.log',
                    true);

                Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);
                try {
                    $service->submitAll();
                    Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);
                } catch (Exception $e) {
                    Mage::throwException($e->getMessage());
                }

                $order = $service->getOrder();
                $incrementId = $order->getRealOrderId();

                Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);

                Mage::helper('firtal_reepay/invoice')->setMetadata($invoice, [
                    'order' => $incrementId
                ]);

                Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);
                Mage::log(json_encode(['$order' => $order->getData()], JSON_PRETTY_PRINT), null, 'test.log', true);
                Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);
                Mage::log(json_encode(['$incrementId' => $incrementId], JSON_PRETTY_PRINT), null, 'test.log', true);

                Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);
                Mage::getSingleton('checkout/session')
                    ->setLastQuoteId($quote->getId())
                    ->setLastSuccessQuoteId($quote->getId())
                    ->clearHelperData();
            }

            $incrementId = Mage::helper('firtal_reepay/invoice')->getOrderIncrementId($invoice);
            /** @var Mage_Sales_Model_Order $order */
            $order = Mage::getModel('sales/order')->loadByIncrementId($incrementId);

            Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);
            $chargeObject = []; // This is a hack around my own code to imitate the chargeObject from Reepay :/

            Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
            $invoice = Mage::helper('firtal_reepay/invoice')->getInvoice($invoice);
            Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
            $transaction = $this->getLatestTransaction($invoice);
            Mage::log(json_encode(['$transaction' => $transaction], JSON_PRETTY_PRINT), null, 'test.log', true);
            Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
            Mage::log(json_encode(['$transaction state' => $transaction['state']], JSON_PRETTY_PRINT), null, 'test.log', true);
            Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
            Mage::helper('firtal_reepay/session')->setOrderStatus($order, 'order_status_before_payment');

            if ($transaction['state'] === Firtal_Reepay_Model_Standard::SETTLED
                || $transaction['state'] === Firtal_Reepay_Model_Standard::AUTHORIZED) {
                Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
                Mage::helper('firtal_reepay/session')->setOrderStatus($order, 'order_status_after_payment');
            }

            Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);

            $chargeObject['transaction'] = $transaction['card_transaction']['card']['id'];
            Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
            $chargeObject['paymentInfo'] = $transaction;
            $chargeObject['state'] = $transaction['state'];

            Mage::log(json_encode(['$transaction' => $transaction], JSON_PRETTY_PRINT), null, 'test.log', true);
            Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);

            $payment = Mage::helper('firtal_reepay/payment')
                ->setAdditionalInformationOnPayment($order, $chargeObject);

            Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);

            $stateArr = Mage::helper('firtal_reepay/payment')->getMagentoStateForPayment($chargeObject);

            Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);
            Mage::helper('firtal_reepay/payment')->createTransactionOnOrder(
                $payment,
                $chargeObject,
                $stateArr['state'],
                $stateArr['isTransactionClosed']
            );

            Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);

            if (Mage::getStoreConfigFlag('payment/reepay_standard/order_email')) {
                Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);
                $order->sendNewOrderEmail()
                    ->setEmailSent(true);
            }

            Mage::log("Order created with increment id: #{$incrementId}", null, 'test.log', true);

            $this->invoiceOrder($order);
        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
            return null;
        }

        Mage::log(__METHOD__ . ":" . __LINE__, null, 'test.log', true);
    }

    private function getLatestTransaction($invoice)
    {
        Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
        return Mage::helper('firtal_reepay/invoice')->getLastCreatedTransaction($invoice['handle']);
    }

    private function isOrderCreatedForInvoice($invoice): bool
    {
        Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
        return (bool) Mage::helper('firtal_reepay/invoice')->getOrderIncrementId($invoice);
    }

    private function invoiceOrder(Mage_Sales_Model_Order $order)
    {
        foreach ($order->getAllVisibleItems() as $item) {
            /** @var Mage_Sales_Model_Order_Item $item */
            if ($item->getProductType() === Mage_Catalog_Model_Product_Type::TYPE_VIRTUAL) {
                if (!$order->canInvoice()) {
                    Mage::throwException(sprintf("%s cannot be invoiced", $order->getRealOrderId()));
                }

                $invoice = Mage::getModel('sales/service_order', $order)->prepareInvoice();

                if (!$invoice->getTotalQty()) {
                    Mage::throwException(Mage::helper('core')->__('Cannot create an invoice without products.'));
                }

                $invoice->setRequestedCaptureCase(Mage_Sales_Model_Order_Invoice::CAPTURE_ONLINE);
                $invoice->register();
                $transactionSave = Mage::getModel('core/resource_transaction')
                    ->addObject($invoice)
                    ->addObject($invoice->getOrder());
                $transactionSave->save();

                return;
            }
        }
    }
}