<?php

class Firtal_Reepay_Helper_Addon extends Mage_Core_Helper_Abstract
{
    const ENDPOINT = 'add_on';
    const ADDON_STATE_ACTIVE = 'active';
    const ADDON_STATE_DELETED = 'deleted';

    /**
     * Updates the price on a adddon every time
     *
     * @param Mage_Catalog_Model_Product $product
     * @throws Exception
     */
    public function updatePrice(Mage_Catalog_Model_Product $product)
    {
        if (Mage::getStoreConfigFlag('reepaysubscription_options/subscriptions/update_price') === false) {
            return;
        }

        $response = Mage::helper('firtal_reepay/client')->put(
            Mage::helper('firtal_reepay/api')->getPrivateKey(),
            self::ENDPOINT . "/{$product->getId()}", [
                'handle' => $product->getId(),
                'name' => $product->getName(),
                'amount' => (float)Mage::helper('firtal_reepay')->getLowestDiscountedPrice($product) * 100,
                'all_plans' => true
            ]
        );

        if (array_key_exists('error', $response)) {
            throw new Exception($response['message']);
        }
    }

    /**
     * Sets the addon's state
     *
     * @param $handle
     * @return null|void
     */
    public function undelete($handle)
    {
        try {
            Mage::helper('firtal_reepay/client')->post(
                Mage::helper('firtal_reepay/api')->getPrivateKey(),
                self::ENDPOINT . "/{$handle}/undelete", [
                    'handle' => $handle,
                ]
            );
        } catch (Exception $e) {
            Mage::logException($e);
            return null;
        }
    }
}