<?php

class Firtal_Reepay_Helper_Data extends Mage_Core_Helper_Abstract
{
    protected $fixedDiscount;
    private $periods = [];

    public function __construct()
    {
        if (!$this->fixedDiscount = Mage::getStoreConfig('reepaysubscription_options/subscriptions/fixed_discount')) {
            return;
        }
    }

    public function isReepayEnabled($store = null): bool
    {
        return (bool) Mage::getStoreConfigFlag('reepaysubscription_options/general/enabled', $store);
    }

    public function getFixedPercentageDiscount()
    {
        if (!$discount = Mage::getStoreConfig('reepaysubscription_options/subscriptions/fixed_discount')) {
           return '';
        }

        return trim($discount) . '%';
    }

    /**
     * @param Mage_Sales_Model_Quote_Item $item
     * @return bool
     */
    public function isQuoteItemAddedAsAProductSubscription(Mage_Sales_Model_Quote_Item $item): bool
    {
        foreach ($item->getOptions() as $option) {

            if ($option->getCode() === 'additional_options') {
                $values = unserialize($option->getValue())[0];

                if ($values['type'] === 'rp_subscription') {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Gets the lowest price of 2:
     *
     * 1) final price (discounted)
     * 2) Subscription price (if set on product, the normal price * fixed discount set in backend)
     *
     * @param Mage_Catalog_Model_Product $product
     * @return mixed
     * @throws Exception
     */
    public function getLowestDiscountedPrice(Mage_Catalog_Model_Product $product)
    {
        $helper = Mage::helper('firtal_reepay');

        Mage::dispatchEvent('get_lowest_discounted_price_before', ['product' => $product, 'discount' => $helper->getFixedDiscount()]);

        if (!$fixedDiscount = $helper->getFixedDiscount()) {
            if (!($subcriptionPriceOnProduct = $helper->getProductAttributeFromCode($product,
                Firtal_Reepay_Helper_Subscription::SUBSCRIPTION_ATTRIBUTE))) {
                return $product->getFinalPrice();
            }
        }

        $subcriptionPriceOnProduct = $helper->getProductAttributeFromCode($product,
            Firtal_Reepay_Helper_Subscription::SUBSCRIPTION_ATTRIBUTE);

        if ($subcriptionPriceOnProduct === false) {
            return min($product->getFinalPrice(), $product->getFinalPrice() * $fixedDiscount);
        }

        return min($product->getFinalPrice(), (float) $subcriptionPriceOnProduct);
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @param string $attributeCode
     * @return bool|string|array
     */
    public function getProductAttributeFromCode(Mage_Catalog_Model_Product $product, string $attributeCode)
    {
        return Mage::getResourceModel('catalog/product')
            ->getAttributeRawValue($product->getId(), $attributeCode, $product->getStoreId());
    }

    /**
     * @return float|int|void
     */
    public function getFixedDiscount()
    {
        return (1 - $this->fixedDiscount / 100);
    }

    /**
     * Get array of allowed payment methods
     *
     * @return array
     */
    public function getPaymentMethods()
    {
        $methods = explode(',', Mage::getStoreConfig('payment/reepay_standard/payment_methods'));

        if (Mage::getStoreConfigFlag('payment/reepay_standard/mobilepay')
            && (in_array('card', $methods) && count($methods) === 1)) {
            array_push($methods, 'mobilepay');

            return $methods;
        }

        return $methods;
    }

    /**
     * @return array
     */
    public function getSubscriptionPeriods(): array
    {
        /** @var Firtal_Reepay_Model_Reepay_Plan[] $plans */
        $plans = Mage::helper('firtal_reepay/reepay')->getPlans();
        $periods = [];

        /** @var Firtal_Reepay_Model_Reepay_Plan $plan */
        foreach ($plans as $plan) {
            $periods[] = new Firtal_Reepay_Model_DTO_Period($plan->getHandle(), $plan->getName());
        }

        return $periods;
    }

    /**
     * @param $planHandle
     * @return Firtal_Reepay_Model_DTO_Period
     */
    public function getSubscriptionPeriod($planHandle): Firtal_Reepay_Model_DTO_Period
    {
        foreach ($this->getSubscriptionPeriods() as $period) {
            if ($period->getId() === $planHandle) {
                return new Firtal_Reepay_Model_DTO_Period($period->getId(), $period->getTitle());
            }
        }
    }

    /**
     * @param $subscriptionHandle
     * @param $subscriptionAddOnHandle
     * @return Firtal_Reepay_Model_Reepay_Addon
     * @throws Exception
     */
    public function getProductSubscriptionFromAddOn(
        $subscriptionHandle,
        $subscriptionAddOnHandle
    ): Firtal_Reepay_Model_Reepay_Addon {
        try {
            $response = Mage::helper('firtal_reepay/client')->getJson("subscription/{$subscriptionHandle}/add_on/{$subscriptionAddOnHandle}");
        } catch (Exception $e) {
            Mage::logException($e);
            return null;
        }

        return new Firtal_Reepay_Model_Reepay_Addon($response);
    }

    /**
     * @param mixed $fixedDiscount
     */
    public function setFixedDiscount($fixedDiscount): void
    {
        $this->fixedDiscount = $fixedDiscount;
    }
}