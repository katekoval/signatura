<?php

class Firtal_Reepay_Helper_Customer extends Mage_Core_Helper_Abstract
{
    const ENDPOINT = 'customer';

    /**
     * Check if customer already exists
     *
     * @param $handle
     * @return bool
     * @throws Exception
     */
    public function newCustomer($handle)
    {
        $client = Mage::helper('firtal_reepay/client');

        try {
            $client->getJson(
                self::ENDPOINT, ['handle' => $handle]
            );
        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
            return false;
        }

        if ($client->success()) {
            return true;
        }

        return false;
    }

    /**
     * Getting the customer from the order
     *
     * @param Mage_Sales_Model_Order $order
     * @return array
     */
    public function getDetails(Mage_Sales_Model_Order $order)
    {
        if (!$order->getBillingAddress()) {
            return $this->_redirect('checkout/cart');
        }

        return [
            'handle' => $order->getCustomerId(),
            'email' => $order->getBillingAddress()->getEmail(),
            'first_name' => $order->getBillingAddress()->getFirstname(),
            'last_name' => $order->getBillingAddress()->getLastname(),
            'address' => $order->getBillingAddress()->getStreet(1),
            'address2' => $order->getBillingAddress()->getStreet(2),
            'city' => $order->getBillingAddress()->getCity(),
            'country' => $order->getBillingAddress()->getCountryId(),
            'phone' => $order->getBillingAddress()->getTelephone(),
            'company' => $order->getBillingAddress()->getCompany(),
            'postal_code' => $order->getBillingAddress()->getPostcode(),
            'vat' => $order->getBillingAddress()->getVatId()
        ];
    }

    /**
     * @param Mage_Customer_Model_Customer $customer
     * @return Varien_Object
     */
    public function getOrderAddress(Mage_Customer_Model_Customer $customer)
    {
        try {
            $info =  new Varien_Object([
                Mage::helper('firtal_reepay/client')->getJson(self::ENDPOINT . "/{$customer->getId()}")
            ]);

            Mage::log(json_encode(['$info' => $info->getData()], JSON_PRETTY_PRINT), null, 'test.log', true);
            return $info;
        } catch (Exception $e) {
            Mage::logException($e);
        }
    }
}