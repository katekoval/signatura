<?php

class Firtal_Reepay_Helper_Refund extends Mage_Core_Helper_Abstract
{
    const ENDPOINT = 'refund/';

    /** @var Firtal_Reepay_Helper_Client $this->_client */
    protected $_client;
    /** @var Firtal_Reepay_Helper_Api $this->_apiKey */
    protected $_apiKey;

    public function __construct()
    {
        $this->_apiKey = Mage::helper('firtal_reepay/api')->getPrivateKey();
        $this->_client = Mage::helper('firtal_reepay/client');
    }

    public function createRefund($handle, $amount)
    {
        /** @var Mage_Sales_Model_Order $order */
        if (!$order = Mage::getModel('sales/order')->loadByIncrementId($handle)) {
            Mage::getModel('adminhtml/session')->addError($this->__('No order was found with that increment id.'));
            $this->_redirectReferer();
        }

        try {
            $this->_client->post($this->_apiKey, self::ENDPOINT, ['invoice' => $handle, 'amount' => (int) $amount * 100]);
        } catch (\Exception $e) {
            Mage::getModel('adminhtml/session')->addError($e->getMessage());
            $this->_redirectReferer();
        }
    }
}