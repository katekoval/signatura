<?php

class Firtal_Reepay_Helper_Reepay extends Mage_Core_Helper_Abstract
{
    const SUBSCRIPTIONS_QUERY_SIZE = 100;
    const ADDON_STATE_DELETED = 'deleted';
    const ADDON_STATE_ACTIVE = 'active';

    /** @var Firtal_Reepay_Helper_Client */
    private $client;
    private $_apiKey;
    private $privateKey;

    /**
     * Firtal_Reepay_Helper_Reepay constructor.
     */
    public function __construct()
    {
        $this->client = Mage::helper('firtal_reepay/client');
        $this->_apiKey = Mage::helper('firtal_reepay/api')->getPrivateKey();
        $this->privateKey = Mage::helper('firtal_reepay/api')->getPrivateKey();
    }

    /**
     * Add-ons are Reepay's template for products attached to a subscription
     *
     * @param Mage_Catalog_Model_Product $product
     * @return bool|Firtal_Reepay_Model_Reepay_Addon
     */
    public function createAddon(Mage_Catalog_Model_Product $product)
    {
        Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
        Mage::log(json_encode(['Mage::helper(\'firtal_reepay\')->getLowestDiscountedPrice($product)' => Mage::helper('firtal_reepay')->getLowestDiscountedPrice($product)], JSON_PRETTY_PRINT), null, 'test.log', true);
        $amount = Mage::helper('firtal_reepay')->getLowestDiscountedPrice($product) * 100;

        /** @var $addon Firtal_Reepay_Model_Reepay_Addon */
        if ($addon = $this->getAddon($product->getId())) {
            $addon->setAmount($amount);
            $this->updateAddon($product, 'amount', $amount);

            return new Firtal_Reepay_Model_Reepay_Addon($addon);
        }

        Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
        try {
            $addon = $this->client->post(
                $this->_apiKey,
                'add_on', [
                    'name' => $product->getName(),
                    'amount' => (float) $amount,
                    'handle' => $product->getId(),
                    'type' => 'quantity',
                    'all_plans' => true
                ]
            );

            if (!$this->client->success()) {
                return false;
            }

            return new Firtal_Reepay_Model_Reepay_Addon($addon);
        } catch (Exception $e) {
            Mage::logException($e);
            return null;
        }
    }

    /**
     * Gets the plan response
     *
     * @param $plan
     * @return mixed
     */
    public function getPlan($plan)
    {
        try {
            $response = $this->client->getJson(
                "plan/{$plan}",
                ['only_active' => true]
            );

            return $response;
        } catch (Exception $e) {
            Mage::logException($e);
            return null;
        }
    }

    public function getPlans(): array
    {
        $response = $this->client->getJson(
            'plan?only_active=true'
        );

        $plans = [];

        foreach ($response as $plan) {
            $plans[] = new Firtal_Reepay_Model_Reepay_Plan($plan);
        }

        return $plans;
    }

    /**
     * @param $handle
     * @return bool|Firtal_Reepay_Model_Reepay_Addon
     */
    public function getAddon($handle)
    {
        try {
            $helper = Mage::helper('firtal_reepay/addon');
            $addon = $this->client->getJson("add_on/${handle}");

            if (array_key_exists('error', $addon)) {
                return false;
            }

            if ($addon['state'] === self::ADDON_STATE_DELETED) {
                $addon = $helper->undelete($addon['handle']);
            }

            return new Firtal_Reepay_Model_Reepay_Addon($addon);
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $customerHandle
     * @return array
     * @throws Exception
     */
    public function getSubscriptions($customerHandle): array
    {
        $response = Mage::helper('firtal_reepay/client')->getJson('subscription', [
            "size" => self::SUBSCRIPTIONS_QUERY_SIZE,
            "search" => "customer.handle:{$customerHandle},state:active,is_cancelled:false"
        ]);

        if ($response['count'] == 0) {
            return [];
        }

        $subscriptions = [];

        foreach ($response['content'] as $subscription) {
            $subscriptions[] = new Firtal_Reepay_Model_Reepay_Subscription($subscription);
        }

        return $subscriptions;
    }

    /**
     * @param Mage_Catalog_Model_product $product
     * @param string $attribute
     * @param $value
     * @return bool
     * @throws Exception
     */
    private function updateAddon(Mage_Catalog_Model_Product $product, string $attribute, $value): bool
    {
        $response = $this->client->put($this->_apiKey, "add_on/{$product->getId()}", [
            'handle' => $product->getId(),
            'name' => $product->getName(),
            'all_plans' => true,
            $attribute => $value
        ]);

        if (array_key_exists('error', $response)) {
            Mage::log(json_encode(['error in response' => $response], JSON_PRETTY_PRINT), null, 'test.log', true);
            return false;
        }

        Mage::log(json_encode(['$response' => $response], JSON_PRETTY_PRINT), null, 'test.log', true);
        return true;
    }
}
