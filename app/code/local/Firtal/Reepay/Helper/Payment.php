<?php

class Firtal_Reepay_Helper_Payment extends Mage_Core_Helper_Abstract
{
    /**
     * @param Mage_Sales_Model_Order_Payment $payment
     * @param $chargeObject
     * @param $state
     * @param $isTransactionClosed
     * @throws Exception
     */
    public function createTransactionOnOrder(Mage_Sales_Model_Order_Payment $payment, $chargeObject, $state, $isTransactionClosed): void
    {
        $transaction = $payment->addTransaction($state);
        $transaction->setAdditionalData(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS, $chargeObject['transaction']);
        $transaction->setTxnId($chargeObject['transaction']);
        $transaction->setIsClosed($isTransactionClosed);
        $transaction->save();
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @param $chargeObject
     * @return Mage_Sales_Model_Order_Payment
     * @throws Exception
     */
    public function setAdditionalInformationOnPayment(
        Mage_Sales_Model_Order $order,
        $chargeObject
    ): Mage_Sales_Model_Order_Payment {
        /** @var Mage_Sales_Model_Order_Payment $payment */
        $payment = $order->getPayment();
        $payment->setTransactionId($chargeObject['transaction']);
        $payment->setTransactionAdditionalInfo(Mage_Sales_Model_Order_Payment_Transaction::RAW_DETAILS, $chargeObject['transaction']);
        $payment->save();

        return $payment;
    }

    /**
     * @param $chargeObject
     * @return mixed
     * @throws Mage_Core_Exception
     */
    public function getMagentoStateForPayment($chargeObject)
    {
        Mage::log(json_encode(['$chargeObject' => $chargeObject], JSON_PRETTY_PRINT), null, 'test.log', true);

        switch ($chargeObject['state']) {
            case Firtal_Reepay_Model_Standard::AUTHORIZED:
                return [
                    'state' =>  Mage_Sales_Model_Order_Payment_Transaction::TYPE_AUTH,
                    'isTransactionClosed' => false
                ];
            case Firtal_Reepay_Model_Standard::SETTLED:
                return [
                    'state' =>  Mage_Sales_Model_Order_Payment_Transaction::TYPE_CAPTURE,
                    'isTransactionClosed' => true
                ];
            default:
                Mage::throwException('The transaction state cannot be mapped together with Reepay.');
                return null;
        }
    }

    /**
     * Logic on whether an item should be considered as a subscription product
     *
     * @param Mage_Sales_Model_Order_Item $item
     * @return bool
     */
    public function isItemRecurring(Mage_Sales_Model_Order_Item $item)
    {
        if ((bool)$item->getProductOptionByCode('info_buyRequest')['reepay_subscription']) {
            return true;
        }

        foreach ($item->getProductOptionByCode('additional_options') as $option) {
            if ($option['type'] === Firtal_Reepay_Model_Observer::REEPAY_SUBSCRIPTION_NAME_TO_CHECK_IN_CART) {
                return true;
            }
        }

        return false;
    }
}