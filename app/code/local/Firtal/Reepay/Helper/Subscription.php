<?php

class Firtal_Reepay_Helper_Subscription extends Mage_Core_Helper_Abstract
{
    const SUBSCRIPTION_ATTRIBUTE = 'rp_subscription_price';
    const EXCLUDE_PRODUCT_AS_SUBSCRITION_PRODUCT = 'rp_subscription_exlude_product';
    const PRODUCT_SUBSCRIPTION_TIMING_ON_RENEWAL = 'renewal';
    const PRODUCT_SUBSCRIPTION_TIMING_IMMEDIATE = 'immediate';
    const ENDPOINT = 'subscription';
    const BILLING = 'none';
    const SIGNUP_METHOD = 'source';
    const PARTIAL_PERIOD_HANDLING = 'no_bill';

    /**
     * @param $plan
     * @param Mage_Sales_Model_Order $order
     * @param $payment_source
     * @return Firtal_Reepay_Model_Reepay_Subscription|null
     * @throws Mage_Core_Exception
     */
    public function getSubscription($plan, Mage_Sales_Model_Order $order, $payment_source)
    {
        $client = Mage::helper('firtal_reepay/client');

        /** @var Zend_Date $startDate */
        $startDate = $this->getStartDate($order, $plan);

        try {
            $subscription = $client->post(
                Mage::helper('firtal_reepay/api')->getPrivateKey(),
                self::ENDPOINT, [
                    'generate_handle' => true,
                    'plan' => $plan,
                    'customer' => $order->getCustomerId(),
                    'source' => $payment_source,
                    'start_date' => $startDate->get('yyyy-MM-dd'),
                    'signup_method' => self::SIGNUP_METHOD
                ]
            );

            return new Firtal_Reepay_Model_Reepay_Subscription($subscription);
        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
            return null;
        }
    }

    /**
     * @param $handle
     * @return Firtal_Reepay_Model_Reepay_Subscription|null
     * @throws Mage_Core_Exception
     */
    public function getSubscriptionByHandle($handle): Firtal_Reepay_Model_Reepay_Subscription
    {
        try {
            $response = Mage::helper('firtal_reepay/client')->getJson(self::ENDPOINT . "/{$handle}");
            return new Firtal_Reepay_Model_Reepay_Subscription($response);
        } catch (Exception $e) {
            Mage::logException($e);
            return null;
        }
    }

    /**
     * Remving product subscriptin without compensation
     *
     * @param $handle
     * @param Firtal_Reepay_Model_DTO_ProductSubscription $product
     * @return bool|void
     */
    public function removeProductSubscription($handle, Firtal_Reepay_Model_DTO_ProductSubscription $product)
    {
        try {
            /** @var Firtal_Reepay_Helper_Client $response */
            $response = Mage::helper('firtal_reepay/client')->put(
                Mage::helper('firtal_reepay/api')->getPrivateKey(),
                self::ENDPOINT . "/{$handle}", [
                'handle' => $handle,
                'timing' => self::PRODUCT_SUBSCRIPTION_TIMING_IMMEDIATE,
                'partial_period_handling' => self::PARTIAL_PERIOD_HANDLING,
                'billing' => self::BILLING,
                'compensation_method' => self::BILLING,
                'remove_add_ons' => [
                    $product->getId()
                ]
            ]);

            if (array_key_exists('error', $response)) {
                Mage::getSingleton('core/session')->addError($this->__("%s was not cancelled. Contact customer service for help.", $product->getProduct()->getName()));
                return false;
            }

            Mage::getSingleton('core/session')->addSuccess($this->__("%s was successfully cancelled.", $product->getProduct()->getName()));
            return true;
        } catch (Exception $e) {
            Mage::logException($e);
            return null;
        }
    }

    public function reactivate($handle)
    {
        try {
            $response = Mage::helper('firtal_reepay/client')->post(
                Mage::helper('firtal_reepay/api')->getPrivateKey(),
                self::ENDPOINT . "/{$handle}/reactivate"
            );
        } catch (Exception $e) {
            Mage::logException($e);
            return null;
        }
    }

    /**
     * Changing the product subscription quantity without compensation.
     * @param $subscriptionData
     * @return null
     * @throws Exception
     */
    public function changeProductSubscriptionQty(array $subscriptionData)
    {
        $sub = [];
        $productSubscriptionId = $subscriptionData['productSubscriptionId'];
        $subscriptionId = $subscriptionData['subscriptionId'];

        if (!Mage::getSingleton('customer/session')->isLoggedIn()) {
            Mage::getSingleton('core/session')->addError('You\'re not logged in.');
            Mage::app()->getResponse()->setRedirect('/customer/account/login');
        };

        $customer = Mage::getSingleton('customer/session')->getCustomer();

        $subscriptions = Mage::getSingleton('firtal_reepay/repository_subscriptionRepository')->getSubscriptions($customer);

        /** @var Firtal_Reepay_Model_DTO_Subscription $sub */
        $sub = $this->getCorrespondingSubscription($subscriptions, $subscriptionId);

        /** @var Firtal_Reepay_Model_DTO_ProductSubscription $product */
        foreach ($sub->getProductSubscriptions() as $product) {
            if ($product->getId() == $productSubscriptionId) {
                Mage::getSingleton('firtal_reepay/repository_subscriptionRepository')->remove($subscriptionId,
                    $product);
                break;
            }
        }

        $addon = new Firtal_Reepay_Model_Reepay_Addon([
            'handle' => $productSubscriptionId,
            'add_on' => $productSubscriptionId,
            'quantity' => $subscriptionData['productSubscriptionQty']
        ]);

        try {
            /** @var Firtal_Reepay_Helper_Client $response */
            $response = Mage::helper('firtal_reepay/client')->put(
                Mage::helper('firtal_reepay/api')->getPrivateKey(),
                self::ENDPOINT . "/{$subscriptionId}", [
                    'handle' => $subscriptionId,
                    'timing' => self::PRODUCT_SUBSCRIPTION_TIMING_IMMEDIATE,
                    'billing' => 'none',
                    'partial_period_handling' => 'no_bill', // don't invoice when partially changing the subscription
                    'compensation_method' => 'none',
                    'add_ons' => [
                        $addon->getData()
                    ]
                ]
            );
        } catch (Exception $e) {
            Mage::throwException("Error occured while changing quantity to: {$subscriptionData['productSubscriptionQty']} on subscription {$subscriptionId} with subscription product: {$productSubscriptionId}{$e->getMessage()}");
            return null;
        }
    }

    public function getPendingSubscriptions(Mage_Customer_Model_Customer $customer): array
    {
        $repo = Mage::getSingleton('firtal_reepay/repository_subscriptionRepository');
        $subscriptions = $repo->getSubscriptions($customer);
    }

    /**
     * @param $handle
     * @return bool
     * @throws Mage_Core_Exception
     */
    public function cancelSubscription($handle): bool
    {
        try {
            $response = Mage::helper('firtal_reepay/client')->post(
                Mage::helper('firtal_reepay/api')->getPrivateKey(),
                self::ENDPOINT . "/{$handle}/cancel", [
                    'handle' => $handle
                ]
            );

            if ($response['is_cancelled'] == true && isset($response['expires'])) {
                return true;
            }

            return false;
        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
            return false;
        }
    }

    /**
     * @param array $subscriptions
     * @param $subscriptionId
     * @return mixed
     */
    private function getCorrespondingSubscription(array $subscriptions, $subscriptionId)
    {
        if (!$subscriptions || !$subscriptionId) {
            return;
        }

        /** @var Firtal_Reepay_Model_DTO_Subscription $subscription */
        foreach ($subscriptions as $subscription) {
            if ($subscription->getId() == $subscriptionId) {
                return $subscription;
            }
        }
    }

    /**
     * Gets the status for a specific subscription
     *
     * @param $handle
     * @return mixed
     */
    public function getStatus($handle)
    {
        try {
            return Mage::helper('firtal_reepay/client')->getJson(self::ENDPOINT . "/{$handle}")['state'];
        } catch (Exception $e) {
            Mage::logException($e);
            return null;
        }
    }

    private function getStartDate(Mage_Sales_Model_Order $order, $plan)
    {
        $createdAt = $order->getCreatedAtDate();

        $response = Mage::helper('firtal_reepay/reepay')->getPlan($plan)[0];

        switch ($response['schedule_type']) {
            case 'daily':
                $date = $createdAt->addDay((int)$response['interval_length']);
                return $date;
                break;
            case 'month_startdate':
                $date = $createdAt->addMonth((int)$response['interval_length']);
                return $date;
                break;
            case 'weekly_fixedday':
            case 'manual':
            case 'month_fixedday':
            case 'month_lastday':
                break;
            default:
                return null;
        }
    }
}