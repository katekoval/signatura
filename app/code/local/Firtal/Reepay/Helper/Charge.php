<?php

use Exception;

class Firtal_Reepay_Helper_Charge extends Mage_Core_Helper_Abstract
{
    const ENDPOINT = 'charge/';

    /** @var Firtal_Reepay_Helper_Client $_client */
    protected $_client;

    protected $_apiKey;

    public function __construct()
    {
        $this->_client = Mage::helper('firtal_reepay/client');
        $this->_apiKey = Mage::helper('firtal_reepay/api')->getPrivateKey();
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @return mixed
     * @throws Exception
     */
    public function getState(Mage_Sales_Model_Order $order)
    {
        try {
            $response = $this->_client->get($this->_apiKey, self::ENDPOINT . $order->getIncrementId());

            return $response['state'];
        } catch (Exception $e) {
           throw new Exception('response from ' . __CLASS__ . ':' . __METHOD__ . ' failed: ' . $e->getMessage());
        }

    }

    /**
     * @param $handle
     * @return mixed
     * @throws Exception
     */
    public function getChargeObject($handle)
    {
        try {
            $response = $this->_client->get($this->_apiKey, self::ENDPOINT . $handle);
            return $response;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function settle($handle, $amount)
    {
        try {
            $response = $this->_client->post($this->_apiKey, self::ENDPOINT . $handle . '/settle', ['amount' => $amount]);
            return $response;
        } catch (Exception $e) {
            Mage::logException($e);
            return null;
        }
    }
}