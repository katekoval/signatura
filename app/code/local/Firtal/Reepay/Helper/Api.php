<?php

class Firtal_Reepay_Helper_Api extends Mage_Core_Helper_Abstract
{
    /** @var string */
    private $_privateKey;

    public function __construct()
    {
        $this->_privateKey = Mage::getStoreConfig('payment/reepay_standard/private_key');
    }


    public function getPrivateKey()
    {
        return $this->_privateKey;
    }
}