<?php

class Firtal_Reepay_Helper_Validator extends Mage_Core_Helper_Abstract
{
    /**
     * Validate the state from the charge
     *
     * @param $state
     * @return bool
     */
    public function validate($state)
    {
        if ($state !== Firtal_Reepay_Model_Standard::AUTHORIZED) {
           return false;
        }

        return true;
    }
}
