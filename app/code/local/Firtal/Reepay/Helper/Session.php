<?php

class Firtal_Reepay_Helper_Session extends Mage_Core_Helper_Abstract
{
    CONST ENDPOINT = 'session/charge';
    CONST RECURRING_ENDPOINT = 'session/recurring';

    /** @var Firtal_Reepay_Helper_Client $_client */
    private $_client;

    /** @var Firtal_Reepay_Helper_Customer $_customer */
    private $_customer;

    /** @var string */
    private $_privateKey;

    public function __construct()
    {
        $this->_client = Mage::helper('firtal_reepay/client');
        $this->_customer = Mage::helper('firtal_reepay/customer');
        $this->_privateKey = Mage::getStoreConfig('payment/reepay_standard/private_key');
    }

    /**
     * Getting the Reepay Charge Sessinon Id
     *
     * @param Mage_Sales_Model_Order $order
     * @param bool $recurring
     * @return mixed
     * @throws Mage_Core_Exception
     */
    public function getChargeSessionId(Mage_Sales_Model_Order $order, $recurring = false)
    {
        Mage::log(json_encode(['$order fra getChargeSessionId() ' => $order->getData()], JSON_PRETTY_PRINT), null, 'test.log', true);

        try {
             $response = $this->_client->post($this->_privateKey, self::ENDPOINT, [
                'order' => [
                    'handle' => $order->getIncrementId(),
                    'amount' => $order->getBaseTotalDue() * 100,
                    'currency' => $order->getOrderCurrencyCode(),
                    'customer' => $this->_customer->getDetails($order),
                ],
                'metadata' => (new Varien_Object([
                    'order' => $order->getIncrementId()
                ]))->getData(),
                'recurring' => $recurring,
                'accept_url' => Mage::getUrl('reepay/standard/accept/order-' . $order['increment_id']),
                'cancel_url' => Mage::getUrl('reepay/standard/decline/order-'. $order['increment_id']),
                'payment_methods' => Mage::helper('firtal_reepay')->getPaymentMethods()
            ], true);

            Mage::log(json_encode([
                '$response fra getChargeSessionId' => $response,
                'increment_id' => $order->getIncrementId(),
                'customer_id' => $order->getCustomerId()
            ], JSON_PRETTY_PRINT), null, 'test.log', true);
            return $response;
        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
        }
    }

    /**
     * @param Mage_Sales_Model_Order $order
     * @param string $state
     * @throws Exception
     */
    public function setOrderStatus(Mage_Sales_Model_Order $order, string $state): void
    {
        $state = trim($state);

        if (!$orderStatus = Mage::getStoreConfig("payment/reepay_standard/{$state}")) {
            Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
            throw new Exception('No order state specified or unknown');
        }

        Mage::log(json_encode(['$order->getIncrementId()' => $order->getIncrementId(), 'which line' => __METHOD__ . ':' . __LINE__], JSON_PRETTY_PRINT), null, 'test.log', true);
        switch ($state) {
            case "order_status_before_payment":
                Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
                $magentoState = Mage_Sales_Model_Order::STATE_PROCESSING;

                if ($order->getState() === $magentoState) {
                    Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
                    return;
                }

                Mage::log(json_encode(['$order' => $order->getIncrementId(), '$state' => $state, '$status' => $orderStatus], JSON_PRETTY_PRINT), null, 'test.log', true);
                $order
                    ->setState($magentoState, $orderStatus ?: true)
                    ->save();
                return;
            case "order_status_after_payment":
                Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
                $magentoState = Mage_Sales_Model_Order::STATE_NEW;
                if ($order->getState() === $magentoState) {
                    Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
                    return;
                }
                
                Mage::log(json_encode(['$order' => $order->getIncrementId(), '$state' => $state, '$status' => $orderStatus], JSON_PRETTY_PRINT), null, 'test.log', true);
                $order
                    ->setState($magentoState, $orderStatus ?: true)
                    ->save();
                return;
            default:
                Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
                break;
        }
    }
}