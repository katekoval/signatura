<?php

class Firtal_Reepay_Helper_Invoice extends Mage_Core_Helper_Abstract
{
    CONST ENDPOINT = 'invoice';

    /** @var Firtal_Reepay_Helper_Client $_client */
    protected $_client;

    /** @var Firtal_Reepay_Helper_Api $_privateKey */
    private $_privateKey;

    public function __construct()
    {
        $this->_client = Mage::helper('firtal_reepay/client');
        $this->_privateKey = Mage::helper('firtal_reepay/api')->getPrivateKey();
    }

    /**
     * @param $handle
     * @return mixed|null
     * @throws Exception
     */
    public function getInvoice($handle)
    {
        try {
            $response = $this->_client->getJson(self::ENDPOINT . "/{$handle}");
            Mage::log(json_encode(['$response' => $response], JSON_PRETTY_PRINT), null, 'test.log', true);
            return $response;
        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
            return null;
        }
    }

    /**
     * @param mixed $handle
     * @param array $data
     * @return bool
     * @throws Mage_Core_Exception
     */
    public function setMetadata($handle, array $data): bool
    {
        try {
            $response = $this->_client->put(
                Mage::helper('firtal_reepay/api')->getPrivateKey(),
                self::ENDPOINT . "/{$handle}/metadata",
                (new Varien_Object($data))->getData()
        );
            Mage::log(json_encode(['$response' => $response], JSON_PRETTY_PRINT), null, 'test.log', true);
            return true;
        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
            return false;
        }
    }

    /**
     * @param $handle
     * @return bool|mixed|null
     * @throws Mage_Core_Exception
     */
    public function getOrderIncrementId($handle)
    {
        Mage::log(json_encode(['$handle for invoice' => $handle], JSON_PRETTY_PRINT), null, 'test.log', true);
        Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
        try {
            $response = $this->_client->get($this->_privateKey, self::ENDPOINT . "/{$handle}/metadata");
            Mage::log(json_encode(['$response metadata' => $response], JSON_PRETTY_PRINT), null, 'test.log', true);
            Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);

            if (array_key_exists('error', $response)) {
                Mage::log(__METHOD__.":".__LINE__, null, 'test.log', true);
                Mage::log(json_encode(['get order meta data error response' => $response['message']], JSON_PRETTY_PRINT), null, 'test.log', true);
                return false;
            }

            Mage::log(json_encode(['$response meta data order response' => $response], JSON_PRETTY_PRINT), null, 'test.log', true);
            return $response['order'];
        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
            return false;
        }
    }

    /**
     * @param $handle
     * @return array
     * @throws Mage_Core_Exception
     */
    public function getLastCreatedTransaction($handle)
    {
        try {
            $transaction = $this->_client->getJson(self::ENDPOINT . "/{$handle}/transaction", [
                'sort' => '-created'
            ]);

            Mage::log(json_encode(['transaction list sorted DESC' => $transaction], JSON_PRETTY_PRINT), null, 'test.log', true);
            return $transaction['content'][0];
        } catch (Exception $e) {
            Mage::throwException($e->getMessage());
            return null;
        }

    }
}