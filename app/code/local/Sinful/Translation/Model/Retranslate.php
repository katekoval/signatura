<?php

class Sinful_Translation_Model_Retranslate extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('translation/retranslate');
    }
}