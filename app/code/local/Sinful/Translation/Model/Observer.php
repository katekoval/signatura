<?php

class Sinful_Translation_Model_Observer
{


    public function identifyChange(Varien_Event_Observer $observer)
    {


        $currentUrl = Mage::app()->getRequest()->getOriginalPathInfo();
        $allowed = 'catalog_product/save';
        $product = $observer->getProduct();
        $sourceId = $product->getEntityId();
        $sourceStore = $product->getStoreId();
        $title = $product->getName();
        $type = 'product';
        $sku = $product->getSku();
        parse_str(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY), $queries);
        $needRetranslation = $queries['retranslation'];

        if ($needRetranslation == 1) {
            if (strpos($currentUrl, $allowed) !== false) {

                if ($sourceStore == 0) {

                    $keyField = ['name', 'meta_title', 'meta_description', 'description', 'product_details', 'videos'];
                    $allStores = Mage::app()->getStores();
                    $exceptionStores = array(0, 2, 7);

                    foreach ($allStores as $_eachStoreId) {

                        $storeCode = Mage::app()->getStore($_eachStoreId)->getId();
                        $currentWebsite = $storeCode - 1;
                        if (in_array($storeCode, $exceptionStores)) {
                            continue;
                        } else {
                            $targetProd = Mage::getModel('catalog/product')->setStore($storeCode)->setStoreId($storeCode)->load($sourceId);
                            if (in_array($currentWebsite, $targetProd->getWebsiteIds())) {
                                $targetStoreStatus = $targetProd->getStatus();
                                if ($targetStoreStatus == 1) {
                                    if ($product->hasDataChanges()) {
                                        $newValues = array_diff_assoc($product->getData(), $product->getOrigData());
                                        $oldValues = array_diff_assoc($product->getOrigData(), $product->getData());

                                        foreach ($keyField as $field) {
                                            if (!empty(($newValues[$field]))) {
                                                $change = mage::helper('sinful_translation')->getDiff($oldValues[$field], $newValues[$field]);
                                                mage::helper('sinful_translation')->flagForRetranslationProduct($sourceId, $sourceStore, $field, $title, $change, $type, $oldValues[$field], $newValues[$field], $sku, $storeCode);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }


    public function identifyCatChange(Varien_Event_Observer $observer){

        $currentUrl = Mage::app()->getRequest()->getOriginalPathInfo();
        $allowed = 'catalog_category/save';
        $category = $observer->getEvent()->getCategory();
        $sourceId = $category->getId();
        $sourceStore = $category->getStoreId();
        $title = $category->getName();
        $type = 'category';

        parse_str(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY), $queries);
        $needRetranslation = $queries['retranslation'];

        if($needRetranslation == 1) {
            if (strpos($currentUrl, $allowed) !== false) {

                if ($sourceStore == 0) {

                    $keyField = ['top_text', 'description', 'meta_title', 'meta_description', 'category_bottom_title', 'bottom_column1_title', 'bottom_column1_text', 'bottom_column2_title', 'bottom_column2_text', 'bottom_column3_title', 'bottom_column3_text'];

                    if ($category->hasDataChanges()) {

                        $newValues = array_diff_assoc($category->getData(), $category->getOrigData());
                        $oldValues = array_diff_assoc($category->getOrigData(), $category->getData());

                        foreach ($keyField as $field) {
                            if (!empty(($newValues[$field]))) {
                                $change = mage::helper('sinful_translation')->getDiff($oldValues[$field], $newValues[$field]);
                                mage::helper('sinful_translation')->flagForRetranslation($sourceId, $sourceStore, $field,$title, $change, $type, $oldValues[$field], $newValues[$field]);
                            }
                        }
                    }
                }
            }
        }

    }

    public function identifyCmsPageChange(Varien_Event_Observer $observer){

        $cmsPage = $observer->getEvent()->getObject();
        $sourceId = $cmsPage->getData('page_id');
        $sourceStore = $cmsPage->getData('stores')[0];
        $title = $cmsPage->getData('title');
        $type = 'cmspage';
        $defaultStoreId = 2;

        parse_str(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY), $queries);
        $needRetranslation = $queries['retranslation'];

        if($needRetranslation == 1) {

            if ($sourceStore == $defaultStoreId) {

                $keyField = ['title', 'meta_keywords', 'meta_description', 'content_heading', 'content'];

                $newValues = array_diff_assoc($cmsPage->getData(), $cmsPage->getOrigData());
                $oldValues = array_diff_assoc($cmsPage->getOrigData(), $cmsPage->getData());

                foreach ($keyField as $field) {
                    if (!empty(($newValues[$field]))) {
                        $change = mage::helper('sinful_translation')->getDiff($oldValues[$field], $newValues[$field]);
                        mage::helper('sinful_translation')->flagForRetranslation($sourceId, $sourceStore, $field,$title, $change, $type, $oldValues[$field], $newValues[$field]);
                    }
                }
            }
        }

    }

    public function identifyCmsBlockChange(Varien_Event_Observer $observer){


        $cmsBlock = $observer->getData('new_value');
        $sourceId    = $cmsBlock['block_id'];
        $sourceStore = $observer->getData('old_value')->getData('store_id')[0];
        $title = $cmsBlock['title'];
        $type = 'cmsblock';
        $defaultStoreId = 2;

        parse_str(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY), $queries);
        $needRetranslation = $queries['retranslation'];

        if($needRetranslation == 1) {

            if ($sourceStore == $defaultStoreId) {

                $keyField = ['title', 'content'];

                $newValues = array_diff_assoc($observer->getData('new_value'), $observer->getData('old_value')->getData());
                $oldValues = array_diff_assoc($observer->getData('old_value')->getData(), $observer->getData('new_value'));

                foreach ($keyField as $field) {
                    if (!empty(($newValues[$field]))) {
                        $change = mage::helper('sinful_translation')->getDiff($oldValues[$field], $newValues[$field]);
                        mage::helper('sinful_translation')->flagForRetranslation($sourceId, $sourceStore, $field,$title, $change, $type, $oldValues[$field], $newValues[$field]);
                    }
                }
            }
        }

    }



    # Used for grid action in reviews
    public function addMassAction($observer)
    {
        $block = $observer->getEvent()->getBlock();
        $this->_block = $block;

        #Add Mass Action for Review Grid
        if (get_class($block) == 'Mage_Adminhtml_Block_Widget_Grid_Massaction' && $block->getRequest()->getControllerName() == 'catalog_product_review') {
            $block->addItem('massreview', array(
                'label' => Mage::helper('sales')->__('Flag for Translation'),
                'url' => $block->getUrl('*/review/massreview'),
            ));
        }

    }

}

