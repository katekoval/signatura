<?php

class Sinful_Translation_Model_Translator extends Mage_Core_Model_Abstract
{
    protected $_type;
    protected $_store;

    protected function _construct()
    {
        $this->_init('translation/translator');
        date_default_timezone_set('Europe/Copenhagen');
    }

    public function initStatic($type = null, $store_id = null)
    {
        $this->_type = $type;
        $this->_store = $store_id;
    }

    public function canTranslate()
    {
        if($this->getCurrentTranslatorModel()->getCurrentEntity()){
            return true;
        }
        return false;
    }

    public function getRange()
    {
        $range = explode('-',Mage::app()->getRequest()->getParam('range'));

        if(!isset($range[1])) {
            $range[1] = 0;
        }

        return array(
            'from' => $range[0],
            'to' => $range[1],
        );
    }

    public function getTranslatorModel($translator_type)
    {
        if ($translator_type) {
            return Mage::getSingleton('translation/translators_'.$translator_type);
        }

        return false;
    }

    public function getCurrentTranslatorModel()
    {
        return $this->getTranslatorModel($this->getTranslatorType());
    }

    public function isStoreSynced()
    {
        $syns = Mage::getModel('translation/sync')->getStoreSyncMappings();
        if(array_search($this->getTranslatorStoreId(),$syns)){
           return true;
        }
        else{
            return false;
        }
    }

    public function getSyncedStore()
    {
        $syns = Mage::getModel('translation/sync')->getStoreSyncMappings();
        foreach($syns as $from => $to){
            if($to == $this->getTranslatorStoreId()){
                return $from;
            }
        }
    }

    public function getTranslators()
    {
        /*$syns = Mage::getModel('translation/sync')->getStoreSyncMappings();

        if(array_search($this->getTranslatorStoreId(),$syns)){
            return array(
                'block',
                'email'

            );
        }*/

        return array(
            'product',
            'review',
            'category',
           /* 'block',*/
            'cms',
           /* 'cmsblock',*/
            'attribute',
          /*  'strings',*/
            'email',

        );
    }

    public function getTranslatorType(){
        if(isset($this->_type)){
            return $this->_type;
        }
        return Mage::app()->getRequest()->getParam('type');
    }

    public function getTranslatorStoreId(){
        if(isset($this->_store)){
            return $this->_store;
        }
        return Mage::app()->getRequest()->getParam('storeid');
    }

    public function getCurrentEntityData()
    {
        return $this->getCurrentTranslatorModel()->getCurrentEntityData();
    }

    public function getTranslationFields()
    {
        return $this->getCurrentTranslatorModel()->getTranslationFields();
    }

    public function getPreviousEntities()
    {
        return $this->getCurrentTranslatorModel()->getPreviousEntities();
    }

    public function getEntitiesLeft()
    {
        return $this->getCurrentTranslatorModel()->getEntitiesLeft();
    }


    public function getTranslatorOverview()
    {
        return $this->getCurrentTranslatorModel()->getOverview();
    }

    public function getTranslatorProductapprove()
    {
        return $this->getCurrentTranslatorModel()->getProductsToApprove();
    }

    public function getTranslatorOverviewForMe()
    {
        return $this->getCurrentTranslatorModel()->getOverviewForMe();
    }

    public function getProductsToAssign()
    {
        return $this->getCurrentTranslatorModel()->getProductsToAssign();
    }

    public function checkSave()
    {
        if($this->getTranslatorType() && $_SERVER['REQUEST_METHOD'] === 'POST'){
            $this->getCurrentTranslatorModel()->saveTranslation();
            $this->saveTimeTracking();
        }
    }

    public function saveTimeTracking()
    {
        Mage::getModel('translation/timetracking')
            ->setStoreId($this->getTranslatorStoreId())
            ->setDate(date('Y-m-d H:i:s'))
            ->setType($this->getTranslatorType())
            ->setEntityId($_POST['entity_id'])
            ->setUser(Mage::getSingleton('admin/session')->getUser()->getUserId())
            ->setTime($_POST['timetracking'])
            ->save();
    }

    public function formatUrlKey($str)
    {
        $urlKey = preg_replace('#[^0-9a-z]+#i', '-', Mage::helper('catalog/product_url')->format($str));
        $urlKey = strtolower($urlKey);
        $urlKey = trim($urlKey, '-');

        return $urlKey;
    }

    public function googletranslate($string){
        // $store = $this->getTranslatorStoreId();
        // $source_lang = 'en';

        // if($store == 2){
        //     $new_lang = '';
        // } else if($store == 7){
        //     $new_lang = '';
        // } else{
        //     $new_lang = '';
        // }

        // $apiKey = 'AIzaSyBQ-nhDIh0bZSWdTcu4rzSRuPBmVnZ0y4E';

        // $url = 'https://www.googleapis.com/language/translate/v2?key=' . $apiKey . '&q=' . rawurlencode($string) . '&source='.$source_lang.'&target='.$new_lang.'&format=text';

        // $handle = curl_init($url);
        // curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        // $response = curl_exec($handle);
        // $responseDecoded = json_decode($response, true);
        // curl_close($handle);

        // $rawtext = $responseDecoded['data']['translations'][0]['translatedText'];

        // //HTML corrections to google api
        // $rawtext = str_replace("</ li> ","</li>",$rawtext);
        // $rawtext = str_replace("</ LI> ","</li>",$rawtext);
        // $rawtext = str_replace("<Li> ","<li>",$rawtext);
        // $rawtext = str_replace("</ Li>","</li>",$rawtext);
        // $rawtext = str_replace("</ li>","</li>",$rawtext);
        // $rawtext = str_replace("</ ul> ","</ul>",$rawtext);
        // $rawtext = str_replace("</ Ul> ","</ul>",$rawtext);
        // $rawtext = str_replace("</ UL>","</ul>",$rawtext);
        // $rawtext = str_replace("</ UL> ","</ul>",$rawtext);
        // $rawtext = str_replace("</ Ul>","</ul>",$rawtext);
        // $rawtext = str_replace("<Ul>","<ul>",$rawtext);
        // $rawtext = str_replace("</ U>","</U>",$rawtext);
        // $rawtext = str_replace("</ b> ","</b>",$rawtext);
        // $rawtext = str_replace("</ B> ","</b>",$rawtext);
        // $rawtext = str_replace("<B> ","<b>",$rawtext);
        // $rawtext = str_replace("</ a>","</a>",$rawtext);
        // $rawtext = str_replace("</ div>","</div>",$rawtext);
        // $rawtext = str_replace("</ div>","</div>",$rawtext);
        // $rawtext = str_replace("</ strong>","</strong>",$rawtext);
        // $rawtext = str_replace("</ b>","</b>",$rawtext);
        // $rawtext = str_replace(" </ h2>","</h2>",$rawtext);
        // $rawtext = str_replace("<H2> ","<h2>",$rawtext);
        // $rawtext = str_replace(" </ h3>","</h3>",$rawtext);
        // $rawtext = str_replace("<H3> ","<h3>",$rawtext);
        // $rawtext = str_replace("<h2> ","<h2>",$rawtext);

        //return $rawtext;

        return $string;
    }

    public function pushstatus(){
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);


        /** PUSH STATUS ABOUT MISSING */

        $total_count = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('visibility', array('in' => array(
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_SEARCH,
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
            )))
            ->joinField('stock_status','cataloginventory/stock_status','stock_status',
                'product_id=entity_id', array(
                    'stock_status' => Mage_CatalogInventory_Model_Stock_Status::STATUS_IN_STOCK,
                    'website_id' => 1,
                ))
            ->getSize();

        $keys = array(
            11 => '86670-5a6dfae0-cda1-0133-d3a0-22000bca46d2',

        );


        $sync_stores = Mage::getModel('translation/sync')->getStoreSyncMappings();
        foreach(Mage::app()->getStores() as $_eachStoreId => $val){
            if(in_array($_eachStoreId,$sync_stores)){
                continue;
            }

            $count = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect('*')
                ->setStoreId($_eachStoreId)
                ->addAttributeToFilter('status', array('eq' => 1))
                ->addAttributeToFilter('visibility', array('in' => array(
                    Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_SEARCH,
                    Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
                )))
                ->joinField('stock_status','cataloginventory/stock_status','stock_status',
                    'product_id=entity_id', array(
                        'stock_status' => Mage_CatalogInventory_Model_Stock_Status::STATUS_IN_STOCK,
                        'website_id' => 1,
                    ))
                ->getSize();

            $storeid = $_eachStoreId;
            $data = array(
                'item' =>  $count,
                'min' => array(
                    'value' => 0
                ),
                'max' => array(
                    'value' => $total_count
                ),
            );
            var_dump($data);

            //Push data
            //$url = 'https://hooks.zapier.com/hooks/catch/68175/5p1ewf/?status='.$count.'&store='. $storeid;
            var_dump($url);
            $finalarray = array(
                'api_key' => $apikey,
                'data' => $data
            );

            $data_string = json_encode($finalarray);

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
            );
            $result = curl_exec($ch);
        }

        /** PUSH STATUS ABOUT YESTODAY */


        $keys = array(
            11 => '86670-58326b90-cf32-0133-f866-22000b560299',
        );

        $from = date("Y-m-d", strtotime("yesterday"));
        $to = date("Y-m-d", strtotime("today"));

        $weekdays = array(
            1 =>  date("Y-m-d", strtotime('-1 day')),
            2 =>  date("Y-m-d", strtotime('-2 days')),
            3 =>  date("Y-m-d", strtotime('-3 days')),
            4 =>  date("Y-m-d", strtotime('-4 days')),
            5 =>  date("Y-m-d", strtotime('-5 days')),
            6 =>  date("Y-m-d", strtotime('-6 days')),
            7 =>  date("Y-m-d", strtotime('-7 days')),
        );


        foreach(Mage::app()->getStores() as $_eachStoreId => $val) {
            if (in_array($_eachStoreId, $sync_stores)) {
                continue;
            }

            $count = Mage::getModel('translation/timetracking')->getCollection()
                ->addFieldToFilter('date', array('from' => $from, 'to'=> $to))
                ->addFieldToFilter('type', 'product')
                ->addFieldToFilter('store_id', $_eachStoreId)
                ->addFieldToFilter('time', array('neq' => 0))
                ->getSize();

            $days = [];
            foreach($weekdays as $index => $day){
                if($index != 7){
                    $days[] = Mage::getModel('translation/timetracking')->getCollection()
                        ->addFieldToFilter('date', array('from' => $weekdays[$index+1], 'to'=> $day))
                        ->addFieldToFilter('type', 'product')
                        ->addFieldToFilter('store_id', $_eachStoreId)
                        ->addFieldToFilter('time', array('neq' => 0))
                        ->getSize();
                }
            }
            $days = array_reverse($days);

            $data = array(
                'item' => array(
                    array(
                        'value' => $count
                    ),
                    array(
                        $days[0],
                        $days[1],
                        $days[2],
                        $days[3],
                        $days[4],
                        $days[5],
                        $count,
                    )
                )
            );

            //Push data
            $url = 'https://hook.integromat.com/6tkokqnfd7fsmdx73psostplutsqx4t4';
        //    $apikey = 'db3d1bb9e4cd25e9919bf1195defe920';

            $finalarray = array(
                'api_key' => $apikey,
                'data' => $data
            );

            $data_string = json_encode($finalarray);

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($data_string))
            );
            $result = curl_exec($ch);
            //echo var_dump($result).'<br><br>';
        }
    }
}