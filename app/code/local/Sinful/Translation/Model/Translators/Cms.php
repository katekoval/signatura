<?php

class Sinful_Translation_Model_Translators_Cms extends Sinful_Translation_Model_Translator
{
    protected $current_entity;

    public function getCurrentEntity(){


        if(isset($this->current_entity)){
            return $this->current_entity;
        }


        $defaultStore = Mage::app()->getWebsite()->getDefaultGroup()->getDefaultStoreId();

        if(Mage::app()->getRequest()->getParam('retrans')){

            $entity_id = Mage::app()->getRequest()->getParam('load_entity_id');
            $originalData = Mage::getModel('cms/page')->load($entity_id);

            $missing = Mage::getModel('cms/page')->getCollection()
                ->addStoreFilter(parent::getTranslatorStoreId())
                ->addFieldToFilter('sinful_identifier', $originalData->getSinfulIdentifier())
                ->getFirstItem();

            $this->current_entity = $missing;

        }elseif($entity_id = Mage::app()->getRequest()->getParam('load_entity_id')){
            $missing = Mage::getModel('cms/page')->load($entity_id);
            $this->current_entity = $missing;
        }elseif($entity_id = Mage::app()->getRequest()->getParam('load_identifier')){
            $missing = Mage::getModel('cms/page')->getCollection()
                ->addStoreFilter(parent::getTranslatorStoreId(), false)
                ->addFieldToFilter('sinful_identifier',$entity_id)
                ->getFirstItem();
            $this->current_entity = $missing;
        } else{
            $missing = Mage::getModel('cms/page')->getCollection()
                ->addStoreFilter($defaultStore)
                ->addFieldToFilter('sinful_identifier', array('nin' => Mage::getModel('cms/page')->getCollection()->addStoreFilter(parent::getTranslatorStoreId())->getColumnValues('sinful_identifier')))
                ->addFieldToFilter('is_active', 1)
                ->getFirstItem();
        }

        if($missing->getPageId() == ''){
            return false;
        } elseif(Mage::app()->getRequest()->getParam('load_entity_id') || Mage::app()->getRequest()->getParam('load_identifier')){
            return $missing;
        }
        else{
            return false;
        }


    }

    public function getCurrentEntityData()
    {

        $missing = $this->getCurrentEntity();

        $original = Mage::getModel('cms/page')->getCollection()
            ->addStoreFilter(2)
            ->addFieldToFilter('sinful_identifier', $this->getCurrentEntity()->getSinfulIdentifier())
            ->getFirstItem();

        $target = Mage::getModel('cms/page')->getCollection()
            ->addStoreFilter(parent::getTranslatorStoreId())
            ->addFieldToFilter('sinful_identifier', $this->getCurrentEntity()->getSinfulIdentifier())
            ->getFirstItem();

        $baseStoreUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        $destination_url =  Mage::app()->getStore(parent::getTranslatorStoreId())->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);

        $data =  array(
            'entity_id'       => $missing->getPageId(),
            'entity_title'    => $missing->getTitle(),
            'entity_metakeyword' => $missing->getMetaKeywords(),
            'entity_metadescription' => $missing->getMetaDescription(),
            'entity_identifier' =>  $missing->getIdentifier(),
            'url' => $baseStoreUrl.$original->getData('identifier'),
            'url_new' => $destination_url.$target->getData('identifier'),
            'entity_sinfulidentifier' =>  $missing->getSinfulIdentifier(),
            'entity_content' => $missing->getContent()
        );


        return $data;

    }

    public function getEntitiesLeft()
    {
        $pending = Mage::getModel('cms/page')->getCollection()
            ->addStoreFilter(parent::getTranslatorStoreId(), false)
            #->addFieldToFilter('sinful_identifier', array('neq' => 'NULL'))
            ->addFieldToFilter('proofreading', 1)
            ->addFieldToFilter('is_active', 0);

        $missing = Mage::getModel('cms/page')->getCollection()
            ->addStoreFilter(2, false)
            ->addFieldToFilter('is_active', 1)
            #->addFieldToFilter('sinful_identifier', array('neq' => 'NULL'))
            ->addFieldToFilter('sinful_identifier', array('nin' => Mage::getModel('cms/page')->getCollection()->addStoreFilter(parent::getTranslatorStoreId())->getColumnValues('sinful_identifier')))
            ->addFieldToFilter('main_table.page_id', array('nin' => $pending->getColumnValues('page_id')))
            ->setOrder('proofreading', 'DESC');

        $retransEntities = mage::helper('sinful_translation')->getRetranslationEntities('cmspage', parent::getTranslatorStoreId());

        $data['total'] = (int)$missing->getSize() + (int)$pending->getSize() + count($retransEntities);

        return $data;
    }

    public function getPreviousEntities()
    {

        //Gets the default store
        $default_store = Mage::app()->getWebsite()->getDefaultGroup()->getDefaultStoreId();



        return $data;
    }

    public function getTranslationFields(){



        $retranslate = Mage::app()->getRequest()->getParam('retrans');

        $fields = [];

        if($retranslate == 1){

            $field_data  = mage::helper('sinful_translation')->getRetranslationData(Mage::app()->getRequest()->getParam('load_entity_id'), 'cmspage', parent::getTranslatorStoreId());
            foreach($field_data as $retrans){
                $field_names[] = $retrans['field'];

            }

            $value_data = $this->getFieldValues($field_names);

            $fields[] = array(
                'type' => 'hidden',
                'name' => 'page_id',
                'orig_value' => $field_data[0]['source_id'],
                'trans_value' => $field_data[0]['source_id'],
            );

            $fields[] = array(
                'type' => 'hidden',
                'name' => 'retrans',
                'retrans' => 'true',
                'orig_value' => 1,
                'trans_value' => 1
            );


            foreach($field_data as $retrans){

                $fields[] = array(
                    'type' => 'text',
                    'name' => $retrans['field'],
                    'old_value' => $retrans['old_value'],
                    'new_value' => $retrans['new_value'],
                    'change' => $retrans['change'],
                    'orig_value' => $value_data[$retrans['field']]['change'],
                    'trans_value' => $value_data[$retrans['field']]['trans_value'],
                    'desc' => mage::helper('sinful_translation')->getFieldDescription($retrans['field'], 'cms'),
                    'p-br' => mage::helper('sinful_translation')->formattingPBr($retrans['field']),
                    'br-p' => mage::helper('sinful_translation')->formattingBrp($retrans['field']),
                );
            }

            $fields[] = array(
                'type' => 'select',
                'desc' => 'If update is done, set to "Yes"',
                'name' => 'update_done',
                'update_done' => 'true',
                'orig_value' => array('0' => 'No', '1' => 'Yes'),
                'trans_value' => array('0' => 'No', '1' => 'Yes'),
            );




        }else {

            $field_names = array('page_id', 'title', 'meta_keywords', 'meta_description', 'identifier', 'content', 'root_template', 'layout_update_xml', 'custom_theme', 'custom_root_template', 'sinful_identifier');

            $value_data = $this->getFieldValues($field_names);

            $fields = [];

            $fields[] = array(
                'type' => 'hidden',
                'name' => 'page_id',
                'orig_value' => $value_data['page_id']['orig_value'],
                'trans_value' => $value_data['page_id']['orig_value'],
            );

            $fields[] = array(
                'type' => 'hidden',
                'name' => 'root_template',
                'orig_value' => $value_data['root_template']['orig_value'],
                'trans_value' => $value_data['root_template']['trans_value'],
            );

            $fields[] = array(
                'type' => 'hidden',
                'name' => 'layout_update_xml',
                'orig_value' => $value_data['layout_update_xml']['orig_value'],
                'trans_value' => $value_data['layout_update_xml']['trans_value'],
            );

            $fields[] = array(
                'type' => 'hidden',
                'name' => 'custom_theme',
                'orig_value' => $value_data['custom_theme']['orig_value'],
                'trans_value' => $value_data['custom_theme']['trans_value'],
            );

            $fields[] = array(
                'type' => 'hidden',
                'name' => 'custom_root_template',
                'orig_value' => $value_data['custom_root_template']['orig_value'],
                'trans_value' => $value_data['custom_root_template']['trans_value'],
            );

            $fields[] = array(
                'type' => 'hidden',
                'name' => 'sinful_identifier',
                'orig_value' => $value_data['sinful_identifier']['orig_value'],
                'trans_value' => $value_data['sinful_identifier']['trans_value']
            );

            $fields[] = array(
                'type' => 'text',
                'name' => 'title',
                'orig_value' => $value_data['title']['orig_value'],
                'trans_value' => $value_data['title']['trans_value'],
                'desc' => mage::helper('sinful_translation')->getFieldDescription('title', 'cms')
            );

            $fields[] = array(
                'type' => 'text',
                'name' => 'meta_description',
                'orig_value' => $value_data['meta_description']['orig_value'],
                'trans_value' => $value_data['meta_description']['trans_value'],
                'desc' => mage::helper('sinful_translation')->getFieldDescription('meta_description', 'cms')
            );

            $fields[] = array(
                'type' => 'text',
                'name' => 'identifier',
                'orig_value' => $value_data['identifier']['orig_value'],
                'trans_value' => $value_data['identifier']['trans_value'],
                'desc' => mage::helper('sinful_translation')->getFieldDescription('identifier', 'cms')
            );

            $fields[] = array(
                'type' => 'text',
                'name' => 'content',
                'orig_value' => $value_data['content']['orig_value'],
                'trans_value' => $value_data['content']['trans_value'],
                'desc' => mage::helper('sinful_translation')->getFieldDescription('content', 'cms'),
                'p-br' => mage::helper('sinful_translation')->formattingPBr('content')
            );

            $fields[] = array(
                'type' => 'select',
                'desc' => 'Once translation has been proofread and can be activated, set to "Active"',
                'name' => 'is_done',
                'isDone' => 'true',
                'data' => $value_data['status']
            );

        }

        return $fields;
    }

    public function getFieldValues($fields){

        $missing = $this->getCurrentEntity();
        $use_dest_data = false;
        $missing_data = $missing;

        if($missing_data){
            $identifier = $missing_data->getSinfulIdentifier();
        }

        if($identifier){
            $dest_review_data = Mage::getModel('cms/page')->getCollection()
                ->addStoreFilter(parent::getTranslatorStoreId())
                ->addFieldToFilter('sinful_identifier', $identifier)
                ->getFirstItem()->getData();
        }

        if($dest_review_data){
            $use_dest_data = true;
            $missing_data = Mage::getModel('cms/page')->getCollection()
                ->addStoreFilter(2)
                ->addFieldToFilter('sinful_identifier', $identifier)
                ->getFirstItem()->getData();

            if(empty($missing_data)){
                $missing_data = $missing;
            }
        }


        $data = [];

        if($use_dest_data)
        {

            foreach($fields as $field){
                $data[$field] = array(
                    'orig_value' => $missing_data[$field],
                    'trans_value' => $dest_review_data[$field],
                );
            }
        } else {

            foreach ($fields as $field) {

                $data[$field] = array(
                    'orig_value' => $missing_data[$field],
                    'trans_value' => ($missing_data[$field]),
                );
            }
        }

        return $data;
    }

    public function saveTranslation()
    {


        # Globals
        $destinationStore = parent::getTranslatorStoreId();
        $retranslate      = $_POST['retrans'];

        $cmsPageExists    = Mage::getModel('cms/page')->getCollection()
            ->addStoreFilter(parent::getTranslatorStoreId(), false)
            ->addFieldToFilter('sinful_identifier', $_POST['sinful_identifier'])
            ->getFirstItem();


        if($_POST['meta_keywords']){
            $_POST['meta_keywords'] = mage::helper('sinful_translation')->htmlentitytag($_POST['meta_keywords']);
        }
        if($_POST['meta_description']){
            $_POST['meta_description'] = mage::helper('sinful_translation')->htmlentitytag($_POST['meta_description']);
        }

        if($retranslate == 1){

            $identifier =  Mage::getModel('cms/page')->load($_POST['page_id'])->getData('sinful_identifier');
            $newCmsPage = Mage::getModel('cms/page')->getCollection()
                ->addStoreFilter(parent::getTranslatorStoreId(), false)
                ->addFieldToFilter('sinful_identifier', $identifier)
                ->getFirstItem();

            $originalId = $_POST['page_id'];
            $_POST['page_id'] = $newCmsPage->getPageId();

            $newCmsPage->setStores(array($destinationStore));
            $newCmsPage->setData(array_merge($newCmsPage->getData(), $_POST));//$data);

            if($_POST['update_done'] == 1){
                mage::helper('sinful_translation')->removeFromRetranslation($originalId, 'cmspage', parent::getTranslatorStoreId(), Mage::getSingleton('admin/session')->getUser()->getData('username'), 1);
                # Save KPI on entity retranslation
                mage::helper('sinful_translation')->saveKpi($_POST['page_id'], parent::getTranslatorStoreId(), 'cms', 1, $newCmsPage->getTitle());
            }
        }elseif($cmsPageExists->getPageId()){
            $newCmsPage = $cmsPageExists;
            $newCmsPage->setStores(array($destinationStore));
            $newCmsPage->setRootTemplate($_POST['root_template'])
                ->setLayoutUpdateXml($_POST['layout_update_xml'])
                ->setCustomTheme($_POST['custom_theme'])
                ->setCustomRootTemplate($_POST['custom_root_template'])
                ->setTitle($_POST['title'])
                ->setMetaKeywords($_POST['meta_keywords'])
                ->setMetaDescription($_POST['meta_description'])
                ->setIdentifier($_POST['identifier'])
                ->setSinfulIdentifier($_POST['sinful_identifier'])
                ->setContent($_POST['content']);

        } else{
            $newCmsPage = Mage::getModel('cms/page')->setStores(array(parent::getTranslatorStoreId()))
                ->setRootTemplate($_POST['root_template'])
                ->setLayoutUpdateXml($_POST['layout_update_xml'])
                ->setCustomTheme($_POST['custom_theme'])
                ->setCustomRootTemplate($_POST['custom_root_template'])
                ->setTitle($_POST['title'])
                ->setMetaKeywords($_POST['meta_keywords'])
                ->setMetaDescription($_POST['meta_description'])
                ->setIdentifier($_POST['identifier'])
                ->setSinfulIdentifier($_POST['sinful_identifier'])
                ->setContent($_POST['content']);
        }

        if($_POST['is_done'] == 'isDone'){
            $newCmsPage->setData('is_active',1);
            $newCmsPage->setData('proofreading',0);
            # Save KPI on entity activation
            mage::helper('sinful_translation')->saveKpi($_POST['page_id'], parent::getTranslatorStoreId(), 'cms', 0, $newCmsPage->getTitle());
        }
        else{
            $newCmsPage->setIsActive(0);
        }

        if($_POST['proofread'] === "isDone") {
            $newCmsPage->setProofreading(1);
        }elseif($_POST['proofread'] === "notDone"){
            $newCmsPage->setProofreading(0);
        }



        try{
            $newCmsPage->save();
            #Run Changelog
            $this->saveChangelog($newCmsPage->getTitle());
            Mage::getSingleton('core/session')->addSuccess(ucfirst(parent::getTranslatorType()).' saved');
        }catch(Exception $e){
            Mage::getSingleton('core/session')->addError($e->getMessage());
        }

    }





    public function getOverview()
    {

        $data = array();

        $pending = Mage::getModel('cms/page')->getCollection()
            ->addStoreFilter(parent::getTranslatorStoreId(), false)
            #->addFieldToFilter('sinful_identifier', array('neq' => 'NULL'))
            ->addFieldToFilter('proofreading', 1)
            ->addFieldToFilter('is_active', 0);

        $missing = Mage::getModel('cms/page')->getCollection()
            ->addStoreFilter(2, false)
            ->addFieldToFilter('is_active', 1)
           # ->addFieldToFilter('sinful_identifier', array('neq' => 'NULL'))
            ->addFieldToFilter('sinful_identifier', array('nin' => Mage::getModel('cms/page')->getCollection()->addStoreFilter(parent::getTranslatorStoreId())->getColumnValues('sinful_identifier')))
            ->addFieldToFilter('page_id', array('nin' => $pending->getColumnValues('page_id')))
            ->setOrder('proofreading', 'DESC');

        if($pending->getData()){
            $missing->addFieldToFilter('sinful_identifier', array('nin' => $pending->getColumnValues('sinful_identifier')));
        }



        $retransEntities = mage::helper('sinful_translation')->getRetranslationEntities('cmspage', parent::getTranslatorStoreId());

        foreach($retransEntities as $retrans){

            $data[] = array(
                'id' => $retrans['source_id'],
                'info' => '<p><strong>'. $retrans['title'] . '</strong></p> ',
                'retranslation' => 'yes',
            );

        }

        foreach($pending as $post){

            $data[] = array(
                'id' => $post->getPageId(),
                'info' => '<p><strong>' . $post->getTitle().'</strong></p> ',
                'proofreading' => 'yes',
            );

        }

        foreach($missing as $post){

            $data[] = array(
                'id' => $post->getPageId(),
                'info' => '<p><strong>' . $post->getTitle().'</strong></p> ',
            );

        }

        return $data;
    }



    public function aggregateFlagReview($sourceId, $destinationStore, $type){

        $missing = Mage::getModel('translation/missing')
            ->getCollection()
            ->addFieldToFilter('source_id', $sourceId)
            ->addFieldToFilter('type', $type)
            ->getFirstItem();

        if($missing){

            $array = explode(",",$missing->getTargetStore());
            $array[] .= $destinationStore;
            $targetString = implode(",", $array);

            $missing->setTargetStore($targetString);

            $missing->save();

        }

    }

    public function saveChangelog($humanReadable){

        $entityId    = $_POST['page_id'];
        $storeId     = Mage::app()->getWebsite()->getDefaultGroup()->getDefaultStoreId();
        $targetStore = parent::getTranslatorStoreId();
        $user        = Mage::getSingleton('admin/session')->getUser()->getData('username');
        $type        = 'cms';

        $fieldsForChangeLog = $this->getTranslationFields();

        foreach($fieldsForChangeLog as $logItem){
            mage::helper('sinful_translation')->saveChangelog($entityId, $storeId, $targetStore, $user, $type, $logItem['name'], $logItem['trans_value'], $logItem['trans_value'],$humanReadable);
        }
    }
}