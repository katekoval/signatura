<?php

class Sinful_Translation_Model_Translators_Product extends Sinful_Translation_Model_Translator
{
    protected $current_entity;

    public function getCurrentEntity()
    {

        if (isset($this->current_entity)) {
            return $this->current_entity;
        }
        $destinationStore = parent::getTranslatorStoreId();
        $range = parent::getRange();
        $defaultStore = Mage::app()->getWebsite()->getDefaultGroup()->getDefaultStoreId();

        if ($entity_id = Mage::app()->getRequest()->getParam('load_entity_id')) {
            $product = Mage::getModel('catalog/product')->load($entity_id);
            $this->current_entity = $product;
        } elseif ($entity_id = Mage::app()->getRequest()->getParam('load_sku_id')) {
            $product = Mage::getModel('catalog/product')->loadByAttribute('sku', $entity_id);
            $this->current_entity = $product;
        } else {
            //Inactive get all products from current store
            $activeInTargetStore = Mage::getResourceModel('catalog/product_collection')
                ->addStoreFilter(parent::getTranslatorStoreId())
                ->addAttributeToFilter('status', array('eq' => 1))
                ->getColumnValues('entity_id');

            $product = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect('*')
                //->addStoreFilter($destinationStore)
                ->addStoreFilter($defaultStore)
                ->addAttributeToFilter('entity_id', array('lt' => (int)$range['to'] + 1))
                ->addAttributeToFilter('entity_id', array('gt' => (int)$range['from'] - 1))
                ->setOrder('entity_id', 'ASC')
                ->addAttributeToFilter('status', array('eq' => 1))
                ->addAttributeToFilter('visibility', array('in' => array(
                    Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_SEARCH,
                    Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
                )))
                ->addAttributeToSort('branch_to')
                ->addAttributeToFilter('branch_to', array($this->getBranchToStore($destinationStore)))
                ->getFirstItem();

            $this->current_entity = $product;
        }

        if ($product->getId() == '') {
            return false;
        } else {
            return $product;
        }


    }

    public function getCurrentEntityData()
    {

        $product = $this->getCurrentEntity();
        $img_product = $product;
        $product = Mage::getModel('catalog/product')->load($product->getId());
        $hasImg = $product->getImage();
        $baseStoreUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);

        $destinationStore = parent::getTranslatorStoreId();
        $productTranslation = Mage::getModel('catalog/product')->setStoreId($destinationStore)->load($product->getId());
        $destination_url = Mage::app()->getStore($destinationStore)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);

        $images = [];
        $count = 0;
        foreach ($img_product->getMediaGallery()['images'] as $_image) {
            $count++;
            if ($count == 1) continue;
            $images[] = array(
                'image_small' => (string)Mage::helper("catalog/image")->init($img_product, "small_image", $_image['file'])->resize(182),
                'image_big' => (string)Mage::helper("catalog/image")->init($img_product, "image", $_image['file'])->resize(1600),
            );
        }

        $mediaArray = "";

        if ($hasImg == "no_selection") {
            $mediaArray = array(
                'entity_id' => $product->getId(),
                'entity_sku' => $product->getSku(),
                'entity_name' => $product->getName(),
                'entity_type' => $product->getTypeId(),
                'url' => $baseStoreUrl . $product->getUrlKey(),
                'url_new' => $destination_url . $productTranslation->getUrlKey(),
                'entity_more_images' => $images,
            );
        } else {
            $mediaArray = array(
                'entity_id' => $product->getId(),
                'entity_sku' => $product->getSku(),
                'entity_name' => $product->getName(),
                'entity_type' => $product->getTypeId(),
                'url' => $baseStoreUrl . $product->getUrlKey(),
                'url_new' => $destination_url . $productTranslation->getUrlKey(),
                'entity_more_images' => $images,
            );
        }

        return $mediaArray;

    }

    public function getEntitiesLeft()
    {

        //Gets the default store
        $defaultStore = Mage::app()->getWebsite()->getDefaultGroup()->getDefaultStoreId();

        //Merge arrays for exclude statement in translated
        $totalActive = array_merge($this->getActiveInStore(), $this->getProofReadingInStore()->getColumnValues('entity_id'));
        $totalActive = array_merge($totalActive, $this->getPendingInStore()->getColumnValues('entity_id'));

        //Translated gets all products that are NOT in current store, and missing translation
        $translated = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('sku', 'name', 'status', 'entity_id')
            ->setStoreId($defaultStore)
            ->addStoreFilter($defaultStore)
            ->setOrder('name', 'ASC')
            ->addAttributeToFilter('status', array('eq' => 1))
            ->addAttributeToFilter('entity_id', array('nin' => $totalActive))
            ->addAttributeToFilter('visibility', array('in' => array(
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_SEARCH,
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
            )));

        $retransEntities = mage::helper('sinful_translation')->getRetranslationEntities('product', parent::getTranslatorStoreId());

        $data = [];

        $data['total'] = $translated->getSize() + count($retransEntities) + count($this->getProofReadingInStore()->getColumnValues('entity_id'));

        if ($range = parent::getRange()) {

            $data['range'] = Mage::getResourceModel('catalog/product_collection')
                ->addAttributeToSelect('*')
                //->setStoreId(parent::getTranslatorStoreId())
                ->setStoreId($defaultStore)
                ->addAttributeToFilter('entity_id', array('lt' => (int)$range['to'] + 1))
                ->addAttributeToFilter('entity_id', array('gt' => (int)$range['from'] - 1))
                ->setOrder('entity_id', 'ASC')
                ->addAttributeToFilter('status', array('eq' => 1))
                ->addAttributeToFilter('entity_id', array('nin' => $totalActive))
                ->addAttributeToFilter('visibility', array('in' => array(
                    Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_SEARCH,
                    Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
                )))
                ->joinField('stock_status', 'cataloginventory/stock_status', 'stock_status',
                    'product_id=entity_id', array(
                        'stock_status' => Mage_CatalogInventory_Model_Stock_Status::STATUS_IN_STOCK,
                        'website_id' => 1,
                    ))->getSize();

        } else {
            $data['range'] = 'Not using range';

        }

        return $data;
    }

    public function getPreviousEntities()
    {

        //Inactive get all products from current store
        $activeInTargetStore = Mage::getResourceModel('catalog/product_collection')
            ->addStoreFilter(parent::getTranslatorStoreId())
            ->addAttributeToFilter('status', array('eq' => 1))
            ->getColumnValues('entity_id');


        $range = parent::getRange();
        $latest = Mage::getResourceModel('catalog/product_collection')
            ->addStoreFilter(parent::getTranslatorStoreId())
            //->addStoreFilter($defaultStore)
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('entity_id', array('lt' => (int)$this->getCurrentEntity()->getId()))
            ->addAttributeToFilter('entity_id', array('gt' => (int)$range['from'] - 1))
            ->setOrder('entity_id', 'DESC')
            ->addAttributeToFilter('status', array('eq' => 1))
            ->addAttributeToFilter('entity_id', array('nin' => $activeInTargetStore))
            ->addAttributeToFilter('visibility', array('in' => array(
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_SEARCH,
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
            )))
            ->setPageSize(5);

        $data = array();

        foreach ($latest as $product) {
            $data[] = array(
                'entity_id' => $product->getId(),
                'name' => $product->getName()
            );
        }

        return $data;
    }

    /**
     * @param $entityId
     * @param $storeId
     * @return mixed
     */
    public function getCurrentStatus($entityId, $storeId)
    {
        $status = Mage::getModel('translation/item')->getCollection()
            ->addFieldToFilter('parent_id', $entityId)
            ->addFieldToFilter('store_id', $storeId)
            ->getFirstItem()
            ->getStatus();

        return $status;
    }

    /**
     * @param $entityId
     * @param $storeId
     * @return mixed
     */
    public function getAssignedTo($entityId, $storeId)
    {
        $assigned_user = Mage::getModel('translation/item')->getCollection()
            ->addFieldToFilter('parent_id', $entityId)
            ->addFieldToFilter('store_id', $storeId)
            ->getFirstItem()
            ->getAssigned();

        return $assigned_user;
    }

    public function getTranslationFields(){

    $fields = [];

        $field_names = array('entity_id','name','short_description', 'description','meta_title', 'meta_description', 'is_active', 'is_done', 'proofread', 'status');
        $value_data = $this->getFieldValues($field_names);

        $fields[] = array(
            'type' => 'hidden',
            'name' => 'entity_id',
            'orig_value' => $value_data['entity_id']['orig_value'],
            'trans_value' => $value_data['entity_id']['trans_value'],
        );

        $fields[] = array(
            'type' => 'text',
            'name' => 'name',
            'orig_value' => $value_data['name']['orig_value'],
            'trans_value' => $value_data['name']['trans_value'],
            'desc' => mage::helper('sinful_translation')->getFieldDescription('name', 'product')
        );

        $fields[] = array(
            'type' => 'textarea',
            'name' => 'short_description',
            'orig_value' => $value_data['short_description']['orig_value'],
            'trans_value' => $value_data['short_description']['trans_value'],
            'desc' => mage::helper('sinful_translation')->getFieldDescription('short_description', 'product'),
            'p-br' => mage::helper('sinful_translation')->formattingPBr('short_description')
        );

        $fields[] = array(
            'type' => 'textarea',
            'name' => 'description',
            'orig_value' => $value_data['description']['orig_value'],
            'trans_value' => $value_data['description']['trans_value'],
            'desc' => mage::helper('sinful_translation')->getFieldDescription('description', 'product'),
            'p-br' => mage::helper('sinful_translation')->formattingPBr('description')
        );

        $fields[] = array(
            'type' => 'text',
            'name' => 'meta_title',
            'orig_value' => $value_data['meta_title']['orig_value'],
            'trans_value' => $value_data['meta_title']['trans_value'],
            'desc' => mage::helper('sinful_translation')->getFieldDescription('meta_title', 'product')
        );

        $fields[] = array(
            'type' => 'textarea',
            'name' => 'meta_description',
            'orig_value' => $value_data['meta_description']['orig_value'],
            'trans_value' => $value_data['meta_description']['trans_value'],
            'desc' => mage::helper('sinful_translation')->getFieldDescription('meta_description', 'product'),
            'p-br' => mage::helper('sinful_translation')->formattingPBr('meta_description')
        );

        $fields[] = array(
            'type' => 'select',
            'desc' => 'Status',
            'name' => 'branch_status',
            'data' => $value_data['branch_status'],
            'values' => $this->getBranchStatuses(),
        );

        return $fields;
    }

    private function stripListString($string)
    {
        $string = str_replace(array("\r", "\n"), '', $string);
        $string = str_replace(array("</li>"), PHP_EOL, $string);
        $string = ltrim($string);
        $string = strip_tags($string,'<strong>');
        $string = preg_replace('/\n$/','',$string);
        return $string;
    }

    public function getFieldValues($fields){

        $product = $this->getCurrentEntity();
        $use_dest_data = true;

        if(empty($product)){
            return false;
        } else{
            $product_data = Mage::getModel('catalog/product')->load($product->getId())->getData();
            $dest_product_data = Mage::getModel('catalog/product')->setStoreId(parent::getTranslatorStoreId())->load($product->getId())->getData();

        }

        $data = [];

        foreach($fields as $field){
                $data[$field] = array(
                    'orig_value' => $product_data[$field],
                    'trans_value' => $dest_product_data[$field],
                );

        }

        return $data;
    }

    public function saveTranslation()
    {
        # Globals
        $destinationStore = parent::getTranslatorStoreId();
        $retranslate =   $_POST['retrans'];

        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        $_product = Mage::getModel('catalog/product')->load($_POST['entity_id']);
        $_product->setStoreId($destinationStore);
        $availInStore = $_product->getWebsiteIds();
        $currentWebsiteId = Mage::getModel('core/store')->load($destinationStore)->getWebsiteId();
        $availInStore[] = $currentWebsiteId;
        $isDone     = $_POST['is_done'];
        $proofread  = $_POST['proofread'];

        if($_POST['meta_title']){
            $_POST['meta_title'] = mage::helper('sinful_translation')->htmlentitytag($_POST['meta_title']);
        }
        if($_POST['meta_description']){
            $_POST['meta_description'] = mage::helper('sinful_translation')->htmlentitytag($_POST['meta_description']);
        }
        if($_POST['name']){
            $_POST['name'] = mage::helper('sinful_translation')->htmlentitytag($_POST['name']);
        }


        /**
         * Save status start
         */
        $status = Mage::app()->getRequest()->getParam('branch_status');

        $translationItem = Mage::getModel('translation/item')->getCollection()
            ->addFieldToFilter('parent_id', $_POST['entity_id'])
            ->addFieldToFilter('store_id', $destinationStore)
            ->getFirstItem();

        $translationItem->setStatus($status)->save();
        /**
         * Save status stop
         */

        $_product->setData(array_merge($_product->getData(), $_POST));//$data);
        $_product->setShortDescription($_POST['short_description']);
        $_product->setStoreId($destinationStore);
        if($_product->getStatus() == 2){
            $_product->setUrl_key(parent::formatUrlKey($_POST['name']));
        }

        $assigned_to = Mage::getModel('translation/translators_product')->getAssignedTo($_POST['entity_id'], $destinationStore);

        if(!empty($assigned_to))
        {
            $_product->setTranslator($assigned_to);
        } else {
            $_product->setTranslator(Mage::getSingleton('admin/session')->getUser()->getUsername());
        }

        if($status == "complete"){
            $_product->setStatus(1);
            $_product->setWebsiteIds($availInStore);
            $_product->setPendingTranslation(0);
            $this->_enableUnderProducts($_POST['entity_id'], $destinationStore);

            # Save KPI on product activation
            mage::helper('sinful_translation')->saveKpi($_POST['entity_id'], parent::getTranslatorStoreId(), 'product', 0, $_product->getSku());
        }elseif($status != "complete"){
            $_product->setStatus(2);
        }

        $_product->save();

        if($_POST['name']){
            $this->_resetName($_POST['name'], $_POST['entity_id']);
        }

        if($status == "complete"){
            $this->_defaultImages($_POST['entity_id'], $destinationStore, $_POST['name']);
        }

        #Run Changelog
        $this->saveChangelog($_product->getSku());

        Mage::getSingleton('core/session')->addSuccess(ucfirst(parent::getTranslatorType()).' saved');

    }

    public function _setPendingProduct($entityId, $destinationStore, $availInStore){

        try{
            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
            $_product = Mage::getModel('catalog/product')->setStoreId($destinationStore)->load($entityId);
            $_product->setWebsiteIds($availInStore);
            $_product->setStatus(2);
            $_product->setProofReading(0);
            $_product->setPendingTranslation(1);
            $_product->save();
            mage::log("Product " . $_product->getSku() . " was set for pending translation in store " . $destinationStore, null, "pendingtranslation.log", true);
        } catch (Error $ex) {
            mage::log($ex->getMessage(), null, "pendingtranslation.log", true);
        }


    }


    public function _defaultImages($entityId, $destinationStore, $imageLabel){

        try{
            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
            $_product = Mage::getModel('catalog/product')->load($entityId);
            $_product->setStoreId($destinationStore)->setThumbnail(false);
            $_product->setStoreId($destinationStore)->setSmallImage(false);
            $_product->setStoreId($destinationStore)->setImage(false);
            foreach($_product->getData('media_gallery') as $each){
                foreach($each as $image){
                    $attributes = $_product->getTypeInstance(true)->getSetAttributes($_product);
                    $attributes['media_gallery']->getBackend()->updateImage($_product, $image['file'], array('label' => $imageLabel));
                }
            }
            $_product->save();
            mage::log("Set default images on product " . $entityId . " on storeID: " . $destinationStore, null, "translation.log", true);
        } catch (Error $ex) {
            mage::log($ex->getMessage(), null, "translation.log", true);
        }


    }

    public function _resetName($name, $id){
        $destinationStore = parent::getTranslatorStoreId();
        $product = Mage::getModel('catalog/product')->setStoreId($destinationStore)->load($id);
        $product->setName($name);
        # Set URL key to name, using formatUrlKey as formatting
        if($product->getStatus() == 2){
            $product->setUrlKey(parent::formatUrlKey($name));
        }
        $product->save();
    }

    public function _enableUnderProducts($entityId, $destinationStore){


        $childProducts = Mage::getModel('catalog/product_type_configurable')
            ->getChildrenIds($entityId);

        if(!empty($childProducts)){

            $formattedArray = $childProducts[0];

            foreach($formattedArray as $child){

                try {
                    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
                    $_product = Mage::getModel('catalog/product')->load($child);
                    $_product->setStoreId($destinationStore);
                    $availInStore = $_product->getWebsiteIds();
                    $currentWebsiteId = Mage::getModel('core/store')->load($destinationStore)->getWebsiteId();
                    $availInStore[] = $currentWebsiteId;
                    $_product->setStatus(1);
                    $_product->setWebsiteIds($availInStore);
                    $_product->save();
                    mage::log("Saved underproduct on " . $entityId . ". Child product ID is: " . $child . " on storeID: " . $destinationStore, null, "translation.log", true);
                } catch (Error $ex) {
                    mage::log($ex->getMessage(), null, "translation.log", true);
                }
            }

        }

    }

    public function getRedirect(){
        $destinationStore = parent::getTranslatorStoreId();
        $test = Mage::helper("adminhtml")->getUrl("sinfultranslation/adminhtml_translator/overview", array('storeid' => $destinationStore, 'type' => $this->getTranslatorType()));
        echo "<script>window.location.href='$test';</script>";
    }

    public function getOverview()
    {
        //Global array init
        $data = [];

        //Gets the default store
        $defaultStore = Mage::app()->getWebsite()->getDefaultGroup()->getDefaultStoreId();
        //Merge arrays for exclude statement in translated
        $totalActive = array_merge($this->getActiveInStore(), $this->getProofReadingInStore()->getColumnValues('entity_id'));
        //Get Pending in current store
        $pendingTranslation = $this->getPendingInStore();
        $totalActive = array_merge($totalActive, $this->getPendingInStore()->getColumnValues('entity_id'));
        //Translated gets all products that are NOT in default store, and missing translation
        $translated = $this->getNeedTranslation($defaultStore, $totalActive);

        foreach($translated as $prod){
            $data[] = array(
                'id' => $prod->getId(),
                'info' => '<p><strong>' . $prod->getSku() . ' - ' . $prod->getName() . '</strong></p> ',
            );
        }

        return $data;

    }

    public function getProductsToApprove()
    {
        //Global array init
        $data = [];

        $user = Mage::getSingleton('admin/session')->getUser()->getUsername();

        $product = Mage::getModel('translation/item')->getCollection()
            ->addFieldToFilter('store_id', parent::getTranslatorStoreId())
            ->addFieldToFilter('status', 'waiting_for_approval');

        foreach($product as $prod) {
            $product = Mage::getSingleton('catalog/product')->load($prod->getParentId());

            $data[] = array(
                'id' => $prod->getParentId(),
                'info' => '<p><strong>' . $product->getName() . '</strong></p>',
                'potential' => $product->getBranchPotential(),
            );
        }

        usort($data, function($a, $b) {
            return $a['potential'] < $b['potential'];
        });

        return $data;
    }

    public function getOverviewForMe()
    {
        //Global array init
        $data = [];

        $user = Mage::getSingleton('admin/session')->getUser()->getUsername();

        $product = Mage::getModel('translation/item')->getCollection()
            ->addFieldToFilter('store_id', parent::getTranslatorStoreId())
            ->addFieldToFilter('assigned', $user)
            ->addFieldToFilter('status', 'incomplete');

        foreach($product as $prod) {
            $product = Mage::getSingleton('catalog/product')->load($prod->getParentId());

            $data[] = array(
                'id' => $prod->getParentId(),
                'info' => '<p><strong>' . $product->getName() . '</strong></p>',
                'potential' => $product->getBranchPotential(),
                'barcode'   => $product->getBarcode(),
            );
        }

        usort($data, function($a, $b) {
            return $a['potential'] < $b['potential'];
        });

        return $data;
    }

    public function getActiveInStore() {
        //Gets all active get all products from current store
        $activeInTargetStore = Mage::getResourceModel('catalog/product_collection')
            ->addStoreFilter(parent::getTranslatorStoreId())
            ->addAttributeToFilter('status', array('eq' => 1))
            ->getColumnValues('entity_id');

        return $activeInTargetStore;
    }

    public function getNeedTranslation($defaultStore, $totalActive)
    {
        $destinationStore = parent::getTranslatorStoreId();

        //Translated gets all products that are NOT in current store, and missing translation
        $translated = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect(array('sku', 'name', 'status', 'entity_id', 'branch_to'))
            ->setStoreId($defaultStore)
            ->addStoreFilter($defaultStore)
            ->addAttributeToFilter('visibility', array('in' => array(
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_SEARCH,
                Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
            )))
            ->addAttributeToFilter("branch_to", array(
                "finset" => $this->getBranchToStore($destinationStore)
            ))
            ->setOrder('name', 'ASC');

        return $translated;
    }


    public function getProofReadingInStore(){

        //Gets all inactive product from target store, that haves translanslation prior and/or proofreading
        $needsProofReadingInTargetStore = mage::getResourceModel('catalog/product_collection')
            ->setStoreId(parent::getTranslatorStoreId())
            ->addStoreFilter(parent::getTranslatorStoreId())
            ->addAttributeToSelect('sku', 'name', 'status', 'entity_id')
            ->setOrder('name', 'ASC')
            ->addFieldToFilter('status', 2);

        return $needsProofReadingInTargetStore;
    }


    public function getPendingInStore(){

        //Pending in store
        $pendingInStore = mage::helper('sinful_translation')->getEntityByAttribute(183, parent::getTranslatorStoreId(), 1);
        $pendingInStore = array_column($pendingInStore, 'entity_id');

        $pendingInStore = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('sku', 'name', 'status', 'entity_id')
            ->addStoreFilter(parent::getTranslatorStoreId())
            ->addAttributeToFilter('entity_id', array('in' => $pendingInStore))
            ->setOrder('name', 'ASC');

        return $pendingInStore;
    }


    public function saveChangelog($humanReadable){

        $entityId    = $_POST['entity_id'];
        $storeId     = Mage::app()->getWebsite()->getDefaultGroup()->getDefaultStoreId();
        $targetStore = parent::getTranslatorStoreId();
        $user        = Mage::getSingleton('admin/session')->getUser()->getData('username');
        $type        = 'product';

        $fieldsForChangeLog = $this->getTranslationFields();

        foreach($fieldsForChangeLog as $logItem){
            mage::helper('sinful_translation')->saveChangelog($entityId, $storeId, $targetStore, $user, $type, $logItem['name'], $logItem['trans_value'], $logItem['trans_value'],$humanReadable);
        }
    }

    public function getProductsToAssign()
    {
        $destinationStore = parent::getTranslatorStoreId();

        /**
         * Get all completed items for this store ID
         */
        $completed = Mage::getModel('translation/item')->getCollection()
            ->addFieldToFilter('status', array('eq' => 'complete'))
            ->addFieldToFilter('store_id', $destinationStore);

        $ignoreCompletedList = array();

        // Add all completed items to an ignore list
        foreach($completed as $status) {
            $ignoreCompletedList[] = $status->getParentId();
        }

        //Translated gets all products that are NOT in current store, and missing translation
        $products = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('*')
            ->addAttributeToFilter("branch_to", array(
                    "finset" => $this->getBranchToStore($destinationStore))
            );

        if(count($ignoreCompletedList) > 0) {
            $products->addAttributeToFilter("entity_id", array('nin' => $ignoreCompletedList)); //Ignore all completed items
        }

        /**
         * Join left sinful_translation_item to get assigned and status
         */
        $products->getSelect()->joinLeft(
            array('assigned' => 'sinful_translation_item'),
            'assigned.parent_id=e.entity_id AND assigned.store_id=' . $destinationStore,
            array(
                'assigned',
                'assigned_status' => 'status',
            ));

        return $products;
    }

    public function saveProductAssignment($productIds, $username, $storeId)
    {
        foreach($productIds as $productId)
        {
            $product = Mage::getModel('translation/item')->getCollection()
                ->addFieldToFilter('parent_id', $productId)
                ->addFieldToFilter('store_id', $storeId)
                ->getFirstItem();

            if(!$product->getId())
            {
                $product = Mage::getModel('translation/item')
                    ->setParentId($productId)
                    ->setStoreId($storeId);
            }

            /**
             * Set status to incomplete if not waiting for approval or complete
             */
            if($product->getStatus() != 'waiting_for_approval' AND $product->getStatus() != 'complete') {
                $product->setStatus('incomplete');
            }

            $product->setAssigned($username)
                ->save();
        }

        return true;
    }


    /**
     * @param $storeId
     * @return bool
     * @throws Mage_Core_Model_Store_Exception
     *
     * Gets branch value based on attribute to match from store id
     */
    private function getBranchToStore($storeId)
    {
        $attribute = Mage::getSingleton('eav/config')
            ->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'branch_to');

        if ($attribute->usesSource()) {
            $storeName = Mage::getModel('core/store')->load($storeId)->getName();
            $options = $attribute->getSource()->getAllOptions(false);

            foreach($options as $option)
            {
                if(strtolower($option['label']) == strtolower($storeName))
                {
                    return (int)$option['value'];
                }
            }
        }

        return false;
    }

    private function getBranchStatuses()
    {
        $statuses = array();

        $attribute = Mage::getSingleton('eav/config')
            ->getAttribute(Mage_Catalog_Model_Product::ENTITY, 'branch_status');

        if ($attribute->usesSource()) {
            $options = $attribute->getSource()->getAllOptions(false);

            foreach($options as $option)
            {
                $status = array(
                    'value' => $option['value'],
                    'label' => $option['label'],
                );

                array_push($statuses, $status);
            }

        }

        return $statuses;
    }

}