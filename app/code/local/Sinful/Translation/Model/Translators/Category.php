<?php

class Sinful_Translation_Model_Translators_Category extends Sinful_Translation_Model_Translator
{
    public function getCurrentEntity(){
        $destinationStore = parent::getTranslatorStoreId();
        $range = parent::getRange();
        $rootcatID = Mage::app()->getStore($destinationStore)->getRootCategoryId();
        if($entity_id = Mage::app()->getRequest()->getParam('load_entity_id')){
            $category = Mage::getModel('catalog/category')->load($entity_id);
        } else{
            $category = Mage::getResourceModel('catalog/category_collection')
                ->addAttributeToSelect('*')
                ->setStoreId($destinationStore)
                ->addAttributeToFilter('entity_id',array('nin'=>array(1,2)))
                ->addAttributeToFilter('entity_id', array('lt' => (int)$range['to']+1))
                ->addAttributeToFilter('entity_id', array('gt' => (int)$range['from']-1))
                ->setOrder('entity_id', 'ASC')
                ->getFirstItem();

        }

        if($category->getId() == ''){
            return false;
        } else{
            return $category;
        }
    }

    public function getCurrentEntityData()
    {
        $category = $this->getCurrentEntity();
        $category =  Mage::getModel('catalog/category')->load($category->getId());
        $baseStoreUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        $image_path = '/media/sinful/translation/';

        $destinationStore = parent::getTranslatorStoreId();
        $productTranslation = Mage::getModel('catalog/category')->setStoreId($destinationStore)->load($category->getId());
        $destination_url =  Mage::app()->getStore($destinationStore)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);

        $urlFormatOriginal = str_replace($baseStoreUrl.'index.php/',"",$category->getUrl());
        $urlFormatTranslated = str_replace($baseStoreUrl.'index.php/',"",$productTranslation->getUrl());

        $images = [];
        $images[] = array(
            'image_small' => $image_path.'top-text.jpg',
            'image_big' => $image_path.'top-text.jpg',
        );
        $images[] = array(
            'image_small' => $image_path.'description.jpg',
            'image_big' => $image_path.'description.jpg',
        );
        $images[] = array(
            'image_small' => $image_path.'description-bottom.jpg',
            'image_big' => $image_path.'description-bottom.jpg',
        );

        return array(
            'entity_id' => $category->getId(),
            'entity_name' => $category->getName(),
            'url' => $baseStoreUrl.$urlFormatOriginal,
            'url_new' => $destination_url.$urlFormatTranslated,
            'entity_image' => array(
                'small' => $image_path.'name.jpg',
                'large' => $image_path.'name.jpg',
            ),
            'entity_more_images' => $images,
        );
    }

    public function getEntitiesLeft()
    {
        $data = [];
        $destinationStore = parent::getTranslatorStoreId();
        $total =  Mage::getResourceModel('catalog/category_collection')
            ->setStoreId(parent::getTranslatorStoreId())
            ->setOrder('entity_id', 'ASC')
            ->getSize();


        $retransEntities = mage::helper('sinful_translation')->getRetranslationEntities('category', parent::getTranslatorStoreId());
        $data['total'] = $total + count($retransEntities);


        if($range = parent::getRange()){
            $data['range'] =  Mage::getResourceModel('catalog/category_collection')
                ->setStoreId(parent::getTranslatorStoreId())
                ->setOrder('entity_id', 'ASC')
                ->addAttributeToFilter('entity_id', array('lt' => (int)$range['to']+1))
                ->addAttributeToFilter('entity_id', array('gt' => (int)$range['from']-1))
                ->getSize();
        } else{
            $data['range'] = 'Not using range';
        }

        return $data;
    }

    public function getPreviousEntities()
    {
        $range = parent::getRange();
        $latest = Mage::getResourceModel('catalog/category_collection')
            ->setStoreId(parent::getTranslatorStoreId())
            ->addAttributeToSelect('entity_id', 'name')
            ->addAttributeToFilter('entity_id', array('nin' => array(1,2)))
            ->addAttributeToFilter('entity_id', array('lt' => (int)$this->getCurrentEntity()->getId()))
            ->addAttributeToFilter('entity_id', array('gt' => (int)$range['from']-1))
            ->setOrder('entity_id', 'DESC')
            ->setPageSize(5);

        $data = array();

        foreach($latest as $product){
            $data[] = array(
                'entity_id' => $product->getId(),
                'name' => $product->getName()
            );
        }

        return $data;
    }

    public function getTranslationFields(){

        $retranslate = Mage::app()->getRequest()->getParam('retrans');

        $fields = [];

        if($retranslate == 1){

            $field_data  = mage::helper('sinful_translation')->getRetranslationData(Mage::app()->getRequest()->getParam('load_entity_id'), 'category', parent::getTranslatorStoreId());
            foreach($field_data as $retrans){
                $field_names[] = $retrans['field'];

            }

            $value_data = $this->getFieldValues($field_names);

            $fields[] = array(
                'type' => 'hidden',
                'name' => 'entity_id',
                'orig_value' => $field_data[0]['source_id'],
                'trans_value' => $field_data[0]['source_id'],
            );

            $fields[] = array(
                'type' => 'hidden',
                'name' => 'retrans',
                'retrans' => 'true',
                'orig_value' => 1,
                'trans_value' => 1
            );


            foreach($field_data as $retrans){

                $fields[] = array(
                    'type' => 'text',
                    'name' => $retrans['field'],
                    'old_value' => $retrans['old_value'],
                    'new_value' => $retrans['new_value'],
                    'change' => $retrans['change'],
                    'orig_value' => $value_data[$retrans['field']]['change'],
                    'trans_value' => $value_data[$retrans['field']]['trans_value'],
                    'desc' => mage::helper('sinful_translation')->getFieldDescription($retrans['field'], 'category'),
                    'p-br' => mage::helper('sinful_translation')->formattingPBr($retrans['field']),
                    'br-p' => mage::helper('sinful_translation')->formattingBrp($retrans['field']),
                );
            }

            $fields[] = array(
                'type' => 'select',
                'desc' => 'If update is done, set to "Yes"',
                'name' => 'update_done',
                'update_done' => 'true',
                'orig_value' => array('0' => 'No', '1' => 'Yes'),
                'trans_value' => array('0' => 'No', '1' => 'Yes'),
            );

            /*$fields[] = array(
                'type' => 'select',
                'desc' => 'If translation needs proofreading, set to "Yes"',
                'name' => 'proofread',
                'proofread' => 'true',
                'data' => $value_data['proof_reading']
            );*/




        }else {

            $field_names = array('entity_id', 'name', 'top_text', 'description', 'url_key', 'meta_title', 'meta_description', 'is_done', 'in_menu', 'proofread', 'category_proofread', 'include_in_menu', 'is_active', 'category_bottom_title', 'bottom_column1_title', 'bottom_column1_text', 'bottom_column2_title', 'bottom_column2_text', 'bottom_column3_title', 'bottom_column3_text');
            $value_data = $this->getFieldValues($field_names);

            $fields = [];

            $fields[] = array(
                'type' => 'hidden',
                'name' => 'entity_id',
                'orig_value' => $value_data['entity_id']['orig_value'],
                'trans_value' => $value_data['entity_id']['trans_value'],
            );

            $fields[] = array(
                'type' => 'text',
                'name' => 'name',
                'orig_value' => $value_data['name']['orig_value'],
                'trans_value' => $value_data['name']['trans_value'],
                'desc' => mage::helper('sinful_translation')->getFieldDescription('name', 'category')
            );

            $fields[] = array(
                'type' => 'textarea',
                'name' => 'description',
                'orig_value' => $value_data['description']['orig_value'],
                'trans_value' => $value_data['description']['trans_value'],
                'desc' => mage::helper('sinful_translation')->getFieldDescription('description', 'category'),
                'p-br' => mage::helper('sinful_translation')->formattingPBr('description')
            );

            $fields[] = array(
                'type' => 'text',
                'name' => 'url_key',
                'orig_value' => $value_data['url_key']['orig_value'],
                'trans_value' => $value_data['url_key']['trans_value'],
                'desc' => mage::helper('sinful_translation')->getFieldDescription('url_key', 'category')
            );

            $fields[] = array(
                'type' => 'textarea',
                'name' => 'meta_title',
                'orig_value' => $value_data['meta_title']['orig_value'],
                'trans_value' => $value_data['meta_title']['trans_value'],
                'desc' => mage::helper('sinful_translation')->getFieldDescription('meta_title', 'category')
            );

            $fields[] = array(
                'type' => 'textarea',
                'name' => 'meta_description',
                'orig_value' => $value_data['meta_description']['orig_value'],
                'trans_value' => $value_data['meta_description']['trans_value'],
                'desc' => mage::helper('sinful_translation')->getFieldDescription('meta_description', 'category')
            );

            $fields[] = array(
                'type' => 'textarea',
                'name' => 'category_bottom_title',
                'orig_value' => $value_data['category_bottom_title']['orig_value'],
                'trans_value' => $value_data['category_bottom_title']['trans_value'],
                'desc' => mage::helper('sinful_translation')->getFieldDescription('category_bottom_title', 'category'),
                'p-br' => mage::helper('sinful_translation')->formattingPBr('category_bottom_title')
            );


            $fields[] = array(
                'type' => 'textarea',
                'name' => 'bottom_column1_title',
                'orig_value' => $value_data['bottom_column1_title']['orig_value'],
                'trans_value' => $value_data['bottom_column1_title']['trans_value'],
                'desc' => mage::helper('sinful_translation')->getFieldDescription('bottom_column1_title', 'category'),
                'p-br' => mage::helper('sinful_translation')->formattingPBr('bottom_column1_title')
            );


            $fields[] = array(
                'type' => 'textarea',
                'name' => 'bottom_column1_text',
                'orig_value' => $value_data['bottom_column1_text']['orig_value'],
                'trans_value' => $value_data['bottom_column1_text']['trans_value'],
                'desc' => mage::helper('sinful_translation')->getFieldDescription('bottom_column1_text', 'category'),
                'p-br' => mage::helper('sinful_translation')->formattingPBr('bottom_column1_text')
            );

            $fields[] = array(
                'type' => 'textarea',
                'name' => 'bottom_column2_title',
                'orig_value' => $value_data['bottom_column2_title']['orig_value'],
                'trans_value' => $value_data['bottom_column2_title']['trans_value'],
                'desc' => mage::helper('sinful_translation')->getFieldDescription('bottom_column2_title', 'category'),
                'p-br' => mage::helper('sinful_translation')->formattingPBr('bottom_column2_title')
            );


            $fields[] = array(
                'type' => 'textarea',
                'name' => 'bottom_column2_text',
                'orig_value' => $value_data['bottom_column2_text']['orig_value'],
                'trans_value' => $value_data['bottom_column2_text']['trans_value'],
                'desc' => mage::helper('sinful_translation')->getFieldDescription('bottom_column2_text', 'category'),
                'p-br' => mage::helper('sinful_translation')->formattingPBr('bottom_column2_text')
            );

            $fields[] = array(
                'type' => 'textarea',
                'name' => 'bottom_column3_title',
                'orig_value' => $value_data['bottom_column3_title']['orig_value'],
                'trans_value' => $value_data['bottom_column3_title']['trans_value'],
                'desc' => mage::helper('sinful_translation')->getFieldDescription('bottom_column3_title', 'category'),
                'p-br' => mage::helper('sinful_translation')->formattingPBr('bottom_column3_title')
            );

            $fields[] = array(
                'type' => 'textarea',
                'name' => 'bottom_column3_text',
                'orig_value' => $value_data['bottom_column3_text']['orig_value'],
                'trans_value' => $value_data['bottom_column3_text']['trans_value'],
                'desc' => mage::helper('sinful_translation')->getFieldDescription('bottom_column3_text', 'category'),
                'p-br' => mage::helper('sinful_translation')->formattingPBr('bottom_column3_text')
            );

            $fields[] = array(
                'type' => 'select',
                'desc' => 'Choose whether category should be included in navigation menu',
                'name' => 'include_in_menu',
                'data' => $value_data['include_in_menu']
            );

            $fields[] = array(
                'type' => 'select',
                'desc' => 'If text needs proofreading, set to "Yes"',
                'name' => 'proofread',
                'proofread' => 'true',
                'dataCat' => $value_data['category_proofread']
            );

            $fields[] = array(
                'type' => 'select',
                'desc' => 'Once translation has been proofread and can be activated, set to "Active"',
                'name' => 'is_done',
                'isDone' => 'true',
                'data' => $value_data['is_active']
            );

        }

        return $fields;
    }


    public function getFieldValues($fields){

        $category = $this->getCurrentEntity();
        $use_dest_data = true;

        if($category->getId() == ''){
            return false;
        } else{
            $category_data = Mage::getModel('catalog/category')->load($category->getId())->getData();
            $dest_category_data = Mage::getModel('catalog/category')->setStoreId(parent::getTranslatorStoreId())->load($category->getId())->getData();
        }
        $data = [];

        if($use_dest_data)
        {
            foreach($fields as $field){
                $data[$field] = array(
                    'orig_value' => $category_data[$field],
                    'trans_value' => $dest_category_data[$field],
                );
            }
        } else{
            foreach($fields as $field){
                $data[$field] = array(
                    'orig_value' => $category_data[$field],
                    'trans_value' => $category_data[$field],
                );
            }
        }

        return $data;
    }

    public function saveTranslation()
    {
        # Globals
        $destinationStore = parent::getTranslatorStoreId();
        $retranslate =   $_POST['retrans'];
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);


        if($_POST['meta_title']){
            $_POST['meta_title'] = mage::helper('sinful_translation')->htmlentitytag($_POST['meta_title']);
        }
        if($_POST['meta_description']){
            $_POST['meta_description'] = mage::helper('sinful_translation')->htmlentitytag($_POST['meta_description']);
        }
        if($_POST['name']){
            $_POST['name'] = mage::helper('sinful_translation')->htmlentitytag($_POST['name']);
        }
        if($_POST['bottom_column1_title']){
            $_POST['bottom_column1_title'] = mage::helper('sinful_translation')->htmlentitytag($_POST['bottom_column1_title']);
        }

        if($_POST['bottom_column2_title']){
            $_POST['bottom_column2_title'] = mage::helper('sinful_translation')->htmlentitytag($_POST['bottom_column2_title']);
        }
        if($_POST['bottom_column3_title']){
            $_POST['bottom_column3_title'] = mage::helper('sinful_translation')->htmlentitytag($_POST['bottom_column3_title']);
        }

        if($retranslate == 1) {
            $_category = Mage::getModel('catalog/category')->load($_POST['entity_id']);
            $_category->setStoreId($destinationStore);
            $_category->setData(array_merge($_category->getData(), $_POST));//$data);

            if ($_POST['update_done'] == 1) {
                mage::helper('sinful_translation')->removeFromRetranslation($_POST['entity_id'], 'category', parent::getTranslatorStoreId(), Mage::getSingleton('admin/session')->getUser()->getData('username'));
                # Save KPI on entity retranslation
                mage::helper('sinful_translation')->saveKpi($_POST['entity_id'], parent::getTranslatorStoreId(), 'category', 1, $_category->getName());
            }
        }else {

            $base_url = Mage::getModel('catalog/category')->load($_POST['entity_id'])->getUrlKey();
            $current_url = Mage::getModel('catalog/category')->setStoreId(parent::getTranslatorStoreId())->load($_POST['entity_id'])->getUrlKey();

            $_category = Mage::getModel('catalog/category')->load($_POST['entity_id']);
            $_category->setStoreId($destinationStore);

            $isDone = $_POST['is_done'];
            $isInMenu = $_POST['include_in_menu'];
            $proofread = $_POST['proofread'];

            //Set translated fields
            $_category->setName($_POST['name']);
            $_category->setTopText($_POST['top_text']);
            $_category->setDescription($_POST['description']);
            $_category->setSecondDescription($_POST['second_description']);
            //$_category->setUrl_key($_POST['url_key']);
            $_category->setMetaTitle($_POST['meta_title']);
            $_category->setMetaDescription($_POST['meta_description']);
            $_category->setStoreId($destinationStore);
            $_category->setcategory_bottom_title($_POST['category_bottom_title']);
            $_category->setbottom_column1_title($_POST['bottom_column1_title']);
            $_category->setbottom_column1_text($_POST['bottom_column1_text']);
            $_category->setbottom_column1_text($_POST['bottom_column1_text']);
            $_category->setbottom_column2_title($_POST['bottom_column2_title']);
            $_category->setbottom_column2_text($_POST['bottom_column2_text']);
            $_category->setbottom_column3_title($_POST['bottom_column3_title']);
            $_category->setbottom_column3_text($_POST['bottom_column3_text']);

            if ($base_url == $current_url) {
                $_category->setUrl_key(parent::formatUrlKey($_POST['name']));
            } else {
                //$_category->setUrl_key($current_url);
                $_category->setUrl_key($_POST['url_key']);
            }

            if ($isInMenu === 1) {
                $_category->setIncludeInMenu(1);
            } elseif ($isInMenu === 0) {
                $_category->setIncludeInMenu(0);
            }

            if ($isDone === "isDone") {
                $_category->setIsActive(1);
                $_category->setStoreId($destinationStore);
                # Save KPI on entity activation
                mage::helper('sinful_translation')->saveKpi($_POST['entity_id'], parent::getTranslatorStoreId(), 'category', 0, $_category->getName());
            } elseif ($isDone === "notDone") {
                $_category->setStoreId($destinationStore);
                $_category->setIsActive(0);
            }
        }


        try{
            $_category->save();
            #Run Changelog
            $this->saveChangelog($_category->getTitle());
            Mage::getSingleton('core/session')->addSuccess(ucfirst(parent::getTranslatorType()).' saved');
        }catch(Exception $e){
            Mage::getSingleton('core/session')->addError($e->getMessage());
        }

    }

    public function getRedirect(){
        $destinationStore = parent::getTranslatorStoreId();
        $test = Mage::helper("adminhtml")->getUrl("sinfultranslation/adminhtml_translator/overview", array('storeid' => $destinationStore, 'type' => $this->getTranslatorType()));
        echo "<script>window.location.href='$test';</script>";
    }

    public function getOverview()
    {
        $translatedPending = Mage::getResourceModel('catalog/category_collection')
            ->addAttributeToSelect('name','entity_id', 'category_proofread')
            ->setStoreId(parent::getTranslatorStoreId())
            ->setOrder('entity_id', 'ASC')
            ->addAttributeToFilter('entity_id', array('nin' => array(1,2)))
            ->addAttributeToSort('category_proofread')
            ->addAttributeToFilter('category_proofread', array('eq' => '2'));


        $translated = Mage::getResourceModel('catalog/category_collection')
            ->addAttributeToSelect('name','entity_id', 'category_proofread')
            ->setStoreId(parent::getTranslatorStoreId())
            ->setOrder('entity_id', 'ASC')
            ->addAttributeToFilter('entity_id', array('nin' => array(1,2)))
            ->addAttributeToSort('category_proofread')
            ->addAttributeToFilter('category_proofread', array('eq' => '1'));

        $data = [];


        $retransEntities = mage::helper('sinful_translation')->getRetranslationEntities('category', parent::getTranslatorStoreId());

        foreach($retransEntities as $retrans){

            $data[] = array(
                'id' => $retrans['source_id'],
                'info' => '<p><strong>'. $retrans['title'] . '</strong></p> ',
                'retranslation' => 'yes',
            );

        }



        foreach($translatedPending as $category){

            if($category->category_proofread == 2){
                $data[] = array(
                    'id' => $category->getId(),
                    'info' => '<p>- <strong>'.$category->getName().'</strong></p>',
                    'proofreading' => 'yes'
                );
            }
        }

        foreach($translated as $category){
           if($category->category_proofread == 2){
                $data[] = array(
                    'id' => $category->getId(),
                    'info' => '<p>- <strong>'.$category->getName().'</strong></p>',
                    'proofreading' => 'yes'
                );
            }
            else{
                $data[] = array(
                    'id' => $category->getId(),
                    'info' => '<p>- <strong>'.$category->getName().'</strong></p>',
                );
            }
        }

        return $data;
    }


    public function saveChangelog($humanReadable){

        $entityId    = $_POST['entity_id'];
        $storeId     = Mage::app()->getWebsite()->getDefaultGroup()->getDefaultStoreId();
        $targetStore = parent::getTranslatorStoreId();
        $user        = Mage::getSingleton('admin/session')->getUser()->getData('username');
        $type        = 'category';

        $fieldsForChangeLog = $this->getTranslationFields();

        foreach($fieldsForChangeLog as $logItem){
            mage::helper('sinful_translation')->saveChangelog($entityId, $storeId, $targetStore, $user, $type, $logItem['name'], $logItem['trans_value'], $logItem['trans_value'], $humanReadable);
        }
    }

}