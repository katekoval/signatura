<?php

class Sinful_Translation_Model_Translators_Review extends Sinful_Translation_Model_Translator
{
    protected $current_entity;

    public function getCurrentEntity(){


        if(isset($this->current_entity)){
            return $this->current_entity;
        }

        $default_store = Mage::app()->getWebsite()->getDefaultGroup()->getDefaultStoreId();

        if($entity_id = Mage::app()->getRequest()->getParam('load_entity_id')){
            $review = Mage::getModel('review/review')->load($entity_id);
            $this->current_entity = $review;
        }

        else{

            //Gets current store
            $currentStore = parent::getTranslatorStoreId();

            //Inactive get all reviews  from current store
            //Inactive get all reviews  from current store
            $inactive =  Mage::getModel('review/review')
                ->getCollection()
                ->addFieldToFilter('status_id', 1)
                ->addFilterToMap('store_id', 'review_detail.store_id')
                ->addFieldToFilter('store_id', 3);

            $inactive->getSelect()->join( array('review_detail'=> 'review_detail'), 'review_detail.review_id = main_table.review_id', array('review_detail.store_id'));

            $inactive = $inactive->getColumnValues('review_id');

            $review = Mage::getModel('review/review')
                ->getCollection()
                ->addStoreFilter($default_store)
                ->addFilterToMap('review_id', 'main_table.review_id')
                ->addFieldToFilter('review_id', array('nin' => $inactive))
                ->addStatusFilter(Mage_Review_Model_Review::STATUS_APPROVED)
                ->getFirstItem();

            $this->current_entity = $review;
        }

        if($review->getReviewId() == ''){
            return false;
        } else{
            return $review;
        }


    }

    public function getCurrentEntityData()
    {

        $review = $this->getCurrentEntity();
        $review = Mage::getModel('review/review')->load($review->getReviewId());

        $product =  Mage::getModel('catalog/product')->setStoreId($review->getData('store_id'))->load($review->getEntityPkValue());
        $destination_url =  Mage::app()->getStore($review->getData('store_id'))->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);



        $data =  array(
            'entity_id'       => $review->getReviewId(),
            'entity_title'    => $review->getTitle(),
            'entity_detail'   => $review->getDetail(),
            'entity_nickname' => $review->getNickname(),
            'entity_createdat' => $review->getCreatedAt(),
            'entity_productid' => $review->getEntityPkValue(),
            'entity_email' => $review->getEmail(),
            'url' => $destination_url.$product->getUrlKey(),
        );


        return $data;

    }

    public function getEntitiesLeft()
    {

        $type = 'review';
        $data = array();
        $websiteId = parent::getTranslatorStoreId();

        $missing = Mage::getModel('translation/missing')
            ->getCollection()
            ->addFieldToFilter('type', $type);
        $missing->getSelect()->where('source_store != '.$websiteId.' and target_store NOT LIKE "%'.$websiteId.'%" OR source_store != '.$websiteId.' and target_store is null');

        $pending = Mage::getModel('review/review')->getCollection()
            ->addFieldToFilter('status_id', 2)
            ->addFieldToFilter('proofreading', 1);

        $pending->getSelect()->joinLeft(array('review_detail'=>$pending->getTable('review/review_detail')), 'review_detail.review_id=main_table.review_id',array('store_id'=>'store_id'));
        $pending->getSelect()->where('review_detail.store_id = '.$websiteId);



        $data['total'] = (int)$missing->getSize() + (int)$pending->getSize();

        return $data;
    }

    public function getPreviousEntities()
    {

        //Gets the default store
        $default_store = Mage::app()->getWebsite()->getDefaultGroup()->getDefaultStoreId();



        return $data;
    }

    public function getTranslationFields(){
        $field_names = array('review_id','title','detail','nickname', 'created_at', 'entity_pk_value', 'email', 'proofreading','status');

        $value_data = $this->getFieldValues($field_names);

        $fields = [];

        $fields[] = array(
            'type' => 'hidden',
            'name' => 'review_id',
            'orig_value' => $value_data['review_id']['orig_value'],
            'trans_value' => $value_data['review_id']['trans_value'],
        );

        $fields[] = array(
            'type' => 'hidden',
            'name' => 'entity_pk_value',
            'orig_value' => $value_data['entity_pk_value']['orig_value'],
            'trans_value' => $value_data['entity_pk_value']['trans_value'],
        );

        $fields[] = array(
            'type' => 'text',
            'name' => 'title',
            'orig_value' => $value_data['title']['orig_value'],
            'trans_value' => $value_data['title']['trans_value'],
            'desc' => 'REVIEW HEADLINE'
        );

        $fields[] = array(
            'type' => 'text',
            'name' => 'detail',
            'orig_value' => $value_data['detail']['orig_value'],
            'trans_value' => $value_data['detail']['trans_value'],
            'desc' => 'REVIEW TEXT',
           /* 'p-br' => mage::helper('sinful_translation')->formattingPBr('detail')*/
        );

        $fields[] = array(
            'type' => 'text',
            'name' => 'nickname',
            'orig_value' => $value_data['nickname']['orig_value'],
            'trans_value' => $value_data['nickname']['trans_value'],
            'desc' => 'REVIEWER ALIAS'
        );

        $fields[] = array(
            'type' => 'text',
            'name' => 'created_at',
            'orig_value' => $value_data['created_at']['orig_value'],
            'trans_value' => $value_data['created_at']['trans_value'],
            'desc' => 'REVIEW DATE & TIME (date needs to match the original, ignore time)'
        );

        $fields[] = array(
            'type' => 'text',
            'name' => 'email',
            'orig_value' => $value_data['email']['orig_value'],
            'trans_value' => $value_data['email']['trans_value'],
            'desc' => 'REVIEWER EMAIL (use oversat@sinful.dk)'
        );

        $fields[] = array(
            'type' => 'select',
            'name' => 'proofread',
            'proofread' => 'true',
            'data' => $value_data['proofreading']['trans_value'],
            'desc' => 'If text needs proofreading, set to "Yes"'
        );

        $fields[] = array(
            'type' => 'select',
            'desc' => 'Once translation has been proofread and can be activated, set to "Active"',
            'name' => 'is_done',
            'isDone' => 'true',
            'data' => $value_data['status']
        );



        return $fields;
    }

    public function getFieldValues($fields){

        $review = $this->getCurrentEntity();
        $use_dest_data = true;

        if($review->getReviewId() == ''){
            return false;
        } else{

            if($review->getData('parent_id')){;
                $origDataId = $review->getData('parent_id');
                $review_data = Mage::getModel('review/review')->load($origDataId)->getData();
            }else{
                $review_data = Mage::getModel('review/review')->load($review->getReviewId())->getData();
            }

            $dest_review_data = Mage::getModel('review/review')->load($review->getReviewId())->getData();
        }
        $data = [];

        if($use_dest_data)
        {
            foreach($fields as $field){
                    $data[$field] = array(
                        'orig_value' => $review_data[$field],
                        'trans_value' => $dest_review_data[$field],
                    );
            }
        } else {
            foreach ($fields as $field) {

                $data[$field] = array(
                    'orig_value' => $review_data[$field],
                    'trans_value' => ($review_data[$field]),
                );
            }
        }

        return $data;
    }

    public function saveTranslation()
    {

        $destinationStore = parent::getTranslatorStoreId();

        $reviewExists = Mage::getModel('review/review')->load($_POST['review_id']);


        if($_POST['detail']){
            $_POST['detail'] = mage::helper('sinful_translation')->htmlentitytag($_POST['detail']);
        }

        if($_POST['email']){
            $_POST['email'] = mage::helper('sinful_translation')->htmlentitytag($_POST['email']);
        }

        if($reviewExists->getReviewId() && $reviewExists->getData('store_id') == $destinationStore){
            $newReview = $reviewExists;
            $newReview->setData('title',$_POST['title']);
            $newReview->setData('detail',$_POST['detail']);
            $newReview->setData('email',$_POST['email']);
            $newReview->setData('nickname',$_POST['nickname']);
            $newReview->setData('created_at', $_POST['created_at']);
        }else{
            $newReview = Mage::getModel('review/review')
                ->setEntityPkValue($_POST['entity_pk_value'])
                ->setTitle($_POST['title'])
                ->setDetail($_POST['detail'])
                ->setEntityId(1) // review_entity: 1 - Product
                ->setStatusId(2) // pending
                ->setStoreId($destinationStore)
                ->setStores($destinationStore)
                ->setCustomerId(null)
                ->setCreatedAt($_POST['created_at'])
                ->setEmail($_POST['email'])
                ->setNickname($_POST['nickname']);

            $newReview->setData('store_id',$destinationStore);
            $newReview->setData('parent_id',$_POST['review_id']);
        }

        $originalReviewId = $_POST['review_id'];


        if($_POST['is_done'] === "isDone"){
            $newReview->setProofreading(0);
            $newReview->setStatusId(1); # Approved
            # Save KPI on entity activation
            mage::helper('sinful_translation')->saveKpi($_POST['review_id'], parent::getTranslatorStoreId(), 'product_review', 0, $newReview->getTitle());
        }

        if($_POST['proofread'] === "isDone") {
            $newReview->setProofreading(1);
        }elseif($_POST['proofread'] === "notDone"){
            $newReview->setProofreading(0);
        }

        $newReview->save();
        $newReviewId = $newReview->getReviewId();

        $this->aggregateFlagReview($_POST['review_id'], $destinationStore,'review');
        $this->aggregateRating($originalReviewId, $newReviewId);
        $this->saveChangelog($newReview->getTitle());
        $this->setOriginalCreatedAt($newReview, $_POST['created_at']);
        $newReview->aggregate();

        # Set post var if continue is yes:
        $_POST['newReviewId'] = $newReview->getReviewId();

        Mage::getSingleton('core/session')->addSuccess(ucfirst(parent::getTranslatorType()).' saved');

    }


    public function getOverview()
    {


        $type = 'review';
        $data = array();
        $websiteId = parent::getTranslatorStoreId();

        $missing = Mage::getModel('translation/missing')
            ->getCollection()
            ->addFieldToFilter('type', $type);
        $missing->getSelect()->where('source_store != '.$websiteId.' and target_store NOT LIKE "%'.$websiteId.'%" OR source_store != '.$websiteId.' and target_store is null');

        $pending = Mage::getModel('review/review')->getCollection()
            ->addFieldToFilter('status_id', 2)
            ->addFieldToFilter('proofreading', 1);

      /*  $pendingMissing = Mage::getModel('review/review')->getCollection()
            ->addFieldToFilter('proofreading', 0)
            ->addFieldToFilter('main_table.store_id', $websiteId)
            ->addFieldToFilter('status_id', 2);*/

        $pending->getSelect()->joinLeft(array('review_detail'=>$pending->getTable('review/review_detail')), 'review_detail.review_id=main_table.review_id',array('store_id'=>'store_id'));
        $pending->getSelect()->where('review_detail.store_id = '.$websiteId);


        foreach($pending as $post){

            $data[] = array(
                'id' => $post->getReviewId(),
                'info' => '<p><strong>' . $post->getTitle().'</strong></p> ',
                'proofreading' => 'yes',
            );

        }

      /*  foreach($pendingMissing as $post){

            $data[] = array(
                'id' => $post->getReviewId(),
                'info' => '<p><strong>PENDING: ' . $post->getTitle().'</strong></p> '
            );

        }*/

        foreach($missing as $post){

            $data[] = array(
                'id' => $post->getSourceId(),
                'info' => '<p><strong>' . $post->getTitle().'</strong></p> ',
            );

        }

        return $data;
    }



    public function aggregateFlagReview($sourceId, $destinationStore, $type){

        $missing = Mage::getModel('translation/missing')
            ->getCollection()
            ->addFieldToFilter('source_id', $sourceId)
            ->addFieldToFilter('type', $type)
            ->getFirstItem();

        if($missing){

            $array = explode(",",$missing->getTargetStore());
            $array[] .= $destinationStore;
            $targetString = implode(",", $array);

            $missing->setTargetStore($targetString);

            $missing->save();

        }

    }


    public function aggregateRating($originalReviewId, $newReviewId){

        $votesCollection = Mage::getModel('rating/rating_option_vote')
            ->getResourceCollection()
            ->setReviewFilter($originalReviewId)
            ->load();

        $exists = Mage::getModel('rating/rating_option_vote')
            ->getResourceCollection()
            ->setReviewFilter($newReviewId)
            ->load();

        $existsId = $exists->getData('review_id');

        if(empty($existsId)){
            foreach($votesCollection->getData() as $data){
                try{
                    $data['vote_id'] = null;
                    $newVotes = Mage::getModel('rating/rating_option_vote')
                        ->setData($data);
                    $newVotes->setReviewId($newReviewId);
                    $newVotes->save();
                }catch(Error $ex){
                    mage::log($ex->getMessage(), null, "translation-review.log", true);
                }

            }
        }
    }


    public function saveChangelog($humanReadable){

        $entityId    = $_POST['entity_id'];
        $storeId     = 2;
        $targetStore = parent::getTranslatorStoreId();
        $user        = Mage::getSingleton('admin/session')->getUser()->getData('username');
        $type        = 'review';

        $fieldsForChangeLog = $this->getTranslationFields();

        foreach($fieldsForChangeLog as $logItem){
            mage::helper('sinful_translation')->saveChangelog($entityId, $storeId, $targetStore, $user, $type, $logItem['name'], $logItem['trans_value'], $logItem['trans_value'],$humanReadable);
        }
    }

    public function setOriginalCreatedAt($review, $createdAtDate){

        $review->setCreatedAt($createdAtDate);
        $review->save();

    }
}