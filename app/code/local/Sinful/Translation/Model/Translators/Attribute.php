<?php

class Sinful_Translation_Model_Translators_Attribute extends Sinful_Translation_Model_Translator
{
    static protected $_translated_labels = array();

    public function getCurrentEntity(){

        if($entity_id = Mage::app()->getRequest()->getParam('load_entity_id')){
            $attribute = Mage::getModel('eav/entity_attribute')->load($entity_id);
        } else{
            $attribute = Mage::getModel('eav/entity_attribute');
            $attributes = Mage::getModel('eav/entity_attribute')->getCollection()
                ->addFieldToSelect('*')
                ->AddFieldToFilter('attribute_code', array('like' => '%filter_%'))
                ->AddFieldToFilter('attribute_code', array('neq' => 'filter_price_range'));


            foreach($attributes as $att){
                if(!$this->isAttributeTranslated($att->getId()))
                {
                    $attribute = $att;
                    break;
                }
            }
        }

        if($attribute->getId() == ''){
            return false;
        } else{
            return $attribute;
        }
    }

    private function isAttributeTranslated($attribute_id)
    {

        $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
        $table = 'eav_attribute_label';

        $select = $adapter->select()
            ->from($table, array())
            ->where('attribute_id =?', $attribute_id)
            ->where('store_id =?', parent::getTranslatorStoreId())
            ->columns(
                '*'
            );
        $values = $adapter->fetchAll($select);

        if (empty($values)) {
            return false;
        }

        return true;
    }

    private function isAttributeOptionTranslated($option_id)
    {

        $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
        $table = 'eav_attribute_option_value';

        $select = $adapter->select()
            ->from($table, array())
            ->where('option_id =?', $option_id)
            ->where('store_id =?', parent::getTranslatorStoreId())
            ->columns(
                'value'
            );
        $values = $adapter->fetchAll($select);

        if (empty($values)) {
                return false;
        }

        self::$_translated_labels[$option_id] = $values[0]['value'];

        return true;
    }

    private function updateAttributeOption($option_id, $value)
    {

        $adapter = Mage::getSingleton('core/resource')->getConnection('core_write');
        $table = 'eav_attribute_option_value';


        if($this->isAttributeOptionTranslated($option_id)){
            $data  = array('value' => $value);
            $where = array('option_id =?' => $option_id, 'store_id =?' => parent::getTranslatorStoreId());
            $adapter->update($table, $data, $where);

        } else{
            $data = array(
                'option_id' => $option_id,
                'store_id'  => parent::getTranslatorStoreId(),
                'value'     => $value,
            );
            $adapter->insert($table, $data);
        }
    }

    public function getCurrentEntityData()
    {

        $attribute = $this->getCurrentEntity();
        $image_path = '/media/sinful/translation/';

        return array(
            'entity_id' => $attribute->getId(),
            'entity_name' => $attribute->getAttributeCode(),
            'entity_image' => array(
                'small' => $image_path.'attributes.png',
                'large' => $image_path.'attributes.png',
            ),
            'entity_more_images' => '',
        );
    }

    public function getEntitiesLeft()
    {

        $idArray = [];

        $attributes = Mage::getModel('eav/entity_attribute')->getCollection()
            ->addFieldToSelect('*');


        foreach($attributes as $att){
            #if($this->isAttributeTranslated($att->getId()))
            #{
            $option_data = '';

            $attributeOptions = Mage::getResourceModel('eav/entity_attribute_option_collection')
                ->setPositionOrder('asc')
                ->setAttributeFilter($att->getId())
                ->setStoreFilter(parent::getTranslatorStoreId())
                ->load();


            if( !empty($attributeOptions->getData()) ) {

                foreach($attributeOptions as $option){
                    if( empty($option->getStoreDefaultValue()) ){
                        if(!in_array($att->getId(), $idArray)){
                            $idArray[] = $att->getId();
                        }
                    }
                }
            }

        }

        $data['total'] = count($idArray);
        $data['range'] = 'Not Available';

        return $data;
    }

    public function getPreviousEntities()
    {
        return array();
    }

    public function getTranslationFields(){

        $field_names = array('attribute_id','label');
        $value_data = $this->getFieldValues($field_names);

        $fields = [];

        $fields[] = array(
            'type' => 'hidden',
            'name' => 'attribute_id',
            'orig_value' => $value_data['attribute_id']['orig_value'],
            'trans_value' => $value_data['attribute_id']['trans_value'],
        );

        $fields[] = array(
            'type' => 'text',
            'name' => 'label',
            'orig_value' => $value_data['label']['orig_value'],
            'trans_value' => $value_data['label']['trans_value'],
        );


        /** Handle attribute option fields */
        $attribute = $this->getCurrentEntity();

        $attributeOptions = Mage::getResourceModel('eav/entity_attribute_option_collection')
            ->setPositionOrder('asc')
            ->setAttributeFilter($attribute->getId())
            ->setStoreFilter(2)
            ->load();

        foreach($attributeOptions as $option){
            if(!$this->isAttributeOptionTranslated($option->getId(),$attribute->getId())){
                $fields[] = array(
                    'type' => 'text',
                    'name' => 'option:'.$option->getId(),
                    'orig_value' => $option->getValue(),
                    'trans_value' => '',
                );
            } else{
                $fields[] = array(
                    'type' => 'text',
                    'name' => 'option:'.$option->getId(),
                    'orig_value' => $option->getValue(),
                    'trans_value' => self::$_translated_labels[$option->getId()],
                );
            }

        }

        return $fields;
    }


    public function getFieldValues($fields){


        $attribute = $this->getCurrentEntity();

        if($attribute->getId() == 'frontend_label'){
            return false;
        } else{
            $attribute_data = $attribute->getData();
        }

        $data = [];

        foreach($fields as $field){
            if($field == 'label')
            {
                $label = '';
                $labels = $attribute->getStoreLabels();
                if(array_key_exists(parent::getTranslatorStoreId(),$labels)){
                    $label = $labels[parent::getTranslatorStoreId()];
                }

                $data[$field] = array(
                    'orig_value' => $attribute->getStoreLabel(2),
                    'trans_value' => $label,
                );
            } else {
                $data[$field] = array(
                    'orig_value' => $attribute_data[$field],
                    'trans_value' => $attribute_data[$field],
                );
            }
        }

        return $data;
    }

    public function saveTranslation()
    {
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        $attribute = Mage::getModel('eav/entity_attribute')->load($_POST['attribute_id']);

        $labels = Mage::getModel('eav/entity_attribute')->getResource()
            ->getStoreLabelsByAttributeId($attribute->getId());


        if($labels[parent::getTranslatorStoreId()] !== $_POST['label']){
            # Save KPI on entity translation
            mage::helper('sinful_translation')->saveKpi($_POST['attribute_id'], parent::getTranslatorStoreId(), 'attribute-label', 0, $_POST['label']);
        }


        $labels[parent::getTranslatorStoreId()] = $_POST['label'];

        $attribute->setStoreLabels($labels)->save();


        /** Handle save of option labels */
        $option_data = [];
        foreach($_POST as $key => $data){
            if (strpos($key, 'option') !== false) {
                $option_data[explode('option:',$key)[1]] = $data;
            }
        }

        // key = option_id
        foreach($option_data as $option_id => $value){
            $this->updateAttributeOption($option_id,$value);

            if(!empty($value)){
                mage::helper('sinful_translation')->saveKpi($_POST['attribute_id'], parent::getTranslatorStoreId(), 'attribute-option', 0, $value);
            }
        }

        Mage::getSingleton('core/session')->addSuccess(ucfirst(parent::getTranslatorType()).' saved');
    }


    /*public function getOverview()
    {

        $data = [];
        $idArray = [];

        $attributes = Mage::getModel('eav/entity_attribute')->getCollection()
            ->addFieldToFilter('is_user_defined', 1)
            #->addFieldToFilter('attribute_id', 138)
            ->addFieldToSelect('*');

//            ->AddFieldToFilter('attribute_code', array('like' => '%filter_%'))
//            ->AddFieldToFilter('attribute_code', array('neq' => 'filter_price_range'));


        foreach($attributes as $att){
            #if($this->isAttributeTranslated($att->getId()))
            #{
                $option_data = '';

                $attributeOptions = Mage::getResourceModel('eav/entity_attribute_option_collection')
                    ->setPositionOrder('asc')
                    ->setAttributeFilter($att->getId())
                    ->setStoreFilter(6)
                    ->load();

                foreach($attributeOptions as $option){

                    if( empty($option->getValue()) ){
                        if(!in_array($att->getId(), $idArray)){
                            $idArray[] = $att->getId();
                        }
                    }
                }

                if(in_array($att->getId(), $idArray)){
                    $data[] = array(
                        'id' => $att->getId(),
                        'info' =>
                            '<p>'.$att->getStoreLabel(2).'</p><p>- <strong>'.$att->getStoreLabel(parent::getTranslatorStoreId()).'</strong></p><br>'.$option_data.'</p>',
                    );
                }

            }

        #}
        return $data;
    }*/

    public function getOverview()
    {

        $data = [];
        $idArray = [];

        $attributes = Mage::getModel('eav/entity_attribute')->getCollection()
            ->addFieldToSelect('*');
//            ->AddFieldToFilter('attribute_code', array('like' => '%filter_%'))
//            ->AddFieldToFilter('attribute_code', array('neq' => 'filter_price_range'));



        foreach($attributes as $att){
            #if($this->isAttributeTranslated($att->getId()))
            #{
                $option_data = '';

                $attributeOptions = Mage::getResourceModel('eav/entity_attribute_option_collection')
                    ->setPositionOrder('asc')
                    ->setAttributeFilter($att->getId())
                    ->setStoreFilter(parent::getTranslatorStoreId())
                    ->load();


                if( !empty($attributeOptions->getData()) ) {

                foreach($attributeOptions as $option){
                    if( empty($option->getStoreDefaultValue()) ){
                       if(!in_array($att->getId(), $idArray)){
                           $idArray[] = $att->getId();
                       }
                    }
                }

                if(in_array($att->getId(), $idArray)){
                    $data[] = array(
                        'id' => $att->getId(),
                        'info' =>
                            '<p>'.$att->getStoreLabel(2).'</p><p>- <strong>'.$att->getStoreLabel(parent::getTranslatorStoreId()).'</strong></p><br>'.$option_data.'</p>',
                    );

                    }
                }

            #}
        }
        return $data;
    }

}