<?php

class Sinful_Translation_Model_Translators_Block extends Sinful_Translation_Model_Translator
{
    static $sync_store;

    protected function _construct()
    {
        if(parent::isStoreSynced()){
            $this::$sync_store = parent::getSyncedStore();
        }
    }

    public function isStoreSync()
    {
        if($store = $this::$sync_store){
            return $store;
        }
        return false;
    }

    public function getCurrentEntity(){
        $destination_store = parent::getTranslatorStoreId();

        if($entity_id = Mage::app()->getRequest()->getParam('load_entity_id')){
            $block = Mage::getModel('cms/block')->load($entity_id);
        } else{
            if($sync = $this->isStoreSync()){
               $store = $sync;
            } else{
               $store = 2;
            }
            $block = Mage::getModel('cms/block')->getCollection()
                ->addStoreFilter($store, false)
                ->addFieldToFilter('exclude_from_trans', 0)
                ->addFieldToFilter('identifier', array('nin' => Mage::getModel('cms/block')->getCollection()->addStoreFilter($destination_store)->getColumnValues('identifier')))
                ->addFieldToFilter('is_active', 1)
                ->getFirstItem();
        }

        if($block->getBlockId() == ''){
            return false;
        } elseif(Mage::app()->getRequest()->getParam('load_entity_id')){
            return $block;
        }
        else{
            return false;
        }
    }

    public function getCurrentEntityData()
    {
        $block = $this->getCurrentEntity();
        $image_path = '/media/sinful/translation/';

        return array(
            'entity_id' => $block->getBlockId(),
            'entity_name' => $block->getIdentifier(),
            'entity_image' => array(
                'small' => $image_path.'blocks.jpg',
                'large' => $image_path.'blocks.jpg',
            ),
            'entity_more_images' => '',
        );
    }

    public function getEntitiesLeft()
    {
        $destination_store = parent::getTranslatorStoreId();

        $data = [];

        $data['total'] = Mage::getModel('cms/block')->getCollection()
            ->addStoreFilter(2, false)
            ->addFieldToFilter('exclude_from_trans', 0)
            ->addFieldToFilter('identifier', array('nin' => Mage::getModel('cms/block')->getCollection()->addStoreFilter(parent::getTranslatorStoreId())->getColumnValues('identifier')))
            ->addFieldToFilter('is_active', 1)
            ->getSize();

        $dataPending = Mage::getModel('cms/block')->getCollection()
            ->addStoreFilter($destination_store, false)
            ->addFieldToFilter('is_active', 0)
            ->addFieldToFilter('exclude_from_trans', 0)
            ->setOrder('need_proofread', 'DESC')
            ->getSize();


        $data['total'] += $dataPending;

        $data['range'] = 'Not available';

        return $data;
    }

    public function getPreviousEntities()
    {

        if($sync = $this->isStoreSync()){
            $store = $sync;
        } else{
            $store = 1;
        }

        $latest = Mage::getModel('cms/block')->getCollection()
            ->addStoreFilter($store, false)
            ->addFieldToFilter('exclude_from_trans', 0)
            ->addFieldToFilter('identifier', array('nin' => Mage::getModel('cms/block')->getCollection()->addStoreFilter(parent::getTranslatorStoreId())->getColumnValues('identifier')))
            ->addFieldToFilter('is_active', 1)
            ->setPageSize(5);

        $latestPending = Mage::getModel('cms/block')->getCollection()
            ->addStoreFilter($store, false)
            ->addFieldToFilter('is_active', 0)
            ->addFieldToFilter('exclude_from_trans', 0)
            ->setOrder('need_proofread', 'DESC')
            ->setPageSize(5);

        $data = array();

        foreach($latestPending as $block){
            $data[] = array(
                'entity_id' => $block->getBlockId(),
                'name' => $block->getName()
            );
        }

        foreach($latest as $block){
            $data[] = array(
                'entity_id' => $block->getBlockId(),
                'name' => $block->getName()
            );
        }

        return $data;
    }

    public function getTranslationFields(){
        $field_names = array('identifier','content', 'is_done', 'proofread', 'is_active', 'need_proofread', 'is_active');
        $value_data = $this->getFieldValues($field_names);

        $fields = [];

        $fields[] = array(
            'type' => 'hidden',
            'name' => 'identifier',
            'orig_value' => $value_data['identifier']['orig_value'],
            'trans_value' => $value_data['identifier']['trans_value'],
        );

        $fields[] = array(
            'type' => 'textarea',
            'name' => 'title',
            'orig_value' => $value_data['identifier']['orig_value'],
            'trans_value' => $value_data['identifier']['trans_value'],
        );

        $fields[] = array(
            'type' => 'textarea',
            'name' => 'content',
            'orig_value' => $value_data['content']['orig_value'],
            'trans_value' => $value_data['content']['trans_value'],
        );

        $fields[] = array(
            'type' => 'select',
            'desc' => 'If translation needs Proofreading, set to "Yes"',
            'name' => 'proofread',
            'proofread' => 'true',
            'data' => $value_data['need_proofread']
        );

        $fields[] = array(
            'type' => 'select',
            'desc' => 'Once translation is finished, set to "Active"',
            'name' => 'is_done',
            'isDone' => 'true',
            'data' => $value_data['is_active']
        );


        return $fields;
    }


    public function getFieldValues($fields){
        $block = $this->getCurrentEntity();
        $use_orig_data = false;
        $destination_store = parent::getTranslatorStoreId();

        if($block->getBlockId() == ''){
            return false;
        } else{

            if($sync = $this->isStoreSync()){

                $store = $sync;
            } else{
                $store = 2;
            }


            /*MAKE LOGIC TO GET FROM CURRENT STORE */
            $block_data = $block->getData();
            $orig_data = Mage::getModel('cms/block')->getCollection()
                ->addStoreFilter($store, false)
                ->addFieldToFilter('identifier', $block->getIdentifier())
                ->getFirstItem()->getData();

//            $block_data = Mage::getModel('cms/block')->getCollection()
//                ->addStoreFilter($destination_store, false)
//                ->addFieldToFilter('identifier', $block->getIdentifier())
//                ->getFirstItem()->getData();

            if($orig_data['content']){
                $use_orig_data = true;
            }
        }
        $data = [];

        foreach($fields as $field){
            if($use_orig_data){
                $data[$field] = array(
                    'orig_value' => $orig_data[$field],
                    'trans_value' => $block_data[$field],
                );
            } else{
                $data[$field] = array(
                    'orig_value' => $block_data[$field],
                    'trans_value' => $block_data[$field],
                );
            }
        }


        return $data;
    }

    public function saveTranslation()
    {
        $destination_store = parent::getTranslatorStoreId();

        $block = Mage::getModel('cms/block')->getCollection()
            ->addStoreFilter(parent::getTranslatorStoreId(), false)
            ->addFieldToFilter('identifier', $_POST['identifier'])
            ->getFirstItem();


        if($block->getId()){

            $isDone = $_POST['is_done'];
            $proofread = $_POST['proofread'];


            if($proofread === "isDone"){
                $block->setNeedProofread(1);
            }elseif($proofread === "notDone"){
                $block->setNeedProofread(0);
            }

            if($isDone === "isDone"){
                $block->setIsActive(1);
                $block->setNeedProofread(0);
            }elseif($isDone === "notDone"){
                $block->setIsActive(0);
            }

            $block->setContent($_POST['content']);
            $block->setStores(array($destination_store));
            $block->save();

        } else {
            $_block = Mage::getModel('cms/block');

            //Set translated fields
            $_block->setData($_POST);

            $isDone = $_POST['is_done'];
            $proofread = $_POST['proofread'];

            $_block->setStores(array($destination_store));


            if($proofread === "isDone"){
                $_block->setNeedProofread(1);
            }elseif($proofread === "notDone"){
                $_block->setNeedProofread(0);
            }

            if($isDone === "isDone"){
                $_block->setIsActive(1);
                $_block->setExcludeFromTrans(0);
                $_block->setNeedProofread(0);
            }elseif($isDone === "notDone"){
                $_block->setIsActive(0);
            }


            $_block->save();
        }

        Mage::getSingleton('core/session')->addSuccess(ucfirst(parent::getTranslatorType()).' saved');

    }

    public function getRedirect(){
        $destination_store = parent::getTranslatorStoreId();
        $test = Mage::helper("adminhtml")->getUrl("sinfultranslation/adminhtml_translator/overview", array('storeid' => $destination_store, 'type' => $this->getTranslatorType()));
        echo "<script>window.location.href='$test';</script>";
    }

    public function getOverview()
    {
        $destination_store = parent::getTranslatorStoreId();

        $translated = Mage::getModel('cms/block')->getCollection()
        ->addStoreFilter(2, false)
        ->addFieldToFilter('is_active', 1)
        ->addFieldToFilter('exclude_from_trans', 0)
        ->addFieldToFilter('identifier', array('nin' => Mage::getModel('cms/block')->getCollection()->addStoreFilter(parent::getTranslatorStoreId())->getColumnValues('identifier')))
        ->setOrder('need_proofread', 'DESC');

        $translated2 = Mage::getModel('cms/block')->getCollection()
            ->addStoreFilter($destination_store, false)
            ->addFieldToFilter('is_active', 0)
            ->addFieldToFilter('exclude_from_trans', 0)
            ->setOrder('need_proofread', 'DESC');


        $data = [];

        $this_store = parent::getTranslatorStoreId();
        $this_store_init = Mage::getModel('core/store')->load($this_store);
        $this_store_name = $this_store_init->getName();


        foreach($translated2 as $block){
            $orig_block =  Mage::getModel('cms/block')->getCollection()
                ->addStoreFilter(2, false)
                ->addFieldToFilter('identifier', $block->getIdentifier())
                ->getFirstItem();

            if($orig_block->getContent()){
                if($block->need_proofread == 1){
                    $data[] = array(
                        'id' => $block->getId(),
                        'info' => '<strong><p>'.htmlspecialchars($orig_block->getTitle()).'</p></strong>' . '<p>'.substr(htmlspecialchars($orig_block->getContent()), 0, 26).'</p><p>- <strong>'.substr(htmlspecialchars($block->getContent()), 0, 26).'</strong></p>',
                        'proofreading' => 'yes'
                    );
                }else{
                    $data[] = array(
                        'id' => $block->getId(),
                        'info' => '<strong><p>'.htmlspecialchars($orig_block->getTitle()).'</p></strong>' . '<p>'.substr(htmlspecialchars($orig_block->getContent()), 0, 26).'</p><p>- <strong>'.substr(htmlspecialchars($block->getContent()), 0, 26).'</strong></p>',
                    );
                }
            } else{
                $data[] = array(
                    'id' => $block->getId(),
                    'info' => '<p>'.$this_store_name.' version not found</p>'. '<strong><p>'.htmlspecialchars($block->getTitle()).'</p></strong>' .'<p>- <strong>'.substr(htmlspecialchars($block->getContent()), 0, 26).'</strong></p>',
                );
            }
        }



        foreach($translated as $block){
        $orig_block =  Mage::getModel('cms/block')->getCollection()
            ->addStoreFilter(2, false)
            ->addFieldToFilter('identifier', $block->getIdentifier())
            ->getFirstItem();

        if($orig_block->getContent()){
            if($orig_block->need_proofread == 1){
                $data[] = array(
                    'id' => $block->getId(),
                    'info' => '<strong><p>'.htmlspecialchars($orig_block->getTitle()).'</p></strong>' . '<p>'.substr(htmlspecialchars($orig_block->getContent()), 0, 26).'</p><p>- <strong>'.substr(htmlspecialchars($block->getContent()), 0, 26).'</strong></p>',
                    'proofreading' => 'yes'
                );
            }else{
                $data[] = array(
                    'id' => $block->getId(),
                    'info' => '<strong><p>'.htmlspecialchars($orig_block->getTitle()).'</p></strong>' . '<p>'.substr(htmlspecialchars($orig_block->getContent()), 0, 26).'</p><p>- <strong>'.substr(htmlspecialchars($block->getContent()), 0, 26).'</strong></p>',
                );
            }
        } else{
            $data[] = array(
                'id' => $block->getId(),
                'info' => '<p>'.$this_store_name.' version not found</p>'. '<strong><p>'.htmlspecialchars($block->getTitle()).'</p></strong>' .'<p>- <strong>'.substr(htmlspecialchars($block->getContent()), 0, 26).'</strong></p>',
            );
        }
    }

        return $data;
    }
}