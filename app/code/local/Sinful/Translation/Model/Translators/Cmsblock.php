<?php

class Sinful_Translation_Model_Translators_Cmsblock extends Sinful_Translation_Model_Translator
{
    protected $current_entity;

    public function getCurrentEntity(){


        if(isset($this->current_entity)){
            return $this->current_entity;
        }

        if($entity_id = Mage::app()->getRequest()->getParam('load_entity_id')){
            $missing = Mage::getModel('cms/block')->load($entity_id);
            $this->current_entity = $missing;
        }elseif($entity_id = Mage::app()->getRequest()->getParam('load_identifier')){
            $missing = Mage::getModel('cms/block')->getCollection()
                ->addStoreFilter(parent::getTranslatorStoreId(), false)
                ->addFieldToFilter('identifier', $entity_id)
                ->getFirstItem();
            $this->current_entity = $missing;
        } else{
            $missing = Mage::getModel('cms/block')->load($entity_id);
        }


       if($missing->getBlockId() == ''){
            return false;
       }elseif(Mage::app()->getRequest()->getParam('load_identifier')){
           return $missing;
       }else{
           return $missing;
       }


    }

    public function getCurrentEntityData()
    {

        $missing = $this->getCurrentEntity();
        $missing = Mage::getModel('cms/block')->load($missing->getBlockId());


        $data =  array(
            'entity_id'       => $missing->getBlockId(),
            'entity_title'    => $missing->getTitle(),
            'entity_identifier'   => $missing->getIdentifier(),
            'entity_content' =>  $missing->getIdentifier()
        );


        return $data;

    }

    public function getEntitiesLeft()
    {

        /*$type = 'cmsblock';

        $missing = Mage::getModel('translation/missing')
            ->getCollection()
            ->addFieldToFilter(
                array('source_store'), // columns
                array( // conditions
                    array( // conditions for source_store
                        array('neq' => parent::getTranslatorStoreId()),
                    )
                ));

        $missing->getSelect()->where('type ="'.$type.'" and target_store NOT LIKE "%'.parent::getTranslatorStoreId().'%" or target_store IS NULL');

        $pending = Mage::getModel('cms/page')->getCollection()
            ->addFieldToFilter('is_active', 0)
            ->addFieldToFilter('proofreading', 1);

        $pending->getSelect()->joinLeft(array('page_id'=>$pending->getTable('cms/page_store')), 'page_id.page_id=main_table.page_id',array('store_id'=>'store_id'));

        $pending->getSelect()->where('store_id = '.parent::getTranslatorStoreId());*/


        $pending = Mage::getModel('cms/block')->getCollection()
            ->addStoreFilter(parent::getTranslatorStoreId(), false)
            ->addFieldToFilter('is_active', 0)
            ->addFieldToFilter('exclude_from_trans', 0)
            ->setOrder('proofreading', 'DESC');


        $missing = Mage::getModel('cms/block')->getCollection()
            ->addStoreFilter(2, false)
            ->addFieldToFilter('is_active', 1)
            ->addFieldToFilter('exclude_from_trans', 0)
            ->addFieldToFilter('identifier', array('nin' => Mage::getModel('cms/block')->getCollection()->addStoreFilter(6)->getColumnValues('identifier')))
            ->addFieldToFilter('main_table.block_id', array('nin' => $pending->getColumnValues('block_id')))
            ->setOrder('proofreading', 'DESC');


        $data['total'] = (int)$missing->getSize() + (int)$pending->getSize();

        return $data;
    }

    public function getPreviousEntities()
    {

        //Gets the default store
        $default_store = Mage::app()->getWebsite()->getDefaultGroup()->getDefaultStoreId();



        return '';
    }

    public function getTranslationFields(){

        $fields = [];

        $field_names = array('block_id', 'title', 'identifier', 'content');

        $value_data = $this->getFieldValues($field_names);

        $fields = [];

        $fields[] = array(
            'type' => 'hidden',
            'name' => 'block_id',
            'orig_value' => $value_data['block_id']['orig_value'],
            'trans_value' => $value_data['block_id']['orig_value'],
        );

        $fields[] = array(
            'type' => 'text',
            'name' => 'title',
            'orig_value' => $value_data['title']['orig_value'],
            'trans_value' => $value_data['title']['trans_value'],
        );

        $fields[] = array(
            'type' => 'text',
            'name' => 'identifier',
            'orig_value' => $value_data['identifier']['orig_value'],
            'trans_value' => $value_data['identifier']['trans_value'],
        );

        $fields[] = array(
            'type' => 'text',
            'name' => 'content',
            'orig_value' => $value_data['content']['orig_value'],
            'trans_value' => $value_data['content']['trans_value'],
        );

        $fields[] = array(
            'type' => 'select',
            'desc' => 'Once translation is finished, set to "Active"',
            'name' => 'is_done',
            'isDone' => 'true',
            'data' => $value_data['status']
        );

        return $fields;
    }

    public function getFieldValues($fields){

        $missing = $this->getCurrentEntity();
        $use_dest_data = true;

        if($missing->getBlockId() == ''){
            return false;
        } else{

            $missing_data = Mage::getModel('cms/block')->load($missing->getBlockId())->getData();
            $dest_review_data = Mage::getModel('cms/block')->getCollection()
                ->addStoreFilter(parent::getTranslatorStoreId(), false)
                ->addFieldToFilter('identifier', $missing->getIdentifier())
                ->getFirstItem()->getData();
        }
        $data = [];

        if($use_dest_data)
        {
            foreach($fields as $field){
                $data[$field] = array(
                    'orig_value' => $missing_data[$field],
                    'trans_value' => $dest_review_data[$field],
                );
            }
        } else {
            foreach ($fields as $field) {

                $data[$field] = array(
                    'orig_value' => $missing_data[$field],
                    'trans_value' => ($missing_data[$field]),
                );
            }
        }

        return $data;
    }

    public function saveTranslation()
    {
        # Globals
        $destinationStore = parent::getTranslatorStoreId();
        $retranslate      = $_POST['retrans'];
        $cmsBlockExists   = Mage::getModel('cms/block')->getCollection()
            ->addStoreFilter(parent::getTranslatorStoreId(), false)
            ->addFieldToFilter('identifier', $_POST['identifier'])
            ->getFirstItem();

        Mage::app()->setCurrentStore($destinationStore); //desired store id


        if($retranslate == 1) {
            $newCmsBlock = Mage::getModel('cms/block')->getCollection()
                ->addStoreFilter(parent::getTranslatorStoreId(), false)
                ->addFieldToFilter('identifier', $_POST['identifier'])
                ->getFirstItem();
            $newCmsBlock->setData('content',$_POST['content']);
            $newCmsBlock->setData('title',$_POST['content']);

            if ($_POST['update_done'] == 1) {
                mage::helper('sinful_translation')->removeFromRetranslation($_POST['block_id'], 'cmsblock', parent::getTranslatorStoreId(), Mage::getSingleton('admin/session')->getUser()->getData('username'));
                $this->saveKpi(1, $newCmsBlock->getTitle());
            }
        }elseif($cmsBlockExists->getId()){
            $newCmsBlock = $cmsBlockExists;
            $newCmsBlock->setData('content',$_POST['content']);
            $newCmsBlock->setData('title',$_POST['content']);
        }else{
            $newCmsBlock = Mage::getModel('cms/block');
            $newCmsBlock->setData('content',$_POST['content']);
            $newCmsBlock->setData('title',$_POST['content']);
            $newCmsBlock->setData('identifier', $_POST['identifier']);
            $newCmsBlock->setData('stores', array($destinationStore));
        }

        if($_POST['is_done'] == 'isDone '){
            $newCmsBlock->setData('content',$_POST['content']);
            $newCmsBlock->setData('title',$_POST['content']);
            $newCmsBlock->setData('identifier', $_POST['identifier']);
            $newCmsBlock->setData('is_active', 1);
            $newCmsBlock->setData('proofreading',0);
            $this->saveKpi(0, $newCmsBlock->getTitle());
        }
        else{
            $newCmsBlock->setData('is_active', 0);

        }

        if($_POST['proofread'] === "isDone") {
            $newCmsBlock->setData('proofreading',1);

        }elseif($_POST['proofread'] === "notDone"){
            $newCmsBlock->setData('proofreading',0);
        }

        try{
            $newCmsBlock->save();
            #Run Changelog
            $this->saveChangelog($newCmsBlock->getTitle());
            Mage::getSingleton('core/session')->addSuccess(ucfirst(parent::getTranslatorType()).' saved');
        }catch(Exception $e){
            Mage::getSingleton('core/session')->addError($e->getMessage());
        }


    }





    public function getOverview()
    {

        $data = array();

        /*$type = 'cmsblock';

        $missing = Mage::getModel('translation/missing')
            ->getCollection()
            ->addFieldToFilter('type', $type);
        $missing->getSelect()->where('source_store != '.parent::getTranslatorStoreId().' and target_store NOT LIKE "%'.parent::getTranslatorStoreId().'%" or target_store IS NULL');

        $pending = Mage::getModel('cms/block')->getCollection()
            ->addFieldToFilter('is_active', 0)
            ->addFieldToFilter('proofreading', 1);

        $pending->getSelect()->joinLeft(array('block_id'=>$pending->getTable('cms/block_store')), 'block_id.block_id=main_table.block_id',array('store_id'=>'store_id'));

        $pending->getSelect()->where('store_id = '.parent::getTranslatorStoreId());*/


        $pending = Mage::getModel('cms/block')->getCollection()
            ->addStoreFilter(parent::getTranslatorStoreId(), false)
            ->addFieldToFilter('is_active', 0)
            ->addFieldToFilter('exclude_from_trans', 0)
            ->setOrder('proofreading', 'DESC');


        $missing = Mage::getModel('cms/block')->getCollection()
            ->addStoreFilter(2, false)
            ->addFieldToFilter('is_active', 1)
            ->addFieldToFilter('exclude_from_trans', 0)
            ->addFieldToFilter('identifier', array('nin' => Mage::getModel('cms/block')->getCollection()->addStoreFilter(6)->getColumnValues('identifier')))
            ->addFieldToFilter('main_table.block_id', array('nin' => $pending->getColumnValues('block_id')))
            ->setOrder('proofreading', 'DESC');



        $retransEntities = mage::helper('sinful_translation')->getRetranslationEntities('cmsblock', parent::getTranslatorStoreId());

        foreach($retransEntities as $retrans){

            $data[] = array(
                'id' => $retrans['source_id'],
                'info' => '<p><strong>'. $retrans['title'] . '</strong></p> ',
                'retranslation' => 'yes',
            );

        }



        foreach($pending as $post){

            $data[] = array(
                'id' => $post->getBlockId(),
                'info' => '<p><strong>' . $post->getTitle().'</strong></p> ',
                'proofreading' => 'yes',
            );

        }

        foreach($missing as $post){

            $data[] = array(
                'id' => $post->getBlockId(),
                'info' => '<p><strong>' . $post->getTitle().'</strong></p> ',
            );

        }

        return $data;
    }



    public function aggregateFlagReview($sourceId, $destinationStore, $type){

        $missing = Mage::getModel('translation/missing')
            ->getCollection()
            ->addFieldToFilter('source_id', $sourceId)
            ->addFieldToFilter('type', $type)
            ->getFirstItem();

        if($missing){

            $array = explode(",",$missing->getTargetStore());
            $array[] .= $destinationStore;
            $targetString = implode(",", $array);

            $missing->setTargetStore($targetString);

            $missing->save();

        }

    }


    public function saveChangelog($humanReadable){

        $entityId    = $_POST['block_id'];
        $storeId     = Mage::app()->getWebsite()->getDefaultGroup()->getDefaultStoreId();
        $targetStore = parent::getTranslatorStoreId();
        $user        = Mage::getSingleton('admin/session')->getUser()->getData('username');
        $type        = 'cmsblock';

        $fieldsForChangeLog = $this->getTranslationFields();

        foreach($fieldsForChangeLog as $logItem){
            mage::helper('sinful_translation')->saveChangelog($entityId, $storeId, $targetStore, $user, $type, $logItem['name'], $logItem['trans_value'], $logItem['trans_value'],$humanReadable);
        }
    }

    public function saveKpi($retranslation, $humanReadable){

        $entityId    = $_POST['block_id'];
        $storeId     = Mage::app()->getWebsite()->getDefaultGroup()->getDefaultStoreId();
        $targetStore = parent::getTranslatorStoreId();
        $user        = Mage::getSingleton('admin/session')->getUser()->getData('username');
        $type        = 'cmsblock';

        mage::helper('sinful_translation')->saveKpi($entityId, $storeId, $targetStore, $user, $type, $retranslation, $humanReadable);

    }
}