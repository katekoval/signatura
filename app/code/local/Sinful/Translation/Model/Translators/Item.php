<?php

class Sinful_Translation_Model_Translators_Item extends Sinful_Translation_Model_Translator
{
    static $sync_store;

    protected function _construct()
    {
        if(parent::isStoreSynced()){
            $this::$sync_store = parent::getSyncedStore();
        }
    }

    public function isStoreSync()
    {
        if($store = $this::$sync_store){
            return $store;
        }
        return false;
    }

    public function getCurrentEntity(){
        if($entity_id = Mage::app()->getRequest()->getParam('load_entity_id')){
            $section = Mage::getModel('translation/item')->load($entity_id);
        } else{
            $section = $this->getNotTranslatedItems()->getFirstItem();
        }

        if($section->getId() == ''){
            return false;
        } else{
            return $section;
        }
    }

    public function getNotTranslatedItems()
    {
        $model = Mage::getModel('translation/item');
        $items = $model->getCollection()->addFieldToFilter('store_id',parent::getTranslatorStoreId())->getColumnValues('ref_id');

        if($store = $this->isStoreSync()){
            $left = $model->getCollection()
                ->addFieldToFilter('store_id',$store);
            $sections = Mage::getModel('translation/section')->getCollection()->addFieldToFilter('store_id',$store)->getColumnValues('id');
        } else{
            $left = $model->getCollection()
                ->addFieldToFilter('store_id',8);
            $sections = Mage::getModel('translation/section')->getCollection()->addFieldToFilter('store_id',parent::getTranslatorStoreId())->getColumnValues('source_id');
        }

        if(!empty($items)){
            $left->addFieldToFilter('ref_id',array('nin' => $items));
        }

        //* Only sections which are translated *//
        $left->addFieldToFilter('parent_id',array('in' => $sections));

        return $left;
    }

    public function getParentSectionId($parent_id)
    {
        //Load the parent and get section
        $source_id = Mage::getModel('translation/section')->load($parent_id)->getSourceId();

        return Mage::getModel('translation/section')->getCollection()
            ->addFieldToFilter('store_id', parent::getTranslatorStoreId())
            ->addFieldToFilter('source_id', $source_id)
            ->getFirstItem()
            ->getId();
    }


    public function getCurrentEntityData()
    {
        $item = $this->getCurrentEntity();

        $image_path = '/media/trendhim/translation/';

        return array(
            'entity_id' => $item->getId(),
            'entity_name' => $item->getTitle(),
            'entity_image' => array(
                'small' => $image_path.'name.jpg',
                'large' => $image_path.'name.jpg',
            ),
            'entity_more_images' => '',
        );
    }

    public function getEntitiesLeft()
    {
        $data = [];
        $data['total'] = $this->getNotTranslatedItems()->getSize();
        $data['range'] = 'Not available';
        return $data;
    }

    public function getPreviousEntities()
    {
        $model = Mage::getModel('translation/item');
        $latest = $model->getCollection()->addFieldToFilter('store_id',parent::getTranslatorStoreId())
        ->setPageSize(5);

        $data = array();

        foreach($latest as $item){
            $data[] = array(
                'entity_id' => $item->getRefId(),
                'name' => $item->getTitle()
            );
        }

        return $data;
    }

    public function getTranslationFields(){
        $field_names = array('id','ref_id','parent_id','title','content');
        $value_data = $this->getFieldValues($field_names);

        $fields = [];
        $fields[] = array(
            'type' => 'hidden',
            'name' => 'id',
            'orig_value' => $value_data['id']['orig_value'],
            'trans_value' => $value_data['id']['trans_value'],
        );

        $fields[] = array(
            'type' => 'hidden',
            'name' => 'ref_id',
            'orig_value' => $value_data['ref_id']['orig_value'],
            'trans_value' => $value_data['ref_id']['trans_value'],
        );

        $fields[] = array(
            'type' => 'hidden',
            'name' => 'parent_id',
            'orig_value' => $value_data['parent_id']['orig_value'],
            'trans_value' => $value_data['parent_id']['trans_value'],
        );

        $fields[] = array(
            'type' => 'text',
            'name' => 'title',
            'orig_value' => $value_data['title']['orig_value'],
            'trans_value' => $value_data['title']['trans_value'],
        );

        $fields[] = array(
            'type' => 'textarea',
            'name' => 'content',
            'orig_value' => $value_data['content']['orig_value'],
            'trans_value' => $value_data['content']['trans_value'],
        );


        return $fields;
    }


    public function getFieldValues($fields){

        $item = $this->getCurrentEntity();
        $use_dest_data = false;

        if($item->getId() == ''){
            return false;
        } else{
            $section_data = Mage::getModel('translation/item')->load($item->getId())->getData();
            $dest_section_data = Mage::getModel('translation/item')->getCollection()->addFieldToFilter('store_id',parent::getTranslatorStoreId())->addFieldToFilter('ref_id', $item->getRefId())->getFirstItem()->getData();

            /** Fallback for not translated yet */
            if($dest_section_data['id']){
                $use_dest_data = true;
            }
        }
        $data = [];

        if($use_dest_data)
        {
            foreach($fields as $field){
                if($field == 'id'){
                    $data[$field] = array(
                        'orig_value' => $section_data[$field],
                        'trans_value' => $section_data[$field],
                    );
                } else{
                    $data[$field] = array(
                        'orig_value' => $section_data[$field],
                        'trans_value' => $dest_section_data[$field],
                    );
                }
            }
        } else{
            foreach($fields as $field){
                $data[$field] = array(
                    'orig_value' => $section_data[$field],
                    'trans_value' => $section_data[$field],
                );
            }
        }

        return $data;
    }

    public function saveTranslation()
    {
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        $section = Mage::getModel('translation/item')->getCollection()->addFieldToFilter('store_id',parent::getTranslatorStoreId())->addFieldToFilter('ref_id',$_POST['ref_id'])->getFirstItem();

        if($section->getId()){
            //Update
            $section->setTitle($_POST['title'])->setContent($_POST['content'])->save();
        } else{
            //Save new
            $data = Mage::getModel('translation/item')->load($_POST['id'])->getData();
            unset($data['id']);

            Mage::getModel('translation/item')
                ->setData($data)
                ->setStoreId(parent::getTranslatorStoreId())
                ->setTitle($_POST['title'])
                ->setContent($_POST['content'])
                ->setParentId($this->getParentSectionId($_POST['parent_id']))
                ->save();
        }

        Mage::getSingleton('core/session')->addSuccess(ucfirst(parent::getTranslatorType()).' saved');
    }

    public function getOverview()
    {
        $translated = Mage::getModel('translation/item')->getCollection()->addFieldToFilter('store_id', parent::getTranslatorStoreId());
        $data = [];

        foreach($translated as $item){
            $orig_item = Mage::getModel('translation/item')->getCollection()->addFieldToFilter('store_id',8)->addFieldToFilter('ref_id',$item->getRefId())->getFirstItem();
            $data[] = array(
                'id' => $orig_item->getId(),
                'info' => '<p>'.$orig_item->getTitle().'</p><p>- <strong>'.$item->getTitle().'</strong></p>',
            );
        }

        return $data;
    }
}