<?php

class Sinful_Translation_Model_Translators_Email extends Sinful_Translation_Model_Translator
{
    protected static $_templates_configs = [
        #'sales_email:template',
        'sales_email:guest_template',
        'creditmemo:template',
        #'shipment:template',
        'shipment:guest_template',
        #'invoice:template',
        'invoice:guest_template',
        //'Ny Kreditnota For Gæst - DK',

    ];

    protected $_current_config;
    static $sync_store;

    protected function _construct()
    {
        if(parent::isStoreSynced()){
            $this::$sync_store = parent::getSyncedStore();
        }
    }

    public function isStoreSync()
    {
        if($store = $this::$sync_store){
            return $store;
        }
        return false;
    }

    public function getTemplateConfig($template_config, $store_id)
    {
        $parts = explode(':',$template_config);
        $type = $parts[0];
        $config = $parts[1];

        switch($type){
            case 'creditmemo':
                return (int)Mage::getStoreConfig(('sales_email/creditmemo/'.$config), $store_id);
                break;
            case 'sales_email':
                return (int)Mage::getStoreConfig(('sales_email/order/'.$config), $store_id);
                break;

            case 'shipment':
                return (int)Mage::getStoreConfig(('sales_email/shipment/'.$config), $store_id);
                break;
            case 'invoice':
                return (int)Mage::getStoreConfig(('sales_email/invoice/'.$config), $store_id);
                break;

        }

        return false;
    }

    public function setTemplateConfig($template_config, $entity_id, $store_id){
        $parts = explode(':',$template_config);
        $type = $parts[0];
        $config = $parts[1];

        $scope = 'websites';
        $value = Mage::getModel('core/store')->load($store_id)->getWebsiteId();

        if($this->isStoreSync()){
            $scope = 'stores';
            $value = $store_id;
        }

        switch($type){
            case 'creditmemo':
                Mage::getModel('core/config')->saveConfig(('sales_email/creditmemo/'.$config), $entity_id, $scope, $value);
                Mage::getModel('core/config')->saveConfig(('sales_email/creditmemo/guest_template'), $entity_id, $scope, $value);
                break;

            case 'sales_email':
                Mage::getModel('core/config')->saveConfig(('sales_email/order/'.$config), $entity_id, $scope, $value);
                Mage::getModel('core/config')->saveConfig(('sales_email/order/guest_template'), $entity_id, $scope, $value);
                break;

            case 'shipment':
                Mage::getModel('core/config')->saveConfig(('sales_email/shipment/'.$config), $entity_id, $scope, $value);
                Mage::getModel('core/config')->saveConfig(('sales_email/shipment/guest_template'), $entity_id, $scope, $value);
                break;
            case 'invoice':
                Mage::getModel('core/config')->saveConfig(('sales_email/invoice/'.$config), $entity_id, $scope, $value);
                Mage::getModel('core/config')->saveConfig(('sales_email/invoice/guest_template'), $entity_id, $scope, $value);
                break;
        }
        Mage::getConfig()->reinit();
    }

    public function getMissingEmails()
    {
        $missing = [];

        foreach($this::$_templates_configs as $config){
            if(!$this->getTemplateConfig($config,parent::getTranslatorStoreId())){
                $missing[] = $config;
            }
        }

        return $missing;
    }

    public function getTranslatedEmails()
    {
        $translated = [];

        foreach($this::$_templates_configs as $config){
            if($this->getTemplateConfig($config,parent::getTranslatorStoreId())){
                $translated[] = $config;
            }
        }

        return $translated;
    }

    public function getConfigFromEntity($entity_id, $store_id)
    {
        $ids = [];
        foreach($this::$_templates_configs as $config){
            $ids[$this->getTemplateConfig($config,$store_id)] = $config;
        }

        return $ids[$entity_id];
    }


    public function getCurrentEntity(){
        if($sync = $this->isStoreSync()){
            $store = $sync;
        } else{
            $store = 2;
        }


        if($entity_id = Mage::app()->getRequest()->getParam('load_entity_id')){
            $config = $this->getConfigFromEntity($entity_id, parent::getTranslatorStoreId());
            $email = Mage::getModel('core/email_template')->load($this->getTemplateConfig($config, $store));

        } else{
            if(!empty($missing)){
                $email = Mage::getModel('core/email_template')->load($this->getTemplateConfig($missing[0], $store));
            } else{
                return false;
            }
        }

        if($email->getId() == ''){
            return false;
        } else{
            return $email;
        }
    }

    public function getCurrentEntityData()
    {
        $email = $this->getCurrentEntity();
        $image_path = '/media/sinful/translation/';

        $images = [];
        $images[] = array(
            'image_small' => $image_path.'email2.png',
            'image_big' => $image_path.'email2.png',
        );
        $images[] = array(
            'image_small' => $image_path.'email3.png',
            'image_big' => $image_path.'email3.png',
        );

        return array(
            'entity_id' => $email->getId(),
            'entity_name' => $email->getTemplateSubject(),
            'entity_image' => array(
                'small' => $image_path.'email1.png',
                'large' => $image_path.'email1.png',
            ),
            'entity_more_images' => $images,
            'original_id' => $this->getCurrentEntity()->getId(),
            'translated_id' => Mage::app()->getRequest()->getParam('load_entity_id')
        );
    }


    public function getEntitiesLeft()
    {
        $data = [];
        $data['total'] = count($this->getTranslatedEmails());
        $data['range'] = 'Not available';
        return $data;
    }

    public function getPreviousEntities()
    {
        $translated = $this->getTranslatedEmails();
        $data = array();
        foreach($translated as $email){
            $email =  Mage::getModel('core/email_template')->load($this->getTemplateConfig($email, parent::getTranslatorStoreId()));
            $data[] = array(
                'entity_id' => $email->getId(),
                'name' => $email->getTemplateSubject()
            );
        }

        return $data;
    }

    public function getTranslationFields(){
        $field_names = array('template_id','config','template_code','template_styles','template_subject','template_text','store_id','is_active');
        $value_data = $this->getFieldValues($field_names);

        $fields = [];

        $fields[] = array(
            'type' => 'hidden',
            'name' => 'template_id',
            'orig_value' => $value_data['template_id']['orig_value'],
            'trans_value' => $value_data['template_id']['trans_value'],
        );

        $fields[] = array(
            'type' => 'hidden',
            'name' => 'config',
            'orig_value' => $value_data['config']['orig_value'],
            'trans_value' => $value_data['config']['trans_value'],
        );

        $fields[] = array(
            'type' => 'hidden',
            'name' => 'template_styles',
            'orig_value' => $value_data['template_styles']['orig_value'],
            'trans_value' => $value_data['template_styles']['trans_value'],
        );


        $fields[] = array(
            'type' => 'hidden',
            'name' => 'store_id',
            'orig_value' => $value_data['store_id']['orig_value'],
            'trans_value' => $value_data['store_id']['trans_value'],
        );

        $fields[] = array(
            'type' => 'text',
            'name' => 'template_code',
            'orig_value' => $value_data['template_code']['orig_value'],
            'trans_value' => $value_data['template_code']['trans_value'],
            'desc' => mage::helper('sinful_translation')->getFieldDescription('template_code', 'email'),
        );

        $fields[] = array(
            'type' => 'text',
            'name' => 'template_subject',
            'orig_value' => $value_data['template_subject']['orig_value'],
            'trans_value' => $value_data['template_subject']['trans_value'],
            'desc' => mage::helper('sinful_translation')->getFieldDescription('template_subject', 'email')
        );

        $fields[] = array(
            'type' => 'textarea',
            'name' => 'template_text',
            'orig_value' => $value_data['template_text']['orig_value'],
            'trans_value' => $value_data['template_text']['trans_value'],
            'desc' => mage::helper('sinful_translation')->getFieldDescription('template_text', 'email')
        );

        $fields[] = array(
            'type' => 'select',
            'name' => 'is_active',
            'orig_value' => $value_data['is_active']['orig_value'],
            'trans_value' => $value_data['is_active']['trans_value'],
            'desc' => mage::helper('sinful_translation')->getFieldDescription('is_active', 'email')
        );

        return $fields;
    }


    public function getFieldValues($fields){

        $email = $this->getCurrentEntity();


        if(Mage::app()->getRequest()->getParam('load_entity_id') == ''){
            return false;
        } else{
            $email_data = $email->getData();
            if($entity = Mage::app()->getRequest()->getParam('load_entity_id')){
                $trans_data = Mage::getModel('core/email_template')->load($entity)->getData();
            }
        }
        $data = [];

        foreach($fields as $field){
            if($entity = Mage::app()->getRequest()->getParam('load_entity_id')){
                if($field == 'config'){
                    $config = $this->getConfigFromEntity($entity, parent::getTranslatorStoreId());
                    $data[$field] = array(
                        'orig_value' => $config,
                        'trans_value' => $config,
                    );
                }
                else{
                    $data[$field] = array(
                        'orig_value' => $email_data[$field],
                        'trans_value' => $trans_data[$field],
                    );
                }
            }
            else{
                if($field == 'config'){
                    if($sync = $this->getSyncedStore()){
                        $config = $this->getConfigFromEntity($email_data['template_id'], $sync);
                    } else{
                        $config = $this->getConfigFromEntity($email_data['template_id'], 8);
                    }
                    $data[$field] = array(
                        'orig_value' => $config,
                        'trans_value' => $config,
                    );
                }
                else{
                    $data[$field] = array(
                        'orig_value' => $email_data[$field],
                        'trans_value' => $email_data[$field],
                    );
                }
            }
        }


        return $data;
    }

    public function saveTranslation()
    {
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        //Make sure template code ain't default
        if($this->isStoreSynced()){
            if(strpos($_POST['template_code'],Mage::getStoreConfig('general/locale/code', parent::getTranslatorStoreId())) === false)
            {
                $start = substr($_POST['template_code'],0,4);
                $_POST['template_code'] = str_replace($start,strtoupper(Mage::getModel('core/store')->load(parent::getTranslatorStoreId())->getCode()).' - '. Mage::getStoreConfig('general/locale/code', parent::getTranslatorStoreId()) .' ('.parent::getTranslatorStoreId().') -',$_POST['template_code']);
            }
        } else{
            $_POST['template_code'] = str_replace('UK -',strtoupper(Mage::getModel('core/store')->load(parent::getTranslatorStoreId())->getCode()).' - '. Mage::getStoreConfig('general/locale/code', parent::getTranslatorStoreId()) .' ('.parent::getTranslatorStoreId().') -',$_POST['template_code']);
        }

        $emailTemplate = Mage::getModel('core/email_template')->loadByCode($_POST['template_code']);

        if($emailTemplate->getTemplateId()){
            //Set translated fields
            $emailTemplate->setTemplateSubject($_POST['template_subject']);
            $emailTemplate->setTemplateText($_POST['template_text']);
            $emailTemplate->setStoreId(parent::getTranslatorStoreId());

            if($_POST['is_active'] == 1){
                $emailTemplate->setData('is_active',1);
            }

            $emailTemplate->save();
        }
        else {
            $_email = Mage::getModel('core/email_template');
            //Set translated fields
            $_email->setData($_POST);
            $_email->setAddedAt(date('Y-m-d H:i:s'))->setModifiedAt(date('Y-m-d H:i:s'));
            $_email->setTemplateType(2); //HTML


            if($_POST['is_active'] == 1){
                 $_email->setData('is_active',1);
            }

            $_email->save();

            //Assign to config
            $this->setTemplateConfig($_POST['config'], $_email->getTemplateId() , parent::getTranslatorStoreId());
        }

        Mage::getSingleton('core/session')->addSuccess(ucfirst(parent::getTranslatorType()).' saved');
    }

    public function getOverview()
    {
        $translated = $this->getTranslatedEmails();
        $data = array();
        foreach($translated as $config){
            $email = Mage::getModel('core/email_template')->load($this->getTemplateConfig($config, parent::getTranslatorStoreId()));

            $store = 2;

            $orig_email = Mage::getModel('core/email_template')->load($this->getTemplateConfig($config, $store));

            if($email->getData('is_active') != 1){

                $data[] = array(
                    'id' => $email->getId(),
                    'info' => '<p>'.$orig_email->getTemplateCode().'</p>',
                );

            }
        }

        return $data;
    }



    public function getRetransDesc($field){

        if($field == 'template_subject'){
            return 'EMAIL SUBJECT LINE';
        }elseif($field == 'template_text') {
            return 'EMAIL TEXT';
        }elseif($field == 'is_active') {
            return 'Once translation is finished, set to “Yes”';
        }
    }

}