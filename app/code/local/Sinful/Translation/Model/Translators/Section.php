<?php

class Sinful_Translation_Model_Translators_Section extends Sinful_Translation_Model_Translator
{
    static $sync_store;

    protected function _construct()
    {
        if(parent::isStoreSynced()){
            $this::$sync_store = parent::getSyncedStore();
        }
    }

    public function isStoreSync()
    {
        if($store = $this::$sync_store){
            return $store;
        }
        return false;
    }

    public function getCurrentEntity(){
        if($entity_id = Mage::app()->getRequest()->getParam('load_entity_id')){
            $section = Mage::getModel('translation/section')->load($entity_id);
        } else{
            $section = $this->getNotTranslatedSections()->getFirstItem();
        }

        if($section->getId() == ''){
            return false;
        } else{
            return $section;
        }
    }

    public function getNotTranslatedSections()
    {
        $model = Mage::getModel('translation/section');
        $sections = $model->getCollection()->addFieldToFilter('store_id',parent::getTranslatorStoreId())->getColumnValues('source_id');

        if($store = $this->isStoreSync()){
            $left = $model->getCollection()
                ->addFieldToFilter('store_id',$store);
            if(!empty($sections)){
                $left->addFieldToFilter('source_id',array('nin' => $sections));
            }
        } else{
            $left = $model->getCollection()
                ->addFieldToFilter('store_id',8);
            if(!empty($sections)){
                $left->addFieldToFilter('id',array('nin' => $sections));
            }
        }


        return $left;
    }

    public function getMainSourceId($id)
    {
        return Mage::getModel('translation/section')->load($id)->getSourceId();
    }


    public function getCurrentEntityData()
    {
        $section = $this->getCurrentEntity();

        $image_path = '/media/trendhim/translation/';

        return array(
            'entity_id' => $section->getId(),
            'entity_name' => $section->getName(),
            'entity_image' => array(
                'small' => $image_path.'name.jpg',
                'large' => $image_path.'name.jpg',
            ),
            'entity_more_images' => '',
        );
    }

    public function getEntitiesLeft()
    {
        $data = [];
        $data['total'] = $this->getNotTranslatedSections()->getSize();
        $data['range'] = 'Not available';
        return $data;
    }

    public function getPreviousEntities()
    {
        $model = Mage::getModel('translation/section');
        $latest = $model->getCollection()->addFieldToFilter('store_id',parent::getTranslatorStoreId());

        $data = array();

        foreach($latest as $section){
            $data[] = array(
                'entity_id' => $section->getSourceId(),
                'name' => $section->getName()
            );
        }

        return $data;
    }

    public function getTranslationFields(){
        $field_names = array('id','name','icon');
        $value_data = $this->getFieldValues($field_names);

        $fields = [];
        $fields[] = array(
            'type' => 'hidden',
            'name' => 'id',
            'orig_value' => $value_data['id']['orig_value'],
            'trans_value' => $value_data['id']['trans_value'],
        );

        $fields[] = array(
            'type' => 'hidden',
            'name' => 'icon',
            'orig_value' => $value_data['icon']['orig_value'],
            'trans_value' => $value_data['icon']['trans_value'],
        );

        $fields[] = array(
            'type' => 'text',
            'name' => 'name',
            'orig_value' => $value_data['name']['orig_value'],
            'trans_value' => $value_data['name']['trans_value'],
        );

        return $fields;
    }


    public function getFieldValues($fields){

        $section = $this->getCurrentEntity();
        $use_dest_data = false;

        if($section->getId() == ''){
            return false;
        } else{
            $section_data = Mage::getModel('translation/section')->load($section->getId())->getData();

            $dest_section_data = Mage::getModel('translation/section')->getCollection()->addFieldToFilter('store_id',parent::getTranslatorStoreId())->addFieldToFilter('source_id', $section->getId())->getFirstItem()->getData();

            /** Fallback for not translated yet */
            if($dest_section_data['id']){
                $use_dest_data = true;
            }
        }
        $data = [];

        if($use_dest_data)
        {
            foreach($fields as $field){
                if($field == 'id'){
                    $data[$field] = array(
                        'orig_value' => $section_data[$field],
                        'trans_value' => $section_data[$field],
                    );
                } else{
                    $data[$field] = array(
                        'orig_value' => $section_data[$field],
                        'trans_value' => $dest_section_data[$field],
                    );
                }
            }
        } else{
            foreach($fields as $field){
                $data[$field] = array(
                    'orig_value' => $section_data[$field],
                    'trans_value' => $section_data[$field],
                );
            }
        }

        return $data;
    }

    public function saveTranslation()
    {
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);


        if(!$this->isStoreSynced()){
            $section = Mage::getModel('translation/section')->getCollection()->addFieldToFilter('store_id',parent::getTranslatorStoreId())->addFieldToFilter('source_id',$_POST['id'])->getFirstItem();
            $from_id = $_POST['id'];
        } else{
            $from_id = $this->getMainSourceId($_POST['id']);
            $section = Mage::getModel('translation/section')->getCollection()->addFieldToFilter('store_id',parent::getTranslatorStoreId())->addFieldToFilter('source_id',$from_id)->getFirstItem();
        }

        if($section->getId()){
            //Update
            $section->setName($_POST['name'])->save();
        } else{
            //Save new

            $data = Mage::getModel('translation/section')->load($from_id)->getData();
            unset($data['id']);

            Mage::getModel('translation/section')
                ->setData($data)
                ->setStoreId(parent::getTranslatorStoreId())
                ->setName($_POST['name'])
                ->save();
        }

        Mage::getSingleton('core/session')->addSuccess(ucfirst(parent::getTranslatorType()).' saved');
    }

    public function getOverview()
    {
        $translated = Mage::getModel('translation/section')->getCollection()->addFieldToFilter('store_id', parent::getTranslatorStoreId());

        $data = [];

        foreach($translated as $section){
            $orig_section = Mage::getModel('translation/section')->load($section->getSourceId());
            $data[] = array(
                'id' => $section->getSourceId(),
                'info' => '<p>'.$orig_section->getName().'</p><p>- <strong>'.$section->getName().'</strong></p>',
            );
        }

        return $data;
    }
}