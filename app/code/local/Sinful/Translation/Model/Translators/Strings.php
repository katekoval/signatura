<?php

class Sinful_Translation_Model_Translators_Strings extends Sinful_Translation_Model_Translator
{
    public function getCurrentEntity()
    {
        if ($entity_id = Mage::app()->getRequest()->getParam('load_entity_id')) {
            $string = Mage::getModel('translation/strings')
                ->getCollection()
                ->addFieldToFilter('locale', $this->getLocale())
                ->addFieldToFilter('source_id', $entity_id)
                ->getFirstItem();
        } else {
            $translated = Mage::getModel('translation/strings')
                ->getCollection()
                ->addFieldToFilter('locale', $this->getLocale())
                ->addFieldToFilter('translation', array('notnull' => true))
                ->getColumnValues('source_id');

            $string = Mage::getModel('translation/strings')->getCollection()
                ->addFieldToFilter('locale', array('null' => true));

            if (!empty($translated)) {
                $string = $string->addFieldToFilter('id', array('nin' => $translated));
            }

            $string = $string->getFirstItem()->load();
        }

        if ($string->getId() == '') {
            return false;
        } else {
            return $string;
        }
    }

    public function getCurrentEntityData()
    {
        $string = $this->getCurrentEntity();
        $image_path = '/media/sinful/translation/';

        return array(
            'entity_id' => $string->getId(),
            'entity_name' => $string->getString(),
            'entity_image' => array(
                'small' => $image_path . 'strings.jpg',
                'large' => $image_path . 'strings.jpg',
            ),
            'entity_more_images' => '',
        );
    }

    public function getEntitiesLeft()
    {
        $data = [];

        $translated = Mage::getModel('translation/strings')
            ->getCollection()
            ->addFieldToFilter('locale', $this->getLocale())
            ->addFieldToFilter('translation', array('notnull' => true))
            ->getColumnValues('source_id');


        $string = Mage::getModel('translation/strings')->getCollection()
            ->addFieldToFilter('locale', array('null' => true));

        if (!empty($translated)) {
            $string = $string->addFieldToFilter('id', array('nin' => $translated));
        }

        $data['total'] = $string->getSize();
        $data['range'] = 'Not available';

        return $data;
    }

    public function getPreviousEntities()
    {
        $latest = Mage::getModel('translation/strings')->getCollection()
            ->addFieldToFilter('locale', $this->getLocale())
            ->addFieldToFilter('translation', array('notnull' => true))
            ->addFieldToFilter('source_id', array('lt' => $this->getCurrentEntity()->getId()))
            ->setOrder('id', 'DESC')
            ->setPageSize(5);

        $data = array();

        foreach ($latest as $string) {
            $data[] = array(
                'entity_id' => $string->getSourceId(),
                'name' => $string->getString()
            );
        }

        return $data;
    }

    public function getTranslationFields()
    {
        $field_names = array('id', 'string', 'translation');
        $value_data = $this->getFieldValues($field_names);

        $fields = [];

        $fields[] = array(
            'type' => 'hidden',
            'name' => 'id',
            'orig_value' => $value_data['id']['orig_value'],
            'trans_value' => $value_data['id']['trans_value'],
        );

        $fields[] = array(
            'type' => 'hidden',
            'name' => 'string',
            'orig_value' => $value_data['string']['orig_value'],
            'trans_value' => $value_data['string']['trans_value'],
        );

        $fields[] = array(
            'type' => 'text',
            'name' => 'translation',
            'orig_value' => $value_data['translation']['orig_value'],
            'trans_value' => $value_data['translation']['trans_value'],
        );

        return $fields;
    }


    public function getFieldValues($fields)
    {

        $string = $this->getCurrentEntity();

        if ($string->getId() == '') {
            return false;
        } else {
            $string_data = $string->getData();
        }
        $data = [];

        foreach ($fields as $field) {
            if ($field == 'translation') {
                $data[$field] = array(
                    'orig_value' => $string_data['string'],
                    'trans_value' => $string_data['translation'],
                );
            } elseif ($field == 'id') {
                if(Mage::app()->getRequest()->getParam('load_entity_id')){
                    $data[$field] = array(
                        'orig_value' => $string_data['id'],
                        'trans_value' => $string_data['source_id'],
                    );
                } else{
                    $data[$field] = array(
                        'orig_value' => $string_data[$field],
                        'trans_value' => $string_data[$field],
                    );
                }

            } else {
                $data[$field] = array(
                    'orig_value' => $string_data[$field],
                    'trans_value' => $string_data[$field],
                );
            }
        }

        return $data;
    }

    public function saveTranslation()
    {
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        $string = Mage::getModel('translation/strings')->getCollection()
            ->addFieldToFilter('locale', $this->getLocale())
            ->addFieldToFilter('source_id', $_POST['id'])->getFirstItem();

        if ($string->getId()) {
            $string->setTranslation($_POST['translation'])->save();
        } else {
            Mage::getModel('translation/strings')
                ->setLocale($this->getLocale())
                ->setString($_POST['string'])
                ->setSourceId($_POST['id'])
                ->setTranslation($_POST['translation'])
                ->save();
        }

        Mage::getSingleton('core/session')->addSuccess(ucfirst(parent::getTranslatorType()) . ' saved');
    }

    public function getLocale()
    {
        return Mage::getStoreConfig('general/locale/code', parent::getTranslatorStoreId());
    }

    public function getOverview()
    {
        $translated = Mage::getModel('translation/strings')
            ->getCollection()
            ->addFieldToFilter('locale', $this->getLocale())
            ->addFieldToFilter('translation', array('notnull' => true));

        $data = [];

        foreach($translated as $string){
            $data[] = array(
                'id' => $string->getSourceId(),
                'info' => '<p>'.$string->getString().'</p><p>- <strong>'.$string->getTranslation().'</strong></p>',
            );
        }

        return $data;
    }
}