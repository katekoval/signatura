<?php

class Sinful_Translation_Model_Sync extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('translation/sync');
        date_default_timezone_set('Europe/Copenhagen');
    }

    private function updateSyncStatus($entity_id, $type, $dest_store_id, $source_store_id){
        if(!$this->isSyncReady($entity_id, $dest_store_id, $type)){
            return;
        }

        $_source_stores_mappings = $this->getRelevantStoreSyncMapping($dest_store_id);

        foreach($_source_stores_mappings as $source_store => $dest_store){
            $sync_item = $this->getCollection()
                ->addFieldToFilter('entity_id',$entity_id)
                ->addFieldToFilter('type',$type)
                ->addFieldToFilter('source_store',$source_store)
                ->addFieldToFilter('destination_store',$dest_store)
                ->getFirstItem();

            if($sync_item->getId()){
                $sync_item
                    ->setSync(1)
                    ->setSyncRequestedAt(date('Y-m-d H:i:s'))
                    ->save();
            }
            else {
                Mage::getModel('translation/sync')
                    ->setEntityId($entity_id)
                    ->setType($type)
                    ->setCreatedAt(date('Y-m-d H:i:s'))
                    ->setSyncRequestedAt(date('Y-m-d H:i:s'))
                    ->setSync(1)
                    ->setFirstTimeSync(1)
                    ->setSourceStore($source_store)
                    ->setDestinationStore($dest_store)
                    ->save();
            }
        }
    }

    public function getStoreSyncMappings()
    {
        $mappings = array(
            1 => 8,
            10 => 12,
        );
        return $mappings;
    }

    private function getRelevantStoreSyncMapping($dest_store_id){

        $_source_stores_mappings = $this->getStoreSyncMappings();
        $_website_stores = $this->getWebsiteStores($dest_store_id);
        $_not_relevant_stores = array_diff($_source_stores_mappings,$_website_stores);

        foreach($_not_relevant_stores as $_store){
            if(($key = array_search($_store, $_source_stores_mappings)) !== false) {
                unset($_source_stores_mappings[$key]);
            }
        }

        return $_source_stores_mappings;
    }

    private function getWebsiteStores($store_id){
        $website_id = Mage::getModel('core/store')->load($store_id)->getWebsiteId();
        $website = Mage::getModel('core/website')->load($website_id);
        $website_store_ids = [];

        foreach ($website->getGroups() as $group) {
            $stores = $group->getStores();
            foreach ($stores as $store) {
                $website_store_ids[] = $store->getId();
            }
        }

        return $website_store_ids;
    }

    public function isSyncReady($entity_id, $dest_store_id, $type)
    {
        //Lock to prevent default value ever getting overridden
        if($dest_store_id == 0){
            return false;
        }

        //Let's find out which store ids this website contains
        $website_store_ids = $this->getWebsiteStores($dest_store_id);

        //Which stores are these mapped to?
        $mappings = $this->getStoreSyncMappings();
        $source_store_ids = [];

        foreach($website_store_ids as $store_id){
            if($key = array_search($store_id,$mappings)){
                $source_store_ids[] = $key;
            }
        }


        if($type == 'product'){
            $attributeCode = 'status';
            $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
            $attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode(Mage_Catalog_Model_Product::ENTITY, $attributeCode);
            $table = $attributeModel->getBackend()->getTable();
            $entityTypeIdField = Mage::getModel('eav/entity')->setType(Mage_Catalog_Model_Product::ENTITY)->getEntityIdField();

            $select = $adapter->select()
                ->from($table, array())
                ->where($entityTypeIdField . ' =?', $entity_id)
                ->where('attribute_id =?', $attributeModel->getId())
                ->where('store_id IN(?)', $source_store_ids)
                ->columns(
                    '*'
                );

            $values = $adapter->fetchAll($select);

            foreach($values as $value){
                if($value['value'] == 2){
                    return false;
                }
            }
            return true;

        } elseif($type == 'category'){
            $attributeCode = 'name';
            $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
            $attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode(Mage_Catalog_Model_Category::ENTITY, $attributeCode);
            $table = $attributeModel->getBackend()->getTable();
            $entityTypeIdField = Mage::getModel('eav/entity')->setType(Mage_Catalog_Model_Category::ENTITY)->getEntityIdField();

            $select = $adapter->select()
                ->from($table, array())
                ->where($entityTypeIdField . ' =?', $entity_id)
                ->where('attribute_id =?', $attributeModel->getId())
                ->where('store_id IN(?)', $source_store_ids)
                ->columns(
                    '*'
                );


            $values = $adapter->fetchAll($select);

            if(!empty($values)){
                return true;
            }
            return false;
        }

        //Return false in case we don't know which type this is
        return false;
    }

    private function sync_store($source_store, $destination_store)
    {
        //Sync products
        $this->updateSyncList($destination_store, $source_store, 'product');

        //Sync categories
        $this->updateSyncList($destination_store, $source_store, 'category');
    }


    public function sync()
    {
        $syncs = $this->getStoreSyncMappings();
        foreach($syncs as $key => $sync){
            $source_store = $key;
            $destination_store = $sync;
            $this->sync_store($source_store, $destination_store);
        }
    }


    public function updateSyncList($dest_store_id, $source_store_id, $sync_type)
    {
        if($sync_type == 'product'){
            $source_products =
                Mage::getModel('catalog/product')->getCollection()
                    ->addStoreFilter($source_store_id)
                    ->addAttributeToFilter('status',1)
                    ->addAttributeToFilter('visibility', 4)
                    //->addAttributeToFilter('entity_id', 5100)
                    ->joinField('stock_status','cataloginventory/stock_status','stock_status',
                        'product_id=entity_id', array(
                            'stock_status' => Mage_CatalogInventory_Model_Stock_Status::STATUS_IN_STOCK,
                            'website_id' => 1,
                        ))
                    ->getColumnValues('entity_id');

            //Search for any new  products
            $attributeCode = 'name';
            $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
            $attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode(Mage_Catalog_Model_Product::ENTITY, $attributeCode);
            $table = $attributeModel->getBackend()->getTable();
            $entityTypeIdField = Mage::getModel('eav/entity')->setType(Mage_Catalog_Model_Product::ENTITY)->getEntityIdField();

            foreach ($source_products as $product_id)
            {
                $select = $adapter->select()
                    ->from($table, array())
                    ->where($entityTypeIdField . ' =?', $product_id)
                    ->where('attribute_id =?', $attributeModel->getId())
                    ->where('store_id =?', $dest_store_id)
                    ->columns(
                        '*'
                    );
                $values = $adapter->fetchAll($select);

                if (empty($values)) {
                    //First time sync
                    $this->updateSyncStatus($product_id,$sync_type, $dest_store_id, $source_store_id);
                }
            }
        } elseif($sync_type == 'category'){
            $source_categories =
                Mage::getModel('catalog/category')->setStoreId($source_store_id)->getCollection()
                    ->addIsActiveFilter()
                    ->getColumnValues('entity_id');

            //Search for any new categories
            $attributeCode = 'name';
            $adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
            $attributeModel = Mage::getModel('eav/entity_attribute')->loadByCode(Mage_Catalog_Model_Category::ENTITY, $attributeCode);
            $table = $attributeModel->getBackend()->getTable();
            $entityTypeIdField = Mage::getModel('eav/entity')->setType(Mage_Catalog_Model_Category::ENTITY)->getEntityIdField();

            foreach ($source_categories as $category_id)
            {
                $select = $adapter->select()
                    ->from($table, array())
                    ->where($entityTypeIdField . ' =?', $category_id)
                    ->where('attribute_id =?', $attributeModel->getId())
                    ->where('store_id =?', $dest_store_id)
                    ->columns(
                        '*'
                    );
                $values = $adapter->fetchAll($select);

                if (empty($values)) {
                    //First time sync
                    $this->updateSyncStatus($category_id,$sync_type, $dest_store_id, $source_store_id);
                }
            }
        }
    }

    public function sync_proccess()
    {
        $items_to_proccess = 5;
        $items = $this->getCollection()
            ->addFieldToFilter('sync',1)
            ->setPageSize($items_to_proccess);

        foreach($items as $item){

            if($item->getType() == 'product'){
                $source_product = Mage::getModel('catalog/product')->setStoreId($item->getSourceStore())->load($item->getEntityId());
                $source_data = $source_product->getData();
                unset($source_data['id']);
                unset($source_data['store_id']);

                $dest_product = Mage::getModel('catalog/product')->load($item->getEntityId())->setStoreId($item->getDestinationStore());
                $dest_product->setData($source_data)->setStoreId($item->getDestinationStore())->setStatus(1)->save();

            } elseif($item->getType() == 'category'){
                $source_category = Mage::getModel('catalog/category')->setStoreId($item->getSourceStore())->load($item->getEntityId());
                $source_data = $source_category->getData();
                unset($source_data['id']);
                unset($source_data['store_id']);

                $dest_category = Mage::getModel('catalog/category')->load($item->getEntityId())->setStoreId($item->getDestinationStore());
                $dest_category
                    ->setData($source_data)
                    ->setStoreId($item->getDestinationStore())
                    ->setStatus(1);

                $replace_url = Mage::app()->getStore($item->getSourceStore())->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);
                $destination_url =  Mage::app()->getStore($item->getDestinationStore())->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);
                $dest_category->setDescription(str_replace($replace_url,$destination_url,$source_data['description']));
                $dest_category->setDescriptionBottom(str_replace($replace_url,$destination_url,$source_data['description_bottom']));

                $dest_category->save();
            }

            $item
                ->setSync(0)
                ->setFirstTimeSync(0)
                ->setUpdatedAt(date('Y-m-d H:i:s'))
                ->save();
        }
    }

    public function afterProductSave($observer)
    {
        $product = $observer->getEvent()->getProduct();
        $mappings = $this->getStoreSyncMappings();

        if(array_key_exists($product->getStoreId(),$mappings)){
            $stock = Mage::getModel('cataloginventory/stock_item')->loadByProduct($product);
            if($stock->getIsInStock() == 1 && $product->getStatus() == 1){
                $this->updateSyncStatus($product->getId(),'product', $mappings[$product->getStoreId()], $product->getStoreId());
            }
        }
    }

    public function afterCategorySave($observer)
    {
        $category = $observer->getEvent()->getCategory();
        $mappings = $this->getStoreSyncMappings();

        $category = Mage::getModel('catalog/category')->setStoreId($category->getStoreId())->load($category->getId());

        if(array_key_exists($category->getStoreId(),$mappings)){
            if($category->getIsActive() == 1){
                $this->updateSyncStatus($category->getId(),'category', $mappings[$category->getStoreId()], $category->getStoreId());
            }
        }
    }

}