<?php

class Sinful_Translation_Model_Strings extends Mage_Core_Model_Abstract
{
    const CACHE_TAG         = 'translation';

    protected $_data = array();
    protected $_locale;
    protected $_cacheId;

    protected $_config;
    const CONFIG_KEY_LOCALE = 'locale';


    protected function _construct()
    {
        $this->_init('translation/strings');
    }

//    public function init($forceReload = false)
//    {
//        $this->setConfig();
//
//        if (!$forceReload) {
//            $this->_data = $this->_loadCache();
//            if ($this->_data !== false) {
//                return $this;
//            }
//
//            Mage::app()->removeCache($this->getCacheId());
//        }
//
//        //No cache was found, fill cache
//        $this->_data = array();
//        $this->_loadTranslations($forceReload);
//
//        if (!$forceReload) {
//            $this->_saveCache();
//        }
//
//        return $this;
//    }

    protected function _loadTranslations()
    {
        //Load data from database
        $strings = $this->getCollection()->addFieldToFilter('locale',$this->getLocale());

        foreach ($strings as $string) {
            $from = $string->getString();
            $to = $string->getTranslation();

            $this->_data[$from] = $to;
        }
        return $this;
    }


    public function _getTranslatedString($text)
    {
        if (array_key_exists($text, $this->getData())) {
            return $this->_data[$text];
        }

        return false;
    }

    public function searchForStrings()
    {
        $sourcePaths = array(
            Mage::getBaseDir('app').'/design/frontend/sinful/default/template',
            Mage::getBaseDir('app').'/design/frontend/sinful/default/template'
        );
        var_dump($sourcePaths);
        $searchExtensions = array('php', 'phtml');
        $strTranslateFunc = '->__';

        $arrItems = array();

        foreach ($sourcePaths as $sourcePath)
        {
            $di = new RecursiveDirectoryIterator($sourcePath);
            foreach (new RecursiveIteratorIterator($di) as $filename => $file) {
                if (in_array( strtolower( pathinfo($file, PATHINFO_EXTENSION) ), $searchExtensions))
                {
                    $strContent = file_get_contents($filename);
                    $arrMatches = array();
                    preg_match_all("/{$strTranslateFunc}\((?:(?:\"(?:\\\\\"|[^\"])+\")|(?:'(?:\\\'|[^'])+'))/is", $strContent, $arrMatches);
                    foreach($arrMatches as $matched) {
                        foreach($matched as $match) {
                            $_item = trim( str_replace("{$strTranslateFunc}(", '', $match) , "'\"");
                            if ( ! in_array($_item, $arrItems))
                            {
                                $arrItems[] = $_item;
                            }
                        }
                    }
                }
            }
        }

        //Be sure translations are loaded
        $this->_loadTranslations();

        foreach($arrItems as $string){
            if (!array_key_exists($string, $this->getData())) {
                /*Mage::getModel('translation/strings')
                    ->setString($string)
                    ->save();*/

                $write_adapter = Mage::getSingleton('core/resource')->getConnection('core_write');
                $read_adapter = Mage::getSingleton('core/resource')->getConnection('core_read');
                $table = 'sinful_translation_strings';

                $select = $read_adapter->select()
                    ->from($table, array())
                    ->where('string =?', $string)
                    ->where('locale IS NULL')
                    ->columns(
                        'id'
                    );

                $values = $read_adapter->fetchAll($select);

                if (empty($values)) {
                    //String does not exist, inserting
                    $data = array(
                        'string' => $string,
                    );
                    $write_adapter->insert($table, $data);

                    echo 'Creating: '.$string.'
                ';
                }
            }
        }

        //Remove not used
        $arrItemsFormat = [];
        foreach($arrItems as $item){
            $arrItemsFormat[$item] = $item;
        }

        $remove = array_diff_key($this->getData(),$arrItemsFormat);

        foreach ($remove as $delete){
            echo 'Deleting: '.$delete.'
                ';
        }
    }

    public function getStringsForTranslation()
    {
        $collection = $this->getCollection()
        ->addFieldToFilter('locale', array('null' => true));

        return $collection;
    }

    public function getMissingTranslations($locale){

        $translated = Mage::getModel('translation/strings')->getCollection()
            ->addFieldToFilter('locale', $locale)
            ->getColumnValues('source_id');

        $missing_translation = $this->getCollection()
            ->addFieldToFilter('locale', array('null' => true));

        if(!empty($translated)){
            $missing_translation->addFieldToFilter('id', array('nin'=> $translated));
        }

    }

    public function importFromTable()
    {
        $write_adapter = Mage::getSingleton('core/resource')->getConnection('core_write');
        $read_adapter = Mage::getSingleton('core/resource')->getConnection('core_read');

        $table = 'sinful_translation_strings';
        $import_table = 'sinful_translation_strings_import';

        $imports = array(1,2,3,4,5,6);

        foreach($imports as $store){

            $locale = Mage::getStoreConfig('general/locale/code',$store);

            foreach($this->getStringsForTranslation() as $string_for_trans){

                $trans = $read_adapter->select()
                    ->from($import_table, array())
                    ->where('string =?', $string_for_trans->getString())
                    ->where('locale =?', $locale)
                    ->columns(
                        '*'
                    );

                $values = $read_adapter->fetchAll($trans);


                if (empty($values)) {
                    //Translation string not found, inserting null
                    $data = array(
                        'string' => $string_for_trans->getString(),
                        'source_id' => $string_for_trans->getId(),
                        'locale' => $locale,
                    );

                    $write_adapter->insert($table, $data);

                    echo 'Creating: '.$string_for_trans->getString().' for: '.$locale.'
                ';
                } else{
                    //Translation found
                    $data = array(
                        'string' => $string_for_trans->getString(),
                        'source_id' => $string_for_trans->getId(),
                        'translation' => $values[0]['translation'],
                        'locale' => $locale,
                    );

                    $write_adapter->insert($table, $data);

                    echo 'Creating: '.$string_for_trans->getString().' for: '.$locale.'
                ';
                }
            }
        }

    }

    public function importTranslations()
    {
        $appEmulation = Mage::getSingleton('core/app_emulation');
        $imports = array(1,2,3,4,5,6);

        foreach($imports as $store){
            $initialEnvironmentInfo = $appEmulation->startEnvironmentEmulation($store);

            $locale = Mage::app()->getLocale()->getLocaleCode();
            Mage::app()->getTranslator()->init('frontend', true);

            foreach($this->getStringsForTranslation() as $string_for_trans){

                $string = Mage::getModel('translation/strings')->getCollection()
                    ->addFieldToFilter('locale', $locale)
                    ->addFieldToFilter('source_id', $string_for_trans->getId())
                    ->getFirstItem();

                $translation = Mage::helper('core')->__($string_for_trans->getString());

                if($string->getId()){
                    if($translation != $string_for_trans->getString() || $store == 1){
                        $string->setTranslation(Mage::helper('core')->__($string_for_trans->getString()))->save();
                    }
                } else{
                    echo $string_for_trans->getString().' - Not found,<br> Query:'.Mage::getModel('translation/strings')->getCollection()
                        ->addFieldToFilter('locale', $locale)
                        ->addFieldToFilter('source_id', $string_for_trans->getId())->getSelect().'<br><br>';

                    $trans = Mage::getModel('translation/strings')
                        ->setLocale($locale)
                        ->setSourceId($string_for_trans->getId())
                        ->setString($string_for_trans->getString());

                    $translation = Mage::helper('core')->__($string_for_trans->getString());
                    if($translation != $string_for_trans->getString() || $store == 1){
                        $trans->setTranslation($translation);
                    }
                    $trans->save();
                }
            }

            $appEmulation->stopEnvironmentEmulation($initialEnvironmentInfo);
        }
    }

    public function convertTranslationToEn()
    {
        $danish = $this->getCollection()
            ->addFieldToFilter('locale','da_DK');

        foreach ($danish as $dk){
            $dk->setTranslation($dk->getString())->save();
        }

        //First lets get the EN

        $english = $this->getCollection()
            ->addFieldToFilter('locale','en_GB');

        foreach ($english as $en){

            $en_trans = $en->getTranslation();

            if($en_trans){
                //Fix base
                $this->load($en->getSourceId())
                    ->setString($en->getTranslation())
                    ->save();

                //Fix rest
                $translations = $this->getCollection()
                    ->addFieldToFilter('source_id',$en->getSourceId());

                foreach($translations as $trans){
                    $trans
                        ->setString($en_trans)
                        ->save();
                }
            }
        }

    }

    public function editTemplatesToEn()
    {
        $sourcePaths = array(
            Mage::getBaseDir('app').'/shop/design/frontend/sinful/default/template',
            Mage::getBaseDir('app').'/shop/design/frontend/sinful/default/template'
        );

        $searchExtensions = array('php', 'phtml');
        $strTranslateFunc = '->__';

        $arrItems = array();

        foreach ($sourcePaths as $sourcePath)
        {
            $di = new RecursiveDirectoryIterator($sourcePath);
            foreach (new RecursiveIteratorIterator($di) as $filename => $file) {
                if (in_array( strtolower( pathinfo($file, PATHINFO_EXTENSION) ), $searchExtensions))
                {
                    $strContent = file_get_contents($filename);
                    $arrMatches = array();
                    preg_match_all("/{$strTranslateFunc}\((?:(?:\"(?:\\\\\"|[^\"])+\")|(?:'(?:\\\'|[^'])+'))/is", $strContent, $arrMatches);
                    foreach($arrMatches as $matched) {
                        foreach($matched as $match) {
                            $_item = trim( str_replace("{$strTranslateFunc}(", '', $match) , "'\"");

                            $compare_string = str_replace('?','_',$_item);
                            $compare_string = str_replace('\\\'','__',$compare_string);
                            $source_id = $this->getCollection()
                                ->addFieldToFilter('locale', 'da_DK')
                                ->addFieldToFilter('translation', array('like'=> $compare_string))->getFirstItem()->getSourceId();

                            if(!$source_id){
                                //If not found, it's prolly in english in template, try finding default english

                                /*$source_id = $this->getCollection()
                                    ->addFieldToFilter('locale', array('null' => true))
                                    ->addFieldToFilter('string', array('like'=> $compare_string))->getFirstItem()->getId();

                                $english_item = $this->load($source_id)->getString();*/

                            } else{
                                $english_item = $this->load($source_id)->getString();
                            }


                            if($source_id){

                                $file_contents = $strContent;
                                $new_file_contents = str_replace("__('".$_item,"__('".$english_item,$file_contents);
                                $new_file_contents = str_replace('__("'.$_item,'__("'.$english_item,$new_file_contents);

                                echo $_item. ' => '.$english_item.' ('.$source_id.')
                            ';
                                file_put_contents($file,$new_file_contents);
                                $strContent = $new_file_contents;
                            } /*else{
                                echo 'Could not find: '.$_item.'
                                ';
                            }*/
                        }
                    }
                }
            }
        }
    }



    public function flushCache($observer)
    {
        if($observer->getType() == 'translate'){
            foreach (Mage::app()->getWebsites() as $website) {
                foreach ($website->getGroups() as $group) {
                    $stores = $group->getStores();
                    foreach ($stores as $store) {
                        $locale = Mage::getStoreConfig('general/locale/code', $store->getId());
                        Mage::app()->removeCache('translation_'.$locale);
                    }
                }
            }
        }
    }

    protected function _loadCache()
    {
        $data = Mage::app()->loadCache($this->getCacheId());
        $data = unserialize($data);
        return $data;
    }

    protected function _saveCache()
    {
        Mage::app()->saveCache(serialize($this->getTransData()), $this->getCacheId(), array(self::CACHE_TAG), null);
        return $this;
    }


    public function getCacheId()
    {
        if (is_null($this->_cacheId)) {
            $this->_cacheId = 'translation';
            if (isset($this->_config[self::CONFIG_KEY_LOCALE])) {
                $this->_cacheId.= '_'.$this->_config[self::CONFIG_KEY_LOCALE];
            }
        }
        return $this->_cacheId;
    }

    public function getLocale()
    {
        if (is_null($this->_locale)) {
            $this->_locale = Mage::app()->getLocale()->getLocaleCode();
        }
        return $this->_locale;
    }

    public function setConfig()
    {
        if (!isset($this->_config[self::CONFIG_KEY_LOCALE])) {
            $this->_config[self::CONFIG_KEY_LOCALE] = $this->getLocale();
        }
        return $this;
    }

    public function getTransData()
    {
        if (is_null($this->_data)) {
            return array();
            //Mage::throwException('Translation data is not initialized. Please contact developers.');
        }
        return $this->_data;
    }
}