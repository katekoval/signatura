<?php

class Sinful_Translation_Model_Timetracking extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('translation/timetracking');
    }

    public function getItemsTranslated($_user, $type = 'all')
    {
        if($type == 'all'){
            return (int)$this->getCollection()
                ->addFieldToFilter('user',$_user)
                ->addFieldToFilter('time', array('neq' => 0))
                ->getSize();
        } else{
            return (int)$this->getCollection()
                ->addFieldToFilter('user',$_user)
                ->addFieldToFilter('type',$type)
                ->addFieldToFilter('time', array('neq' => 0))
                ->getSize();
        }
    }


    public function getTotalTimeSpend($_user, $type = 'all')
    {
        if($type == 'all'){
            return (int)array_sum($this->getCollection()->addFieldToFilter('user',$_user)->getColumnValues('time'));
        } else{
            return (int)array_sum($this->getCollection()
                ->addFieldToFilter('user',$_user)
                ->addFieldToFilter('type',$type)
                ->addFieldToFilter('time', array('neq' => 0))
                ->getColumnValues('time'));
        }
    }

    public function getProductSpeed($_user)
    {
        $qty = $this->getItemsTranslated($_user,'product');
        $time = $this->getTotalTimeSpend($_user,'product')/60;

        if($time < 1) {
            $time = 1;
        }

        if($qty < 1) {
            $qty = 1;
        }

        return round($time/$qty,2);
    }
}