<?php

class Sinful_Translation_Model_Mysql4_Timetracking_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    protected function _construct()
    {
        $this->_init('translation/timetracking');
    }
}