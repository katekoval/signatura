<?php

class Sinful_Translation_Model_Mysql4_Review extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init('translation/review' , 'entity_id');
    }
}