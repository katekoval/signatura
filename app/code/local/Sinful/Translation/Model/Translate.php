<?php

class Sinful_Translation_Model_Translate extends Mage_Core_Model_Translate
{
    protected function _getTranslatedString($text, $code)
    {
    	try{
	    	if($translation = Mage::getModel('translation/strings')->init()->_getTranslatedString($text)){
	            return $translation;
	        }

        	return parent::_getTranslatedString($text,$code);

    	} catch(\Exception $e){
        	return parent::_getTranslatedString($text,$code);
    	}

    }
}