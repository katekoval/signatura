<?php

class Sinful_Translation_Block_Adminhtml_Translator_Select extends Mage_Core_Block_Template
{
    public function getTranslators()
    {
        return Mage::getModel('translation/translator')->getTranslators();
    }
}