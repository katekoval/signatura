<?php

class Sinful_Translation_Block_Adminhtml_Translator_Productapprove extends Mage_Core_Block_Template
{
    private function getTranslator()
    {
        if (Mage::registry('translator')) {
            return Mage::registry('translator');
        }

        $translator = Mage::getModel('translation/translator');
        Mage::register('translator', $translator);
        return $translator;
    }

    public function getTranslatorType()
    {
        return $this->getTranslator()->getTranslatorType();
    }

    public function getTranslatorProductapprove()
    {
        return $this->getTranslator()->getTranslatorProductapprove();
    }
}