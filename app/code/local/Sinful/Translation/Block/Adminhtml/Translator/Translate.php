<?php

class Sinful_Translation_Block_Adminhtml_Translator_Translate extends Mage_Core_Block_Template
{
    private function getTranslator()
    {
        if (Mage::registry('translator')) {
            return Mage::registry('translator');
        }

        $translator = Mage::getModel('translation/translator');
        Mage::register('translator', $translator);
        return $translator;
    }

    public function canTranslate()
    {
        return $this->getTranslator()->canTranslate();
    }

    public function getTranslatorType()
    {
        return $this->getTranslator()->getTranslatorType();
    }

    public function getTranslationFields()
    {
        return $this->getTranslator()->getTranslationFields();
    }

    public function getPreviousEntities()
    {
        return $this->getTranslator()->getPreviousEntities();
    }

    public function getCurrentEntityData()
    {
        return $this->getTranslator()->getCurrentEntityData();
    }

    public function getEntitiesLeft()
    {
        return $this->getTranslator()->getEntitiesLeft();

    }
}