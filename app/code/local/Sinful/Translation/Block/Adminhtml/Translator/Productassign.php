<?php

class Sinful_Translation_Block_Adminhtml_Translator_Productassign extends Mage_Adminhtml_Block_Widget_Grid
{
    private function getTranslator()
    {
        if (Mage::registry('translator')) {
            return Mage::registry('translator');
        }

        $translator = Mage::getModel('translation/translator');
        Mage::register('translator', $translator);
        return $translator;
    }

    public function __construct()
    {
        parent::__construct();
        $this->setId('assignGrid');
        $this->setDefaultSort('branch_potential');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $view = $this->getRequest()->getParam('view');

        $collection = $this->getTranslator()->getProductsToAssign();

        if($view == "unassigned")
        {
            $collection->getSelect()->where("assigned.assigned = '' OR assigned.assigned is null");
        }

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('entity_id',
            array(
                'header' => 'ID',
                'align' =>'left',
                'index' => 'entity_id',
                'width' => '100px',
            ));

        $this->addColumn('name',
            array(
                'header' => 'Name',
                'align' =>'left',
                'index' => 'name',
            ));

        $this->addColumn('sku',
            array(
                'header' => 'SKU',
                'align' =>'left',
                'index' => 'sku',
            ));

        $this->addColumn('branch_potential',
            array(
                'header' => 'Potential',
                'align' =>'left',
                'index' => 'branch_potential',
            ));

        $this->addColumn('branch_assigned_to',
            array(
                'header' => 'Assigned to',
                'align' =>'left',
                'index' => 'assigned',
                'sortable'  => true,
                'filter'    => false,
            ));

        $this->addColumn('branch_status',
            array(
                'header' => 'Status',
                'align' =>'left',
                'index' => 'assigned_status',
                'sortable'  => true,
                'filter'    => false,
            ));

        $this->addColumnAfter('thumb',
            array(
                'header'    => Mage::helper('catalog')->__('Thumbnail'),
                'renderer'  => 'ampgrid/adminhtml_catalog_product_grid_renderer_thumb',
                'index'		=> 'thumbnail',
                'sortable'  => true,
                'filter'    => false,
                'width'     => 90,
            ), 'entity_id');

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('assign_products');
        $this->getMassactionBlock()->setFormFieldName('assign_products');

        $users = Mage::getModel('admin/user')->getCollection()->setOrder('username', 'ASC');

        $this->getMassactionBlock()->addItem('unassign', array(
            'label'=> 'Unassign',
            'url'  => $this->getUrl('*/*/saveproductassign', array(
                'user'      => '',
                'storeid'   => $this->getRequest()->getParam('storeid'),
                'type'      => 'product'
            ))
        ));

        foreach($users as $user) {
            $this->getMassactionBlock()->addItem('assign_' . $user->getId(), array(
                'label'=> 'Assign to: ' . $user->getUsername(),
                'url'  => $this->getUrl('*/*/saveproductassign', array(
                    'user'      => $user->getUsername(),
                    'storeid'   => $this->getRequest()->getParam('storeid'),
                    'type'      => 'product'
                ))
            ));
        }

        return $this;
    }

    /**
     * Add unassigned button
     *
     * @return string
     */
    protected function _toHtml()
    {
        $view = $this->getRequest()->getParam('view');

        if($view == "unassigned")
        {
            $viewBtnMsg = 'Show assigned & unassigned';
            $view = '';
        } else {
            $viewBtnMsg = 'Only show unassigned';
            $view = 'unassigned';
        }

        $buttonUrl = $this->getUrl('*/*/*/', array(
            'storeid'   => $this->getRequest()->getParam('storeid'),
            'type'      => 'product',
            'view'      => $view,
        ));

        $html = '<button style="float:right;margin-bottom:10px;" onclick="setLocation(\'' . $buttonUrl . '\')"><span><span><span>' . $viewBtnMsg . '</span></span></span></button>';
        $html .= parent::_toHtml();

        return $html;
    }
}