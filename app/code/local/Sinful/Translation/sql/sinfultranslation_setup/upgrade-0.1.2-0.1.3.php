<?php
$installer = $this;
$installer->startSetup();


# Add proofreading for cms_page
$installer->run("
ALTER TABLE {$this->getTable('cms_page')} ADD `proofreading` INT NULL DEFAULT 0;
");

# Add proofreading for cms_block
$installer->run("
ALTER TABLE {$this->getTable('cms_block')} ADD `proofreading` INT NULL DEFAULT 0;
");

# Add proofreading for reviews
$installer->run("
ALTER TABLE {$this->getTable('review')} ADD `proofreading` INT NULL DEFAULT 0;
");

$installer->endSetup();
