<?php
$installer = $this;
$installer->startSetup();

# Add parent_id for reviews

$installer->run("
ALTER TABLE {$this->getTable('review')} ADD `parent_id` INT NULL DEFAULT 0;
");

$installer->endSetup();
