<?php
$installer = $this;
$installer->startSetup();

# Add sinful_identifier for cms_page
$installer->run("
ALTER TABLE {$this->getTable('sinful_translation_retranslate')}  ADD `human_readable` VARCHAR(25) NULL DEFAULT NULL;
");
$installer->endSetup();
