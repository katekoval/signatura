<?php
$installer = $this;
$installer->startSetup();

$installer->run("
ALTER TABLE {$this->getTable('sinful_translation_retranslate')} ADD `old_value` VARCHAR(100) NULL DEFAULT NULL;
");

$installer->run("
ALTER TABLE {$this->getTable('sinful_translation_retranslate')} ADD `is_done` INT NULL DEFAULT NULL;
");

$installer->run("
ALTER TABLE {$this->getTable('sinful_translation_retranslate')} ADD `user` VARCHAR(25) NULL DEFAULT NULL;
");


$installer->endSetup();
