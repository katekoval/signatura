<?php
$installer = $this;
$installer->startSetup();

# Add retranslation for KPI
$installer->run("
ALTER TABLE {$this->getTable('sinful_translation_changelog')} ADD `human_readable` VARCHAR(250) NULL DEFAULT NULL;
");

$installer->endSetup();
