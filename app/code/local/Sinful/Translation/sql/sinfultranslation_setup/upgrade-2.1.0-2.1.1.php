<?php
$installer = $this;
$installer->startSetup();

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'branch_to', array(
        'group'                      => 'Enrichment (Sales)',
        'input'                      => 'multiselect',
        'type'                       => 'varchar',
        'label'                      => 'Branch product to',
        'backend'                    => 'eav/entity_attribute_backend_array',
        'frontend'                   => '',
        'visible'                    => false,
        'required'                   => false,
        'visible_on_front'           => false,
        'visible_in_advanced_search' => false,
        'user_defined'               => true,
        'searchable'                 => false,
        'filterable'                 => false,
        'comparable'                 => false,
        'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'source'                     => '',
        'unique'                     => false,
        'option'                     => array ('value' =>
                                        array(
                                            'Billigvoks_dk' => array('Billigvoks'),
                                            'Signatura_dk'  => array('Signatura DK'),
                                        )),
));

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'branch_potential', array(
        'group'                      => 'Enrichment (Sales)',
        'input'                      => 'text',
        'type'                       => 'int',
        'label'                      => 'Branch Potential',
        'visible'                    => false,
        'required'                   => false,
        'visible_on_front'           => false,
        'visible_in_advanced_search' => false,
        'user_defined'               => true,
        'searchable'                 => false,
        'filterable'                 => false,
        'comparable'                 => false,
        'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'source'                     => '',
        'unique'                     => false,
));

$installer->addAttribute(Mage_Catalog_Model_Product::ENTITY, 'translator', array(
        'group'                      => 'Enrichment (Sales)',
        'input'                      => 'text',
        'type'                       => 'varchar',
        'label'                      => 'Translator/Text Author',
        'visible'                    => false,
        'required'                   => false,
        'visible_on_front'           => false,
        'visible_in_advanced_search' => false,
        'user_defined'               => true,
        'searchable'                 => false,
        'filterable'                 => false,
        'comparable'                 => false,
        'global'                     => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
        'source'                     => '',
        'unique'                     => false,
));

$installer->endSetup();