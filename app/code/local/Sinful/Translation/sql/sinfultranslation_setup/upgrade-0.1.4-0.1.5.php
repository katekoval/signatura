<?php

$installer = $this;
$installer->startSetup();
$installer->run("
ALTER TABLE {$this->getTable('sinful_translation_retranslate')} CHANGE COLUMN `old_value` `title` TEXT NULL;
");


$installer->endSetup();

