<?php
$installer = $this;
$installer->startSetup();

# Add retranslation for KPI
$installer->run("
ALTER TABLE {$this->getTable('sinful_translation_kpi')} ADD `is_retranslation` INT NULL DEFAULT NULL;
");

$installer->endSetup();
