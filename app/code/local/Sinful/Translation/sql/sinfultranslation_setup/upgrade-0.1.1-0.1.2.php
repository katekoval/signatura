<?php

$installer = $this;
$installer->startSetup();
$installer->run("
    CREATE TABLE `{$installer->getTable('sinful_translation_missing')}` (
      `entity_id` int(11) NOT NULL auto_increment,
      `source_id` int(11) NOT NULL,
      `source_store` int(11),
      `target_store` text,
      `type` text,
      `title` text,
      `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP,
      PRIMARY KEY  (`entity_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();

?>