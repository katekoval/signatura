<?php

$installer = $this;
$installer->startSetup();
$installer->run("
    CREATE TABLE `{$installer->getTable('sinful_translation_retranslate')}` (
      `entity_id` int(11) NOT NULL auto_increment,
      `source_id` int(11) NOT NULL,
      `source_store` int(11),
      `target_store` int(11),
      `field` text,
      `old_value` text,
      `new_value` text,
      `type` text,
      `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP,
      PRIMARY KEY  (`entity_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();

?>