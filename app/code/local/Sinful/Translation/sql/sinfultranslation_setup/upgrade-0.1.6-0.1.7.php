<?php
$installer = $this;
$installer->startSetup();
$installer->run("
ALTER TABLE {$this->getTable('sinful_translation_missing')} ADD `parent_id` INT(11) NULL;
");
$installer->endSetup();
