<?php
$installer = $this;
$installer->startSetup();

# Add sinful_identifier for cms_page
$installer->run("
ALTER TABLE {$this->getTable('cms_page')}  ADD `sinful_identifier` VARCHAR(100) NULL DEFAULT NULL;
");
$installer->endSetup();
