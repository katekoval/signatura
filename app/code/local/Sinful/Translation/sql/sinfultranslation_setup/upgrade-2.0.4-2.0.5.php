<?php
$installer = $this;
$installer->startSetup();

# Add store_id for reviews
$installer->run("
ALTER TABLE {$this->getTable('review')} ADD `store_id` INT NULL DEFAULT 0;
");

$installer->endSetup();
