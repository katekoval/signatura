<?php

$installer = $this;

$installer->startSetup();

$installer->run("
DROP TABLE IF EXISTS `sinful_translation_timetracking`;
CREATE TABLE `sinful_translation_timetracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `store_id` int(100) NOT NULL,
  `date` datetime NOT NULL,
  `type` varchar(250) NOT NULL,
  `entity_id` int(100) NOT NULL,
  `user` int(100) DEFAULT NULL,
  `time` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12033 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `sinful_translation_strings`;
CREATE TABLE `sinful_translation_strings` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `source_id` int(100) NOT NULL,
  `locale` varchar(250) DEFAULT NULL,
  `string` text NOT NULL,
  `translation` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23432 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `sinful_translation_item`;
CREATE TABLE `sinful_translation_item` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `source_id` int(100) NOT NULL,
  `store_id` int(100) NOT NULL,
  `parent_id` int(100) NOT NULL,
  `ref_id` int(100) NOT NULL,
  `orig_value` text,
  `trans_value` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23432 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `sinful_translation_section`;
CREATE TABLE `sinful_translation_section` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `source_id` int(100) NOT NULL,
  `store_id` int(100) NOT NULL,
  `parent_id` int(100) NOT NULL,
  `ref_id` int(100) NOT NULL,
  `orig_value` text,
  `trans_value` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23432 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS sinful_translation_sync;
CREATE TABLE `sinful_translation_sync` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `entity_id` int(100) NOT NULL,
  `source_store` int(100) NOT NULL,
  `destination_store` int(100) NOT NULL,
  `type` varchar(250) NOT NULL,
  `created_at` datetime NOT NULL,
  `sync_requested_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `first_time_sync` int(100) NOT NULL,
  `sync` int(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33941 DEFAULT CHARSET=latin1;

");


$installer->endSetup();

?>