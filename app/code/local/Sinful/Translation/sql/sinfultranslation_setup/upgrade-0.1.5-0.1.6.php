<?php

$installer = $this;
$installer->startSetup();
$installer->run("
    CREATE TABLE `{$installer->getTable('sinful_translation_kpi')}` (
      `entity_id` int(11) NOT NULL auto_increment,
      `source_id` int(11) NOT NULL,
      `source_store` int(11),
      `target_store` int(11),
      `user` text,
      `type` text,
      `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP,
      PRIMARY KEY  (`entity_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
");
$installer->endSetup();

?>