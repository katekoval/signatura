<?php
$installer = $this;
$installer->startSetup();

# Add store_id, is_active for email

$installer->run("
ALTER TABLE {$this->getTable('core_email_template')} ADD `store_id` INT NULL DEFAULT 0;
");

$installer->run("
ALTER TABLE {$this->getTable('core_email_template')} ADD `is_active` INT NULL DEFAULT 0;
");

$installer->endSetup();
