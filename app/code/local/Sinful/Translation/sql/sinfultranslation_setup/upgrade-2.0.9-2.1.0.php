<?php
$installer = $this;
$installer->startSetup();

$installer->run("
ALTER TABLE {$this->getTable('sinful_translation_item')}  ADD `assigned` VARCHAR(100) NULL DEFAULT NULL;
");

$installer->run("
ALTER TABLE {$this->getTable('sinful_translation_item')}  ADD `status` varchar(50) NULL DEFAULT NULL;
");

$installer->endSetup();