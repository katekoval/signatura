<?php
$installer = $this;
$installer->startSetup();

# Add change for retranslation
$installer->run("
ALTER TABLE {$this->getTable('sinful_translation_retranslate')} ADD `change` TEXT NULL DEFAULT NULL;
");


$installer->endSetup();
