<?php

class Sinful_Translation_Adminhtml_ReviewController extends Mage_Adminhtml_Controller_Action
{

    protected function _isAllowed(){
        #return true;
        return Mage::getSingleton('admin/session')->isAllowed('sinfulsuite/translators');
    }


    public function massReviewAction()
    {
        $entityId = $this->getRequest()->getPost('reviews', array());
        $type = 'review';

        foreach($entityId as $id){
            mage::helper('sinful_translation')->flagTranslation($id, $type);
        }
        $this->_redirect('adminhtml/catalog_product_review/');
    }

    public function massCmsblockAction()
    {
        $entityId = $this->getRequest()->getPost('block_ids', array());
        $type = 'cmsblock';

        foreach($entityId as $id){
            mage::helper('sinful_translation')->flagTranslation($id, $type);
        }

        $this->_redirect('adminhtml/cms_block/');
    }

    public function massCmspageAction()
    {
        $entityId = $this->getRequest()->getPost('block_ids', array());
        $type = 'cmspage';

        foreach($entityId as $id){
            mage::helper('sinful_translation')->flagTranslation($id, $type);
        }

        $this->_redirect('adminhtml/cms_page/');
    }


}