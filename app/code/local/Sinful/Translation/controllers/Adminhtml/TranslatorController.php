<?php

class Sinful_Translation_Adminhtml_TranslatorController extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed(){
        return Mage::getSingleton('admin/session')->isAllowed('sinfulsuite/translators');
    }

    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function translateAction()
    {
        Mage::getModel('translation/translator')->checkSave();

        $entity        = $this->getRequest()->getParam('load_entity_id');
        $storeId       = $this->getRequest()->getParam('storeid');
        $tranlatorType = $this->getRequest()->getParam('type');
        $continue      = $this->getRequest()->getParam('continue');
        $transValue    = $this->getRequest()->getParam('trans_value');
        $identifier    = $this->getRequest()->getParam('identifier');
        $newReviewId   = $this->getRequest()->getParam('newReviewId');

        if($continue == 'yes') {

            if(!empty($transValue)){
                $this->_redirect(Mage::helper('sinful_translation')->getRedirect($transValue, $storeId,$tranlatorType));
            }elseif(!empty($newReviewId)){
                $this->_redirect(Mage::helper('sinful_translation')->getRedirect($newReviewId, $storeId,$tranlatorType));
            }elseif(!empty($identifier)){
                $this->_redirect(Mage::helper('sinful_translation')->getRedirectIdentifier($identifier, $storeId, $tranlatorType));
            }else{
                $this->_redirect(Mage::helper('sinful_translation')->getRedirect($entity, $storeId,$tranlatorType));
            }

        }elseif ($continue == 'no') {
            $this->_redirect(Mage::helper('sinful_translation')->getRedirect(null, $storeId, $tranlatorType));
        }

        $this->loadLayout();
        $this->renderLayout();
    }

    public function overviewAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function productapproveAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function productassignAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function saveproductassignAction()
    {
        $productIds = $this->getRequest()->getParam('assign_products');
        $username = $this->getRequest()->getParam('user');
        $storeId = $products = $this->getRequest()->getParam('storeid');

        $assignment = Mage::getModel('translation/translators_product')->saveProductAssignment($productIds, $username, $storeId);

        if($assignment === TRUE)
        {
            if(empty($username))
            {
                $successMsg = 'Products successfully unassigned';
            } else {
                $successMsg = 'Products successfully assigned to ' . $username;
            }

            Mage::getSingleton('adminhtml/session')->addSuccess($successMsg);

            $this->_redirect('*/*/productassign', array(
                'storeid' => $storeId,
                'type'    => 'product'
            ));
        }
    }
}