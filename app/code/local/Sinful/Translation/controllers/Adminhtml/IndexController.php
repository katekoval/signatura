<?php

class Sinful_Translation_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction(){
        $this->loadLayout();
        $this->renderLayout();
    }


    public function exportAction(){
        $type = $_GET['type'];

        switch($type){
            case 'category':
                $collection = Mage::getModel('catalog/category')->setStoreId(1)->getCollection()
                    ->addAttributeToSelect('*')
                    ->addAttributeToFilter('is_active',1)
                    ->addAttributeToFilter('parent_id',2)
                    ->addAttributeToFilter('include_in_menu', 1)
                    ->addAttributeToSort('position', 'ASC');

foreach($collection as $category){
echo '-------------------- Name: - ID => '.$category->getName().' ('.$category->getId().') - (Do not translate this line)

'.$category->getName().'

';
echo '-------------------- Top tekst:

'.$category->getTopText().'

';
echo '-------------------- Top description:

'.$category->getDescription().'

';
echo '-------------------- Bottom description:
'.$category->getDescriptionBottom().'









';
$childcollection = Mage::getModel('catalog/category')->setStoreId(1)->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('is_active',1)
    ->addAttributeToFilter('parent_id',$category->getId())
    ->addAttributeToFilter('include_in_menu', 1)
    ->addAttributeToSort('position', 'ASC');

if($childcollection->count()){
    foreach($childcollection as $subcategory){
echo '-------------------- Name: - ID => '.$subcategory->getName().' ('.$subcategory->getId().') - (Do not translate this line)

'.$subcategory->getName().'

';
echo '-------------------- Top tekst:

'.$subcategory->getTopText().'

';
echo '-------------------- Top description:

'.$subcategory->getDescription().'

';
echo '-------------------- Bottom description:
'.$subcategory->getDescriptionBottom().'









';
    }
}
}
                break;
            case 'attributes':
                $collection = Mage::getModel('eav/entity_attribute')->getCollection()
                    ->addFieldToSelect('*')
                    ->addFieldToFilter(
                        array('attribute_code', 'attribute_id'),
                        array(
                            array('like'=>'%filter_%'),
                            array('in'=> array(634,527))
                        )
                    );

                foreach($collection as $attribute){
echo 'Attribute: '. $attribute->getAttributeCode().' - (do not translate this line)

';
echo 'Label: '. $attribute->getStoreLabel().' = ?

';
                    $attributeOptions = Mage::getResourceModel('eav/entity_attribute_option_collection')
                        ->setPositionOrder('asc')
                        ->setAttributeFilter($attribute->getId())
                        ->setStoreFilter($attribute->getStoreId())
                        ->load();
                    foreach($attributeOptions as $option){
echo $option->getValue().' = ?

';
                    }
echo '







';
                }



                break;

            case 'emails':

                $emails = Mage::getModel('core/email_template')->getCollection()
                    ->addFieldToSelect('*')
                    ->addFieldToFilter('template_id', array('in' => array(12,13,14,15,16,17,52,53,61)));

                foreach($emails as $email){
echo '-------------------- '.$email->getTemplateCode().' - (Do not translate this line)

';


echo '-------------------- Subject line:

'.$email->getTemplateSubject().'

';

 echo '-------------------- Email content:

'.$email->getTemplateText().'








';


                }

                break;

            case 'cmsblocks':

                $blocks = Mage::getModel('cms/block')->getCollection()
                    ->addStoreFilter(1, false)
                    ->addFieldToFilter('is_active',1)
                    ->setOrder('identifier', 'DESC');

                foreach($blocks as $block) {
echo '-------------------- ' . $block->getIdentifier() . ' - (Do not translate this line)

';


echo '-------------------- Subject line:

' . $block->getTitle() . '

';

echo '-------------------- Block content:

' . $block->getContent() . '








';
                }

                break;
        }
    }
}