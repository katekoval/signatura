<?php


class Sinful_Translation_Helper_Data extends Mage_Core_Helper_Abstract
{


    public function getRedirect($targetId, $storeId, $translatorType)
    {

        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        if ($targetId != null || $targetId != null) {
            $url = Mage::helper("adminhtml")->getUrl("sinfultranslation/adminhtml_translator/translate", array('load_entity_id' => $targetId, 'storeid' => $storeId, 'type' => $translatorType, '_secure' => true));
            $start = strpos($url, 'sinfultranslation');
            $path = substr($url, $start);

            return $path;
        } else {
            $url = Mage::helper("adminhtml")->getUrl("sinfultranslation/adminhtml_translator/overview", array('storeid' => $storeId, 'type' => $translatorType, '_secure' => true));
            $start = strpos($url, 'sinfultranslation');
            $path = substr($url, $start);

            return $path;
        }
    }

    public function getRedirectIdentifier($identifier, $storeId, $translatorType)
    {

        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

        if ($identifier != null) {
            $url = Mage::helper("adminhtml")->getUrl("sinfultranslation/adminhtml_translator/translate", array('load_identifier' => $identifier, 'storeid' => $storeId, 'type' => $translatorType, '_secure' => true));
            $start = strpos($url, 'sinfultranslation');
            $path = substr($url, $start);

            return $path;

        } else {
            $url = Mage::helper("adminhtml")->getUrl("sinfultranslation/adminhtml_translator/overview", array('storeid' => $storeId, 'type' => $translatorType, '_secure' => true));
            $start = strpos($url, 'sinfultranslation');
            $path = substr($url, $start);

            return $path;
        }
    }


    public function getDiff($old, $new)
    {
        return '';
    }

    public function allowTags($str){
        $allowed = array("ins", "del");
            foreach( $allowed as $a ){
                $str = str_replace("&lt;".$a."&gt;", "<".$a.">", $str);
                $str = str_replace("&lt;/".$a."&gt;", "</".$a.">", $str);
            }

        return htmlspecialchars_decode($str);
    }


    public function flagReview($id)
    {

        $review = Mage::getModel('review/review')->load($id);
        $reviewId = $review->getReviewId();
        $storeId = $review->getStoreId();

        try {
            $exists = Mage::getModel('translation/review')->load($id, 'review_id');
            if ($exists) {
                Mage::getSingleton('core/session')->addSuccess('Review was already flagged for translation');
            } else {
                Mage::getModel('translation/review')
                    ->setReviewId($reviewId)
                    ->setSourceStore($storeId)
                    ->save();
                Mage::getSingleton('core/session')->addSuccess('Review was flagged for translation');
            }
        } catch (Error $ex) {
            Mage::getSingleton('core/session')->addError('Error: ' . $ex->getMessage());
        }

    }


    public function flagTranslation($id, $type)
    {
       if ($type === 'cmsblock') {
            $entity = Mage::getModel('cms/block')->load($id);
            $entityId = $entity->getBlockId();
            $storeId = $entity->getStoreId()[0];
            $title = $entity->getTitle();
        } elseif ($type === 'cmspage') {
            $entity = Mage::getModel('cms/page')->load($id);
            $entityId = $entity->getPageId();
            $storeId = $entity->getStoreId()[0];
            $title = $entity->getTitle();
        } elseif ($type === 'review') {
            $entity = Mage::getModel('review/review')->load($id);
            $entityId = $entity->getReviewId();
            $storeId = $entity->getStoreId()[0];
            $title = $entity->getTitle();
            $soruceId = $id;
            if ($storeId == 0) {
                $storeId = 2;
            }
        }

        try {

            $missing = Mage::getModel('translation/missing')
                ->getCollection()
                ->addFieldToFilter('source_id', $id)
                ->addFieldToFilter('type', $type)
                ->addFieldToFilter('source_store', array('in' => array($storeId)));

            if ($missing->getData()) {
                Mage::getSingleton('core/session')->addError('Entity was already flagged for translation');
            } else {

                Mage::getModel('translation/missing')
                    ->setSourceId($entityId)
                    ->setSourceStore($storeId)
                    ->setTitle($title)
                    ->setType($type)
                    ->setParentId($soruceId)
                    ->save();

                Mage::getSingleton('core/session')->addSuccess('Entity was flagged for translation');
            }
        } catch (Error $ex) {
            Mage::getSingleton('core/session')->addError('Error: ' . $ex->getMessage());
        }

    }


    public function flagForRetranslation($sourceId, $sourceStore, $field, $title, $change, $type, $oldValue, $newValue, $sku = null)
    {
        # If in store
        $allStores = Mage::app()->getStores();
        $exceptionStores = array(0, 2, 7);

        foreach ($allStores as $_eachStoreId) {

            $storeCode = Mage::app()->getStore($_eachStoreId)->getId();
            if (in_array($storeCode, $exceptionStores)) {
                continue;
            } else {
                try{
                    Mage::getModel('translation/retranslate')
                        ->setSourceId($sourceId)
                        ->setSourceStore($sourceStore)
                        ->setField($field)
                        ->setTargetStore($storeCode)
                        ->setTitle($title)
                        ->setOldValue($oldValue)
                        ->setNewValue($newValue)
                        ->setChange($change)
                        ->setType($type)
                        ->setIsDone(0)
                        ->setHumanReadable($sku)
                        ->save();
                        Mage::getSingleton('core/session')->addSuccess('Entity was flagged for retranslation');
                    } catch (Error $ex) {
                        Mage::getSingleton('core/session')->addError('Error: ' . $ex->getMessage());
                    }
                }
        }


    }

    public function flagForRetranslationProduct($sourceId, $sourceStore, $field, $title, $change, $type, $oldValue, $newValue, $sku = null, $storeCode)
    {

        try{
            Mage::getModel('translation/retranslate')
                ->setSourceId($sourceId)
                ->setSourceStore($sourceStore)
                ->setField($field)
                ->setTargetStore($storeCode)
                ->setTitle($title)
                ->setOldValue($oldValue)
                ->setNewValue($newValue)
                ->setChange($change)
                ->setType($type)
                ->setIsDone(0)
                ->setHumanReadable($sku)
                ->save();
            Mage::getSingleton('core/session')->addSuccess('Entity was flagged for retranslation');
        } catch (Error $ex) {
            Mage::getSingleton('core/session')->addError('Error: ' . $ex->getMessage());
        }

    }


    public function saveKpi($sourceId, $targetStore, $type, $retranslation, $humanReadable)
    {

        $storeId = Mage::app()->getWebsite()->getDefaultGroup()->getDefaultStoreId();
        $user = Mage::getSingleton('admin/session')->getUser()->getData('username');

        try {

            Mage::getModel('translation/kpi')
                ->setSourceId($sourceId)
                ->setSourceStore($storeId)
                ->setTargetStore($targetStore)
                ->setUser($user)
                ->setType($type)
                ->setIsRetranslation($retranslation)
                ->setHumanReadable($humanReadable)
                ->save();

        } catch (Error $ex) {
            Mage::getSingleton('core/session')->addError('KPI Data Error: ' . $ex->getMessage());
        }

    }

    public function saveChangelog($sourceId, $storeId, $targetStore, $user, $type, $field, $oldValue, $newValue, $humanReadable)
    {

        try {

            Mage::getModel('translation/changelog')
                ->setSourceId($sourceId)
                ->setSourceStore($storeId)
                ->setTargetStore($targetStore)
                ->setUser($user)
                ->setType($type)
                ->setField($field)
                ->setOldValue($oldValue)
                ->setNewValue($newValue)
                ->setHumanReadable($humanReadable)
                ->save();

        } catch (Error $ex) {
            Mage::getSingleton('core/session')->addError('KPI Data Error: ' . $ex->getMessage());
        }

    }


    public function getRetranslationEntities($type, $storeId)
    {

        $retranslate = Mage::getModel('translation/retranslate')
            ->getCollection()
            ->addFieldToFilter('type', $type)
            ->addFieldToFilter('source_store', array('nin' => array($storeId)))
            ->addFieldToFilter('target_store', $storeId)
            ->addFieldToFilter('is_done', 0);

        $retranslate->getSelect()->group('source_id');

        $result = $retranslate->getData();

        return $result;

    }


    public function getRetranslationData($entityId, $type, $storeId)
    {

        $retranslate = Mage::getModel('translation/retranslate')
            ->getCollection()
            ->addFieldToFilter('type', $type)
            ->addFieldToFilter('source_id', $entityId)
            ->addFieldToFilter('source_store', array('nin' => array($storeId)))
            ->addFieldToFilter('target_store', $storeId)
            ->addFieldToFilter('is_done', 0)
            ->setOrder('entity_id', 'ASC');

        $retranslate->getSelect()->group('field');

        return $retranslate->getData();

    }


    public function removeFromRetranslation($entityId, $type, $storeId, $user)
    {

        $retranslate = Mage::getModel('translation/retranslate')
            ->getCollection()
            ->addFieldToFilter('type', $type)
            ->addFieldToFilter('source_id', $entityId)
            ->addFieldToFilter('is_done', array('in' => array(0, NULL)))
            ->addFieldToFilter('source_store', array('nin' => array($storeId)))
            ->addFieldToFilter('target_store', $storeId)
            ->setOrder('entity_id', 'ASC');

        $retranslate->getSelect()->group('field');

        foreach ($retranslate as $retrans) {
            $retrans->setUser($user);
            $retrans->setIsDone(1);
            $retrans->save();
        }
    }


    public function strposa($haystack, $needle, $offset = 0)
    {
        if (!is_array($needle)) $needle = array($needle);
        foreach ($needle as $query) {
            if (strpos($haystack, $query, $offset) !== false) return true; // stop on first true result
        }
        return false;
    }


    public function formattingPBr($field)
    {

        $pBrFields = ['description', 'bottom_column1_text',
            'bottom_column2_text', 'bottom_column3_text', 'content', 'post_content', 'post_shortdescription',
            'top_content','detail'];

        if (in_array($field, $pBrFields)) {
            return true;
        } else {
            return false;
        }

    }


    public function formattingBrp($field)
    {

        $BrpFields = ['product_details'];

        if (in_array($field, $BrpFields)) {
            return true;
        } else {
            return false;
        }
    }


    public function getFieldDescription($field, $entityType)
    {

        if ($entityType == 'category') {
            if ($field == 'name') {
                return 'NAME IN NAVIGATION';
            } elseif ($field == 'description') {
                return 'H1 & TEASER TEXT';
            } elseif ($field == 'url_key') {
                return 'URL KEY';
            } elseif ($field == 'meta_title') {
                return 'META TITLE';
            } elseif ($field == 'meta_description') {
                return 'META DESCRIPTION';
            } elseif ($field == 'category_bottom_title') {
                return 'FOOTER TEXT TITLE (H2)';
            } elseif ($field == 'bottom_column1_title') {
                return 'FOOTER COLUMN 1 TITLE (H3)';
            } elseif ($field == 'bottom_column1_text') {
                return 'FOOTER COLUMN 1 TEXT';
            } elseif ($field == 'bottom_column2_title') {
                return 'FOOTER COLUMN 2 TITLE (H3)';
            } elseif ($field == 'bottom_column2_text') {
                return 'FOOTER COLUMN 2 TEXT';
            } elseif ($field == 'bottom_column3_title') {
                return 'FOOTER COLUMN 3 TITLE (H3)';
            } elseif ($field == 'bottom_column3_text') {
                return 'FOOTER COLUMN 3 TEXT';
            }
        }

        if ($entityType == 'product') {
            if ($field == 'name') {
                return 'PRODUCT NAME';
            } elseif ($field == 'description') {
                return 'PRODUCT DESCRIPTION';
            } elseif ($field == 'short_description') {
                return 'PRODUCT SHORT DESCRIPTION';
            } elseif ($field == 'meta_title') {
                return 'META TITLE';
            } elseif ($field == 'meta_description') {
                return 'META DESCRIPTION';
            }
        }


        if ($entityType == 'cms') {
            if ($field == 'title') {
                return 'META TITLE';
            } elseif ($field == 'meta_description') {
                return 'META DESCRIPTION';
            } elseif ($field == 'url_key') {
                return 'URL KEY';
            } elseif ($field == 'identifier') {
                return 'PAGE IDENTIFIER';
            } elseif ($field == 'content') {
                return 'PAGE CONTENT';
            }
        }

        if ($entityType == 'email') {
            if ($field == 'template_subject') {
                return 'EMAIL SUBJECT LINE';
            } elseif ($field == 'template_text') {
                return 'EMAIL TEXT';
            } elseif ($field == 'is_active') {
                return 'Once translation is finished, set to "Active"';
            } elseif ($field == 'template_code') {
                return 'EMAIL NAME';
            }
        }
    }

    public function getTranslatorLabel($t)
    {
        if ($t == 'product') {
            return 'Products';
        }elseif ($t == 'category') {
            return 'Categories';
        }elseif ($t == 'cms') {
            return 'CMS Pages';
        }elseif ($t == 'cmsblock') {
            return 'CMS Blocks';
        }elseif ($t == 'attribute') {
            return 'Attributes';
        }elseif ($t == 'review') {
            return 'Product Reviews';
        }elseif ($t == 'email') {
            return 'Emails';
        }
    }

    public function getStoreLabel($t)
    {
        $storeName = Mage::getModel('core/store')->load($t)->getName();

        return $storeName;
    }


    public function stripHtml($input){

        return strip_tags($input);

    }

    public function htmlentitytag($input){
        $input = html_entity_decode($input);
        $input = strip_tags($input);

        return $input;
    }


    public function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    public function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    public function getEntityByAttribute($attributeId, $storeId, $value){

        $read   = Mage::getSingleton('core/resource')->getConnection('core_read');
        $query = 'SELECT * FROM catalog_product_entity_int where store_id = :storeId and attribute_id = :attributeId and value = :avalue;';

        $binds = array(
            'storeId' => $storeId,
            'attributeId' => $attributeId,
            'avalue' => $value,
        );

        $results  = $read->query( $query, $binds );
        $data     = $results->fetchAll();

        return $data;

    }

    /**
     * @return string[]
     */
    public function getStatuses()
    {
        $statuses = array(
            'incomplete'            => 'incomplete',
            'waiting_for_approval'  => 'waiting for approval',
            'complete'              => 'complete'
        );

        return $statuses;
    }

}