<?php
/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */

class Varien_Data_Form_Element_Wcmultiselect extends Varien_Data_Form_Element_Abstract
{
     public function __construct($attributes=array())
    {
        parent::__construct($attributes);
        $this->setType('select');
        $this->setExtType('multiple');
    }

    public function getName()
    {
        $name = parent::getName();
        if (strpos($name, '[]') === false) {
            $name.= '[]';
        }
        return $name;
    }

    public function getElementHtml()
    {
        $this->addClass('select wcmultiselect');
        $html = '';
        if ($this->getCanBeEmpty() && empty($this->_data['disabled'])) {
            $html .= '<input type="hidden" name="' . parent::getName() . '" value="" />';
        }
        
        $html .= '<a href="javascript:void(0);" onclick="javascript:newelem=$(\''.$this->getHtmlId().'\').clone(true);newelem.removeAttribute(\'id\');newelem.removeAttribute(\'style\');newelem.removeAttribute(\'disabled\');newelem.setValue(\'\');$(this).next().insert({after:newelem});">';
        $html .= '<img src="' . Mage::getDesign()->getSkinUrl('images/icon_btn_add.gif') . '" alt="Add Row"/>';
        $html .= '</a>' . "\n";
        $html .= '<br/>' . "\n";
        
        
        
        $value = $this->getValue();
        if (!is_array($value)) {
            $value = explode(',', $value);
        }
        $value = array_unique($value);
        $value = array_filter($value);
        foreach($value as $val) {
            $html .= '<select name="' . $this->getName() . '" ' .
            $this->serialize($this->getHtmlAttributes()) . '>' . "\n";


            if ($values = $this->getValues()) {
                foreach ($values as $option) {
                    $html .= $this->_optionToHtml($option, $val);
                }
            }

            $html .= '</select>' . "\n";
            $html .= '<a href="javascript:void(0);" onclick="javascript:$(this).previous().remove();$(this).remove()">';
            $html .= '<img src="' . Mage::getDesign()->getSkinUrl('images/icon_btn_delete.gif') . '" alt="Remove Row"/>';
            $html .= '</a>' . "\n";

        }

        $html .= '<select id="' . $this->getHtmlId() . '" name="' . $this->getName() . '" style="display:none" disabled="disabled" ' .
        $this->serialize($this->getHtmlAttributes()) . '>' . "\n";
        if ($values = $this->getValues()) {
            foreach ($values as $option) {
                $html .= $this->_optionToHtml($option, false);
            }
        }
        $html .= '</select>' . "\n";
        
        $html .= $this->getAfterElementHtml();

        return $html;
    }

    public function getHtmlAttributes()
    {
        return array('title', 'class', 'style', 'onclick', 'onchange', 'disabled', 'tabindex');
    }

    public function getDefaultHtml()
    {
        $result = ( $this->getNoSpan() === true ) ? '' : '<span class="field-row">'."\n";
        $result.= $this->getLabelHtml();
        $result.= $this->getElementHtml();

        $result.= ( $this->getNoSpan() === true ) ? '' : '</span>'."\n";

        return $result;
    }

    public function getJsObjectName() {
         return $this->getHtmlId() . 'ElementControl';
    }

    protected function _optionToHtml($option, $selected)
    {
        $html = '<option value="'.$this->_escape($option['value']).'"';
        $html.= isset($option['title']) ? 'title="'.$this->_escape($option['title']).'"' : '';
        $html.= isset($option['style']) ? 'style="'.$option['style'].'"' : '';
        if ((string)$option['value'] == $selected) {
            $html.= ' selected="selected"';
        }
        $html.= '>'.$this->_escape($option['label']). '</option>'."\n";
        return $html;
    }
}
