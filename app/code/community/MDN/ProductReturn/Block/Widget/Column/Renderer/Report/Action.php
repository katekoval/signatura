<?php

class MDN_ProductReturn_Block_Widget_Column_Renderer_Report_Action extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    public function render(Varien_Object $row)
    {

        //get data
        if ($this->getColumn()->getuse_product_id())
            $productId = $row->getrp_product_id();
        else
            $productId = null;
        $from   = $row->getrrp_from();
        $to     = $row->getrrp_to();
        $action = $this->getColumn()->getaction();

        //return value
        return Mage::helper('ProductReturn/Report')->getActionCount($productId, $from, $to, $action);
    }

}
