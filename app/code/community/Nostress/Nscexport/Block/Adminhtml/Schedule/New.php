<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

class Nostress_Nscexport_Block_Adminhtml_Schedule_New extends Mage_Adminhtml_Block_Widget_Form_Container
{
	/**
	 *
	 */
	public function __construct()
	{
		#$this->_objectId 	= 'schedule_id';
		$this->_blockGroup 	= 'nscexport';
		$this->_controller 	= 'nscexport_schedule_grid';

		parent::__construct();

		$this->setTemplate('nscexport/cron/schedule/new.phtml');
	}

	/**
	 *
	 */
	protected function _prepareLayout()
	{
		$this->setChild('new_form',
			$this->getLayout()->createBlock('nscexport/adminhtml_schedule_new_form')
		);

		return parent::_prepareLayout();
	}

	/**
	 *
	 */
	public function getHeaderText()
	{
		$schedule = Mage::registry('nscexport_cron_schedule');
		if( $schedule && $schedule->getId() ) {
			return Mage::helper('nscexport')->__("New Item");
		} else {
			return Mage::helper('nscexport')->__('Add Item');
		}

	}

	/**
	 *
	 */
	public function getFormHtml()
	{
		return $this->getChildHtml('new_form');
	}

	/**
	 *
	 */
	public function getBackButtonHtml()
	{
		return $this->getChildHtml('back_button');
	}

	/**
	 *
	 */
	public function getSaveButtonHtml()
	{
		return $this->getChildHtml('save_button');
	}
}