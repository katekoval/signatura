<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

class Nostress_Nscexport_Block_Adminhtml_Schedule_New_Form extends Mage_Adminhtml_Block_Widget_Form
{

	protected function _prepareForm()
	{
        $form = new Varien_Data_Form(array('id' => 'new_form', 'action' => $this->getData('action'), 'method' => 'post'));
		$fieldset = $form->addFieldset('general', array('legend' => Mage::helper('nscexport')->__('Schedule Data')));

		// Job-Code
		$codeSelect = Mage::getSingleton('nscexport/schedule_code')->toFormElementSelect();

		$fieldset->addField('job_code', 'note', array(
			'label' => Mage::helper('nscexport')->__('Job Code'),
			'title' => Mage::helper('nscexport')->__('Job Code'),
			'class' => 'required-entry',
			'text' => $codeSelect->toHtml(),
		));

		$fieldset->addField('status', 'note', array(
			'label' => Mage::helper('nscexport')->__('Status'),
			'title' => Mage::helper('nscexport')->__('Status'),
			'class' => 'required-entry',
			'text' => Mage::helper('nscexport')->__(Mage_Cron_Model_Schedule::STATUS_PENDING),
		));

		$fieldset->addField('scheduled_at', 'date', array(
			'label' => Mage::helper('nscexport')->__('Scheduled At'),
			'title' => Mage::helper('nscexport')->__('Scheduled At'),
			'html_id' => 'scheduled_at',
			'name' => 'scheduled_at',
			'class' => 'required-entry',
			'format' => Varien_Date::DATETIME_INTERNAL_FORMAT, // hardcode because hardcoded values delimiter
            'time' => true,
			'image' => $this->getSkinUrl('images/grid-cal.gif'),
		));

		#$form->setValues($schedule->getData());
		$form->setUseContainer(true);
		$this->setForm($form);
        return parent::_prepareForm();
	}

}