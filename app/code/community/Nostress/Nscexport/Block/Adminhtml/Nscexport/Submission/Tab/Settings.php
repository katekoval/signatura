<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
* @category Nostress
* @package Nostress_Nscexport
*/

class Nostress_Nscexport_Block_Adminhtml_Nscexport_Submission_Tab_Settings extends Nostress_Nscexport_Block_Adminhtml_Nscexport_Tab
{
	protected function _construct()
	{
	    parent::_construct();
	    $this->setShowGlobalIcon(true);
	    $this->setTemplate('nscexport/submission/form.phtml');
	}
	
	protected function _prepareForm() {
	
	    // needed for correct function
	    $this->_feed = $this->getProfile()->getFeedObject();
	
	    $exportApiAdapter = $this->getProfile()->getExportApiAdapter();
	    $apiLabel = $exportApiAdapter->getLabel();
	    $apiCode = $exportApiAdapter->getCode();
	
	    $form = new Varien_Data_Form();
	    	
	    $form->setHtmlIdPrefix(Nostress_Nscexport_Model_Profile::EXPORT_API."_");
	    $this->setForm($form);
	
	    $fieldset = $form->addFieldset('base_fieldset', array('legend' => $exportApiAdapter->getShortLabel()." ".Mage::helper('nscexport')->__(' Settings')));
	
	    $fieldset->setHeaderBar($this->getHelpButtonHtmlByFieldset( $exportApiAdapter->getHelpId()));
	    $yesnoSource = Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray();
	
	    $fieldset->addType('nostress_button','Nostress_Nscexport_Block_Adminhtml_Nscexport_Helper_Form_Button');
	
	    $fieldset->addField('api_type', 'hidden', array(
	            'name' => "api_type",
	            'value' => $apiCode
	    ));
	     
	
	    $apiFormItemsMethod = $apiCode."Items";
	    
	    $exportApiConfig = $this->getExportApiConfig();
	
	    $exportApiAdapter->initFormElements( $fieldset, $exportApiConfig);
	
	    $requiredFields = $exportApiAdapter->getRequiredFields();
	    foreach( $fieldset->getElements() as $element) {
	        if( in_array( $element->getName(), $requiredFields)) {
	            $element->setData( 'required', true);
	        }
	    }
	    
	    $fieldset->addField('enabled', 'select', array(
	            'label' => Mage::helper('nscexport')->__("Auto-Submit:"),
	            'name' => "enabled",
	            'values' => $yesnoSource,
	            'note' => Mage::helper('nscexport')->__("Submit via %s automatically whenever you regenerate channel feed.", $apiLabel),
	            'class' => 'step-2'
	    ));
	    
	    if( $this->getProfile()->isApi()) {
	        $field = $fieldset->addField( 'cron_interval', 'select', array(
	                'label' => Mage::helper('nscexport')->__("Frequency").":",
	                'name' => "cron_interval",
	                'values' => Mage::getSingleton('nscexport/config_source_croninterval')->toOptionArray(),
	                'value' => Mage::getSingleton('nscexport/config_source_croninterval')->getDefaultValue(),
	                'class' => 'step-2',
	                'note' => Mage::helper('nscexport')->__( 'Frequency of feed submission, if Auto-Submit is enabled.')
	        ));
	    }
	     
	    $sauUrl = $this->getUrl('*/*/save', array( 'id'=>$this->getProfile()->getId(), 'back'=>'submit'));
	    $sauLabel = $this->getProfile()->isApi() ? Mage::helper('adminhtml')->__('Save & Submit') : Mage::helper('adminhtml')->__('Save & Upload');
	    $fieldset->addField('save_and_submit', 'nostress_button', array(
	            'label' => $sauLabel,
	            'name' => 'save_and_submit',
	            'class' => 'save step-2',
	            'onclick' => 'editForm.submit( \''.$sauUrl.'\');'
	    ));
	    
	    $submissionParams = $this->getFeed()->getSubmissionParams();
	    $reportPath = isset( $submissionParams['processing_report']['dir_path']) ? $submissionParams['processing_report']['dir_path'] : false;
	    $reportDescription = isset( $submissionParams['processing_report']['description']) ? $submissionParams['processing_report']['description'] : false;
	    $isFilled = $exportApiAdapter->isFilled( $exportApiConfig);
	
	    if( $reportDescription) {
	        $fieldset->addField('processing_description', 'note', array(
	                'text' => "<br />".$reportDescription,
	                'name' => "processing_description"
	        ));
	    }
	    if( $reportPath) {
	        $dataPr = array(
	                'label' => Mage::helper('nscexport')->__("See Procesing Reports"),
	                'title' => Mage::helper('nscexport')->__("See Procesing Reports"),
	                'name' => 'test_connection',
	                'class' => 'see-reports-button back step-2',
	                'onclick' => "javascript:loadPath( 'dir', '', '$reportPath')"
	        );
	        if( !$isFilled) {
	            $dataPr['disabled'] = 'disabled';
	            $dataPr['class'] .= ' disabled';
	        }
	        $fieldset->addField('processing_button', 'nostress_button', $dataPr);
	    }
	
	    $form->setFieldNameSuffix(Nostress_Nscexport_Model_Profile::EXPORT_API);
	    $this->setForm($form);
	
	    $formList= new Varien_Data_Form(array(
	            'id' => 'list_form',
	            'method' => 'get',
	    ));
	    $formList->setParent($this);
	    $formList->setBaseUrl(Mage::getBaseUrl());
	    $this->setListForm( $formList);
	
	    $fieldsetList = $formList->addFieldset('list_fieldset', array('legend' => $exportApiAdapter->getSubmissionListLabel()));
	
	    $fieldsetList->addType('submission_list','Nostress_Nscexport_Block_Adminhtml_Nscexport_Helper_Form_Submission_List');
		    	
	    $fieldsetList->addField('submission_list', 'submission_list', array(
	            'values' => $this->getExportApiConfig(),
	            'profile' => $this->getProfile(),
	            'api_code' => $apiCode
	    ), 'frontend_class');
	
	    
	    if(!isset($exportApiConfig['enabled']))
	        $exportApiConfig['enabled'] = 1;
	    $form->addValues($exportApiConfig);
	    
        // cron_interval from general config
	    $form->addValues( $this->getGeneralConfig());
	     
	    return parent::_prepareForm();
	}
}
