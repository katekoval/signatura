<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
 * Form select element
 *
 * @category   Varien
 * @package    Varien_Data
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Nostress_Nscexport_Block_Adminhtml_Nscexport_Helper_Form_Attributeselect extends Varien_Data_Form_Element_Select
{
	const WARNING_IMAGE_PATH = "adminhtml/default/default/images/error_msg_icon.gif";

    protected function _optionToHtml($option, $selected)
    {
        if (is_array($option['value'])) {
            $html ='<optgroup label="'.$option['label'].'">'."\n";
            foreach ($option['value'] as $groupItem) {
                $html .= $this->_optionToHtml($groupItem, $selected);
            }
            $html .='</optgroup>'."\n";
        }
        else {
        	if (isset($option['disabled'])) {
        		$style = 'style=""';
        	} else {
        		$style = 'style="color: black;"';
        	}
            $html = '<option value="'.$this->_escape($option['value']).'"';
            $html.= isset($option['title']) ? 'title="'.$this->_escape($option['title']).'"' : '';
            $html.= isset($option['style']) ? 'style="'.$option['style'].'"' : '';
            $html.= (isset($option['red']) && $option['red'] == 1) ? 'style="color: red;" class="warning"' : $style;
            $html.= isset($option['disabled']) ? 'disabled="disabled"' : '';
            if (in_array($option['value'], $selected)) {
                $html.= ' selected="selected"';
            }
            $html.= '>'.$this->_escape($option['label']). '</option>'."\n";
        }
        return $html;
    }
    
    public function getElementHtml() {
        $this->addClass('select');
		$value = $this->getValue();
        if (!is_array($value)) {
            $value = array($value);
        }
        $display = 'display:none;';
        if ($values = $this->getValues()) {
            foreach ($values as $option) {
                if (is_array($option) && !is_array($option['value'])) {
                    if (isset($option['red']) && $option['red'] == 1 && isset($option['value']) && in_array($option['value'], $value)) {
                    	$display = 'display:block;';
                    }
                }
            }
        }        
		
        $html = '<select id="'.$this->getHtmlId().'" name="'.$this->getName().'" '.$this->serialize($this->getHtmlAttributes()).'>'."\n";
        

        if ($values) {
            foreach ($values as $key => $option) {
                if (!is_array($option)) {
                    $html.= $this->_optionToHtml(array(
                        'value' => $key,
                        'label' => $option),
                        $value
                    );
                }
                elseif (is_array($option['value'])) {
                    $html.='<optgroup label="'.$option['label'].'">'."\n";
                    foreach ($option['value'] as $groupItem) {
                        $html.= $this->_optionToHtml($groupItem, $value);
                    }
                    $html.='</optgroup>'."\n";
                }
                else {
                    $html.= $this->_optionToHtml($option, $value);
                }
            }
        }

        $html.= '</select>'."\n";
        $html.= $this->getAfterElementHtml();
        
    	$html = str_replace('TO_BE_REPLACED_WITH_ID', $this->getHtmlId(), $html);
    	
    	$html = '<div id="'.$this->getHtmlId().'_warning" style=" '.$display.'float:left;"><img src="'.Mage::getBaseUrl('skin').self::WARNING_IMAGE_PATH.'"></div>'.$html;
    	return $html;
    }

}