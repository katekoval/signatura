<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
 * Adminhtml form container block
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */

class Nostress_Nscexport_Block_Adminhtml_Nscexport_Preview extends Mage_Adminhtml_Block_Widget_Form_Container
{
    protected $_objectId = 'id';
    protected $_mode = 'preview';
    protected $_blockGroup = 'nscexport';
    protected $_controller = 'adminhtml_nscexport';
    
    /**
     * @var Nostress_Nscexport_Model_Profile
     */
    protected $_profile = null;

    public function __construct()
    {
        parent::__construct();
        
        $this->_removeButton( 'save');
        $this->_removeButton( 'reset');

        $profile = $this->getProfile();
        $this->_headerText = $this->helper( 'nscexport')->__('Koongo Connector')." - ".
                $this->helper( 'nscexport')->__('Feed Preview')." '".$profile->getName()."' (ID: ".$profile->getId().")";

        $this->_addButton('back', array(
            'label'     => Mage::helper('nscexport')->__('Back to profile grid'),
            'onclick'   => 'setLocation(\'' . $this->getBackUrl() . '\')',
            'class'     => 'back',
        ), -1);
        
        $this->_addButton('edit', array(
                'label'     => Mage::helper('nscexport')->__('Edit Profile'),
                'onclick'   => 'setLocation(\'' . $this->getUrl('*/*/edit', array( 'id'=>$profile->getId())) . '\')',
                'class' => 'back'
        ), 5);
        
        $downloadOptions = array(
            'label'     => Mage::helper('nscexport')->__('Download File'),
            'onclick'   => 'setLocation(\'' . $this->getUrl('*/*/download', array( 'export_id'=>$profile->getId())) . '\')',
        );
        if( !$profile->isFileExists()) {
            $downloadOptions['disabled'] = 'disabled';
        }
        $this->_addButton('download', $downloadOptions, 10);
        
        if( $profile->isSubmitable()) {
            
            $apiLabel = $profile->getExportApiAdapter()->getLabel();
            
            if( $profile->isApi()) {
                $uploadButtonLabel = Mage::helper('nscexport')->__('Submit via %s', $apiLabel);
            } else {
                $uploadButtonLabel = Mage::helper('nscexport')->__('Upload File via %s', $apiLabel);
            }
          
            $this->_addButton('upload', array(
                    'label'     => $uploadButtonLabel,
                    'onclick'   => 'setLocation(\'' . $this->getUrl('*/*/submit', array( 'id'=>$profile->getId())) . '\')',
            ), 40);
        }
        
        $this->_addButton('livechat', $this->helper('nscexport')->getLivechatButtonOptions(), -200);
    }
    
   /**
	* Retrieve current profile instance
	*
	* @return Nostress_Nscexport_Profile
	*/
	public function getProfile() {
	    if( $this->_profile === null) {
            $this->_profile = Mage::registry('nscexport_profile');
	    }
	    return $this->_profile;
	}
		  
    /**
     * Get URL for back (reset) button
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('*/*/');
    }

    public function getHeaderWidth()
    {
        return '';
    }

    public function getHeaderCssClass()
    {
        return 'icon-head head-' . strtr($this->_controller, '_', '-');
    }
    
}
