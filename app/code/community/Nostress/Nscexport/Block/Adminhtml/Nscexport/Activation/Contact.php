<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* @category Nostress 
* @package Nostress_Nscexport
*/

class Nostress_Nscexport_Block_Adminhtml_Nscexport_Activation_Contact extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm() 
	{
		$client = Mage::helper('nscexport/data_client');
		$connectorInfo = $client->getConnectorInfoByCode($this->getCode());		
		if(!$connectorInfo)
		{
			$connectorInfo = $client->getConnectorInfoByCode();
		}
		$connectorInfo = new Varien_Object($connectorInfo);
		
		$form = new Varien_Data_Form(array(
			'id' => 'contact_form',
			'action' => $this->getUrl("*/*/post"),
			'method' => 'post',
			'enctype' => 'multipart/form-data',
		));
		
		$form->setUseContainer(true);
		$this->setForm($form);
		
		$fieldset = $form->addFieldset('contact_form', array(
			'legend' => Mage::helper('nscexport')->__('Contact Form')
		));		
		
		$fieldset->addField('code', 'hidden', array(
				'name' => 'code',
				'value' => $this->getCode(),
		));
		
		$fieldset->addField('subject_field', 'text', array(
			'label' => $this->__('Subject:'),
			'disabled' => 'disabled',
			'name' => 'subject_field',
			'value' => $this->__('Koongo Trial activation request')			
		));
		
		$fieldset->addField('subject', 'hidden', array(
				'label' => $this->__('Subject:'),				
				'name' => 'subject',
				'value' => $this->__('Koongo Trial activation request')
		));
		
		$fieldset->addField('from_email', 'text', array(
				'label' => $this->__('Email:'),
				'class' => 'required-entry validate-email',
				'required' => true,
				'name' => 'from_email'		
		));
		
		$serverId = Mage::helper('nscexport/version')->getServerId();
		$bodyText = $this->__('Hi,'.PHP_EOL.
								'I need help with Koongo Trial activation.'.PHP_EOL.								
								'My backend URL is: %s'.PHP_EOL.
								'Server Id: %s'.PHP_EOL.
								'Backend username:'.PHP_EOL.
								'Password:'.PHP_EOL.PHP_EOL.											
								
								'Thanks, '.PHP_EOL,Mage::helper('adminhtml')->getUrl('adminhtml'),$serverId);
		
		$fieldset->addField('body', 'textarea', array(
				'label' => $this->__('Question:'),
				'name' => 'body',
				'class' => 'required-entry',
				'required' => true,
				'value' => $bodyText
		));
		
		$class = 'btn-koongo-submit-yellow';
		if($connectorInfo->getIsTrial() == true)
			$class = 'btn-koongo-submit-orange';
		
		$fieldset->addField('send', 'button', array(	
			'value' => $this->__('Send Email'),
			'onclick'   => 'contactForm.submit();',
			'class' => $class,
		));
				
		return parent::_prepareForm();
	}
}