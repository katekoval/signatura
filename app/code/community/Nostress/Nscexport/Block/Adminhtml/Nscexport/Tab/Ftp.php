<?php
/**
* Magento Module developed by NoStress Commerce
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to info@nostresscommerce.cz so we can send you a copy immediately.
*
* @copyright Copyright (c) 2012 NoStress Commerce (http://www.nostresscommerce.cz)
*
*/

/**
* @category Nostress
* @package Nostress_Nscexport
*/

class Nostress_Nscexport_Block_Adminhtml_Nscexport_Tab_Ftp extends Nostress_Nscexport_Block_Adminhtml_Nscexport_Tab
{
	public function __construct() {
		parent::__construct();
		$this->setShowGlobalIcon(true);
	}
	
	public function _prepareLayout() {
	    parent::_prepareLayout();
	
	    $form = new Varien_Data_Form();
	    $form->setHtmlIdPrefix(Nostress_Nscexport_Model_Profile::UPLOAD."_");
	    $fieldset = $form->addFieldset('base_fieldset', array('legend' => Mage::helper('nscexport')->__('FTP Submission')));
	
	    $fieldset->setHeaderBar($this->getHelpButtonHtmlByFieldset("ftp_info"));
	    
	    $fieldset->addType('nostress_button','Nostress_Nscexport_Block_Adminhtml_Nscexport_Helper_Form_Button');
	    
		$url = $this->getUrl("*/*/submission", array( 'id'=> $this->getProfile()->getId()));
		$data = array(
	        'label' => Mage::helper('nscexport')->__("Navigate to FTP Submission page"),
	        'name' => 'ftp_submission',
		    'onclick' => "updateOutput();$('profile_edit_form').action = $('profile_edit_form').action+'back/submission/'; profileSubmit('neco', false)",
		);
		if( !$this->getProfile()->isFileExists()) {
		    $data['disabled'] = 'disabled';
		    $data['title'] = Mage::helper('nscexport')->__("Please execute profile to enable this feature.");
		    $data['class'] = 'disabled';
		}
		$fieldset->addField('ftp_submission', 'nostress_button', $data);
		
		$fieldset->addField('ftp_note', 'note', array(
	        'text' => Mage::helper('nscexport')->__("Ftp Settings were moved to separated page."),
	        'name' => "ftp_note",
		));
	    
	    $this->setForm($form);
	}
}
