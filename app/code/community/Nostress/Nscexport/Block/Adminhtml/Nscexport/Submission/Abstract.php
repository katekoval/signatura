<?php
/**
* Magento Module developed by NoStress Commerce
*
* NOTICE OF LICENSE
*
* This source file is subject to the Open Software License (OSL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/osl-3.0.php
* If you did of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to info@nostresscommerce.cz so we can send you a copy immediately.
*
* @copyright Copyright (c) 2012 NoStress Commerce (http://www.nostresscommerce.cz)
*
*/

/**
* @category Nostress
* @package Nostress_Nscexport
*/

class Nostress_Nscexport_Block_Adminhtml_Nscexport_Submission_Abstract extends Nostress_Nscexport_Block_Adminhtml_Nscexport_Tab
{
	protected function _prepareForm() {
		
		// needed for correct function
		$this->_feed = $this->getProfile()->getFeedObject();
		
		$exportApiAdapter = $this->getProfile()->getExportApiAdapter();
		$apiLabel = $exportApiAdapter->getLabel();
		$apiCode = $exportApiAdapter->getCode();
		
		$form = new Varien_Data_Form();
			
	    $form->setHtmlIdPrefix(Nostress_Nscexport_Model_Profile::EXPORT_API."_");
	    $this->setForm($form);
	
	    $fieldset = $form->addFieldset('base_fieldset', array('legend' => Mage::helper('nscexport')->__('Submission settings')));
	
	    $fieldset->setHeaderBar($this->getHelpButtonHtmlByFieldset("submission_info"));
	    $yesnoSource = Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray();
	
	    $fieldset->addType('nostress_button','Nostress_Nscexport_Block_Adminhtml_Nscexport_Helper_Form_Button');
	
	    $fieldset->addField('api_type', 'hidden', array(
	            'name' => "api_type",
	            'value' => $apiCode
	    ));
	
	    $fieldset->addField('enabled', 'select', array(
	            'label' => Mage::helper('nscexport')->__("Feed Auto-Submit:"),
	            'name' => "enabled",
	            'values' => $yesnoSource,
	            'note' => Mage::helper('nscexport')->__("Upload via %s automatically whenever you regenerate channel feed.", $apiLabel)
	    ));
	
	    $apiFormItemsMethod = $apiCode."Items";
	
	    $exportApiAdapter->initFormElements( $fieldset);
	
	    $requiredFields = $exportApiAdapter->getRequiredFields();
	    foreach( $fieldset->getElements() as $element) {
	        if( in_array( $element->getName(), $requiredFields)) {
	            $element->setData( 'required', true);
	        }
	    }
	
	    $urlTestExportApiConnection = $this->getUrl("*/*/testExportApiConnection");
	    $fieldset->addField('test_connection', 'nostress_button', array(
	            'label' => Mage::helper('nscexport')->__("Test Connection"),
	            'name' => 'test_connection',
	            'class' => 'save',
	            'onclick' => "testExportApiConnection('$urlTestExportApiConnection');"
	    ));
	
	    $exportApiConfig = $this->getExportApiConfig();
	    $submissionParams = $this->getFeed()->getSubmissionParams();
	    $reportPath = isset( $submissionParams['processing_report']['dir_path']) ? $submissionParams['processing_report']['dir_path'] : false;
	    $reportDescription = isset( $submissionParams['processing_report']['description']) ? $submissionParams['processing_report']['description'] : false;
	    $isFilled = $exportApiAdapter->isFilled( $exportApiConfig);
	
	    if( $reportDescription) {
	        $fieldset->addField('processing_description', 'note', array(
	                'text' => "<br />".$reportDescription,
	                'name' => "processing_description"
	        ));
	    }
	    if( $reportPath) {
	        $dataPr = array(
	                'label' => Mage::helper('nscexport')->__("See Procesing Reports"),
	                'title' => Mage::helper('nscexport')->__("See Procesing Reports"),
	                'name' => 'test_connection',
	                'class' => 'see-reports-button',
	                'onclick' => "javascript:loadPath( 'dir', '', '$reportPath')"
	        );
	        if( !$isFilled) {
	            $dataPr['disabled'] = 'disabled';
	            $dataPr['class'] .= ' disabled';
	        }
	        $fieldset->addField('processing_button', 'nostress_button', $dataPr);
	    }
	
	    $form->addValues( $exportApiConfig);
	
	    $form->setFieldNameSuffix(Nostress_Nscexport_Model_Profile::EXPORT_API);
	    $this->setForm($form);
	
	    $formList= new Varien_Data_Form(array(
	            'id' => 'list_form',
	            'method' => 'get',
	    ));
	    $formList->setParent($this);
	    $formList->setBaseUrl(Mage::getBaseUrl());
	    $this->setListForm( $formList);
	
	    $fieldsetList = $formList->addFieldset('list_fieldset', array('legend' => $exportApiAdapter->getSubmissionListLabel()));
	
	    $fieldsetList->addType('submission_list','Nostress_Nscexport_Block_Adminhtml_Nscexport_Helper_Form_Submission_List');
	
	    $fieldsetList->addField('submission_feed_filename', 'label', array(
	            'value' => Mage::helper('nscexport')->__("Feed filename: %s", $this->getProfile()->getFilename()),
	            'name' => "submission_client_help",
	    ));
	
	    $fieldsetList->addField('submission_list', 'submission_list', array(
	            'values' => $this->getExportApiConfig(),
	            'profile' => $this->getProfile(),
	            'api_code' => $apiCode
	    ), 'frontend_class');
	
	    $form->addValues($this->getExportApiConfig());
	    
	    return parent::_prepareForm();
	}
}
