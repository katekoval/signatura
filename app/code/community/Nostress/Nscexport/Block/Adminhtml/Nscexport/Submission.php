<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
* @category Nostress
* @package Nostress_Nscexport
*/

class Nostress_Nscexport_Block_Adminhtml_Nscexport_Submission extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
	{
		$this->_objectId = 'id';
		$this->_controller = 'adminhtml_nscexport';
		$this->_blockGroup = 'nscexport';
		$this->_mode = 'submission';
		
		parent::__construct();
		
		$this->_removeButton('save');
		$this->_removeButton('delete');
		$this->_removeButton('reset');
		
		$profile = $this->getProfile();
		
		$this->_addButton('edit', array(
		        'label'     => Mage::helper('nscexport')->__('Edit Profile'),
		        'onclick'   => 'setLocation(\'' . $this->getUrl('*/*/edit', array( 'id'=>$profile->getId())) . '\')',
		        'class' => 'back'
		), -1);

		$sauUrl = $this->getUrl('*/*/save', array( 'id'=>$profile->getId(), 'back'=>'submit'));
		$sauLabel = $profile->isApi() ? Mage::helper('adminhtml')->__('Save & Submit') : Mage::helper('adminhtml')->__('Save & Upload');
		$this->_addButton('save_and_upload', array(
		        'label'     => $sauLabel,
		        'onclick'   => 'editForm.submit( \''.$sauUrl.'\');',
		        'class'     => 'save step-2',
		), 5);
		
		$this->_addButton('save', array(
		        'label'     => Mage::helper('adminhtml')->__('Save Settings'),
		        'onclick'   => 'editForm.submit();',
		        'class'     => 'save',
		), 10);
		
		$this->addLiveChatButton();
		
	}
	
	protected function addLiveChatButton() {
	
		$this->_addButton('livechat', $this->helper('nscexport')->getLivechatButtonOptions(), 100);
	}
	
	/**
	 * Retrieve current profile instance
	 *
	 * @return Nostress_Nscexport_Model_Profile
	 */
	public function getProfile() {
	    
	    return Mage::registry('nscexport_profile');
	}
	
	public function getHeaderText()
	{
	    $profile = $this->getProfile();
		return Mage::helper('nscexport')->__('Koongo Connector')." - ".$this->helper( 'nscexport')->__('Submission Form')." '".$profile->getName()." - ".$profile->getFeedObject()->getType()."' (ID: ".$profile->getId().")";
	}
}