<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
* @category Nostress
* @package Nostress_Nscexport
*/

class Nostress_Nscexport_Block_Adminhtml_Nscexport_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
	
	protected $_profile;
	
	/**
	* Default Attribute Tab Block
	*
	* @var string
	*/
	protected $_attributeTabBlock = 'nscexport/adminhtml_nscexport_tabs';
	
	/**
	* Initialize Tabs
	*
	*/
	public function __construct() {
		parent::__construct();
		$this->setId('profile_info_tabs');
		$this->setDestElementId('profile_tab_content');
		$this->setTitle(Mage::helper('catalog')->__('Category Data'));
		$this->setTemplate('widget/tabshoriz.phtml');
	}
	
	/**
	* Retrieve profile object
	*
	* @return Nostress_Nscexport_Model_Profile
	*/
	public function getProfile() {
		if (!$this->_profile) {
			$this->_profile = Mage::registry('nscexport_profile');
		}
		return $this->_profile;
	}
	
	/**
	* Return Adminhtml Catalog Helper
	*
	* @return Mage_Adminhtml_Helper_Catalog
	*/
	public function getProfileHelper() {
		return Mage::helper('adminhtml/catalog');
	}
	
	/**
	* Getting attribute block name for tabs
	*
	* @return string
	*/
	public function getAttributeTabBlock() {
		if ($block = $this->getProfileHelper()->getCategoryAttributeTabBlock()) {
			return $block;
		}
		return $this->_attributeTabBlock;
	}
	
	/**
	* Prepare Layout Content
	*
	* @return Nostress_Nscexport_Block_Adminhtml_Nscexport_Tabs
	*/
	protected function _prepareLayout() {
		$config = "";
		$feed = "";
		if (!$this->getProfile()->getId()) {
			$feed = $this->getProfile()->getFeed();
			
			if ($feed) {
				$config = Mage::getModel('nscexport/profile')->getBackendConfig($feed);
			}
		}
		else {
			$config = $this->getProfile()->getBackendConfig();
		}
		
		$tabActive = $this->getRequest()->getParam( 'tab', 'general');
		
		if(Mage::registry('nscexport_profile_config'))
			Mage::unregister('nscexport_profile_config');
		Mage::register('nscexport_profile_config', $config);
		
		$this->addTab('general', array(
			'label' => Mage::helper('nscexport')->__('General'),
			'content' => $this->getLayout()->createBlock('nscexport/adminhtml_nscexport_tab_general')
				->setData(array(
					"config1" => $config
				))
				->toHtml(),
			'active' => false
		));
		
		$this->addTab('feed', array(
			'label' => Mage::helper('nscexport')->__('Feed details'),
			'content' => $this->getLayout()->createBlock('nscexport/adminhtml_nscexport_tab_feed')->toHtml(),
			'active' => false
		));
		
		$this->addTab('products', array(
			'label' => Mage::helper('nscexport')->__('Product filter'),
			'content' => $this->getLayout()->createBlock('nscexport/adminhtml_nscexport_tab_products')->toHtml(),
			'active' => false
		));
		
        $this->addTab('productscat', array(
            'label'     => Mage::helper('catalog')->__('Attribute filter'),
            'content' => $this->getLayout()->createBlock('nscexport/adminhtml_nscexport_tab_attributes')->toHtml(),
        	'active'  => false
        ));
        
        if( $this->getProfile()->getId()) {
            $this->addTab('submission', array(
                    'label'     => Mage::helper('catalog')->__('Submission Settings'),
                    'content' => $this->getLayout()->createBlock('nscexport/adminhtml_nscexport_tab_submission')->toHtml(),
                    'active'  => false
            ));
        }
        
        $this->_activeTab = $tabActive;

		return parent::_prepareLayout();
	}
}