<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* @category Nostress 
* @package Nostress_Nscexport
*/

class Nostress_Nscexport_Block_Adminhtml_Nscexport_Activation_Form extends Mage_Adminhtml_Block_Widget_Form
{
	protected function _prepareForm() 
	{
		$form = new Varien_Data_Form(array(
			'id' => 'edit_form',
			'action' => $this->getUrl("*/*/activate"),
			'method' => 'post',
			'enctype' => 'multipart/form-data',
		));
		
		$form->setUseContainer(true);
		$this->setForm($form);
		
		$client = Mage::helper('nscexport/data_client');
		$connectorInfo = $client->getConnectorInfoByCode($this->getCode());		
		if(!$connectorInfo)
		{
			$connectorInfo = $client->getConnectorInfoByCode();
		}
		$connectorInfo = new Varien_Object($connectorInfo);
		
		$fieldset = $form->addFieldset('activation_form', array(
			'legend' => Mage::helper('nscexport')->__('Activation Form')
		));		
		
		$fieldset->addField('code', 'hidden', array(
				'name' => 'code',
				'value' => $this->getCode(),
		));
		
		$fieldset->addField('name', 'text', array(
			'label' => $this->__('Name:'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'name'			
		));
		
		$fieldset->addField('email', 'text', array(
				'label' => $this->__('Email:'),
				'class' => 'required-entry validate-email',
				'required' => true,
				'name' => 'email'
		));
		
		$fieldset->addField('telephone', 'text', array(
				'label' => $this->__('Phone:'),
				'class' => 'validate-phone',
				'required' => false,
				'name' => 'telephone'
		));
		
		if($connectorInfo->getIsTrial() == true)
		{
			$fieldset->addField('collection', 'select', array(
				'label' => $this->__('Feed Collection:'),
				'class' => 'required-entry',
				'required' => true,
				'name' => 'collection',
				'values' => $client->getAvailableCollectionsAsOptionArray(),
				'note' => $this->__('What is').' <a target="_blank" href="'.Mage::helper('nscexport')->getHelpUrl(Nostress_Nscexport_Helper_Data::HELP_FEED_COLLECTIONS).'">'
							.$this->__('Feed Collection').'</a>?'
			));
		}
		
		$licenseConditionsLink = $client->getHelpUrl(Nostress_Nscexport_Helper_Data::HELP_LICENSE_CONDITIONS);
		$fieldset->addField('accept_license_conditions', 'checkbox', array(
				'label' => $this->__('I accept Koongo License Condtiions'),
				'note' =>  $this->__('See').' <a href="'.$licenseConditionsLink.'" target="_blank">'.$this->__('Koongo License Condtions').'</a>',
				'name' => 'accept_license_conditions',
				'value' => 0,
				//'checked' => 'false',
				'onclick' => 'this.value = this.checked ? 1 : 0;',
				'disabled' => false,
				'readonly' => false,
				'required' => true,
		));
		
		$class = 'btn-koongo-submit-yellow';
		if($connectorInfo->getIsTrial() == true)
			$class = 'btn-koongo-submit-orange';
		
		$fieldset->addField('submit_and_activate', 'button', array(
			'label' => "",			
			'onclick'   => 'if(editForm.submit()){addOverlay();document.getElementById(\'loading-mask\').style.display = \'block\';}',			
			'class' => $class,
			'value' => strip_tags($connectorInfo->getLabel()),
		));
				
		return parent::_prepareForm();
	}
}