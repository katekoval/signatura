<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* @category Nostress 
* @package Nostress_Nscexport
*/

class Nostress_Nscexport_Block_Adminhtml_Nscexport_New_Form extends Nostress_Nscexport_Block_Adminhtml_Nscexport_Tab
{
	protected function _prepareForm() {
		$form = new Varien_Data_Form(array(
			'id' => 'edit_form',
			'action' => $this->getUrl("*/*/edit", array('id' => $this->getRequest()->getParam('id'))),
			'method' => 'post',
			'enctype' => 'multipart/form-data',
		));
		
		$form->setUseContainer(true);
		$this->setForm($form);
		
		$fieldset = $form->addFieldset('edit_form', array(
			'legend' =>Mage::helper('nscexport')->__('Select Store View and Feed')
		));
		$fieldset->setHeaderBar($this->getHelpButtonHtmlByFieldset("step1"));
		
		$fieldset->addField('profile_store_id', 'select', array(
			'label' => Mage::helper('nscexport')->__('Store View:'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'store',
			'note' => Mage::helper('nscexport')->__('Select Store View.'),
			'values' => Mage::getSingleton('adminhtml/system_store')->getStoreValuesForForm(false, false)
		));
		
		
		if(Mage::helper('nscexport/data_feed')->someFeedsDisabled())
		{	
			$title = Mage::helper('nscexport')->__("Are you missing a feed? Some of the feeds are disabled in connector configuration.");
			$fieldset->addField('nscexport_feed_note', 'link', array(
					'title' => $title,
					'value' => $title,
					'href' => Mage::helper("adminhtml")->getUrl('adminhtml/system_config/edit/section/koongo_config'),	
					'target' => "_blank"						
			));
		}
		
		$fieldset->addField('nscexport_feed', 'select', array(
			'label' => Mage::helper('nscexport')->__('Feed:'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'feed',
			'note' => Mage::helper('nscexport')->__('Select Feed.'),
			'onchange' => "showOption(this); changeType(this)",
			'values' => Mage::helper('nscexport/data_feed')->getFeedOptions(true, false, false)
		));
		
		$fieldset->addField('nscexport_type', 'select', array(
			'label' => Mage::helper('nscexport')->__('Type'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'type',
			'onchange' => "changeFile(this)",
			'values' => array()
		));
		
		$fieldset->addField('nscexport_file', 'select', array(
			'label' => Mage::helper('nscexport')->__('File'),
			'class' => 'required-entry',
			'required' => true,
			'name' => 'file',
			'values' => array()
		));
		
		/*$fieldset->addField('next', 'button', array(
			'label' => "",
			'value' => "Next Step"
		));*/
		
		return parent::_prepareForm();
	}
}