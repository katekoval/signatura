<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
 *
 * @category Nostress
 * @package Nostress_Nscexport
 */

class Nostress_Nscexport_Block_Adminhtml_Nscexport_Grid_Renderer_Action extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Action
{
	public function render(Varien_Object $row)
	{
		$html = "";
		$html .= $this->getRunButtonHtml($row);
		$html .= $this->getEditLinkHtml($row);
		return $html;
	}
	
	protected function getRunButtonHtml($row)
	{
		$url = $this->getUrl('*/nscexport_profiles_grid/generate', array('export_id' => $row->getId()));
		$label = Mage::helper('nscexport')->__('Execute Profile');
		$enabled =  Mage::helper('nscexport/version')->isLicenseValid();
		$disabledHtml = "";
		
		if(!$enabled)
		{
			$label = Mage::helper('nscexport')->__('Execute Disabled');
			$url = "";
			$disabledHtml = 'disabled="disabled" class="disabled"';
		}
		$title =  Mage::helper('nscexport')->__('Execute profile to update feed.');
		$html = '<form><button title="'.$title.'"onclick="event.stopPropagation();setLocation(\''.$url.'\');" type="button" '.$disabledHtml.' >' .$label. '</button></form>';
		return $html;
	}
	
	
	
	protected function getEditLinkHtml($row)
	{
		$url = $this->getUrl("*/*/edit",array("id"=>$row->getId()));
		$label = Mage::helper('nscexport')->__('Edit Profile');
		$html = '<a href="'.$url.'"  title="'.$label.'">'.$label.'</a>';
		return $html;
	}
}
