<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
 * 
 * @category Nostress 
 * @package Nostress_Nscexport
 */

class Nostress_Nscexport_Block_Adminhtml_Nscexport_Grid_Renderer_Categorymapping extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * Prepare link to display in grid
     *
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row)
    {
    	//$feed = $row->getFeedObject();	
    	$taxonomyCode = $row->getTaxonomyCode();//$feed->getTaxonomyCode();
    	if($taxonomyCode)
    	{	
    		$url = $this->getUrl('*/catalog_category/');
    		
    		$taxonomyFullName = $row->getTaxonomyName();
    		$type = $row->getTaxonomyType();
    		if(!empty($type))
    			$taxonomyFullName .= " - ".$row->getTaxonomyType();
    		$title =  Mage::helper('nscexport')->__('Set up %s Categories',$taxonomyFullName);
    		$label =  $row->getTaxonomyName()."<br>".Mage::helper('nscexport')->__('Categories');
    		$html = '<form><button  title="'.$title.'" onclick="event.stopPropagation();setLocation(\''.$url.'\');" type="button" >' .$label. '</button></form>';    		
    		return $html;
    	}
    	else
    	{
    		$feed = $row->getFeedObject();
    		$channelLabel = $feed->getChannelLabel();
    		return  Mage::helper('nscexport')->__('Not supported by %s',$channelLabel);
    	}
    		
    }

}
