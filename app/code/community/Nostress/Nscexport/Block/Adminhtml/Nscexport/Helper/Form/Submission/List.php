<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
 * Form submit element
 *
 * @category   Varien
 * @package    Varien_Data
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Nostress_Nscexport_Block_Adminhtml_Nscexport_Helper_Form_Submission_List extends Varien_Data_Form_Element_Abstract
{
    public function __construct($attributes=array())
    {
        parent::__construct($attributes);
        $this->setExtType('submission_list');
        $this->setType('submission_list');
    }
        
    public function getElementHtml()
    {
		$form = $this->getForm();
		$values = $this->getValues();
				
		$submissionListBlock = $form->getParent()->getLayout()->createBlock('nscexport/adminhtml_nscexport_submission_list')->setData( array(
			'form' => $form,
			'profile' => $this->getProfile(),
		    'api_code' => $this->getApiCode(),
		    'init' => true
        ))->toHtml();
		$html = $submissionListBlock;
		
		return $html;
    }
}
