<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
 *
 * @category Nostress
 * @package Nostress_Nscexport
 */

class Nostress_Nscexport_Block_Adminhtml_Nscexport_Grid_Renderer_Submission extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Action
{
	public function render(Varien_Object $row)
	{
		$html = $this->_getSubmissionButton($row);
		
		$html .= $this->_getSubmissionInfo( $row);
		
		return $html;
	}
	
	protected function _getSubmissionButton( $row) {
	    
	    $apiLabel = $row->getExportApiAdapter()->getLabel();
	    
	    if(strpos($apiLabel, "API") !== false)
	    	$apiLabel = "API";
	    
	    $url = $this->getUrl('*/nscexport_profiles_grid/submission', array('id' => $row->getId()));
	    $label = Mage::helper('nscexport')->__('%s Submission', $apiLabel);
	    $disabledHtml = "";
	     
	    if(!$row->isFileExists())
	    {
	        $url = "";
	        $disabledHtml = 'disabled="disabled" class="disabled"';
	        $title = Mage::helper('nscexport')->__("Please execute profile for allowing this feature.");
	    } else {
	        $title =  Mage::helper('nscexport')->__('%s Submission', $apiLabel);
	    }
	    
	    return '<form><button title="'.$title.'" onclick="event.stopPropagation();setLocation(\''.$url.'\');" type="button" '.$disabledHtml.' >' .$label. '</button></form>';
	}
	
	protected function _getSubmissionInfo( $row) {
	    
	    $enabled =  Mage::helper('nscexport/version')->isLicenseValid();
	    $disabledHtml = "";
	    
	    if(!$enabled)
	    {
	        $disabledHtml = 'disabled="disabled" class="disabled"';
	    }
	    
	    //$html .= "<br/>&nbsp;";
	    $label = Mage::helper('nscexport')->__('Submission Info');
	    $feedObject = $row->getFeedObject();
	    $channelManualUrl = $feedObject->getChannelManualUrl();
	    $channelPageUrl = $feedObject->getChannelPageUrl();
	    $channelLogoUrl = $feedObject->getChannelLogoUrl();
	    
	    if(!Mage::helper('nscexport/version')->isLicenseValid(true))
	    {
	        $channelManualUrl = "empty";
	        $channelPageUrl = "";
	    }
	    
	    $channelLabel = $feedObject->getChannelLabel();
	    $pageLabel = $this->__("How to submit %s feed",$channelLabel);
	    $feedFileUrl = $row->getUrl();
	    if(!(strstr($feedFileUrl,'http') > -1))
	        $feedFileUrl = "";
	    
	    $executeProfileUrl = $this->getUrl('*/nscexport_profiles_grid/generate', array('export_id' => $row->getId()));
	    //button
	    return '<form><button title="'.$label.'" onclick="event.stopPropagation();loadSubmissionInfo(\''.$channelLabel.'\',\''.$pageLabel.'\',\''.$channelPageUrl.'\',\''.$channelManualUrl.'\',\''.$feedFileUrl.'\',\''.$channelLogoUrl.'\',\''.$executeProfileUrl.'\')" type="button" '.$disabledHtml.'>' .$label. '</button></form>';
	    //link
	    //$html .= '<br><a href="#" title="'.$label.'" onclick="event.stopPropagation();loadSubmissionInfo(\''.$pageLabel.'\',\''.$channelPageUrl.'\',\''.$channelManualUrl.'\')">'.$label.'</a>';
	}
}
