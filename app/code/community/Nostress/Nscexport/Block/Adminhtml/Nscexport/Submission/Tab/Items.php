<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
 * Product in category grid
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Nostress_Nscexport_Block_Adminhtml_Nscexport_Submission_Tab_Items extends Mage_Adminhtml_Block_Widget_Grid
{
   
    public function __construct()
    {
        parent::__construct();
        $this->setId('submission_items');
        $this->setDefaultSort('updated_at');
        $this->setDefaultDir('DESC');
        $this->setUseAjax(true);
        $this->setSaveParametersInSession(false);
    }

    protected function _prepareCollection()
    {
        $profileId = $this->getProfile()->getId();
        
        $collection = Mage::getModel( 'nscexport/submission_items')->getCollectionByProfile( $profileId);
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('item_id', array(
            'header' => Mage::helper('nscexport')->__('Product Id'),
            'width' => '10px',
            'align'	=> 'center',
            'index' => 'item_id',
            'type' => 'number'
        ));
        
        $this->addColumn('sku', array(
            'header' => Mage::helper('nscexport')->__('Sku'),
            'width' => '10px',
            'align'	=> 'center',
            'index' => 'sku',
            'filter_index' => 'cpe.sku'
        ));
        
        $this->addColumn('price', array(
            'header' => Mage::helper('nscexport')->__('Price'),
            'align'	=> 'center',
            'index' => 'price',
            'sortable' => false,
            'filter' => false,
        ));
        
        $this->addColumn('discount_price', array(
            'header' => Mage::helper('nscexport')->__('Discount Price'),
            'align'	=> 'center',
            'index' => 'discount_price',
            'sortable' => false,
            'filter' => false,
        ));
        
        $this->addColumn('delivery_cost_nl', array(
                'header' => Mage::helper('nscexport')->__('Delivery Cost NL'),
                'align'	=> 'center',
                'index' => 'delivery_cost_nl',
                'sortable' => false,
                'filter' => false,
        ));
        
        $this->addColumn('delivery_cost_be', array(
                'header' => Mage::helper('nscexport')->__('Delivery Cost BE'),
                'align'	=> 'center',
                'index' => 'delivery_cost_be',
                'sortable' => false,
                'filter' => false,
        ));
        
        $this->addColumn('stock', array(
            'header' => Mage::helper('nscexport')->__('Stock'),
            'align'	=> 'center',
            'index' => 'stock',
            'sortable' => false,
            'filter' => false,
        ));
        
        $this->addColumn('updated_at', array(
            'header' => Mage::helper('nscexport')->__('Updated At'),
            'align'	=> 'center',
            'index' => 'updated_at',
            'type' => 'datetime'
        ));
        
        $this->addColumn('status', array(
            'header' => Mage::helper('nscexport')->__('Status'),
            'align'	=> 'center',
            'index' => 'status',
        ));
        
        $this->addColumn('message', array(
                'header' => Mage::helper('nscexport')->__('Note'),
                'align'	=> 'center',
                'index' => 'message',
        ));
                
        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/submissionItems', array('_current'=>true));
    }
}

