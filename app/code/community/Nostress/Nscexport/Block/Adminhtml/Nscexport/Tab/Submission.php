<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
* @category Nostress
* @package Nostress_Nscexport
*/

class Nostress_Nscexport_Block_Adminhtml_Nscexport_Tab_Submission extends Nostress_Nscexport_Block_Adminhtml_Nscexport_Tab
{
	public function __construct() {
		parent::__construct();
		$this->setShowGlobalIcon(true);
	}
	
	public function _prepareLayout() {
	    parent::_prepareLayout();
	
	    $form = new Varien_Data_Form();
	    $form->setHtmlIdPrefix(Nostress_Nscexport_Model_Profile::EXPORT_API."_");
	    $fieldset = $form->addFieldset('base_fieldset', array('legend' => Mage::helper('nscexport')->__('Submission Settings')));
	
	    $fieldset->setHeaderBar($this->getHelpButtonHtmlByFieldset("export_api"));
	    
	    $fieldset->addType('nostress_button','Nostress_Nscexport_Block_Adminhtml_Nscexport_Helper_Form_Button');
	    
		$url = $this->getUrl("*/*/submission", array( 'id'=> $this->getProfile()->getId()));
		$data = array(
	        'label' => Mage::helper('nscexport')->__("Navigate to Submission page"),
	        'name' => 'submission_button',
		    'onclick' => "updateOutput();$('profile_edit_form').action = $('profile_edit_form').action+'back/submission/'; profileSubmit('neco', false)",
		);
		if( !$this->getProfile()->isFileExists()) {
		    $data['disabled'] = 'disabled';
		    $data['title'] = Mage::helper('nscexport')->__("Please execute profile to enable this feature.");
		    $data['class'] = 'disabled';
		}
		$fieldset->addField('submission_button', 'nostress_button', $data);
		
		$fieldset->addField('submission_note', 'note', array(
	        'text' => Mage::helper('nscexport')->__("Submission Settings were moved to separated page."),
	        'name' => "submission_note",
		));
	    
	    $this->setForm($form);
	}
}
