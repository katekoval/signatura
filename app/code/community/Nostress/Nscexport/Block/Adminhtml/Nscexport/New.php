<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
* @category Nostress
* @package Nostress_Nscexport
*/

class Nostress_Nscexport_Block_Adminhtml_Nscexport_New extends Mage_Adminhtml_Block_Widget_Form_Container
{
	protected $_categoryIds;
	protected $_selectedNodes = null;
	
	public function __construct() {
		parent::__construct();
		$this->_objectId = 'id_new';
		$this->_blockGroup = 'nscexport';
		$this->_controller = 'adminhtml_nscexport';
		$this->_mode = 'new';
		
		$this->_addButton('next_step', array(
                  'label' => Mage::helper('nscexport')->__('Next Step'),
                  'onclick' => 'continueCreate()'
		), -100);
		$this->_removeButton('save');
		
		$this->addLiveChatButton();
		
		$this->_formScripts[] = '
			var productTemplateSyntax = /(^|.|\r|\n)({{(\w+)}})/;
			
			function continueCreate(select) {
				editForm.submit($("edit_form").action);
			}
			
			function setSettings(urlTemplate, setElement) {
				var template = new Template(urlTemplate, productTemplateSyntax);
				setLocation(template.evaluate({attribute_set:$F(setElement)}));
			}';
		$this->_formScripts[] = "
			function showOption(select) {
				select = $(select);
				for (var i = 0, l = select.options.length; i<l; i++) {
					$$('.'+select.id+'_'+select.options[i].value).each(function (el) {
						elsearchengine.style.display = select.selectedIndex==i ? '' : 'none';
					});
				}
			}
			
			function changeType(feedSelect) {
				new Ajax.Updater(
					{success: 'nscexport_type'},
					'".$this->getUrl('adminhtml/nscexport_action/getTypeByFeed')."', {
						asynchronous: true,
						evalScripts: false,
						onComplete: function(request, json) {
							Element.hide('nscexport_type');
							Element.show('nscexport_type');
							changeFile(document.getElementById('nscexport_type'));
						},
						onLoading: function(request, json){},
						parameters: {feed: feedSelect.options[feedSelect.selectedIndex].value}
				});
			}
			
			function changeFile(typeSelect) {
				new Ajax.Updater(
					{success: 'nscexport_file'},
					'".$this->getUrl('adminhtml/nscexport_action/getFileByType')."', {
						asynchronous: true,
						evalScripts: false,
						onComplete: function(request, json) {
							Element.hide('nscexport_file');
							Element.show('nscexport_file');
						},
						onLoading: function(request, json){},
						parameters: {type: typeSelect.options[typeSelect.selectedIndex].value, feed: $('nscexport_feed').options[$('nscexport_feed').selectedIndex].value}
				});
			}";
	}
	
	protected function addLiveChatButton() {
	
	    $this->_addButton('livechat', $this->helper('nscexport')->getLivechatButtonOptions(), -100);
	}
	
	protected function _prepareLayout() {
		if ($this->_blockGroup && $this->_controller && $this->_mode) {
			$this->setChild('form', $this->getLayout()->createBlock($this->_blockGroup . '/' . $this->_controller . '_' . $this->_mode . '_form'));
		}
		return parent::_prepareLayout();
	}
	
	public function getHeaderText() {
		return Mage::helper('nscexport')->__('Koongo Connector')." - ".Mage::helper('nscexport')->__('New Profile').' ('.Mage::helper('nscexport')->__('Step').' 1)';
	}
}