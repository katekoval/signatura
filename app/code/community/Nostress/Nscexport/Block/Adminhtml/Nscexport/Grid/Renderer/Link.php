<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
 *
 * @category Nostress
 * @package Nostress_Nscexport
 */

class Nostress_Nscexport_Block_Adminhtml_Nscexport_Grid_Renderer_Link extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{

    /**
     * Prepare link to display in grid
     *
     * @param Varien_Object $row
     * @return string
     */
    public function render(Varien_Object $row)
    {
        $html = "";
        $html .= $this->getPreviewButtonHtml($row);
        //$html .= $this->getLinkButton($row); - not used actualy
        return $html;
    }
    
    protected function getLinkButton( $row) {
        
        if(strstr($row->getUrl(),'http') > -1)
        {
            $url = $row->getUrl();
            $title =  Mage::helper('nscexport')->__('Open Feed File');
            $html = '<form><button title="'.$title.'" onclick="event.stopPropagation();setLocation(\''.$url.'\');" type="button" >' .$title. '</button></form>';
            $html .= sprintf('<a href="%1$s" style="display:block;text-align:center;width:100%;" >'.$this->__("Feed File Link").'</a>', $row->getUrl());
            return $html;
        }
        else
            return $row->getUrl();
    }
    
    protected function getPreviewButtonHtml($row)
    {
        $url = $this->getUrl('*/nscexport_profiles_grid/preview', array('export_id' => $row->getId()));
        $label = Mage::helper('nscexport')->__('Show Preview');
        $enabled =  Mage::helper('nscexport/version')->isLicenseValid();
        $disabledHtml = "";
    
        if(!$enabled || !$row->isFileExists())
        {
            $url = "";
            $disabledHtml = 'disabled="disabled" class="disabled"';
            $title = Mage::helper('nscexport')->__("Please execute profile to enable this feature.");
        } else {
            $title =  Mage::helper('nscexport')->__('Show Preview of profile to check how feed looks like.');
        }
        
        $html = '<form><button title="'.$title.'"onclick="event.stopPropagation();setLocation(\''.$url.'\');" type="button" '.$disabledHtml.' >' .$label. '</button></form>';
        return $html;
    }

}

