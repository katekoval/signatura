<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
* @category Nostress
* @package Nostress_Nscexport
*
*/

class Nostress_Nscexport_Block_Adminhtml_Nscexport_Docs extends Mage_Adminhtml_Block_Widget_Container
{
    
  public function __construct()
  {
    $this->_headerText = Mage::helper('nscexport')->__('Koongo university - Watch Koongo explaning videos!');
    
    $this->_addButton('documentation', array(
        'label'     => Mage::helper('nscexport')->__('Are you experiencing some issues? Check out Koongo Documentation!'),
        'onclick'   => "setLocation('https://docs.koongo.com/display/koongo/Connector+for+Magento');",
        'class'     => 'reload',
    ), -100);
    
    $this->_addButton('support', array(
        'label'     => Mage::helper('nscexport')->__('Support Desk'),
        'onclick'   => "setLocation('https://store.koongo.com/support-and-contact.html');",
        'class'     => 'reload',
    ), -100);
    
    $this->_addButton('livechat', $this->helper('nscexport')->getLivechatButtonOptions(), 100);
  }
  
  public function getVideos() {
      
      $videos = Mage::helper( 'nscexport/data_client')->getUniversityInfo();
      if( isset( $videos['videos'])) {
          return $videos['videos'];
      } else {
          return false;
      }
  }
}