<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
* @category Nostress
* @package Nostress_Nscexport
*/

class Nostress_Nscexport_Block_Adminhtml_Nscexport_Activation extends Mage_Adminhtml_Block_Widget_Form_Container
{
	public function __construct()
	{
		$this->_objectId = 'id';
		$this->_controller = 'adminhtml_nscexport';
		$this->_blockGroup = 'nscexport';
		$this->_mode = 'activation';
		
		parent::__construct();
		
		$this->addLiveChatButton();
		$this->_removeButton('reset');
		$this->_removeButton('save');
		$this->_removeButton('back');
	}
	
	protected function addLiveChatButton() {
	
		$this->_addButton('livechat', $this->helper('nscexport')->getLivechatButtonOptions(), -1);
	}
	
	public function getHeaderText()
	{
		$client = Mage::helper('nscexport/data_client');
		$connectorInfo = $client->getConnectorInfoByCode($this->getCode());
		if(empty($connectorInfo))
			$connectorInfo = $client->getConnectorInfoByCode();
		
		$title = "";
		if(isset($connectorInfo["title"]))
			$title = $connectorInfo["title"];
		return Mage::helper('nscexport')->__('Koongo Connector')." - ".$title;//." ".Mage::helper('nscexport')->__('Activation');
	}
}