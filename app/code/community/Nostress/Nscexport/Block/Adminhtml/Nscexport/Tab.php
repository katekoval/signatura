<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
* @category Nostress
* @package Nostress_Nscexport
*/

class Nostress_Nscexport_Block_Adminhtml_Nscexport_Tab extends Mage_Adminhtml_Block_Widget_Form
{
	protected $_generalConfig;
    protected $_feedConfig;
    protected $_productConfig;
    protected $_exportApiConfig;
	protected $_profile;
	protected $_feed;
	protected $_config;
	protected $_type;
	protected $_file;
	
	const COMMON_PATH = "common";
	
	public function getProfile() {
		if (!$this->_profile) {
			$this->_profile = Mage::registry('nscexport_profile');
		}
		return $this->_profile;
	}
	
	public function getConfig() {
		if (!$this->_config) {
			$this->_config = Mage::registry('nscexport_profile_config');
		}
		if( !$this->_config && $this->getProfile()) {
		    $this->_config = $this->getProfile()->getConfig();
		}
		return $this->_config;
	}
	
    public function getGeneralConfig() {
		if (!$this->_generalConfig) {
			$config = $this->getConfig();
			if(is_array($config) && array_key_exists("general",$config))
			    $this->_generalConfig = $config["general"];
			else
			    $this->_generalConfig = array();
		}
		return $this->_generalConfig;
	}
	
    public function getFeedConfig() {
		if (!$this->_feedConfig) {
			$config = $this->getConfig();
			if(is_array($config) && array_key_exists("feed",$config))
			    $this->_feedConfig = $config["feed"];
			else
			    $this->_feedConfig = array();
		}
		return $this->_feedConfig;
	}
	
    public function getExportApiConfig() {
	    if (!$this->_exportApiConfig) {
	        $this->_exportApiConfig = $this->getProfile()->getExportApiConfig();
	    }
	    return $this->_exportApiConfig;
	}
	
    public function getProductConfig()
    {
		if (!$this->_productConfig)
		{
			$config = $this->getConfig();
			if(is_array($config) && array_key_exists("product",$config))
			{
			    $productConfig = $config["product"];
			    if(isset($productConfig['types']))
			    {
			        $productConfig['types'] = explode(",",$productConfig['types']);
			    }
			    $this->_productConfig = $productConfig;
			}
			else
			    $this->_productConfig = array();
		}
		return $this->_productConfig;
	}
	
	public function getAttributeConfig()
	{
		if (!$this->_attributeConfig) {
			$config = $this->getConfig();
			if(array_key_exists("attribute_filter",$config))
			    $this->_attributeConfig = $config["attribute_filter"];
			else
			    $this->_attributeConfig = array();
		}
		return $this->_attributeConfig;
	}
	
	public function getAttributeValue($branch, $attribute = null)
	{
		$feedConfig = $this->getFeedConfig();
	
		if ($attribute != null && isset($feedConfig[$branch][$attribute]))
		{
			return $feedConfig[$branch][$attribute];
		}
		else
		{
			return "";
		}
	}
	
	public function getStoreName($storeId = null) {
		$store = null;
		if (!$storeId) {
			$store = $this->getStore();
		}
		else {
			$store = Mage::app()->getStore($storeId);
		}
		return Mage::helper('nscexport')->getFullStoreName($store);
	}
	
	public function getFeedName($feedId = null) {
		if (!$feedId) {
			return $this->getFeed()->getLink();
		}
		return Mage::getModel("nscexport/feed")->getFeedByCode($feedId)->getLink();
	}
	
	protected function getConfigField($index,$dataIndex = null)
	{
		if(!isset($dataIndex))
			$dataIndex = $index;
					
		$result = null;
		if (Mage::registry('nscexport_data')) {
			$result = Mage::registry('nscexport_data')->getData($dataIndex);
		}
		if (!$result) {
			$result = $this->getValue($index);
		}
		if (!$result) {
			$result = $this->getRequest()->getParam($index);
		}
		return $result;
	}
	
	public function getType()
	{
		if (!$this->_type)
		{
			$this->_type = $this->getConfigField("type");
		}
		return $this->_type;
	}
	
	public function getFile()
	{
		if (!$this->_file) {
			$this->_file = $this->getConfigField("file");
		}
		return $this->_file;
	}
	
    /**
     * @return Nostress_Nscexport_Model_Feed
     */
	public function getFeed()
	{
		if (!$this->_feed)
		{
			$feedId = $this->getConfigField("feed");
			$this->_feed = Mage::getModel("nscexport/feed")->getFeedByCode($feedId);
		}
		return $this->_feed;
	}
	
	public function getStore()
	{
		if (!$this->_store)
		{
			$this->_store = Mage::app()->getStore($this->getProfile()->getStoreId());
		}
		return $this->_store;
	}
	
	protected function arrayField($array,$key,$default = "")
	{
		if(is_array($array) && array_key_exists($key,$array))
			return $array[$key];
		else
			$default;
	}
	
    /**
     * Create buttonn and return its html
     *
     * @param string $label
     * @param string $onclick
     * @param string $class
     * @param string $id
     * @return string
     */
    public function getButtonHtml($label, $onclick, $class='', $id=null) {
        return $this->getLayout()->createBlock('adminhtml/widget_button')
            ->setData(array(
                'label'     => $label,
                'onclick'   => $onclick,
                'class'     => $class,
                'type'      => 'button',
                'id'        => $id,
            ))
            ->toHtml();
    }
    
    public function getHelpButtonHtmlByFieldset($fieldsetId)
    {
    	$targetUrl = Mage::helper('nscexport')->getHelpUrl($fieldsetId);
    	return $this->getHelpButtonHtml($targetUrl);
    }
    
    public function getHelpButtonHtml($targetUrl)
    {
    	$label = Mage::helper('nscexport')->__('Get Help!');
    	$class = 'scalable go';
    	$onclick = "window.open('{$targetUrl}')";
    	return $this->getButtonHtml($label,$onclick, $class, $id=null);
    }
}
