<?php

class Nostress_Nscexport_Block_Adminhtml_Nscexport_Submission_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    

  public function __construct()
  {
      parent::__construct();
      $this->setId('submission_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('nscexport')->__('Submission'));
  }

  protected function _beforeToHtml()
  {
     $adapter = $this->getProfile()->getExportApiAdapter();
     $apiLabel = $adapter->getShortLabel();
      
     $this->addTab('settings', array(
        'label'     => $apiLabel." ".Mage::helper('nscexport')->__('Settings'),
        'title'     => $apiLabel." ".Mage::helper('nscexport')->__('Settings'),
        'content' => $this->getLayout()->createBlock('nscexport/adminhtml_nscexport_submission_tab_settings', 'settings')->toHtml(),
        'active' => true
     ));
     
     if( $adapter->isItemable()) {
         $this->addTab('items', array(
             'label'     => Mage::helper('nscexport')->__('Products Report'),
             'title'     => Mage::helper('nscexport')->__('Products Report'),
             'url'       => $this->getUrl('*/*/submissionItems', array('_current' => true)),
             'class'     => 'ajax',
         ));
     }
     
     $this->addTab('manual', array(
         'label'     => Mage::helper('nscexport')->__('Submission Manual'),
         'title'     => Mage::helper('nscexport')->__('Submission Manual'),
         'content' => $this->getLayout()->createBlock('nscexport/adminhtml_nscexport_submission_tab_manual', 'manual')->toHtml(),
     ));
     
     return parent::_beforeToHtml();
  }
}