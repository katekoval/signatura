<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
* @category Nostress
* @package Nostress_Nscexport
*/

class Nostress_Nscexport_Block_Adminhtml_Nscexport_Tab_General extends Nostress_Nscexport_Block_Adminhtml_Nscexport_Tab
{
	public function __construct() {
		parent::__construct();
		$this->setShowGlobalIcon(true);
	}
	
	public function _prepareLayout() {
		parent::_prepareLayout();
				
		$form = new Varien_Data_Form();
		$form->setHtmlIdPrefix('_general');
		$fieldset = $form->addFieldset('base_fieldset', array('legend' => Mage::helper('nscexport')->__('General Information'), 'class' => "collapseable"));
		
		$fieldset->setHeaderBar($this->getHelpButtonHtmlByFieldset("general_info"));
		$yesnoSource = Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray();
				$compressFile = "";
				
		$generalConfig = $this->getGeneralConfig();
		
		$profile = $this->getProfile();
						
		if ($profile->getId()) {
			$fieldset->addField('id', 'hidden', array(
				'name' => 'id',
				'value' => $profile->getId()
			));
		}
		
		$freqItem = new Mage_Adminhtml_Model_System_Config_Source_Cron_Frequency();
      	$freqArr = $freqItem->toOptionArray();
		
      	$fieldset->addField('feed', 'hidden', array(
				'name' => 'feed',
				'value' => $profile->getFeed()
			));
		
		$fieldset->addField('store_id', 'hidden', array(
			'name' => 'store_id',
			'value' => $profile->getStoreId()
		));
		
		$fieldset->addField('store', 'text', array(
			'label' => Mage::helper('nscexport')->__("Store View:"),
			'name' => "store",
			'disabled' => true,
			'value' => $this->getStoreName($profile->getStoreId())
		));
		
		$enabled = $profile->getEnebled();
		$fieldset->addField('enabled', 'select', array(
			'label' => Mage::helper('nscexport')->__("Enabled:"),
			'name' => "enabled",
			'values' => $yesnoSource,
			'value' => !empty($enabled)?$enabled:"1"
		));
		
		$fieldset->addField('name', 'text', array(
			'label' => Mage::helper('nscexport')->__("Profile Name:"),
			'name' => "name",
			'required' => true
		));
		
		$fieldset->addField('nscexport_filename', 'text', array(
			'label' => Mage::helper('nscexport')->__("Export File Name:"),
			'name' => "filename",
			'required' => true,
			'note' => Mage::helper('nscexport')->__("File Name without suffix - . xml or .csv will be added according to selected feed type.")
		));
		
		$fieldset->addField('compress_file', 'select', array(
			'label' => Mage::helper('nscexport')->__("Compress File:"),
			'name' => "compress_file",
			'values' => $yesnoSource
		));
		
		$fieldset2 = $form->addFieldset('cron_fieldset', array('legend' => Mage::helper('nscexport')->__('Profile Run Schedule'), 'class' => "collapseable"));
		
		$fieldset2->setHeaderBar($this->getHelpButtonHtmlByFieldset("general_info"));
		$fieldset2->addType( 'nscexport_checkboxes', 'Nostress_Nscexport_Model_Data_Form_Element_Checkboxes');
		
		
		
		
			
		if( $profile->isApi()) {
		    
		    $field = $fieldset2->addField( 'cron_interval', 'select', array(
	            'label' => Mage::helper('nscexport')->__("Frequency").":",
	            'name' => "cron_interval",
	            'values' => Mage::getSingleton('nscexport/config_source_croninterval')->toOptionArray(),
	            'value' => Mage::getSingleton('nscexport/config_source_croninterval')->getDefaultValue()
		    ));
		    
		} else {
		    $cronDaysElement = $fieldset2->addField( 'cron_days', 'nscexport_checkboxes', array(
		            'label' => Mage::helper('nscexport')->__("Days").":",
		            'name' => "cron_days",
		            'values' => Mage::getSingleton('nscexport/config_source_dayofweek')->toOptionArray(),
		            'value' => Mage::getSingleton('nscexport/config_source_dayofweek')->getAllValues()
		    ));
		    
		    $field = $fieldset2->addField( 'cron_times', 'nscexport_checkboxes', array(
	            'label' => Mage::helper('nscexport')->__("Times").":",
	            'name' => "cron_times",
	            'values' => Mage::getSingleton('nscexport/config_source_daytimes')->toOptionArray(),
	            'class' => 'floated',
	            'value' => array(Mage::getSingleton('nscexport/config_source_daytimes')->getDefaultValue())
		    ));
		}
		
		if(isset($generalConfig["compress_file"]))
		{
			$form->addValues(array("compress_file" => $generalConfig["compress_file"]));
		}

		$id = $profile->getId();
		if($id)
		{
			$cronModel = Mage::getModel("nscexport/cron");
			if( !$profile->isApi()) {
    			$generalConfig["cron_days"] = $cronModel->getDaysPerProfile($id);
    			$generalConfig["cron_times"] = $cronModel->getTimesPerProfile($id);
			}
			$generalConfig["nscexport_filename"] = $profile->getFilename(false,"");
		}
		else
		{
			$feed = $profile->getFeedObject();
						
			$generalConfig["name"] = $feed->getLink();
			$fileIndex = rand(0,10000);
			$generalConfig["nscexport_filename"] = $feed->getChannelCode()."_".$fileIndex;
		}
		
		$form->addValues($profile->getData());
		// vlozeni dnu a casu do formulare
		$form->addValues( $generalConfig);
		
		$form->setFieldNameSuffix('general');
		$this->setForm($form);
	}
}
