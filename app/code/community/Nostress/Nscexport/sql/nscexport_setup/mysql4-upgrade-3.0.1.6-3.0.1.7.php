<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* Sql update skript
* 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

$this->startSetup()->run("

CREATE TABLE IF NOT EXISTS {$this->getTable('nostress_export_enginecategory')} (
  `entity_id` int(11) unsigned NOT NULL auto_increment,   
  `engine_code` varchar(255) NOT NULL,
  `locale` varchar(255) NOT NULL DEFAULT 'en_UK',
  `name` varchar(255) NOT NULL DEFAULT '',
  `id` int(11) NOT NULL DEFAULT '-1',
  `path` text CHARACTER SET utf8 DEFAULT '' NOT NULL,
  `ids_path` text CHARACTER SET utf8 DEFAULT '' NOT NULL, 
  `level` int(11) NOT NULL DEFAULT '-1', 
  `parent_name` varchar(255) DEFAULT '',
  `parent_id` int(11) DEFAULT '-1',
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE {$this->getTable('nostress_export')}
ADD COLUMN `start_time` time DEFAULT '00:00:00' AFTER `frequency`,
ADD COLUMN `product_url_params` varchar(255) DEFAULT NULL AFTER `url`,
DROP `centrumcategory`,
DROP `start_time_hour`,
DROP `start_time_minute`,
DROP `start_time_second`;
    
")->endSetup();

