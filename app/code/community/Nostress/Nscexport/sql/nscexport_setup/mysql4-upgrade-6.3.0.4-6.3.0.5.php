<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* Sql instalation skript
* 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

$installer = $this;

$installer->startSetup();

$helper = Mage::helper('nscexport/data_loader');
$taxonomyModel = Mage::getModel('nscexport/taxonomy');
$taxonomySetupCollection = Mage::getModel('nscexport/taxonomy_setup')->getCollection()->load();
$updateQuery = "";
$stores = $helper->getStoresWithLocale();

//transform category taxonomy mapping identificator to hash(from id)
foreach($taxonomySetupCollection as $taxonomyItem)
{
	$code =  $taxonomyItem->getCode();
	$config = $taxonomyItem->getDecodedSetup();
	$config = $taxonomyModel->prepareTaxonomyConfig($config);
	
	//get taxonomy attribute id
	$attributeCode = $helper->createCategoryAttributeCode($code);
	$attribute = Mage::getModel('eav/entity_attribute')->loadByCode(Mage_Catalog_Model_Category::ENTITY, $attributeCode);
	$attributeId = $attribute->getId();
	
	//get value column name
	$fields = $taxonomyModel->getSelectFields($code);
	$valueColumn = 'id';
	if(isset($fields['value']))
		$valueColumn = $fields['value'];
	
	foreach($stores as $store)
	{
		$storeId = $store->getId();		
		$locale = $helper->getStoreTaxonomyLocale($code,$store['locale']);
		
		$updateQuery .= "UPDATE {$this->getTable('catalog_category_entity_text')} AS dest,
			(
			SELECT *
			FROM {$this->getTable('nostress_export_enginecategory')} 
			WHERE taxonomy_code = '{$code}'
			AND locale = '{$locale}'
			) AS src
			SET dest.value = src.hash WHERE store_id = {$storeId} AND attribute_id = '{$attributeId}' AND dest.value = src.{$valueColumn};
			";
	}
}

$installer->run($updateQuery);

$installer->endSetup(); 
