<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* Sql update skript
* 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

$this->startSetup()->run("

ALTER TABLE {$this->getTable('nostress_export')}
ADD COLUMN  `centrumcategory` varchar(5) NULL default '',     
ADD COLUMN  `start_time_hour` varchar(2) NULL default '00',
ADD COLUMN  `start_time_minute` varchar(2) NULL default '00',
ADD COLUMN  `start_time_second` varchar(2) NULL default '00',
DROP `start_time`,
DROP `product_url_params`;

")->endSetup();

