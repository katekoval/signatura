<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* Sql update skript
* Table stores relation product-category, where category has max level from categories related to the product. 
* Particular records are distinguished by export profile id.
* 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

$this->startSetup()->run("
CREATE TABLE IF NOT EXISTS {$this->getTable('nostress_export_cache_profilecategory')} (
  `export_id` int(10) unsigned NOT NULL COMMENT 'Export Profile Id',
  `product_id` int(10) unsigned NOT NULL COMMENT 'Product Id',
  `category_id` int(10) unsigned DEFAULT NULL COMMENT 'Main Category Id',
  `category_max_level` smallint(6) DEFAULT '-1' COMMENT 'Main Category max Level',
  `categories` text COMMENT 'Categories',
  `category_ids` varchar(255) DEFAULT NULL COMMENT 'Category_ids'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Cache table for profile product categories';
")->endSetup();

