<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* Sql instalation skript
* 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

$installer = $this;

$installer->startSetup();

$installer->run("
	ALTER TABLE {$this->getTable('nostress_export_enginecategory')} 
		ADD COLUMN `hash` varchar(255) NOT NULL AFTER `locale`;
	UPDATE {$this->getTable('nostress_export_enginecategory')}  AS dest, 
			(SELECT  entity_id,MD5(path) AS pathhash FROM {$this->getTable('nostress_export_enginecategory')} ) AS src
			SET dest.hash = src.pathhash 
			WHERE dest.entity_id = src.entity_id;	
");

$installer->endSetup(); 
