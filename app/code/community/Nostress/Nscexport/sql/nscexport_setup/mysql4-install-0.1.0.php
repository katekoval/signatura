<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* Sql instalation skript
* 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

$installer = $this;

$installer->startSetup();

$installer->run("

-- DROP TABLE IF EXISTS {$this->getTable('nostress_export')};
CREATE TABLE {$this->getTable('nostress_export')} (
  `export_id` int(11) unsigned NOT NULL auto_increment, 
  `name` varchar(255) NOT NULL default '',
  `enabled` int(1) NOT NULL default '0',  
  `frequency` varchar(1) NULL ,
  `filename` varchar(255) character set utf8 default NULL,
  `url` varchar(255) character set utf8 default NULL,
  `searchengine` varchar(255) NOT NULL default '',
  `centrumcategory` varchar(5) NULL default '',     
  `start_time_hour` varchar(2) NULL default '00',
  `start_time_minute` varchar(2) NULL default '00',
  `start_time_second` varchar(2) NULL default '00',
  `category_ids` varchar(1000) NULL,
  `created_time` datetime NULL,
  `update_time` datetime NULL,
  PRIMARY KEY (`export_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

    ");

$installer->endSetup(); 
