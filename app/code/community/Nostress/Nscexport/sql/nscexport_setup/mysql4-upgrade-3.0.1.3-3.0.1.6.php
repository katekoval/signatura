<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* Sql update skript
* 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

$this->startSetup()->run("

-- DROP TABLE IF EXISTS {$this->getTable('nostress_export_categoryproducts')};
ALTER TABLE {$this->getTable('nostress_export')}
DROP `category_ids`;
ALTER TABLE {$this->getTable('nostress_export')}
DROP `product_id`;


CREATE TABLE {$this->getTable('nostress_export_categoryproducts')} (
  `entity_id` bigint(20) unsigned NOT NULL auto_increment, 
  `export_id` int(11) unsigned NOT NULL, 
  `category_id` int(10) unsigned NOT NULL, 
  `product_id` int(10) unsigned NOT NULL, 
  FOREIGN KEY (export_id) REFERENCES {$this->getTable('nostress_export')} (export_id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (category_id) REFERENCES {$this->getTable('catalog_category_entity')}(entity_id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (product_id) REFERENCES {$this->getTable('catalog_product_entity')}(entity_id) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE {$this->getTable('nostress_export_records')} (
  `entity_id` bigint(20) unsigned NOT NULL auto_increment, 
  `export_id` int(11) unsigned NOT NULL, 
  `relation_id` bigint(20) unsigned NOT NULL, 
  FOREIGN KEY (export_id) REFERENCES {$this->getTable('nostress_export')}(export_id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (relation_id) REFERENCES {$this->getTable('nostress_export_categoryproducts')}(entity_id) ON DELETE CASCADE ON UPDATE CASCADE,
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

")->endSetup();

