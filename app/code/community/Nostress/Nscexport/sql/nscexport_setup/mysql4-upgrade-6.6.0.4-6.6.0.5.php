<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
* Sql update skript
* Table stores relation product-category, where category has max level from categories related to the product.
* Particular records are distinguished by export profile id.
*
* @category Nostress
* @package Nostress_Nscexport
*
*/

$this->startSetup()->run("

ALTER TABLE {$this->getTable('nostress_export_feed')}
    ADD COLUMN `api_file_type`  varchar(255) NULL AFTER `file_type`;
    
DROP TABLE IF EXISTS {$this->getTable('nostress_export_submission_items')};
CREATE TABLE {$this->getTable('nostress_export_submission_items')} (
  `entity_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) unsigned DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `data` varchar(1024) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`entity_id`),
  KEY `nscexport_submission_profile_id_idx` (`profile_id`),
  CONSTRAINT `nscexport_submission_profile_id` FOREIGN KEY (`profile_id`) REFERENCES {$this->getTable('nostress_export')} (`export_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS {$this->getTable('nostress_export_submission_report')};
CREATE TABLE {$this->getTable('nostress_export_submission_report')} (
  `entity_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `profile_id` int(11) unsigned DEFAULT NULL,
  `merchant_id` varchar(45) DEFAULT NULL,
  `submission_id` varchar(255) DEFAULT NULL,
  `submit_date` datetime DEFAULT NULL,
  `status` varchar(45) DEFAULT NULL,
  `repport_message` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`entity_id`),
  KEY `nscexport_submission_report_profile_id_idx` (`profile_id`) USING BTREE,
  CONSTRAINT `nostress_export_submission_report_ibfk_1` FOREIGN KEY (`profile_id`) REFERENCES {$this->getTable('nostress_export')} (`export_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 
")->endSetup();