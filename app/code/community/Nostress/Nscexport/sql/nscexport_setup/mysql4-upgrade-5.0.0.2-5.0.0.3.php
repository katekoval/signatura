<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* Sql instalation skript
* 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

$installer = $this;

$installer->startSetup();

$installer->run("

CREATE TABLE IF NOT EXISTS {$this->getTable('nostress_export_feed')} (
  `entity_id` int(11) unsigned NOT NULL auto_increment,   
  `code` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `enabled` int(1) NOT NULL default '0', 
  `type` varchar(255) NULL,  
  `country` varchar(255) NOT NULL DEFAULT 'INTERNATIONAL',
  `file_type` varchar(255) NOT NULL DEFAULT 'xml',
  `taxonomy_code` varchar(255) NULL,
  `layout` text NOT NULL, 
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS {$this->getTable('nostress_export_taxonomy_setup')} (
	`entity_id` int(11) unsigned NOT NULL auto_increment, 
	`name` varchar(255) NOT NULL,
	`code` varchar(255) NOT NULL,
	`type` varchar(255) NULL,  
	`setup` text NOT NULL,
  PRIMARY KEY (`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE {$this->getTable('nostress_export_enginecategory')} 
	CHANGE `engine_code` `taxonomy_code` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL; 

ALTER TABLE {$this->getTable('nostress_export')} 
	CHANGE `searchengine` `feed` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT ''; 
");

$installer->endSetup(); 
