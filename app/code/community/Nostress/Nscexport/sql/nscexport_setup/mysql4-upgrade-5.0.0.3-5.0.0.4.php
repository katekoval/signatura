<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* Sql instalation skript
* 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

$installer = $this;

$installer->startSetup();

$installer->run("

CREATE TABLE IF NOT EXISTS {$this->getTable('nostress_export_cache_categorypath')} ( 
	`category_id` int(10) unsigned NOT NULL,
	`store_id` smallint(5) unsigned NOT NULL, 
	`category_path` text CHARACTER SET utf8,
	`ids_path` varchar(255) CHARACTER SET utf8 NOT NULL, 
	`level` int(11) NOT NULL, 
	PRIMARY KEY (`category_id`,`store_id`) 
	)ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS {$this->getTable('nostress_export_cache_categories')} ( 
	`product_id` int(10) unsigned NOT NULL, 
	`store_id` smallint(5) unsigned NOT NULL, 
	`categories` text CHARACTER SET utf8, 
	PRIMARY KEY (`product_id`,`store_id`) 
	)ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS {$this->getTable('nostress_export_cache_superattributes')} ( 
	`product_id` int(10) unsigned NOT NULL, 
	`store_id` smallint(5) unsigned NOT NULL, 
	`super_attributes` text CHARACTER SET utf8, 
	PRIMARY KEY (`product_id`,`store_id`) 
	)ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS {$this->getTable('nostress_export_cache_mediagallery')} ( 
	`product_id` int(10) unsigned NOT NULL, 
	`store_id` smallint(5) unsigned NOT NULL, 
	`media_gallery` text CHARACTER SET utf8, 
	PRIMARY KEY (`product_id`,`store_id`) 
	)ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS {$this->getTable('nostress_export_cache_tax')} ( 
	`tax_class_id`  smallint(6) NOT NULL, 
	`store_id` smallint(5) unsigned NOT NULL, 
	`tax_percent` decimal(12,4) NOT NULL, 
	PRIMARY KEY (`tax_class_id`,`store_id`) 
	)ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

ALTER TABLE {$this->getTable('nostress_export')}	
	ADD COLUMN `config` TEXT character set utf8 NOT NULL AFTER `feed`,	
	CHANGE `status` `status` ENUM('RUNNING','FINISHED','INTERRUPTED','ERROR') CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'FINISHED',
	DROP `product_url_params`;

DROP TABLE IF EXISTS {$this->getTable('nostress_export_records')};

");


$installer->endSetup(); 
