<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* Sql update skript
* Table stores relation product-category, where category has max level from categories related to the product
* 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

$this->startSetup()->run("
CREATE TABLE IF NOT EXISTS {$this->getTable('nostress_export_cache_categorymaxlevel')} ( 
	`product_id` int(10) unsigned NOT NULL,
	`store_id` smallint(5) unsigned NOT NULL,  
	`category_id` int(10) unsigned NOT NULL,
	`max_level` int(11) NOT NULL DEFAULT '-1',	
	PRIMARY KEY (`product_id`,`store_id`) 
	)ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Stores relation product-category, where category has max level from categories related to the product';    
")->endSetup();

