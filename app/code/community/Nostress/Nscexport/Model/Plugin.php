<?php 
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

class Nostress_Nscexport_Model_Plugin extends Nostress_Nscexport_Model_Abstract
{   	
	public function _construct() 
	{
		parent::_construct ();
		$this->_init ('nscexport/plugin');
	}
	
	public function updatePluginInfo($info)
	{
		if(empty($info))
			return;
			
		$collection = $this->getCollection()->load();
		
		//update existing items
		foreach ($collection as $item)
		{
			$code = $item->getCode();
			$pluginInfo = array();
			if(isset($info[$code]))
			{
				foreach ($info[$code] as $key => $value)
					$item->setData($key,$value);
				$item->setUpdateTime($this->helper()->getDatetime());
				$item->save();
				
				unset($info[$code]);
			}
		}
		
		//add new items
		$this->insertData($info, $collection); 
	}
	
	protected function insertData($data, $collection) 
	{
		$now = $this->helper()->getDatetime();
		foreach ($data as $itemData) 
		{			
			$colItem = $collection->getNewEmptyItem();
			$colItem->setData($itemData);
			$colItem->setUpdateTime($now);
			$collection->addItem($colItem);
		}
		$collection->save();
	}
}
?>