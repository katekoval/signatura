<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
*
* @category Nostress
* @package Nostress_Nscexport
*
*/

class Nostress_Nscexport_Model_Rule_Condition_Product extends Mage_CatalogRule_Model_Rule_Condition_Product
{
    /**
     *
     * @var
     */
    protected $_store = null;
    
    public function __construct()
    {
        parent::__construct();
        $this->setType('nscexport/rule_condition_product');
    }

    public function getJsFormObject()
    {
        return 'conditions';
    }

    public function loadOperatorOptions()
    {
        parent::loadOperatorOptions();
        $byInputType = $this->getOperatorByInputType();
        $byInputType['multiselect'] = array('==', '!=', '()', '!()');
        $this->setOperatorByInputType($byInputType);
        return $this;
    }

    public function loadAttributeOptions()
    {
        $attributes = array();
        $attributesValues = Mage::helper('nscexport/data_feed')->getAttributeOptions( $this->getStoreId(), true,"",false,$this->getFeedCode());
        unset( $attributesValues[0]);
        
        $disabledAttributes = Mage::helper('nscexport')->getConditionsDisabledAttributes();
        
        foreach( $attributesValues as $key => $attribute) {
            if( !in_array( $key, $disabledAttributes)) {
                $attributes[ $attribute['value']] = $attribute['label'];
            }
        }
        $this->setAttributeOption($attributes);

        return $this;
    }

    public function getInputType()
    {
        if ($this->getAttribute()==='type_id') {
            return 'multiselect';
        }
        return parent::getInputType();
    }

    public function getValueElementType()
    {
        if ($this->getAttribute()==='type_id') {
            return 'multiselect';
        }
        return parent::getValueElementType();
    }

    public function getValueSelectOptions()
    {
        if ($this->getAttribute()==='type_id') {
            $arr = Mage::getSingleton('catalog/product_type')->getOptionArray();
            $options = array();
            foreach ($arr as $k=>$v) {
                $options[] = array('value'=>$k, 'label'=>$v);
            }
            return $options;
        }
        return parent::getValueSelectOptions();
    }
    
    protected function _getAttributeAliases( $attribute, $columns) {
        
        foreach( $columns as $tableAlias => $tableColumns) {
            foreach( $tableColumns as $columnAlias => $columnValue) {
                if( $attribute == $columnAlias) {
                    if( strpos( $columnValue, $tableAlias) !== false) {
                        return $columnValue;
                    } else {
                        return $tableAlias.".".$columnValue;
                    }
                }
            }
        }
        return false;
    }
    
    /**
     * @param Zend_Db_Select
     * @return bool|mixed|string
     */
    public function asSqlWhere( $columns)
    {
        $a = $where = $this->getAttribute();
        $o = $this->getOperator();
        $v = $this->getValue();
        if (is_array($v)) {
            $ve = addslashes(join(',', $v));
        } else {
            $ve = addslashes($v);
        }
        
        if(($attributeAlias = $this->_getAttributeAliases($a, $columns)) === false) {
            return false;
        }


        $attr = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $a);

        // whether attribute is multivalue
        $m = $attr->getId() && ($attr->getFrontendInput() == 'multiselect');

        switch ($o) {
        case '==': 
            $wt = '{{ta}}'.'='."'{$ve}'";
            break;
        case '!=':
            if(!empty($ve))
            	$wt = '{{ta}}'.'<>'."'{$ve}' OR {{ta}} IS NULL";
        	else 
        		$wt = '{{ta}}'.'<>'."'' AND {{ta}} IS NOT NULL";
            break;

        case '>=': case '<=': case '>': case '<':
            $wt = "{{ta}}{$o}'{$ve}'";
            break;

        case '{}':
            $wt = "{{ta}} LIKE '%{$ve}%'";
            break;
        case '!{}':
            $wt = "{{ta}} NOT LIKE '%{$ve}%' OR {{ta}} IS NULL";            
            break;

        case '()': 
            $va = preg_split('|\s*,\s*|', $ve);
            if (!$m) {
                $wt = "{{ta}} IN ('".join("','", $va)."')";
            } else {
                $w1 = array();
                foreach ($va as $v1) {
                    $w1[] = "find_in_set('".addslashes($v1)."', {{ta}})";
                }
                $wt = '('.join(') OR (', $w1).')';
            }
            break;
        
       case '!()':
            $va = preg_split('|\s*,\s*|', $ve);
            if (!$m) {
                $wt = "{{ta}} NOT IN ('".join("','", $va)."')";
            } else {
                $w1 = array();
                foreach ($va as $v1) {
                    $w1[] = "find_in_set('".addslashes($v1)."', {{ta}})";
                }
                $wt = '('.join(') OR (', $w1).')';
            }
            $wt = "({$wt}) OR {{ta}} IS NULL";
            
            break;

        default:
            return false;
        }
        
        $w = str_replace('{{ta}}', $attributeAlias, $wt);
        
        return $w;
    }

    public function getStoreId() {
        
        if (!$this->_store) {
            $profile = Mage::registry( 'nscexport_profile');
            if( $profile) {
                $this->_store = $profile->getStoreId();
            } else {
                $this->_store = Mage::registry('nscexport_store');
            }
        }
        return $this->_store;
    }
    
    public function getFeedCode() {
    
    	if (!$this->_feedCode) {
    		$this->_feedCode = Mage::registry( 'nsc_current_feed_code');
    	}
    	return $this->_feedCode;
    }
}