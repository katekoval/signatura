<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

class Nostress_Nscexport_Model_Schedule_Code
{
	const CRONTAB_JOB_CODE = 'nostress_koongo_connector';
	const CRONTAB_JOB_NAME = 'Koongo Connector';
	
	protected $data = array();

	/**
	 *
	 * @return array
	 */
	public function get()
	{
		if(count($this->data) == 0) {
			$this->_init();
            natcasesort($this->data);
		}

		return $this->data;
	}

	/**
	 *
	 */
	protected function _init()
	{
		$this->data[self::CRONTAB_JOB_CODE] = $this->helper()->__(self::CRONTAB_JOB_NAME);
	}

	/**
	 *
	 * @return Varien_Data_Form_Element_Select 
	 */
	public function toFormElementSelect()
	{
		$data = $this->getProcessesCodes();
        natcasesort($data);
		array_unshift($data, '');
		$selectType = new Varien_Data_Form_Element_Select();
		$selectType->setName('job_code')
				->setId('job_code')
				->setForm(new Varien_Data_Form())
				->addClass('required-entry')
				->setValues($data);
		return $selectType;
	}
	
	public function getProcessesCodes()
    {
    	return $this->get();	
    }	
//		foreach($jobCodes as $key => $code)
//		{
//			
//			if(strpos($code,'nscexport_') === FALSE)
//				unset($jobCodes[$key]);
//		}
//		
//		$result = array();
//		foreach($jobCodes as $key => $code)
//		{
//			//$result[str_replace("nscexport_nscexport","",$key)] = str_replace("nscexport_nscexport","",$code);
//			
//			$engine = $this->getEngineConfig(str_replace("nscexport_nscexport","",$code),null,true);
//			$result[$key] = $engine[self::PARAM_NAME].$engine['suffix'];
//		}
//		return $result;

	/**
	 *
	 * @return Nostress_Nscexport_Helper_Data
	 */
	protected function helper()
	{
		return Mage::helper('nscexport');
	}

}