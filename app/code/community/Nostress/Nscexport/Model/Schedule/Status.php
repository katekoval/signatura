<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

class Nostress_Nscexport_Model_Schedule_Status
{
	protected $states = array();
	protected $statesShort = array();

	/**
	 * Retrieve all states
	 *
	 * @return array
	 */
	public function getStates()
	{
		if(count($this->states) == 0) {
			$this->_initStates();
		}

		return $this->states;
	}

    /**
	 * Retrieve all states for the history grid
	 *
	 * @return array
	 */
	public function getStatesHistory()
	{
        $states_history = array();
        // Translate State
		$states_history[Mage_Cron_Model_Schedule::STATUS_ERROR] = Mage::helper('nscexport')->__(Mage_Cron_Model_Schedule::STATUS_ERROR);
        $states_history[Mage_Cron_Model_Schedule::STATUS_MISSED] = Mage::helper('nscexport')->__(Mage_Cron_Model_Schedule::STATUS_MISSED);
        $states_history[Mage_Cron_Model_Schedule::STATUS_SUCCESS] = Mage::helper('nscexport')->__(Mage_Cron_Model_Schedule::STATUS_SUCCESS);

		return $states_history;
	}

/**
	 * Retrieve all states for the schedule grid
	 *
	 * @return array
	 */
	public function getStatesSchedule()
	{
        $states_schedule = array();
        // Translate State
		$states_schedule[Mage_Cron_Model_Schedule::STATUS_RUNNING] = Mage::helper('nscexport')->__(Mage_Cron_Model_Schedule::STATUS_RUNNING);
        $states_schedule[Mage_Cron_Model_Schedule::STATUS_MISSED] = Mage::helper('nscexport')->__(Mage_Cron_Model_Schedule::STATUS_MISSED);
        $states_schedule[Mage_Cron_Model_Schedule::STATUS_PENDING] = Mage::helper('nscexport')->__(Mage_Cron_Model_Schedule::STATUS_PENDING);

		return $states_schedule;
	}


	/**
	 * Retrieve all states with short Text
	 *
	 * @return array
	 */
	public function getStatesShort()
	{
		if(count($this->statesShort) == 0) {
			$this->_initStates();
		}

		return $this->statesShort;
	}

	/**
	 *
	 */
	protected function _initStates()
	{
		$this->_addState(Mage_Cron_Model_Schedule::STATUS_ERROR);
		$this->_addState(Mage_Cron_Model_Schedule::STATUS_MISSED);
		$this->_addState(Mage_Cron_Model_Schedule::STATUS_PENDING);
		$this->_addState(Mage_Cron_Model_Schedule::STATUS_RUNNING);
		$this->_addState(Mage_Cron_Model_Schedule::STATUS_SUCCESS);
	}

	/**
	 *
	 * @param string $state
	 */
	protected function _addState($state)
	{
		// Translate State
		$this->states[$state] = Mage::helper('nscexport')->__($state);

		// Translate Short State Identifier
		$this->statesShort[$state] = Mage::helper('nscexport')->__('short_' . $state);
	}

	/**
	 *
	 * @return Varien_Data_Form_Element_Select 
	 */
	public function toFormElementSelect()
	{
		$data = $this->getStates();
		array_unshift($data, '');
		$selectType = new Varien_Data_Form_Element_Select();
        $selectType->setName('status')
            ->setId('status')
            ->setForm(new Varien_Data_Form())
            ->addClass('required-entry')
            ->setValues($data);
		return $selectType;
	}

}