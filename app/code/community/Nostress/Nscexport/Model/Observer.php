<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/** 
 * Observer for Export
 * 
 * @category Nostress 
 * @package Nostress_Nscexport
 * 
 */

class Nostress_Nscexport_Model_Observer extends Nostress_Nscexport_Model_Abstract
{	
	const ADD_PRODUCT_TO_PROFILE_FLAG = "nostress_add_product_to_profile";
	
	protected $_rulePrices = array();
	protected $_srcModel;

	
	protected function getSourceModel()
	{
		if(!isset($this->_srcModel))
			$this->_srcModel = Mage::getModel('nscexport/profile');
		return $this->_srcModel;
	}
	
	/**
     * Handles a custom AfterSave event for Catalog Products
     * Determines if the product is new.
     * Calls function for adding product to proper profile.
     *
     * @param array $eventArgs
     */
    public function processCatalogProductAfterSaveEvent($eventArgs)
    {    	
		//Pull the product out of the EventArgs parameter
        $product = $eventArgs['data_object'];     
        $flag = Mage::registry(self::ADD_PRODUCT_TO_PROFILE_FLAG);   
        if(isset($flag) && $flag == true)
        {
        	Mage::unregister(self::ADD_PRODUCT_TO_PROFILE_FLAG);
         	Mage::getModel('nscexport/categoryproducts')->updateProductAssignment($product);	
        }
   	}
   	
   	public function setNewProductSaveFlag($eventArgs)
   	{
//    		Add only brand new products
//    	 	$product = $eventArgs['data_object'];
//    	 	$productIsNew = $product->isObjectNew();
   	 	
//        	if($productIsNew)
//         {
         
//         }
        	$regValue = Mage::registry(self::ADD_PRODUCT_TO_PROFILE_FLAG);
        	if(isset($regValue))
    			Mage::unregister(self::ADD_PRODUCT_TO_PROFILE_FLAG);
         	Mage::register(self::ADD_PRODUCT_TO_PROFILE_FLAG,true);
       
   	}   
    
	/**
     * Generate Koongo Connector scheduled profiles
     *
     * @param Mage_Cron_Model_Schedule $schedule
     */
    public function generateFeeds($schedule)
    {   
    	$profileIds = Mage::getModel('nscexport/cron')->getScheduledProfiles();
    	if(empty($profileIds))
    		return;
    	
    	$this->helper()->runProfilesByIds($profileIds);  			       		 	    		    	                                              
    }
    
    public function updatePluginInfo($schedule)
    {
        Mage::helper('nscexport/data_client')->updatePlugins();
        Mage::helper('nscexport/data_client')->updateLicense();
    }
    
    protected function helper()
    {
    	return Mage::helper('nscexport/data_profile');
    }
}