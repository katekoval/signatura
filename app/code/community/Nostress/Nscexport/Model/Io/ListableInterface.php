<?php
interface Nostress_Nscexport_Model_Io_ListableInterface
{
    
    const DIR = 'dir';
    const FILE = 'file';
    
    public function getItems();
}
