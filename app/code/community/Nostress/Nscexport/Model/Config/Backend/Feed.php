<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* Nscexports model
* 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

class Nostress_Nscexport_Model_Config_Backend_Feed extends Mage_Core_Model_Config_Data
{
	const LABEL = 'label';
	const VALUE = 'value';
	
    public function getValue()
    {
    	$feedOptions =  Mage::helper('nscexport/data_feed')->getOptions("link",false,true); 
   		$codes = array();
    	foreach($feedOptions as $feed)
		{
			$codes[] = $feed[self::VALUE];
		}
		return $codes;
    }
    
    protected function _beforeSave()
    {
    	$feedsStatusChanged = Mage::getModel('nscexport/feed')->updateFeedsEnabled(array_values(parent::getValue()));
    	$this->setValue(null);
    	    	
    	foreach ($feedsStatusChanged as $item)
    	{
    		$taxonomyCode = $item->getTaxonomyCode();
    		if(isset($taxonomyCode))
    		{	
    			//just modify attributes
    			Mage::getModel('nscexport/entity_attribute_taxonomy')->prepareAttributes();
        		//taxonomy reload is disabled in this phase
    			//$message = Mage::getModel('nscexport/taxonomy')->reloadTaxonomy();        		
        		Mage::getSingleton('core/session')->addSuccess(Mage::helper('nscexport')->__("Taxonomy successfully reloaded."));        		
        		break;
    		}
    	}    	
    	
		return $this;
    }
}
?>