<?php 
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* Exports model - source for dropdown menu "Product group size"
* 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

class Nostress_Nscexport_Model_Config_Source_Productgroupsize
{
    public function toOptionArray()
    {
        return array(
/*            array('value'=>'1', 'label'=>Mage::helper('nscexport')->__('1')),
            array('value'=>'5', 'label'=>Mage::helper('nscexport')->__('5')),
            array('value'=>'10', 'label'=>Mage::helper('nscexport')->__('10')),
            array('value'=>'30', 'label'=>Mage::helper('nscexport')->__('30')),
            array('value'=>'50', 'label'=>Mage::helper('nscexport')->__('50')),*/
            array('value'=>'100', 'label'=>Mage::helper('nscexport')->__('100')),
            array('value'=>'300', 'label'=>Mage::helper('nscexport')->__('300')),
            array('value'=>'500', 'label'=>Mage::helper('nscexport')->__('500')),  
            array('value'=>'1000', 'label'=>Mage::helper('nscexport')->__('1000')),
            array('value'=>'2000', 'label'=>Mage::helper('nscexport')->__('2000')),
            array('value'=>'5000', 'label'=>Mage::helper('nscexport')->__('5000')),
            array('value'=>'10000', 'label'=>Mage::helper('nscexport')->__('10000')), 
        	array('value'=>'20000', 'label'=>Mage::helper('nscexport')->__('20000')),
//         	array('value'=>'30000', 'label'=>Mage::helper('nscexport')->__('30000')),
//         	array('value'=>'40000', 'label'=>Mage::helper('nscexport')->__('40000')),
//         	array('value'=>'50000', 'label'=>Mage::helper('nscexport')->__('50000')),
        );
    }
}
?>
