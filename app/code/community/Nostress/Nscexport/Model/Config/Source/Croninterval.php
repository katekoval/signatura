<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
* Exports model - source for dropdown menu "Product group size"
*
* @category Nostress
* @package Nostress_Nscexport
*
*/

class Nostress_Nscexport_Model_Config_Source_Croninterval
{
	const DEFAULT_INTERVAL = 15;
	
    public function toOptionArray()
    {
        $options = array(
            array( 'value'=>15, 'label'=> '15 minutes'),
            array( 'value'=>30, 'label'=> '30 minutes'),
            array( 'value'=>60, 'label'=> '1 hour'),
            array( 'value'=>120, 'label'=> '2 hours'),
            array( 'value'=>240, 'label'=> '4 hours'),
        );
        return $options;
    }
    
    public function getDefaultValue()
    {
    	return self::DEFAULT_INTERVAL;
    }
}
?>