<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
 * Used in creating options Export parent product attribute
 *
 */
class Nostress_Nscexport_Model_Config_Source_Parentattribute
{
    const YES = 1;
    const NO = 0;
    const IF_EMPTY = 2;
    
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => self::YES, 'label'=>Mage::helper('adminhtml')->__('Yes')),
            array('value' => self::NO, 'label'=>Mage::helper('adminhtml')->__('No')),
            array('value' => self::IF_EMPTY, 'label'=>Mage::helper('nscexport')->__('If empty'))
        );
    }

}
