<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
* Exports model - condtions disabled attributes
*
* @category Nostress
* @package Nostress_Nscexport
*
*/

class Nostress_Nscexport_Model_Config_Source_Conditionsdisabledattributes
{
	const LABEL = 'label';
	const VALUE = 'value';
	
	protected $_options;
	
	public function toOptionArray() {
	    
	    if( !$this->_options) {
    	    try
    	    {
		    	$defaultStoreId = current(array_keys( Mage::app()->getStores()));
	    
	    	    $attributes = array();
	            $attributesValues = Mage::helper('nscexport/data_feed')->getAttributeOptions( $defaultStoreId);
	            unset( $attributesValues[0]);
	            $this->_options = $attributesValues;
    	    }
    	    catch (Exception $e)
    	    {
    	    	$helper = Mage::helper('nscexport');
    	    	$error = $helper->getErrorByCode($e->getMessage());
    	    	
    	    	$message = $helper->__('Options for field "Disabled Attributes for Attribute Filters" can\'t be loaded.');
    	    	if($error)
    	    	{
    	    		$message .= " ".$error['message'];
    	    		$message .= " ".$helper->__('More information you may find in <a href="%s" target="blank" >documentation</a>.',$error['link']); 
    	    	}
    	    	
    	    	Mage::getSingleton('adminhtml/session')->addError($message);
    	    	return array();
    	    }
	    }
	    
		return $this->_options;
	}
}
?>