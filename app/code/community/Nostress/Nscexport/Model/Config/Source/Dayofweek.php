<?php 
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* Exports model - source for dropdown menu "Product group size"
* 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

class Nostress_Nscexport_Model_Config_Source_Dayofweek
{        
	const MONDAY = 1;
	const TUESDAY = 2;
	const WEDNESDAY = 3;
	const THURSDAY = 4;
	const FRIDAY = 5;
	const SATURDAY = 6;
	const SUNDAY = 7;
	
    public function toOptionArray()
    {
        return array(
            array('value' => self::MONDAY, 'label' => Mage::helper('nscexport')->__('Monday')),
            array('value' => self::TUESDAY, 'label' => Mage::helper('nscexport')->__('Tuesday')),
            array('value' => self::WEDNESDAY, 'label' => Mage::helper('nscexport')->__('Wednesday')),
            array('value' => self::THURSDAY, 'label' => Mage::helper('nscexport')->__('Thursday')),
            array('value' => self::FRIDAY, 'label' => Mage::helper('nscexport')->__('Friday')),
            array('value' => self::SATURDAY, 'label' => Mage::helper('nscexport')->__('Saturday')),
            array('value' => self::SUNDAY, 'label' => Mage::helper('nscexport')->__('Sunday'))                             
        );
    }
    
    public function getAllValues()
    {
    	$options = $this->toOptionArray();
    	$result = array();
    	foreach ($options as $option)
    	{
    		$result[] = $option["value"];
    	}
    	return $result;
    }
}
?>