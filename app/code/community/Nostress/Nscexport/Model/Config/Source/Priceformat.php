<?php 
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* Exports model - source for dropdown menu "Product group size"
* 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

class Nostress_Nscexport_Model_Config_Source_Priceformat
{
    const STANTDARD = "standard";
    const CURRENCY_SUFFIX = "currency_suffix";
    const CURRENCY_PREFIX = "currency_prefix";
    const SYMBOL_SUFFIX = "symbol_suffix";
    const SYMBOL_PREFIX = "symbol_prefix";
        
    public function toOptionArray()
    {
        return array(
            array('value'=> self::STANTDARD, 'label'=>Mage::helper('nscexport')->__('Standard e.g. 149.99')),
            array('value'=>self::CURRENCY_SUFFIX, 'label'=>Mage::helper('nscexport')->__('Currency suffix e.g. 149.99 USD')),
            array('value'=>self::CURRENCY_PREFIX, 'label'=>Mage::helper('nscexport')->__('Currency prefix e.g. USD 149.99')),
            array('value'=>self::SYMBOL_SUFFIX, 'label'=>Mage::helper('nscexport')->__('Symbol suffix e.g 149.99 US$')),
            array('value'=>self::SYMBOL_PREFIX, 'label'=>Mage::helper('nscexport')->__('Symbol prefix e.g US$ 149.99'))                                  
        );
    }
}
?>