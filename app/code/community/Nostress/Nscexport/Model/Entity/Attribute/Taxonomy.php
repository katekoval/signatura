<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/** 
 * Model for search engines taxonomy
 * 
 * @category Nostress 
 * @package Nostress_Nscexport
 * 
 */

class Nostress_Nscexport_Model_Entity_Attribute_Taxonomy extends Nostress_Nscexport_Model_Abstract 
{    
	const ATTRIBUTE_CODE = 'nsc_engine_taxonomy';
	const GROUP_NAME = 'Koongo Connector';
	const SOURCE_MODEL = 'nscexport/entity_attribute_source_taxonomy';
	const ENGINE_NAME_SUFFIX = ' Taxonomy';
	 
	const FIELD_CODE = 'code';
	const FIELD_NAME = 'name';
	const FIELD_TYPE = 'type';
	const FIELD_TITLE = 'title';
	
	const NULL_GROUP_ID = 0;
	
	protected $_setup;
	protected $_entityTypeId;
	
	public function prepareAttributes()
	{
		$attributes = $this->getAttributes();
		$sets = $this->helper()->getAttributeSets(Nostress_Nscexport_Helper_Data::ENTITY_CATEGORY);
		$entityTypeId = $this->getEntityTypeId();
		foreach ($sets as $set)
		{
			$setId = $set->getId();
			$groupId = $this->addAttributeGroup($set->getId(),self::GROUP_NAME);
			$this->addAttributes($attributes,$entityTypeId,$setId,$groupId);			
		}
		$this->closeSetup();
	}
	
	protected function addAttributeGroup($setId, $name, $sortOrder = null)
	{
		$setup = $this->getSetup();
		$setup->addAttributeGroup($this->getEntityTypeId(), $setId, $name, $sortOrder);
		$groupId = $setup->getAttributeGroup($this->getEntityTypeId(), $setId, self::GROUP_NAME, 'attribute_group_id');
		return $groupId;
	}
	
	protected function addAttributes($attributes,$entityTypeId,$attributeSetId,$attributeGroupId)
	{
		//original group Id
		$originalGroupId = $attributeGroupId;
		$enabled = Mage::getModel('nscexport/feed')->getEnabledTaxonomies();
		$used = Mage::getModel('nscexport/profile')-> getUsedTaxonomies();
		$taxonomies = array_intersect($enabled, $used);
		$existingAttributes = $this->getExistingTaxonomyAttributeCodes();
		
		foreach($attributes as $attribute)
		{			
			$code = $this->helper()->createCategoryAttributeCode($attribute->getData(self::FIELD_CODE));
			$taxonomyCode = $attribute->getCode();
			if(!in_array($taxonomyCode,$taxonomies))
			{
				$attributeGroupId = self::NULL_GROUP_ID;
				if(!in_array($code, $existingAttributes))
					continue;
			}
			else 
				$attributeGroupId = $originalGroupId;
							
			$title = $attribute->getData(self::FIELD_TITLE);
			$this->addAttribute($title,$code,$entityTypeId,$attributeSetId,$attributeGroupId);
		}
	}
	
	protected function addAttribute($name,$code,$entityTypeId,$attributeSetId,$attributeGroupId)
	{
		$setup = $this->getSetup();
		
		$setup->addAttribute('catalog_category', $code,  array(
							    'type'     => 'text',
							    'label'    => $name,
							    'input'    => 'select',
							    'global'   => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
							    'visible'           => true,
							    'required'          => false,
							    'user_defined'      => false,
							    'default'           => "",
								'source'      =>  self::SOURCE_MODEL
							));
 
		$setup->addAttributeToGroup(
		    $entityTypeId,
		    $attributeSetId,
		    $attributeGroupId,
		    $code,
		    '0'                    //position
		);
	}
	
	protected function getSetup()
	{
		if(!isset($this->_setup))		
		{
			$this->_setup = Mage::getModel('eav/entity_setup','eav_setup');
			$this->_setup->startSetup();
		}
		return $this->_setup;
	}
	
	protected function closeSetup()
	{
		$setup = $this->getSetup();
		$setup->cleanCache();
		$setup->endSetup();
	}
	
	protected function getEntityTypeId()
	{
		if(!isset($this->_entityTypeId))
		{	
			$entity = $this->helper()->getEntityType(Nostress_Nscexport_Helper_Data::ENTITY_CATEGORY);
			$this->_entityTypeId = $entity->getId();
		}
		return $this->_entityTypeId;
	}
    
	protected function getAttributes()
	{
		$attributes = Mage::getModel('nscexport/taxonomy_setup')->getCollection();
		$attributes->addFieldToSelect(self::FIELD_CODE);
		$attributes->addFieldToSelect(self::FIELD_NAME);
		$attributes->addFieldToSelect(self::FIELD_TYPE);
		$attributes->getSelect();
		$attributes->load();
		$items = $attributes->getItems();
		foreach($items as $key => $item)
		{
			$type = $item->getType();
			$title = $item->getName();
			if(!empty($type))
				$title .= " - ".$type;
			$items[$key]->setTitle($title);
		}
		return $items;
	}    
	
	protected function getExistingTaxonomyAttributeCodes()
	{
		$collection = $this->getExistingTaxonomyAttributes();
		$codes = array();
		foreach($collection as $item)
			$codes[] = $item->getAttributeCode();
		return $codes;
	}
	
	protected function getExistingTaxonomyAttributes()
	{
		$attribsTemplate = Nostress_Nscexport_Helper_Data::CATEGORY_ATTRIBUTE_PREFIX."%";
		$collection = Mage::getModel('eav/entity_attribute')->getCollection()->addFieldToFilter('attribute_code', array('like' => $attribsTemplate));
		return $collection;
	}
}