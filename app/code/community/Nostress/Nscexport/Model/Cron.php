<?php 
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

class Nostress_Nscexport_Model_Cron extends Nostress_Nscexport_Model_Abstract
{   	
	const TIME_MAX = "23:59:59";
	const TIME_MIN = "00:00:00";
	
	const TIME_FROM = 'time_from';
	const TIME_TO = 'time_to';
	const DAY_OF_WEEK = 'dow'; 
	
	const COLUMN_EXPORT_ID = 'export_id';
	const COLUMN_DAY_OF_WEEK = 'day_of_week';
	const COLUMN_TIME = 'time';
	
	public function _construct() 
	{
		parent::_construct ();
		$this->_init ('nscexport/cron');
	}
	
	public function getScheduledProfiles()
	{
		$intervals = $this->getScheduledIntervals();
		
		$collection = $this->getCollection();
		$select = $collection->getSelect();
		
		$whereSql = "";
		foreach ($intervals as $interval)
		{
			if(!empty($whereSql))
				$whereSql .= "OR ";
			$whereSql .= "(time >= '{$interval[self::TIME_FROM]}' AND time <= '{$interval[self::TIME_TO]}' AND day_of_week = '{$interval[self::DAY_OF_WEEK]}') ";
		}
		$select->where($whereSql);
		$select->group("export_id");
		
//		echo $select->__toString();
//		exit();
		
		$collection->load();
		$profileIds = array();
		foreach ($collection as $record)
		{
			$profileIds[] = $record->getExportId();
		}
		return $profileIds;
	}
	
	public function getDaysPerProfile($exportId)
	{
		return $this->getColumPerProfile($exportId,"day_of_week");
	}
	
	public function getTimesPerProfile($exportId)
	{
		$times = $this->getColumPerProfile($exportId,"time");
		foreach($times as $index => $time)
		{
			$times[$index] = substr($time,0,strrpos($time,":"));			
		}		
		return $times;
	}
	
	public function deleteRecords($exportId,$days,$times)
	{
		$records = $this->prepereRecords($exportId,$days,$times);
		$this->getResource()->deleteRecords($records);
	}
	
	public function addRecords($exportId,$days,$times)
	{
		$records = $this->prepereRecords($exportId,$days,$times);
		$this->getResource()->insertRecords($records);
	}
	
	protected function getColumPerProfile($exportId,$columnName)
	{
		$collection = $this->getCollection();
		$collection->addFieldToFilter("export_id",$exportId);
		$collection->addFieldToSelect($columnName);
		$collection->getSelect()->group($columnName);
		$collection->load();
		
		$result = array();
		foreach ($collection as $item) 
		{
			$result[] = $item->getData($columnName);			
		}
		return $result;
	}
	
	protected function prepereRecords($exportId,$days,$times)
	{
		$records = array();
		foreach ($days as $day)
		{
			foreach ($times as $time)
			{
				$records[] = array(self::COLUMN_EXPORT_ID => $exportId,self::COLUMN_DAY_OF_WEEK => $day,self::COLUMN_TIME => $time);
			}
		}
		return $records;
	}
	
	/**
	 * Prepare interval(s) from last cron run. 
	 */
	protected function getScheduledIntervals()
	{
		$currentDateTime = $this->helper()->getDateTime(null,true);
		$dayOfWeek = $this->helper()->getDayOfWeek($currentDateTime);
		
		$timeFormated = $this->helper()->getTime(null,true);
		$time = $this->helper()->getTime();
		//$lastRunTimeFormated = "23:10:59";
		$lastRunTimeFormated = $this->helper()->getGeneralConfig(Nostress_Nscexport_Helper_Data::PARAM_CRON_LAST_RUN);
		
		$this->helper()->setGeneralConfig(Nostress_Nscexport_Helper_Data::PARAM_CRON_LAST_RUN,$timeFormated);		         
		$lastRunTime = strtotime($lastRunTimeFormated);
		$lastRunDayOfWeek = $dayOfWeek;
		
		if($lastRunTime > $time)
		{
			$lastRunDayOfWeek--;
			if($lastRunDayOfWeek < Nostress_Nscexport_Model_Config_Source_Dayofweek::MONDAY)
				$lastRunDayOfWeek = Nostress_Nscexport_Model_Config_Source_Dayofweek::SUNDAY;
		}
		
		$intervals = array();
		if($lastRunDayOfWeek == $dayOfWeek)
		{
			$intervals[] = array(self::TIME_FROM => $lastRunTimeFormated,self::TIME_TO => $timeFormated,self::DAY_OF_WEEK => $dayOfWeek);
		}
		else 
		{
			$intervals[] = array(self::TIME_FROM => $lastRunTimeFormated,self::TIME_TO => self::TIME_MAX,self::DAY_OF_WEEK => $lastRunDayOfWeek);
			$intervals[] = array(self::TIME_FROM => self::TIME_MIN,self::TIME_TO => $timeFormated,self::DAY_OF_WEEK => $dayOfWeek);
		}					
		return $intervals;
	}
}
?>