<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
 *
 * @category Nostress
 * @package Nostress_Nscexport
 *
 */

class Nostress_Nscexport_Model_Export_Api_Adapter_Beslist extends Nostress_Nscexport_Model_Export_Api_Adapter_Abstract
{
    const API_FILE_TYPE = '_BESLIST_SHOPITEM_API_';
    
    protected $_requiredFields = array( 'api_key', 'shop_id');
    
    protected $_code = 'beslist';
    
    protected $_label = 'Beslist API';
    
    protected $_itemable = true;
    
    /**
     * @var Wienkit\BeslistShopitemClient\BeslistShopitemClient
     */
    protected $_client = null;
    
    protected function _authenticate( $config, $next = true) {
        
        try {
            $client = $this->getClient( $config);
            $sites = $client->getSites();
            
            return $sites;
        } catch( Exception $e) {
            if( $next && $e->getCode() == 401) {
                return false;
            } else {
                throw $e;
            }
        }
    }
    
    public function checkConnection( array $config) {
        
        try {
            $sites = $this->_authenticate($config);
            
            // next try with another mode
            if( $sites === false) {
                $config['test_mode'] = !$config['test_mode'];
                $sites = $this->_authenticate($config, false);
            }
        
            if( !isset( $sites[ $config['shop_id']])) {
                $currentShopId = key($sites);
            } else {
                $currentShopId = $config['shop_id'];
            }
            
            return array( 'error'=>false, 'message'=>$this->helper()->__('Connection Successful!'), 'test_mode' => intval($config['test_mode']), 'shop_id'=>$currentShopId, 'sites'=> $sites);
        } catch (Exception $e) {
            return array( 'error'=>true, 'message'=>$this->helper()->__('Error: '.$e->getMessage()."!"));
        }
    }
    
    /**
     * @param array $params
     * @return \Wienkit\BeslistShopitemClient\BeslistShopitemClient
     */
    public function getClient( $config) {
        
        $client = new Wienkit\BeslistShopitemClient\BeslistShopitemClient( $config['api_key']);
        $client->setTestMode($config['test_mode']);
        
        return $client;
    }
    
    public function uploadFlatFile() {
            
        $profile = $this->getProfile();
        $profileId = $profile->getId();
        $config = $profile->getExportApiConfig( $this->_code);
        
        if( !$this->isFilled( $config)) {
            throw new Zend_Exception( 'BESLIST API config is not complete! Check your config by test connection feature.', Nostress_Nscexport_Model_Profile::CODE_NOT_ENABLED);
        }
        $client = $this->getClient($config);
        
        $shopId = $config['shop_id'];
        
        // get old data
        $itemModel = Mage::getModel( 'nscexport/submission_items');
        $itemRes = $itemModel->getResource();
        $submissionItems = $itemModel->getCollection()->getProducts( $profile->getId());
        
        // get actual data from preview
        $preview = $profile->getPreview( false);
        
        // choose what is new and what is updated
        $insertData = array();
        $updateData = array();
	    foreach( $preview as $id => $row) {
	        
	        $this->_formatItem( $row);
	        
	        // is ID already submitted in past?
	        if( isset( $submissionItems[ $id])) {
	            $oldItem = $submissionItems[ $id];
	            $oldStatus = $oldItem['status'];
	            $this->_formatItem( $oldItem);
	            
	            // is there some change or error?
	            // or stuck in processing status
	            if( $row != $oldItem || $oldStatus == Nostress_Nscexport_Model_Submission_Items::STATUS_ERROR || $oldStatus == Nostress_Nscexport_Model_Submission_Items::STATUS_PROCESSING) {
	                $updateData[] = $row;
	            }
	        // new item
	        } else {
	            $insertData[] = $row;
	        }
	    }
	    	    	    	    
	    // we are sending new and updated data together
	    $data = array_merge( $insertData, $updateData);
	    
	    // nothing to submit, all data are updated already
	    if( !count( $data)) {
	       return false;
	    }
	    
	    // send data to beslist
        $response = $client->updateShopItems($shopId, $data);
                        
        // get batchID from url
        $url = trim( $response['url']);
    	$batchId = substr( $url, strrpos( $url, '/')+1);
    	
    	// add status to data
    	foreach( $insertData as &$row) {
    	    $row['status'] = Nostress_Nscexport_Model_Submission_Items::STATUS_PROCESSING;
    	}
    	unset( $row);
    	foreach( $updateData as &$row) {
    	    $row['status'] = Nostress_Nscexport_Model_Submission_Items::STATUS_PROCESSING;
    	}
    	unset( $row);
    	
    	// now data are correctly sended and we can update history
    	$itemRes->insertItems( $profileId, $insertData);
    	$itemRes->updateItems( $profileId, $updateData);
    	
        $response = $client->getBatchResult( $shopId, $batchId);
        
        $profileSubmit = $this->_saveBatchResponse($profileId, $shopId, $response);
            
        return $profileSubmit->toArray();
    }
    
    protected function _formatItem( &$item) {
         
        $allowedCols = array( 'externalId', 'price', 'discount_price', 'stock', 'delivery_cost_nl', 'delivery_cost_be');
        foreach( $item as $key => &$value) {
            if( !in_array( $key, $allowedCols)) {
                unset( $item[$key]);
            }
        }
        unset( $value);
    }
       
    public function getSubmissionList( array $params) {
    
        try {
            $profile = $this->getProfile();
            $profileId = $profile->getId();
        
            if( isset( $params[ Nostress_Nscexport_Model_Profile::EXPORT_API])) {
                $config = $params[ Nostress_Nscexport_Model_Profile::EXPORT_API];
            } else {
                $config = $profile->getExportApiConfig( $this->_code);
            }
        
            if( !$this->isFilled( $config)) {
                throw new Zend_Exception( $this->getLabel().' config is not complete! Check your config by test connection feature.', Nostress_Nscexport_Model_Profile::CODE_NOT_ENABLED);
            }
        
            $client = $this->getClient($config);
            
            // check connection
            $result = $client->authenticate();
            $shopId = $config['shop_id'];
        
            // shopId site
            $site = $result->sites[ $shopId];
            
            $profileSubmitCollection = $profile->getSubmitCollection();
            
            foreach( $profileSubmitCollection as $profileSubmit) {
                if( $profileSubmit->isProcessing()) {
                    $batchId = $profileSubmit->getSubmissionId();
                    
                    try {
                        $response = $client->getBatchResult( $shopId, $batchId);
                    } catch( Exception $e) {
                        if( $e->getCode() == '404') {
                            $response = array( 'batch' => $batchId, 'status' => $e->getMessage());
                        } else {
                            throw $e;
                        }
                    }
                    
                    $this->_saveBatchResponse($profileId, $shopId, $response);
                }
            }
            
            // clear old reports
            $profileSubmitCollection = $profile->getSubmitCollection();
            $limit = 30;
            $index = 0;
            foreach( $profileSubmitCollection as $profileSubmit) {
                // remove old submission
                if( $index++ >= $limit) {
                    $profileSubmitCollection->removeItemByKey( $profileSubmit->getId());
                    $profileSubmit->delete();
                }
            }
        
            return array( 'error'=>false, 'list'=> $profileSubmitCollection, 'site'=>$site);
        } catch( Exception $e) {
        
            if( $e->getCode() == Nostress_Nscexport_Model_Profile::CODE_NOT_ENABLED) {
                return array( 'error'=>true, 'message'=> $e->getMessage());
            } else {
                return array( 'error'=>true, 'message'=>$this->helper()->__('Error: '.$e->getMessage()."!"));
            }
        }
    }

    public function initFormElements( Varien_Data_Form_Element_Fieldset $fieldset, array $config = array()) {
        
        $fieldset->addField('api_key', 'text', array(
            'label' => Mage::helper('nscexport')->__("Api Key:"),
            'name' => "api_key",
            'note' => Mage::helper('nscexport')->__('Contact <a href="mailto:cm.shopitemapi@beslist.nl">cm.shopitemapi@beslist.nl</a> and request an Shopitem API key for test and production environment. <a href="#" onclick="javascript:showTab( \'submission_tabs_manual\');">See Beslist submission manual.</a>')
        ));
        
        $yesnoSource = Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray();
        $fieldset->addField('test_mode', 'select', array(
            'label' => Mage::helper('nscexport')->__("Test Mode:"),
            'name' => 'test_mode',
            'values' => $yesnoSource,
            'value' => 1,
            'note' => Mage::helper('nscexport')->__('Set to Yes if test API key is used. Otherwise set to No, if production API key is inserted.')
        ));
        
        $urlTestExportApiConnection = Mage::getUrl("*/*/testExportApiConnection");
        $fieldset->addField('test_connection', 'nostress_button', array(
            'label' => Mage::helper('nscexport')->__("Test Connection"),
            'name' => 'test_connection',
            'class' => 'save',
            'onclick' => "testExportApiConnection('$urlTestExportApiConnection');"
        ));
        
        $element = $fieldset->addField('shop_id', 'select', array(
            'label' => Mage::helper('nscexport')->__("Shop ID:"),
            'name' => "shop_id",
            'class' => 'step-2'
        ));
        
        if( !empty( $config['api_key']) && !empty( $config['test_mode'])) {
            try {
                $client = $this->getClient($config);
                $sites = $client->getSites();
                
                foreach( $sites as $id => &$label) {
                    $label = $id." (".$label.")";
                }
                
                $element->setValues( $sites);
                $element->setDisabled( null);
                
            } catch( Exception $e) {
                // nothing to do. Wrong settings
            }
        }
    }
    
    protected function _saveBatchResponse( $profileId, $shopId, array $response) {
        
        $itemModel = Mage::getModel( 'nscexport/submission_items');
        $itemRes = $itemModel->getResource();
        $submissionItems = $itemModel->getCollection()->getProducts( $profileId, array( 'status'=>Nostress_Nscexport_Model_Submission_Items::STATUS_PROCESSING));
        
        // update not finnished items
        $updateItems = array();
        $errors = 0;
        if( isset( $response['items']) && count( $response['items'])) {
            foreach( $response['items'] as $item) {
                
                $itemId = $item[ 'externalId'];
                
                if( $submissionItems[ $itemId]) {
                    
                    $sItem = $submissionItems[ $itemId];
                    $sItem['message'] = ""; // empty message for later use
                
                    $sItem['status'] = $item['response']['status'];
                    if( $sItem['status'] == Nostress_Nscexport_Model_Submission_Items::STATUS_ERROR) {
                        $sItem['message'] = isset($item['response']['message']) ? $item['response']['message'] : 'error not found';
                        $errors++;
                        // error for complete batch
                        $response['status'] = Nostress_Nscexport_Model_Submission_Items::STATUS_ERROR;
                    }
                    $updateItems[] = $sItem;
                }
            }
            
            if( count( $updateItems)) {
                
                $itemRes->updateItems( $profileId, $updateItems);
            }
        }
        
        
        // save batch result as submission report
        // batch result has data about updated items, but we dont care it. We have our system
        $profileSubmit = Mage::getModel( 'nscexport/submission_report');
        $profileSubmit->load( $response['batch'], 'submission_id');
        if( !$profileSubmit->getId()) {
            $profileSubmit
                ->setProfileId( $profileId)
                ->setMerchantId( $shopId)
                ->setSubmissionId( $response['batch'])
                ->setSubmitDate( new Zend_Db_Expr( 'now()'))
                ;
        }
        $profileSubmit
            ->setStatus( $response['status'])
            ->setRepportMessage( json_encode( array(
                    'count' => $response['count'],
                    'processed' => $response['processed'],
                    'errors' => $errors
            )))
            ->save()
            ;
            
       return $profileSubmit;
    }

    public function getHelpId() {
    
        return 'export_api_beslist';
    }
}