<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
 *
 * @category Nostress
 * @package Nostress_Nscexport
 *
 */

abstract class Nostress_Nscexport_Model_Export_Api_Adapter_Abstract extends Nostress_Nscexport_Model_Abstract
{
    /**
     * @var Nostress_Nscexport_Model_Profile
     */
    protected $_profile = null;
    
    protected $_requiredFields = array();
    
    protected $_code = null;
    
    protected $_label = null;
    
    protected $_itemable = false;
    
    public function isItemable() {
        return $this->_itemable;
    }
    
    public function getRequiredFields() {
        return $this->_requiredFields;
    }
    
    public function isFilled( array $config) {
        
        foreach( $this->getRequiredFields() as $field) {
            if( empty( $config[ $field])) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * @param Nostress_Nscexport_Model_Profile $profile
     * @return Nostress_Nscexport_Model_Export_Api_Adapter_Abstract
     */
    public function setProfile( Nostress_Nscexport_Model_Profile $profile) {
        $this->_profile = $profile;
        return $this;
    }
    
    /**
     *
     * @return Nostress_Nscexport_Model_Profile
     */
    public function getProfile() {
        return $this->_profile;
    }

    abstract public function checkConnection( array $config);
    
    abstract public function uploadFlatFile();
    
    abstract public function initFormElements( Varien_Data_Form_Element_Fieldset $fieldset);
    
    public function getSubmissionList( array $params) {
        
        // not implemented
    }

    public function getSubmissionListLabel() {
        
        return $this->helper()->__( 'Submission List');
    }
    
    public function getLabel() {
        return $this->_label;
    }
    
    public function getCode() {
        return $this->_code;
    }
    
    public function getShortLabel() {
        
        if( $this->getCode() == Nostress_Nscexport_Model_Abstract::EXPORT_API_FTP) {
            $apiLabel = $this->getLabel();
        } else {
            $apiLabel = 'API';
        }
        return $apiLabel;
    }

    public function getHelpId() {
        
        return 'export_api';
    }
}