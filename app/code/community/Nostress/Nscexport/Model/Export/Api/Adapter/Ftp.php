<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
 *
 * @category Nostress
 * @package Nostress_Nscexport
 *
 */

class Nostress_Nscexport_Model_Export_Api_Adapter_Ftp extends Nostress_Nscexport_Model_Export_Api_Adapter_Abstract
{
    protected $_requiredFields = array( 'hostname', 'port', 'username', 'password');
    
    protected $_code = 'ftp';
    
    protected $_label = 'FTP';
    
    public function checkConnection( array $config) {
        
        $ftp = null;
        try {
            $ftp = $this->getClient($config);
            if( !$ftp->cd( $config['path'])) {
                throw new Zend_Exception( $this->helper()->__("Path '%s' does not exist", $config['path']), Nostress_Nscexport_Model_Profile::CODE_ERROR);
            }
            if( isset( $config['protocol']) && $config['protocol'] == Nostress_Nscexport_Model_Config_Source_Ftpprotocol::SFTP) {
                $ls = $ftp->rawls();
                if( isset( $ls['.']['permissions'])) {
                    $rights = decoct( $ls['.']['permissions']);
                    $attarray = array_reverse( preg_split("//",$rights));
                    $perms = array();
                    foreach( $attarray as $index => $value) {
                        if( $value != "") {
                            $perms[] = intval( $value);
                        }
                    }
                    if( !isset($perms[2]) || $perms[2] < 6) {
                        throw new Zend_Exception( $this->helper()->__("Check write permissions"), Nostress_Nscexport_Model_Profile::CODE_ERROR);
                    }
                    // empty dir is ok
                } elseif( !is_array( $ls) || count( $ls) != 0) {
                    throw new Zend_Exception( $this->helper()->__('FTP parser error'), Nostress_Nscexport_Model_Profile::CODE_ERROR);
                }
            } else {
                $ls = $ftp->lsDetailed();
                if( isset( $ls['.']['rights'])) {
                    $rights = $ls['.']['rights'];
                    $attarray = preg_split("//",$rights);
                    if( !isset($attarray[3]) || $attarray[3] != 'w') {
                        throw new Zend_Exception( $this->helper()->__("Check write permissions"), Nostress_Nscexport_Model_Profile::CODE_ERROR);
                    }
                    // empty dir is ok
                } elseif( !is_array( $ls)) {
                    Mage::log( $ls, Zend_Log::ERR,'koongo-ftp.log');
                    throw new Zend_Exception( $this->helper()->__('FTP parser error'), Nostress_Nscexport_Model_Profile::CODE_ERROR);
                }
            }
            $ftp->close();
            return array( 'error'=>false, 'message'=>$this->helper()->__('Connection Successful!'));
        } catch (Exception $e) {
            if( is_object( $ftp)) {
                $ftp->close();
            }
            return array( 'error'=>true, 'message'=>$this->helper()->__('Error: '.$e->getMessage()."!"));
        }
    }
    
    public function uploadFlatFile() {
            
        $profile = $this->getProfile();
        $config = $profile->getExportApiConfig( $this->_code);
        
        if( !$this->isFilled( $config)) {
            throw new Zend_Exception( 'FTP config is not complete! Check your config by test connection feature.', Nostress_Nscexport_Model_Profile::CODE_NOT_ENABLED);
        }
        
        $suffix = $profile->getFeedObject()->getFileType();
        $fullFilename = $profile->getFilename(true, $suffix,true);
        $filename = $profile->getFilename(false, $suffix,true);
        
        $fullpath = rtrim( $config['path'], '/').'/'.$filename;
    
        if( !is_file($fullFilename)) {
            throw new Zend_Exception( 'Feed file does not exists!', Nostress_Nscexport_Model_Profile::CODE_ERROR);
        }
    
        $ftp = $this->getClient($config);
        $result = $ftp->write($fullpath, file_get_contents($fullFilename));
    
        if( !$result) {
            $ftp->close();
            throw new Zend_Exception( 'Feed file can not be uploaded via FTP! Check permissions!', Nostress_Nscexport_Model_Profile::CODE_ERROR);
        }
    
        $ftp->close();
    
        return $result;
    }
    
    /**
     *
     * @param unknown $config
     * @return Nostress_Nscexport_Model_Io_Ftp
     */
    public function getClient( $config) {
        
        if( isset( $config['protocol']) && $config['protocol'] == Nostress_Nscexport_Model_Config_Source_Ftpprotocol::SFTP) {
            $ftp = new Nostress_Nscexport_Model_Io_Sftp();
            $ftpConfig = array(
                    'host'      => $config['hostname'].":".$config['port'],
                    'username'  => $config['username'],
                    'password'  => $config['password'],
            );
        } else {
            $ftp = new Nostress_Nscexport_Model_Io_Ftp();
            $ftpConfig = array(
                    'host'      => $config['hostname'],
                    'port' => $config['port'],
                    'user'  => $config['username'],
                    'password'  => $config['password'],
                    'passive' => (bool) $config['passive_mode']
            );
        }
        $ftp->open( $ftpConfig);
        
        return $ftp;
    }

    public function getSubmissionList( array $params) {
    
        try {
            $profile = $this->getProfile();
            
            if( isset( $params[ Nostress_Nscexport_Model_Profile::EXPORT_API])) {
                $config = $params[ Nostress_Nscexport_Model_Profile::EXPORT_API];
            } else {
                $config = $profile->getExportApiConfig( $this->_code);
            }
            
            if( empty($params['path'])) {
                $params['path'] = isset($config['path']) ? $config['path'] : "";
            }
    
            if( !$this->isFilled( $config)) {
                throw new Zend_Exception( 'Ftp config is not complete! Check your config by test connection feature.', Nostress_Nscexport_Model_Profile::CODE_NOT_ENABLED);
            }
            
            $adapter = $this->getClient($config);
            
            if( !empty($params['file'])) {
                
                $file = $params['file'];
                $content = $adapter->read( $file);
                $adapter->close();

                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="'.basename($file).'"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . strlen($content));
                header('Last-Modified: '. date('r'));
                echo $content;
                exit;
            }
            
            if( !empty( $params['path'])) {
                $adapter->cd( $params['path']);
            }
    
            $list = $this->_getFilesSorted( $adapter);
    
            return array( 'error'=>false, 'list'=> $list, 'path' => $adapter->pwd());
        } catch( Exception $e) {
            
            if( $e->getCode() == Nostress_Nscexport_Model_Profile::CODE_NOT_ENABLED) {
                return array( 'error'=>true, 'message'=> $e->getMessage());
            } else {
                return array( 'error'=>true, 'message'=>$this->helper()->__('Error: '.$e->getMessage()."!"));
            }
            
        }
    }

    protected function _getFilesSorted( $ftp) {
         
        $items = $ftp->getItems();
        if( !$items) {
            return false;
        }
        uksort($items, "strnatcasecmp");
        $dirs = array();
        $files = array();
        foreach( $items as $name => $item) {
            $item['name'] = $name;
            if( $item['type'] == Nostress_Nscexport_Model_Io_ListableInterface::DIR) {
                $dirs[] = $item;
            } else {
                $files[] = $item;
            }
        }
         
        return array_merge( $dirs,  $files);
    }

    public function getSubmissionListLabel() {
    
        return $this->helper()->__( 'Ftp Client');
    }

    public function initFormElements( Varien_Data_Form_Element_Fieldset $fieldset) {
    
        $yesnoSource = Mage::getModel('adminhtml/system_config_source_yesno')->toOptionArray();
	     
	    $fieldset->addField('protocol', 'select', array(
	            'label' => Mage::helper('nscexport')->__("Protocol:"),
	            'name' => "protocol",
	            'onchange' => "changeFtpProtocol(this);",
	            'values' => Mage::getModel('nscexport/config_source_ftpprotocol')->toOptionArray()
	    ));
	     
	    $fieldset->addField('hostname', 'text', array(
	            'label' => Mage::helper('nscexport')->__("Host Name:"),
	            'name' => "hostname",
	            'note' => 'e.g. "ftp.domain.com"'
	    ));
	     
	    $fieldset->addField('port', 'text', array(
	            'label' => Mage::helper('nscexport')->__("Port:"),
	            'name' => "port",
	            'value' => 21
	    ));
	     
	    $fieldset->addField('username', 'text', array(
	            'label' => Mage::helper('nscexport')->__("User Name:"),
	            'name' => "username",
	    ));
	     
	    $fieldset->addField('password', 'password', array(
	            'label' => Mage::helper('nscexport')->__("Password:"),
	            'name' => "password",
	    ));
	     
	    $fieldset->addField('path', 'text', array(
	            'label' => Mage::helper('nscexport')->__("Path:"),
	            'name' => "path",
	            'value' => '/',
	            'note' => 'e.g. "/yourfolder"'
	    ));
	     
	    $fieldset->addField('passive_mode', 'select', array(
	            'label' => Mage::helper('nscexport')->__("Passive Mode:"),
	            'name' => 'passive_mode',
	            'values' => $yesnoSource
	    ));
	    
	    $urlTestExportApiConnection = Mage::getUrl("*/*/testExportApiConnection");
	    $fieldset->addField('test_connection', 'nostress_button', array(
	            'label' => Mage::helper('nscexport')->__("Test Connection"),
	            'name' => 'test_connection',
	            'class' => 'save',
	            'onclick' => "testExportApiConnection('$urlTestExportApiConnection');"
	    ));
    }

    public function getHelpId() {
    
        return 'ftp_info';
    }
}