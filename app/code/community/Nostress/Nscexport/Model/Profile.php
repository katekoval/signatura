<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
 * Model for Export
 *
 * @category Nostress
 * @package Nostress_Nscexport
 *
 */

class Nostress_Nscexport_Model_Profile extends Nostress_Nscexport_Model_Abstract
{
	const START_TIME = 'start_time';
	const FILTER_FROM = 'from';
	const FILTER_TO = 'to';
	const FILTER_GTEQ = 'gteq'; //greaterOrEqualValue
	const FILTER_LTEQ = 'lteq';  //lessOrEqualValue

	const CONDITIONS = 'conditions';
	const VISIBILITY = 'visibility';
	const VISIBILITY_PARENT = 'visibility_parent';
	
	const CODE_NOT_ENABLED = 101;
	const CODE_ERROR = 301;

	const DEF_GROUP_BY_CATEGORY = "0";
	const DEF_RELOAD_CACHE = 1;

	const START_TIME_HOUR_INDEX = 0;
	const START_TIME_MINUTE_INDEX = 1;
	const START_TIME_SECOND_INDEX = 2;
	const DEFAULT_START_TIME = "00";

	const COL_FILENAME = "filename";
	const COL_CONFIG = "config";
	const COL_STATUS = "status";
	const COL_UPDATE_TIME = "update_time";

	private $product; //current processed product
	private $store; //current chosen store
	private $category; //category for which i product exported
	private $_taxHelper; //tax helper
	private $encoding; //chosen encoding
	private $editedCategoryIds; //edited ids of categories
	private $configManageStock = ''; //edited ids of categories

	private $_attributeSetMap;
	private $_attributeSet = null;
	private $_decimalDelimiter = ".";
	private $_feedObject;
    private $_reloadCache;
    
    /**
     * @var Nostress_Nscexport_Model_Export_Api_Adapter_Abstract
     */
    private $_exportApiAdapter = null;
    private $_exportApiType = null;
    private $_profileDirectAttributes = array("id","store_id","name","frequency","enabled","filename","feed");
	private $_loaderParamsConfigAttributes = array("category_lowest_level","datetime_format","category_path_delimiter","sort_attribute","sort_order","currency","all_images","all_categories","price_customer_group_id","load_product_reviews");
    
	public function _construct()
	{
		parent::_construct ();
		$this->_init ( 'nscexport/profile' );
		$this->_taxHelper = new Mage_Tax_Helper_Data ( );
		$delimiter = $this->getConfig(Nostress_Nscexport_Helper_Data::PARAM_DELIMITER);
		if(isset($delimiter))
			$this->setDecimalDelimiter($delimiter);
	}

	public function setStatus($status,$setUpdateTime = false)
	{
	    $id = $this->getId();
		if(!$id)
			return;
		$this->log("Export profile {$id} {$status}");
		$this->setData(self::COL_STATUS,$status);
		if($setUpdateTime)
			$this->setUpdateTime($this->helper()->getDateTime(null,true));
        $this->save();
	}

	public function getAllProfiles()
	{
		$collection = $this->getCollection();
		$collection->load();
		return $collection->getItems();
	}
	
	public function getUsedTaxonomies()
	{
		$collection = $this->getCollection()->load();
		
		$taxArray = array();
		foreach ($collection as $profile)
		{
			$feedObject = $profile->getFeedObject();
			if(empty($feedObject))
				continue;
			
			$code = $feedObject->getTaxonomyCode();
			if(!empty($code) && !in_array($code,$taxArray))
				$taxArray[] = $code;
		}
		return $taxArray;
	}

    public function getCollectionByStoreId($storeId)
	{
		$collection = $this->getCollection()->addFieldToFilter('store_id',$storeId);
		$collection->load();
		return $collection;
	}

	public function getCommonConfig( $key = null) {
	    
	    $config = $this->getConfig();
	    $feed = $this->getConfigField(self::FEED, $config);
	    $common = $this->getConfigField(self::COMMON, $feed);
	    if( $key) {
	        return $this->getArrayField( $key, $common);
	    } else {
	        return $common;
	    }
	}
	
	public function processSubmissionData( $data, $id) {
	    
	    $config = array();
	    // move cron_interval to general config and process days and times
	    if( isset( $data[ self::EXPORT_API][self::CRON_INTERVAL])) {
	        $cronInterval = $data[ self::EXPORT_API][self::CRON_INTERVAL];
	        unset( $data[ self::EXPORT_API][self::CRON_INTERVAL]);
	        
	        $config[self::GENERAL][self::CRON_INTERVAL] = $cronInterval;
	        
	        $days = Mage::getSingleton('nscexport/config_source_dayofweek')->getAllValues();
	        $times = $this->_getTimesFromInterval( $cronInterval);
	        
	        $cronModel = Mage::getModel("nscexport/cron");
	        $oldDays = $cronModel->getDaysPerProfile($this->getId());
	        $oldTimes = $cronModel->getTimesPerProfile($this->getId());
	        
	        $this->updateDayTimes($oldDays,$oldTimes,$days,$times);
	    }
	    
	    // update export API config
        $config[ self::EXPORT_API] = $this->getConfigField(self::EXPORT_API,$data, false);
        
        $merged = array_merge( $this->getConfig(), $config);
        
        $this->setConfig( $merged);
	    $this->save();
	}
	
	public function processData($data, $id) {

		$config = array();

		//general configuration part
		$general = $this->getConfigField(self::GENERAL,$data);
		foreach ($this->_profileDirectAttributes as $attribute) {
			$this->setData($attribute,$this->getArrayField($attribute,$general,""));
			unset($general[$attribute]);
		}
		$this->addFilenameSuffix();
		$config[self::GENERAL] = $general;
		
		if( $this->isApi()) {
		    $interval = $this->getConfigField(self::CRON_INTERVAL, $general,false);
		    $days = Mage::getSingleton('nscexport/config_source_dayofweek')->getAllValues();
		    $times = $this->_getTimesFromInterval( $interval);
		} else {
    		$days = $this->getConfigField(self::CRON_DAYS, $general,false);
    		$times = $this->getConfigField(self::CRON_TIMES, $general,false);
		}
		
		unset($config[self::GENERAL][self::CRON_DAYS]);
		unset($config[self::GENERAL][self::CRON_TIMES]);

		//feed configuration part
		$feed = $this->getConfigField(self::FEED, $data);

		$preparedAttributes = array();
		$counter = 0;
		$allAttributesInfo = $this->helper()->getVisibleProductAttributes(true);
		$attributes = $this->getAttributeFromArray($feed,array());

		foreach ($attributes as $key => $attribute) {

			$index = $counter;
			if (isset($attribute[self::CODE]) && $attribute[self::CODE] == self::CUSTOM_ATTRIBUTE) {
				if (isset($attribute[self::DELETE]) && $attribute[self::DELETE] == 1) {
					continue;
				}
				$index = $counter+Nostress_Nscexport_Helper_Data::CUSTOM_ATTRIBUTE_ROW_INDEX_OFFSET;
			}

			if(isset($attribute[self::POST_PROCESS]))
			{
				$postprocFunctions = $attribute[self::POST_PROCESS];
				if(is_array($postprocFunctions))
					$attribute[self::POST_PROCESS] = implode(self::POSTPROC_DELIMITER, $postprocFunctions);
			}

			//add attribute type
			if(isset($attribute[self::MAGENTO_ATTRIBUTE]))
			{
				$magentoAttributeCode = $attribute[self::MAGENTO_ATTRIBUTE];
				if(isset($allAttributesInfo[$magentoAttributeCode]))
				{
					$attributeInfo = $allAttributesInfo[$magentoAttributeCode];
					$attribute[self::MAGENTO_ATTRIBUTE_TYPE] = $attributeInfo->getFrontendInput();
				}

				if($magentoAttributeCode == self::SHIPPING_COST)
					$attribute[self::MAGENTO_ATTRIBUTE_TYPE] = "price";
			}

			$preparedAttributes[$index] = $attribute;
			$counter++;
		}

		$feed[self::ATTRIBUTES][self::ATTRIBUTE] = $preparedAttributes;
		
		if(isset($feed[self::COMMON][self::NEWLINE]))
			$feed[self::COMMON][self::NEWLINE] = $this->helper()->decodeSpaceCharacters($feed[self::COMMON][self::NEWLINE]);
		$config[self::FEED] = $feed;

	    //product configuration part
	    $product = $this->getConfigField(self::PRODUCT,$data);//self::PRODUCT,$data);
	    $types = $this->getArrayField(self::TYPES,$product,array());
	    $types = implode(",",$types);
	    $product[self::TYPES] = $types;
	    if(empty($product[self::AUTO_ADD_NEW_PRODUCTS_USE_DEFAULT]))
	    	$product[self::AUTO_ADD_NEW_PRODUCTS_USE_DEFAULT] = '0';
	    $config[self::PRODUCT] = $product;
	    
	    // leave ftp config not changed
 	    $config[self::EXPORT_API] = $this->getExportApiConfig();

	    $attributeFilter = $this->getConfigField(self::ATTRIBUTE_FILTER,$data);
	    foreach( array( self::VISIBILITY, self::VISIBILITY_PARENT) as $vis) {
	        if( !isset( $attributeFilter[ $vis])) {
	            $attributeFilter[$vis] = array();
	        }
	    }
	    $config[self::ATTRIBUTE_FILTER] = $attributeFilter;
	    $config[self::ATTRIBUTE_FILTER][self::CONDITIONS] = Mage::getModel( 'nscexport/rule')->parseConditionsPost( $this->getConfigField( 'rule',$data));
	    
	    $this->setConfig($config);
		$categoryProductIds = $data['category_product_ids'];
		$oldCategoryProductIds = "";
		$oldDays = array();
		$oldTimes = array();
		$profileIsNew = false;

		if($this->getId() != '')
		{
			// Get old export values
			$originalProfile = Mage::getModel('nscexport/profile')->load($this->getId());
			$deleteFeedFiles = false;

			$cronModel = Mage::getModel("nscexport/cron");
			$oldDays = $cronModel->getDaysPerProfile($this->getId());
			$oldTimes = $cronModel->getTimesPerProfile($this->getId());

			$oldCategoryProductIds = Mage::getModel('nscexport/categoryproducts')->getExportCategoryProducts($this->getId());

			//If search engine was changed => start times have to be recounted or categories to export were changed
			if($originalProfile->getFeed() != $this->getFeed() || $oldCategoryProductIds != $categoryProductIds)
			{
				$deleteFeedFiles = true;
			}
			else
			{
				//rename xml files
				if($this->getFilename() != $originalProfile->getFilename())
				{
				    //rename file nad temp file
				    $this->helper()->renameFile($originalProfile->getCurrentFilename(true),$this->getCurrentFilename(true));
				    $this->resetUrl();
				}
			}
			if($deleteFeedFiles)
			{
			    $originalProfile->deleteFiles();
			    $this->setUrl($this->helper()->__("Feed File doesn't exist."));
			}

    	}
    	else
    	{
    		$profileIsNew = true;
    		$this->setUrl($this->helper()->__("Feed File doesn't exist."));
    		$this->setCreatedTime($this->helper()->getDateTime());
    	}
    	$this->save();
    	
    	if($oldCategoryProductIds != $categoryProductIds)
    		$this->helper()->updateCategoryProducts($this->getId(),$categoryProductIds,$this->getStoreId());
    	$this->updateDayTimes($oldDays,$oldTimes,$days,$times);

    	if($profileIsNew)
    		$this->helper()->logNewProfileEvent($this->getFeed(),$this->getFileUrl());
	}
	
	protected function _getTimesFromInterval( $interval) {
	    
	    // minutes to seconds
	    $interval *= 60;
	    
	    $times = array();
	    $counter = 0;
	    $date=strtotime("00:00");
	    do {
	        // neverending loop control
	        if( $counter++ > 100) break;
	        
	        $times[] = date('H:i',$date);
	        $date += $interval;
	        
	    } while( date('H:i',$date) != "00:00");
	    
	    return $times;
	}

	public function getFilename($full = false,$fileSuffix = null, $addDate = false)
	{
	    $filename = $this->getData(self::COL_FILENAME);
	    
	    if($addDate)
	    {
	    	$timestamp = strtotime($this->getData(self::COL_UPDATE_TIME));
	    	$filename = $this->helper()->addDateToString($filename,$timestamp);
	    }
	    if(isset($fileSuffix))
	        $filename = $this->helper()->changeFileSuffix($filename,$fileSuffix);
	    if($full)
	    {
	        $feedDir = $this->helper()->getFeedDirectoryName($this->getFeed());

	        $dirPath = $this->helper()->getFullFilePath("",$feedDir);

	        $this->helper()->createDirectory($dirPath);

	        $filename = $this->helper()->getFullFilePath($filename,$feedDir);
	    }
	    return $filename;
	}

	protected function updateDayTimes($originalDays,$originalTimes, $days,$times)
	{
		if(empty($days))
			$days = array();
		if(empty($times))
			$times = array();
		
		$cronModel = Mage::getModel("nscexport/cron");

		$daysToDelete = array_diff($originalDays,$days);
		$timesToDelete = array_diff($originalTimes,$times);
		$cronModel->deleteRecords($this->getId(),$daysToDelete,$originalTimes);
		$cronModel->deleteRecords($this->getId(),$originalDays,$timesToDelete);

		$daysToAdd = array_diff($days,$originalDays);
		$timesToAdd = array_diff($times,$originalTimes);
		$cronModel->addRecords($this->getId(),$daysToAdd,$times);
		$cronModel->addRecords($this->getId(),array_diff($days,$daysToAdd),$timesToAdd);
	}

	protected function getCurrentFilename($full=false)
	{
	    $suffix = $this->getFeedObject()->getFileType();
	    $generalConfig = $this->getCustomConfig(self::GENERAL,false);
	    $compressFile = $this->getArrayField("compress_file",$generalConfig,"0");
	    if($compressFile)
	        $suffix = Nostress_Nscexport_Helper_Data::FILE_TYPE_ZIP;

	    return $this->getFilename($full,$suffix,true);
	}

	public function delete()
	{
	    $this->deleteFiles();
	    parent::delete();
	}

	public function setMessageStatusError($message,$status,$errorLink = "")
	{
    	$this->setMessage($message);
    	$this->setStatus($status);
    	$this->save();
	}

	//delete feed and temp feed files
	protected function deleteFiles()
	{
		$this->helper()->deleteFile($this->getCurrentFilename());
	    $this->setUrl($this->helper()->__("Feed File doesn't exist."));
	}

	public function resetUrl()
	{
	    $this->setUrl($this->getFileUrl());
	}

    protected function getFileUrl()
	{
	    $filename = $this->getCurrentFilename();
	    $feedDir = $this->helper()->getFeedDirectoryName($this->getFeed());
	    $filename = $this->helper()->getFileUrl($filename,$feedDir);
	    return $filename;
	}

	protected function addFilenameSuffix()
	{
	    $filename = $this->helper()->addFileSuffix($this->getFilename(),$this->getFeed());
	    $this->setFilename($filename);
	}

    protected function helper()
    {
    	return Mage::helper('nscexport/data');
    }

    public function exportCategoryTree()
    {
    	$feedConfig = $this->getMergedProfileFeedConfig();
    	return $this->getArrayField("category_tree",$feedConfig[self::COMMON],false);
    }

    public function exportProducts()
    {
    	$attributes = $this->getMagentoAttributes();
    	if(isset($attributes) && !empty($attributes))
    		return true;
    	else
    		return false;
    }

    public function getLoaderParams()
    {
        $productConfig = $this->getCustomConfig(self::PRODUCT);
        $attributeFilterConfig = $this->getCustomConfig(self::ATTRIBUTE_FILTER,false);
        if(isset($attributeFilterConfig))
        	$productConfig = array_merge($productConfig,$attributeFilterConfig);
        $feedObject = $this->getFeedObject();
        $attributes = $this->getMagentoAttributes();

        $params = array();
        $params["export_id"] = $this->getExportId();
        $params["store_id"] = $this->getStoreId();
        $params["use_product_filter"] = $this->getArrayField("use_product_filter",$productConfig,"0");
        $params["group_by_category"] = self::DEF_GROUP_BY_CATEGORY;
        $params["reload_cache"] = $this->getReloadCache();
        $params["taxonomy_code"] = $feedObject->getTaxonomyCode();
        $params["batch_size"] = $this->helper()->getGeneralConfig(Nostress_Nscexport_Helper_Data::PARAM_BATCH_SIZE);
        $params["conditions"] = $productConfig;
        $params["attributes"] = $attributes;
		$params["allow_inactive_categories_export"] = $this->helper()->getGeneralConfig(Nostress_Nscexport_Helper_Data::PARAM_ALLOW_INACTIVE_CATEGORIES_EXPORT);
        
        $feedConfig = $this->getMergedProfileFeedConfig();

        $stockDependence = $this->getArrayField("stock_status_dependence",$productConfig,"");
        if(empty($stockDependence))
        	$stockDependence = $this->getArrayField("stock_status_dependence",$feedConfig[self::COMMON][self::STOCK],"");
        $params["stock_status_dependence"] = $stockDependence;

        $commonConfigAttributes = $this->_loaderParamsConfigAttributes;
        foreach ($commonConfigAttributes as $attributeCode)
        {
        	$params[$attributeCode] = $this->getArrayField($attributeCode,$feedConfig[self::COMMON],null);
        }

        $loadAllProductCategories = "0";
        if(in_array("categories",$attributes))
            $loadAllProductCategories = "1";
        $params['load_all_product_categories'] = $loadAllProductCategories;
        
        $loadProductReviews = "0";
        if(in_array("reviews",$attributes))
        	$loadProductReviews = "1";
        $params['load_product_reviews'] = $loadProductReviews;

        return $params;
    }

    protected function getTransformParams()
    {
        $productConfig = $this->getCustomConfig(self::PRODUCT);

        $params = $this->getMergedProfileFeedConfig();
		if(isset($params['common']))
		{
        	foreach($params['common'] as $key => $param)
        	{
        		$params[$key] = $param;
        	}
        	unset($params['common']);
        }
        $params["file_type"] = $this->getFeedObject()->getFileType();
        $params[self::FILE_URL] = $this->getFileUrl();
        $params["store_id"] = $this->getStoreId();
        $params["parents_childs"] = $this->getArrayField("parents_childs",$productConfig,"0");
        $params["xslt"] = $this->getFeedObject()->getTrnasformationXslt();
        return $params;
    }

    public function getXmlTransformParams()
    {
    	return $this->getTransformParams();
    }

    public function getXsltTransformParams()
    {
    	$params = $this->getTransformParams();
    	$attributes = $this->getAttributeFromArray($params);
    	//add params
    	if(isset($attributes) && is_array($attributes))
    	{
    		$cdataSectionElements = array();
    		$customColumnsHeader = array();
    		$columnsHeader = array();
    		foreach ($params[self::ATTRIBUTES][self::ATTRIBUTE] as $key => $attribute)
    		{
    			if ($attribute[self::CODE] == self::CUSTOM_ATTRIBUTE)
    			{
    				$attribute[self::TAG] =  $this->helper()->createCode($attribute[self::LABEL],"_",false,":");
    				$customColumnsHeader[] = $attribute[self::LABEL];
    			}
    			else if($attribute[self::TYPE] != self::DISABLED && $attribute[self::TYPE] != self::CSV_DISABLED)
    			{
    				$columnsHeader[] = $attribute[self::LABEL];
    			}

    			if(array_key_exists(self::POST_PROCESS,$attribute) && strpos($attribute[self::POST_PROCESS], self::CDATA) !== false)
    			{
    				if(isset($attribute[self::TAG]))
    					$cdataSectionElements[] = $attribute[self::TAG];
    				else
    					$cdataSectionElements[] =  $attribute[self::LABEL];
    			}
    		}
    		$params[self::CUSTOM_COLUMNS_HEADER] = $customColumnsHeader;
    		$params[self::CDATA_SECTION_ELEMENTS] = $cdataSectionElements;
    		$params[self::BASIC_ATTRIBUTES_COLUMNS_HEADER] = $columnsHeader;
    	}
    	return $params;
    }

    public function getWriterParams()
    {
        $params = $this->getCustomConfig(self::GENERAL);
        $suffix = $this->getFeedObject()->getFileType();
        $params["full_filename"] = $this->getFilename(true,$suffix,true);
        $params["filename"] = $this->getFilename(false,$suffix,true);
        $params["zip_filename"] = $this->getFilename(true,Nostress_Nscexport_Helper_Data::FILE_TYPE_ZIP,true);
        return $params;
    }

    protected function getMergedProfileFeedConfig($feedCode = null)
    {
        $feedConfig = $this->getFeedObject($feedCode)->getAttributesSetup();

        $profileFeedConfig = $this->getCustomConfig(self::FEED,false);
        if(!empty($profileFeedConfig))
        {
            $profileFeedConfig = $this->helper()->updateArray($profileFeedConfig,$feedConfig);
        }
        else
        {
            $profileFeedConfig = $feedConfig;
        }

        $attributes = $this->removeEmptyAttributes($profileFeedConfig);
        if(!empty($attributes))
        	$profileFeedConfig[self::ATTRIBUTES][self::ATTRIBUTE] = $attributes;

        return $profileFeedConfig;
    }

    protected function removeEmptyAttributes($feedConfig)
    {
        $attributeInfoArray = $this->getAttributeFromArray($feedConfig,array());

        $attributes = array();
        foreach ($attributeInfoArray as $attribute)
        {
         	$code = $this->getArrayField(self::CODE,$attribute);
         	$label = $this->getArrayField(self::LABEL,$attribute,"");
         	if(empty($label) && $code != self::CUSTOM_ATTRIBUTE)
         		continue;
         	$attributes[] = $attribute;
        }
        return $attributes;
    }

    public function getBackendConfig($feedCode = null)
    {
        $profileFeedConfig = $this->getMergedProfileFeedConfig($feedCode);

        $config = $this->getConfig();
        if(empty($config))
        {
            $config = array();
        }

        $config[self::FEED] = $profileFeedConfig;
        return $config;
    }

    protected function getCustomConfig($index,$exception = true)
    {
        return $this->getConfigField($index,$this->getConfig(),$exception);
    }

    protected function getConfigField($index,$config,$exception = true)
    {
        $field = $this->getArrayField($index,$config);
        if(!isset($field) && $exception)
        {
            $this->logAndException("Can't load %s configuration.",$index);

        }
        return $field;
    }

    public function getReloadCache()
    {
        if(!isset($this->_reloadCache))
            $this->_reloadCache = self::DEF_RELOAD_CACHE;
        return $this->_reloadCache;
    }

    public function setReloadCache($reloadCache)
    {
        $this->_reloadCache = $reloadCache;
    }

    public function getFeedObject($feedCode = null)
    {
        if(!isset($feedCode))
            $feedCode = $this->getFeed();
        if(!isset($this->_feedObject))
            $this->_feedObject = Mage::getModel('nscexport/feed')->getFeedByCode($feedCode);
        return $this->_feedObject;
    }

    protected function getMagentoAttributes()
    {
         //$feedConfig = $this->getCustomConfig(self::FEED);
         $feedConfig = $this->getMergedProfileFeedConfig();
         $attributeInfoArray = $this->getAttributeFromArray($feedConfig);

         $attributes = array();
          if(empty($attributeInfoArray))
         {
         	if($this->exportCategoryTree())
         		 return $attributes;

         	$this->logAndException("Missing feed attributes configuration.");
         }

         $attributes = array();
         foreach ($attributeInfoArray as $attribute)
         {
             $magentoAttribute = $this->getArrayField(self::MAGENTO_ATTRIBUTE,$attribute);
             if(isset($magentoAttribute) && !empty($magentoAttribute) && !in_array($attribute,$attributes))
                 $attributes[] = $magentoAttribute;

             $prefix = $this->getArrayField(self::PREFIX,$attribute,"");
             $suffix = $this->getArrayField(self::SUFFIX,$attribute,"");
             $prefixSuffixAttributes = $this->helper()->grebVariables($prefix.$suffix);
             if(!empty($prefixSuffixAttributes))
             	$attributes = array_merge($attributes,$prefixSuffixAttributes);
         }

         //attribute from stock setup
         $common = $this->getArrayField("common",$feedConfig,array());
         $stock = $this->getArrayField("stock",$common);
         if(!isset($stock))
             $this->logAndException("Missing feed stock configuration.");
         $availabilityAttribute = $this->getArrayField("availability",$stock);
         if(!empty($availabilityAttribute))
             $attributes[] = $availabilityAttribute;

         //attribute from shipping setup
         $shipping = $this->getArrayField(self::SHIPPING,$common);
         if(isset($shipping))
         {
         	$dependentAttribute = $this->getArrayField(self::DEPENDENT_ATTRIBUTE,$shipping);
         	if(!empty($dependentAttribute))
            	 $attributes[] = $dependentAttribute;
         }
         
         //remove unexisting attributes
        $allAttributes = Mage::helper('nscexport/data_feed')->getAttributeCodes($this->getStoreId());
        $attributes = array_intersect($attributes, $allAttributes);
         
         return $attributes;
    }

    public function setConfig($config)
    {
        $this->setData(self::COL_CONFIG,json_encode($config));
    }

    public function getConfig()
    {
        $id = $this->getId();
         if(!isset($id))
             return null;
        $config = json_decode($this->getData(self::COL_CONFIG),true);
		return $config;
    }

    protected function getAttributeFromArray($input,$default = null)
    {
    	$attributes = $this->getArrayField(self::ATTRIBUTES,$input);
        $attributeInfoArray = $this->getArrayField(self::ATTRIBUTE,$attributes,$default);
		return $attributeInfoArray;
    }

    public function getConditions() {

        if (!$this->hasData(self::CONDITIONS)) {
            $config = $this->getConfig();
            if( $config !== null && isset( $config[self::ATTRIBUTE_FILTER][self::CONDITIONS])) {
                $this->setData(self::CONDITIONS, $config[self::ATTRIBUTE_FILTER][self::CONDITIONS]);
            } else {
                $this->setData(self::CONDITIONS, array());
            }
        }
        return $this->getData(self::CONDITIONS);
    }

    public function getStoreIdsByProfileIds($profileIds)
    {
    	$collection = $this->getCollection();
    	$select = $collection->getSelect();
    	$select->columns('store_id');
    	$select->group('store_id');
    	$select->where('export_id IN (?)',$profileIds);
    	$collection->load();
    	$storeIds = array();
    	foreach ($collection as $item)
    		$storeIds[] = $item->getStoreId();
    	return $storeIds;
    }

    public function getProfilesByIds($profileIds)
    {
    	$collection = $this->getCollection();
    	$select = $collection->getSelect();
    	$select->where('export_id IN (?)',$profileIds);
    	return $collection->load();
    }

    public function getProfilesByNames($names)
    {
    	$collection = $this->getCollection();
    	$select = $collection->getSelect();
    	$select->where('name IN (?)',$names);
    	return $collection->load();
    }

    public function updateProfileFeedConfig()
    {
        $feedObject = $this->getFeedObject();
        if(empty($feedObject))
            return;
        $feedConfig = $this->getFeedObject()->getAttributesSetup();
        $profileFeedConfig = $this->getCustomConfig(self::FEED,false);
        if(empty($feedConfig[self::ATTRIBUTES][self::ATTRIBUTE]) || empty($profileFeedConfig[self::ATTRIBUTES][self::ATTRIBUTE]))
            return;

        $attributesProfile = $profileFeedConfig[self::ATTRIBUTES][self::ATTRIBUTE];
        $attributesFeed = $feedConfig[self::ATTRIBUTES][self::ATTRIBUTE];

        //Fast check
        if($this->attributesCompare($attributesProfile,$attributesFeed))
            return;

        $attributesResult = array();
        $attributesProfileMap = array();
        $attributesCustom = array();

        //separate custom attributes
        foreach ($attributesProfile as $key => $data)
        {
            if(empty($data[self::CODE]))
                return;
            if($data[self::CODE] == self::CUSTOM_ATTRIBUTE)
            {
                $attributesCustom[] = $data;
                unset($attributesProfile[$key]);
            }
            else
            {
                $code = $data[self::CODE];

                if(array_key_exists($code,$attributesProfileMap))
                    array_push($attributesProfileMap[$code], $data);
                else
                    $attributesProfileMap[$code] = array($data);
            }
        }

        $attributeCodes = Mage::helper('nscexport/data_feed')->getAttributeCodes($this->getStoreId());
        //recreate profile attributes
        foreach ($attributesFeed as $data)
        {
            $index = 0;
            if(empty($data[self::CODE]))
                return;
            else
                $index = $data[self::CODE];

            if(isset($attributesProfileMap[$index][0]))
            {
                $attributesResult[] = $attributesProfileMap[$index][0];
                array_shift($attributesProfileMap[$index]);
            }
            else
            {
                $magentoAttribute = "";
                if(array_key_exists(self::MAGENTO_ATTRIBUTE, $data) && in_array($data[self::MAGENTO_ATTRIBUTE], $attributeCodes))
                    $magentoAttribute = $data[self::MAGENTO_ATTRIBUTE];

                $attributesResult[] = array(self::CODE => $index, self::MAGENTO_ATTRIBUTE => $magentoAttribute);
            }
        }
        $attributesResult = array_merge($attributesResult,$attributesCustom);

        //save to config
        $config = $this->getConfig();
        $config[self::FEED][self::ATTRIBUTES][self::ATTRIBUTE] = $attributesResult;
        $this->setConfig($config);
        $this->save();
    }

    public function isFileExists() {
        
        $suffix = $this->getFeedObject()->getFileType();
        $fullFilename = $this->getFilename(true, $suffix,true);
        $enabled =  Mage::helper('nscexport/version')->isLicenseValid();
        
        return ( $enabled && is_file( $fullFilename));
    }
    
    public function isSubmitable() {
        
        $code = $this->getExportApiType();
        $config = $this->getExportApiConfig( $code);
        return ( $this->isFileExists() && $this->getExportApiAdapter( $code)->isFilled($config));
    }
      
    public function isAutosubmitable() {
        
        $code = $this->getExportApiType();
        $config = $this->getExportApiConfig( $code);
        return (isset( $config['enabled']) && $config['enabled'] && $this->getExportApiAdapter( $code)->isFilled($config));
    }
    
    public function submit() {
        
        if( !$this->isFileExists()) {
            throw new Exception( 'Update feed before submit!');
        }
        
        $apiType = $this->getApiType();
        try {
            $adapter = $this->getExportApiAdapter( $apiType);
            $result = $adapter->uploadFlatFile();
            if( $result) {
                $this->saveSubmit( $apiType);
                //$this->_saveApiSettingStatus( $apiType, 1);
                return true;
            } else {
                return false;
            }
        } catch( Exception $e) {
            if( $e->getCode() != self::CODE_NOT_ENABLED) {
                $method = $this->isApi() ? $this->helper()->__('Submit:') : $this->helper()->__('Upload:');
                $this->setMessageStatusError( $method." ".$e->getMessage(),
                        Nostress_Nscexport_Model_Profile::STATUS_ERROR);
            }
            //$this->_saveApiSettingStatus( $apiType, 0);
            throw $e;
        }
    }
    
    public function saveSubmit( $type, $timestamp = null) {
        
        $method = $this->isApi() ? $this->helper()->__('Submit:') : $this->helper()->__('Upload:');
        
        $pMessage = str_replace($method." OK", '', $this->getMessage());
        $this->setMessage( $pMessage." ".$method." OK");
        $this->setStatus( Nostress_Nscexport_Model_Profile::STATUS_FINISHED);
    
//         $this->submit_type = $type;
//         if( $timestamp === null) {
//             $this->last_submit_at = new Zend_Db_Expr( 'NOW()');
//         } else {
//             $this->last_submit_at = new Zend_Db_Expr( "FROM_UNIXTIME($timestamp)");
//         }
        $this->save();
    }
    
    public function getExportApiType() {
    
        if( !$this->_exportApiType) {
            switch( $this->getFeedObject()->getApiFileType()) {
                case Nostress_Nscexport_Model_Export_Api_Adapter_Beslist::API_FILE_TYPE:
                    $this->_exportApiType = self::EXPORT_API_BESLIST;
                    break;
                default:
                    $this->_exportApiType = self::EXPORT_API_FTP;
                    break;
            }
        }
        return $this->_exportApiType;
    }
    
    public function isApi() {
        return $this->getExportApiType() != self::EXPORT_API_FTP;
    }
    
    /**
     *
     * @param unknown $exportApiCode
     * @throws Exception
       @return Nostress_Nscexport_Model_Export_Api_Adapter_Abstract
     */
    public function getExportApiAdapter( $exportApiCode = null) {
        
        if( !$exportApiCode) {
            $exportApiCode = $this->getExportApiType();
        }
        $exportApiAdapter = Mage::getModel('nscexport/export_api_adapter_'.ucfirst($exportApiCode));
        if( !$exportApiAdapter) {
            throw new Exception( Mage::helper('nscexport')->__("Adatper for export api '%s' is not implemented!", $exportApiCode));
        }
        $exportApiAdapter->setProfile( $this);
    
        return $exportApiAdapter;
    }
    
    public function getExportApiConfig( $code = null)
    {
        if( !$code) {
            $code = $this->getExportApiType();
        }
        
        $config = $this->getCustomConfig( self::EXPORT_API, false);
        
        $result = array();
        if( !empty($config)) {
            $result = $config;
        } else {
            $submissionParams = $this->getFeedObject()->getSubmissionDefaults( $code);
            if( !empty( $submissionParams)) {
                $result = $submissionParams;
            }
        }
        return $result;
    }
    
    public function getSubmitCollection( $merchantId = null) {
    
        $profileSubmitCollection = Mage::getModel( 'nscexport/submission_report')->getCollection();
        if( $merchantId !== null) {
            $profileSubmitCollection->addFieldToFilter('merchant_id', $merchantId);
        } else {
            $profileSubmitCollection->addFieldToFilter('profile_id', $this->getId());
        }
        $profileSubmitCollection->getSelect()
            ->order( 'submit_date DESC')
            ;
    
        return $profileSubmitCollection;
    }
    
    /**
     * Fast compare of both attribute arrays
     * @param unknown $attributesProfile
     * @param unknown $attributesFeed
     * @return boolean
     */
    protected function attributesCompare($attributesProfile,$attributesFeed)
    {
        $lastIndex = 0;
        foreach ($attributesFeed as $index => $data)
        {
            $lastIndex = $index;
            $code = '';
            if(isset($data[self::CODE]))
                $code = $data[self::CODE];
            else
                return false;

            if(isset($attributesProfile[$index][self::CODE]) && $attributesProfile[$index][self::CODE] == $code)
                continue;
            return false;
        }

        $lastIndex++;
        if(isset($attributesProfile[$lastIndex][self::CODE]) &&  $attributesProfile[$lastIndex][self::CODE] != self::CUSTOM_ATTRIBUTE)
            return false;
        return true;
    }
    
	public function autoAddNewProducts()
    {
    	$productConfig = $this->getCustomConfig(self::PRODUCT,false);
    	$useDefault = $this->getConfigField(self::AUTO_ADD_NEW_PRODUCTS_USE_DEFAULT,$productConfig,false);
    	if($useDefault == '0' && isset($productConfig))
    	{
    		return $this->getConfigField(self::AUTO_ADD_NEW_PRODUCTS,$productConfig,false);
    	}
    	else
    	{
    		return Mage::helper('nscexport')->getGeneralConfig(Nostress_Nscexport_Helper_Data::PARAM_ADD_PRODUCTS);
    	}
    }

    public function getPreview( $formated = true) {
    
        $suffix = $this->getFeedObject()->getFileType();
        $url = $this->getFilename(true, $suffix,true);
    
        if( $this->getFeedObject()->getFileType() == 'xml') {
    
            $preview = $this->readXmlPreview( $url);
        } else {
    
            $cd = $this->getCommonConfig( 'column_delimiter');
            if( $cd === false) {
                $cd = ';';
            }
            $enc = $this->getCommonConfig( 'text_enclosure');
            if( $enc === false) {
                $enc = '';
            }
    
            if( $formated) {
                list( $header, $array) = $this->readCsvPreview( $url, $cd, $enc);
                $preview = $this->formatCsvPreview( $header, $array);
                // for submit without formating
            } else {
                list( $header, $array) = $this->readCsvPreview( $url, $cd, $enc, null); // max = null
                $preview = array();
                foreach( $array as $index => $arrayRow) {
                    $row = array();
                    foreach( $arrayRow as $rowIndex => $value) {
                        if( isset($header[$rowIndex])) {
                            $row[ $header[$rowIndex]] = $value;
                        }
                    }
                    $preview[ $arrayRow[ 0]] = $row;
                }
            }
        }
    
        return $preview;
    }
    
    public function readXmlPreview( $url, $max = 30000) {
    
        if( !is_file( $url)) return false;
    
        // read file by lines
        $code = '';
        $braked = false;
        $fp = fopen( $url, "r");
        while(!feof($fp))
        {
            $result = fgets($fp, 10000);
            $code .= $result;
            // if max is reached, we must end xml with correct element
            if( strlen( $code) > $max) {
                $braked = true;
                break;
            }
        }
        fclose($fp);
    
        $code = $this->formatXmlString( $code);
    
        // prepare for print
        $code = $this->_formatHtmlEntities( $code);
        if( $braked) {
            $code .= "\n...";
        }
    
        $stylesheet = $this->helper( 'nscexport')->getGeneralConfig( 'highlightjs_style');
    
        // dark - darkula
        // light -
        $html = "
        <link rel='stylesheet' href='$stylesheet'>
        <script src='//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.3.0/highlight.min.js'></script>
        <script>hljs.initHighlightingOnLoad();</script>
        <pre class='noformat'><code class='html'>$code</code></pre>";
    
        return $html;
    }
    
    protected function _formatHtmlEntities( $string) {
    
        $encoding = $this->getCommonConfig( 'encoding');
        if( $encoding != 'utf-8') {
            $string = mb_convert_encoding($string, 'UTF-8', $encoding);
        }
        return $string = htmlentities( $string, ENT_SUBSTITUTE, 'UTF-8');
    }
     
    public function formatXmlString($xml) {
    
        $xml = preg_replace('/(>)(<)(\/*)/', "$1\n$2$3", $xml);
        $token      = strtok($xml, "\n");
        $result     = '';
        $pad        = 0;
    
        $matches    = array();
        while ($token !== false) :
    
        // text + koncovy element (nebo zacatek, text a konec)
        if (preg_match('/.+<\/\w[^>]*>$/', $token, $matches)) :
        $indent=0;
        // samostatny konec
        elseif (preg_match('/^<\/\w/', $token, $matches)) :
        $pad-=4;
        $indent = 0;
        // zacatek, muze byt s textem ale nemusi
        elseif (preg_match('/^<\w[^>]*[^\/]>$/', $token, $matches)) :
        $indent=4;
        // bez elementu
        else :
        $indent = 0;
        endif;
    
        $line    = str_pad($token, strlen($token)+$pad, ' ', STR_PAD_LEFT);
    
        $result .= $line . "\n";
    
        $token   = strtok("\n");
        $pad    += $indent;
        endwhile;
    
        return $result;
    }
    
    public function readCsvPreview( $url, $columnSeparator = ';', $enclosure = '', $max = 500) {
    
        if( !is_file( $url)) return false;
    
        $header = array();
        $array = array();
        if (($handle = fopen( $url, "r")) !== FALSE) {
    
            // NOTE - enclosure does not work for bol.com. Each row returns false
    
            if(($header = fgetcsv($handle, 10000, $columnSeparator)) !== false) {
    
                // skip commented row and read header again
                // commented row has only one column (#v2.0.1)
                if( count( $header) == 1) {
                    $header = fgetcsv($handle, 10000, $columnSeparator);
                }
            }
            $row = 0;
            while (($data = fgetcsv($handle, 10000, $columnSeparator)) !== FALSE) {
    
                if( $max !== null && $row > $max) {
                    break;
                }
    
                $array[] = $data;
    
                $row++;
            }
            fclose($handle);
        }
    
        return array( $header, $array);
    }
    
    public function formatCsvPreview( $header, $array) {
    
        $html = '<table class="data feed-preview-table"><thead><tr class="headings">';
        foreach( $header as $col) {
            $html .= "<th>$col</th>";
        }
        $html .= '</tr></thead><tbody>';
        foreach( $array as $row) {
            $html .= '<tr>';
            foreach( $row as $value) {
                $html .= "<td>".$this->_formatHtmlEntities( $value)."</td>";
            }
            $html .= "</tr>";
        }
        $html .= "</tbody></table>";
    
        return $html;
    }
}