<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
 * Model for Export
 *
 * @category Nostress
 * @package Nostress_Nscexport
 *
 */

class Nostress_Nscexport_Model_Submission_Report extends Mage_Core_Model_Abstract
{
	const STATUS_FINISHED = 'finished';
	const STATUS_PROCESING = 'processing';
	
    public function _construct()
	{
		parent::_construct ();
		$this->_init ( 'nscexport/submission_report' );
	}
	
	public function isDone() {
	
	    return ($this->getStatus() == self::STATUS_FINISHED);
	}
	
	public function isProcessing() {
	
	    return ($this->getStatus() == self::STATUS_PROCESING || $this->getStatus() == 'new');
	}
	
	public function initMessage() {
	    
	    $message = json_decode( $this->getRepportMessage(), true);
	    if( is_array( $message) && count( $message)) {
	        $this->addData( $message);
	    }
	}
	
	protected function _afterLoad() {
	
	    $this->initMessage();
	     
	    return parent::_afterLoad();
	}
}