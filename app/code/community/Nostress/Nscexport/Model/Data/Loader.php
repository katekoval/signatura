<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
* Data loader for export process
* @category Nostress
* @package Nostress_Nscexport
*
*/

class Nostress_Nscexport_Model_Data_Loader extends Nostress_Nscexport_Model_Abstract
{
    protected $adapter;
    protected $_flatCatalogs = array('product','category');
    
    public function _construct()
    {
        // Note that the export_id refers to the key field in your database table.
        $this->_init('nscexport/data_loader', 'entity_id');
    }
    
    public function init($params)
    {
        $this->setData($params);
        $this->initAdapter();
    }
    
    public function loadBatch()
    {
    	return $this->adapter->loadBatch();
    }
    
    public function loadAll()
    {
    	return $this->adapter->loadAll();
    }
    
    protected function initAdapter()
    {
        $this->adapter = $this->getResource();
        $this->adapter->setData($this->getData());        
        $this->adapter->setStoreId($this->getStoreId());        
        $this->adapter->setAttributes($this->getAttributes());                                      
        $this->adapter->init();
    }
    
    protected function isFlatEnabled()
    {
        $fc = $this->getFlatCatalogs();
        foreach ($fc as $type)
        {
        	$enabled = $this->helper()->isEnabledFlat($this->getStoreId(),$type);
        	if(!$enabled)
        	{
        	    throw new Exception($this->helper()->__(ucfirst($type)." flat catalog is disabled"));
        	}
        }
        return true;
    }
    
    protected function getFlatCatalogs()
    {
        return $this->_flatCatalogs;
    }
    
    protected function helper()
    {
    	if(!isset($this->_helper))
    		$this->_helper = Mage::helper('nscexport/data_loader');
    	return $this->_helper;
    }
}
?>