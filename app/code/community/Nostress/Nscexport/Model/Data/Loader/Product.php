<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
* Product loader for export process
* @category Nostress
* @package Nostress_Nscexport
*
*/

class Nostress_Nscexport_Model_Data_Loader_Product extends Nostress_Nscexport_Model_Data_Loader
{
    const PRODUCTS_FILTER = 0;
    const PRODUCTS_ALL = 1;
    const PRODUCTS_FILTER_BY_CATEGORY = 2;
    const PRODUCTS_ALL_BY_CATEGORY = 3;
      
    public function _construct()
    {
        // Note that the export_id refers to the key field in your database table.
        $this->_init('nscexport/data_loader_product', 'entity_id');
    }

    public function initAdapter()
    {
        parent::initAdapter();
        
        $this->reloadProfileDependentCache();
        if($this->getReloadCache())
        	$this->reloadCache();
        $this->basePart();
        $this->commonPart();
//         echo $this->adapter->getSelect()->__toString();
//         exit();
    }
    
    protected function reloadProfileDependentCache()
    {    	
    	$filterByProducts = $this->getUseProductFilter();
    	
    	$exportReviews = $this->getLoadProductReviews();    	
    	if($exportReviews)
    	{
    		$reviewsCache = Mage::getModel('nscexport/cache_reviews');    		
    		$sqlDatetimeFormat = Mage::getModel("nscexport/config_source_datetimeformat")->getSqlFormat($this->getDatetimeFormat(),self::DATE_TIME);
    		$reviewsCache->setTimestampFormatForSql($sqlDatetimeFormat);
    		$reviewsCache->reload($this->getStoreId());
    	}
    	
    	if($filterByProducts)
    	{
    		$profileCategory = Mage::getModel('nscexport/cache_profilecategory');
    		//set profile id,
    		$profileCategory->setExportId($this->getExportId());
    		$profileCategory->setLowestLevel($this->getCategoryLowestLevel());
    		$profileCategory->setAllowInactiveCategoriesExport($this->getAllowInactiveCategoriesExport());
    		
    		$exportAllCategoriesProfileAdjustment = $this->getAllCategories();
    		$exportAllCategoriesAvailableInFeed = $this->loadAllProductCategoriesCondition();
    		$profileCategory->setLoadAllProductCategoriesCondition($exportAllCategoriesAvailableInFeed && $exportAllCategoriesProfileAdjustment);
    		$profileCategory->reload($this->getStoreId());
    	}
    	else
    	{
	    	$categories = Mage::getModel('nscexport/cache_categories');
	    	$categories->setLowestLevel($this->getCategoryLowestLevel());
	    	$categories->setCategoryPathDelimiter($this->getCategoryPathDelimiter());
	    	$categories->reload($this->getStoreId());
	    	
	    	$categoryMaxLevel = Mage::getModel('nscexport/cache_categorymaxlevel');
	    	$categoryMaxLevel->reload($this->getStoreId());
    	}
    	
    	
    	
    	$taxonomyCats = Mage::getModel('nscexport/cache_enginecategory');
    	$taxonomyCats->setTaxonomyCode($this->getTaxonomyCode());
    	$taxonomyCats->reload($this->getStoreId());
    }
    
    protected function reloadCache()
    {    	
    	$storeId = $this->getStoreId();
    	$websiteId = Mage::app()->getStore($storeId)->getWebsiteId();
    	$this->helper()->reloadCache(array($storeId),array($websiteId));       
    }
    
    //***************************BASE PART**************************************
    protected function basePart()
    {
    	$filterByProducts = $this->getUseProductFilter();
    	$groupByCategory = $this->getGroupByCategory();
    	if($filterByProducts && $groupByCategory)
    	{
    		$this->productsFilterByCategory();
    	}
        else if(!$filterByProducts && $groupByCategory)
    	{
    		$this->productsAllByCategory();
    	}
        else if(!$filterByProducts && !$groupByCategory)
    	{
    		$this->productsAll();
    	}
    	else
    	{	
    		$this->productsFilter();
    	}
    }
    
    /**
     * Init sql.
	 * Load filteres products from current store.
     */
	protected function productsFilter()
	{
		$this->adapter->joinProductRelation();
		$this->adapter->joinProductFilter ();	
		 
		$this->adapter->joinCategoryFlatWithFilteredProducts();
		//$this->adapter->joinCategoryMaxLevelFiltredProducts();		
		
		$this->adapter->joinTaxonomy();
		$this->adapter->joinParentCategory();
		
		$this->adapter->groupByProduct ();
	}
	
	/**
	 * Init sql.
	 * Load all products from current store.
	 */
	protected function productsAll()
	{
		//add category information
		$this->adapter->joinProductCategoryMaxLevel();
		$this->adapter->joinLeftCategoryFlat();
		
		if ($this->loadAllProductCategoriesCondition())
		{
			//add all category information
			$this->adapter->joinAllCategoriesCache ();
		}
		
		$this->adapter->joinTaxonomy();
		$this->adapter->joinParentCategory();
		$this->adapter->joinProductRelation();
		$this->adapter->groupByProduct();
	}
	
	/**
	 * Init sql.
	 * Load all products from current store, order by category.
	 */
	protected function productsAllByCategory()
	{
		$this->adapter->joinCategoryProduct ();
		$this->adapter->joinCategoryFlat ();
		
		if (!$this->loadAllProductCategoriesCondition())
		{
			//one category per product
			$this->adapter->joinProductCategoryMaxLevel ();
			$this->adapter->groupByProduct ();
		}
		$this->adapter->orderByCategory ();
		$this->adapter->joinTaxonomy();
		$this->adapter->joinParentCategory();
		$this->adapter->joinProductRelation();
	}
	
	/**
	 * Init sql.
	 * Load filtered products from current store, order by category.
	 */
	protected function productsFilterByCategory()
	{
		$this->adapter->joinProductFilter ();
		//add category information
		$this->adapter->joinCategoryFlat ();
		
		if(!$this->loadAllProductCategoriesCondition())
		{
			//one category per product
			$this->adapter->joinExportCategoryProductMaxLevel ();
			$this->adapter->groupByProduct ();
		}
		$this->adapter->orderByCategory ();
		$this->adapter->joinTaxonomy();
		$this->adapter->joinParentCategory();
		$this->adapter->joinProductRelation();
	}
	
	protected function loadAllProductCategoriesCondition()
    {
    	$allProductCategories = $this->getLoadAllProductCategories();
    	return $allProductCategories;
    }
    
    //***************************COMMON PART**************************************
    
    protected function commonPart()
    {
    	$this->adapter->joinProductEntity();
    	$this->adapter->joinProductUrlRewrite(); 
    	$this->adapter->joinCategoryUrlRewrite();
    	$this->adapter->addTypeCondition();
    	$this->visibility();
    	
    	$this->adapter->addSortAttribute();
    	$this->adapter->setProductsOrder();
    	
    	$this->stock();
        $this->adapter->joinSuperAttributesCache();
        $this->adapter->joinMediaGalleryCache();
        $this->adapter->joinReview();
        $this->adapter->joinReviewsCache();
        $this->price();
        
        $this->adapter->addAttributeFilter();
        $this->adapter->joinWeee();
        $this->adapter->joinCustomTables();
    }

    protected function visibility()
    {
    	$this->adapter->addVisibilityCondition();
    }
    
    protected function stock()
    {
    	$this->adapter->joinStock();
    	$this->adapter->addStockCondition();
    }
    
    protected function price()
    {
    	$this->adapter->joinTax();
    	$this->adapter->joinPrice();
    }
}
?>