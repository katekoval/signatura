<?php 
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* Product loader for export process
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

class Nostress_Nscexport_Model_Data_Loader_Category extends Nostress_Nscexport_Model_Data_Loader
{       
    
    public function _construct()
    {    
        // Note that the export_id refers to the key field in your database table.
        $this->_init('nscexport/data_loader_category', 'entity_id');
    }	
    
    public function initAdapter()
    {
        parent::initAdapter();                
        $this->basePart();
//          echo $this->adapter->getSelect()->__toString();
//          exit();
    } 
  
    
    //***************************BASE PART**************************************    
    protected function basePart()
    {
    	$this->adapter->joinParentCategory();
    	$this->adapter->joinCategoryUrlRewrite();
        $this->adapter->joinCategoryPath();
    	$this->adapter->orderByLevel();
        //$filterByProducts = $this->getUseProductFilter();
    	
    	$this->adapter->addAttributeFilter();
//    	if($filterByProducts)
//    	{
//    		$this->categoryFilterByProducts();
//    	}
    }
    
    protected function categoryFilterByProducts()
    {
    	$this->adapter->joinProductFilter ();    	
    }
    
    //***************************COMMON PART**************************************
    
    protected function commonPart()
    {
    	
    } 
}
?>