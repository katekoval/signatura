<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* Helper.
* 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

class Nostress_Nscexport_Helper_Data_Loader extends Nostress_Nscexport_Helper_Data
{
    const GROUP_ROW_SEPARATOR = ";;";
    const GROUP_ROW_ITEM_SEPARATOR = "||";
    const GROUP_CONCAT = "GROUP_CONCAT";
    const GROUP_SEPARATOR = "SEPARATOR";
    const GROUPED_COLUMN_ALIAS = "concat_colum";
    
    const COLUMN_CATEGORIES_DATA = "ccd";
	
    const CONDITION_EXPORT_OUT_OF_STOCK = 'export_out_of_stock';
    const CONDITION_PARENTS_CHILDS = 'parents_childs';
    const CONDITION_TYPES = 'types';
    const DISABLED = "disabled";
    
    public function getCommonParameters()
    {
    	$paramIndexes = array(self::PARAM_REVIEW_URL,self::PARAM_IMAGE_FOLDER,self::PARAM_ALLOW_INACTIVE_CATEGORIES_EXPORT/*,self::PARAM_ALLOW_CHILD_PRODUCTS_EXPORT*/,self::PARAM_URL_ATTRIBUTE_SOURCE,self::PARAM_ALLOW_PLACEHOLDER_IMAGES_EXPORT);
    	$params = array();
    	
    	foreach($paramIndexes as $index)
    		$params[$index] = $this->getGeneralConfig($index);    	
    	
    	return $params;
    }
    
	public function groupConcatColumns($columns, $groupRowSeparator = null, $groupedColumnAlias = null)
    {
        if (!isset($groupRowSeparator)) {
            $groupRowSeparator = $this->getGroupRowSeparator();
        }
        
        if (!isset($groupedColumnAlias)) {
            $groupedColumnAlias = self::GROUPED_COLUMN_ALIAS;
        }
        
        $res = self::GROUP_CONCAT . "(";
        $columnValues = array_values($columns);
    
        $columnString = "";
        $separator = $this->getGroupRowItemSeparator();
        foreach ($columnValues as $value) {
            if (empty($columnString)) {
                $columnString = $value;
            } else {
                $columnString .= ",'{$separator}',".$value;
            }
        }
        $res .= $columnString." ".self::GROUP_SEPARATOR." '{$groupRowSeparator}'";
    
        $res .= ") as ".$groupedColumnAlias;
        return new \Zend_Db_Expr($res);
    }
    
    protected function getGroupRowSeparator()
    {
        return self::GROUP_ROW_SEPARATOR;
    }
    
    protected function getGroupRowItemSeparator()
    {
        return self::GROUP_ROW_ITEM_SEPARATOR;
    }
    
	public function getPriceColumnFormat($columnName, $taxRateColumnName,$currencyRate = null, $originalPriceIncludeTax=false,$calcPriceIncludeTax = true, $round=true,$weeeColumnTaxable,$weeeColumnNonTaxable)
	{
		$resSql = $columnName;
		
		if(isset($currencyRate) && is_numeric($currencyRate))
		{
			$resSql .= "*".$currencyRate;
		}
		
		if(!empty($weeeColumnTaxable))
			$resSql = "(({$resSql})+{$weeeColumnTaxable})";
		
		if(!$originalPriceIncludeTax && $calcPriceIncludeTax)
		{
			$resSql .= "*(1+ IFNULL(".$taxRateColumnName.",0))";		
		}
		else if($originalPriceIncludeTax && !$calcPriceIncludeTax)
		{
			$resSql .= "*(1/(1+ IFNULL(".$taxRateColumnName.",0)))";
		}	

		if(!empty($weeeColumnNonTaxable))
			$resSql = "(({$resSql})+{$weeeColumnNonTaxable})";
			
	    if ($round) {
	    	$resSql = $this->getRoundSql($resSql);            
	    }
	   
	    return $resSql;
	}
	
	public function getRoundSql($column,$decimalPlaces = 2)
	{
		return "ROUND(".$column.",{$decimalPlaces})";     
	}
	
	public function getStoreCurrencyRate($store, $toCurrencyCode = null)
	{
		$from = $store->getBaseCurrencyCode();
		if(empty($toCurrencyCode))
			$toCurrencyCode = $store->getCurrentCurrencyCode();

		if($from == $toCurrencyCode)
			return null;
		else
			return $this->getCurrencyRate($from,$toCurrencyCode);
	}
	
	protected function getCurrencyRate($from,$to)
	{
		return Mage::getModel('directory/currency')->load($from)->getRate($to);
	}
	
	public function checkFlatCatalogs()
	{
	 	$allStoreIds = array_keys(Mage::app()->getStores());
	    foreach($allStoreIds as $id)
	    {
		    $this->checkFlatCatalog($id);
	    }
	}
	
	public function checkFlatCatalog($storeId)
	{
		$this->getProductFlatColumns($storeId);
		$this->getCategoryFlatColumns($storeId);
	}
	
	public  function getProductFlatColumns($storeId)
	{
		try
		{
	    	$productFlatResource = Mage::getResourceModel('catalog/product_flat')->setStoreId($storeId);
	    	return $productFlatResource->getAllTableColumns();
		}
		catch (Exception $e)
		{
			Mage::throwException("11");			
			return array();
		} 
	}
	
	public  function getCategoryFlatColumns($storeId)
	{
	    try
	    {
			$flatResource = Mage::getResourceModel('catalog/category_flat')->setStoreId($storeId);	    
	    	$describe =  Mage::getSingleton('core/resource')->getConnection('core_write')->describeTable($flatResource->getMainTable());
        	return array_keys($describe);
        }
        catch (Exception $e)
        {
        	Mage::throwException("11");        	
        	return array();
        }
	}
	
	public function getLoaderAttributes()
	{
		$resource = Mage::getResourceModel('nscexport/data_loader_product');
		
		$columns = $resource->getAllColumns();
		$staticColumns = $resource->getStaticColumns();
		$staticColumns = array_combine($staticColumns, $staticColumns);
		$columns = array_merge($columns,$staticColumns);
		$multiColumns = $resource->getMultiColumns();
		
		ksort($columns);
		$attributes = array();
		foreach ($columns as $alias => $column) 
		{
			$attribute = array();
			$attribute[self::VALUE] = $alias;
			$attribute[self::LABEL] = $this->codeToLabel($alias);
			if(in_array($alias,$multiColumns))
				$attribute[self::DISABLED] = "1";
			$attributes[$attribute[self::VALUE]] = $attribute;
		}
		return $attributes;
	}
	
	public function getDefaultTaxCountry($store)
	{
		return $store->getConfig(Mage_Tax_Model_Config::CONFIG_XML_PATH_DEFAULT_COUNTRY);
	}
	
	public function getDefaultTaxRegion($store)
	{
		return $store->getConfig(Mage_Tax_Model_Config::CONFIG_XML_PATH_DEFAULT_REGION);
	}
	
	public function reloadCache($storeIds,$websiteIds)
	{
		foreach ($storeIds as $storeId)
		{
			Mage::getModel('nscexport/cache_categorypath')->reload($storeId);
			Mage::getModel('nscexport/cache_superattributes')->reload($storeId);
			Mage::getModel('nscexport/cache_mediagallery')->reload($storeId);
			Mage::getModel('nscexport/cache_tax')->reload($storeId);
		}
		
		foreach ($websiteIds as $websiteId) {
			Mage::getModel('nscexport/cache_weee')->reload($websiteId);
		}
	}
	
	public function getStoreTaxonomyLocale($taxonomyCode,$storeLocale)
	{			
		$locale = $storeLocale;
		$taxonomyModel = Mage::getModel('nscexport/taxonomy');
	
		$cols = $taxonomyModel->countColumns($taxonomyCode,$locale);
		if(!isset($cols) || $cols == "0")
		{
			$defaultLocale = $taxonomyModel->getTaxonomyDefaultLocale($taxonomyCode);
			if(!empty($defaultLocale) && $taxonomyModel->countColumns($taxonomyCode,$defaultLocale) > 0)
			{
				$locale = $defaultLocale;
			}
			else
			{
				$cols = $taxonomyModel->countColumns($taxonomyCode);
				if(!isset($cols) || $cols == "0")
				{
					$message = $this->log($this->__("Taxonomy table empty"));
					return null;
				}
				$locale = Nostress_Nscexport_Model_Taxonomy::ALL_LOCALES;
			}
		}
		return $locale;
	}
}