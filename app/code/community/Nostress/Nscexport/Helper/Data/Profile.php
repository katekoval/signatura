<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
* Helper.
*
* @category Nostress
* @package Nostress_Nscexport
*
*/

class Nostress_Nscexport_Helper_Data_Profile extends Nostress_Nscexport_Helper_Data
{
	protected $_errorList;

	public function runAllProfiles()
	{
		$this->runProfiles(Mage::getModel('nscexport/profile')->getAllProfiles());
	}

	public function runProfilesByIds($profileIds)
	{
		$profiles = Mage::getModel('nscexport/profile')->getProfilesByIds($profileIds);
		$this->runProfiles($profiles);
		return true;
	}

	public function runProfilesByNames($profileNames)
	{
		$profiles = Mage::getModel('nscexport/profile')->getProfilesByNames($profileNames);
		$this->runProfiles($profiles);
		return true;
	}

	public function runProfiles($profiles, $submit = true)
	{
		if(empty($profiles))
			return;
		Mage::helper('nscexport/version')->validateLicenceBackend();
		$this->reloadProfilesCache($profiles);
		foreach ($profiles as $item)
		{
			try
			{
				$item->setReloadCache(false);
				$this->runProfile($item, $submit);
			}
			catch(Exception $e)
			{
				$this->log($e->getMessage());
			}
		}
	}

	public function runProfile($profile, $submit = true)
	{
		if (!$profile->getId()) {
			Mage::getSingleton('adminhtml/session')->addError($this->__('Invalid Profile ID'));
		} else {
			Mage::getModel('nscexport/unit_control')->run($profile);
			if( $submit && $profile->isAutosubmitable()) {
		        $profile->submit();
			}
		}
	}

    protected function reloadProfilesCache($profiles)
    {
    	if(empty($profiles))
    		return;
    	$storeIds = array();
    	$websiteIds = array();
    	$ids = array();
    	foreach ($profiles as $profile)
    	{
    		if(is_numeric($profile))
    			$ids[] = $profile;
    		else
    		{
    			$storeId = $profile->getStoreId();
    			if(!in_array($storeId,$storeIds))
    				$storeIds[] = $storeId;
    		}
    	}

    	if(!empty($ids))
    		$storeIds = Mage::getModel('nscexport/profile')->getStoreIdsByProfileIds($ids);

    	foreach(Mage::app()->getWebsites() as $website)
    	{
    		$websiteStoreIds = $website->getStoreIds();
    		$intersec = array_intersect($websiteStoreIds,$storeIds);
    		if(!empty($intersec))
    			$websiteIds[] = $website->getWebsiteId();
    	}

    	Mage::helper('nscexport/data_loader')->reloadCache($storeIds,$websiteIds);
    }

    public function updateProfilesFeedConfig()
    {
        $profiles = Mage::getModel('nscexport/profile')->getAllProfiles();

        foreach ($profiles as $profile)
        {
            $profile->updateProfileFeedConfig();
        }
    }
    
    
}