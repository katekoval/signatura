<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* Helper.
* 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

class Nostress_Nscexport_Helper_Data_Source extends Nostress_Nscexport_Helper_Data
{	
	const DEF_CATEGORY_LOWEST_LEVEL = 2;
	
	protected function toOptionArray($input)
	{
		foreach ($input as $key => $data) {
			$return[] = array('label' => $data, 'value' => $key);
		}
		return $return;
	}
	
	public function getDelimitersOptionArray() {
		$return = array();
		$delimiters = array("." => $this->__("Dot")." - ( . )",
							"," => $this->__("Comma")." - ( , )",
							"" => $this->__("Empty")." - price in cents",);
		natsort($delimiters);
		return $this->toOptionArray($delimiters);
	}
	
	public function getEnclosureOptionArray() {
		$return = array();
		$enclosures = array('"' => $this->__("Double quotes").' - ( " )',
							"'" => $this->__("Quotes")." - ( ' )",
							"" => $this->__("Empty - no enclosure"));
		return $this->toOptionArray($enclosures);
	}
	
	public function getColumnDelimiterOptionArray() {
		$return = array();
		$delimiters = array("|" => $this->__("Pipe")." - ( | )",
							"," => $this->__("Comma")." - ( , )",
							"\t" => $this->__("Tab")." - ( \\t )",
							" " => $this->__("Space")." - ( ' ' )",
							";" => $this->__("Semicolon")." - ( ; )",
							"/" => $this->__("Slash")." - ( / )",
							"-" => $this->__("Dash")." - ( - )",
							"*" => $this->__("Star")." - ( * )",
							'\\' => $this->__("Backslash")." - ( \\ )",
							":" => $this->__("Colon")." - ( : )",
							"#" => $this->__("Grid")." - ( # )",
							"&" => $this->__("Ampersand")." - ( & )",
							"~" => $this->__("Tilde")." - ( ~ )"
							);
		
		return $this->toOptionArray($delimiters);
	}
	
	public function getNewlineDelimiterOptionArray() {
		$return = array();
		$delimiters = array("\\r\\n" => "CR+LF - ( \\r\\n )",
							"\\n\\r" => "LF+CR - ( \\n\\r )",
							"\\n" => "LF - ( \\n )",
							"\\r" => "CR - ( \\r )"
							);
		
		return $this->toOptionArray($delimiters);
	}
	
	public function getCategoryLevelOptionArray()
	{
		$return = array();
		$delimiters = array("1" => "1",
							"2" => "2",
							"3" => "3",
							"4" => "4",
							"5" => "5",
							"6" => "6",
							"7" => "7",
							"8" => "8",
							"9" => "9",
							"10" => "10",
						);
		
		return $this->toOptionArray($delimiters);
	}
	
	public function getStoreCurrenciesOptionArray($store)
	{
		$codes = $store->getAvailableCurrencyCodes(true);
		sort($codes);
		$baseCurrencyCode = $store->getBaseCurrencyCode();
		$defaultCurrencyCode = $store->getDefaultCurrencyCode();
		
		$index = array_search($defaultCurrencyCode, $codes);
		if($index !== false)
		{
			unset($codes[$index]);
			$options = array($defaultCurrencyCode => $defaultCurrencyCode." (".$this->__("Default Display Currency").")");
		}
		else
			$options = array();
		
		foreach ($codes as $code)
		{
			$label = $code;
			if($code == $baseCurrencyCode)
				$label .= " (".$this->__("Base Currency").")"; 
			$options[$code] = $label;
		}
		
		return $this->toOptionArray($options);
	}
	
	public function getCustomerGroupsOptions()
	{
		$groups = Mage::getModel('customer/group')->getCollection();
		$options = array();
		foreach($groups as $group)
		{
			$options[] = array('label' => $group->getCode(), 'value' => $group->getId());
		}
		return $options;		
	}
}