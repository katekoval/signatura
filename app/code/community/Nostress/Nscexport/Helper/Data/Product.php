<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* Helper.
* 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

class Nostress_Nscexport_Helper_Data_Product extends Nostress_Nscexport_Helper_Data
{
	const CODE = 'code';
	const LABEL = 'label';
	
	public function getSubProducts($product)
    {   
    	if(!isset($product))
    		return null;     
    	
    	$products = false;
    	$copyUrl = false;
    	switch($product->getTypeId())
    	{
    		case Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE:
    			$products = $this->getConfigurableSubProducts($product);
    			$copyUrl = true;
    			break;
    		case Mage_Catalog_Model_Product_Type::TYPE_GROUPED:
    		    $products = $this->getGroupedSubProducts($product);
    			break;
    		case Mage_Catalog_Model_Product_Type::TYPE_BUNDLE:
    		    $products = $this->getBundleSubProducts($product);
    			break;
    		default:    			
    			break;
    	}
    	$products = $this->addParentAttributes($product,$products,$copyUrl);
    	return $products;
    	
    }
    
    public function getConfigurableAttributes($product,$format = false)
    {
        $attributes = $product->getTypeInstance(true)->getConfigurableAttributes($product);
        $attributes =  Mage::helper('core')->decorateArray($attributes);
        if(!$format)
        	return $attributes;
        else 
        {
        	$result = array();
            foreach($attributes as $attribute)
   		    {
   		    	$productAttribute = $attribute->getProductAttribute();
   		    	if(!isset($productAttribute))
   		    	{
   		    	    $this->log("Missing product attribute, product id: {$product->getId()}");   		    	    
   		    	    continue;
   		    	}
   		    	$code = $productAttribute->getAttributeCode();
   		    	$label = $attribute->getLabel();
   		    	$result[] = array(self::CODE => $code,self::LABEL=>$label);
   		    }
   		    return $result;
        }
    }
    
    
    protected function getConfigurableSubProducts($product)
    { 
        //$subProduct->isSealable()
       return  $product->getTypeInstance(true)->getUsedProducts(null, $product);
    }
    
    protected function getBundleSubProducts($product)
    { 
       return $this->getProductsToPurchase($product);
    }
    
    protected function getGroupedSubProducts($product)
    { 
       return  $product->getTypeInstance(true)->getAssociatedProducts($product);
    }
    
    protected function addParentAttributes($parent,$childs,$copyUrl=false)
    {
        if(!isset($childs) || !is_array($childs))
            return $childs;
            
        $parentId = $parent->getId();
		$parentSku = $parent->getSku();
		$parentName = $parent->getName();
		
		$parentUrl = null;
		if($copyUrl)
			$parentUrl = $parent->getUrl();
		
		$i = 0;
	    foreach ($childs as $p) 
        {
           	$p->setParentId($parentId);
		   	$p->setParentSku($parentSku);
		   	$p->setParentName($parentName);
		   	if(isset($parentUrl))
		   	{	
		   		$i++;
		   		$p->setUrl($parentUrl."#{$i}");		   		
		   	}
        }
        return $childs;
    }
    
    /**
     * Retrieve products divided into groups required to purchase
     * At least one product in each group has to be purchased
     *
     * @param  Mage_Catalog_Model_Product $product
     * @return array
     */
    protected function getProductsToPurchase($product = null)
    {
        $bundle = $product->getTypeInstance(true);
        $product = $bundle->getProduct($product);
        $allProducts = array();
        
        foreach ($bundle->getOptions($product) as $option) 
        {
            $groupProducts = array();
            foreach ($bundle->getSelectionsCollection(array($option->getId()), $product) as $childProduct) 
            {                
                $allProducts[] = $childProduct;
            }
        }
        return $allProducts;
    }
}