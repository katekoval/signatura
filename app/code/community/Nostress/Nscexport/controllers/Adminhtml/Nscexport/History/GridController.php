<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */ 

/** 
* Kontroler pro historii exportnich procesu spoustenych pres cron
* 
* @category Nostress 
* @package Nostress_Nscexport
* 
*/

class Nostress_Nscexport_Adminhtml_Nscexport_History_GridController extends Mage_Adminhtml_Controller_Action
{

	/** Magento 1.9.2.0 or patch 6285_CE_1.9.1.1 access permissions fix*/
	protected function _isAllowed()
	{
		return Mage::getSingleton('admin/session')->isAllowed('koongoconnector/nscexportcron/history');
	}
	/**
	 *
	 * @return Mage_Cron_Model_Schedule
	 */
	protected function _initSchedule()
	{
		$scheduleId = (int) $this->getRequest()->getParam('schedule_id');

		$schedule = Mage::getModel('cron/schedule');

		if($scheduleId) {
			$schedule->load($scheduleId);
		}

		Mage::register('nscexport_cron_schedule', $schedule);

		return $schedule;
	}

	/**
	 * Init layout, menu and breadcrumb
	 *
	 * @return Nostress_Cron_Adminhtml_HistoryController
	 */
	protected function _initAction()
	{
		$this->loadLayout()
				->_setActiveMenu('koongoconnector/nscexportcron/history')
				->_addBreadcrumb($this->__('System'), $this->__('System'))
				->_addBreadcrumb($this->__('Schedule'), $this->__('Schedule'));
		return $this;
	}

	public function indexAction()
	{
		$this->_initAction();
		$this->_addContent($this->getLayout()->createBlock('nscexport/adminhtml_history'));
		$this->renderLayout();
	}

	/**
	 * Grid
	 */
	public function gridAction()
	{
		$this->getResponse()->setBody(
				$this->getLayout()->createBlock('nscexport/adminhtml_history_grid')->toHtml()
		);
	}
	
	/**
	 * Cancel selected orders
	 */
	public function massDeleteAction()
	{
		$cronIds = $this->getRequest()->getPost('cron_ids', array());
		$countDeleteCron = 0;
		$countNonDeleteCron = 0;
		foreach($cronIds as $cronId)
		{
			try {
				$cron = Mage::getModel('cron/schedule')->load($cronId);
				$cron->delete();
				$countDeleteCron++;
			}
			catch(Exception $e) {
				$countNonDeleteCron++;
			}
		}
		if($countNonDeleteCron) {
			if($countDeleteCron) {
				$this->_getSession()->addError($this->__('%s Cronjob(s) cannot be deleted', $countNonDeleteCron));
			}
			else {
				$this->_getSession()->addError($this->__('The Cronjob(s) cannot be deleted'));
			}
		}
		if($countDeleteCron) {
			$this->_getSession()->addSuccess($this->__('%s Cronjob(s) have been deleted.', $countDeleteCron));
		}
		$this->_redirect('*/*/');
	}
}