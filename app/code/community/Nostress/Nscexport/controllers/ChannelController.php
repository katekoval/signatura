<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
 * Frontend kontroler pro exportni modul
 *
 * @category Nostress
 * @package Nostress_Nscexport
 *
 */

class Nostress_Nscexport_ChannelController extends Mage_Core_Controller_Front_Action 
{
	public function loadChannelManualAction()
	{
		$url = $this->getRequest()->getParam('url',"");
		if($url == 'empty')
		{
			$html = "<br>";
			$html .= Mage::helper('nscexport')->__("Feed submission information availabe only for users with valid Support and Updates period.")."<br>";
			$html .=  Mage::helper('nscexport')->__("License Status").": ".Mage::helper('nscexport/version')->getLicenseKeyStatusHtml(false);
			$this->getResponse()->setBody($html);
		}
		else 
		{
			try
			{
				$html = Mage::helper('nscexport')->sendCurlRequest($url);
			}
			catch(Exception $e)
			{
				$html = "<br>";
				$html .= Mage::helper('nscexport')->__("Subbmision information not available for this feed.");
			}
			$this->getResponse()->setBody($html);
		}				
	}
}