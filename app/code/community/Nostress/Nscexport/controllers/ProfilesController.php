<?php
/**
 * Magento Module developed by NoStress Commerce
 *
 * NOTICE OF LICENSE
 *
 * This program is licensed under the Koongo software licence (by NoStress Commerce). 
 * With the purchase, download of the software or the installation of the software 
 * in your application you accept the licence agreement. The allowed usage is outlined in the
 * Koongo software licence which can be found under https://docs.koongo.com/display/koongo/License+Conditions
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at https://store.koongo.com/.
 *
 * See the Koongo software licence agreement for more details.
 * @copyright Copyright (c) 2017 NoStress Commerce (http://www.nostresscommerce.cz, http://www.koongo.com/)
 *
 */

/**
 * Frontend kontroler pro exportni modul
 *
 * @category Nostress
 * @package Nostress_Nscexport
 *
 */

class Nostress_Nscexport_ProfilesController extends Mage_Core_Controller_Front_Action
{
	const PROFILES = 'profiles';
	
	public function runAction()
	{
		try
		{
		   	$helper = Mage::helper('nscexport/data_profile');
		   	$profiles = null;
		   	$profiles = $this->getRequest()->getParam(self::PROFILES);
		   	if(isset($profiles))
		   	{
				$profiles = explode(",",$profiles);
				$profileNames = array();
				$profileIds = array();
				foreach ($profiles as $profile)
				{
					if(is_numeric($profile))
						$profileIds[] = $profile;
					else
						$profileNames[] = $profile;
				}
				// run profile using profile IDs or names
				$helper->runProfilesByNames($profileNames);
				$helper->runProfilesByIds($profileIds);
		   	}
		   	else
		   		$helper->runAllProfiles();
		}
		catch (Exception $e)
		{
		    Mage::printException($e);
		}
	}
}