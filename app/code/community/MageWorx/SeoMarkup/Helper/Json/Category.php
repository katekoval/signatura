<?php
/**
 * MageWorx
 * MageWorx SeoMarkup Extension
 *
 * @category   MageWorx
 * @package    MageWorx_SeoMarkup
 * @copyright  Copyright (c) 2017 MageWorx (http://www.mageworx.com/)
 */

class MageWorx_SeoMarkup_Helper_Json_Category extends MageWorx_SeoMarkup_Helper_Abstract
{

    public function getJsonCategoryData($productCollection)
    {
        $speakableData = array();
        if ($this->_helperConfig->isCategoryGaEnabled()) {
            $speakableData['@context']  = 'http://schema.org/';
            $speakableData['@type']     = 'WebPage';
            $speakable                  = array();
            $speakable['@type']         = 'SpeakableSpecification';
            $speakable['cssSelector']   = explode(',', $this->_helperConfig->getCategoryGaCssSelectors());
            $speakable['xpath']         = array('/html/head/title');
            $speakableData['speakable'] = $speakable;
        }

        if (!$this->_helperConfig->isCategoryRichsnippetEnabled()) {
            return !empty($speakableData) ? $speakableData : false;
        }

        $category = Mage::registry('current_category');
        if (!is_object($category)) {
            return !empty($speakableData) ? $speakableData : false;
        }

        $data = array();
        $data['@context']    = 'http://schema.org';
        $data['@type']       = 'WebPage';
        $data['url']         = Mage::helper('core/url')->getCurrentUrl();
        $data['mainEntity']                    = array();
        $data['mainEntity']['@context']        = 'http://schema.org';
        $data['mainEntity']['@type']           = 'OfferCatalog';
        $data['mainEntity']['name']            = $category->getName();
        $data['mainEntity']['url']             = Mage::helper('core/url')->getCurrentUrl();
        $data['mainEntity']['numberOfItems']   = count($productCollection->getItems());

        if ($this->_helperConfig->isUseOfferForCategoryProducts()) {
            $data['mainEntity']['itemListElement'] = array();

            foreach ($productCollection as $product) {
                $data['mainEntity']['itemListElement'][] = $this->_getProductData($product);
            }
        }

        $data = array_merge($data, $speakableData);

        return $data;
    }

    public function _getProductData($product)
    {
        $this->_helper->reset();

        $speakableData = array();
        if ($this->_helperConfig->isProductGaEnabled()) {
            $speakableData['@context']  = 'http://schema.org/';
            $speakableData['@type']     = 'WebPage';
            $speakable                  = array();
            $speakable['@type']         = 'SpeakableSpecification';
            $speakable['cssSelector']   = explode(',', $this->_helperConfig->getProductGaCssSelectors());
            $speakable['xpath']         = array('/html/head/title');
            $speakableData['speakable'] = $speakable;
        }

        if (!$this->_helperConfig->isProductRichsnippetEnabled()) {
            return !empty($speakableData) ? $speakableData : false;
        }

        if (!$this->_helperConfig->isProductJsonLdMethod()) {
            return !empty($speakableData) ? $speakableData : false;
        }

        $product = $product;
        $data = array();
        $data['@context']    = 'http://schema.org';
        $data['@type']       = 'Product';
        $data['name']        = $product->getName();
        $data['description'] = $this->_helper->getDescriptionValue($product);

        /** @var Mage_Catalog_Helper_Image $helperImage */
        $helperImage = Mage::helper('catalog/image');
        $data['image'] = $helperImage->init($product, 'small_image')->__toString();

        if ($this->_helperConfig->getProductOffer()) {
            $data['offers'] = $this->_getOfferData($product);

            if (empty($data['offers']['price'])) {
                return !empty($speakableData) ? $speakableData : false;
            }
        }

        if ($this->_helperConfig->isRatingEnabled()) {
            $aggregateRatingData = $this->_helper->getAggregateRatingData($product, false);

            if ($this->_helper->isValidReviewData($aggregateRatingData)) {
                $data['aggregateRating'] = $aggregateRatingData;
            }
        }

        $productIdValue = $this->_helper->getProductIdValue($product);
        if ($productIdValue) {
            $data['productID'] = $productIdValue;
        }

        $color = $this->_helper->getColorValue($product);
        if ($color) {
            $data['color'] = $color;
        }

        $brand = $this->_helper->getBrandValue($product);
        if ($brand) {
            $data['brand'] = $brand;
        }

        $manufacturer = $this->_helper->getManufacturerValue($product);
        if ($manufacturer) {
            $data['manufacturer'] = $manufacturer;
        }

        $model = $this->_helper->getModelValue($product);
        if ($model) {
            $data['model'] = $model;
        }

        $gtin =  $this->_helper->getGtinData($product);
        if (!empty($gtin['gtinType']) && !empty($gtin['gtinValue'])) {
            $data[$gtin['gtinType']] = $gtin['gtinValue'];
        }

        $skuValue = $this->_helper->getSkuValue($product);
        if ($skuValue) {
            $data['sku'] = $skuValue;
        }

        $data['url'] = $this->_helper->getProductCanonicalUrl($product);

        $heightValue = $this->_helper->getHeightValue($product);
        if ($heightValue) {
            $data['height'] = $heightValue;
        }

        $widthValue = $this->_helper->getWidthValue($product);
        if ($widthValue) {
            $data['width'] = $widthValue;
        }

        $depthValue = $this->_helper->getDepthValue($product);
        if ($depthValue) {
            $data['depth'] = $depthValue;
        }

        $weightValue = $this->_helper->getWeightValue($product);
        if ($weightValue) {
            $data['weight'] = $weightValue;
        }

        $categoryName = $this->_helper->getCategoryValue($product);
        if ($categoryName) {
            $data['category'] = $categoryName;
        }

        $customProperties = $this->_helperConfig->getCustomProperties();
        if ($customProperties) {
            foreach ($customProperties as $propertyName => $propertyValue) {
                if ($propertyName && $propertyValue) {
                    $value = $this->_helper->getCustomPropertyValue($product, $propertyValue);
                    if ($value) {
                        $data[$propertyName] = $value;
                    }
                }
            }
        }

        return !empty($speakableData) ? array($data, $speakableData) : $data;
    }

    /**
     * @param $product
     * @return array
     * @throws Mage_Core_Model_Store_Exception
     */
    protected function _getOfferData($product)
    {
        $data   = array();
        $prices = Mage::helper('mageworx_seomarkup/price')->getPricesByProductType($product->getTypeId(), $product);

        if (is_array($prices) && count($prices)) {
            $data['price'] = $prices[0];
        }

        $data['priceCurrency'] = Mage::app()->getStore()->getCurrentCurrencyCode();

        $availability = $this->_helper->getAvailability($product);
        if ($availability) {
            $data['availability'] = $availability;
        }

        $condition = $this->_helper->getConditionValue($product);
        if ($condition) {
            $data['itemCondition'] = $condition;
        }

        $priceValidUntil = $this->_helper->getPriceValidUntilValue($product);
        if ($priceValidUntil) {
            $data['priceValidUntil'] = $priceValidUntil;
        }

        $data['url'] = $product->getProductUrl();

        return $data;
    }
}
