<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Banners
 */ 
class Amasty_Banners_Block_Container extends Amasty_Banners_Block_Abstract
{  
    public function __construct()
    {
        parent::__construct();
        $this->setTemplate('ambanners/container.phtml');
    }

    public function needShowProducts($banner)
    {
        $restrictedForProductsPositions = array(7, 8, 14);

        $needShowProduct = false;

        if (Amasty_Banners_Model_Rule::SHOW_PRODUCTS_YES == $banner->getShowProducts()
            && !in_array($this->getCurrentPosition(), $restrictedForProductsPositions)
        ) {
            $needShowProduct = true;
        }

        return $needShowProduct;
    }
}
