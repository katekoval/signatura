<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Banners
 */ 
class Amasty_Banners_Block_Adminhtml_Rule_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('ruleTabs');
        $this->setDestElementId('edit_form');
        $this->setTitle(Mage::helper('ambanners')->__('Banners'));
    }

    protected function _beforeToHtml()
    {
        $tabs = array(
            'general'    => 'General',
            'banners'    => 'Banner Content',
            'conditions' => 'Cart Conditions',
            'analytics' => 'Google Analytics',
        );
        
        foreach ($tabs as $code => $label){
            $label = Mage::helper('ambanners')->__($label);
            $content = $this->getLayout()->createBlock('ambanners/adminhtml_rule_edit_tab_' . $code)
                ->setTitle($label)
                ->toHtml();
                
            $this->addTab($code, array(
                'label'     => $label,
                'content'   => $content,
            ));
        }
        
        /*
         * Add Products Tab
         */
        $this->addTab('products', array(
			'label'     => Mage::helper('ambanners')->__('Products'),
            'title'     => Mage::helper('ambanners')->__('Products'),
            'class'     => 'ajax',
            'url'       => $this->getUrl('adminhtml/ambanners_products/products', array('_current' => true)),
		));              
        
        return parent::_beforeToHtml();
    }

    /**
     * Prepare html output
     *
     * @return string
     */
    protected function _toHtml()
    {
        $html = parent::_toHtml();

        return "<script>

            FormElementDependenceController.prototype = Object.extend(new FormElementDependenceController(), {
              trackChange : function(e, idTo, valuesFrom) {
                if (!$(idTo)) {
                    return;
                }

                // define whether the target should show up
                var shouldShowUp = true;
                for (var idFrom in valuesFrom) {
                    var from = $(idFrom);
                    if (valuesFrom[idFrom] instanceof Array) {
                        if (!from || valuesFrom[idFrom].indexOf(from.value) == -1) {
                            shouldShowUp = false;
                        }
                    } else {
                        var value = from.getValue();
                        if (typeof(value) === 'object'){
                            if (!from ||
                                " . '$' ."A(value).detect(function(v){return v === valuesFrom[idFrom]}) === undefined
                            ) {
                                shouldShowUp = false;
                            }
                        } else {
                            if (!from || from.value != valuesFrom[idFrom]) {
                                shouldShowUp = false;
                            }
                        }

                    }
                }

                // toggle target row
                if (shouldShowUp) {
                    var currentConfig = this._config;
                    $(idTo).up(this._config.levels_up).select('input', 'select', 'td').each(function (item) {
                        // don't touch hidden inputs (and Use Default inputs too), bc they may have custom logic
                        if ((!item.type || item.type != 'hidden') && !($(item.id+'_inherit') && $(item.id+'_inherit').checked)
                            && !(currentConfig.can_edit_price != undefined && !currentConfig.can_edit_price)) {
                            item.disabled = false;
                        }
                    });
                    $(idTo).up(this._config.levels_up).show();
                } else {
                    $(idTo).up(this._config.levels_up).select('input', 'select', 'td').each(function (item){
                        // don't touch hidden inputs (and Use Default inputs too), bc they may have custom logic
                        if ((!item.type || item.type != 'hidden') && !($(item.id+'_inherit') && $(item.id+'_inherit').checked)) {
                            item.disabled = true;
                        }
                    });
                    $(idTo).up(this._config.levels_up).hide();
                }
              }
            });
        </script>".$html;
    }


}