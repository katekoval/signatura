<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Banners
 */

class Amasty_Banners_Block_Inject extends Amasty_Banners_Block_Abstract
{
    /**
     * @return null|string
     */
    public function getContainerSelector()
    {
        return Mage::getStoreConfig('ambanners/general/product_container');
    }

    /**
     * @return null|string
     */
    public function getItemSelector()
    {
        return Mage::getStoreConfig('ambanners/general/product_item');
    }

    /**
     * @return int|null
     */
    public function getColumnCount()
    {
        $count = null;
        /** @var Mage_Catalog_Block_Product_List $parent */
        $parent = $this->getParentBlock();
        $count = $parent->getColumnCount();

        return $count;
    }
}
