<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Banners
 */
class Amasty_Banners_Model_Observer
{
    protected $_firstTime = true;

    /**
     * Append rule product attributes to select by quote item collection
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function addProductAttributes(Varien_Event_Observer $observer)
    {
        // @var Varien_Object
        $attributesTransfer = $observer->getEvent()->getAttributes();

        $attributes = Mage::getResourceModel('ambanners/rule')->getAttributes();
        
        $result = array();
        foreach ($attributes as $code) {
            $result[$code] = true;
        }
        $attributesTransfer->addData($result);
        
        return $this;
    }

    public function coreBlockAbstractToHtmlAfter(Varien_Event_Observer $observer)
    {
        $block = $observer->getBlock();
        $isScroll = Mage::app()->getRequest()->getParam('is_scroll');
        if ($block instanceof Mage_Catalog_Block_Product_List
            && $this->_firstTime
            && !$isScroll
        ) {
            $this->_firstTime = false;
            $transport = $observer->getTransport();
            if (is_object($transport)) {
                $html = $transport->getHtml();

                /** @var Amasty_Banners_Block_Inject $injectBlock */
                $injectBlock = Mage::getBlockSingleton('ambanners/inject');
                if  (!$injectBlock) {
                    return $this;
                }
                $injectBlock->setTemplate('ambanners/inject.phtml');
                $injectBlock->setParentBlock($block);
                $injectBlock->setPosition(14);
                $injectBlockHtml = $injectBlock->toHtml();

                /** @var Amasty_Banners_Block_Container $containerBlock */
                $containerBlock = Mage::getBlockSingleton('ambanners/container');
                $containerBlock->setPosition(14);
                $containerBlock->setTemplate('ambanners/container.phtml');
                $containerBlock->append($injectBlock);

                $containerBlockHtml = $containerBlock->toHtml();
                $isFirstSpan = '<span id="ambanner_first_time" style="display: none">1</span>';
                $html = $html . $isFirstSpan . $containerBlockHtml . $injectBlockHtml;

                $transport->setHtml($html);
            }
        }

        return $this;
    }
    
}
