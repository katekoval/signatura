<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Banners
 */

$this->startSetup();

$this->run("
    alter table `{$this->getTable('ambanners/rule')}`
    add column `after_product`  SMALLINT(5) NOT NULL DEFAULT 0;
");

$this->endSetup();
