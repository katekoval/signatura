<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Banners
 */

$this->startSetup();

$this->run("
    ALTER TABLE `{$this->getTable('ambanners/rule')}` ADD `countries` TEXT NOT NULL DEFAULT '';
    ALTER TABLE `{$this->getTable('ambanners/rule')}` ADD `brands` TEXT NOT NULL DEFAULT '';
");

$this->endSetup();
