<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Banners
 */
$this->startSetup();
$fieldsSql = 'SHOW COLUMNS FROM ' . $this->getTable('ambanners/rule');
$cols = $this->getConnection()->fetchCol($fieldsSql);

if (!in_array('show_on_products', $cols)){
    $this->run("ALTER TABLE `{$this->getTable('ambanners/rule')}` ADD COLUMN `show_on_products` TEXT DEFAULT NULL AFTER `cats`;");
}

if (!in_array('banner_type', $cols)){
    $this->run("ALTER TABLE `{$this->getTable('ambanners/rule')}` ADD COLUMN `banner_type` VARCHAR(16) NOT NULL DEFAULT 'image' COMMENT '0 - image, 1 - cms block, 2 - html page, 3 - products' AFTER `show_on_products`;");
}

if (!in_array('html_text', $cols)){
    $this->run("ALTER TABLE `{$this->getTable('ambanners/rule')}` ADD COLUMN `html_text` TEXT DEFAULT NULL AFTER `banner_type`;");
}

if (in_array('name', $cols)){
    $this->run("ALTER TABLE `{$this->getTable('ambanners/rule')}` CHANGE COLUMN `name` `rule_name` VARCHAR(255) NOT NULL DEFAULT '';");
}

$this->run("
 	ALTER TABLE `{$this->getTable('ambanners/rule')}` MODIFY COLUMN `from_date` DATETIME DEFAULT NULL,
	 MODIFY COLUMN `to_date` DATETIME DEFAULT NULL;
    
    CREATE TABLE IF NOT EXISTS `{$this->getTable('ambanners/rule_products')}` (
  		`id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
  		`rule_id` INTEGER UNSIGNED NOT NULL DEFAULT 0,
  		`product_id` BIGINT UNSIGNED NOT NULL DEFAULT 0,
  		 PRIMARY KEY(`id`)
	)
	ENGINE = InnoDB;
");

$this->endSetup();
