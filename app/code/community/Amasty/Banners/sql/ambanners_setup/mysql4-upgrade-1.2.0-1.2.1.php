<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Banners
 */

$this->startSetup();

$this->run("
    alter table `{$this->getTable('ambanners/rule')}`
    add column `after_n_product_row`  SMALLINT(5) NOT NULL DEFAULT 0;

    alter table `{$this->getTable('ambanners/rule')}`
    add column `n_product_width`  SMALLINT(5) NOT NULL DEFAULT 2;
");

$this->endSetup();