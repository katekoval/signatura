<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Banners
 */

$this->startSetup();

$this->run("
    UPDATE `{$this->getTable('ambanners/rule')}` SET `cats` = TRIM(BOTH ',' FROM `cats`);
");

$this->endSetup();
