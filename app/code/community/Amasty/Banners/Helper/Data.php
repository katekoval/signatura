<?php
/**
 * @author Amasty Team
 * @copyright Copyright (c) 2020 Amasty (https://www.amasty.com)
 * @package Amasty_Banners
 */ 
class Amasty_Banners_Helper_Data extends Mage_Core_Helper_Abstract
{
    const ERROR_MESSAGE = 'If there is the following text it means that Amasty_Base is not updated to the latest 
                             version.<p>In order to fix the error, please, download and install the latest version of 
                             the Amasty_Base, which is included in all our extensions.
                        <p>If some assistance is needed, please submit a support ticket with us at: 
                        <a href="https://amasty.com/contacts/" target="_blank">https://amasty.com/contacts/</a>';

    public function getAllGroups()
    {
        $customerGroups = Mage::getResourceModel('customer/group_collection')
            ->load()->toOptionArray();

        $found = false;
        foreach ($customerGroups as $group) {
            if ($group['value']==0) {
                $found = true;
            }
        }
        if (!$found) {
            array_unshift($customerGroups, array('value'=>0, 'label'=>Mage::helper('salesrule')->__('NOT LOGGED IN')));
        } 
        
        return $customerGroups;
    }

    public function getStatuses()
    {
        return array(
            '0' => $this->__('Inactive'),
            '1' => $this->__('Active'),
        );       
    }

    public function getPosition()
    {
        $a = array(
            Amasty_Banners_Model_Rule::POS_ABOVE_CART => $this->__('Above cart'),
            Amasty_Banners_Model_Rule::POS_BELOW_CART => $this->__('Below cart'),
            Amasty_Banners_Model_Rule::POS_SIDEBAR_RIGHT => $this->__('Sidebar-Right'),
            Amasty_Banners_Model_Rule::POS_SIDEBAR_LEFT => $this->__('Sidebar-Left'),
            Amasty_Banners_Model_Rule::POS_PROD_PAGE  => $this->__('Product page (Top)'),
			Amasty_Banners_Model_Rule::POS_PROD_PAGE_BOTTOM => $this->__('Product Page (Bottom)'),
			Amasty_Banners_Model_Rule::POS_PROD_PAGE_BELOW_CART => $this->__('Product page (Near Cart Button)'),
            Amasty_Banners_Model_Rule::POS_CATEGORY_PAGE => $this->__('Category page (Top)'),
            Amasty_Banners_Model_Rule::POS_CATEGORY_PAGE_BOTTOM => $this->__('Category page (Bottom)'),
            Amasty_Banners_Model_Rule::POS_CHECKOUT_ABOVE_TOTAL => $this->__('Cart Page (Totals Sidebar)'),
            Amasty_Banners_Model_Rule::POS_CATALOG_SEARCH_TOP => $this->__('Catalog Search (Top)'),
            Amasty_Banners_Model_Rule::POS_TOP_PAGE => $this->__('On Top of Page'),
            Amasty_Banners_Model_Rule::POS_TOP_INDEX => $this->__('Home Page under Menu'),
            Amasty_Banners_Model_Rule::POS_AFTER_N_PRODUCT_ROW => $this->__('Among category products'),
            Amasty_Banners_Model_Rule::POS_PROD_PAGE_RIGHT => $this->__('Product page (Sidebar Right)'),
            Amasty_Banners_Model_Rule::POS_PROD_PAGE_LEFT => $this->__('Product page (Sidebar Left)'),
        );

        if ($this->needBrands()) {
            $a[Amasty_Banners_Model_Rule::BRAND_PAGE] = $this->__('Improved Navigation Brand Page (Top)');
        }

        return $a;
    }

    public function needBrands()
    {
        $needBrands = false;

        $brandCode = $this->getBrandCode();
        if (Mage::getConfig()->getModuleConfig('Amasty_Shopby')->is('active', 'true') && !empty($brandCode)) {
            $needBrands = true;
        }

        return $needBrands;
    }

    public function getBrandCode()
    {
        return Mage::getStoreConfig('amshopby/brands/attr');
    }

    public function getBrands()
    {
        $brandCode = Mage::getStoreConfig('amshopby/brands/attr');
        $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', $brandCode);
        $options = $attribute->getSource()->getAllOptions();

        return $options;
    }

    /**
     * @return array
     */
    public function getPositionMulti()
    {
        $pos = $this->getPosition();
        $result = array();
        foreach ($pos as $k => $v) {
            $result[] = array(
                "label" => $v,
                "value" => $k,
            );
        }
        return $result;
    }

	public function showProductsListOptions()
    {
    	return array(
    		Amasty_Banners_Model_Rule::SHOW_PRODUCTS_NO => $this->__('No'),
    		Amasty_Banners_Model_Rule::SHOW_PRODUCTS_YES => $this->__('Yes'),
    	);
    }
    
    public function getBannerTypes()
    {
    	return array(
    		Amasty_Banners_Model_Rule::TYPE_IMAGE => $this->__('Image'),
    		Amasty_Banners_Model_Rule::TYPE_CMS => $this->__('CMS Block'),
    		Amasty_Banners_Model_Rule::TYPE_HTML => $this->__('HTML text'),
    	);
    }
    
    public function getBlock()
    {
        $a = array(
            Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE
        );
        return $a;       
    }

    public function getCountries()
    {
        $countries = Mage::getModel('directory/country')->getCollection()->toOptionArray();

        unset($countries[0]);

        return $countries;
    }

    /**
     * @param string $string
     *
     * @return array|null
     */
    public function unserialize($string)
    {
        if (!@class_exists('Amasty_Base_Helper_String')) {
            Mage::logException(new Exception(self::ERROR_MESSAGE));

            if (Mage::app()->getStore()->isAdmin()) {
                Mage::helper('ambase/utils')->_exit(self::ERROR_MESSAGE);
            } else {
                Mage::throwException($this->__('Sorry, something went wrong. Please contact us or try again later.'));
            }
        }

        return \Amasty_Base_Helper_String::unserialize($string);
    }
}
