<?php

class Dixa_SearchAPI_SearchController extends Mage_Core_Controller_Front_Action {
  private function _verifyApikey($params) {
    return array_key_exists('apikey', $params) and $params['apikey'] === Mage::getStoreConfig('dixa_searchapi_options/security/api_field');
  }

  private function _filterParams() {
    $keys = Array(
      'email_address' => 'email',
      'phone_number'  => 'phone',
      'apikey'        => 'apikey',
      'country_code'  => 'country_code',
    );

    $filtered_params = Array();

    foreach ($this->getRequest()->getParams() as $key => $value) {
      if (array_key_exists($key, $keys) && !empty($value)) {
        $filtered_params[$keys[$key]] = $value;
      }
    }

    return $filtered_params;
  }

  private function _formatResponse($data, $code = 200) {
    $this->getResponse()->setHeader('HTTP/1.0', strval($code), true);
    $this->getResponse()->setHeader('Content-Type', 'application/json');
    $this->getResponse()->setHeader('X-Dixa-SearchAPI-Version', '0.3.0');
    if (401 != $code) {
      $this->getResponse()->setHeader('X-Dixa-Magento-Version', Mage::getVersion());
    }
    $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($data));
  }

  private function _formatQuantity($amount) {
    return rtrim(rtrim($amount, '0'), '.');
  }

  private function _notFound() {
    $this->_formatResponse(
      Array(
        'error' => Array(
          'message' => 'Not found',
          'code' => 404,
        )
      ),
      404
    );
  }

  private function _unauthorized() {
    $this->_formatResponse(
      Array(
        'error' => Array(
          'message' => 'Unauthorized',
          'code' => 401,
        )
      ),
      401
    );
  }

  private function _found($result) {
    $this->_formatResponse(
      Array(
        'results' => $result
      )
    );
  }

  private function _loadSelectedOrders($ids) {
    $orders = Mage::getModel('sales/order')
                -> getCollection()
                -> distinct(true)
                -> addFieldToFilter('entity_id', Array('in' => $ids));
    $orders     -> getSelect()
                -> order('main_table.created_at desc');

    return (0 !== $orders->getSize()) ? $orders : null;
  }

  private function _getOrdersByRawPhone($phone) {
    $orderIds = Mage::getModel('sales/order_address')
                  -> getCollection()
                  -> distinct(true)
                  -> addFieldToFilter('telephone', Array('eq' => $phone))
                  -> getColumnValues('parent_id');

    return $this->_loadSelectedOrders($orderIds);
  }

  private function _getOrdersByFuzzyPhone($phone) {
    $charFilter = array('+', ' ', '(', ')', '-');
    $phone = str_replace($charFilter, '', $phone);

    $sqlFilter = 'telephone';
    foreach ($charFilter as $_char) {
      $sqlFilter = sprintf('REPLACE(%s, "%s", "")', $sqlFilter, $_char);
    }

    $addressCollection = Mage::getModel('sales/order_address')
                           -> getCollection()
                           -> distinct(true)
                           -> addAttributeToSelect('parent_id');
    $addressCollection     -> getSelect()
                           -> where("$sqlFilter = ?", $phone);

    $orderIds = $addressCollection->getColumnValues('parent_id');

    return $this->_loadSelectedOrders($orderIds);
  }

  private function _getOrdersByEmail($email) {
    $orders = Mage::getModel('sales/order')
                -> getCollection()
                -> addAttributeToFilter('customer_email', $email);
    $orders     -> getSelect()
                -> order('main_table.created_at desc');

    return (0 !== $orders->getSize()) ? $orders : null;
  }

  private function _formatCustomer($order) {
    $customer = Mage::getModel('customer/customer')->load($order->getCustomerId());

    $formatted = Array(
      'id'            => $customer->getId(),
      'name'          => $customer->getName(),
      'created'       => date('c', $customer->getCreatedAtTimestamp()),
      'email_address' => $customer->getEmail(),
    );

    $address = $customer->getDefaultBillingAddress();

    if (false !== $address) {
      $formatted['phone_number'] = $address->getTelephone();
      $formatted['address']      = $this->_formatAddress($address);
    }

    return $formatted;
  }

  private function _formatAddress($address) {
    if (false !== $address) {
      return Array(
        'street1'   => $address->getStreet()[0],
        'street2'   => count($address->getStreet()) > 1 ? $address->getStreet()[1] : null,
        'postcode'  => $address->getPostcode(),
        'formatted' => $address->getFormated(),
        'country'   => $address->getCountry(),
        'city'      => $address->getCity(),
      );
    }
  }

  private function _getOrderUrl($order) {
    return Mage::helper('adminhtml')->getUrl(
      'adminhtml/sales_order/view',
      Array('order_id' => $order->getEntityId())
    );
  }

  private function _formatItem($item) {
    $formatted = Array(
      'id'         => $item->getProductId(),
      'sku'        => $item->getSku(),
      'quantities' => Array(
          'ordered'  => $this->_formatQuantity($item->getQtyOrdered()),
          'shipped'  => $this->_formatQuantity($item->getQtyShipped()),
          'invoiced' => $this->_formatQuantity($item->getQtyInvoiced()),
          'refunded' => $this->_formatQuantity($item->getQtyRefunded())
      ),
      'weight'     => $item->getWeight(),
      'name'       => $item->getName(),
      'price'      => Mage::helper('core')->currency($item->getPrice(), true, false),
      'currency'   => $item->getOrder()->getBaseCurrencyCode(),
      'url'        => $item->getProduct()->getUrlInStore(),
      'status'     => $item->getStatus(),
    );

    return $formatted;
  }

  private function _formatItems($order) {
    $items = $order->getAllVisibleItems();
    $formatted = Array();

    foreach ($items as $item) {
      $formatted[] = $this->_formatItem($item);
    }

    return $formatted;
  }

  private function _formatOrder($order) {
    $formatted = Array(
      'status'           => $order->getStatusLabel(),
      'created'          => date('c', strtotime($order->getCreatedAt())),
      'id'               => $order->getRealOrderId(),
      'totals'           => Array(
        'subtotal'    => Mage::helper('core')->currency($order->getSubtotal(), true, false),
        'grand_total' => Mage::helper('core')->currency($order->getGrandTotal(), true, false),
        'tax'         => Mage::helper('core')->currency($order->getTaxAmount(), true, false),
        'shipping'    => Mage::helper('core')->currency($order->getShippingAmount(), true, false)
      ),
      'currency'         => $order->getBaseCurrencyCode(),
      'tracking_numbers' => $order->getTrackingNumbers(),
      'url'              => $this->_getOrderUrl($order),
      'items'            => $this->_formatItems($order),
      'store'            => $order->getStoreName(),
      'customer'         => $this->_formatCustomer($order),
      'shipping'         => $order->getShippingDescription(),
    );

    $billing_address  = $order->getBillingAddress();
    $shipping_address = $order->getShippingAddress();

    if (false !== $billing_address) {
      $formatted['billing_address'] = $this->_formatAddress($billing_address);
    }

    if (false !== $shipping_address) {
      $formatted['shipping_address'] = $this->_formatAddress($shipping_address);
    }

    return $formatted;
  }

  private function _formatOrders($orders) {
    $formatted = Array();

    foreach ($orders as $order) {
      $formatted[] = $this->_formatOrder($order);
    }

    return $formatted;
  }

  public function ordersAction() {
    $params = $this->_filterParams();

    if (!$this->_verifyApikey($params)) {
      $this->_unauthorized();
      return;
    }

    $orders = null;

    if (array_key_exists('phone', $params)) {
      $numbers = Array($params['phone']);

      if (array_key_exists('country_code', $params)) {
        $numbers[] = str_replace("+{$params['country_code']}", '', $params['phone']);
      }

      foreach ($numbers as $number) {
        $orders = $this->_getOrdersByRawPhone($number);

        if (null === $orders) {
          $orders = $this->_getOrdersByFuzzyPhone($number);
        }

        if (null !== $orders) {
          break;
        }
      }
    }

    if (null === $orders && array_key_exists('email', $params)) {
      $orders = $this->_getOrdersByEmail($params['email']);
    }

    if (null === $orders) {
      $this->_notFound();
    }

    else {
      $orders = $this->_formatOrders($orders);
      $this->_found($orders);
    }
  }
}

?>
