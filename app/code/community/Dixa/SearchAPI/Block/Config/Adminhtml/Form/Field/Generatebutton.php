<?php
class Dixa_SearchAPI_Block_Config_Adminhtml_Form_Field_Generatebutton extends Mage_Adminhtml_Block_System_Config_Form_Field
{
  private function _makeGenerateButton() {
    $buttonBlock = $this->getLayout()->createBlock('adminhtml/widget_button');

    $data = Array(
      'label'   => Mage::helper('Dixa_SearchAPI')->__('Generate'),
      'onclick' => "function dixa_makeid() { var text = ''; var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'; for (var i = 0; i < 64; i++) text += possible.charAt(Math.floor(Math.random() * possible.length)); return text; }; $('dixa_searchapi_options_security_api_field').value = dixa_makeid();",
      'class'   => '',
    );

    $html = $buttonBlock->setData($data)->toHtml();
    return $html;
  }

  private function _makeCopyClipboardButton() {
    $buttonBlock = $this->getLayout()->createBlock('adminhtml/widget_button');

    $data = Array(
      'label'   => Mage::helper('Dixa_SearchAPI')->__('Copy to clipboard'),
      'onclick' => "function dixa_cb() { var target = document.getElementById('dixa_searchapi_options_security_api_field'); var originalFocus = document.activeElement; target.focus(); target.setSelectionRange(0, target.value.length); try { document.execCommand('copy'); if (originalFocus && typeof originalFocus.focus === 'function') { originalFocus.focus(); } return true; } catch(e) { return false; } }; dixa_cb();",
      'class'   => '',
    );

    $html = $buttonBlock->setData($data)->toHtml();
    return $html;
  }

  private function _makeSeparator() {
    return '<span style="display:inline; margin-left: 5px;"></span>';
  }

  protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {
    return $this->_makeGenerateButton() . $this->_makeSeparator() . $this->_makeCopyClipboardButton();
  }
}

?>
