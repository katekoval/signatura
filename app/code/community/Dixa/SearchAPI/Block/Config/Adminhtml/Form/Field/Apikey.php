<?php
class Dixa_SearchAPI_Block_Config_Adminhtml_Form_Field_Apikey extends Mage_Adminhtml_Block_System_Config_Form_Field
{
  protected function _getElementHtml(Varien_Data_Form_Element_Abstract $element) {
    $element->setReadonly('readonly');
    $element->setStyle('width: 500px;');
    $element->setValue(Mage::getStoreConfig('dixa_searchapi_options/security/api_field'));
    return parent::_getElementHtml($element);
  }
}

?>
