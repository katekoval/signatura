<?php
/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
if(class_exists('Mage_Rule_Model_Condition_Product_Abstract', false)) {
    require_once 'Webcooking/All/Model/Rule/Condition/ProductCompatibility/Mage_Rule_Model_Condition_Product_Abstract.php';
} else {
    require_once 'Webcooking/All/Model/Rule/Condition/ProductCompatibility/Webcooking_All_Model_Rule_Condition_Product_Abstract.php';
}
class Webcooking_All_Model_Rule_Condition_Product extends Webcooking_All_Model_Rule_Condition_Product_Compatibility {

   /**
     * Add special attributes
     *
     * @param array $attributes
     */
    protected function _addSpecialAttributes(array &$attributes)
    {
        parent::_addSpecialAttributes($attributes);
        $attributes['type_id'] = Mage::helper('catalogrule')->__('Product Type');
        $attributes['in_promo'] = Mage::helper('catalogrule')->__('In Promo ?');
        $attributes['is_new'] = Mage::helper('catalogrule')->__('Is New ?');
    }

    /**
     * Validate Product Rule Condition
     *
     * @param Varien_Object $object
     *
     * @return bool
     */
    public function validate(Varien_Object $object)
    {
        /** @var Mage_Catalog_Model_Product $product */
        $product = $object->getProduct();
        if (!($product instanceof Mage_Catalog_Model_Product)) {
            $product = Mage::getModel('catalog/product')->load($object->getProductId());
        }

        $product
            ->setQuoteItemQty($object->getQty())
            ->setQuoteItemPrice($object->getPrice()) // possible bug: need to use $object->getBasePrice()
            ->setQuoteItemRowTotal($object->getBaseRowTotal());

        $valid = parent::validate($object);
        if (!$valid && $product->getTypeId() == Mage_Catalog_Model_Product_Type_Configurable::TYPE_CODE) {
            $children = $object->getChildren();
            if (is_array($children) and isset($children[0])) {
                $child = $children[0];

                /** @var Mage_Catalog_Model_Product $childProduct */
                $childProduct = Mage::getModel('catalog/product')->load($child->getProductId());
                $childProduct
                    ->setQuoteItemQty($object->getQty())
                    ->setQuoteItemPrice($object->getPrice())
                    ->setQuoteItemRowTotal($object->getBaseRowTotal());

                if (!$childProduct->isVisibleInSiteVisibility()) {
                    $childProduct->setCategoryIds($product->getCategoryIds());
                }

                $valid = parent::validate($childProduct);
            }
        }

        return $valid;
    }
    
    
    public function getDefaultOperatorInputByType()
    {
        if (null === $this->_defaultOperatorInputByType) {
            $this->_defaultOperatorInputByType = array(
                'string'      => array('empty', '!empty', '==', '!=', '>=', '>', '<=', '<', '{}', '!{}', '()', '!()'),
                'numeric'     => array('empty', '!empty', '==', '!=', '>=', '>', '<=', '<', '()', '!()'),
                'date'        => array('empty', '!empty', '==', '>=', '<='),
                'select'      => array('empty', '!empty', '==', '!='),
                'boolean'     => array('empty', '!empty', '==', '!='),
                'multiselect' => array('empty', '!empty', '{}', '!{}', '()', '!()'),
                'grid'        => array('empty', '!empty', '()', '!()'),
                'category'    => array('empty', '!empty', '==', '!=', '{}', '!{}', '()', '!()')
            );
            $this->_arrayInputTypes = array('multiselect', 'grid', 'category');
        }
        return $this->_defaultOperatorInputByType;
    }
    
    public function getDefaultOperatorOptions()
    {
        if (null === $this->_defaultOperatorOptions) {
            $this->_defaultOperatorOptions = array(
                '=='      => Mage::helper('rule')->__('is'),
                '!='      => Mage::helper('rule')->__('is not'),
                '>='      => Mage::helper('rule')->__('equals or greater than'),
                '<='      => Mage::helper('rule')->__('equals or less than'),
                '>'       => Mage::helper('rule')->__('greater than'),
                '<'       => Mage::helper('rule')->__('less than'),
                '{}'      => Mage::helper('rule')->__('contains'),
                '!{}'     => Mage::helper('rule')->__('does not contain'),
                '()'      => Mage::helper('rule')->__('is one of'),
                '!()'     => Mage::helper('rule')->__('is not one of'),
                'empty'   => Mage::helper('rule')->__('is empty'),
                '!empty'  => Mage::helper('rule')->__('is not empty'),
            );
        }
        return $this->_defaultOperatorOptions;
    }
    
    
    public function getFieldCondition($attribute, $field, $operator, $value, $not = false) {
        switch ($operator) {
                case '!=':
                case '>=':
                case '<=':
                case '>':
                case '<':
                    if(is_array($value)) {
                        $results = array();
                        foreach($value as $v) {
                            $results[] = $this->getFieldCondition($attribute, $field, $operator, $v);
                        }
                        if(empty($results)) {
                            return '1=1';
                        }
                        return '(' . ($not?implode(' OR ', $results):implode(' AND ', $results)) . ')';
                    } 
                    $selectOperator = sprintf('%s ?', $operator);
                    return $this->getAdapter()->quoteInto("{$field}{$selectOperator}", is_numeric($value)?floatval($value):$value);
                case '{}':
                case '!{}':
                    if(preg_match('%category_id%', $field) || is_array($value)) {
                        return $this->getFieldCondition($attribute, $field, str_replace(array('{', '}'), array('(', ')'), $operator), $value, $not);
                    } else {
                        $selectOperator = ' LIKE ?';
                        $value          = '%' . $value . '%';
                    }
                    if (substr($operator, 0, 1) == '!') {
                        $selectOperator = ' NOT' . $selectOperator;
                    }
                    /*if(is_array($value)) {
                        $results = array();
                        foreach($value as $v) {
                            $results[] = $this->getFieldCondition($attribute, $field, $operator, $v);
                        }
                        return '(' . ($not?implode(' OR ', $results):implode(' AND ', $results)) . ')';
                    }*/
                    return $this->getAdapter()->quoteInto("{$field}{$selectOperator}", $value);

                case '()':
                    if(!is_array($value)) {
                        $value = explode(',', $value);
                        $value = array_filter($value);
                    }
                    if($attribute->getFrontendInput() == 'multiselect' && is_array($value)) {
                        $orParts = array();
                        foreach($value as $val) {
                            $orParts[] = $this->getAdapter()->quoteInto(" FIND_IN_SET(?, {$field})", $val);
                        }
                        if(empty($orParts)) {
                            return '1=1';
                        }
                        return "(" . implode(' OR ', $orParts) . ")";
                    } else {
                        $selectOperator = ' IN(?)';
                        return $this->getAdapter()->quoteInto("{$field}{$selectOperator}", $value);
                    }

                case '!()':
                    if(!is_array($value)) {
                        $value = explode(',', $value);
                    }
                    $selectOperator = ' NOT IN(?)';
                    return $this->getAdapter()->quoteInto("{$field}{$selectOperator}", $value);

                case 'empty':
                    if($not) {
                        return $this->getAdapter()->quoteInto("({$field} IS NOT NULL AND {$field} != '' AND {$field} != 'no_selection')", array());
                    }
                    return $this->getAdapter()->quoteInto("({$field} IS NULL OR {$field} = '' OR {$field} = 'no_selection')", array());

                case '!empty':
                    if($not) {
                        return $this->getAdapter()->quoteInto("({$field} IS NULL OR {$field} = '' OR {$field} = 'no_selection')", array());
                    }
                    return $this->getAdapter()->quoteInto("({$field} IS NOT NULL AND {$field} != '' AND {$field} != 'no_selection')", array());
            }
            
            if(is_array($value)) {
                $results = array();
                foreach($value as $v) {
                    $results[] = $this->getFieldCondition($attribute, $field, $operator, $v);
                }
                if(empty($results)) {
                    return '1=1';
                }
                return '(' . ($not?implode(' OR ', $results):implode(' AND ', $results)) . ')';
            } 
            return $this->getAdapter()->quoteInto("{$field} = ?", $value);
    }
    
    public function getAdapter() {
        if(!$this->hasData('adapter')) {
            $this->setData('adapter', Mage::getSingleton('core/resource')->getConnection(Mage_Core_Model_Resource::DEFAULT_READ_RESOURCE));
        }
        return $this->getData('adapter');
    }
    
    public function getIsNewConditionForProductCollection($productCollection) {
        $fromPart = $productCollection->getSelect()->getPart(Zend_Db_Select::SQL_FROM);
        $attributesNeeded = array('news_from_date', 'news_to_date');
        foreach($attributesNeeded as $attributeCode) {
            if(!isset($fromPart['at_'.$attributeCode])) {
                $productCollection->joinAttribute($attributeCode, 'catalog_product/'.$attributeCode, 'entity_id', null, 'left');
            }
        }
        if($this->getValue()) {
            $conditions = array();
            $conditions[] = "( at_news_from_date.value < '" . date('Y-m-d 00:00:00') ."' )";
            $conditions[] = "( at_news_to_date.value > '" . date('Y-m-d 23:59:59') ."' )";
            $condition = implode(' AND ', $conditions);
        } else {
            $conditions = array();
            $conditions[] = "( at_news_from_date.value IS NULL )";
            $conditions[] = "( at_news_from_date.value = '' )";
            $conditions[] = "( at_news_to_date.value IS NULL )";
            $conditions[] = "( at_news_to_date.value = '' )";
            $conditions[] = "( at_news_from_date.value > '" . date('Y-m-d 23:59:59') ."' )";
            $conditions[] = "( at_news_to_date.value < '" . date('Y-m-d 00:00:00') ."' )";
            $condition = implode(' OR ', $conditions);
        }
        return new Zend_Db_Expr($condition);
    }
    
    public function getInPromoConditionForProductCollection($productCollection) {
        $fromPart = $productCollection->getSelect()->getPart(Zend_Db_Select::SQL_FROM);
        $attributesNeeded = array('price', 'special_price', 'special_from_date', 'special_to_date');
        foreach($attributesNeeded as $attributeCode) {
            if(!isset($fromPart['at_'.$attributeCode])) {
                $productCollection->joinAttribute($attributeCode, 'catalog_product/'.$attributeCode, 'entity_id', null, 'left');
            }
        }
        if($this->getValue()) {
            $conditions = array();
            $conditions[] = "( at_special_price.value IS NOT NULL )";
            $conditions[] = "( at_special_price.value < at_price.value )";
            $conditions[] = "( at_special_from_date.value IS NULL OR at_special_from_date.value <= '" . date('Y-m-d 23:59:59') ."' )";
            $conditions[] = "( at_special_to_date.value IS NULL OR at_special_to_date.value >= '" . date('Y-m-d 00:00:00') ."' )";
            $condition = implode(' AND ', $conditions);
        } else {
            $conditions = array();
            $conditions[] = "( at_special_price.value IS NULL )";
            $conditions[] = "( at_special_price.value = '' )";
            $conditions[] = "( at_special_price.value >= at_price.value )";
            $conditions[] = "( at_special_from_date.value > '" . date('Y-m-d 23:59:59') ."' )";
            $conditions[] = "( at_special_to_date.value < '" . date('Y-m-d 00:00:00') ."' )";
            $condition = implode(' OR ', $conditions);
        }
        return new Zend_Db_Expr($condition);
    }
    
    public function getConditionForProductCollection($productCollection)
    {
        $attributeCode  = $this->getAttribute();
        
        if($attributeCode == 'in_promo') {
            return $this->getInPromoConditionForProductCollection($productCollection);
        }
        if($attributeCode == 'is_new') {
            return $this->getIsNewConditionForProductCollection($productCollection);
        }
        $attribute      = Mage::helper('wcooall/attribute')->getAttribute($attributeCode);
        $operator       = $this->getOperator();
        $value          = $this->getValue();
        
        $condition = '';
        
        
        if($attributeCode =='category_ids') {
            $fromPart = $productCollection->getSelect()->getPart(Zend_Db_Select::SQL_FROM);
            if(!isset($fromPart['ccp'])) {
                $productCollection->getSelect()
                        ->joinLeft(
                                array('ccp' => $productCollection->getTable('catalog/category_product')), 
                                'ccp.product_id = e.entity_id', 
                                array('categories' => new Zend_Db_Expr('GROUP_CONCAT(ccp.category_id)'))
                );
            }
            $attributeCode = 'ccp.category_id';
            $field = $this->getAdapter()->quoteIdentifier($attributeCode);
            $condition = $this->getFieldCondition($attribute, $field, $operator, $value);
        } elseif($attributeCode == 'attribute_set_id' || $attributeCode == 'type_id') {
            $field = $this->getAdapter()->quoteIdentifier($attributeCode);
            $condition = $this->getFieldCondition($attribute, $field, $operator, $value);
        } elseif($attribute && $attribute->getId()) {
            $fromPart = $productCollection->getSelect()->getPart(Zend_Db_Select::SQL_FROM);
            if(!$attribute->isStatic()) {
                
                //$productCollection->addAttributeToSelect($attributeCode, 'left');
                if(!isset($fromPart['at_'.$attributeCode])) {
                    $productCollection->joinAttribute($attributeCode, 'catalog_product/'.$attributeCode, 'entity_id', null, 'left');
                }
                
                //Mage::log('add to select'.$attributeCode, null, 'debugdyncat.log', true);
            }
            
            $fromPart = $productCollection->getSelect()->getPart(Zend_Db_Select::SQL_FROM);
            if(!$attribute->isStatic() || isset($fromPart['at_'.$attributeCode])) {
                $expr = 'at_'.$attributeCode.'.value';
                if(isset($fromPart['at_'.$attributeCode.'_default'])) {
                    $expr = new Zend_Db_Expr('IF(at_'.$attributeCode.'.value_id > 0, at_'.$attributeCode.'.value, at_'.$attributeCode.'_default.value)');
                }
                $field = $this->getAdapter()->quoteIdentifier($expr);
            } else if($attribute->isStatic() && isset($fromPart['e'])) {
                $attributeCode = 'e.'.$attributeCode;
                $field = $this->getAdapter()->quoteIdentifier($attributeCode);
            } 
                    

            $condition = $this->getFieldCondition($attribute, $field, $operator, $value);

        }
    
        if(!$condition) {
            $condition = '(1 = 1)';
        }
       
        return new Zend_Db_Expr($condition);
    }
    
    
    protected function _prepareValueOptions()
    {
        // Check that both keys exist. Maybe somehow only one was set not in this routine, but externally.
        $selectReady = $this->getData('value_select_options');
        $hashedReady = $this->getData('value_option');
        if ($selectReady && $hashedReady) {
            return $this;
        }

        // Get array of select options. It will be used as source for hashed options
        $selectOptions = null;
        if ($this->getAttribute() === 'attribute_set_id') {
            $entityTypeId = Mage::getSingleton('eav/config')
                ->getEntityType(Mage_Catalog_Model_Product::ENTITY)->getId();
            $selectOptions = Mage::getResourceModel('eav/entity_attribute_set_collection')
                ->setEntityTypeFilter($entityTypeId)
                ->load()
                ->toOptionArray();
        } else if ($this->getAttribute() === 'type_id') {
            $selectOptions = Mage::getSingleton('catalog/product_type')->getOptionArray();
        } else if(in_array($this->getAttribute(), array('in_promo', 'is_new'))) {
            $selectOptions = Mage::getModel('adminhtml/system_config_source_yesno')
                        ->toOptionArray();
        } else if (is_object($this->getAttributeObject())) {
            $attributeObject = $this->getAttributeObject();
            if ($attributeObject->usesSource()) {
                if ($attributeObject->getFrontendInput() == 'multiselect') {
                    $addEmptyOption = false;
                } else {
                    $addEmptyOption = true;
                }
                $selectOptions = $attributeObject->getSource()->getAllOptions($addEmptyOption);
            }
        }

        // Set new values only if we really got them
        if ($selectOptions !== null) {
            // Overwrite only not already existing values
            if (!$selectReady) {
                $this->setData('value_select_options', $selectOptions);
            }
            if (!$hashedReady) {
                $hashedOptions = array();
                foreach ($selectOptions as $o) {
                    if (is_array($o['value'])) {
                        continue; // We cannot use array as index
                    }
                    $hashedOptions[$o['value']] = $o['label'];
                }
                $this->setData('value_option', $hashedOptions);
            }
        }

        return $this;
    }
    
    
    public function getInputType()
    {
        if (in_array($this->getAttribute(), array('attribute_set_id', 'type_id'))) {
            return 'select';
        }
        if(in_array($this->getAttribute(), array('in_promo', 'is_new'))) {
            return 'boolean';
        }
        if (!is_object($this->getAttributeObject())) {
            return 'string';
        }
        if ($this->getAttributeObject()->getAttributeCode() == 'category_ids') {
            return 'category';
        }
        switch ($this->getAttributeObject()->getFrontendInput()) {
            case 'select':
                return 'select';

            case 'multiselect':
                return 'multiselect';

            case 'date':
                return 'date';

            case 'boolean':
                return 'boolean';

            default:
                return 'string';
        }
    }

    /**
     * Retrieve value element type
     *
     * @return string
     */
    public function getValueElementType()
    {
        if (in_array($this->getAttribute(), array('attribute_set_id', 'type_id', 'in_promo', 'is_new'))) {
            return 'select';
        }
        if (!is_object($this->getAttributeObject())) {
            return 'text';
        }
        switch ($this->getAttributeObject()->getFrontendInput()) {
            case 'select':
            case 'boolean':
                return 'select';

            case 'multiselect':
                return 'multiselect';

            case 'date':
                return 'date';

            default:
                return 'text';
        }
    }
    
    
    
    public function loadAttributeOptions()
    {   
        Mage::getSingleton('eav/config')->clear();
        $productAttributes = Mage::getResourceSingleton('catalog/product')
            ->loadAllAttributes()
            ->getAttributesByCode();

        
        $attributes = array();
        foreach ($productAttributes as $attribute) {
            /* @var $attribute Mage_Catalog_Model_Resource_Eav_Attribute */
            if (!$attribute->getIsVisible() || 
                    ($this->getRestrictAttributes() && (!$attribute->isAllowedForRuleCondition()
                ||  !$attribute->getDataUsingMethod($this->_isUsedForRuleProperty)))
            ) {
                continue;
            }
            $attributes[$attribute->getAttributeCode()] = $attribute->getFrontendLabel();
        }

        $this->_addSpecialAttributes($attributes);

        asort($attributes);
        $this->setAttributeOption($attributes);

        return $this;
    }
    
    
    public function getRestrictAttributes() {
        return $this->hasData('restrict_attributes') ? $this->getData('restrict_attributes') : true;
    }
    
    
}
