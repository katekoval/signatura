<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_All_Model_Cron_Collection extends Varien_Data_Collection
{
    
    public function loadData($printQuery = false, $logQuery = false)
    {
         if ($this->isLoaded()) {
            return $this;
        }
        $cronJobs = Mage::getConfig()->getNode('crontab/jobs');
        if(empty($cronJobs)) {
            return $this;
        }
        $cronJobsArray = $cronJobs->asArray();

        
        // calculate totals
        $this->_totalRecords = count($cronJobsArray);
        $this->_setIsLoaded();
        
        

        // paginate and add items
        $from = ($this->getCurPage() - 1) * $this->getPageSize();
        $to = $from + $this->getPageSize() - 1;
        $isPaginated = $this->getPageSize() > 0;

        $cnt = 0;
        foreach($cronJobsArray as $identifier => $detail){
            $cnt++;
            if ($isPaginated && ($cnt < $from || $cnt > $to)) {
                continue;
            }
            $item = new Varien_Object();
            $item->setIdentifier($identifier);

            if(isset($detail['schedule']['cron_expr'])){
                $item->setCronExpr($detail['schedule']['cron_expr']);
            }
            if(isset($detail['run']['model'])){
                $item->setModel($detail['run']['model']);
            }
            $this->addItem($item);
            if (!$item->hasId()) {
                $item->setId($cnt);
            }
        }
        return $this;
    }

    public function setOrder($field, $direction = 'desc')
    {
        $direction = strtolower($direction)=='desc' ? -1 : 1;
        $this->_orders = array($field, $direction);
        return $this;
    }

    protected function _checkCondition($item)
    {
        foreach ($this->_filters as $field => $condition) {
            if (is_array($condition)) {
                if (isset($condition['from']) || isset($condition['to'])) {
                    if (isset($condition['from']) && $item->getData($field) < $condition['from']) {
                        return false;
                    }
                    if (isset($condition['to']) && $item->getData($field) > $condition['to']) {
                         return false;
                    }
                }
                elseif (!empty($condition['eq']) && $item->getData($field) != $condition['eq']) {
                    return false;
                }
                elseif (!empty($condition['neq']) && $item->getData($field) == $condition['neq']) {
                    return false;
                }
                elseif (!empty($condition['like']) && strpos($item->getData($field), trim($condition['like'], '%')) === false) {
                    return false;
                }
                elseif (!empty($condition['nlike']) && strpos($item->getData($field), trim($condition['nlike'], '%')) !== false) {
                    return false;
                }
                elseif (!empty($condition['in'])) {
                    $values = $condition['in'];
                    if(!is_array($values)) {
                        $values =  array($values);
                    }
                    if(!in_array($item->getData($field), $values)) {
                        return false;
                    }
                }
                elseif (!empty($condition['nin'])) {
                    $values = $condition['in'];
                    if(!is_array($values)) {
                        $values =  array($values);
                    }
                    if(in_array($item->getData($field), $values)) {
                        return false;
                    }
                }
            } else if($item->getData($field) != $condition) {
                return false;
            }
        }

        return true;
    }
    
    

    public function addFieldToFilter($fieldName, $condition)
    {
        $this->_filters[$fieldName] = $condition;
        return $this;
    }
}
