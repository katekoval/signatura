<?php
/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */

class Webcooking_All_Model_Cron  {

   public function executeTasks() {
       $startAt = time();
       $taskCollection = Mage::getModel('wcooall/task')->getCollection();
       $taskCollection->addFieldToFilter('status', Mage_Cron_Model_Schedule::STATUS_PENDING);
       $taskCollection->addFieldToFilter('scheduled_at', array('lteq'=>Mage::getSingleton('core/date')->date('Y-m-d H:i:s')));
       $taskCollection->getSelect()->order('priority ASC')->order('scheduled_at ASC');
       $taskCollection->getSelect()->limit(1000);
       foreach($taskCollection as $task) {
           if(time() - $startAt >= 60*10) {
               break;
           }
           $task->execute();
       }
   }
   
   
   public function cleanupTasks() {
       $taskCollection = Mage::getModel('wcooall/task')->getCollection();
       $taskCollection->addFieldToFilter('finished_at', array('lteq'=>date('Y-m-d H:i:s', strtotime('-15 days'))));
       $taskCollection->getSelect()->limit(1000);
       $taskCollection->walk('delete');
       if($taskCollection->getSize() > 0) {
           $this->cleanupTasks();
       }
       
       
       // crashed Tasks
       $taskCollection = Mage::getModel('wcooall/task')->getCollection();
       $taskCollection->addFieldToFilter('executed_at', array('lteq'=>date('Y-m-d H:i:s', strtotime('-2 month'))));
       $taskCollection->getSelect()->limit(1000);
       $taskCollection->walk('delete');
       if($taskCollection->getSize() > 0) {
           $this->cleanupTasks();
       }
   }
   
   
}
