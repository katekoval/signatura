<?php
/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */

class Webcooking_All_Model_Task extends Mage_Core_Model_Abstract {

    
    const MODEL_PATTERN = "@)`_')=@ ";
    
    protected $_params = null;
    
    public function _construct() {
        parent::_construct();
        $this->_init('wcooall/task');
    }
    
    
     protected function _beforeSave() {
        parent::_beforeSave();
        $date = Mage::getSingleton('core/date')->date('Y-m-d H:i:s');
        if(!$this->getId()) {
            $this->setCreatedAt($date);
        }
        if(is_array($this->getParams())) {
            try {
                $this->setParams($this->serializeParams($this->getParams()));
            } catch(Exception $e) {
                Mage::logException($e);
            }
        }
        return $this;
    }
    
    
    public function serializeParams($params) {
        foreach($params as $key => $value) {
            if(is_object($value) && is_subclass_of($value, 'Mage_Core_Model_Abstract', false) && $value->getId()) {
                $params[$key] = self::MODEL_PATTERN . get_class($value) . '::' . $value->getId();
            }
        }
        return serialize($params);
    }
    
    public function getUnserializedParams() {
        if(is_null($this->_params)) {
            $this->_params = unserialize($this->getParams());
            foreach($this->_params as $key => $value) {
                if(is_string($value) && preg_match('%^'.preg_quote(self::MODEL_PATTERN).'(.*)::([0-9]+)$%', $value, $pregResult)) {
                    $this->_params[$key] = Mage::getModel($pregResult[1])->load($pregResult[2]);
                }
            }
        }
        return $this->_params;
    }
    
    public function execute() {
        try {
            $this->setExecutedAt(Mage::getSingleton('core/date')->date('Y-m-d H:i:s'))->setStatus(Mage_Cron_Model_Schedule::STATUS_RUNNING)->save();

            $methodParts = explode('/', $this->getMethod());
            $classType = array_shift($methodParts);
            $method = array_pop($methodParts);
            $class = implode('/', $methodParts);
            $params = $this->getUnserializedParams();
            switch($classType) {
                case 'resource':
                    $object = Mage::getResourceModel($class);
                    break;
                case 'singleton':
                    $object = Mage::getSingleton($class);
                    break;
                case 'helper':
                    $object = Mage::helper($class);
                    break;
                case 'model':
                default:
                    $object = Mage::getModel($class);
                    break;

            }
            if(!$object || !is_object($object)) {
                throw new Exception('Model class ' . $class . ' does not exists');
            }
            if(!method_exists($object, $method)) {
                throw new Exception('Method ' . $method . ' of class ' . $class . ' does not exists');
            }
            call_user_func_array(array($object, $method), $params);
            $this->setFinishedAt(Mage::getSingleton('core/date')->date('Y-m-d H:i:s'))->setStatus(Mage_Cron_Model_Schedule::STATUS_SUCCESS)->save();
        } catch(Exception $e) {
            $this->setFinishedAt(Mage::getSingleton('core/date')->date('Y-m-d H:i:s'))->setStatus(Mage_Cron_Model_Schedule::STATUS_ERROR)->setMessages($e->getMessage()."\n".$e->getTraceAsString())->save();
        }
    }
    

}
