<?php
/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_Model_Resource_Catalog_Product_Collection extends Mage_Catalog_Model_Resource_Product_Collection {

    /* WebCooking Fix to fix an issue in Varien_Data_Collection_Db::getSize() when a group by clause is used. */
    public function getSize() {
        if (is_null($this->_totalRecords)) {
            $sql = $this->getSelectCountSql();

            $result = $this->getConnection()->fetchAll($sql, $this->_bindParams);
            

            foreach ($result as $row) {
                $this->_totalRecords += reset($row);
            }
        }
        return intval($this->_totalRecords);
    }
    
}
