<?php
/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_Helper_Data extends Mage_Core_Helper_Abstract {

    
    protected $_defaultStore = null;
    protected $_currentStore = null;
    protected $_isCurrentlySecure = null;
    
    protected static $_models = array();
    
    public static function getModel($modelId, $class='catalog/product', $loadMethod = 'load', $storeId = false) {
        if(!isset(self::$_models[$class])) {
            self::$_models[$class] = array();
        }
        $key = $modelId.'-'.($storeId?:0);
        if(!isset(self::$_models[$class][$key])) {
            if($class == 'catalog/product' && Mage::registry('current_product') && Mage::registry('current_product')->getId() == $modelId) {
                self::$_models[$class][$key] = Mage::registry('current_product');
            } else if($class == 'catalog/category' && Mage::registry('current_category') && Mage::registry('current_category')->getId() == $modelId) {
                self::$_models[$class][$key] = Mage::registry('current_category');
            } else {
                $model = Mage::getModel($class);
                if($storeId) {
                    $model->setStoreId($storeId);
                }
                self::$_models[$class][$key] = $model->$loadMethod($modelId);
            }
        }
        return self::$_models[$class][$key];
    }
    public static function getSavedProduct($modelId, $storeId = false) {
        return self::getModel($modelId, 'catalog/product', 'load', $storeId);
    }
    public static function getSavedStockItem($productId) {
        $stockItem = self::getModel($productId, 'cataloginventory/stock_item', 'loadByProduct');
        $stockItemId = $stockItem->getId();
        if (!$stockItemId) {
            $stockItem->setData('product_id', $productId);
            $stockItem->setData('stock_id', 1);
        }
        return $stockItem;
    }
    
    public static function getSavedCategory($modelId) {
        return self::getModel($modelId, 'catalog/category');
    }
    
    
    public function getDefaultStoreId($cached = true) {
        return  $this->getDefaultStore($cached)->getId();
    }
    
    public function getDefaultStore($cached = true) {
        if(!$cached) {
            return Mage::app()->getDefaultStoreView();
        }
        if(is_null($this->_defaultStore)) {
            $this->_defaultStore = Mage::app()->getDefaultStoreView();
        }
        return $this->_defaultStore;
    }
    
    public function isCurrentlySecure($storeId = null, $cached = true) {
        if(!$cached) {
            return Mage::app()->getStore($storeId)->isCurrentlySecure();
        }
        if(is_null($this->_isCurrentlySecure)) {
            $this->_isCurrentlySecure = (int)Mage::app()->getStore($storeId)->isCurrentlySecure();
        }
        return $this->_isCurrentlySecure;
    }
    
    public function getCurrentStoreId($cached = true) {
        return $this->getCurrentStore($cached)->getId();
    }
    
    public function getCurrentStore($cached = true) {
        if(!$cached) {
            return Mage::app()->getStore();
        }
        if(is_null($this->_currentStore)) {
            $this->_currentStore = Mage::app()->getStore();
        }
        return $this->_currentStore;
    }
    
    
    
    public function isModuleInstalled($module) {
        $modules = Mage::getConfig()->getNode('modules')->children();
        $modulesArray = (array) $modules;

        return isset($modulesArray[$module]);
    }
    
    public function isModuleEnable($module) {
        return $this->isModuleEnabled($module);
    }

    public function applyReplaceAccent($data) {
        $data = @iconv('UTF-8', 'UTF-8//IGNORE', $data);
        $data = htmlentities($data, ENT_COMPAT, 'UTF-8');
        $data = preg_replace('#\&([A-za-z])(?:acute|cedil|circ|grave|ring|tilde|uml)\;#', '\1', $data);
        $data = preg_replace('#\&([A-za-z]{2})(?:lig)\;#', '\1', $data); // pour les ligatures e.g. '&oelig;'
        $data = preg_replace('#\&[^;]+\;#', '', $data); // supprime les autres caractères
        return $data;
    }
    
    public function sanitizeUrlKey($urlKey) {
        $urlKey = $this->applyReplaceAccent($urlKey);
        $urlKey = preg_replace('%[^a-zA-Z0-9-]%i', '-', strtolower($urlKey));
        $urlKey = preg_replace('%--+%i', '-', $urlKey);
        $urlKey = preg_replace('%^-|-$%i', '', $urlKey);
        return $urlKey;
    }
    
    
    /**
     * 
     * @param Mage_Catalog_Model_Product $product : Product to check
     * @param integer $decimals
     * @param boolean $withSymbol
     * @param boolean $allowEmptyEndDate
     * @return boolean|string promo percentage or false if no promo
     */
    public function isInPromo($product, $decimals = 0, $withSymbol = true, $allowEmptyEndDate = true) {
        if (!$product || !$product->getPrice())
            return false;
        
        $finalPrice = $product->getFinalPrice();
        if($finalPrice == $product->getPrice()) {
            return false;
        }
        
        $percentage = round((100 - (100 * $finalPrice) / $product->getPrice()), $decimals);
        
        if ($withSymbol)
            $percentage .= '%';

        return $percentage;
        /*
        $dailydealPercentage = false;
        
        //MW_Dailydeals support
        if($this->isModuleEnabled('MW_Dailydeal')) {
            $tblCatalogStockItem = Mage::getSingleton('core/resource')->getTableName('cataloginventory_stock_item');
            $currenttime = date('Y-m-d H:i:s', Mage::getModel('core/date')->timestamp(time()));
            $deals = Mage::getModel('dailydeal/dailydeal')->getCollection()
                    ->addFieldToFilter('status', '1')
                    ->addFieldToFilter('main_table.product_id', $product->getId())
                    ->addFieldToFilter('start_date_time', array('to' => $currenttime))
                    ->addFieldToFilter('end_date_time', array('from' => $currenttime))
                    ->addAttributeToSort('dailydeal_id', 'ASC')
                    ->addAttributeToSort('start_date_time', 'ASC');
           
            foreach($deals as $deal) {
                $dealPercentage = (100 - (100 * $deal->getDailydealPrice()) / $product->getPrice());
                if(!$dailydealPercentage || $dealPercentage > $dailydealPercentage) {
                    $dailydealPercentage = $dealPercentage;
                }
            }
        }
        
        if (!$dailydealPercentage && !$product->getSpecialPrice() && !$product->getGroupPrice()) {
            return false;
        }
        
        
        $now = time();
        $aDay = 24 * 60 * 60;

        $specialPrice = $product->getSpecialPrice();
        if (!$allowEmptyEndDate && !trim($product->getSpecialToDate())) {
            $specialPrice = false;
        }
        if (trim($product->getSpecialToDate()) && $now >= strtotime($product->getSpecialToDate()) + $aDay) {
            $specialPrice = false;
        }
        if (trim($product->getSpecialFromDate()) && $now < strtotime($product->getSpecialFromDate())) {
            $specialPrice = false;
        }
        
        
        
        if($product->getGroupPrice() && $specialPrice) {
            $specialPrice = min($product->getGroupPrice(), $specialPrice);
        } else if($product->getGroupPrice()) {
            $specialPrice = $product->getGroupPrice();
        }
        
        
        if (!$dailydealPercentage && $specialPrice >= $product->getPrice()) {
            return false;
        }
        
        
        
        if ($product->getTypeId() == 'bundle') {
            $percentage = (100 - $specialPrice);
        } else {
            $percentage = (100 - (100 * $specialPrice) / $product->getPrice());
        }

        if ($product->getTypeId() != 'bundle' && $product->getPrice() <= 0)
            return true;
        
        $percentage = round(max($dailydealPercentage, $percentage), $decimals);
        
        
        if ($withSymbol)
            $percentage .= '%';

        return $percentage;*/
    }
    
    
    public function isInGroupPromo($product, $decimals = 0, $withSymbol = true, $allowEmptyEndDate = true) {
        if($product->getGroupPrice() && $product->getGroupPrice() == $product->getFinalPrice()) {
            return $this->isInPromo($product, $decimals, $withSymbol, $allowEmptyEndDate);
        }
        return false;
    }

    public function encrypt($value) {
        return openssl_encrypt($value, 'aes256', (string)Mage::getConfig()->getNode('global/crypt/key'));
    }
    
    public function decrypt($value) {
        return openssl_decrypt($value, 'aes256', (string)Mage::getConfig()->getNode('global/crypt/key'));
    }
    
    public function sendAlert($subject, $content, $to = false, $from = false) {
        if(!$to) {
            $to = explode(',', Mage::getStoreConfig('wcooall/alerts/to'));
        } else if(!is_array($to)) {
            $to = array($to);
        }
        $to = array_filter($to);
        if(empty($to)) {
            return;
        }
        foreach($to as $email) {
            $tpl = Mage::getModel('core/email_template');
            $tpl->setDesignConfig(array('area'=>'adminhtml'))
                ->sendTransactional(
                    Mage::getStoreConfig('wcooall/alerts/template'),
                    $from ? $from : Mage::getStoreConfig('wcooall/alerts/from'),
                    trim($email),
                    trim($email),
                    array(
                        'subject'  => $subject,
                        'content'  => $content,
                    )
            );
        }
        
    }

    public function isXhr() {
        $request = Mage::app()->getRequest();
        if(!$request) {
            return false;
        }
        return $request->isXmlHttpRequest() || $request->getParam('awacp') || $request->getParam('isAjax') || $request->getParam('isLNAjax');
    }
    
    
}