<?php
/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_Helper_Product extends Mage_Core_Helper_Abstract 
{

    
    /**
     * Template processor instance
     *
     * @var Varien_Filter_Template
     */
    protected $_templateProcessor = null;
    
    public function _getProduct($product) {
        if (is_object($product)) {
            return $product;
        }
        return Webcooking_All_Helper_Data::getSavedProduct($product);
    }

    public function getMinSaleQty($productId) {
        $stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
        if ($stockItem->getMinQty() > 1) {
            return $stockItem;
        }
        if ($stockItem->getQtyIncrements() > 1) {
            return $stockItem->getQtyIncrements();
        }
        return 1;
    }

    public function isBundleProductWithoutConfiguration($product, $alwaysReturnArray = false) {
        $product = $this->_getProduct($product);
        if ($product->getTypeId() != 'bundle') {
            return $alwaysReturnArray?array():false;
        }
        
        $bundleProduct = clone $product;
        $additional = array();
        $isBundleProductWithoutConfiguration = true;
       
        $typeInstance = $bundleProduct->getTypeInstance(true);
        $typeInstance->setStoreFilter($bundleProduct->getStoreId(), $bundleProduct);
        $optionCollection = $typeInstance->getOptionsCollection($bundleProduct);

        $selectionCollection = $typeInstance->getSelectionsCollection(
                $typeInstance->getOptionsIds($bundleProduct), $bundleProduct
        );
        $options = $optionCollection->appendSelections($selectionCollection, false);
        $bundleOption = array();
        foreach ($options as $option) {
            $selections = $option->getSelections();
            if($selections == 1) {
                $selection = current($selections);
                $bundleOption[$option->getOptionId()] = $selection->getSelectionId();
            } else {
                foreach ($selections as $selection) {
                    if ($option->getRequired()) {
                        if ($selection->getIsDefault()) {
                            $bundleOption[$option->getOptionId()] = $selection->getSelectionId();
                        } else {
                            $isBundleProductWithoutConfiguration = false;
                            continue;
                        }
                    }
                }
            }
        }

        if (!$isBundleProductWithoutConfiguration) {
            return $alwaysReturnArray?array():false;
        }
        if(!empty($bundleOption)) {
            $additional['bundle_option'] = $bundleOption;
        }
        return $additional;
    }
    
    
    public function removeProductImageCache($imgFile, $product = false, $onBg = true) {
        if($onBg) {
            Mage::helper('wcooall/cron')->addTask('remove Product Image Cache', 'helper/wcooall/product/removeProductImageCache', array($imgFile, $product, false));
        } else {
            $imgFile = strtolower(array_pop(explode('/', $imgFile)));
            $baseCachePath = Mage::getSingleton('catalog/product_media_config')->getBaseMediaPath() . '/cache/';
            $storeDirs = scandir($baseCachePath);
            foreach($storeDirs as $storeDir) {
                if (in_array($storeDir, [".",".."])) {
                    continue;
                } 
                $storeDirPath = $baseCachePath . $storeDir . '/';
                //Mage::log('store '.$storeDirPath, null, 'debug.image.cache.log', true);
                $typeDirs = scandir($storeDirPath);
                foreach($typeDirs as $typeDir) {
                    if (in_array($typeDir, [".",".."])) {
                        continue;
                    } 
                    $typeDirPath = $storeDirPath . $typeDir . '/';
                    //Mage::log('type '.$typeDirPath, null, 'debug.image.cache.log', true);
                    $sizeDirs = scandir($typeDirPath);
                    foreach($sizeDirs as $sizeDir) {
                        if (in_array($sizeDir, [".",".."])) {
                            continue;
                        } 
                        $sizeDirPath = $typeDirPath . $sizeDir . '/';
                        //Mage::log('size '.$sizeDirPath, null, 'debug.image.cache.log', true);
                        $keyDirs = scandir($sizeDirPath);
                        foreach($keyDirs as $keyDir) {
                            if (in_array($keyDir, [".",".."])) {
                                continue;
                            } 
                            $keyDirPath = $sizeDirPath . $keyDir . '/';
                            //Mage::log('key '.$keyDirPath, null, 'debug.image.cache.log', true);
                            $fullFileName = $keyDirPath . $imgFile[0] . '/' . $imgFile[1] . '/' . $imgFile;
                            //Mage::log($fullFileName. ' '.$imgFile, null, 'debug.image.cache.log', true);
                            if(file_exists($fullFileName)) {
                                Mage::log($fullFileName. ' '.$imgFile. ' DELETE', null, 'debug.image.cache.log', true);
                                unlink($fullFileName);
                            }
                        }
                    }
                }
            }
            /*$directoryIterator = new RecursiveDirectoryIterator($baseCachePath);
            foreach(new RecursiveIteratorIterator($directoryIterator) as $file)
            {
                $filename = strtolower(array_pop(explode('/', $file)));
                //Mage::log($filename. ' '.$imgFile, null, 'debug.image.cache.log', true);
                if($filename && $filename == $imgFile) {
                  //  Mage::log($file, null, 'debug.image.cache.log', true);
                    unlink($file);
                }
            }*/
            if(Mage::helper('wcooall')->isModuleInstalled('Webcooking_ImageIOOptimizer')) {
                $products = array();
                if(!$product) {
                    $read = Mage::getSingleton('core/resource')->getConnection('core_read');
                    $select = $read->select()->from(array('main_table' => Mage::getSingleton('core/resource')->getTableName('catalog_product_entity_media_gallery')))
                        ->where('value = ?', $imgFile{0}.'/'.$imgFile{1}.'/'.$imgFile)
                        ->group('entity_id');

                    $data = $read->fetchAll($select);
                    foreach($data as $row) {
                        $products[] =  Webcooking_All_Helper_Data::getSavedProduct($row['entity_id']);
                    }
                } else {
                    $products[] = $product;
                }
                foreach($products as $p) {
                    if(Mage::getStoreConfigFlag('iioo/general/generate_on_bg')) {
                        Mage::helper('wcooall/cron')->addTask('Generate Images Cache', 'singleton/iioo/cron/generateImagesCache', array($p, true));
                    } else {
                        Mage::getSingleton('iioo/cron')->generateImagesCache($p, true);
                        if($varnishAdmin = Mage::getModel('turpentine/varnish_admin')) {
                            Mage::dispatchEvent('turpentine_varnish_flush_partial', array('pattern' => $imgFile));
                            $varnishAdmin->flushUrl($imgFile);
                        }
                    }
                }
            }
        }
    }
    
    public function removeProductImagesCache($product) {
        if(is_numeric($product)) {
            $product =  Webcooking_All_Helper_Data::getSavedProduct($product);
            foreach ($product->getMediaGalleryImages() as $image) { 
                $this->removeProductImageCache($image->getFile());
            }
        }
    }
    
    public function getAttributeText($product, $attributeCode)
    {
        return Mage::helper('wcooall/attribute')
            ->getAttribute($attributeCode)
                ->getSource()
                    ->getOptionText($product->getData($attributeCode));
    }

    protected function _getTemplateProcessor()
    {
        if (null === $this->_templateProcessor) {
            $this->_templateProcessor = Mage::helper('catalog')->getPageTemplateProcessor();
        }

        return $this->_templateProcessor;
    }
    
    public function productAttribute($product, $attributeHtml, $attributeName)
    {
        $attribute = Mage::helper('wcooall/attribute')->getAttribute($attributeName);
        if ($attribute && $attribute->getId() && ($attribute->getFrontendInput() != 'media_image')
            && (!$attribute->getIsHtmlAllowedOnFront() && !$attribute->getIsWysiwygEnabled())) {
                if ($attribute->getFrontendInput() != 'price') {
                    $attributeHtml = $this->escapeHtml($attributeHtml);
                }
                if ($attribute->getFrontendInput() == 'textarea') {
                    $attributeHtml = nl2br($attributeHtml);
                }
        }
        if ($attribute->getIsHtmlAllowedOnFront() && $attribute->getIsWysiwygEnabled()) {
            if (Mage::helper('catalog')->isUrlDirectivesParsingAllowed()) {
                $attributeHtml = $this->_getTemplateProcessor()->filter($attributeHtml);
            }
        }

        $attributeHtml = Mage::helper('catalog/output')->process('productAttribute', $attributeHtml, array(
            'product'   => $product,
            'attribute' => $attributeName
        ));

        return $attributeHtml;
    }
    
    
    public function isInWhishlist($product) {
        return (bool) $this->getWhishlistItem($product);
    }
    
      public function getWhishlistItem($product) {
        if(!Mage::getSingleton('customer/session')->isLoggedIn()) {
            return false;
        }
        $session = Mage::getSingleton('customer/session');
        $customerId = $session->getId();
        $wishlist = Mage::getModel('wishlist/item')->getCollection();
        $wishlist->addFieldToSelect('*');
        $wishlist->getSelect()
                  ->join(array('t2' => $wishlist->getTable('wishlist')),
                         'main_table.wishlist_id = t2.wishlist_id',
                         array('wishlist_id','customer_id'))
                         ->where('main_table.product_id = '.$product->getId().' AND t2.customer_id='.$customerId);
        return $wishlist->count() > 0?$wishlist->getFirstItem():false;
    }
}
