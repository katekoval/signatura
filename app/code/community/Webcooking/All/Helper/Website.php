<?php
/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_Helper_Website extends Mage_Core_Helper_Abstract {

    public  function getAllWebsitesId() {
            $websites = array();
            foreach(Mage::app()->getWebsites() as $website) {
                    $websites[] = $website->getData('website_id');
            }
            return $websites;
    }

}
