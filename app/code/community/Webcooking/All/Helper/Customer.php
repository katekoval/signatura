<?php
/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_Helper_Customer extends Mage_Core_Helper_Abstract {

    public function getAllGroupsId() {
        $groups = Mage::helper('customer')->getGroups()->getData();
        $groupsIds = array('0'); // Not Logged
        foreach ($groups as $group) {
            $groupsIds[] = $group['customer_group_id'];
        }
        return $groupsIds;
    }

}
