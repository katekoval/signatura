<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
$installer = $this;
$installer->startSetup();

$installer->getConnection()->modifyColumn(
        $installer->getTable('wcooall/task'),
        'name', 
        'varchar(255) NULL');
$installer->getConnection()->modifyColumn(
        $installer->getTable('wcooall/task'),
        'method', 
        'varchar(255) NULL');

$installer->getConnection()->addIndex(
        $installer->getTable('wcooall/task'),
        $installer->getIdxName('wcooall/task', array('status')),
        array('status'));
$installer->getConnection()->addIndex(
        $installer->getTable('wcooall/task'),
        $installer->getIdxName('wcooall/task', array('status', 'method')),
        array('status', 'method'));

$installer->endSetup(); 
