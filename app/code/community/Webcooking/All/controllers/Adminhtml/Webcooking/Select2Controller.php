<?php
/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_Adminhtml_Webcooking_Select2Controller extends Mage_Adminhtml_Controller_Action {

    protected function _construct() {
        $this->setUsedModuleName('Webcooking_All');
    }
    
    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isLoggedIn();
    }

    public function indexAction() {
        if (!$this->getRequest()->isAjax()) {
            $this->_forward('noRoute');
            return;
        }
        $search = $this->getRequest()->getParam('search');
        $attributeCode = $this->getRequest()->getParam('attribute_code');
        $entityType = $this->getRequest()->getParam('entity_type');
        $attribute = Mage::helper('wcooall/attribute')->getAttribute($attributeCode, $entityType);
        
        
        $options = array();
        $store  = Mage::app()->getStore();
        $read = Mage::getSingleton('core/resource')->getConnection('core_read');
        if($attribute->getSourceModel()) {
            $sourceModel = Mage::getModel($attribute->getSourceModel());
            $sourceOptions = method_exists($sourceModel, 'getAllOptionsForSelect2') ? $sourceModel->getAllOptionsForSelect2() : $sourceModel->getAllOptions();
            $i = 0;
            foreach($sourceOptions as $option) {
                if(preg_match('%'.preg_quote($search).'%i', $option['label'])) {
                    $options[$i]['id'] = $option['value'];
                    $options[$i]['text'] = $option['label'];
                    $i++;
                }
            }
        } else {
            $select = $read->select()->from(array('a' => Mage::getSingleton('core/resource')->getTableName('eav/attribute_option_value')))
                ->joinLeft(
                    array('b' => Mage::getSingleton('core/resource')->getTableName('eav/attribute_option')),
                    'a.option_id = b.option_id',
                    array()
                )
                ->where('a.store_id = ?', $store->getId())
                ->where('b.attribute_id = ?', $attribute->getId())
                ->where('a.value LIKE ?', '%' . $search . '%');
            
            $data = $read->fetchAll($select);
            
            $i = 0;
            foreach ($data as $opt) {
                $options[$i]['id'] = $opt['option_id'];
                $options[$i]['text'] = $opt['value'];
                $i ++;
            }
        }
        
        
        
        
        
        $this->getResponse()->setBody(Mage::helper('core')->
            jsonEncode($options));
    }
    
}
