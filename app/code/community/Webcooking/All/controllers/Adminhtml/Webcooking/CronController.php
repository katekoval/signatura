<?php
/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_Adminhtml_Webcooking_CronController extends Mage_Adminhtml_Controller_Action {

    protected function _construct() {
        $this->setUsedModuleName('Webcooking_All');
    }

    public function indexAction() {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function scheduledgridAction()
    {
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('wcooall/adminhtml_cron_schedule_grid')->toHtml()
        );

        return $this;
    }
    
    public function gridAction()
    {
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('wcooall/adminhtml_cron_grid')->toHtml()
        );

        return $this;
    }
    
    public function execAction()
    {
        try {
            $id = $this->getRequest()->getParam('identifier');
            if(!$id){
                $this->_getSession()->addWarning(Mage::helper('wcooall')->__("Job not found"));
                 return $this->_redirect('*/*/index');
            }
            
            $cronModelData = Mage::getConfig()->getNode('crontab/jobs/'.$id.'/run/model');

            if(!$cronModelData){
                $this->_getSession()->addWarning(Mage::helper('wcooall')->__("Job not found"));
                return $this->_redirect('*/*/index');
            }
            $cronModelData = explode('::', $cronModelData->asArray());

            $modelClass = isset($cronModelData[0]) ? $cronModelData[0] : '';
            $method = isset($cronModelData[1]) ? $cronModelData[1] : '';

            if(!$modelClass || !$method){
                $this->_getSession()->addWarning(Mage::helper('wcooall')->__("Job model or method not found for Job '%s'", $id));
                return $this->_redirect('*/*/index');
            }
            $cronModel = Mage::getModel($modelClass);
            
            $schedule = Mage::getModel('cron/schedule');
            $cronModel->$method($schedule);

            $this->_getSession()->addSuccess(Mage::helper('wcooall')->__("Job '%s' has been executed.", $id));

        } catch(Exception $e){
            $this->_getSession()->addError($e->getMessage());
        }

        return $this->_redirect('*/*/index');
    }
    
    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('system/tools/crons');
    }

}
