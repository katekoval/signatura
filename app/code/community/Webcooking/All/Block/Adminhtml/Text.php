<?php
/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_Block_Adminhtml_Text extends Webcooking_All_Block_Text {

    protected function _getUrlModelClass()
    {
        return 'adminhtml/url';
    }
}