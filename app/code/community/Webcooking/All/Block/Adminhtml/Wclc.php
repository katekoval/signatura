<?php
/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_Block_Adminhtml_Wclc extends Webcooking_All_Block_Adminhtml_Text {

    
    public function addWclc($lc=false) {
        $moduleName = preg_replace('%wclc_%i', '',  $this->getNameInLayout());
        $moduleName = preg_replace('%_%i', ' ',  $moduleName);
        $moduleName = 'Webcooking_' . preg_replace('% %i', '',  ucwords($moduleName));
        $moduleVersion = '??';
        if(Mage::getConfig()->getNode()->modules->$moduleName) {
            $moduleVersion = (string) Mage::getConfig()->getNode()->modules->$moduleName->version;
        }
        if(!$lc && Mage::getConfig()->getNode()->modules->$moduleName->webcooking) {
            $lc = (string) @Mage::getConfig()->getNode()->modules->$moduleName->webcooking->key;
        }
        
        $a = $lc . '-' . base64_encode(Mage::getUrl()) . '-' . base64_encode(Mage::getVersion()) . '-' . base64_encode($moduleVersion);
        if(is_null(Mage::registry('wclc'))) {
            Mage::register('wclc', array());
        }
        $b = Mage::registry('wclc');
        $b[] = $a;
        Mage::unregister('wclc');
        Mage::register('wclc', $b);
    }
}