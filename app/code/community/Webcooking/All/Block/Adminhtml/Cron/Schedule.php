<?php
/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_Block_Adminhtml_Cron_Schedule extends Mage_Adminhtml_Block_Widget_Grid_Container {

    public function __construct() {
        parent::__construct();

        $this->_controller = 'adminhtml_cron_schedule';
        $this->_blockGroup = 'wcooall';
        $this->_headerText = $this->__('Crons Scheduled (Web Cooking Tool)');

        $this->_removeButton('add');
    }

}
