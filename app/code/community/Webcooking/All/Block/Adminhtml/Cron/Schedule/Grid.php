<?php
/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_Block_Adminhtml_Cron_Schedule_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	

    public function __construct()
    {
        parent::__construct();
        $this->setId('wcooScheduledCronGrid');
        $this->setUseAjax(true);
        $this->setDefaultSort('created_at');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }


    protected function _prepareCollection()
    {
        $collection = Mage::getModel("cron/schedule")->getCollection();
        $collection->getSelect()->order('schedule_id desc');
        
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn('schedule_id', array(
            'header'=> Mage::helper('cron')->__('Schedule Id'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'schedule_id',
        ));

        $this->addColumn('job_code', array(
            'header'=> Mage::helper('cron')->__('Job Code'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'job_code',
        ));

        $this->addColumn('status', array(
            'header'=> Mage::helper('cron')->__('Status'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'status',
        ));

        $this->addColumn('messages', array(
            'header'=> Mage::helper('cron')->__('Messages'),
            'width' => '80px',
            'type'  => 'text',
            'index' => 'messages',
            'truncate' => 10000
        ));

        $this->addColumn('scheduled_at', array(
            'header'=> Mage::helper('cron')->__('Scheduled at'),
            'width' => '80px',
            'type'  => 'datetime',
            'index' => 'scheduled_at',
        ));


        $this->addColumn('created_at', array(
            'header'=> Mage::helper('cron')->__('Created at'),
            'width' => '80px',
            'type'  => 'datetime',
            'index' => 'created_at',
        ));

        $this->addColumn('executed_at', array(
            'header'=> Mage::helper('cron')->__('Executed at'),
            'width' => '80px',
            'type'  => 'datetime',
            'index' => 'executed_at',
        ));

        $this->addColumn('finished_at', array(
            'header'=> Mage::helper('cron')->__('Finished at'),
            'width' => '80px',
            'type'  => 'datetime',
            'index' => 'finished_at',
        ));


        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/scheduledgrid', array('_current'=>true));
    }

}
