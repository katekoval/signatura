<?php
/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See LICENSE-EN.txt for license details.
 */
class Webcooking_All_Block_Adminhtml_Log_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	

    public function __construct()
    {
        parent::__construct();
        $this->setId('wcooLogGrid');
        $this->setUseAjax(true);
        $this->setDefaultSort('date');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }


    protected function _prepareCollection()
    {
        $collection = Mage::getSingleton('wcooall/system_log_fs_collection');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $this->addColumn('time', array(
            'header'    => Mage::helper('wcooall')->__('Last Modification Time'),
            'index'     => 'time',
            'type'      => 'datetime',
            'width'     => 200
        ));

        $this->addColumn('name', array(
            'header'    => Mage::helper('wcooall')->__('Name'),
            'index'     => 'name',
            'filter'    => false,
            'sortable'  => true,
            'width'     => 350
        ));

        $this->addColumn('size', array(
            'header'    => Mage::helper('wcooall')->__('Size, MegaBytes'),
            'index'     => 'size',
            'type'      => 'number',
            'sortable'  => true,
            'filter'    => false
        ));

       

        $this->addColumn('download', array(
            'header'    => Mage::helper('wcooall')->__('Download'),
            'format'    => '<a href="' . $this->getUrl('*/*/download', array('name' => '$name'))
                . '">$name</a> &nbsp;',
            'index'     => 'type',
            'sortable'  => false,
            'filter'    => false
        ));
        
         $this->addColumn('action', array(
                    'header'   => Mage::helper('wcooall')->__('Action'),
                    'type'     => 'action',
                    'width'    => '80px',
                    'filter'   => false,
                    'sortable' => false,
                    'actions'  => array(array(
                        'url'     => '#',
                        'caption' => Mage::helper('wcooall')->__('Delete file'),
                        'onclick' => 'setLocation(\''.$this->getUrl('*/*/delete', array('name' => '$name')).'\')'
                    )),
                    'index'    => 'type',
                    'sortable' => false
            ));

       

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }

}
