<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Model_Cron {

    protected function _getApi() {
        return Mage::helper('googleuniversalanalytics/api');
    }
    
    
    protected $_productFeed = null;
    protected function _getProductFeed($storeId = false) {
        if(is_null($this->_productFeed)) {
            $productCollection = Mage::getModel('catalog/product')->getCollection();
            $productCollection->addAttributeToSelect('name', 'left');
            $productCollection->addAttributeToSelect('price', 'left');
            $productCollection->addCategoryIds();
            //$productCollection->addAttributeToSelect('category_ids', 'left');
            $brandAttributeCode = Mage::helper('googleuniversalanalytics/ecommerce')->getBrandAttributeCode($storeId);
            if($brandAttributeCode) {
                $productCollection->addAttributeToSelect($brandAttributeCode, 'left');
            }
            $header = array('ga:productSku','ga:productBrand','ga:productCategoryHierarchy','ga:productName','ga:productPrice');
            $this->_productFeed = implode(',', $header) . "\n";
            Mage::getSingleton('core/resource_iterator')
                        ->walk($productCollection->getSelect(), array(array($this, '_addProductInFeed')), array('brandAttributeCode'=>$brandAttributeCode));
            $this->_productFeed = trim($this->_productFeed);
        }
        return $this->_productFeed;
        
    }
    
    public function _addProductInFeed($args) {
        $productData = $args['row'];
        $product = Mage::getModel('catalog/product');
        $product->setData($productData);
        $brandAttributeCode = $args['brandAttributeCode'];
        $row = array();
        $row[] = $productData['sku'];
        if($brandAttributeCode) {
            $row[] = '"' . Mage::helper('wcooall/attribute')->getAttributeOptionNameById($brandAttributeCode, $productData[$brandAttributeCode]) . '"';
        } else {
            $row[] = '';
        }
        $row[] = '"' . Mage::helper('googleuniversalanalytics/ecommerce')->getProductCategoryValue($product, false) . '"';
        $row[] = '"' . $productData['name'] . '"';
        $row[] = '"' . $productData['price'] . '"';
        $this->_productFeed .= implode(',' , str_replace($row, ",", "\,")) . "\n";
    }

    
    public function updateProducts() {
        if(!Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/export_products_via_api')) {
            return;
        }
        $customDataSourceId = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/export_products_data_source_id');
        if(!$customDataSourceId) {
            return;
        }
        if(!$this->_getApi() || !$this->_getApi()->getClient()) {
            return;
        }
        try {
            $profile = $this->_getApi()->getProfileForStore();
            if($profile) {
                $productFeed = $this->_getProductFeed();
              
                $analytics = new Google_Service_Analytics($this->_getApi()->getClient());
                $result = $analytics->management_uploads->uploadData(
                        $profile->getAccountId(), 
                        Mage::helper('googleuniversalanalytics')->getAccountId(), 
                        $customDataSourceId, 
                        array('data' => $productFeed,
                              'mimeType' => 'application/octet-stream',
                              'uploadType' => 'media'));
            }
        } catch (apiServiceException $e) {
            Mage::helper('googleuniversalanalytics')->log('There was an Analytics API service error '
                    . $e->getCode() . ':' . $e->getMessage());
        } catch (apiException $e) {
            Mage::helper('googleuniversalanalytics')->log('There was a general API error '
                    . $e->getCode() . ':' . $e->getMessage());
        } catch(Exception $e) {
            Mage::helper('googleuniversalanalytics')->log('There was an API error ' . $e->getMessage());
        }
        
    }
    
    
    public function sendQueuedHits() {
        $mp =  Mage::getModel('googleuniversalanalytics/measurement_protocol');
        $queuedHitCollection = Mage::getModel('googleuniversalanalytics/measurement_protocol_queue')->getCollection();
        $queueSize = $queuedHitCollection->getSize();
        $cpt = 1;
        foreach($queuedHitCollection as $queuedHit) {
            try {
                $mp->sendHitFromQueue($queuedHit);
                $queuedHit->delete();
                if($cpt != $queueSize && !$this->isEcommerceHit($queuedHit)) {
                    if($queueSize < 120) {
                        sleep(1);
                    } else if($queueSize < 500) {
                        usleep(1000000/2);
                    } else {
                        usleep(1000000/4);
                    }
                }
                $cpt++;
            } catch(Exception $e) {
                 Mage::helper('googleuniversalanalytics')->log('There was an error when executing hit queue (#'.$queuedHit->getId().') :' . $e->getMessage());
            }
        }
    }
    
    public function isEcommerceHit($queuedHit) {
        $params = @unserialize($queuedHit->getHitData());
        if(isset($params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION])) {
            return true;
        }
        if(isset($params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_ID])) {
            return true;
        }
        
        return false;
    }
    
    public function importProductData() {
        if(!$this->_getApi() || !$this->_getApi()->getClient()) {
            Mage::helper('googleuniversalanalytics')->log('importProductData failed. Api or Api client not found.');
            return;
        }
        
       
        
        $serviceReporting = $this->_getApi()->getServiceReporting();
        
        $dateRange = new Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate('60daysAgo');
        $dateRange->setEndDate('today');
        
        $metrics = array();
        $metric = new \Google_Service_AnalyticsReporting_Metric();
        $metric->setExpression('ga:buyToDetailRate');
        $metrics[] = $metric;
        $metric = new \Google_Service_AnalyticsReporting_Metric();
        $metric->setExpression('ga:cartToDetailRate');
        $metrics[] = $metric;
        
        $dimension = new Google_Service_AnalyticsReporting_Dimension();
        $dimension->setName('ga:productSku');
        
        
        $request = new Google_Service_AnalyticsReporting_ReportRequest();
        $profile = $this->_getApi()->getProfileForStore();
        if($profile) {
            $request->setViewId($profile->id);
            $request->setDateRanges($dateRange);
            $request->setMetrics($metrics);
            $request->setDimensions(array($dimension));

            $body = new \Google_Service_AnalyticsReporting_GetReportsRequest();
            $body->setReportRequests(array($request));
            
            do {
                Mage::helper('googleuniversalanalytics')->log('CRON::importProductData::NEW PAGE');
                $productData = array(
                    'ga_buy_to_detail_rate' => array(),
                    'ga_cartToDetailRate' => array(),
                );
                
                $reportResponse = $serviceReporting->reports->batchGet($body);

                $rows = $reportResponse[0]->getData()->getRows();
                foreach($rows as $row) {
                    $dimensions = $row->getDimensions();
                    $sku = $dimensions[0];
                    Mage::helper('googleuniversalanalytics')->log('CRON::importProductData::DATA for '.$sku);
                    $metrics = $row->getMetrics();
                    $metrics = $metrics[0]->getValues();
                    $buyToDetailRate = round(floatval($metrics[0]), 4) . '';
                    $cartToDetailRate = round(floatval($metrics[1]), 4) . '';

                    if(!isset($productData['ga_buy_to_detail_rate'][$buyToDetailRate])) {
                        $productData['ga_buy_to_detail_rate'][$buyToDetailRate] = array();
                    }
                    $productData['ga_buy_to_detail_rate'][$buyToDetailRate][] = Mage::getSingleton('catalog/product')->getIdBySku($sku);

                    if(!isset($productData['ga_cart_to_detail_rate'][$cartToDetailRate])) {
                        $productData['ga_cart_to_detail_rate'][$cartToDetailRate] = array();
                    }
                    $productData['ga_cart_to_detail_rate'][$cartToDetailRate][] = Mage::getSingleton('catalog/product')->getIdBySku($sku);

                }

                foreach($productData as $attributeCode => $values) {
                    foreach($values as $value => $pids) {
                        Mage::getSingleton('catalog/product_action')->updateAttributes($pids, array($attributeCode => $value), 0);
                    }
                }
                
                //var_Dump($reportResponse[0]->getNextPageToken());
                $nextPageToken = $reportResponse[0]->getNextPageToken();
                $request->setPageToken($nextPageToken);
            } while($nextPageToken != '');
            
        } else {
            Mage::helper('googleuniversalanalytics')->log('CRON::importProductData:: NO PROFILE.');
            //echo "no profile\n";
        }
        
    }

}
