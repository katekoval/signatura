<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Model_Adminhtml_System_Config_Source_Status extends Webcooking_All_Model_System_Config_Source_Status {

    protected $_stateStatuses = null;
    protected $_withSelectOption = false;

}
