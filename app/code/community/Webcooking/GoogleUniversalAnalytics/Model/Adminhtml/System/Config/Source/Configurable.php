<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Model_Adminhtml_System_Config_Source_Configurable extends Varien_Object {

    const CONFIGURABLE_ONLY = 0;
    const CONFIGURABLE_AND_SIMPLES = 1;
    const SIMPLES_ONLY = 2;
    
    public function toOptionArray() {
        return array(
            array('value'=>self::CONFIGURABLE_ONLY, 'label'=>'Send only configurable product'),  
            array('value'=>self::CONFIGURABLE_AND_SIMPLES, 'label'=>'Send configurable and simples products'),
            array('value'=>self::SIMPLES_ONLY, 'label'=>'Send only simples products'),
        );
    }

}
