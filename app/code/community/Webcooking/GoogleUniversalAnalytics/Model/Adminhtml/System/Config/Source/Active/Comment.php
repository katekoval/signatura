<?php

/**
  * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 * @deprecated since version 1.3.2
 */
class Webcooking_GoogleUniversalAnalytics_Model_Adminhtml_System_Config_Source_Active_Comment extends Mage_Core_Model_Config_Data {

    public function getCommentText(Mage_Core_Model_Config_Element $element, $currentValue) {
        $guaUrl = 'http://restaurant.web-cooking.net/seo/magento-google-universal-analytics.html';
        $comment = '';
        if(Mage::helper('wcooall')->isModuleEnabled('Webcooking_GoogleTagManager')) {
           if(Mage::helper('googletagmanager')->isGoogleAnalyticsEnabled()) {
                $comment = '<span style="color:orange;">Webcooking_GoogleTagManager is installed : You should disable classical Analytics, except if you want a double tracking.</span>';
           }
        }
        return $comment;
    }

}
