<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Model_Adminhtml_System_Config_Source_Attribute_Brand extends Webcooking_All_Model_System_Config_Source_Attribute
{
    
    
    protected function _isAttributeAvailable($attribute) {
         if($attribute->getFrontendInput() != 'select' && $attribute->getFrontendInput() != 'text') {
             return false;
         }
         if($attribute->getAttributeCode() == 'category_ids') {
             return false;
         }
         return true;
    }
    
}
