<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */

class Webcooking_GoogleUniversalAnalytics_Model_Adminhtml_System_Config_Source_Accesstoken_Comment extends Mage_Core_Model_Config_Data {
    
    public function getCommentText(Mage_Core_Model_Config_Element $element, $currentValue)
    {
        $url = Mage::helper('googleuniversalanalytics/api')->getClient()->createAuthUrl();
        $result = '<a href="' . $url . '" target="_blank">' .  Mage::helper('googleuniversalanalytics')->__('Get your access code by clicking here') . '</a>';
        $result .= '<br/>' . Mage::helper('googleuniversalanalytics')->__('OR use your own API Project credentials :') . '<br/>';
        return $result;
    }
}
