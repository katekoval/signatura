<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Model_Adminhtml_System_Config_Source_Rate extends Varien_Object {

    public function toOptionArray() {

        $attributes = array();
        for ($i = 1; $i <= 100; $i++) {
            $attributes[] = array('value' => $i, 'label' => $i . '%');
        }

        return $attributes;
    }

}
