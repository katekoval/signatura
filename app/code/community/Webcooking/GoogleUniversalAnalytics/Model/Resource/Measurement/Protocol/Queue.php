<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Model_Resource_Measurement_Protocol_Queue extends Mage_Core_Model_Resource_Db_Abstract {

    protected function _construct() {
        $this->_init('googleuniversalanalytics/queue', 'queue_id');
    }

}
