<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol {
    
    
    //General
    const PROTOCOL_URL = "https://ssl.google-analytics.com/collect";//"http://www.google-analytics.com/collect";
    const PROTOCOL_VERSION = "v";
    const PROTOCOL_VERSION_VALUE = "1";
    const TRACKING_ID = "tid";
    const ANONYMIZE_IP = "aip";
    const DATA_SOURCE = "ds";
    const QUEUE_TIME = "qt";
    const CACHE_BUSTER = "z";
    
    
    //User
    const CLIENT_ID = "cid";
    const USER_ID = "uid";
    
    //Session
    const SESSION_CONTROL = "sc";
    const IP_OVERRIDE = "uip";
    const USER_AGENT_OVERRIDE = "ua";
    const GEOGRAPHICAL_OVERRIDE = "geoid";
    
    //Traffic Sources
    const DOCUMENT_REFERRER = "dr";
    const CAMPAIGN_NAME = "cn";
    const CAMPAIGN_SOURCE = "cs";
    const CAMPAIGN_MEDIUM = "cm";
    const CAMPAIGN_KEYWORD = "ck";
    const CAMPAIGN_CONTENT = "cc";
    const CAMPAIGN_ID = "ci";
    const GOOGLE_ADWORDS_ID = "gclid";
    const GOOGLE_DISPLAY_ADS_ID = "dclid";
    
    //System info
    const SCREEN_RESOLUTION = "sr";
    const VIEWPORT_SIZE = "vp";
    const DOCUMENT_ENCODING = "de";
    const SCREEN_COLORS = "sd";
    const USER_LANGUAGE = "ul";
    const JAVA_ENABLED = "je";
    const FLASH_VERSION = "fl";
    
    //Hit
    const HIT_TYPE = "t";
    const NON_INTERACTION_HIT = "ni";
    
    //Content Information
    const DOCUMENT_LOCATION_URL = "dl";
    const DOCUMENT_HOSTNAME = "dh";
    const DOCUMENT_PATH = "dp";
    const DOCUMENT_TITLE = "dt";
    const SCREEN_NAME = "cd";
    const LINK_ID = "linkid";
    
    //Event Tracking
    const EVENT_CATEGORY = "ec";
    const EVENT_ACTION = "ea";
    const EVENT_LABEL = "el";
    const EVENT_VALUE = "ev";
    
    //E-Commerce
    const TRANSACTION_ID = "ti";
    const TRANSACTION_AFFILIATION = "ta";
    const TRANSACTION_REVENUE = "tr";
    const TRANSACTION_SHIPPING = "ts";
    const TRANSACTION_TAX = "tt";
    const ITEM_NAME = "in";
    const ITEM_PRICE = "ip";
    const ITEM_QUANTITY = "iq";
    const ITEM_SKU = "ic";
    const ITEM_VARIATION = "iv";
    const CURRENCY = "cu";
    
    
    //Enhanced E-Commerce
    const PRODUCT_ID = "pr%did";
    const PRODUCT_NAME = "pr%dnm";
    const PRODUCT_BRAND = "pr%dbr";
    const PRODUCT_CATEGORY = "pr%dca";
    const PRODUCT_VARIANT = "pr%dva";
    const PRODUCT_PRICE = "pr%dpr";
    const PRODUCT_QUANTITY = "pr%dqt";
    const PRODUCT_COUPON = "pr%dcc";
    const PRODUCT_POSITION = "pr%dps";
    const PRODUCT_CUSTOM_DIMENSION = "pr%dcd%d";
    const PRODUCT_CUSTOM_METRIC = "pr%dcm%d";
    const PRODUCT_ACTION = "pa";
    const PRODUCT_ACTION_LIST = "pal";
    const TRANSACTION_COUPON = "tcc";
    const CHECKOUT_STEP = "cos";
    const CHECKOUT_STEP_OPTION = "col";
    
    
    
    
    //Application
    const APPLICATION_NAME = "an";
    const APPLICATION_VERSION = "av";
    
    
    
    
    //Custom dimensions / metrics
    const CUSTOM_DIMENSION = "cd";/*must be suffixed by a number*/
    const CUSTOM_METRIC = "cm";/*must be suffixed by a number*/
    
    const MAX_PRODUCT_PER_EVENT = 35;
    
    
    
    
    protected $_enhancedEcommerceHelper = null;
    
    
    
    
    
    protected function _getDefaultParams($params, $storeId=null) {
        $params[self::PROTOCOL_VERSION] = self::PROTOCOL_VERSION_VALUE;
        $params[self::DATA_SOURCE] = 'web';
        if(Mage::helper('googleuniversalanalytics')->isIPAnonymized($storeId)) {
            $params[self::ANONYMIZE_IP] = '1';
        }
        if(!isset($params[self::IP_OVERRIDE]) && Mage::helper('core/http')->getRemoteAddr() && Mage::helper('core/http')->getRemoteAddr() != '127.0.0.1') {
            $params[self::IP_OVERRIDE] = Mage::helper('core/http')->getRemoteAddr();
        }
        $params[self::TRACKING_ID] = Mage::helper('googleuniversalanalytics')->getAccountId($storeId);
        if(!isset($params[self::USER_AGENT_OVERRIDE]) && Mage::getSingleton('googleuniversalanalytics/session')->getGuaUserAgent()) {
            $params[self::USER_AGENT_OVERRIDE] = Mage::getSingleton('googleuniversalanalytics/session')->getGuaUserAgent();
        }
        if(!isset($params[self::USER_ID]) && Mage::getSingleton('customer/session')->isLoggedIn()) {
            $params[self::USER_ID] = Mage::getSingleton('customer/session')->getCustomer()->getId();
        }
        if(!Mage::getStoreConfigFlag('googleuniversalanalytics/options/enable_user_id', $storeId)) {
            unset($params[self::USER_ID]);
        }
        if(isset($params[self::CLIENT_ID]) && Mage::registry('GUA_NEW_CLIENT_ID_NEED_SESSION_START') && preg_match('%\-%', $params[self::CLIENT_ID])) {
            Mage::unregister('GUA_NEW_CLIENT_ID_NEED_SESSION_START');
            $params[self::SESSION_CONTROL] = 'start';
        }
        
        /*if(!$storeId && !Mage::app()->getStore()->isAdmin()) {
            $storeId = Mage::helper('wcooall')->getCurrentStoreId();
        }
        if($storeId) {
            $params[self::DOCUMENT_HOSTNAME] = Mage::helper('googleuniversalanalytics')->getDocumentHost(false, $storeId);
        } */
        
        if(isset($params[self::DOCUMENT_LOCATION_URL]) && $params[self::DOCUMENT_LOCATION_URL]) {
            /**
             * Google let the choice of using DL or DH+DP. But in the facts, by default ga.js is sending DH+DP, and there is some public filters that only works with DH.
             * So, DH+DP choice is the better one.
             */
            $params[self::DOCUMENT_HOSTNAME] = Mage::helper('googleuniversalanalytics')->getDocumentHost($params[self::DOCUMENT_LOCATION_URL]);
            $params[self::DOCUMENT_PATH] = Mage::helper('googleuniversalanalytics')->getDocumentPath($params[self::DOCUMENT_LOCATION_URL]);
            unset($params[self::DOCUMENT_LOCATION_URL]);
        }
        
        if(!Mage::app()->getStore()->isAdmin()) {
             if (!isset($params[self::DOCUMENT_LOCATION_URL]) && (!isset($params[self::DOCUMENT_HOSTNAME]) || !isset($params[self::DOCUMENT_PATH])) ) {
                $beforeUrl = Mage::app()->getRequest()->getParam(Mage_Core_Controller_Front_Action::PARAM_NAME_URL_ENCODED);
                $dl = '';
                if($beforeUrl) {
                    $beforePage = str_replace(Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB), '', Mage::helper('core')->urlDecode($beforeUrl));
                    if(!preg_match('%^/%',$beforePage)) {
                        $beforePage = '/' . $beforePage;
                    }   
                    $dl = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK) . Mage::helper('googleuniversalanalytics')->getDocumentPath($beforePage, $storeId);
                } else if(Mage::helper('core/url')->getCurrentUrl()) {
                    $dl = Mage::helper('core/url')->getCurrentUrl();
                } else {
                    $dl = Mage::app()->getStore($storeId)->getBaseUrl(Mage_Core_Model_Store::URL_TYPE_LINK);
                }
                //$params[self::DOCUMENT_LOCATION_URL] = $dl;
                $params[self::DOCUMENT_HOSTNAME] = Mage::helper('googleuniversalanalytics')->getDocumentHost($dl);
                $params[self::DOCUMENT_PATH] = Mage::helper('googleuniversalanalytics')->getDocumentPathFromUrl($dl);
            }
        }
        
        return $params;
    }
    
    protected function _parseParams($params, $storeId=null) {
        $parsedParams = $this->_getDefaultParams($params, $storeId);
        $clientIdFound = false;
        $hitTypeFound = false;
        foreach($parsedParams as $key=>$val) {
            if($key == self::CLIENT_ID) {
                $clientIdFound = true;
            }
            if($key == self::HIT_TYPE) {
                $hitTypeFound = true;
            }
        }
        if(!$clientIdFound || !$hitTypeFound) return false;
        return $parsedParams;
    }
    
    public function canExecRequest($storeId = null) {
        return Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsAvailable($storeId);
    }
    
    public function canUseGlobalAccount() {
        return Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsGlobalAccountAvailable();
    }
    
    public function execRequest($params, $storeId = null, $globalAccount = false) { 
        if(!$this->canExecRequest($storeId)) {
            return false;
        }
        Varien_Profiler::start(__METHOD__);
        $params = $this->_addGlobalDimensionsAndMetrics($params);
        $params = $this->_parseParams($params, $storeId);
        if(!$params) {
            Mage::helper('googleuniversalanalytics')->log('Empty params :');
            Mage::helper('googleuniversalanalytics')->log(debug_backtrace());
            Varien_Profiler::stop(__METHOD__);
            return false;
        }
        if($globalAccount) {
            $params[self::TRACKING_ID] = Mage::helper('googleuniversalanalytics')->getGlobalAccountId($storeId);
        }
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/options/queue_mp')) {
            try {
                $queue = Mage::getModel('googleuniversalanalytics/measurement_protocol_queue');
                $queue->setHitData($params);
                $queue->save();
            } catch(Exception $e) {
                 Mage::helper('googleuniversalanalytics')->log($e->getMessage());
                 Varien_Profiler::stop(__METHOD__);
                 return false;
            }
        } else {
            
            if(!$this->sendHit($params)) {
                Varien_Profiler::stop(__METHOD__);
                return false;
            }
        }
        Varien_Profiler::stop(__METHOD__);
        if(!$globalAccount && $this->canUseGlobalAccount()) {
            $this->execRequest($params, $storeId, true);
        }
        return true;
        
    }
    
    public function sendHitFromQueue($queuedHit) {
        $params = @unserialize($queuedHit->getHitData());
        $params[self::QUEUE_TIME] = min(4*60*60*1000 - 1000 ,1000*(strtotime(Mage::getSingleton('core/date')->date('Y-m-d H:i:s')) - strtotime($queuedHit->getCreatedAt())));
        $this->sendHit($params);
    }
    
    public function sendHit($params) {
        Varien_Profiler::start(__METHOD__);
        $httpClient = new Varien_Http_Client();
        try {
            $response = $httpClient->setUri(self::PROTOCOL_URL)
                            ->setParameterPost($params)
                            ->setConfig(array('timeout' => 5))
                            ->request('POST');
            Mage::helper('googleuniversalanalytics')->log('GUA Request :');
            Mage::helper('googleuniversalanalytics')->log($params);
            //Mage::helper('googleuniversalanalytics')->log($response);
        } catch(Exception $e) {
            Mage::helper('googleuniversalanalytics')->log($e->getMessage());
            Varien_Profiler::stop(__METHOD__);
            return false;
        }
        Varien_Profiler::stop(__METHOD__);
        return true;
    }
    
    protected function _addGlobalDimensionsAndMetrics($params) {
        $customDimensions = Mage::helper('googleuniversalanalytics')->getCustomDimensions();
        foreach($customDimensions as $customDimensionIndex => $customDimensionValue) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $customDimensionIndex] = $customDimensionValue;
        }
        $customMetrics = Mage::helper('googleuniversalanalytics')->getCustomMetrics();
        foreach($customMetrics as $customMetricIndex => $customMetricValue) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_METRIC . $customMetricIndex] = $customMetricValue;
        }
        return $params;
    }
    
    public function sendChunkedEvent($eventCategory, $eventAction, $eventLabel, $eventValue, $nonInteractiveMode = false, $guaClientId = false, $storeId = null, $additionalParameters=array()) {
        $baseParameters = $additionalParameters;
        $productCount = 0;
        foreach($additionalParameters as $k=>$v) {
            if(preg_match('%^'. sprintf(str_replace('%d', '%s', self::PRODUCT_ID), '([0-9]+)') . '$%', $k, $pregResult)) {
                if($pregResult[1] > $productCount) {
                    $productCount = (int) $pregResult[1];
                }
                unset($baseParameters[sprintf(self::PRODUCT_ID, $pregResult[1])]);
                unset($baseParameters[sprintf(self::PRODUCT_NAME, $pregResult[1])]);
                unset($baseParameters[sprintf(self::PRODUCT_CATEGORY, $pregResult[1])]);
                unset($baseParameters[sprintf(self::PRODUCT_BRAND, $pregResult[1])]);
                unset($baseParameters[sprintf(self::PRODUCT_VARIANT, $pregResult[1])]);
                unset($baseParameters[sprintf(self::PRODUCT_PRICE, $pregResult[1])]);
                unset($baseParameters[sprintf(self::PRODUCT_QUANTITY, $pregResult[1])]);
            }
        }
        
        $chunkParams = array(0=>$baseParameters);
        $chunkIdx = 0;
        for($i=1, $chunkProductIdx=1; $i<=$productCount; $i++, $chunkProductIdx++) {
            $chunkParams[$chunkIdx][sprintf(self::PRODUCT_ID, $chunkProductIdx)] = $additionalParameters[sprintf(self::PRODUCT_ID, $i)];
            $chunkParams[$chunkIdx][sprintf(self::PRODUCT_NAME, $chunkProductIdx)] = $additionalParameters[sprintf(self::PRODUCT_NAME, $i)];
            $chunkParams[$chunkIdx][sprintf(self::PRODUCT_CATEGORY, $chunkProductIdx)] = $additionalParameters[sprintf(self::PRODUCT_CATEGORY, $i)];
            $chunkParams[$chunkIdx][sprintf(self::PRODUCT_BRAND, $chunkProductIdx)] = $additionalParameters[sprintf(self::PRODUCT_BRAND, $i)];
            $chunkParams[$chunkIdx][sprintf(self::PRODUCT_VARIANT, $chunkProductIdx)] = $additionalParameters[sprintf(self::PRODUCT_VARIANT, $i)];
            $chunkParams[$chunkIdx][sprintf(self::PRODUCT_PRICE, $chunkProductIdx)] = $additionalParameters[sprintf(self::PRODUCT_PRICE, $i)];
            $chunkParams[$chunkIdx][sprintf(self::PRODUCT_QUANTITY, $chunkProductIdx)] = $additionalParameters[sprintf(self::PRODUCT_QUANTITY, $i)];
            if( ($i % self::MAX_PRODUCT_PER_EVENT) == 0 ) {
                $chunkParams[++$chunkIdx] = $baseParameters;
                if($chunkParams[$chunkIdx][self::PRODUCT_ACTION] == 'purchase') {
                    $chunkParams[$chunkIdx][self::TRANSACTION_REVENUE] = 0;
                    $chunkParams[$chunkIdx][self::TRANSACTION_SHIPPING] = 0;
                    $chunkParams[$chunkIdx][self::TRANSACTION_TAX] = 0;
                }
                $chunkProductIdx = 0;
            }
        }
        if(count($chunkParams) > 0 && !in_array($baseParameters[self::PRODUCT_ACTION], array('purchase', 'refund'))) {
            // For hit other than purchase or refund, we send only one chunk... We don't want checkout steps to be counted multiple times, or worst, to sleep..
            $chunkParams = array($chunkParams[0]);
        }
        $result = true;
        $isFirst = true;
        foreach($chunkParams as $params) {
            $isFirst = false;
            $result = $result && $this->sendEvent($eventCategory, $eventAction, $eventLabel, $eventValue, $nonInteractiveMode, $guaClientId, $storeId, $params);
        }
        return $result;
        
    }
    public function sendEvent($eventCategory, $eventAction, $eventLabel, $eventValue, $nonInteractiveMode = false, $guaClientId = false, $storeId = null, $additionalParameters=array()) {
        $clientId = $guaClientId ? $guaClientId : Mage::helper('googleuniversalanalytics')->getClientId();
        if (!$clientId) {
            return;
        }
        
        if(isset($additionalParameters[sprintf(self::PRODUCT_ID, self::MAX_PRODUCT_PER_EVENT+1)])) {
            return $this->sendChunkedEvent($eventCategory, $eventAction, $eventLabel, $eventValue, $nonInteractiveMode, $guaClientId, $storeId, $additionalParameters);
        }
        
        
        $params = array(
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CLIENT_ID => $clientId,
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::HIT_TYPE => 'event',
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::EVENT_CATEGORY => $eventCategory,
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::EVENT_ACTION => $eventAction,
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::EVENT_LABEL => $eventLabel,
        );
        
        
        
        if($eventValue !== false) {
            $eventValue = round($eventValue);
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::EVENT_VALUE] = $eventValue;
        }
        
        if ($nonInteractiveMode) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::NON_INTERACTION_HIT] = 1;
        }

        $params = array_merge($params, $additionalParameters);
        
        return $this->execRequest($params, $storeId);

    }
    
    public function sendPageView($page, $extraParams = array(), $nonInteractiveMode = false, $guaClientId = false, $storeId = null) {
        $clientId = $guaClientId ? $guaClientId : Mage::helper('googleuniversalanalytics')->getClientId();
        if (!$clientId) {
            return;
        }
           
        $params = array(
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CLIENT_ID => $clientId,
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::HIT_TYPE => 'pageview',
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::DOCUMENT_PATH => Mage::helper('googleuniversalanalytics')->getDocumentPath($page, $storeId),
        );
        if ($nonInteractiveMode) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::NON_INTERACTION_HIT] = 1;
        }

        return $this->execRequest($params, $storeId);
    }
    
    public function getTransactionParams($storeId, $id, $affiliation, $revenue, $shipping, $tax, $currency, $coupon='') {
        $params = array();
        $params = array(
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_ID => $id,
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_AFFILIATION => $affiliation,
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_REVENUE => $revenue,
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_SHIPPING => $shipping,
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_TAX => $tax,
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION => 'purchase',
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CURRENCY => $currency,
        );
        if($coupon) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_COUPON] = $coupon;
        }
        if(Mage::getSingleton('core/session')->getLastListName()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ACTION_LIST] = Mage::getSingleton('core/session')->getLastListName();
        }
        return $params;
    }
    
    
    public function sendTransaction($id, $affiliation, $revenue, $shipping, $tax, $currency, $guaClientId=false, $nonInteractiveMode = false, $storeId = null, $additionalParams = array()) {
        $clientId = $guaClientId ? $guaClientId : Mage::helper('googleuniversalanalytics')->getClientId(true);
        if (!$clientId) {
            return;
        }
        if (!Mage::getStoreConfig('googleuniversalanalytics/transactions/use_mp', $storeId)) {
            return;
        }
        $params = array(
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CLIENT_ID => $clientId,
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::HIT_TYPE => 'transaction',
        );
        
        $params = array_merge($params, $this->getTransactionParams($storeId, $id, $affiliation, $revenue, $shipping, $tax, $currency));
        
        
        if($nonInteractiveMode) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::NON_INTERACTION_HIT] = 1;
        }
            
        $params = array_merge($params, $additionalParams);
        
        return $this->execRequest($params, $storeId);
    
    }
    
    
    
    public function sendItem($id, $itemIncrement, $item, $currency, $guaClientId=false, $nonInteractiveMode = false, $storeId = null, $additionalParams = array()) {
        $clientId = $guaClientId ? $guaClientId : Mage::helper('googleuniversalanalytics')->getClientId($storeId);
        if (!$clientId) {
            return;
        }
        if (!Mage::getStoreConfig('googleuniversalanalytics/transactions/use_mp', $storeId)) {
            return;
        }
        
        $params = array(
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CLIENT_ID => $clientId,
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::HIT_TYPE => 'item',
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::TRANSACTION_ID => $id,
            Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CURRENCY => $currency,
        );
        
        $params = array_merge($params, $this->getItemParams($item, $itemIncrement));
        
        
        
        if($nonInteractiveMode) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::NON_INTERACTION_HIT] = 1;
        }
            
        $params = array_merge($params, $additionalParams);
        
        return $this->execRequest($params, $storeId);
    
    }
    
    
    
    
    public function sendTransactionRequestFromOrder($order, $cancelMode = false) {
        $storeId = $order->getStoreId();
        if (!$order->getGuaClientId()) {
            //Fix for Sage Pay extension
            Mage::helper('googleuniversalanalytics')->saveGuaClientIdToOrder($order, true);
        }
        if (!Mage::getStoreConfig('googleuniversalanalytics/transactions/use_mp', $storeId) || !$order->getGuaClientId()) {
            return;
        }
        $useStoreCurrency = Mage::helper('googleuniversalanalytics/ecommerce')->useStoreCurrency($storeId);
        $guaClientId = $order->getGuaClientId();
        $transactionId = Mage::helper('googleuniversalanalytics/ecommerce')->getOrderId($order);
        
        $revenue = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForOrder($order);
        $shipping = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionShippingForOrder($order);
        $tax = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionTaxForOrder($order);
        $currency = $useStoreCurrency ? $order->getOrderCurrencyCode() : $order->getBaseCurrencyCode();
        
        $affiliation = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionAffiliation($order);
        $items = $order->getAllItems();
        $coupon = strtoupper($order->getCouponCode());
        $params = $this->_getAdditionalParamsForOrder($order);
        
        $itemIncrement = 1;
        foreach($items as $item) {
            $hasParentItem = (bool) $item->getParentItemId();
            if(!$hasParentItem || Mage::helper('googleuniversalanalytics/ecommerce')->sendChildrenItems($storeId)) {
                $itemParams = $this->getItemParams($item, $itemIncrement++, $cancelMode);
                $params = array_merge($params, $itemParams);
            }
        }
        $transactionParams = $this->getTransactionParams($storeId, $transactionId, $affiliation, $cancelMode?0-$revenue:$revenue, $shipping, $tax, $currency, $coupon);
        $params = array_merge($params, $transactionParams);
        $this->sendEvent('transaction', 'order', $transactionId, false, true, $guaClientId, $order->getStoreId(), $params);

        $order->setGuaSentFlag(1)->save();
    }
    
    
    public function sendTransactionRequestFromInvoice($invoice, $order) {
        $storeId = $order->getStoreId();
        if (!$order->getGuaClientId()) {
            //Fix for Sage Pay extension
            Mage::helper('googleuniversalanalytics')->saveGuaClientIdToOrder($order, true);
        }
        if (!Mage::getStoreConfig('googleuniversalanalytics/transactions/use_mp', $storeId) || !$order->getGuaClientId() || $order->getGuaSentFlag()) {
            return;
        }
        if(Mage::registry(__METHOD__ . $invoice->getId())) {
            return; //Compatibility fix for codisto.
        }
        Mage::register(__METHOD__ . $invoice->getId(), true);
        
        $useStoreCurrency = Mage::helper('googleuniversalanalytics/ecommerce')->useStoreCurrency($storeId);
                
        $guaClientId = $order->getGuaClientId();
        $transactionId = Mage::helper('googleuniversalanalytics/ecommerce')->getOrderId($order);
        $revenue = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForInvoice($invoice);
        $shipping = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionShippingForInvoice($invoice);
        $tax = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionTaxForInvoice($invoice);
        $affiliation = Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionAffiliation($order);
        $currency = $useStoreCurrency ? $invoice->getOrderCurrencyCode() : $invoice->getBaseCurrencyCode();
        $coupon = strtoupper($order->getCouponCode());
        $items = $invoice->getAllItems();
        $params = $this->_getAdditionalParamsForOrder($order);
        
        $itemIncrement = 1;
        foreach($items as $item) {
            $hasParentItem = (bool) $item->getOrderItem()->getParentItemId();
            if(!$hasParentItem || Mage::helper('googleuniversalanalytics/ecommerce')->sendChildrenItems($storeId)) {
                $itemParams = $this->getItemParams($item, $itemIncrement++);
                $params = array_merge($params, $itemParams);
            }
        }
        $transactionParams = $this->getTransactionParams($storeId, $transactionId, $affiliation, $revenue, $shipping, $tax, $currency, $coupon);
        $params = array_merge($params, $transactionParams);
        $this->sendEvent('transaction', 'invoice', $transactionId, $revenue, true, $guaClientId, $order->getStoreId(), $params);

        $order->setGuaSentFlag(1)->save();
    }
    
    
    protected function _getAdditionalParamsForOrder($order) {
        $params = array();
        
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::USER_ID] = $order->getCustomerId();
        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::DOCUMENT_LOCATION_URL] = Mage::app()->getStore($order->getStoreId())->getUrl('checkout/onepage/success');
        if($order->getGuaUa()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::USER_AGENT_OVERRIDE] = $order->getGuaUa();
        }
        //ShoppingFlux Compatibility
        if ($order->getMarketplaceShoppingflux()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_NAME] = 'Marketplaces';
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_SOURCE] = $order->getMarketplaceShoppingflux();
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_MEDIUM] = 'Shopping Flux';
        }
        //Codisto Compatibility
        if($order->getData('codisto_orderid')) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_NAME] = 'Marketplaces';
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_SOURCE] = 'Amazon / Ebay';
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_MEDIUM] = 'Codisto';
        }
        
        //Channable
        if($order->getData('channel_name')) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_NAME] = 'Marketplaces';
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_SOURCE] = $order->getData('channel_name');
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_MEDIUM] = 'Channable';
        }
        
        if($order->getRemoteIp()) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::IP_OVERRIDE] = $order->getRemoteIp();
        }
        
        // Customer Custom Dimensions
        $customDimensions = Mage::helper('googleuniversalanalytics')->getCustomDimensions($order->getCustomerId()?Mage::getModel('customer/customer')->load($order->getCustomerId()):null);
        foreach($customDimensions as $customDimensionIndex => $customDimensionValue) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_DIMENSION . $customDimensionIndex] = $customDimensionValue;
        }
        
        // Customer Custom Metrics
        $customMetrics = Mage::helper('googleuniversalanalytics')->getCustomMetrics($order->getCustomerId()?Mage::getModel('customer/customer')->load($order->getCustomerId()):null);
        foreach($customMetrics as $customMetricIndex => $customMetricValue) {
            $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CUSTOM_METRIC . $customMetricIndex] = $customMetricValue;
        }
        
        if (Mage::helper('wcooall')->isModuleEnabled('Ess_M2ePro')) {
            if(class_exists('Ess_M2ePro_Helper_Component_Amazon')) {
                try {
                    $collection = Mage::helper('M2ePro/Component_Amazon')->getCollection('Order');
                    $collection->getSelect()
                            ->joinLeft(
                                    array('so' => Mage::getSingleton('core/resource')->getTableName('sales/order')), '(so.entity_id = `main_table`.magento_order_id)', array('magento_order_num' => 'increment_id'))
                            ->where("`main_table`.`magento_order_id` = '" . $order->getId() . "'");
                    if ($collection->count() >= 1) {
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_NAME] = 'Marketplaces';
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_SOURCE] = 'Amazon';
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_MEDIUM] = 'M2ePro';
                    }
                } catch (Exception $e) {
                    Mage::helper('googleuniversalanalytics')->log($e->getMessage(), 'gua.m2epro.log');
                }
            }
            if(class_exists('Ess_M2ePro_Helper_Component_Ebay')) {
                try {
                    $collection = Mage::helper('M2ePro/Component_Ebay')->getCollection('Order');
                    $collection->getSelect()
                            ->joinLeft(
                                    array('so' => Mage::getSingleton('core/resource')->getTableName('sales/order')), '(so.entity_id = `main_table`.magento_order_id)', array('magento_order_num' => 'increment_id'))
                            ->where("`main_table`.`magento_order_id` = '" . $order->getId() . "'");
                    if ($collection->count() >= 1) {
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_NAME] = 'Marketplaces';
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_SOURCE] = 'eBay';
                        $params[Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::CAMPAIGN_MEDIUM] = 'M2ePro';
                    }
                } catch (Exception $e) {
                    Mage::helper('googleuniversalanalytics')->log($e->getMessage(), 'gua.m2epro.log');
                }
            }
        }
        return $params;
    }
    
    public function getEcHelper() {
        if(is_null($this->_enhancedEcommerceHelper)) {
            $this->_enhancedEcommerceHelper = Mage::helper('googleuniversalanalytics/ecommerce');
        }
        return $this->_enhancedEcommerceHelper;
    }
    
    public function getItemParams($item, $itemIncrement, $cancelMode = false) {
        $orderItem = $item->getOrderItem()?$item->getOrderItem():$item;
        $order = $orderItem->getOrder();
        $product = $orderItem->getProduct();
        $storeId = $order?$order->getStoreId():$orderItem->getStoreId();
        if(!$product && $orderItem->getProductId()) {
            //In magento < 1.7, there is no getProduct() on order item model
            $product = Mage::getModel('catalog/product')->setStoreId($storeId)->load($orderItem->getProductId());
        }
        $category = $this->getEcHelper()->getProductCategoryValue($product, true, $storeId);
        $brand = $this->getEcHelper()->getProductBrandValue($product, $storeId);
        $variant = $this->getEcHelper()->getProductVariantValue($product, $orderItem);
        $sku = $this->getEcHelper()->getProductSkuValue($product, $orderItem);
        
        
        
        
        
        
        $itemQty = round($item->getQty() ? $item->getQty() : $item->getQtyOrdered());
        if($cancelMode) {
            $itemQty = 0 - $itemQty;
        }
        $itemPrice = $this->getEcHelper()->getItemPriceForOrder($orderItem);
        $itemName = Mage::helper('googleuniversalanalytics')->formatData($item->getName());
        $params = array();
        
        $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_ID, $itemIncrement)] = $sku;
        $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_NAME, $itemIncrement)] = $itemName;
        $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_CATEGORY, $itemIncrement)] = Mage::helper('googleuniversalanalytics')->formatData($category);
        $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_BRAND, $itemIncrement)] = Mage::helper('googleuniversalanalytics')->formatData($brand);
        $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_VARIANT, $itemIncrement)] = $variant;
        $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_PRICE, $itemIncrement)] = $itemPrice;
        $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_QUANTITY, $itemIncrement)] = $itemQty;

        $customDimensions = Mage::helper('googleuniversalanalytics')->getCustomDimensionsForProduct($product, false);
        foreach($customDimensions as $customDimensionIndex => $customDimensionValue) {
            $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_CUSTOM_DIMENSION, $itemIncrement, $customDimensionIndex)] = $customDimensionValue;
        }
        $customMetrics = Mage::helper('googleuniversalanalytics')->getCustomMetricsForProduct($product, false);
        foreach($customMetrics as $customMetricIndex => $customMetricValue) {
            $params[sprintf(Webcooking_GoogleUniversalAnalytics_Model_Measurement_Protocol::PRODUCT_CUSTOM_METRIC, $itemIncrement, $customMetricIndex)] = $customMetricValue;
        }
        
        return $params;
    }
    
    
}
