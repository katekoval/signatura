<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Model_Session extends Mage_Core_Model_Session_Abstract {
    public function __construct() {
        $this->init('wc_gua');
        Mage::dispatchEvent('gua_session_init', array('gua_session' => $this));
     }
}
