<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Model_Catalog_Product_Type_Configurable extends Mage_Catalog_Model_Product_Type_Configurable  {

  public function getOrderOptions($product = null)
    {
        $options = Mage_Catalog_Model_Product_Type_Abstract::getOrderOptions($product);
        $options['attributes_info'] = $this->getSelectedAttributesInfo($product);
        if ($simpleOption = $this->getProduct($product)->getCustomOption('simple_product')) {
            $options['simple_name'] = $product->getName();
            $options['simple_sku']  = $product->getSku();
        }

        $options['product_calculations'] = self::CALCULATE_PARENT;
        $options['shipment_type'] = self::SHIPMENT_TOGETHER;

        return $options;
    }

}
