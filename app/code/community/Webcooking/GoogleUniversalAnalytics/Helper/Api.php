<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
require_once BP. '/lib/Google/vendor/autoload.php';
 
class Webcooking_GoogleUniversalAnalytics_Helper_Api extends Webcooking_All_Helper_Data {
    
    const WEBCOOKING_CLIENT_ID = '201334511358-3t5reuj3eos0f3f1lbu21hoqcqbhpf3f.apps.googleusercontent.com';
    const WEBCOOKING_CLIENT_SECRET = 'Qi5etCqv2q6uqsJWshETSs6E';
    const WEBCOOKING_DEVELOPER_KEY = 'AIzaSyBlbcJU6TM_OoZEPaOWXXsz5YHeB5ES67U';

    protected $_client = null;
    protected $_service = null;
    protected $_serviceReporting = null;

    public function useOwnApiProject() {
        return
                Mage::getStoreConfig('googleuniversalanalytics/api/client_id') &&
                Mage::getStoreConfig('googleuniversalanalytics/api/client_secret') &&
                Mage::getStoreConfig('googleuniversalanalytics/api/developer_key');
    }

    public function getAccessCode() {
        return Mage::getStoreConfig('googleuniversalanalytics/api/access_code');
    }

    public function getClient() {
        if (is_null($this->_client)) {
            $this->_client = new Google_Client ();
            $this->_client->setScopes(array(Google_Service_Analytics::ANALYTICS, Google_Service_Analytics::ANALYTICS_EDIT));
            $this->_client->setAccessType('offline');
            $this->_client->setApplicationName('Webcooking Google Universal Analytics');
            $this->_client->setRedirectUri('urn:ietf:wg:oauth:2.0:oob');

            if ($this->useOwnApiProject()) {
                $this->_client->setClientId(Mage::getStoreConfig('googleuniversalanalytics/api/client_id'));
                $this->_client->setClientSecret(Mage::getStoreConfig('googleuniversalanalytics/api/client_secret'));
                $this->_client->setDeveloperKey(Mage::getStoreConfig('googleuniversalanalytics/api/developer_key'));
            } else {
                $this->_client->setClientId(self::WEBCOOKING_CLIENT_ID);
                $this->_client->setClientSecret(self::WEBCOOKING_CLIENT_SECRET);
                $this->_client->setDeveloperKey(self::WEBCOOKING_DEVELOPER_KEY);
            }

            try {
                if (Mage::getStoreConfig('googleuniversalanalytics/api/access_token')/*Mage::getSingleton('adminhtml/session')->getGoogleOAuthAccessToken()*/) {
                    $this->_client->setAccessToken(Mage::getStoreConfig('googleuniversalanalytics/api/access_token'));
                } else if($this->getAccessCode()) {
                    $authResult = $this->_client->authenticate($this->getAccessCode());
                    if(is_array($authResult) && isset($authResult['error']) && $authResult['error'] =='invalid_grant') {
                        $config = new Mage_Core_Model_Config();
                        $config->saveConfig('googleuniversalanalytics/api/access_code', '');
                        Mage::app()->getCacheInstance()->cleanType('config');
                    }
                    $accessToken = $this->_client->getAccessToken();
                    //Mage::getSingleton('adminhtml/session')->setGoogleOAuthAccessToken($accessToken);
                    $config = new Mage_Core_Model_Config();
                    $config->saveConfig('googleuniversalanalytics/api/access_token', $accessToken['access_token']);
                    Mage::app()->getCacheInstance()->cleanType('config');
                    
                    $googleToken = json_decode ( $accessToken );
                    Mage::getSingleton('adminhtml/session')->setGoogleOAuthRefreshToken($googleToken->refresh_token);
                } 
            } catch (Exception $e) {
                $config = new Mage_Core_Model_Config();
                $config->saveConfig('googleuniversalanalytics/api/access_token', '');
                Mage::app()->getCacheInstance()->cleanType('config');
                //Mage::helper('googleapi')->logException($e, 'gua');
            }
        }
        return $this->_client;
    }

    public function getService() {
        if(!$this->getClient()) {
            return false;
        }
        if (is_null($this->_service)) {
            $this->_service = new Google_Service_Analytics($this->getClient());
        }
        return $this->_service;
    }
    
    public function getServiceReporting() {
        if(!$this->getClient()) {
            return false;
        }
        if (is_null($this->_serviceReporting)) {
            $this->_serviceReporting = new Google_Service_AnalyticsReporting($this->getClient());
        }
        return $this->_serviceReporting;
    }
    

    public function getProfiles() {
        if (!$this->getService() || !$this->getService()->management_profiles) {
            return array();
        }
        try {
            $profiles = $this->getService()->management_profiles->listManagementProfiles('~all', '~all');
            return $profiles->getItems();
        } catch(Exception $e) {
            $config = new Mage_Core_Model_Config();
            $config->saveConfig('googleuniversalanalytics/api/access_token', '');
            Mage::app()->getCacheInstance()->cleanType('config');
            //Mage::helper('googleapi')->logException($e, 'gua');
        }
        return array();
    }

    
    public function getProfileForStore($storeId = 0) {
        foreach ($this->getProfiles() as $profile) {
            if($profile->getWebPropertyId() == Mage::helper('googleuniversalanalytics')->getAccountId($storeId)) {
                return $profile;
            }
        }
        return false;
    }
    
    
    public function getProfile($storeId = null) {
        return $this->getProfileForStore($storeId);
    }
    
}
