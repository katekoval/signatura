<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Helper_Dashboard extends Webcooking_All_Helper_Data {
    
    public function getApiHelper() {
        return Mage::helper('googleuniversalanalytics/api');
    }
    
    

    public function getChartData($from, $to = false, $query = 'visitors', $projectId=false) {
        $resultData = array();
        if(!$projectId) {
            $projectId = $this->getApiHelper()->getProfile();
        }
        if(!$projectId) {
            return false;
        }
        
        $metrics = 'ga:' . $query;

        if(!$from) {
            $from = date('Y-m-d');
        }
        if (!$to) {
            $to = $from;
        }

        if ($from == $to) {
            $dimensions = 'ga:hour';
        } else {
            $dimensions = 'ga:year,ga:month,ga:day';
        }

        $cacheId = 'gua_dash_' . $from . $to . $query . $projectId . '_cache';
        $tryCache = true;
        if ($to == date('Y-m-d') || $from == date('Y-m-d')) {
            $tryCache = false;
        }

        if ($tryCache) {
            $resultData = Mage::app()->loadCache($cacheId);
            if($resultData) {
                return $resultData;
            }
        }
        try {
            $data = Mage::helper('googleuniversalanalytics/api')->getService()->data_ga->get('ga:' . $projectId, $from, $to, $metrics, array(
                'dimensions' => $dimensions
            ));
        } catch (Exception $e) {
            Mage::helper('googleapi')->logException($e, 'gua');
        }

        $jsonData = "";

        //var_dump($data);
        if ($dimensions == 'ga:hour') {
            for ($i = 0; $i < $data['totalResults']; $i ++) {
                $jsonData .= "['" . $data['rows'][$i][0] . ":00'," . round($data['rows'][$i][1], 2) . "],";
            }
        } else {
            for ($i = 0; $i < $data['totalResults']; $i ++) {
                $jsonData .= "['" . $data['rows'][$i][0] . "-" . $data['rows'][$i][1] . "-" . $data['rows'][$i][2] . "'," . round($data['rows'][$i][3], 2) . "],";
            }
        }
        
        //var_dump($jsonData);
        $resultData['json'] = $jsonData;
        $resultData['totalResults'] = $data['totalResults'];
        $resultData['totalsForAllResults'] = $data['totalsForAllResults'];
        
        if ($tryCache) {
            Mage::app()->saveCache($resultData, $cacheId, array(
                'reports',
                'store'
            ));
        }
        return $resultData;
    }

}
