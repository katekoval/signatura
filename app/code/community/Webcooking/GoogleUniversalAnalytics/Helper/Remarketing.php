<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Helper_Remarketing extends Webcooking_All_Helper_Data {

    const PAGE_TYPE_PRODUCT = 'product';
    const PAGE_TYPE_CATEGORY = 'category';
    const PAGE_TYPE_PURCHASE = 'purchase';
    const PAGE_TYPE_SEARCH_RESULTS = 'searchresults';
    const PAGE_TYPE_HOME = 'home';
    const PAGE_TYPE_CART = 'cart';
    const PAGE_TYPE_OTHER = 'other';

    public function getPageType() {
        $request = Mage::app()->getRequest();
        $_product = Mage::registry('current_product');
        if ($_product && $_product->getId()) {
            return self::PAGE_TYPE_PRODUCT;
        }

        $_category = Mage::registry('current_category');
        if ($_category && $_category->getId()) {
            return self::PAGE_TYPE_CATEGORY;
        }

    
        if ($request->getModuleName() == 'checkout' && $request->getControllerName() == 'onepage' && $request->getActionName() == 'success') {
            return self::PAGE_TYPE_PURCHASE;
        }
        
        if (in_array($request->getModuleName(), array('checkout', 'onestepcheckout', 'onepagecheckout', 'firecheckout', 'singlepagecheckout', 'onepage', 'speedycheckout'))) {
            return self::PAGE_TYPE_CART;
        }

        if (in_array($request->getModuleName(), array('catalogsearch', 'solrsearch')) && $request->getControllerName() == 'result') {
            return self::PAGE_TYPE_SEARCH_RESULTS;
        }

        if ($request->getModuleName() == 'cms' && $request->getControllerName() == 'index' && $request->getActionName() == 'index') {
            return self::PAGE_TYPE_HOME;
        }

        return self::PAGE_TYPE_OTHER;
    }

    
    
    public function getProdId() {
        $useProductId = Mage::getStoreConfigFlag('googleuniversalanalytics/remarketing/use_product_id');
        switch($this->getPageType()) {
            case self::PAGE_TYPE_PRODUCT:
                $_product = Mage::registry('current_product');
                $prodIds = array();
                if($_product->getTypeId() != 'configurable' || in_array(Mage::getStoreConfig('googleuniversalanalytics/remarketing/show_configurable'), array(Webcooking_GoogleUniversalAnalytics_Model_Adminhtml_System_Config_Source_Configurable::CONFIGURABLE_ONLY, Webcooking_GoogleUniversalAnalytics_Model_Adminhtml_System_Config_Source_Configurable::CONFIGURABLE_AND_SIMPLES)) ) {
                    $prodIds[] = "'" . ($useProductId?$_product->getId():$_product->getSku()) . "'";
                }
                if($_product->getTypeId() == 'configurable' && in_array(Mage::getStoreConfig('googleuniversalanalytics/remarketing/show_configurable'), array(Webcooking_GoogleUniversalAnalytics_Model_Adminhtml_System_Config_Source_Configurable::SIMPLES_ONLY, Webcooking_GoogleUniversalAnalytics_Model_Adminhtml_System_Config_Source_Configurable::CONFIGURABLE_AND_SIMPLES) )) {
                   $childProducts = $_product->getTypeInstance()->getUsedProducts(null, $_product);
                   foreach($childProducts as $childProduct) {
                       if(!$childProduct->isSalable()) continue;
                       $prodIds[] = "'" . ($useProductId?$childProduct->getId():$childProduct->getSku()) . "'";
                   }
                }
                if(count($prodIds) == 1) {
                    return current($prodIds);
                }
                return '[' . implode(',', $prodIds) . ']';
            case self::PAGE_TYPE_CART:
                $_cart = Mage::getSingleton('checkout/cart');
                $prodIds = array();
                if($_cart->getQuote()) {
                    foreach($_cart->getQuote()->getAllVisibleItems() as $item) {
                        $prodIds[] = "'" . ($useProductId?$item->getProductId():$item->getSku()) . "'";
                    }
                }
                return '[' . implode(',', $prodIds) . ']';
            case self::PAGE_TYPE_PURCHASE:
                $orderId = Mage::getSingleton('checkout/session')->getLastOrderId();
                $order = Mage::getModel('sales/order')->load($orderId);
                $prodIds = array();
                foreach($order->getAllVisibleItems() as $item) {
                    $prodIds[] = "'" . ($useProductId?$item->getProductId():$item->getSku()) . "'";
                }
                return '[' . implode(',', $prodIds) . ']';
        }
        return "''";
    }
    
    
    public function getTotalValue() {
        switch($this->getPageType()) {
            case self::PAGE_TYPE_PRODUCT:
                $_product = Mage::registry('current_product');
                $prodValues = array();
                if($_product->getTypeId() != 'configurable' || in_array(Mage::getStoreConfig('googleuniversalanalytics/remarketing/show_configurable'), array(Webcooking_GoogleUniversalAnalytics_Model_Adminhtml_System_Config_Source_Configurable::CONFIGURABLE_ONLY, Webcooking_GoogleUniversalAnalytics_Model_Adminhtml_System_Config_Source_Configurable::CONFIGURABLE_AND_SIMPLES) )) {
                    $prodValues[] = floatval(round($_product->getFinalPrice()?$_product->getFinalPrice():$_product->getPrice(), 2));
                }
                if($_product->getTypeId() == 'configurable' && in_array(Mage::getStoreConfig('googleuniversalanalytics/remarketing/show_configurable'), array(Webcooking_GoogleUniversalAnalytics_Model_Adminhtml_System_Config_Source_Configurable::SIMPLES_ONLY, Webcooking_GoogleUniversalAnalytics_Model_Adminhtml_System_Config_Source_Configurable::CONFIGURABLE_AND_SIMPLES) )) {
                   $childProducts = $_product->getTypeInstance()->getUsedProducts(null, $_product);
                   if($this->isModuleEnabled('Amasty_Conf')) {
                       foreach($childProducts as $childProduct) {
                           if(!$childProduct->isSalable()) continue;
                           $prodValues[] = floatval(round($childProduct->getFinalPrice()?$childProduct->getFinalPrice():$childProduct->getPrice(), 2));;
                       }
                   } else {
                        $block = Mage::app()->getLayout()->createBlock('catalog/product_view_type_configurable');
                        $pricesConfig = Mage::helper('core')->jsonDecode($block->getJsonConfig());
                        foreach($childProducts as $childProduct) {
                            if(!$childProduct->isSalable()) continue;
                            $price = $pricesConfig['basePrice'];
                            foreach($pricesConfig['attributes'] as $attributeData) {
                                foreach($attributeData['options'] as $optionData) {
                                    if($optionData['id'] == $childProduct->getData($attributeData['code'])) {
                                        $price += $optionData['price'];
                                    }
                                }
                            }
                            $prodValues[] = $price;
                        }
                   }
                }
                if(count($prodValues) == 1) {
                    return current($prodValues);
                }
                return '[' . implode(',', $prodValues) . ']';
            case self::PAGE_TYPE_CART:
                $_cart = Mage::getSingleton('checkout/cart');
                $useStoreCurrency = Mage::helper('googleuniversalanalytics/ecommerce')->useStoreCurrency($_cart->getQuote()->getStoreId());
                $grandTotal = $useStoreCurrency ? $_cart->getQuote()->getGrandTotal() : $_cart->getQuote()->getBaseGrandTotal();
                return round($grandTotal, 2);
            case self::PAGE_TYPE_PURCHASE:
                $orderId = Mage::getSingleton('checkout/session')->getLastOrderId();
                $order = Mage::getModel('sales/order')->load($orderId);
                $useStoreCurrency = Mage::helper('googleuniversalanalytics/ecommerce')->useStoreCurrency($order->getStoreId());
                $grandTotal = $useStoreCurrency ? $order->getGrandTotal() : $order->getBaseGrandTotal();
                return round($grandTotal, 2);
        }
        return 0;
    }
}
