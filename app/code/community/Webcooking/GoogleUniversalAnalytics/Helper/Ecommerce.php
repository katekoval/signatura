<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Helper_Ecommerce extends Webcooking_All_Helper_Data {
 
    
   protected $_brandAttributeCode = null;


   public function getBrandAttributeCode($store = null) {
       if(is_null($this->_brandAttributeCode)) {
           $this->_brandAttributeCode = Mage::getStoreConfig('googleuniversalanalytics/enhanced_ecommerce/brand', $store);
       }
       return $this->_brandAttributeCode;
   }

   
    public function useStoreCurrency($store = null) {
        return Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/use_store_currency', $store);
    }
    
    public function sendChildrenItems($store = null) {
        return Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/send_children_items', $store);
    }
   
   public function getProductAttributeValue($product, $attributeCode, $storeId = false) {
       if(!$product || !$product->getId()) {
           return '';
       }
       if(!$storeId) {
           $storeId = Mage::helper('wcooall')->getCurrentStoreId();
       }
       $attribute = Mage::helper('wcooall/attribute')->getAttribute($attributeCode);
       if(!$attribute->getId()) {
           return '';
       }
       if($product->getData($attributeCode)) {
           $data = $product->getData($attributeCode);
       } else {
            $data = Mage::getSingleton('catalog/product')->getResource()->getAttributeRawValue($product->getId(), $attributeCode, $storeId);
       }
       if(!$data) {
           return '';
       }
       if (preg_match('%[0-9,]+%i', $data) && $attribute->usesSource() && $attribute->getSource()->getOptionText($data)) {
            $data = $attribute->getSource()->getOptionText($data);
            if(is_array($data)) {
                $data = implode(', ', $data);
            }
       }
       return $this->formatData($data);
   }

   public function getProductPriceValue($product) {
       if($product->getTypeId() == 'grouped') {
           if($product->getMinimalPrice()) {
               return floatval($product->getMinimalPrice());
           }
            $_associatedProducts = $product->getTypeInstance(true)->getAssociatedProducts($product);
            $minPrice = PHP_INT_MAX;
            foreach($_associatedProducts as $_associatedProduct) {
                if($_associatedProduct->getPrice()) {
                    $minPrice = min($_associatedProduct->getPrice(),$minPrice);
                }
            }
            if($minPrice == PHP_INT_MAX) {
                $minPrice = 0;
            }
            return floatval($minPrice);
       }
       return floatval($product->getFinalPrice());
   }
           
   protected $_categoryName = array();
   public function getProductCategoryValue($product, $useCurrentCategory = true, $storeId = null) {
       if(!$product || !$product->getId()) {
           return '';
       }
       if(!$storeId) {
           $storeId = Mage::helper('wcooall')->getCurrentStoreId();
       }
       $categoryId = false;
       $sessionCache = Mage::getSingleton('core/session')->getGuaProductCategoryValue();
       if(!is_array($sessionCache)) {
            $sessionCache = array();
       }
       if(!isset($sessionCache[$product->getId().'-'.$storeId])) {
           $sessionCache[$product->getId().'-'.$storeId] = false;
            $lastCategoryViewed = Mage::getSingleton('catalog/session')->getLastViewedCategoryId();
            if($lastCategoryViewed && in_array($lastCategoryViewed, $product->getCategoryIds())) {
                $sessionCache[$product->getId().'-'.$storeId] = $lastCategoryViewed;
            } else if(Mage::helper('wcooall')->isModuleEnabled('Webcooking_MainCategory')) {
                $sessionCache[$product->getId().'-'.$storeId] = $product->getMainCategory();
            }
            if(!$sessionCache[$product->getId().'-'.$storeId]) {
                $category = Mage::registry('current_category');
                if($useCurrentCategory && $category && $category->getId()) {
                    $sessionCache[$product->getId().'-'.$storeId] = $category->getId();
                } else {
                    $rootCategoryId = Mage::app()->getStore($storeId)->getRootCategoryId();
                    $sessionCache[$product->getId().'-'.$storeId] = false;
                    /*$categoryIds = $product->getCategoryIds();
                    $categoryId = array_pop($categoryIds);*/
                    $categoryCollection = $product->getCategoryCollection();
                    $categoryCollection->setStoreId($storeId);
                    $categoryCollection->addAttributeToSelect('is_active', 'left');
                    $categoryCollection->addAttributeToSelect('path', 'left');
                    $categoryCollection->setOrder('entity_id', 'ASC');
                    foreach($categoryCollection as $category) {
                        $isActive = Mage::getResourceModel('catalog/category')->getAttributeRawValue($category->getId(), 'is_active', $storeId);
                       if($isActive && $category->getLevel() > 1 && preg_match('%^1/' . $rootCategoryId . '/%', $category->getPath())) {
                            $sessionCache[$product->getId().'-'.$storeId] = $category->getId();
                        }
                    }

                }
            }
       }
       $categoryId = $sessionCache[$product->getId().'-'.$storeId];
       Mage::getSingleton('core/session')->setGuaProductCategoryValue($sessionCache);
       
       
       if(!$categoryId) {
           return '';
       }
       
       // too long if many categories 
       // return $this->formatData(Mage::helper('wcooall/category')->getCategoryNameById($categoryId, '/', 5));
       if(!isset($this->_categoryName[$categoryId])) {
           $category = Webcooking_All_Helper_Data::getSavedCategory($categoryId);
           $this->_categoryName[$categoryId] = $category->getName();
           while($category->getLevel() > 2) {
               $category = Webcooking_All_Helper_Data::getSavedCategory($category->getParentId());//$category->getParentCategory();
               $this->_categoryName[$categoryId] = $category->getName() . '/' . $this->_categoryName[$categoryId];
           }
       }
       
       return $this->formatData($this->_categoryName[$categoryId]);
   }

   public function formatData($data) {
       return Mage::helper('googleuniversalanalytics')->formatData($data);
   }

   public function getDefaultListName($useCategoryName = true) {
       if(Mage::app()->getFrontController()->getAction()) {
          $listName = Mage::app()->getFrontController()->getAction()->getFullActionName();
       } else {
           //sphinx compatibility
          $listName = Mage::app()->getRequest()->getModuleName() .'_'.Mage::app()->getRequest()->getControllerName() .'_'.Mage::app()->getRequest()->getActionName();
       }
       switch($listName) {
           case 'cms_index_index':
               $listName = $this->__('Homepage');
               break;
           case 'catalog_category_view':
               $category = Mage::registry('current_category');
               if($category && $category->getId() && $useCategoryName) {
                   $listName = $this->__('Category %s', $category->getName());
               } else {
                   $listName = $this->__('Category view');
               }
               break;
           case 'promotions_promotions_index':
               $listName = $this->__('Promotions');
               break;
           case 'solrsearch_result_index':
           case 'catalogsearch_result_index':
               $listName = $this->__('Search results');
               break;
           case 'advancedcms_page_view':
           case 'cms_page_view':
               $listName = $this->__('CMS page view');
               break;
           case 'nouveautes-new-index':
               $listName = $this->__('Recent products');
               break;
           case 'umicrosite_index_landingPage': 
               $_vendor = Mage::helper('umicrosite')->getCurrentVendor();
               if($_vendor) {
                   $listName = $this->__('Vendor %s landing page', $_vendor->getVendorName());
               } else {
                    $listName = $this->__('Vendor landing page');
               }
               break;
       }
       return $listName;
   }
   
   

   public function getAddPromoImpressionTag($id, $name='', $creative='', $position=false, $withScriptTags = true) {
       //DEPRECATED
       return '';
       
       
       if(!Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsAvailable()) {
           return '';
       }
       
       if(!$position) {
           $position = $this->getPromoPosition();
       } else {
           $this->incrementPromoPosition();
       }
       
       $html = '';
       if($withScriptTags) {
           $html .= '<script type="text/javascript">';
       }
       
       $html .= sprintf("
                    promoImpressions.push({
                        'id': '%s',
                        'name': '%s',
                        'creative': '%s',
                        'position': '%s'
                    });
                    ",
                    $this->formatData($id),
                    $this->formatData($name),
                    $this->formatData($creative),
                    $this->formatData($position)
                );


       if($withScriptTags) {
           $html .= '</script>';
       }
       return $html;

   }
   
   public function getGlobalImpressionCount() {
        return Mage::registry('gua_ec_impression_count')?Mage::registry('gua_ec_impression_count'):0;
   }
   public function getProductPosition($listName, $incrementFirst = true) {
       if($incrementFirst) {
           $this->incrementProductPosition($listName);
       }
       $count = Mage::registry('gua_ec_product_impression_count');
       if($count && isset($count[$listName])) {
           return $count[$listName];
       }
       return 0;
   }
   public function getPromoPosition($incrementFirst = true) {
       if($incrementFirst) {
           $this->incrementPromoPosition();
       }
       return Mage::registry('gua_ec_promo_impression_count')?Mage::registry('gua_ec_promo_impression_count'):0;
   }
   
   public function incrementProductPosition($listName) {
       Mage::getSingleton('core/session')->setShouldSendImpressionEventTag(true);
       $globalCount = (int)Mage::registry('gua_ec_impression_count');
       $count = Mage::registry('gua_ec_product_impression_count');
       if(!is_array($count)) {
           $count = array();
       }
       if(!isset($count[$listName])) {
           $count[$listName] = 0;
       }
       $count[$listName]++;
       $globalCount++;
       Mage::unregister('gua_ec_impression_count');
       Mage::unregister('gua_ec_product_impression_count');
       Mage::register('gua_ec_impression_count', $globalCount);
       Mage::register('gua_ec_product_impression_count', $count);
   }
   public function incrementPromoPosition() {
       Mage::getSingleton('core/session')->setShouldSendImpressionEventTag(true);
       $globalCount = (int)Mage::registry('gua_ec_impression_count');
       $count = (int)Mage::registry('gua_ec_promo_impression_count');
       Mage::unregister('gua_ec_impression_count');
       Mage::unregister('gua_ec_promo_impression_count');
       Mage::register('gua_ec_impression_count', $globalCount+1);
       Mage::register('gua_ec_promo_impression_count', $count+1);
   }

   public function getAddProductImpressionTag($product, $listName=false, $position = false, $withScriptTags = true) {
       //DEPRECATED
       return '';
       if(!Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsAvailable() || !$product || !$product->getId()) {
           return '';
       }
       
       if(!$position) {
           $position = $this->getProductPosition($listName);
       } else {
           $this->incrementProductPosition($listName);
       }
     
       
       $html = '';
       
       
       if($withScriptTags) {
           $html .= '<script type="text/javascript">';
       }
       
       

       //$currency = Mage::app()->getStore()->getCurrentCurrencyCode();
       $brandValue = $this->getProductBrandValue($product);
       $nameValue = $this->getProductNameValue($product);
       $variantValue = $this->getProductVariantValue($product);
       $skuValue = $this->getProductSkuValue($product);
       $categoryValue = $this->getProductCategoryValue($product);
       $priceValue = $this->getProductPriceValue($product);

       if(!$listName) {
           $listName = $this->getDefaultListName();
       }

       
       $html .= sprintf("
           productImpressions.push({
                'id': '%s',
                'name': '%s',
                'category': '%s',
                'brand': '%s',
                'variant': '%s',
                'price': '%s',
                'list': '%s',
                'position': %s
           });
            ",
            $skuValue,
            $nameValue,
            $categoryValue,
            $brandValue,
            $variantValue,
            $priceValue,
            $this->formatData($listName),
            intval($position)
        );


       if($withScriptTags) {
           $html .= '</script>';
       }
       
       $this->saveLastListName($listName);
       
       return $html;
   }
   
   public function saveLastListName($listName) {
       Mage::getSingleton('core/session')->setLastListName($listName);
   }
   
   public function getAddProductDetailsTag($product, $withScriptTags = false) {
        if(!$product) {
            return '';
        }
       if(!Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsAvailable()) {
            return '';
        }
        if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
            return Mage::helper('googleuniversalanalytics/gtm')->getAddProductDetailsTag($product, $withScriptTags, false);
        }
        
        $html = '';
        
        if($withScriptTags) {
            $html = '<script>';
        } 
        
       
        if($product->getTypeId() == 'configurable' && Mage::getStoreConfigFlag('googleuniversalanalytics/enhanced_ecommerce/track_variants')) {
             $html .= 'Product.Config.prototype.origConfigureGUA = Product.Config.prototype.configure;
                    Product.Config.prototype.configure = function(event){               
                        this.origConfigureGUA(event);
                        sendGuaProductVariantDetails(this);
                    };';
         }
         
         
         $html .= sprintf("
             var productDetail = {
                 'id': '%s',
                 'name': '%s',
                 'category': '%s',
                 'brand': '%s',
                 'price': '%s',
                 'variant': '%s'
             };
             gtag('event', 'view_item', {
                'items': [
                    productDetail
                ]
             });",
             $this->getProductSkuValue($product),
             $this->getProductNameValue($product),
             $this->getProductCategoryValue($product), 
             $this->getProductBrandValue($product), 
             $this->getProductPriceValue($product),
             ''
         );

        
        if($withScriptTags) {
           $html .= '</script>'; 
        }
        
        return $html;
   }



   public function getProductBrandValue($product, $storeId = null) {
       $brandAttributeCode = $this->getBrandAttributeCode();
       $brandValue = $this->formatData($this->getProductAttributeValue($product, $brandAttributeCode, $storeId));
        
       $brandObj = new Varien_Object(array('value' => $brandValue));
       Mage::dispatchEvent('gua_get_product_brand_value', array('brand' => $brandObj, 'product'=>$product));
       $brandValue = $brandObj->getValue();
       
       return $brandValue;
   }

   public function getProductNameValue($product, $storeId = null) {
       $nameValue = $this->getProductAttributeValue($product, 'name', $storeId);
       return $nameValue;
   }

   public function getProductVariantValue($product, $orderItem = null) {
       if (!$product) {
            return '';
        }
        try {
            if ($orderItem && get_class($orderItem) == 'Mage_Sales_Model_Order_Item') {
                $options = array();
                if ($productOptions = $orderItem->getProductOptions()) {
                    if (isset($productOptions['options'])) {
                        $options = array_merge($options, $productOptions['options']);
                    }
                    if (isset($productOptions['additional_options'])) {
                        $options = array_merge($options, $productOptions['additional_options']);
                    }
                    if (!empty($productOptions['attributes_info'])) {
                        $options = array_merge($productOptions['attributes_info'], $options);
                    }
                }
            } else {
                $options = array();
                if( $product->getTypeId() == 'configurable' ) {
                    $options = $product->getTypeInstance(true)->getSelectedAttributesInfo($product);
                }
                //$productOptions = $product->getTypeInstance(true)->getOrderOptions($product);
                $typeInstance = new Webcooking_GoogleUniversalAnalytics_Model_Catalog_Product_Type_Configurable();
                $productOptions = $typeInstance->setProduct($product)->getOrderOptions($product);
                if (isset($productOptions['options'])) {
                    $options = array_merge($options, $productOptions['options']);
                }
            }
            
            
            
        } catch (Exception $e) {
            return '';
        }
        $variant = array();
        foreach ($options as $option) {
            $variant[] = $option['value'];
        }
        return $this->formatData(implode(' - ', $variant));
    }

   public function getProductSkuValue($product) {
       $skuValue = $this->getProductAttributeValue($product, 'sku');
       return $skuValue;
   }

   public function getProductLinkData($product, $listName=false, $position = false, $noredirect=false) {
       if(!Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsAvailable() || !$product || !$product->getId()) {
           return '';
       }
       
       $html = ' ';

       $brandValue = $this->getProductBrandValue($product);
       $nameValue = $this->getProductNameValue($product);
       $variantValue = $this->getProductVariantValue($product);
       $skuValue = $this->getProductSkuValue($product);
       $categoryValue = $this->getProductCategoryValue($product);
       $priceValue = $this->getProductPriceValue($product);

       if(!$listName) {
           $listName = $this->getDefaultListName();
       }

       $html .= 'data-gua-ec-id="' . $skuValue . '" ';
       $html .= 'data-gua-ec-name="' . $nameValue . '" ';
       $html .= 'data-gua-ec-category="' . $categoryValue . '" ';
       $html .= 'data-gua-ec-brand="' . $brandValue . '" ';
       $html .= 'data-gua-ec-variant="' . $variantValue . '" ';
       $html .= 'data-gua-ec-list="' . $this->formatData($listName) . '" ';
       $html .= 'data-gua-ec-price="' . $priceValue . '" ';
       $html .= 'data-gua-ec-position="' . intval($position) . '" ';
       if($noredirect) {
            $html .= 'data-gua-ec-noredirect="1" ';
       }

       $this->saveLastListName($listName);
       return $html;
   }



   public function getPromoLinkData($id, $name='', $creative='', $position=false, $noredirect=false) {
       if(!Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsAvailable() ) {
           return '';
       }
       $html = ' ';

       $type = 'gua';
       if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
           $type = 'gtm';
       }
       
       $html .= 'data-gua-ec-promo-id="' . $this->formatData($id) . '" ';
       $html .= 'data-gua-ec-promo-name="' . $this->formatData($name) . '" ';
       $html .= 'data-gua-ec-promo-creative="' . $this->formatData($creative) . '" ';
       $html .= 'data-gua-ec-promo-position="' . $this->formatData($position) . '" ';
       if($noredirect) {
            $html .= 'data-gua-ec-promo-noredirect="1" ';
       }


       return $html;
   }

   public function getEnhancedOrdersTrackingCode($orderIds) {
        if(Mage::getStoreConfig('googleuniversalanalytics/transactions/use_mp')) {
            return '';
        }
        if (empty($orderIds) || !is_array($orderIds)) {
            return '';
        }
        if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
            return Mage::helper('googleuniversalanalytics/gtm')->getEnhancedOrdersTrackingCode($orderIds, false);
        }
       
        $collection = Mage::getResourceModel('sales/order_collection')
            ->addFieldToFilter('entity_id', array('in' => $orderIds))
        ;
       
        foreach ($collection as $order) {
            if ($order->getIsVirtual()) {
                $address = $order->getBillingAddress();
            } else {
                $address = $order->getShippingAddress();
            }
            
        
            $items = array();
            foreach ($order->getAllItems() as $item) {
                if ($item->isDeleted() || ($item->getParentItemId() && !$this->sendChildrenItems())) {
                    continue;
                }
                $items[] = sprintf("
                    {
                    'id': '%s',
                    'name': '%s',
                    'category': '%s',
                    'brand': '%s',
                    'variant': '%s',
                    'price': '%s',
                    'quantity': '%s'
                   }
                    ",
                    $this->jsQuoteEscape($item->getSku()),
                    $this->jsQuoteEscape($item->getName()),
                    $this->jsQuoteEscape($this->getProductCategoryValue($item->getProduct())),
                    $this->jsQuoteEscape($this->getProductBrandValue($item->getProduct())),
                    $this->jsQuoteEscape($this->getProductVariantValue($item->getProduct(), $item)),
                    Mage::helper('googleuniversalanalytics/ecommerce')->getItemPriceForOrder($item),
                    round($item->getQtyOrdered())
                );
            }


               $result[] = sprintf("
                    gtag('event', 'purchase', {
                        'transaction_id': '%s',                         
                        'affiliation': '%s',
                        'value': '%s', 
                        'currency': '%s',
                        'tax':'%s',
                        'shipping': '%s',
                        'coupon': '%s',
                        'items': [%s]
                    });
                 ",
                Mage::helper('googleuniversalanalytics/ecommerce')->getOrderId($order),
                null,
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionRevenueForOrder($order),
                $order->getOrderCurrencyCode(),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionTaxForOrder($order),
                Mage::helper('googleuniversalanalytics/ecommerce')->getTransactionShippingForOrder($order),
                strtoupper($order->getCouponCode()),
                implode(',', $items)
            );


        }
        $guaCode =  implode("\n", $result);
        
        return $guaCode;
    }
    
    
    public function getTransactionRevenueForOrder($order) {
        $storeId = $order->getStoreId();
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $grandTotal = $useStoreCurrency ? $order->getGrandTotal() : $order->getBaseGrandTotal();
        $taxAmount = $useStoreCurrency ? $order->getTaxAmount() : $order->getBaseTaxAmount();
        $shippingAmount = $useStoreCurrency ? $order->getShippingAmount() : $order->getBaseShippingAmount();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_revenue_inc_tax', $storeId) && Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_revenue_inc_shipping', $storeId)) {
            return $grandTotal;
        } else if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_revenue_inc_shipping', $storeId)) {
            return $grandTotal - $taxAmount;
        } else if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_revenue_inc_tax', $storeId)) {
            return $grandTotal - $shippingAmount;
        } 
        return $grandTotal - $taxAmount - $shippingAmount;
    }
    
    
    public function getTransactionRevenueForInvoice($invoice) {
        $storeId = $invoice->getOrder()->getStoreId();
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $grandTotal = $useStoreCurrency ? $invoice->getGrandTotal() : $invoice->getBaseGrandTotal();
        $taxAmount = $useStoreCurrency ? $invoice->getTaxAmount() : $invoice->getBaseTaxAmount();
        $shippingAmount = $useStoreCurrency ? $invoice->getShippingAmount() : $invoice->getBaseShippingAmount();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_revenue_inc_tax', $storeId) && Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_revenue_inc_shipping', $storeId)) {
            return $grandTotal;
        } else if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_revenue_inc_shipping', $storeId)) {
            return $grandTotal - $taxAmount;
        } else if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_revenue_inc_tax', $storeId)) {
            return $grandTotal - $shippingAmount;
        } 
        return $grandTotal - $taxAmount - $shippingAmount;
    }
    
    public function getTransactionShippingForOrder($order) {
        $storeId = $order->getStoreId();
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $shippingAmount = $useStoreCurrency ? $order->getShippingAmount() : $order->getBaseShippingAmount();
        $shippingTaxAmount = $useStoreCurrency ? $order->getShippingTaxAmount() : $order->getBaseShippingTaxAmount();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_shipping_inc_tax', $storeId)) {
           return $shippingAmount + $shippingTaxAmount;
        } 
        return $shippingAmount;
    }
    
    public function getTransactionShippingForInvoice($invoice) {
        $storeId = $invoice->getOrder()->getStoreId();
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $shippingAmount = $useStoreCurrency ? $invoice->getShippingAmount() : $invoice->getBaseShippingAmount();
        $shippingTaxAmount = $useStoreCurrency ? $invoice->getShippingTaxAmount() : $invoice->getBaseShippingTaxAmount();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_shipping_inc_tax', $storeId)) {
           return $shippingAmount + $shippingTaxAmount;
        } 
        return $shippingAmount;
    }
    
    public function getTransactionTaxForOrder($order) {
        $storeId = $order->getStoreId();
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $taxAmount = $useStoreCurrency ? $order->getTaxAmount() : $order->getBaseTaxAmount();
        $shippingTaxAmount = $useStoreCurrency ? $order->getShippingTaxAmount() : $order->getBaseShippingTaxAmount();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_tax_inc_shipping_tax', $storeId)) {
            return $taxAmount;
        } 
        return $taxAmount - $shippingTaxAmount;
    }
    
    public function getTransactionTaxForInvoice($invoice) {
        $storeId = $invoice->getOrder()->getStoreId();
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $taxAmount = $useStoreCurrency ? $invoice->getTaxAmount() : $invoice->getBaseTaxAmount();
        $shippingTaxAmount = $useStoreCurrency ? $invoice->getShippingTaxAmount() : $invoice->getBaseShippingTaxAmount();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_tax_inc_shipping_tax', $storeId)) {
            return $taxAmount;
        } 
        return $taxAmount - $shippingTaxAmount;
    }
    
    public function getItemPriceForQuote($quoteItem) {
        $quote = $quoteItem->getQuote();
        $storeId = null;
        if($quote) {
            $storeId = $quote->getStoreId();
        }
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $priceInclTax = $useStoreCurrency ? $quoteItem->getPriceInclTax() : $quoteItem->getBasePriceInclTax();
        $price = $useStoreCurrency ? $quoteItem->getPrice() : $quoteItem->getBasePrice();
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_item_price_inc_tax', $storeId)) {
            return $priceInclTax?$priceInclTax:$price;
        } 
        return $price;
    }
    
    public function getItemPriceForOrder($orderItem) {
        $order = $orderItem->getOrder();
        $storeId = null;
        if($order) {
            $storeId = $order->getStoreId();
        }
        $weeeHelper = Mage::helper('weee');
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $price = $useStoreCurrency ? $orderItem->getRowTotal() - $orderItem->getDiscountAmount() : $orderItem->getBaseRowTotal() - $orderItem->getBaseDiscountAmount();
        $weeeAmount = method_exists($weeeHelper, 'getRowWeeeAmountAfterDiscount') ? $weeeHelper->getRowWeeeAmountAfterDiscount($orderItem) : $orderItem->getWeeeTaxAppliedRowAmount(); // For compatibility with prior to 1.9
        $weeeBaseAmount = method_exists($weeeHelper, 'getBaseRowWeeeAmountAfterDiscount') ? $weeeHelper->getBaseRowWeeeAmountAfterDiscount($orderItem) : $orderItem->getBaseWeeeTaxAppliedRowAmount(); // For compatibility with prior to 1.9
        $priceInclTax = $price + ($useStoreCurrency ? $orderItem->getTaxAmount() + $orderItem->getHiddenTaxAmount() + $weeeAmount :  $orderItem->getBaseTaxAmount() + $orderItem->getBaseHiddenTaxAmount() + $weeeBaseAmount);
        $price = round($price / max(1,$orderItem->getQtyOrdered()), 4);
        $priceInclTax = round($priceInclTax / max(1,$orderItem->getQtyOrdered()), 4);
        //$priceInclTax = $useStoreCurrency ? $orderItem->getPriceInclTax() - round($orderItem->getDiscountAmount()/max(1,$orderItem->getQtyOrdered()), 4) : $orderItem->getBasePriceInclTax() - round($orderItem->getBaseDiscountAmount()/max(1,$orderItem->getQtyOrdered()), 4);
        //$price = $useStoreCurrency ? $priceInclTax - round($orderItem->getTaxAmount()/max(1,$orderItem->getQtyOrdered()), 4) : $priceInclTax - round($orderItem->getBaseTaxAmount()/max(1,$orderItem->getQtyOrdered()), 4);
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_item_price_inc_tax', $storeId)) {
            return $priceInclTax > 0?$priceInclTax:$price;
        } 
        return $price;
    }
    
    public function getItemPriceForInvoice($invoiceItem) {
        $storeId = $invoiceItem->getInvoice()->getOrder()->getStoreId();
        $weeeHelper = Mage::helper('weee');
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $price = $useStoreCurrency ? $invoiceItem->getRowTotal() - $invoiceItem->getDiscountAmount() : $invoiceItem->getRowTotal() - $invoiceItem->getBaseDiscountAmount();
        $weeeAmount = method_exists($weeeHelper, 'getRowWeeeAmountAfterDiscount') ? $weeeHelper->getRowWeeeAmountAfterDiscount($invoiceItem) : $invoiceItem->getWeeeTaxAppliedRowAmount(); // For compatibility with prior to 1.9
        $weeeBaseAmount = method_exists($weeeHelper, 'getBaseRowWeeeAmountAfterDiscount') ? $weeeHelper->getBaseRowWeeeAmountAfterDiscount($invoiceItem) : $invoiceItem->getBaseWeeeTaxAppliedRowAmount(); // For compatibility with prior to 1.9
        $priceInclTax = $price + ($useStoreCurrency ? $invoiceItem->getTaxAmount() + $invoiceItem->getHiddenTaxAmount() + $weeeAmount :  $invoiceItem->getBaseTaxAmount() + $invoiceItem->getBaseHiddenTaxAmount() + $weeeBaseAmount);
        $price = round($price / max(1,$invoiceItem->getQty()), 4);
        $priceInclTax = round($priceInclTax / max(1,$invoiceItem->getQty()), 4);
        //$priceInclTax = $useStoreCurrency ? $invoiceItem->getPriceInclTax() - $invoiceItem->getDiscountAmount() : $invoiceItem->getBasePriceInclTax()- $invoiceItem->getBaseDiscountAmount();
        //$price = $useStoreCurrency ? $priceInclTax - round($invoiceItem->getTaxAmount()/$invoiceItem->getQty(), 2) : $priceInclTax - round($invoiceItem->getBaseTaxAmount()/$invoiceItem->getQty(), 2);
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_item_price_inc_tax', $storeId)) {
            $price = $priceInclTax > 0?$priceInclTax:$price;
        } 
        if(!$price && $invoiceItem->getOrderItem()) {
            return $this->getItemPriceForOrder($invoiceItem->getOrderItem());
        }
        return $price;
    }
    
    public function getItemPriceForCreditmemo($creditmemoItem) {
        $storeId = $creditmemoItem->getCreditmemo()->getOrder()->getStoreId();
        $weeeHelper = Mage::helper('weee');
        $useStoreCurrency = $this->useStoreCurrency($storeId);
        $price = $useStoreCurrency ? $creditmemoItem->getRowTotal() - $creditmemoItem->getDiscountAmount() : $creditmemoItem->getRowTotal() - $creditmemoItem->getBaseDiscountAmount();
        $weeeAmount = method_exists($weeeHelper, 'getRowWeeeAmountAfterDiscount') ? $weeeHelper->getRowWeeeAmountAfterDiscount($creditmemoItem) : $creditmemoItem->getWeeeTaxAppliedRowAmount(); // For compatibility with prior to 1.9
        $weeeBaseAmount = method_exists($weeeHelper, 'getBaseRowWeeeAmountAfterDiscount') ? $weeeHelper->getBaseRowWeeeAmountAfterDiscount($creditmemoItem) : $creditmemoItem->getBaseWeeeTaxAppliedRowAmount(); // For compatibility with prior to 1.9
        $priceInclTax = $price + ($useStoreCurrency ? $creditmemoItem->getTaxAmount() + $creditmemoItem->getHiddenTaxAmount() + $weeeAmount :  $creditmemoItem->getBaseTaxAmount() + $creditmemoItem->getBaseHiddenTaxAmount() + $weeeBaseAmount);
        $price = round($price / max(1,$creditmemoItem->getQty()), 4);
        $priceInclTax = round($priceInclTax / max(1,$creditmemoItem->getQty()), 4);
        if(Mage::getStoreConfigFlag('googleuniversalanalytics/transactions/transaction_item_price_inc_tax', $storeId)) {
            return $priceInclTax > 0?$priceInclTax:$price;
        } 
        return $price;
    }

    public function getTransactionAffiliation($order) {
        if($order->getRemoteIp()) {
            return $order->getStore()->getName();
        }
        //admin order
        return Mage::helper('googleuniversalanalytics')->__('Admin order (%s)', $order->getStore()->getName());
        
    }
    
    
    public function getOrderId($order) {
       $orderIdObj = new Varien_Object(array('value' => $order->getIncrementId()));
       Mage::dispatchEvent('gua_get_order_id', array('order_id_obj' => $orderIdObj, 'order'=>$order));
       return $orderIdObj->getValue() ? $orderIdObj->getValue() : $order->getIncrementId();
    }
    
}