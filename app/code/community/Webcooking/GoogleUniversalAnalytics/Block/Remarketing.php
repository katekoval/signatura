<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Block_Remarketing extends Mage_Core_Block_Template
{
   
    protected function _construct()
    {
        parent::_construct();
        $this->setCacheLifetime(null);
    }

    public function getConversionId() {
        return intval(Mage::helper('googleuniversalanalytics')->getRemarketingConversionId());
    }
    
    
    public function getPageType() {
        return Mage::helper('googleuniversalanalytics/remarketing')->getPageType();
    }
    
    public function getProdId() {
        return Mage::helper('googleuniversalanalytics/remarketing')->getProdId();
    }
    
    public function getTotalValue() {
        return Mage::helper('googleuniversalanalytics/remarketing')->getTotalValue();
    }
    
  
    
}
