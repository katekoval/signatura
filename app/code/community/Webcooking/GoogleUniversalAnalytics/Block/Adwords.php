<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Block_Adwords extends Mage_Core_Block_Template
{
    protected $_order = null;
    
    protected function _construct()
    {
        parent::_construct();
        $this->setCacheLifetime(null);
    }

    public function getOrder() {
        if(is_null($this->_order)) {
            $this->_order = false;
            $orderId = Mage::getSingleton('checkout/session')->getLastOrderId();
            if($orderId) {
                $this->_order = Mage::getModel('sales/order')->load($orderId);
            }
        }
        return $this->_order;
    }
    
    public function getConversionId() {
        return intval(Mage::helper('googleuniversalanalytics')->getAdwordsConversionId());
    }

    public function getConversionLabel() {
        return Mage::helper('googleuniversalanalytics')->getAdwordsConversionLabel();
    }
    
    
    /***
     ** @deprecated since version 1.8.0
     */
    public function getFormat() {
        return Mage::helper('googleuniversalanalytics')->getAdwordsConversionFormat();
    }

    
    public function getConversionCurrency() {
        $order = $this->getOrder();
        if($order && $order->getId()) {
            $useStoreCurrency = Mage::helper('googleuniversalanalytics/ecommerce')->useStoreCurrency($order->getStoreId());
            return  $useStoreCurrency ? $order->getStoreCurrencyCode() : $order->getBaseCurrencyCode();
        }
        return Mage::app()->getStore()->getCurrentCurrencyCode();
    }
    
    public function getConversionValue() {
        $configValue = intval(Mage::helper('googleuniversalanalytics')->getAdwordsConversionValue());
        if($configValue) {
            return $configValue;
        }
        $order = $this->getOrder();
        if($order && $order->getId()) {
            $useStoreCurrency = Mage::helper('googleuniversalanalytics/ecommerce')->useStoreCurrency($order->getStoreId());
            $grandTotal = $useStoreCurrency ? $order->getGrandTotal() : $order->getBaseGrandTotal();
            if(!Mage::helper('googleuniversalanalytics')->isAdwordsConversionValueInclTax()) {
                $grandTotal -= $useStoreCurrency ? $order->getTaxAmount() : $order->getBaseTaxAmount();
            }
            if(!Mage::helper('googleuniversalanalytics')->isAdwordsConversionValueInclShipping()) {
                $shippingAmount = $useStoreCurrency ? $order->getShippingAmount() : $order->getBaseShippingAmount();
                if(Mage::helper('googleuniversalanalytics')->isAdwordsConversionValueInclTax()) {
                    $shippingAmount += $useStoreCurrency ? $order->getShippingTaxAmount() : $order->getBaseShippingTaxAmount();
                }
                $grandTotal -= $shippingAmount;
            }

            return $grandTotal;
        }
        return 1;
    }
    
    public function getTransactionId() {
        $order = $this->getOrder();
        if($order && $order->getId()) {
            return $order->getIncrementId();
        }
        return '';
    }
    
    
}
