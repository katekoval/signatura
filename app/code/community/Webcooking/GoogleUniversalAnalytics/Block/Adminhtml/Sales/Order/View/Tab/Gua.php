<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Block_Adminhtml_Sales_Order_View_Tab_Gua
    extends Mage_Adminhtml_Block_Sales_Order_Abstract
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('webcooking/googleuniversalanalytics/sales/order/view/tab/gua.phtml');
    }
    
    
    public function getOrder()
    {
        return Mage::registry('current_order');
    }
    
    public function getTabTitle()
    {
        return $this->getTabLabel();
    }
    
    public function getTabLabel()
    {
        return Mage::helper('googleuniversalanalytics')->__('Google Analytics');
    }
    
     public function canShowTab()
    {
        return !$this->getOrder()->getFromShoppingflux();//compatibility with ShoppingFeed extension 
    }

    
    public function isHidden()
    {
        return false;
    }
    
    
    
    public function getAnalyticsData() {
        $dataFormatted = array();
        if($this->getOrder()->getGuaSentFlag()) {
            $dataFormatted[Mage::helper('googleuniversalanalytics')->__('Sent to GA ?')] = Mage::helper('googleuniversalanalytics')->__('Yes - <a href="%s" onclick="return confirm(\'Are you sure ? This could cause inconsistent data in Analytics.\')">Send order again to GA</a>', $this->getUrl('adminhtml/gua_order/sendorder', array('order_id'=>$this->getOrder()->getId())));
        } else {
            $dataFormatted[Mage::helper('googleuniversalanalytics')->__('Sent to GA ?')] = Mage::helper('googleuniversalanalytics')->__('No - <a href="%s">Send order to GA</a>', $this->getUrl('adminhtml/gua_order/sendorder', array('order_id'=>$this->getOrder()->getId())));
        }
        $dataFormatted[Mage::helper('googleuniversalanalytics')->__('GA client ID')] = $this->getOrder()->getGuaClientId();
        $dataFormatted[Mage::helper('googleuniversalanalytics')->__('User Agent')] = $this->getOrder()->getGuaUa();
        if(!$this->getApiHelper()->getProfile($this->getOrder()->getStoreId())) {
            return $dataFormatted;
        }
        $dimensions = array(
            'ga:source'=>'Source',
            'ga:referralPath' => 'Referral Path',
            'ga:campaign' => 'Campaign',
            'ga:medium' => 'Medium',
            'ga:sourceMedium' => 'Source Medium',
            'ga:keyword' => 'Keyword',
            'ga:sessionsToTransaction'=>'Sessions To Transactions',
            'ga:daysToTransaction'=>'Days To Transactions',
            'ga:operatingSystem' => 'OS',
            'ga:operatingSystemVersion' => 'OS Version',
            'ga:browser' => 'Browser',
            'ga:browserVersion' => 'Browser Version',
          //  'ga:productListName' => 'Product List Name'
        );
        
        try {
            $projectId = $this->getApiHelper()->getProfile($this->getOrder()->getStoreId())->getId();
            $data = false;
            $dimensionsChunks = array_chunk(array_keys($dimensions), 7);
            foreach($dimensionsChunks as $dimensionsForRequest) {
                $dimensionsForRequest = implode(',', $dimensionsForRequest);
                $apiResponse =  $this->getApiHelper()->getService()->data_ga->get(
                    'ga:' . $projectId, 
                    substr($this->getOrder()->getCreatedAt(), 0, 10), 
                    substr($this->getOrder()->getUpdatedAt(), 0, 10), 
                    'ga:hits', 
                    array(
                        'dimensions' => $dimensionsForRequest,
                        'filters'=>'ga:transactionId=='.Mage::helper('googleuniversalanalytics/ecommerce')->getOrderId($this->getOrder())
                    )
                );
                if(!$data) {
                     $data = $apiResponse;
                } else {
                    $data['rows'] = array_merge($data['rows'], $apiResponse['rows']);
                }
            }
            /*echo "<pre>";
            //var_dump($data['rows']);
            var_dump($data);
            echo "</pre>";*/
            if(count($data['rows'])>0) {
                $i=0;
                foreach($dimensions as $dimensionCode => $dimensionLabel) {
                    $dataFormatted[$dimensionLabel] = isset($data['rows'][0][$i++]) ? $data['rows'][0][$i++] : '';
                } 
            }
        } catch (Exception $e) {
            //var_dump($e->getMessage());
            Mage::helper('googleapi')->logException($e, 'gua');
        }
        return $dataFormatted;
    }
    
    public function getApiHelper() {
        return Mage::helper('googleuniversalanalytics/api');
    }
    
     public function getTabClass()
    {
        return 'ajax only';
    }
    
     /**
     * Get Class
     *
     * @return string
     */
    public function getClass()
    {
        return $this->getTabClass();
    }

    /**
     * Get Tab Url
     *
     * @return string
     */
    public function getTabUrl()
    {
        return $this->getUrl('adminhtml/gua_order/guatab', array('_current' => true));
    }


}
