<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
 
class Webcooking_GoogleUniversalAnalytics_Block_Gua extends Mage_Core_Block_Template
{
    
    protected function _construct()
    {
        parent::_construct();
        $this->setCacheLifetime(null);
    }

    
    

    public function getClientId() {
        return Mage::helper('googleuniversalanalytics')->getClientId(false);
    }
    
    public function getPageName()
    {
        return $this->_getData('page_name');
    }

  
    
    protected function _getAnalyticsOptionsAsJson()
    {
        return Mage::helper('googleuniversalanalytics')->getGaCreateOptions(null, true);
    }
    
    
    protected function _getOptOutTrackingCode($accountId)
    {
        $guaOptOutCode = '';
        if(Mage::helper('googleuniversalanalytics')->isOptOutActivated()) {
            $guaOptOutCode = "var disableStr = 'ga-disable-{$accountId}';" . "\n";

            $guaOptOutCode .= "if (document.cookie.indexOf(disableStr + '=true') > -1) {" . "\n";
            $guaOptOutCode .= "  window[disableStr] = true;" . "\n";
            $guaOptOutCode .= "}" . "\n";
            $guaOptOutCode .= "\n";

            $guaOptOutCode .= "function gaOptout() {" . "\n";
            $guaOptOutCode .= "  document.cookie = disableStr + '=true; expires=Thu, 31 Dec ".(date('Y')+10)." 23:59:59 UTC; path=/';" . "\n";
            $guaOptOutCode .= "  window[disableStr] = true;" . "\n";
            $guaOptOutCode .= "}" . "\n";
        }
        
        return $guaOptOutCode;
    }
    
    protected function _getBeforePageViewCustomTag() {
        return Mage::getStoreConfig('googleuniversalanalytics/custom_tags/before_pageview');
    }
    
    protected function _getAfterPageViewCustomTag() {
        return Mage::getStoreConfig('googleuniversalanalytics/custom_tags/after_pageview');
    }

    
    
    protected function _getOrdersTrackingCode()
    {
        return Mage::helper('googleuniversalanalytics/ecommerce')->getEnhancedOrdersTrackingCode($this->getOrderIds());
    }
    
    
    
    public function _getProductDetailsViewCode() {
        $product = Mage::registry('current_product');
        return Mage::helper('googleuniversalanalytics/ecommerce')->getAddProductDetailsTag($product);
    }
    
    
    public function _getTimeOnPageEventCode() {
        if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
            return Mage::helper('googleuniversalanalytics/gtm')->getTimeOnPageEventCode();
        }
        $guaCode = '';
        
        $timeOnPageDelays =  Mage::helper('googleuniversalanalytics')->getTimeOnPageDelays();
      
        foreach($timeOnPageDelays as $delay) {
            $delayMs = $delay * 1000;
            $guaCode .= "setTimeout(function(){  gtag('event', 'timeOnPage', { 'value': '{$delay}', 'non_interaction': true});   }, {$delayMs});    " . "\n";
        }
        
        return $guaCode;
    }
    
    public function _getCustomDimensionsCode() {
        if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
            return Mage::helper('googleuniversalanalytics/gtm')->getCustomDimensionsCode();
        }
        return '';
    }
    
    public function _getCustomMetricsCode() {
        if(Mage::helper('googleuniversalanalytics/gtm')->isActive()) {
            return Mage::helper('googleuniversalanalytics/gtm')->getCustomMetricsCode();
        }
        return '';
    }

    protected function _toHtml()
    {
        if (!Mage::helper('googleuniversalanalytics')->isGoogleAnalyticsAvailable()) {
            return '';
        }
        return parent::_toHtml();
    }
    
    
    
}
