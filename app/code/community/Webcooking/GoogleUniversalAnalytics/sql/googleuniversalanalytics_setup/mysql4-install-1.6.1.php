<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
$installer = $this;

$installer->startSetup();



$installer->run("
    
ALTER TABLE  `" . $this->getTable('sales/quote') . "` 
    ADD COLUMN  `gua_client_id` VARCHAR(255) NULL,
    ADD COLUMN `gua_ua` VARCHAR(512) NULL;
    
ALTER TABLE  `" . $this->getTable('sales/order') . "` 
 ADD COLUMN `gua_client_id` VARCHAR(255) NULL,
 ADD COLUMN `gua_sent_flag` VARCHAR(255) NULL,
 ADD COLUMN `gua_ua` VARCHAR(512) NULL;
 

UPDATE `" . $this->getTable('sales/order') . "`  set gua_sent_flag = 1;

");

$installer->endSetup(); 
