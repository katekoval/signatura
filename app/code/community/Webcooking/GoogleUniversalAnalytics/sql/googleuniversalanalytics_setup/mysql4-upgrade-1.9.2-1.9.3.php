<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */

$installer = new Mage_Catalog_Model_Resource_Eav_Mysql4_Setup('core_setup');
$installer->startSetup();



$entityTypeId = $installer->getEntityTypeId(Mage_Catalog_Model_Product::ENTITY);
$attrSetId = $installer->getDefaultAttributeSetId(Mage_Catalog_Model_Product::ENTITY);
$groupId = $installer->getDefaultAttributeGroupId(Mage_Catalog_Model_Product::ENTITY, $attrSetId);


$attribute = $installer->getAttribute($entityTypeId, 'ga_cart_to_detail_rate');

if (!$attribute)
    $installer->addAttribute('catalog_product', 'ga_cart_to_detail_rate', array(
        'type' => 'decimal',
        'backend' => '',
        'frontend' => '',
        'frontend-class' => 'validate-number',
        'label' => 'GA - Cart To Detail Rate',
        'input' => 'text',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible' => 1,
        'required' => 0,
        'user_defined' => 0,
        'searchable' => 0,
        'filterable' => 0,
        'comparable' => 0,
        'visible_on_front' => 0,
        'unique' => 0,
    ));

$attribute = $installer->getAttribute($entityTypeId, 'ga_buy_to_detail_rate');

if (!$attribute)
    $installer->addAttribute('catalog_product', 'ga_buy_to_detail_rate', array(
        'type' => 'decimal',
        'backend' => '',
        'frontend' => '',
        'frontend-class' => 'validate-number',
        'label' => 'GA - Buy To Detail Rate',
        'input' => 'text',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible' => 1,
        'required' => 0,
        'user_defined' => 0,
        'searchable' => 0,
        'filterable' => 0,
        'comparable' => 0,
        'visible_on_front' => 0,
        'unique' => 0,
    ));


$cartToDetailRateAttId = $installer->getAttributeId($entityTypeId, 'ga_cart_to_detail_rate');
$buyToDetailRateAttId = $installer->getAttributeId($entityTypeId, 'ga_buy_to_detail_rate');

$attrSetIds = $installer->getAllAttributeSetIds($entityTypeId);
foreach ($attrSetIds as $attrSetId) {
    $group = $installer->getAttributeGroup($entityTypeId, $attrSetId, 'Google Analytics');
    if (!$group) {
        $installer->addAttributeGroup(Mage_Catalog_Model_Product::ENTITY, $attrSetId, 'Google Analytics');
    }
    $groupId = $installer->getAttributeGroupId(Mage_Catalog_Model_Product::ENTITY, $attrSetId, 'Google Analytics');
    $installer->addAttributeToGroup($entityTypeId, $attrSetId, $groupId, $cartToDetailRateAttId);
    $installer->addAttributeToGroup($entityTypeId, $attrSetId, $groupId, $buyToDetailRateAttId);
}



$installer->endSetup(); 
 