<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
$installer = $this;

$installer->startSetup();


$onInvoice = Mage::getStoreConfig('googleuniversalanalytics/transactions/on_invoice');
if(!is_null($onInvoice)) {
    if($onInvoice) {
        Mage::getConfig()->saveConfig('googleuniversalanalytics/transactions/transaction_event', Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_CAPTURE_INVOICE);
    } else {
        Mage::getConfig()->saveConfig('googleuniversalanalytics/transactions/transaction_event', Webcooking_GoogleUniversalAnalytics_Helper_Data::TRANSACTION_EVENT_PLACE_ORDER);
    }
}
$installer->endSetup(); 
