<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
$installer = $this;

$installer->startSetup();
$installer->run("
    
ALTER TABLE  `" . $this->getTable('sales/quote') . "` ADD  `gua_client_id` VARCHAR(255) NULL;
ALTER TABLE  `" . $this->getTable('sales/quote') . "` ADD  `gua_ua` VARCHAR(512) NULL;
");

$installer->endSetup(); 
