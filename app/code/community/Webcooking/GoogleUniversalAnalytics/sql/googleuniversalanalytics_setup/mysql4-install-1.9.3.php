<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */
$installer = $this;

$installer->startSetup();



$installer->run("
    
ALTER TABLE  `" . $this->getTable('sales/quote') . "` 
    ADD COLUMN  `gua_client_id` VARCHAR(255) NULL,
    ADD COLUMN `gua_ua` VARCHAR(512) NULL;
    
ALTER TABLE  `" . $this->getTable('sales/order') . "` 
 ADD COLUMN `gua_client_id` VARCHAR(255) NULL,
 ADD COLUMN `gua_sent_flag` VARCHAR(255) NULL,
 ADD COLUMN `gua_ua` VARCHAR(512) NULL;
 

UPDATE `" . $this->getTable('sales/order') . "`  set gua_sent_flag = 1;

");


$table = $installer->getConnection()
    ->newTable($installer->getTable('googleuniversalanalytics/queue'))
    ->addColumn('queue_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'unsigned'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Queue Id')
    ->addColumn('hit_data', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        ), 'Hit Data')
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
        'nullable'  => true,
        'default'   => null,
        ), 'Created At')
    ->setComment('GUA Measurement Protocol Hit Queue');
$installer->getConnection()->createTable($table);




$installer->endSetup(); 

$installer = new Mage_Catalog_Model_Resource_Eav_Mysql4_Setup('core_setup');
$installer->startSetup();

$entityTypeId = $installer->getEntityTypeId(Mage_Catalog_Model_Product::ENTITY);
$attrSetId = $installer->getDefaultAttributeSetId(Mage_Catalog_Model_Product::ENTITY);
$groupId = $installer->getDefaultAttributeGroupId(Mage_Catalog_Model_Product::ENTITY, $attrSetId);


$attribute = $installer->getAttribute($entityTypeId, 'ga_cart_to_detail_rate');

if (!$attribute)
    $installer->addAttribute('catalog_product', 'ga_cart_to_detail_rate', array(
        'type' => 'decimal',
        'backend' => '',
        'frontend' => '',
        'frontend-class' => 'validate-number',
        'label' => 'GA - Cart To Detail Rate',
        'input' => 'text',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible' => 1,
        'required' => 0,
        'user_defined' => 0,
        'searchable' => 0,
        'filterable' => 0,
        'comparable' => 0,
        'visible_on_front' => 0,
        'unique' => 0,
    ));

$attribute = $installer->getAttribute($entityTypeId, 'ga_buy_to_detail_rate');

if (!$attribute)
    $installer->addAttribute('catalog_product', 'ga_buy_to_detail_rate', array(
        'type' => 'decimal',
        'backend' => '',
        'frontend' => '',
        'frontend-class' => 'validate-number',
        'label' => 'GA - Buy To Detail Rate',
        'input' => 'text',
        'global' => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
        'visible' => 1,
        'required' => 0,
        'user_defined' => 0,
        'searchable' => 0,
        'filterable' => 0,
        'comparable' => 0,
        'visible_on_front' => 0,
        'unique' => 0,
    ));



$cartToDetailRateAttId = $installer->getAttributeId($entityTypeId, 'ga_cart_to_detail_rate');
$buyToDetailRateAttId = $installer->getAttributeId($entityTypeId, 'ga_buy_to_detail_rate');

$attrSetIds = $installer->getAllAttributeSetIds($entityTypeId);
foreach ($attrSetIds as $attrSetId) {
    $group = $installer->getAttributeGroup($entityTypeId, $attrSetId, 'Google Analytics');
    if (!$group) {
        $installer->addAttributeGroup(Mage_Catalog_Model_Product::ENTITY, $attrSetId, 'Google Analytics');
    }
    $groupId = $installer->getAttributeGroupId(Mage_Catalog_Model_Product::ENTITY, $attrSetId, 'Google Analytics');
    $installer->addAttributeToGroup($entityTypeId, $attrSetId, $groupId, $cartToDetailRateAttId);
    $installer->addAttributeToGroup($entityTypeId, $attrSetId, $groupId, $buyToDetailRateAttId);
}

$installer->endSetup(); 
