<?php

/**
 * Copyright (c) 2011-2019 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */


require_once Mage::getModuleDir('controllers', 'Mage_Adminhtml').DS.'Sales'.DS.'OrderController.php';

class Webcooking_GoogleUniversalAnalytics_Adminhtml_Gua_OrderController extends Mage_Adminhtml_Sales_OrderController {

    protected function _construct() {
        $this->setUsedModuleName('Webcooking_GoogleUniversalAnalytics');
    }


    public function guatabAction() {
       $this->_initOrder();
        $html = $this->getLayout()->createBlock('googleuniversalanalytics/adminhtml_sales_order_view_tab_gua')->toHtml();
        /* @var $translate Mage_Core_Model_Translate_Inline */
        $translate = Mage::getModel('core/translate_inline');
        if ($translate->isAllowed()) {
            $translate->processResponseBody($html);
        }
        $this->getResponse()->setBody($html);
    }
    
    public function sendorderAction() {
        $measurementProtocol = Mage::getModel('googleuniversalanalytics/measurement_protocol');
        $orderId = $this->getRequest()->getParam('order_id');
        $order = Mage::getModel('sales/order')->load($orderId);
        if(!$order || !$order->getId()) {
            Mage::getSingleton('adminhtml/session')->addError(Mage::helper('googleuniversalanalytics')->__('Order not found'));
            $this->_redirect('adminhtml/sales_order');
            return;
        }
        $measurementProtocol->sendTransactionRequestFromOrder($order);
        Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('googleuniversalanalytics')->__('Order has been sent to GA'));
        $this->_redirect('adminhtml/sales_order/view', array('order_id'=>$orderId));
    }

    protected function _isAllowed() {
        return Mage::getSingleton('admin/session')->isAllowed('admin');
    }
}
