/**
 * Copyright (c) 2011-2017 SAS WEB COOKING - Vincent René Lucien Enjalbert. All rights reserved.
 * See http://www.web-cooking.net/licences/magento/LICENSE-EN.txt for license details.
 */

var guaProductImpressions = [];
var guaProductImpressionsSent = [];
var guaProductImpressionsTimeout = false;
var guaPromoImpressions = [];
var guaPromoImpressionsSent = [];
var guaPromoImpressionsTimeout = false;

function guaIsInViewport(elem) {
    var bounding = elem.getBoundingClientRect();
    //console.log(elem, bounding.top, bounding.left, bounding.bottom, (window.innerHeight || document.documentElement.clientHeight), bounding.right, (window.innerWidth || document.documentElement.clientWidth));
    return (
        bounding.top > 0 &&
        bounding.left > 0 &&
        bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
}

function sendGuaProductVariantDetails(productConfig) {
    if(!productDetail) {
        return;
    }
    var variant = [];
    var price = parseFloat(productConfig.config.basePrice);
    for(attributeId in productConfig.config.attributes) {
        stateValue = productConfig.state[attributeId];
        for(var i=0; i < productConfig.config.attributes[attributeId].options.length; i++) {
            if(productConfig.config.attributes[attributeId].options[i].id == stateValue) {
                variant.push(productConfig.config.attributes[attributeId].options[i].label);
                price += parseFloat(productConfig.config.attributes[attributeId].options[i].price);
            }
        }
    }
    productDetail.variant = variant.join(' - ');
    productDetail.price = price;
    
    gtag('event', 'view_item', {
       'items': [
           productDetail
       ]
    });
}

function getGuaProductData(element) {
    var productField = {};
    productField['id'] = element.readAttribute('data-gua-ec-id');
    productField['name'] = element.readAttribute('data-gua-ec-name');
    productField['category'] = element.readAttribute('data-gua-ec-category');
    productField['brand'] = element.readAttribute('data-gua-ec-brand');
    productField['variant'] = element.readAttribute('data-gua-ec-variant');
    productField['list_position'] = element.readAttribute('data-gua-ec-position');
    productField['price'] = element.readAttribute('data-gua-ec-price');
    productField['list_name'] = element.readAttribute('data-gua-ec-list');
    productField['noredirect'] = element.readAttribute('data-gua-ec-noredirect');
    return productField;
}

function getGuaPromoData(element) {
    var promoField = {};
    promoField['id'] = element.readAttribute('data-gua-ec-promo-id');
    promoField['name'] = element.readAttribute('data-gua-ec-promo-name');
    promoField['creative'] = element.readAttribute('data-gua-ec-promo-creative');
    promoField['position'] = element.readAttribute('data-gua-ec-promo-position');
    promoField['noredirect'] = element.readAttribute('data-gua-ec-promo-noredirect');
    return promoField;
}

function guaOnProductImpression(element) {
    var productField = getGuaProductData(element);
    if(!productField.id || guaProductImpressionsSent[productField.id]) {
        return;
    }
    guaProductImpressions.push(productField);
    guaProductImpressionsSent[productField.id] = true;
    if(guaProductImpressions.length > 10) {
        guaSendProductImpressions();
    } else {
        if(guaProductImpressionsTimeout) {
            clearTimeout(guaProductImpressionsTimeout);
        }
        guaProductImpressionsTimeout = setTimeout(guaSendProductImpressions, 500);
    }
}

function guaSendProductImpressions() {
    if(guaProductImpressionsTimeout) {
        clearTimeout(guaProductImpressionsTimeout);
        guaProductImpressionsTimeout = false;
    }
    if(guaProductImpressions.count <= 0) {
        return;
    }
    
    var productImpressions = guaProductImpressions;
    guaProductImpressions = [];
    if(wcIsGtm) {
        //console.log('view_item_list', productImpressions, 'gtm');
        dataLayer.push({
                'ecommerce': {
                  'currencyCode': currencyCode,                       
                  'impressions': productImpressions
                },
                'event':'GAevent',
                'eventCategory':'impression',
                'eventAction':'sent',
                'eventNoInteraction':true
        });
    } else {
        //console.log('view_item_list', productImpressions);
        gtag('event', 'view_item_list', {
            "non_interaction": true,
            "items": productImpressions
        });
    }
}


function guaOnPromoImpression(element) {
    var promoField = getGuaPromoData(element);
    if(!promoField.id || guaPromoImpressionsSent[promoField.id]) {
        return;
    }
    guaPromoImpressions.push(promoField);
    guaPromoImpressionsSent[promoField.id] = true;
    if(guaPromoImpressions.length > 10) {
        guaSendPromoImpressions();
    } else {
        if(guaPromoImpressionsTimeout) {
            clearTimeout(guaPromoImpressionsTimeout);
        }
        guaPromoImpressionsTimeout = setTimeout(guaSendPromoImpressions, 800);
    }
}

function guaSendPromoImpressions() {
    if(guaPromoImpressionsTimeout) {
        clearTimeout(guaPromoImpressionsTimeout);
        guaPromoImpressionsTimeout = false;
    }
    if(guaPromoImpressions.count <= 0) {
        return;
    }
    var promoImpressions = guaPromoImpressions;
    guaPromoImpressions = [];
    if(wcIsGtm) {
        //console.log('view_item_list', promoImpressions, 'gtm');
        dataLayer.push({
            'ecommerce': {
              'promoView' : {
                'promotions': promoImpressions
               }
            }
        });
        dataLayer.push({
            'event':'GAevent',
            'eventCategory':'impression',
            'eventAction':'sent',
            'eventNoInteraction':true
       });
    } else {
        //console.log('view_item_list', promoImpressions);
         gtag('event', 'view_promotion', {
            "non_interaction": true,
            "promotions": promoImpressions
          });
    }
}

function guaOnProductClick(element) {
    var productField = getGuaProductData(element);
    if(productField['id'] || productField['name']) {
        var href = element.readAttribute('href');
        if(!productField['list_name']) {
            productField['list_name'] = 'Product list';
        }
        
        if(wcIsGtm) {
            dataLayer.push({
                'event':'GAevent',
                'eventCategory':'click',
                'eventAction':'product',
                'eventLabel': productField['id'] + ' ' + productField['name'],
                'ecommerce': {
                  'click': {
                    'actionField': {'list_name': productField['list']},      // Optional list property.
                    'products': [productField]
                   }
                 },
                 'eventCallback': function() {
                    if(productField['noredirect']) {
                        return;
                    }
                    document.location = href;
                 }
            });
        } else {
            gtag('event', 'select_content', {
                "content_type": "product",
                "items": [
                  productField
                ],
                "event_callback": function() {
                        if(productField['noredirect']) {
                            return;
                        }
                        document.location = href;
                    }
            });
        }

        if((typeof(ga) !== 'undefined') && ga.loaded) {
            setTimeout(function(){ document.location = href; }, 300); //Security in case hitCallback was not called by GA lib.
            event.stop();
        }
    }
}

function guaOnPromoClick(element) {
    if(wcIsGtm) {
        return gtmOnPromoClick(element);
    }
    var promoField = getGuaPromoData(element);
    if(promoField['id'] || promoField['name']) {
        var href = element.readAttribute('href');
        if(wcIsGtm) {
            dataLayer.push({
                'event':'GAevent',
                'eventCategory':'click',
                'eventAction':'promo',
                'eventLabel': promoField['id'] + ' ' + promoField['name'],
                'ecommerce': {
                  'promoClick': {
                    'promotions': [promoField]
                  }
                },
                'eventCallback': function() {
                    if(promoField['noredirect']) {
                        return;
                    }
                  document.location = href;
                }
            });
        } else {
            gtag('event', 'select_content', {
                "promotions": [
                  promoField
                ],
                "event_callback": function() {
                        if(promoField['noredirect']) {
                            return;
                        }
                        document.location = href;
                    }

            });
        }
        
        if((typeof(ga) !== 'undefined') && ga.loaded) {
            setTimeout(function(){ document.location = href; }, 800); //Security in case hitCallback was not called by GA lib.
            event.stop();
        }
    }
}
function sendGtmProductVariantDetails(productConfig) {
    if(!productDetail) {
        return;
    }
    var variant = [];
    var price = parseFloat(productConfig.config.basePrice);
    for(attributeId in productConfig.config.attributes) {
        stateValue = productConfig.state[attributeId];
        for(var i=0; i < productConfig.config.attributes[attributeId].options.length; i++) {
            if(productConfig.config.attributes[attributeId].options[i].id == stateValue) {
                variant.push(productConfig.config.attributes[attributeId].options[i].label);
                price += parseFloat(productConfig.config.attributes[attributeId].options[i].price);
            }
        }
    }
    productDetail.variant = variant.join(' - ');
    productDetail.price = price;
    dataLayer.push({
        'ecommerce': {
          'detail': {
            'products': [productDetail]
           }
         }
    });
}



function manageImpressions() {
    $$('a[data-gua-ec-id]').each(function(item){
        if (!item.readAttribute('data-impression-sent') && guaIsInViewport(item)) {
            item.writeAttribute('data-impression-sent', 1);
            guaOnProductImpression(item);
        }
    });
    $$('a[data-gua-ec-promo-id]').each(function(item){
        if (!item.readAttribute('data-impression-sent') && guaIsInViewport(item)) {
            item.writeAttribute('data-impression-sent', 1);
            guaOnPromoImpression(item);
        }
    });
}

Event.observe(window, 'load', function() {
    $$('a[data-gua-ec-id]').invoke('observe', 'click', function() { guaOnProductClick($(this)); });
    $$('a[data-gua-ec-promo-id]').invoke('observe', 'click', function() { guaOnPromoClick($(this)); });
    window.addEventListener('scroll', manageImpressions, false);
    manageImpressions();
});

