BrandBundle = {
    prices : {},
    optionIds : {},
    selectionValues : {},
    presetOptions : {},
    selectedProducts : [],
    cheapestOfAll : false,
    init : function() {
        jQuery(document).ready(function() {
            BrandBundle.handleSelectors();
            BrandBundle.handlePreselectedOptions();
            BrandBundle.handleChildRef();
        });
        BrandBundle.setEvents();
    },
    setEvents : function() {
        jQuery(document).on('change','#brandbundle-size',function(e) {
            BrandBundle.handleSelectors();
        });
        jQuery(document).on('click','.brandbundle-products-grid .item button[data-action]',function(e) {
            BrandBundle[jQuery(this).data('action')](jQuery(this).closest('[data-value][data-label]'));
            jQuery(this).data('action');
        });
        jQuery(document).on('keyup','.brandbundle-options-selector--search input[name="q"]',function(e) {
            BrandBundle.searchList();
        });
        jQuery(document).on('click','body',function(e) {
            if(jQuery('.brandbundle-options-selector:visible').length) {
                if(!jQuery(e.target).closest('#brandbundle-options-container').length) {
                    jQuery('.brandbundle-options-selector').removeClass('brandbundle-options-selector--active');
                }
            }
        });
        jQuery(document).on('keyup','body',function(e) {
            if(e.keyCode == 27) {
                if(jQuery('.brandbundle-options-selector:visible').length) {
                    jQuery('.brandbundle-options-selector').removeClass('brandbundle-options-selector--active');
                }
            }
        });
    },
    handleSelectors : function() {
        var showQty = jQuery('#brandbundle-size').val();
        var ul = jQuery('#brandbundle-options-container .brandbundle-options-list').empty();
        jQuery('.brandbundle-products-grid .item [data-action="qtySelectedItem"]').text(0); 
        var i = 0;
        while(i < showQty) {
            var value = '';
            var label = Translator.translate('Please select an item');
            var option_id = BrandBundle.optionIds[i];
            if(typeof BrandBundle.selectedProducts[i] != 'undefined') {
                //value = BrandBundle.selectedProducts[i].value;
                
                value = BrandBundle.selectionValues[BrandBundle.selectedProducts[i].value][option_id];
                
                label = BrandBundle.selectedProducts[i].label;
                var qty = parseInt(jQuery('.brandbundle-products-grid .item[data-value="'+BrandBundle.selectedProducts[i].value+'"] [data-action="qtySelectedItem"]').text());
                jQuery('.brandbundle-products-grid .item[data-value="'+BrandBundle.selectedProducts[i].value+'"] [data-action="qtySelectedItem"]').text(qty+1);
            }
            var li = jQuery('<li>', { 'class' : 'brandbundle-options-list--item' }).append(
                jQuery('<button>', { 'type' : 'button', 'class' : 'brandbundle-options-list--item-button', 'onclick' : 'BrandBundle.toggleItemSelector(this);' }).text(label)
            ).append(
                jQuery('<input>',{type:'hidden',name:'bundle_option['+option_id+']',value:value}).addClass('required-entry')
            ).appendTo(ul);

            i++;   
        }
        BrandBundle.handlePrices();
    },
    toggleItemSelector : function(element, forceRebuild) {
        jQuery('.brandbundle-options-selector--search input[name="q"]').val('').trigger('keyup');
        var isStateActive = jQuery('.brandbundle-options-selector.brandbundle-options-selector--active').length;
        jQuery('.brandbundle-options-list--item-button').removeClass('brandbundle-options-list--item-button--active');
        
        if(!jQuery('.brandbundle-options-selector').length) {
            var itemSelector = BrandBundle.buildItemSelector();
        } else {
            itemSelector = jQuery('.brandbundle-options-selector');
            itemSelector.removeClass('brandbundle-options-selector--active');
        }
        
        if(!isStateActive) {

            if(!jQuery(element).parent().find('input[name^=bundle_option]').val()) {
                if(jQuery(element).closest('.brandbundle-options-list').find('input[name^=bundle_option][value=""]').length) {
                    element = jQuery(element).closest('.brandbundle-options-list').find('input[name^=bundle_option][value=""]').first().parent().find('.brandbundle-options-list--item-button').first();
                }
            }
            
            jQuery(element).addClass('brandbundle-options-list--item-button--active');
            itemSelector.addClass('brandbundle-options-selector--active');
            jQuery(itemSelector).insertAfter(element);
        }

    },
    buildItemSelector : function() {
console.log('buildItemSelector');        
        var container = jQuery('<div>', { 'class' : 'brandbundle-options-selector' } );
        var searchContainer = jQuery('<div>', { 'class' : 'brandbundle-options-selector--search' } );
        var searchInput = jQuery('<input>', { 'type' : 'text', 'name' : 'q', 'autocomplete' : 'off', 'placeholder' : 'Søg efter produkter' } );
        var listContainer = jQuery('<ul>', { 'class' : 'brandbundle-options-selector--list' } );
        var listItems = [];
        var showQty = jQuery('#brandbundle-size').val();
        
        jQuery('.brandbundle-products-grid .brandbundle-item').each(function() {
            var itemLabel = jQuery(this).data('label');
            var itemValue = jQuery(this).data('value');
            
            if(typeof BrandBundle.prices[itemValue]['prices'][showQty] != 'undefined') {
                var itemPrice = BrandBundle.prices[itemValue]['prices'][showQty];
            } else {
                var itemPrice = BrandBundle.prices[itemValue]['final_price'];
            }            
        
            itemPrice = '+'+(itemPrice-BrandBundle.cheapestOfAll).toFixed(2).replace('.',',')+' DKK';

            var listItem = jQuery('<li>', { 'class' : 'brandbundle-options-selector--item', 'data-label' : itemLabel, 'data-value' : itemValue } );
            var listItemButton = jQuery('<button>', { 'type' : 'button', 'class' : 'brandbundle-options-selector--item-button', 'data-label' : itemLabel, 'data-value' : itemValue, 'onclick' : 'BrandBundle.addSelectedItem(this)' } );
            
            if(BrandBundle.prices[itemValue]['out_of_stock_text']) {
                itemPrice = BrandBundle.prices[itemValue]['out_of_stock_text'];
                listItemButton.attr('disabled',true);
            }
            var listItemName = jQuery('<div>', { 'class' : 'brandbundle-options-selector--item-name', 'html' : itemLabel } );
            var listItemPrice = jQuery('<div>', { 'class' : 'brandbundle-options-selector--item-price', 'html' : itemPrice } );
            
            listItems.push(
                listItem.append(
                    listItemButton.append(listItemName, listItemPrice)
                )
            );
        });
        
        container.append(
            searchContainer.append(searchInput),
            listContainer.append(listItems)
        );
        
        return container;
    },
    addSelectedItem : function(element) {
        
        if(jQuery(element).hasClass('brandbundle-options-selector--item-button')) {
            if(jQuery(element).closest('.brandbundle-options-list--item').find('[name^="bundle_option["]').length) {
                var inputOption = jQuery(element).closest('.brandbundle-options-list--item').find('[name^="bundle_option["]').first();
                if(jQuery(inputOption).val()) {
                    var selectedProductsIndexId = 0;
                    jQuery('[name^="bundle_option[').each(function() {
                        if(inputOption.attr('name') == jQuery(this).attr('name')) {
                            return false;
                        }
                        selectedProductsIndexId++;
                    });
                    BrandBundle.selectedProducts[selectedProductsIndexId] = jQuery(element).data();
                } else {
                    BrandBundle.selectedProducts.push(jQuery(element).data());         
                }
            }
        }
        else if(BrandBundle.selectedProducts.length < jQuery('#brandbundle-size').val()) {
            BrandBundle.selectedProducts.push(jQuery(element).data()); 
        }
        BrandBundle.handleSelectors();
    },
    removeSelectedItem : function(element) {
        jQuery(BrandBundle.selectedProducts).each(function(k,v) {
            if(v.value == jQuery(element).data('value')) {
                BrandBundle.selectedProducts.splice(k, 1); 
                return false;
            }
        });
        BrandBundle.handleSelectors(); 
    },
    setData : function(prices,optionIds,selectionValues,presetOptions) {
        BrandBundle.prices = prices;
        BrandBundle.optionIds = optionIds;
        BrandBundle.selectionValues = selectionValues;
        BrandBundle.presetOptions = presetOptions;
        BrandBundle.handlePrices();
    },
    handlePrices : function() {
        var cheapestOfAll = false;
        var cheapestRetailOfAll = false;
        var showQty = jQuery('#brandbundle-size').val();
        var retailTotals = [];
        var totals = [];
        var i = 0;

        while(i < showQty) {
            if(typeof BrandBundle.selectedProducts[i] != 'undefined') {
                var item = BrandBundle.prices[BrandBundle.selectedProducts[i].value];
                retailTotals.push(item['price']);
                if(typeof item['prices'][showQty] != 'undefined') {
                    totals.push(item['prices'][showQty]);
                } else {
                    totals.push(item['final_price']);
                }
            }
            i++;
        }

        Object.keys(BrandBundle.prices).each(function(productId) {
            
            if(typeof BrandBundle.prices[productId]['prices'][showQty] != 'undefined') {
                var price = BrandBundle.prices[productId]['prices'][showQty];
            } else {
                var price = BrandBundle.prices[productId]['final_price'];
            }

            if(cheapestOfAll === false || cheapestOfAll > price) {
                cheapestOfAll = price;
            }
            if(cheapestRetailOfAll === false || cheapestRetailOfAll > BrandBundle.prices[productId]['price']) {
                cheapestRetailOfAll = BrandBundle.prices[productId]['price'];
            }
            
        });
        
        var total = totals.reduce(function(acc, val) { return acc + val; }, (showQty-totals.length)*cheapestOfAll);
        var retailTotal = retailTotals.reduce(function(acc, val) { return acc + val; }, (showQty-totals.length)*cheapestRetailOfAll);
        var totalDiscount = ((retailTotal-total)/retailTotal)*100;

                           
        jQuery('#brandbundle-price-container .brandbundle-price-savings--retail-price .price').text(retailTotal.toFixed(2).replace('.',','));
        jQuery('#brandbundle-price-container .brandbundle-price-final .price').text(total.toFixed(2).replace('.',','));
        jQuery('#brandbundle-price-container .brandbundle-price-from .price').text(cheapestOfAll.toFixed(2).replace('.',','));
        jQuery('#brandbundle-price-container .brandbundle-price-savings--percentage .price').text(totalDiscount.toFixed(0).replace('.',',')+'%');
        
        BrandBundle.cheapestOfAll = cheapestOfAll;
    },
    searchList : function() {
        
        var q = jQuery('.brandbundle-options-selector--search input[name="q"]').val();
        jQuery('.brandbundle-options-selector--item').removeClass('hidden');
        
        if(q) {
            jQuery('.brandbundle-options-selector--item').each(function() {
                if(jQuery(this).data('label').toLowerCase().indexOf(q.toLowerCase()) == -1) {
                    jQuery(this).addClass('hidden');
                }
            });
        }
    },
    handlePreselectedOptions : function() {
        
        if(BrandBundle.presetOptions.length) {
            jQuery('#brandbundle-size').val(BrandBundle.presetOptions.length).trigger('change');
            BrandBundle.presetOptions.each(function(v){ 
                
                if(jQuery('.brandbundle-item[data-value="'+v.product_id+'"]').length) {
                    BrandBundle.addSelectedItem(jQuery('.brandbundle-item[data-value="'+v.product_id+'"]').first());
                }
            
            });
            BrandBundle.presetOptions = [];
        }
    },
    handleChildRef : function() {
        var childRef = parseInt(localStorage.getItem('brandbundle-child-ref-'+jQuery('#product_addtocart_form input[name="product"]').val()));
        if(childRef) {
            localStorage.setItem('brandbundle-child-ref-'+jQuery('#product_addtocart_form input[name="product"]').val(),null);
            jQuery('.brandbundle-products-grid .brandbundle-item[data-value="'+childRef+'"] button[data-action="addSelectedItem"]').click();
        }
        
    }
}
BrandBundle.init();

