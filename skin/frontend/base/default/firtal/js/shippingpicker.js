(function(w){
    function url(path){
        return (window.SP_BASE_URL || window.FE_BASE_URL || location.origin).replace(/\/+$/g,'') + path.replace(/^\/*/g, '/'); /**/
    }
    w.ShippingPicker = function(settings) {
        settings = settings || {};

        var __ = settings.prefix || 'SP_';
        var SP = this;

        SP.objects = [];
        SP.choice = null;
        SP.shippingAddress = null;
        SP.bounds = null;
        SP.initialized = false;
        SP.preselectedDroppoint = null;
        SP.recommendedDroppoint = null;

        SP.settings = settings;

        SP.filters = {
            providers: [],
            dist: null,
            closest: null
        };

        SP.providers = {};


        /**
         * Function SP.init
         *
         * Intializes all functionality
         */
        SP.init = function(){
            SP.createLoadingView();


            SP.getShippingAddress(function(json){
                if (json.ERROR){
                    if (json.CODE==4) {
                        SP.createOverlayError("Udfyld venligst forsendelsesadressen");
                    } else {
                        SP.createOverlayError("Der opstod et problem med forsendelsesadressen. Kontakt venligst kundeservice hvis dette problem er vedvarende.");
                    }
                    return;
                }
                SP.shippingAddress = json;

                SP.getAllDroppointsFromZip(SP.shippingAddress.zip, function(providers){
                    if (providers.ERROR){
                        SP.createOverlayError("Der opstod et problem ved modtagelses af pakkeshops. Kontakt venligst kundeservice hvis dette problem er vedvarende.");
                        return;
                    }

                    clearTimeout(SP.timeout);
                    SP.providers = [];
                    for (var i=0; i<providers.length; i++){
                        if (providers[i].status==0){
                            SP.providers.push(providers[i]);
                        }
                    }


                    if (SP.shippingAddress.coordinates) {
                        SP.calculateAllDroppointDistance();
                    }

                    SP.getSelectedDroppointInfo(function(droppoint) {
                        SP.preselectedDroppoint = droppoint;
                        SP.recommendedDroppoint = SP.findRecommendDroppoint();

                        SP.createMap();
                    });



                });
            });

        };


        /**
         * Function SP.createMap
         *
         * Clean old elements and re-populates them.
         * Creates the actual Google Map.
         */
        SP.createMap = function(){
            var overlay = jQuery('#SP_overlay_content');
            overlay.empty();

            jQuery('#SP_droppointList').empty();
            jQuery('#SP_map').empty();
            jQuery('#SP_infobox').empty();

            var div_list = new Element('div', {
                'id': 'SP_droppointDiv',
                'class': 'col-12 col-lg-3'
            });
            var ul_list = new Element('ul',{
                'id': 'SP_droppointList',
                'class': 'SP_infoList'
            });
            div_list.appendChild(ul_list);

            var div_map = new Element('div',{
                'id': 'SP_map',
                'class': 'col-6'
            });


            var div_info = new Element('div',{
                'id': 'SP_infobox',
                'class': 'col-12 col-lg-3'
            });

            overlay.append(div_list);
            overlay.append(div_map);
            overlay.append(div_info);



            SP.map = new google.maps.Map(document.getElementById(__ + 'map'), {
                center: {lat: 56.1585836 - 0.6, lng: 10.208542999999999},
                zoom: 15,
                zoomControl: true,
                mapTypeControl: false,
                scaleControl: false,
                streetViewControl: false,
                rotateControl: false
            });


            SP.createMarkers();
            SP.createYAH();
            SP.createFilters();
            SP.postInit();
        };


        /**
         * Function SP.createLoadingView
         *
         * Loading screen.
         */
        SP.createLoadingView = function(){
            SP.timedOut = false;

            var overlay = jQuery('#SP_overlay_content');
            overlay.empty();

            var loadingImg = new Element('img',{
                'id': 'SP_loadingImg',
                'src': 'https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif'
            });
            overlay.append(loadingImg);

            SP.timeout = setTimeout(function(){

                SP.createOverlayError('Timed out. Please check connection.');

            }, 20000);
        };

        SP.createOverlayError = function(err){
            clearTimeout(SP.timeout);
            var error = new Element('h1');
            error.innerHTML = err;

            jQuery('#SP_overlay_content').empty().append(error);
        };


        SP.createMarkers = function(){
            var droppoints = SP.getAllDroppintsInArray(); // Unsorted, uncatagorized by provider.
            SP.bounds = new google.maps.LatLngBounds();


            var shouldSort = (SP.shippingAddress.coordinates != null); // If we can't get billing coords from Google, its w/e.

            // Measure distance and if set, list only the closest.
            var amount = droppoints.length;
            if (shouldSort){
                droppoints = SP.sortDroppointsFromArray(droppoints);
                if (SP.filters.closest != null && typeof(SP.filters.closest)=="number"){
                    if (SP.filters.closest==0) SP.filters.closest=1;
                    amount = SP.filters.closest;
                }
            }

            for (var i=0;i<amount;i++) {
                var dummydrop = droppoints[i];
                var provider = SP.providers[dummydrop.provider_key];
                var dropPoint = provider.dropPoints[dummydrop.droppoint_key];

                if (!dropPoint) continue;

                // Check if provider is filtered.
                if (SP.filters.providers.indexOf(provider.provider_title)!=-1) continue;

                var marker = SP.addDroppointMarker(dropPoint);
                var div = SP.addDroppointToList(dropPoint);

                dropPoint.marker = marker;
                dropPoint.div = div;

            }


            SP.postDroppointsAdd();

        };


        /**
         * Function SP.postDroppointsAdd
         *
         * Runs after all markers on the Google Map have been created.
         * Selects a droppoint to start on.
         */
        SP.postDroppointsAdd = function(){
            var droppointAmount = jQuery('.SP_droppointItem').length;
            var droppointTotal = 0;
            for (var i=0; i<SP.providers.length; i++){ droppointTotal += SP.providers[i].dropPoints.length; }
            jQuery('#SP_sliderLabel').text("Viser " + droppointAmount + ' ud af ' + droppointTotal + ' pakkeshops');


            SP.map.fitBounds(SP.bounds);


            // Choice of preselected droppoint
            SP.choice = null;
            var success = SP.selectDroppoint(SP.preselectedDroppoint); // Droppoint saved before
            if (!success) {
                success = SP.selectDroppoint(SP.recommendedDroppoint); // Recommended
            }
            if (!success){
                jQuery("#SP_droppointList li:first-child a:last-child").click(); // Top in list
            }

            SP.createYAH();
            SP.initialized = true;
        };


        SP.postInit = function(){
        };


        SP.createFilters = function(){
            var div = jQuery('#SP_filters');
            div.empty();

            var vis = new Element('p',{
                'id': 'SP_filterTitle'
            });
            vis.innerHTML = 'Vis:';
            jQuery(div).append(vis);

            // Filter providers
            for (var i=0; i<SP.providers.length; i++){
                var checkbox = new Element('input', {
                    'type': 'checkbox',
                    'name': 'providers',
                    'class': 'SP_providerCheck',
                    'value': SP.providers[i].provider_title,
                    'checked': true
                });

                var label = new Element('label',{
                    'class': 'SP_providerCheckLabel'
                });
                label.innerHTML = SP.providers[i].provider_title;

                jQuery(label).append(checkbox);
                jQuery(div).append(label);
            }

            jQuery('.SP_providerCheck').click(function(){
                SP.filterProvider(jQuery(this).val(), jQuery(this).is(':checked'));
            });



            if (SP.shippingAddress.coordinates) {
                // Filter by distance
                var droppointAmount = jQuery('.SP_droppointItem').length;
                var value = droppointAmount;
                if (droppointAmount>20){
                    value = Math.floor(droppointAmount / 2);
                    SP.filterClosest(Number(value));
                }
                var amSlider = new Element('input', {
                    'type': 'range',
                    'id': 'SP_amountSlider',
                    'value': value,
                    'min': '0',
                    'max': droppointAmount
                });
                var label = new Element('p', {
                    'id': 'SP_sliderLabel'
                });
                var droppointTotal = 0;
                for (var i=0; i<SP.providers.length; i++){ droppointTotal += SP.providers[i].dropPoints.length; }
                label.innerHTML = "Viser " + value + ' ud af ' + droppointTotal + ' pakkeshops';


                jQuery(div).append(amSlider);
                jQuery(div).append(label);

                jQuery('#SP_amountSlider').change(function () {
                    if (!SP.initialized) return;
                    SP.filterClosest(Number(jQuery(this).val()));
                });
            }

        };


        /**
         * Function SP.createYAH
         *
         * Adds a 'You are here'-marker to the map.
         */
        SP.createYAH = function() {
            if (!SP.shippingAddress.coordinates) return;

            var marker = new google.maps.Marker({
                position: SP.shippingAddress.coordinates,
                map: SP.map
            });
        };

        /**
         * SP.getAllDroppointsInArray
         *
         * Droppoints are sorted in their respective providers -
         * this function is important for iterating & sorting all droppoints by other factors.
         * @returns {Array}
         */
        SP.getAllDroppintsInArray = function(){
            var points = [];
            for (var i=0; i < SP.providers.length; i++) {
                var provider = SP.providers[i];
                for (var h = 0; h < provider.dropPoints.length; h++) {
                    var d = points.push(provider.dropPoints[h]);
                    points[d-1].provider_key = i; // future reference to our provider array.
                    points[d-1].droppoint_key = h;
                }
            }
            return points;
        };


        SP.formatDistInMeter = function(dist){
            if (dist>1000){
                dist = parseFloat(dist / 1000).toFixed(2).toString().replace('.', ',');
                dist = String(dist) + ' kilometer';
            } else {
                dist = String(dist) + ' meter';
            }

            return dist;
        };

        /**
         * Ranking is calculated based on distance and weight priority
         * @param droppoint
         * @param compareDroppoint
         * @returns {boolean}
         */
        SP.droppointSortAlgorithm = function (droppoint, compareDroppoint) {
            var penaltyDistance = SP.calculateDroppointPenaltyDistance(droppoint);
            var comparePenaltyDistance = SP.calculateDroppointPenaltyDistance(compareDroppoint);

            if (comparePenaltyDistance<penaltyDistance) return true;
        };

        SP.calculateDroppointPenaltyDistance = function(droppoint){
            var provider = SP.getProviderFromDroppoint(droppoint);
            var meter_penalty = provider.variables.meter_penalty ? provider.variables.meter_penalty : 0;
            var percentage_penalty = provider.variables.percentage_penalty ? provider.variables.percentage_penalty : 0;
            var farthestDroppointDistance = SP.getFarthestDroppoint().distance;
            if (!farthestDroppointDistance) farthestDroppointDistance = 0;
            var penaltyDistance = Number(meter_penalty) + Number(percentage_penalty) / 100 * Number(farthestDroppointDistance);

            return droppoint.distance + penaltyDistance;
        };


        SP.getFarthestDroppoint = function(){
            return (typeof SP.farthestDroppoint !== 'undefined')? SP.farthestDroppoint : false;
        };


        SP.calculateFarthestDroppoint = function(){
            var droppoints = SP.getAllDroppintsInArray();
            var droppoints = droppoints.sort(function(a, b){
                return Number(b.distance)-Number(a.distance);
            });

            if (!droppoints[0]) return false;
            SP.farthestDroppoint = droppoints[0];
            return true;
        };

        SP.sortDroppointsFromArray = function(droppoints){
            SP.calculateFarthestDroppoint();

            for(var i=0;i<droppoints.length; i++){
                var min = i;


                for(var h=i;h<droppoints.length;h++){
                    if (SP.droppointSortAlgorithm(droppoints[min], droppoints[h])){
                        min = h;
                    }
                }

                if (i !== min){
                    var t = droppoints[i]; // Swap
                    droppoints[i] = droppoints[min];
                    droppoints[min] = t;
                }
            }
            return droppoints;
        };


        SP.getAllDroppointsFromZip = function(zip, callback) {
            SP.ajax_droppoints = new Ajax.Request(url('shippingpicker/index/getAllDroppoints'), {
                method: 'GET',
                parameters: {postcode: zip},
                onComplete: function (transport) {
                    var json = transport.responseText.evalJSON();
                    if (json) {
                        callback(json);
                        return;
                    }
                    callback(false);
                }
            });
        };

        SP.getSelectedDroppointInfo = function(callback){
            new Ajax.Request(url("shippingpicker/index/getSelectedDroppointInfo"), {
                method: 'GET',
                onComplete: function (transport) {
                    var json = transport.responseText.evalJSON();
                    if (!json || json.ERROR) return callback(false);

                    var drop;
                    for (var i=0; i < SP.providers.length; i++){
                        var provider = SP.providers[i];
                        if (provider.rate.code != json.method) continue;

                        for (var h=0; h < provider.dropPoints.length; h++){
                            var droppoint = provider.dropPoints[h];
                            if (droppoint.name != json.name) continue;

                            drop = droppoint;
                            break;
                        }
                    }
                    callback(drop);

                }
            });
        };


        SP.getShippingAddress = function(callback){
            SP.ajax_shipping = new Ajax.Request(url("shippingpicker/index/getShippingAddress"), {
                method: 'GET',
                onComplete: function(transport){
                    var json = transport.responseText.evalJSON();

                    callback(json)
                }
            });
        };

        SP.addDroppointToList = function(dropPoint){
            var provider = SP.getProviderFromDroppoint(dropPoint);


            var li = new Element('li', {
                //'class': SP.prefix + 'droppoint'
                'class': __ + "infoItem " + __ + 'droppointItem'
            });
            li.innerHTML = '<div class="droppointprovider">' + provider.provider_title + '</div>' + SP.formatPrice(provider.rate.price)
                + '<div class="clear"></div>' + dropPoint.name
                + '</br>' + dropPoint.address
                + '</br>' + dropPoint.zip + ' ' + dropPoint.city;


            if (SP.recommendedDroppoint && SP.recommendedDroppoint==dropPoint){
                li.innerHTML += "<div id='SP_recommendedDroppoint'>Anbefalet</div>";
            }
           


            var today = new Date();

            switch(provider.provider_title){
                case 'DAO':
                    var cutOffTime = 'kl. 16';   
                    if (today.getDay() >= 1 && today.getDay() <= 4) cutOffTime = 'kl. 16';
                    li.innerHTML += "<div id='SP_cutoff'>Bestil inden " + cutOffTime + "</div>";
                    break;
                case 'Bring':
                    var cutOffTime = 'kl. 16';   
                    if (today.getDay() >= 1 && today.getDay() <= 4) cutOffTime = 'kl. 21';
                    li.innerHTML += "<div id='SP_cutoff'>Bestil inden " + cutOffTime + "</div>";
                    break;
                case 'PostDanmark':
                    var cutOffTime = 'kl. 16';   
                    if (today.getDay() >= 1 && today.getDay() <= 4) cutOffTime = 'kl. 21';
                    li.innerHTML += "<div id='SP_cutoff'>Bestil inden " + cutOffTime + "</div>";
                    break;
                case 'GLS':
                    var cutOffTime = 'kl. 16';   
                    if (today.getDay() >= 1 && today.getDay() <= 4) cutOffTime = 'kl. 16';
                    li.innerHTML += "<div id='SP_cutoff'>Bestil inden " + cutOffTime + "</div>";
                    break;
                default:
                    break;
            }



            var anchor = new Element('a',{
                'class': __ + 'droppointAnchor'
            });

            li.appendChild(anchor);

            var dropList = document.getElementById(__ + 'droppointList');
            dropList.appendChild(li);

            jQuery(anchor).click(function(){
                SP.selectDroppoint(dropPoint);
            });

            return li;
        };



        SP.addDroppointMarker = function(dropPoint) {
            var icon_url = SP.providers[dropPoint.provider_key].icon_url;
            if (!icon_url) icon_url = "https://avatars0.githubusercontent.com/u/238156?v=3&s=400";

            var icon = {
                url: icon_url,
                scaledSize: new google.maps.Size(32, 40)
            };

            var pos = new google.maps.LatLng(dropPoint['position']['lat'], dropPoint['position']['lng']);

            var marker = new google.maps.Marker({
                position: pos,
                map: map,
                icon: icon
            });

            marker.addListener('click', function(){
                SP.selectDroppoint(dropPoint);
            });


            // Extend map bounds
            SP.bounds.extend(pos);
            return marker;
        };


        SP.selectDroppoint = function(droppoint){
            if (!droppoint || droppoint == SP.choice || !droppoint.marker.map) return false;

            // Outline pick in slider.
            jQuery('.' + __ + 'droppointItem').css({'background-color': 'white'});
            jQuery(droppoint.div).css({'background-color': '#d8f3d8'});


            // Bouncing
            if (SP.choice){
                SP.choice.marker.setAnimation(null);
            }
            droppoint.marker.setAnimation(google.maps.Animation.BOUNCE);


            // Open up for radio selection
            jQuery('#SP_shippingRadio').attr('disabled', false);


            // Show save button
            jQuery('#SP_save').show();


            // Center on marker
            SP.map.setCenter(droppoint.marker.getPosition());

            SP.updateInfoBox(droppoint);
            SP.choice = droppoint;
            return true;
        };



        SP.updateInfoBox = function(droppoint){

            var provider = SP.getProviderFromDroppoint(droppoint);
            // Show droppoint info. jeg sutter til UI ayy
            var infobox = jQuery('#SP_infobox');
            infobox.empty();

            var ul = new Element('ul', {
                'class': 'SP_infoList'
            });

            var name = new Element('li',{
                'class': 'SP_infoItem'
            });
            name.innerHTML = '<strong>' + droppoint.name + '</strong>'
                + '</br>' + droppoint.address
                + '</br>' + droppoint.zip + ' ' + droppoint.city;

            var providerLabel = new Element('li',{
                'class': 'SP_infoItem'
            });
            providerLabel.innerHTML = '<strong>Leverandør:</strong> ' + provider.provider_title;

            var dist = new Element('li',{
                'class': 'SP_infoItem'
            });
            dist.innerHTML = '<strong>Afstand: </strong>' + SP.formatDistInMeter(droppoint.distance);

            var hours = new Element('li',{
                'class': 'SP_infoItem'
            });
            hours.innerHTML = "<strong>Åbningstider:</strong> " + "</br>";
            for(var i=0;i<droppoint.hours.length;i++){
                hours.innerHTML += droppoint.hours[i] + '</br>';
            }


            var price = new Element('li',{
                'class': 'SP_infoItem'
            });
            price.innerHTML = '<strong>Pris:</strong> ' + SP.formatPrice(provider.rate.price);

            ul.appendChild(name);
            ul.appendChild(providerLabel);
            ul.appendChild(price);

            if (droppoint.distance) {
                ul.appendChild(dist); // We're not going to rely on Google Geocoder API.
            }

            ul.appendChild(hours);
            infobox.append(ul);
        };



        SP.selectShippingRadio = function(dont_fill_radio){
            if (!SP.choice) return;

            var provider = SP.getProviderFromDroppoint(SP.choice);
            //var shippingHeader = jQuery('#SP_openOverlay').find('div.shipping-option-header').clone();

            var iconName = false;
            switch (provider.provider_title){
                case 'PostDanmark':
                    iconName = 'post';
                    break;
                case 'GLS':
                    iconName = 'gls-shipment';
                    break;
                case 'DAO':
                    iconName = 'dao';
                    break;
                case 'Bring':
                    iconName = 'bring';
                    break;
                default:
                    iconName = false;
                    break;
            }

            var price = new Element('strong');
            price.innerHTML = SP.formatPrice(provider.rate.price);

            var choiceNameOrNo = (SP.choice.name.length<20) ? SP.choice.name + ': ' : '';
            var providerNameOrNo = (iconName) ? choiceNameOrNo : provider.provider_title + ': ';
            jQuery('#SP_openOverlay').text(
                providerNameOrNo + SP.choice.address
                + ", " + SP.choice.zip
                + " " + SP.choice.city
            ).append(price);

            if (iconName){
                var iconUrl = 'https://www.helsebixen.dk/shop/media/shipmentlogos/' + iconName + '.png';
                var shipmenticon = new Element('img', {
                    'class': 'shipment-logo',
                    'src': iconUrl
                });
                jQuery('#SP_openOverlay').prepend(shipmenticon);
            }


            jQuery('#SP_shippingRadio').val(provider.rate.code);

            if (!dont_fill_radio){
                jQuery('#SP_shippingRadio').attr('checked', 'checked').prop('checked',true);
            }
           // if (SP.settings.autoSelect) jQuery('#SP_openOverlay').prepend(shippingHeader);
        };


        /**
         * Function SP.saveDroppointChoice
         *
         * Sends selected droppoint and shipping method data to the server and updates it in UI.
         */
        SP.saveDroppointChoice = function(){
            if (!SP.choice) {
                //SP.createOverlayError('Fejl. Prøv igen senere eller vælg en anden leverings metode');
                return
            }

            var provider = SP.getProviderFromDroppoint(SP.choice);
            var totals = get_totals_element();
            totals.update('<div class="loading-ajax">&nbsp;</div>');

            jQuery('#SP_openOverlay').empty().text("Opdaterer...").data('loading', true);
            jQuery('#SP_shippingRadio').attr('disabled', true);

            var totals_summary;

            // Only update UI and fill radio when we're sure all data is set on backend.
            var saveComplete = function(){
                if (setMethod && saveDroppoint) {
                    totals.update(totals_summary);
                    SP.selectShippingRadio();
                    clearTimeout(SP.saveTimeout);
                    jQuery('#SP_openOverlay').data('loading', false);
                    jQuery('#SP_shippingRadio').attr('disabled', false);
                }
            };

            var saveFailed = function(){
                jQuery('#SP_openOverlay').text("Der opstod en fejl, prøv igen").data('loading', false);
                jQuery('#SP_shippingRadio').attr('disabled', false);
            };

            var setMethod = false;
            new Ajax.Request(url('/onestepcheckout/ajax/set_methods_separate'), {
                method: 'post',
                parameters: {shipping_method: provider.rate.code},
                onComplete: function(transport){
                    if (transport.status == 200){
                        var data = transport.responseText.evalJSON();
                        if (!data.summary) return;
                        totals_summary = data.summary;
                        setMethod = true;
                        saveComplete();
                    }
                }
            });


            var saveDroppoint = false;
            var postForm = [];
            postForm['street'] = SP.choice.address;
            postForm['name'] = SP.choice.name;
            postForm['zip'] = SP.choice.zip;
            postForm['city'] = SP.choice.city;
            postForm['code'] = provider.rate.code;
            postForm['serviceId'] = SP.choice.serviceId;

            new Ajax.Request(url('/shippingpicker/index/saveDroppointAsNewRow'), {
                method: 'post',
                parameters: postForm,
                onComplete: function(transport){
                    if (transport.status == 200){
                        saveDroppoint = true;
                        saveComplete();
                    } else {
                        saveFailed();
                    }
                }
            });


            SP.saveTimeout = setTimeout(function(){
               SP.createOverlayError('Fejl. Prøv igen senere eller vælg en anden leverings metode');
            }, 10000);

        };



        SP.getProviderFromDroppoint = function(findDropPoint){
            for (var i=0; i<SP.providers.length; i++){
                var provider = SP.providers[i];
                for (var h=0; h<provider.dropPoints.length; h++){
                    if (provider.dropPoints[h] == findDropPoint){
                        return provider;
                    }
                }
            }
        };


        /**
         * SP.loopDroppoints
         *
         * We loop so much, this would be convenient to weed out redundancies.
         * @param callback
         */
        SP.loopDroppoints = function(callback) {
            for (var i = 0; i < SP.providers.length; i++) {
                var provider = SP.providers[i];

                if (!provider['dropPoints']) continue;

                for (var h=0; h < provider['dropPoints'].length; h++) {
                    var droppoint = provider['dropPoints'][h];
                    callback(droppoint, provider);
                }
            }
        };


        SP.formatPrice = function(price){
            if (price<=0){
                return '<div class="droppointprice free">Gratis</div>';
            }

            return '<div class="droppointprice">' + parseFloat(price).toFixed(2) + ' kr.</div>';
        };


        SP.clearDroppoints = function() {
            SP.loopDroppoints(function(droppoint) {
                var marker = droppoint.marker;
                if (!marker) return;
                marker.setMap(null);
                marker = null;
            });

            jQuery('#SP_droppointList').empty();
        };


        /**
         * SP.calcDistance
         *
         * Distance between two coordinates with respect for globe circumference.
         * @param lat1
         * @param lng1
         * @param lat2
         * @param lng2
         * @returns {number}
         */
        SP.calcDistance = function(lat1, lng1, lat2, lng2){
            var R = 6371; // Radius of the earth in km
            var dLat = (lat2 - lat1) * Math.PI / 180;  // deg2rad below
            var dLon = (lng2 - lng1) * Math.PI / 180;
            var a = 0.5 - Math.cos(dLat)/2 + Math.cos(lat1 * Math.PI / 180) * Math.cos(lat2 * Math.PI / 180) * (1 - Math.cos(dLon))/2;

            return R * 2 * Math.asin(Math.sqrt(a));
        };


        SP.calculateAllDroppointDistance = function(){
            var shp_cords = SP.shippingAddress.coordinates;

            var closestDist;

            for (var i=0; i<SP.providers.length; i++){
                var droppoints = SP.providers[i]['dropPoints'];

                for (var h=0; h<droppoints.length; h++){
                    var dp_cords = droppoints[h].position;

                    var dist = SP.calcDistance(dp_cords.lat, dp_cords.lng, shp_cords.lat, shp_cords.lng);

                    dist = Math.floor(dist * 1000);
                    droppoints[h].distance = dist;
                    droppoints[h].provider_key = i;

                    if (!closestDist || closestDist>dist){
                        closestDist = dist;
                    }

                }
            }
        };


        SP.findRecommendDroppoint = function(){
            if (!SP.shippingAddress.coordinates) return false;
            var useClosest = SP.settings.useClosest;

            var candidate;
            var candidateProvider;
            SP.loopDroppoints(function(droppoint, provider){
                if (!candidate){
                    candidate = droppoint;
                    candidateProvider = provider;
                }

                var penaltyDistance = SP.calculateDroppointPenaltyDistance(droppoint);
                var candidatePenaltyDistance = SP.calculateDroppointPenaltyDistance(candidate);

                // Find den tætteste
                if (useClosest && penaltyDistance < candidatePenaltyDistance){
                    candidate = droppoint;
                    candidateProvider = provider;
                    return;
                }

                // Find den billigste
                if (!useClosest && provider.rate.price < candidateProvider.rate.price){
                    if (penaltyDistance - candidatePenaltyDistance < 1000){ // Kun worth it hvis den ikke er super meget længere væk
                        candidate = droppoint;
                        candidateProvider = provider;
                        return;
                    }
                }

                // Find den tættere på til samme pris
                if (provider.rate.price == candidateProvider.rate.price && penaltyDistance < candidatePenaltyDistance){
                    candidate = droppoint;
                    candidateProvider = provider;
                }
            });


            return candidate;
        };


        /**
         * Array.prototype.indexOf
         *
         * Some browsers don't support this function by default.
         */
        Array.prototype.indexOf || (Array.prototype.indexOf = function(d, e) {
            var a;
            if (null == this) throw new TypeError('"this" is null or not defined');
            var c = Object(this),
                b = c.length >>> 0;
            if (0 === b) return -1;
            a = +e || 0;
            Infinity === Math.abs(a) && (a = 0);
            if (a >= b) return -1;
            for (a = Math.max(0 <= a ? a : b - Math.abs(a), 0); a < b;) {
                if (a in c && c[a] === d) return a;
                a++
            }
            return -1
        });


        SP.filterProvider = function(provider_title, removeFilter){
            var filtered = SP.filters.providers;
            var index = filtered.indexOf(provider_title);

            if (removeFilter && index!=-1) filtered.splice(index, 1);
            if (!removeFilter) filtered.push(provider_title);


            SP.clearDroppoints();
            SP.createMarkers();
            SP.createYAH();
        };


        SP.filterClosest = function(am){
            SP.filters.closest = am;
            SP.clearDroppoints();
            SP.createMarkers();
            SP.createYAH();

        };



        SP.clear = function(){
            if (SP.timeout) clearTimeout(SP.timeout);
            if (SP.saveTimeout) clearTimeout(SP.saveTimeout);
            if (SP.ajax_droppoints) SP.ajax_droppoints.transport.abort();
            if (SP.ajax_shipping) SP.ajax_shipping.transport.abort();
        };

        SP.saved = false;
        // Events
        jQuery('#SP_save').click(function(){
            if (!SP.choice) return;
            jQuery('#SP_overlay').foundation('reveal', 'close');
            if (!SP.saved) {
                SP.saveDroppointChoice();
                SP.saved = true;
            }
        });


        return this;
    };
})(window);

























