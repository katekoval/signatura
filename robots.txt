User-agent: *

# Directories
Disallow: /*?p=*&
Disallow: /shopby?*

# Paths
Disallow: /checkout/
Disallow: /onestepcheckout/
Disallow: /customer/
Disallow: /review/
Disallow: /catalog/
Disallow: /pmatch/
Disallow: /app/
Disallow: /downloader/
Disallow: /lib/
Disallow: /pkginfo/
Disallow: /report/
Disallow: /var/
Disallow: /catalog/product_compare/
Disallow: /catalog/ 
Disallow: /customer/
Disallow: /catalogsearch/advanced/
Disallow: /wishlist/
Disallow: /404/
Disallow: /admin/
Disallow: /api/
Disallow: /install/
Disallow: /catalog/product/view/id/</code>
Disallow: /customer/
Disallow: /sales/order/
Disallow: /tag/
Disallow: /b/tag
Disallow: /mgt/review/
Disallow: /review/product/

# Files
Disallow: /cron.php
Disallow: /cron.sh
Disallow: /error_log
Disallow: /install.php
Disallow: /LICENSE.html
Disallow: /LICENSE.txt
Disallow: /LICENSE_AFL.txt
Disallow: /STATUS.txt

# Layered navigation
Disallow: *?*
Disallow: *?p=*
Disallow: *?price=*
Disallow: *?skin_type=*
Disallow: *?product_attribute=*
Disallow: *?manufacturer=*
Disallow: *?content_attributes=*
Disallow: *?gender=*
Disallow: *?hair_colour=*
Disallow: *?hair_type=*
Disallow: *?styling=*
Disallow: *?nail_polish=*
Disallow: *?skin_colour=*

# Do not index session ID
Disallow: /*.php$
Disallow: /*?SID=
